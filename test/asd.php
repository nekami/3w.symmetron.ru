<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
/** @global CMain $APPLICATION */
/** @global CUser $USER */
\Bitrix\Main\Loader::includeModule('catalog');


$connection = Bitrix\Main\Application::getConnection();
/** Bitrix\Main\Diag\SqlTracker $tracker */
$tracker = $connection->startTracker();

\Bitrix\Main\Loader::includeModule('mcart.symmetronsyncdb');
\Bitrix\Main\Loader::includeModule('catalog');
$lastRegularImportTimestamp = (int)\Bitrix\Main\Config\Option::get('mcart.symmetronsyncdb', 'lastDailyIDB', 0);
$lastFullImportTimestamp = (int)\Bitrix\Main\Config\Option::get('mcart.symmetronsyncdb', 'lastFullIHL', 0);

if ($lastRegularImportTimestamp > $lastFullImportTimestamp) {
    $latestTime = $lastRegularImportTimestamp;
} else {
    $latestTime = $lastFullImportTimestamp;
}
var_dump($latestTime);

$borderDate = \Bitrix\Main\Type\DateTime::createFromTimestamp($latestTime);
var_dump($borderDate);

$arFilter = [
    '<TIMESTAMP_X' => $borderDate,
];

$arFilter['!=ELEMENT.IBLOCK_SECTION_ID'] = 11;

$rsProducts = \Bitrix\Catalog\PriceTable::getList([
    'select' => [
        'ID'
    ],
    'filter' => $arFilter,
]);

$counter = 0;
while ($arProduct = $rsProducts->fetch()) {
    echo '<pre>' . print_r($arProduct, true) . "</pre>\n";
    $counter++;
}
var_dump($counter);

$connection->stopTracker();

echo '<hr>';
foreach ($tracker->getQueries() as $query) {
    echo '<pre>' . print_r($query->getSql(), true) . "</pre>\n";
    echo '<hr>';
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");