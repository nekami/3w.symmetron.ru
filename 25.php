<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("255");
?>
    <style>
        header, footer {
            display: none;
        }
    </style>

    <section class="aniversary">
        <div class="wrapper">
            <div></div>
            <div class="header">
                <div>
                    <div class="logo-holder"><div><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 200 30" style="enable-background:new 0 0 200 30;" xml:space="preserve" preserveAspectRatio="x14Y14 meet">
<style type="text/css">
    .st0{fill:#1F1E21;}
</style>
                                <g>
                                    <path class="st0" d="M37.5,6.2c-1.7,0-5.5,1.2-6.3,1.4c0.2,1,0.7,3.9,0.7,3.9h2c0,1.4-0.7,3.7-1.6,6.1h-0.1l-2.7-7.6   c-0.4-1.2-1.2-3.7-3.7-3.7c-1.8,0-3.9,0.8-5.5,1.5v3l2.6,0.8c0.4,0.8,3.9,10.6,3.9,10.6s2.3,0,3.5,0c-1.1,2-2.1,3.4-2.7,3.4   l-4.4-3.1c-1.1,0-2.4,1.5-2.4,3.9c0,1.9,4.8,3.6,8.1,3.6c4.4,0,7.1-3.2,8.9-7.7c2.1-5.5,3.6-9.5,3.6-12.6   C41.2,8.1,40.6,6.2,37.5,6.2z"/>
                                    <path class="st0" d="M18.4,11.3c-0.7-0.5-1.6-1-2.6-1.3c-1-0.3-2-0.6-3.1-0.9c-0.4-0.1-0.8-0.2-1.3-0.3c-0.5-0.1-1-0.3-1.4-0.5   C9.5,8.1,9.1,7.9,8.8,7.7C8.5,7.5,8.3,7.2,8.3,6.9c0-0.4,0.2-0.8,0.6-1.1c0.4-0.3,1-0.4,1.9-0.4c0.3,0,0.7,0,1.2,0.1   c0.5,0.1,1,0.2,1.4,0.4v2.1h5.3V1.6c-0.6-0.3-1.2-0.5-2-0.7s-1.5-0.4-2.2-0.5c-0.7-0.1-1.5-0.2-2.3-0.3c-0.8-0.1-1.5-0.1-2.1-0.1   c-1.8,0-3.4,0.2-4.7,0.6C4.3,1,3.2,1.5,2.4,2.2C1.6,2.9,0.9,3.7,0.6,4.6C0.2,5.5,0,6.5,0,7.6c0,1.6,0.5,3,1.5,4   c1,1.1,2.5,1.9,4.4,2.4c0.9,0.2,1.8,0.4,2.7,0.6c0.9,0.2,1.6,0.4,2.2,0.5c0.3,0.1,0.6,0.3,0.8,0.5c0.2,0.2,0.3,0.5,0.3,0.9   c0,0.1,0,0.3-0.1,0.4c-0.1,0.2-0.2,0.3-0.4,0.5c-0.2,0.1-0.5,0.3-0.8,0.4C10.3,17.9,9.8,18,9.2,18c-0.2,0-0.4,0-0.7,0   s-0.6,0-0.9-0.1c-0.3,0-0.7-0.1-1-0.1c-0.3-0.1-0.6-0.1-0.9-0.2v-2.3H0.4v6.7c1.5,0.5,3.1,0.9,4.7,1.2c1.6,0.2,3.2,0.4,4.7,0.4   c2,0,3.7-0.2,5-0.5c1.4-0.3,2.5-0.8,3.4-1.5c0.9-0.6,1.5-1.4,2-2.3c0.4-0.9,0.6-2,0.6-3.1c0-1.1-0.2-2-0.6-2.8   C19.7,12.4,19.1,11.8,18.4,11.3z"/>
                                    <path class="st0" d="M121.5,8.9c-0.6-0.7-1.5-1.3-2.5-1.8c-1-0.4-2.4-0.6-3.9-0.6c-1.6,0-3,0.2-4.2,0.7c-1.2,0.5-2.1,1.1-2.8,1.8   c-0.7,0.8-1.2,1.7-1.6,2.7c-0.3,1-0.5,2.1-0.5,3.3c0,1.1,0.1,2.1,0.4,3.1c0.2,1,0.7,1.9,1.4,2.7c0.7,0.8,1.6,1.4,2.8,1.9   c1.2,0.5,2.7,0.7,4.6,0.7c0.6,0,1.2,0,1.8-0.1c0.6-0.1,1.3-0.1,2-0.3c0.7-0.1,1.4-0.3,2-0.5c0.7-0.2,1.3-0.5,1.9-0.8l-1.4-4.4   c-0.7,0.2-1.5,0.4-2.4,0.6c-0.9,0.2-1.7,0.2-2.4,0.2c-0.5,0-0.9,0-1.3-0.1c-0.4-0.1-0.7-0.2-1-0.4c-0.3-0.2-0.5-0.4-0.6-0.8   c-0.1-0.3-0.2-0.7-0.2-0.9l9.6-0.4v-1.1c0-1.1-0.1-2.1-0.4-3.1C122.6,10.5,122.1,9.6,121.5,8.9z M113.6,13.8c0-0.9,0.2-1.5,0.4-1.8   c0.3-0.4,0.6-0.6,1.1-0.6c0.5,0,0.8,0.2,1,0.6c0.2,0.4,0.3,0.9,0.3,1.5L113.6,13.8z"/>
                                    <path class="st0" d="M137,17.7c-0.4,0-0.8,0.1-0.9,0.1c-0.4,0-0.8,0-1-0.1c-0.3,0-0.5-0.2-0.7-0.3c-0.2-0.2-0.3-0.4-0.4-0.7   c-0.1-0.3-0.1-0.8-0.1-1.3v-3.9h4.4V6.9h-4.4V1.8l-5.4,0.4l-1.7,4.4l-2.7,0.8v4.1h2.2v5.8c0,0.6,0.1,1.3,0.2,2c0.1,0.7,0.4,1.4,1,2   c0.5,0.6,1.3,1.1,2.3,1.5c1,0.4,2.4,0.6,4.1,0.6c0.3,0,0.7,0,1.2,0c0.5,0,0.9-0.1,1.4-0.2c0.5-0.1,1-0.2,1.4-0.3   c0.5-0.1,0.9-0.2,1.3-0.4l-0.8-4.8C137.9,17.6,137.5,17.7,137,17.7z"/>
                                    <path class="st0" d="M156.3,6.6c-1.2,0-2.5,0.7-3.1,1c-1.1,0.6-2.5,2-2.9,2.3l-0.2-0.1V6.8H141v4.6h1.6v6.8h-1.7v4.6h11.7v-4.6   h-2.4v-4.5l6.4-0.3c0.6-1.8,1.2-3.8,1.2-4.8c0-0.8-0.1-1.2-0.3-1.6C157.3,6.7,156.9,6.6,156.3,6.6z"/>
                                    <path class="st0" d="M174.9,8.8c-0.8-0.7-1.8-1.3-3.1-1.7c-1.2-0.4-2.6-0.6-4.3-0.6c-1.6,0-3,0.2-4.3,0.6c-1.2,0.4-2.2,1-3.1,1.7   c-0.8,0.7-1.4,1.6-1.8,2.6c-0.4,1-0.6,2.2-0.6,3.4c0,1.3,0.2,2.4,0.6,3.4c0.4,1,1,1.9,1.8,2.7c0.8,0.7,1.8,1.3,3.1,1.7   c1.2,0.4,2.6,0.6,4.3,0.6c1.6,0,3-0.2,4.3-0.6c1.2-0.4,2.2-1,3.1-1.7c0.8-0.7,1.4-1.6,1.8-2.7c0.4-1,0.6-2.2,0.6-3.4   c0-1.2-0.2-2.4-0.6-3.4S175.8,9.6,174.9,8.8z M169.1,17.4c-0.3,0.6-0.8,0.9-1.5,0.9c-0.7,0-1.2-0.3-1.5-1c-0.3-0.6-0.4-1.5-0.4-2.5   c0-0.4,0-0.8,0.1-1.2c0.1-0.4,0.2-0.7,0.3-1c0.2-0.3,0.3-0.5,0.6-0.7c0.2-0.2,0.5-0.3,0.9-0.3c0.7,0,1.2,0.3,1.5,0.9   c0.3,0.6,0.4,1.4,0.4,2.4C169.5,15.9,169.4,16.8,169.1,17.4z"/>
                                    <path class="st0" d="M197.8,18.1c0,0,0-5.6,0-7c0-1.7-0.7-3.1-1.5-3.8c-0.8-0.7-1.8-1.1-3-1.1c-0.4,0-0.9,0.1-1.4,0.1   c-0.5,0.1-1.1,0.3-1.7,0.6c-0.6,0.3-1.2,0.7-1.7,1.2c-0.6,0.5-1.1,1.2-1.6,2h-0.2V6.8h-8.8v3.6l1.8,1v11.5h7.3c0,0,0-6.2,0-7.5   c0-2,1-3.6,2.2-3.6c0.4,0,0.7,0.1,0.9,0.4c0.2,0.3,0.4,0.8,0.4,1.6c0,1.1,0,4.3,0,6c0,1.8,0.7,2.6,1,2.9c0.3,0.3,0.7,0.5,1.2,0.7   c0.5,0.2,1.2,0.2,2,0.2c0.4,0,0.9,0,1.5-0.1c1.6-0.3,3.1-1,3.7-1.3c0-0.8,0-4.1,0-4.1H197.8z"/>
                                    <path class="st0" d="M71.8,18.1c0-0.5,0-5.6,0-6.2c0-0.6,0-1.1,0-1.3c-0.2-1.4-0.7-2.5-1.5-3.2c-0.8-0.7-1.8-1.1-3-1.1   c-2.2,0-4,0.9-5.7,2.6l-0.2,0c-0.4-1.5-2.1-2.6-4.1-2.6c-2.8,0-4.6,1.2-6,2.6h0h-0.1v-2h-8.8v4.6h1.8v11.5h7.2v-7.5v-2.1   c0-2,1.3-2.5,1.9-2.5c0.9,0,1.1,0.9,1.1,1.2c0,0.3,0,10.3,0,10.9h7.2c0,0,0-8.1,0-9.6c0-1.6,0.9-2.5,1.8-2.5c0.8,0,1,0.6,1,1.2   c0,1.2,0,6.4,0,7.7c0,2,0.8,2.6,1.1,2.9c0.3,0.3,0.7,0.5,1.3,0.7c0.5,0.2,1.2,0.2,2,0.2c0.4,0,0.9,0,1.5-0.1   c0.6-0.1,1.1-0.2,1.7-0.4c0.6-0.2,1.2-0.4,1.8-0.7c0-0.8,0-4.2,0-4.2S72.1,18.1,71.8,18.1z"/>
                                    <path class="st0" d="M103.3,18.1c0-0.5,0-5.6,0-6.2c0-0.6,0-1.1,0-1.3c-0.2-1.4-0.7-2.5-1.5-3.2c-0.8-0.7-1.8-1.1-3-1.1   c-2.2,0-4,0.9-5.7,2.6l-0.2,0c-0.4-1.5-2.1-2.6-4.1-2.6c-2.8,0-4.6,1.2-6,2.6h0h-0.1v-2h-8.8v4.6h1.8v11.5H83v-7.5v-2.1   c0-2,1.3-2.5,1.9-2.5c0.9,0,1.1,0.9,1.1,1.2c0,0.3,0,10.3,0,10.9h7.2c0,0,0-8.1,0-9.6c0-1.6,0.9-2.5,1.8-2.5c0.8,0,1,0.6,1,1.2   c0,1.2,0,6.4,0,7.7c0,2,0.8,2.6,1.1,2.9c0.3,0.3,0.7,0.5,1.3,0.7c0.5,0.2,1.2,0.2,2,0.2c0.4,0,0.9,0,1.5-0.1   c0.6-0.1,1.1-0.2,1.7-0.4c0.6-0.2,1.2-0.4,1.8-0.7c0-0.8,0-4.2,0-4.2S103.7,18.1,103.3,18.1z"/>
                                </g>
</svg></div></div>

                    <div class="header-text__ru">Фоторепортаж празднования <span>25-летия компании</span></div>
                    <div class="header-text__en">Photo report of the 25th anniversary celebration</div>
                </div>
                <div class="years-holder">
                    25
                </div>
            </div>
            <div id="block-slider" style="display: none">
                <ul class="photo-slider owl-carousel owl-theme" id="photo_slide">
                    <?
                    $photo_array=array("1"=>array('a','b','c','d','e','f','g'),
                        "2"=>array('a','b','c','d','e'),"3"=>array('a','b','c'),"4"=>array('a','b','c','d','e','f','g','h','i','j'),
                        "5"=>array('a','b','c','d','f','g','h'),"6"=>array('a','b','c','d','e','f','i','k'),
                        "7"=>array('a','b','c','d','e','f','g','h','k','l'),"8"=>array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','r'),
                        "9"=>array('a','b','c'));
                    $maxPhoto = 6;
                    $c = 1;
                    $arAllPhotos = [];
                    $slider = "";

                    $countArr = 0;
                    foreach ($photo_array as $value) {
                        $countArr += count($value);
                    }

                    foreach ($photo_array as $key=>$value) {
                        foreach ($value as $val) { ?>
                            <?
                            /*$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/upload/photo/' .  $key.$val . '.jpg');
                            echo "<pre>";
                            print_r($arFile);
                            echo "</pre>";
                            CFile::ResizeImage($arFile, ["width" => "10", "height" => "15"]);
                            copy($arFile["tmp_name"], $_SERVER["DOCUMENT_ROOT"].'/upload/photo_mini/'.$arFile["name"]);
                            echo "<pre>";
                            print_r($arFile);
                            echo "</pre>";*/
                            ?>


                            <li class="slide">
                                <a href="/upload/photo/<?= $key.$val ?>.jpg" class="image-link">
                                    <img class = "<?= $key.$val ?>"
                                         data-img="/upload/photo/<?= $key.$val ?>.jpg"
                                        <?if($c <= $maxPhoto / 2 || $c >= $countArr - $maxPhoto / 2):?>
                                            src="/upload/photo/<?= $key.$val ?>.jpg"
                                        <?else:?>
                                            src="/upload/photo_mini/<?= $key.$val ?>.jpg"
                                            <?$arAllPhotos[] = $key . $val;?>
                                        <?endif;?>
                                >
                                </a>
                            </li>
                            <?/*if($c <= $maxPhoto):*/?><!--
                                <li class="slide">
                                    <a href="/upload/photo/<?/*= $key.$val */?>.jpg" class="image-link">
                                        <img class = "<?/*= $key.$val */?>"
                                             data-img="/upload/photo/<?/*= $key.$val */?>.jpg"
                                             src="/upload/photo/<?/*= $key.$val */?>.jpg"

                                    >
                                    </a>
                                </li>
                            <?/*else:*/?>
                                <?/*$arAllPhotos[] = $key . $val;*/?>
                            --><?/*endif;*/?>
                            <?$c++;
                        }
                    }
                    echo  $slider;
                    ?>
                    <!--li class="slide">
                        <a href="/upload/photo/2e.jpg" class="image-link"><img  src="/upload/photo/2e.jpg"></a>

                    </li>
                    <li class="slide">
                        <a href="/upload/photo/2e.jpg" class="image-link"><img  src="/upload/photo/2e.jpg"></a>

                    </li>
                    <li class="slide">
                        <a href="/upload/photo/4h.jpg"  class="image-link"  rel="prettyPhoto"><img  src="/upload/photo/4h.jpg"></a>

                    </li>
                    <li class="slide">
                        <a href="/upload/photo/5d.jpg"  class="image-link"  rel="prettyPhoto"><img  src="/upload/photo/5d.jpg"></a>

                    </li>
                    <li class="slide">
                        <a href="/upload/photo/7d.jpg"  class="image-link"  rel="prettyPhoto"><img  src="/upload/photo/7d.jpg"></a>

                    </li-->
                </ul>
            </div>

            <a href="https://www.facebook.com/Symmetron/photos/?tab=album&album_id=2411899228887882" class="fb">
                <div class="fb-img">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#000000" version="1.1" x="0px" y="0px" viewBox="18.5 19 62.5 62" style="enable-background:new 0 0 100 100;" xml:space="preserve"><path d="M74.2099609,19H25.7900391C22.0458984,19,19,22.0458984,19,25.7900391v48.4199219  C19,77.9541016,22.0458984,81,25.7900391,81h21.8701172c0.5527344,0,1-0.4477539,1-1V57.5297852c0-0.5522461-0.4472656-1-1-1  h-4.1708984v-5.2099609h4.1708984c0.5527344,0,1-0.4477539,1-1v-1.6396484c0-5.2666016,1.5302734-9.6225586,4.3066406-12.2641602  C55.2597656,34.2451172,58.6328125,33,62.2197266,33c1.4160156,0,2.734375,0.1230469,3.9296875,0.3657227v5.5341797  c-0.7890625-0.152832-1.6279297-0.2299805-2.5097656-0.2299805c-4.7216797,0-7.4296875,3.5356445-7.4296875,9.7001953v1.949707  c0,0.5522461,0.4472656,1,1,1h6.9980469l-1.7119141,5.2099609h-5.2060547c-0.5527344,0-1,0.4477539-1,1V80  c0,0.5522461,0.4472656,1,1,1h16.9199219C77.9541016,81,81,77.9541016,81,74.2099609V25.7900391  C81,22.0458984,77.9541016,19,74.2099609,19z"/></svg>
                </div>
                <div class="fb-text">
                    <div class="fb-text__ru">Фоторепортаж на нашей странице в Facebook</div>
                    <div class="fb-text__en">Photo report on our Facebook</div>
                </div>
            </a>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            let allPhotos = <?=CUtil::PhpToJSObject($arAllPhotos);?>;
            let allPhotosLen = allPhotos.length;

            function LoadPhoto() {
                let max = 30;
                let count = 0;
                //console.log(allPhotosLen)
                //console.log(allPhotos)
                for(let i in allPhotos){
                    $("." + allPhotos[i]).each(function (idx, elem){
                        let elemJQ = $(elem)
                        elemJQ.attr("src", elemJQ.attr("data-img"))
                    })
                    delete allPhotos[i];
                    count++;
                    allPhotosLen--
                    if(max === count) {
                        break;
                    }
                }
            }

            function LoadPhotoBack() {
                let max = 30;
                let count = 0;
                //console.log(allPhotosLen)
                //console.log(allPhotos)
                let l = allPhotosLen;
                for(let i in allPhotos){
                    if(count >= l - max) {
                        $("." + allPhotos[i]).each(function (idx, elem) {
                            let elemJQ = $(elem)
                            elemJQ.attr("src", elemJQ.attr("data-img"))
                        })
                        delete allPhotos[i];
                        allPhotosLen--

                    }

                    count++;
                }
            }

            let loadFlag = false;
            $('.owl-carousel').on('changed.owl.carousel', function(event) {
                if(loadFlag) {
                    LoadPhoto();
                    LoadPhotoBack();
                } else {
                    document.getElementById("block-slider").style.display = "";
                    loadFlag = true;
                }
            });

        });

    </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>