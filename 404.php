<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?><div class="bx-404-container">
	<div class="bx-404-block">
		<img src="<?=SITE_DIR?>images/404.png" alt="">
	</div>
	<div class="bx-404-text-block">
		Неправильно набран адрес, <br>
		или такой страницы на сайте больше не существует.
	</div>
	<div>
		Вернитесь на <a href="<?=SITE_DIR?>">главную</a> или воспользуйтесь поиском.
	</div>
</div>
<div class="map-columns row" style="display: none !important;">
	<div class="col-sm-10 col-sm-offset-1">
		<div class="bx-maps-title">
			Карта сайта:
		</div>
	</div>
</div>
<div class="col-sm-offset-2 col-sm-4" style="display: none !important;">
	<div class="bx-map-title">
		<i class="fa fa-leanpub"></i> Каталог
	</div>
	 <?/*$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"tree",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "tree",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(0=>"",1=>"",),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"TOP_DEPTH" => "2"
	)
);*/?>
</div>
<div class="col-sm-offset-1 col-sm-4" style="display: none !important;">
	<div class="bx-map-title">
		<i class="fa fa-info-circle"></i> О магазине
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.map",
	".default",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COL_NUM" => "2",
		"COMPONENT_TEMPLATE" => ".default",
		"LEVEL" => "3",
		"SET_TITLE" => "N",
		"SHOW_DESCRIPTION" => "Y"
	)
);?>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>