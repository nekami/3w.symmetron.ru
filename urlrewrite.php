<?php
$arUrlRewrite=array (
  97 => 
  array (
    'CONDITION' => '#^/activity/filter/(.+?)/apply/#',
    'RULE' => 'SMART_FILTER_PATH=$1',
    'ID' => '',
    'PATH' => '/activity/index.php',
    'SORT' => 97,
  ),
  98 => 
  array (
    'CONDITION' => '#^/events/filter/(.+?)/apply/#',
    'RULE' => 'SMART_FILTER_PATH=$1',
    'ID' => '',
    'PATH' => '/events/index.php',
    'SORT' => 98,
  ),
  99 => 
  array (
    'CONDITION' => '#^/news/filter/(.+?)/apply/#',
    'RULE' => 'SMART_FILTER_PATH=$1',
    'ID' => '',
    'PATH' => '/news/index.php',
    'SORT' => 99,
  ),
  108 => 
  array (
    'CONDITION' => '#^/brands/([_a-zA-Z0-9-]*)/series/([_a-zA-Z0-9-]+)((.*))?#',
    'RULE' => 'BRAND=$1&SERIES_CODE=$2',
    'ID' => '',
    'PATH' => '/series/detail.php',
    'SORT' => 100,
  ),
  117 => 
  array (
    'CONDITION' => '#^/amp/sections/([^/]+?)/\\??(.*)#',
    'RULE' => '&SECTION_ID=$1&$2',
    'ID' => 'bitrix:catalog.section',
    'PATH' => '/amp/sections/index.php',
    'SORT' => 100,
  ),
  116 => 
  array (
    'CONDITION' => '#^/amp/catalog/([^/]+?)/\\??(.*)#',
    'RULE' => '&ELEMENT_ID=$1&$2',
    'ID' => 'bitrix:catalog.element',
    'PATH' => '/amp/catalog/index.php',
    'SORT' => 100,
  ),
  50 => 
  array (
    'CONDITION' => '#^/about/sertifikaty/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/about/sertifikaty/index.php',
    'SORT' => 100,
  ),
  69 => 
  array (
    'CONDITION' => '#^/industrysolution/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/industrysolution/index.php',
    'SORT' => 100,
  ),
  118 => 
  array (
    'CONDITION' => '#^/amp/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/amp/news/index.php',
    'SORT' => 100,
  ),
  121 => 
  array (
    'CONDITION' => '#^/activity/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/activity/index.php',
    'SORT' => 100,
  ),
  114 => 
  array (
    'CONDITION' => '#^/catalog/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/catalog/index.php',
    'SORT' => 100,
  ),
  124 => 
  array (
    'CONDITION' => '#^/events/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/events/index.php',
    'SORT' => 100,
  ),
  125 => 
  array (
    'CONDITION' => '#^/brands/#',
    'RULE' => '',
    'ID' => 'mcart:brands.page',
    'PATH' => '/brands/index.php',
    'SORT' => 100,
  ),
  72 => 
  array (
    'CONDITION' => '#^/docs/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/docs/index.php',
    'SORT' => 100,
  ),
  81 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
  120 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
  104 => 
  array (
    'CONDITION' => '#^/faq/#',
    'RULE' => '',
    'ID' => 'bitrix:support.faq',
    'PATH' => '/faq/index.php',
    'SORT' => 100,
  ),
);
