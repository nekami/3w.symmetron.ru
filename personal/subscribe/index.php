<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подписка");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?><section class="subscription">
<div class="wrapper">
	<h1><?$APPLICATION->ShowTitle();?></h1>
	 <?$APPLICATION->IncludeComponent(
	"mcart:sender.subscribe",
	"",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONFIRMATION" => "Y",
		"EVERY_DAY" => array(0=>"3",1=>"4",),
		"HIDE_MAILINGS" => "N",
		"PRODUCTS" => array(0=>"8",1=>"9",2=>"10",),
		"SET_TITLE" => "N",
		"SHOW" => array(0=>"5",1=>"6",2=>"7",),
		"SHOW_HIDDEN" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "N",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_PERSONALIZATION" => "N"
	)
);?>
</div>
</section> <br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>