<?
define("NEED_AUTH", true);

use Bitrix\Main\Localization\Loc;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");

global $USER;
$ID = USER_ID;
$res = CUser::GetList($by = "id", $order = "asc", ['ID' => $ID], ['SELECT' => ["UF_FAVS"]])->Fetch();
$count = '';
if ($res["UF_FAVS"]) {
    global $arrFilter;
    $arrFilter['ID'] = $res["UF_FAVS"];
    $count = count($res["UF_FAVS"]);
}
?>
<section class="favorite">
    <div class="wrapper">
        <h1><? $APPLICATION->ShowTitle(); ?></h1>
        <?
        if ($count) {
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "favorites",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "N",
                    "CHECK_DATES" => "Y",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "ID",
                        1 => "CODE",
                        2 => "NAME",
                        3 => "DETAIL_PICTURE",
                        4 => "",
                    ),
                    "FILTER_NAME" => "arrFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "4",
                    "IBLOCK_TYPE" => "catalog",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => $count,
                    "PAGER_BASE_LINK" => "",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
                    "PAGER_PARAMS_NAME" => "arrPager",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "modern",
                    "PAGER_TITLE" => "Продукция",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "ARTICLE",
                        1 => "",
                        2 => "manufacturer",
                        3 => "article",
                        4 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "PROPERTY_ACTIVE_STATUS",
                    "SORT_BY2" => "PROPERTY_ITEMID",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => "favorites",
                    "PROPERTY_CODE_FAST_SHOW_LEFT" => array(
                        0 => "ARTICLE",
                        1 => "",
                        2 => "KPD",
                        3 => "WATERPROOFNESS",
                        4 => "",
                    ),
                    "PROPERTY_CODE_FAST_SHOW_RIGHT" => array(
                        0 => "BLUETOOTH_LOW_ENERGY",
                        1 => "",
                        2 => "APPLICATIONS",
                        3 => "CAN",
                        4 => "",
                    )
                ),
                false
            );
        } ?>
    </div>
</section>
<br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
