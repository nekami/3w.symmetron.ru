<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Main\Localization\Loc;

$APPLICATION->SetTitle("Персональные данные");
?>
<section class="personal">
	<div class="wrapper">
		<h1><?$APPLICATION->ShowTitle();?></h1>
		<?$APPLICATION->IncludeComponent("bitrix:main.profile", "personal", Array(
			"AJAX_MODE" => "Y",	// Включить режим AJAX
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"CHECK_RIGHTS" => "N",	// Проверять права доступа
			"SEND_INFO" => "N",	// Генерировать почтовое событие
			"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
			"USER_PROPERTY" => "",	// Показывать доп. свойства
			"USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
			),
		false
		);?>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");