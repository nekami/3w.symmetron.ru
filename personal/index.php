<?
define("NEED_AUTH", true);
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
$prefix = "SYMMETRON_DEFAULT_SITE_TEMPLATE_";

global $USER;
$ID = USER_ID;
$res = CUser::GetList($by = "id", $order = "asc", ['ID'=>$ID],['SELECT'=>["UF_FAVS"]])->Fetch();
?>
<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/lk.css");?>
<section class="personal">
    <div class="wrapper">
        <h1><?$APPLICATION->ShowTitle();?></h1>
        <div class="lk-wrapper">
            <div class="lk-left">
                <div class="user-info">
                    <h3><?=Loc::getMessage($prefix."PERSONAL")?></h3>
                    <div class="user-fio">
                        <div class="left"><h4><?=Loc::getMessage($prefix."FIO")?></h4></div>
                        <div class="right">
                        	<?
                        	$email = $res['EMAIL'];
                            $name = $res['NAME'];
                            echo $name ? '<span>'.$name.'</span>' :''; 
                        	$family = $res['LAST_NAME'];
                            echo $family ? '/ <span>'.$family.'</span>' :''; 
                            $otch = $res['SECOND_NAME'];
                        	echo $otch ? '/ <span>'.$otch.'</span>' :'';
                            $phone = $res['PERSONAL_PHONE'];
                            $groups = CUser::GetUserGroup($USER->GetID());
                        	?>
                        </div>
                    </div>
                    <div class="user-mail">
                        <div class="left"><h4>E-mail</h4></div>
                        <div class="right">
                            <span class="current-mail"><?=$email?></span>
                            <!-- добавить элементу ниже класс  для окрашивание в красный -->
                            <span class="current-mail__status <?= $email ? '': 'confirm-false'?>">
                            	<?if( array_search(6, $groups)!==FALSE)  echo '<span class="glk"></span>'.Loc::getMessage($prefix."EMAIL_MESS");?>
                            </span>
                        </div>
                    </div>
                    <div class="user-phone">
                        <div class="left"><h4><?=Loc::getMessage($prefix."PHONE");?></h4></div>
                        <div class="right"><span class="current-phone"><?=$phone?></span></div>
                    </div>
                    <div class="user-button">
                        <a href="edit" class="default-button"><?=Loc::getMessage($prefix."PERSONAL_EDIT");?></a>
                    </div>
                </div>
            </div>
            <div class="lk-right">
                <div class="user-favorites">
                    <h3><?=Loc::getMessage($prefix."FAVORITES");?></h3>
                    <div class="favorites-items">
                        <div class="left">
                            <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                            </span>
                            <span><?=Loc::getMessage($prefix."FAVORITES");?></span>
                            <?
                            $res = CUser::GetList($by = "id", $order = "asc", ['ID'=>$ID],['SELECT'=>["UF_FAVS"]])->Fetch();
                            $res["UF_FAVS"] = array_unique($res["UF_FAVS"]);

                            if(!empty($res["UF_FAVS"])){
                                if(CModule::IncludeModule("iblock"))
                                    $countFavs = CIBlockElement::GetList(array(), array("ID" => $res["UF_FAVS"], "ACTIVE"=>"Y"), array(), array(), array("ID"));
                            }
                            ?>
                            <span ><span class="favorite-total"><?= ($res["UF_FAVS"] ? '+'.$countFavs:0)?></span></span>
                        </div> 
                        <div class="right">
                            <a href="favorites" class="default-button"><?=Loc::getMessage($prefix."EDIT");?></a>
                        </div>
                    </div>
                </div>
                <div class="user-subscribe">
                    <h3><?=Loc::getMessage($prefix."SUBSCRIBE");?></h3>
                    <span class="headings">		
	                    <?
	                    $subscriptionDb = \Bitrix\Sender\MailingSubscriptionTable::getSubscriptionList(array(
						   'select' => array('ID' => 'CONTACT_ID', 'EMAIL' => 'CONTACT.CODE', 'EXISTED_MAILING_ID' => 'MAILING.ID'),
						   'filter' => array('=CONTACT.CODE' => $email, '!MAILING.ID' => null),
						));
						while ( $ob = $subscriptionDb->Fetch() ) {
							$arrSubs[] = (int) $ob["EXISTED_MAILING_ID"];
						}
						
						if( !empty($arrSubs) ) {
							$res = \Bitrix\Sender\MailingTable::getList(array( 
								'select' => array('NAME'), 
								'filter' => array('ID'=> $arrSubs, 'ACTIVE'=>'Y')
							));
							while ( $ob = $res->Fetch() ) {
								echo ($k++ ? ', ' : '').$ob['NAME'];
							}
						}
	                    ?>
                	</span>
                    <span class="subscribe-mail"><?=$email?></span>
                    <a href="subscribe" class="default-button change-subscription"><?=Loc::getMessage($prefix."SUBSCRIBE_EDIT");?></a>
                </div>
            </div>

        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>