<?
$prefix = "SYMMETRON_DEFAULT_SITE_TEMPLATE_";

$MESS[$prefix."EDIT"] = "Изменить";
$MESS[$prefix."EDIT_EMAIL"] = "Изменить e-mail";
$MESS[$prefix."PERSONAL"] = "Личные данные";
$MESS[$prefix."PERSONAL_EDIT"] = "Изменить личные данные";
$MESS[$prefix."FAVORITES"] = "Избранные товары";
$MESS[$prefix."FIO"] = "ФИО";
$MESS[$prefix."PHONE"] = "Телефон";
$MESS[$prefix."EMAIL_MESS"] = "Адрес подтвержден";
$MESS[$prefix."SUBSCRIBE"] = "Подписка на рассылку";
$MESS[$prefix."SUBSCRIBE_EDIT"] = "Изменить подписку";
