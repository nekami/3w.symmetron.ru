<?
@session_start();
header("Content-Type: application/json");

require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.section/");
BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.element/");
extract($_POST);
if ( $action ){
	global $USER, $DB;
	if (!is_object($USER)) $USER = new CUser;
	define('AUTH_USER', $USER->IsAuthorized() );
	if ( AUTH_USER ){
		$ret['noAuth']= false;
		$USER_ID = $USER->GetID();
		switch ($action) {
			
			case 'fav':
			if( (int) $id ) {
				$res = CUser::GetList($by = "id", $order = "asc", ['ID'=>$USER_ID],['SELECT'=>["UF_FAVS"]])->Fetch();
				$favs = $res["UF_FAVS"];
				$ret['success'] = true;
				$k = empty($favs) ? false : array_search( (int) $id, $favs);
				if ( $k!==FALSE ){
					unset($favs[$k]);
					$ret['active'] = 0;
				} else {
					$ret['active'] = 1;
					$favs[] = (int) $id;
				}
				$user = new CUser;
				$user->Update( $USER_ID, ["UF_FAVS" => $favs]);
			}
			die(json_encode( $ret ));
			break;
			
			default:
			break;
			die;
		}
	}else
		$ret['noAuth']= true;

	$ret['success']= false;
	die( json_encode($ret) );
}