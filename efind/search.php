<?
function ads($str)
{
    return addslashes($str);
}

$_R = $_REQUEST;
ads($_R['from']);
ads($_R['search']);
if ($_R['from'] == "") {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
}
if ($_R['from'] == "efind") {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
}

\Bitrix\Main\Loader::includeModule("iblock");

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

if ($_R['search'] == "") die;
/***************** Производители ******************/
$arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y');
$res = CIBlockElement::GetList(array('NAME' => 'asc'), $arFilter, false, false, array('ID', 'NAME', 'CODE'));
while ($el = $res->GetNext()) {
    $arManufacturers[$el['ID']]['NAME'] = $el['NAME'];
    $arManufacturers[$el['ID']]['CODE'] = $el['CODE'];
}
if ($_R['from'] == "") {
    echo "<textarea rows=10 cols=145 name=text>";
}

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

CModule::IncludeModule('highloadblock');
function GetEntityDataClass($HlBlockId)
{
    if (empty($HlBlockId) || $HlBlockId < 1) {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}

$pn = $_R['search'];
$arFilter = array('IBLOCK_ID' => 4, 'ACTIVE' => "Y", "PROPERTY_3657" => '%' . $pn . '%');
//$arFilter[] = array(
////    "LOGIC" => "OR",
////    "PROPERTY_44" => '%' . $pn . '%',
////    "PROPERTY_3657" => '%' . $pn . '%',
//);
$res = CIBlockElement::GetList(false, $arFilter, false, Array("nPageSize" => 5), array('*', 'PROPERTY_3678', 'PROPERTY_44', 'PROPERTY_ITEMID'));
$rows = $res->SelectedRowsCount();
if ($rows != 0) {
    echo '<data version="2.0">' . "\n";
    while ($el = $res->GetNextElement()) {
        echo "<item>\n";
        //echo "<pre>";print_r($el);	echo "</pre>";
        $arFields = $el->GetFields();
        $arImagesPathDETAIL_PICTURE = str_replace("http:", "https:", CFile::GetPath($arFields["DETAIL_PICTURE"]));

        echo "<part>" . $arFields['PROPERTY_44_VALUE'] . "</part>\n";
        echo "<img>" . $arImagesPathDETAIL_PICTURE . "</img>\n";
        echo "<url>https://www.symmetron.ru" . str_replace("http:", "https:", $arFields["DETAIL_PAGE_URL"]) . "</url>\n";
        $arProps = $el->GetProperties();

        $res1 = CIBlockElement::GetList(false, array('IBLOCK_ID' => 16, 'ID' => $arProps["DOC_AND_FILES"]["VALUE"][0]), false, false, array('PROPERTY_FILE', 'PROPERTY_LINK_TO_RES'));
        while ($el1 = $res1->GetNext()) {
            $arFile = CFile::GetPath($el1["PROPERTY_FILE_VALUE"]);
            echo "<pdf>" . $arFile . "</pdf>\n";
        }
        echo "<mfg>" . $arManufacturers[$arProps["PRODUCERID"]["VALUE"]]['NAME'] . "</mfg>\n";
        CModule::IncludeModule('mcart.templatetext');
        $strkey = \Mcart\TemplateText\Text::GenerateText($arFields["ID"], "{{ключевые характеристики}}");
        $strkey = preg_replace('/<a\s.+?>/', '', $strkey);
        echo "<note>Описание: " . $arFields['PROPERTY_3678_VALUE']['TEXT'] . "</note>\n";
        echo "<moq>" . $arProps["PURCHQUANTITY"]["VALUE"] . "</moq>\n";
        $MY_HL_BLOCK_ID = 5;
        $entity_data_class = GetEntityDataClass($MY_HL_BLOCK_ID);
        $rsData = $entity_data_class::getList(array("select" => array("*"), "order" => array("UF_QTY" => "ASC"), "filter" => array("UF_ITEMID" => $arFields['PROPERTY_ITEMID_VALUE'])));
        while ($el = $rsData->fetch()) {
            echo '<stock>' . $el[UF_QTY] . '</stock>' . "\n";
        }

        echo "<cur>RUR</cur>\n";

        $MY_HL_BLOCK_ID = 6;
        $entity_data_class = GetEntityDataClass($MY_HL_BLOCK_ID);
        $rsData = $entity_data_class::getList(array("select" => array("*"), "order" => array("UF_QUANTITYAMOUNT" => "ASC"), "filter" => array("UF_ITEMID" => $arFields['PROPERTY_ITEMID_VALUE'])));
        while ($el = $rsData->fetch()) {
            echo '<pb qty="' . $el[UF_QUANTITYAMOUNT] . '">' . $el[UF_AMOUNT] . '</pb>' . "\n";
        }
        echo "</item>\n";
    }
    echo "</data>\n";
} else {
    $text = file_get_contents_curl("http://old.symmetron.ru/cgi-bin/search4efind.cgi?q=$pn");
}

if ($_R['from'] == "") {
    ?>
    </textarea>
    <?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
}

if ($_R['from'] == "efind") {

}

?>
