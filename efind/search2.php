<?
$IBLOCK_ID = 4;
$SEARCH_PROP_ID = 3657;
$ARTICLE_PROP_ID = 44;
$DOC_AND_FILE_PROP_ID = 3077;
$PRODUCERID_PROP_ID = 3043;
$PURCHQUANTITY_PROP_ID = 3112;

$DOC_AND_FILE_IBLOCK_ID = 16;
$DOC_AND_FILE_FILE_PROP_ID = 3172;

$PRODUCERID_IBLOCK_ID = 11;

if ($_REQUEST['from'] === 'efind') {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
} else {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
}

if ($_REQUEST['search'] == '') die();

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('highloadblock');

$_REQUEST['from'] = addslashes($_REQUEST['from']);
$_REQUEST['search'] = addslashes($_REQUEST['search']);

if ($_REQUEST['from'] !== 'efind') {
    ?>
    <textarea rows="10" cols="145" name="text">
    <?
}

$entityProp = \Bitrix\Main\Entity\Base::compileEntity(
    'PROPERTY_MCART',
    [
        'IBLOCK_ELEMENT_ID' => [
            'data_type' => 'integer'
        ],
        'IBLOCK_PROPERTY_ID' => [
            'data_type' => 'integer'
        ],
        'VALUE' => [
            'data_type' => 'string'
        ],
    ],
    [
        'table_name' => 'b_iblock_element_property'
    ]
);

$entityProduct = \Bitrix\Main\Entity\Base::compileEntity(
    'PRODUCT_MCART',
    [
        'ID' => [
            'data_type' => 'integer'
        ],
        'QUANTITY' => [
            'data_type' => 'integer'
        ]
    ],
    [
        'table_name' => 'b_catalog_product'
    ]
);

$res = \Bitrix\Iblock\ElementTable::getList([
    'count_total' => 1,
    'select' => [
        'ID',
        'PROPERTY_' . $ARTICLE_PROP_ID . '.VALUE',
        'DETAIL_PICTURE',

        'CODE',
        'IBLOCK_ID',
        'IBLOCK_SECTION_ID',
        'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL',

        'PROPERTY_' . $DOC_AND_FILE_FILE_PROP_ID . '.VALUE',
        'PROPERTY_' . $PRODUCERID_IBLOCK_ID . '.NAME',
        'PROPERTY_' . $SEARCH_PROP_ID . '.VALUE',
        'PROPERTY_' . $PURCHQUANTITY_PROP_ID . '.VALUE',

        'PRODUCT.QUANTITY'
    ],
    'filter' => [
        'IBLOCK_ID' => $IBLOCK_ID,
        'ACTIVE' => 'Y',

        'PROPERTY_' . $ARTICLE_PROP_ID . '.IBLOCK_PROPERTY_ID' => $ARTICLE_PROP_ID,
        'PROPERTY_' . $PURCHQUANTITY_PROP_ID . '.IBLOCK_PROPERTY_ID' => $PURCHQUANTITY_PROP_ID,
        'PROPERTY_' . $DOC_AND_FILE_PROP_ID . '.IBLOCK_PROPERTY_ID' => $DOC_AND_FILE_PROP_ID,

        'PROPERTY_' . $DOC_AND_FILE_FILE_PROP_ID . '.IBLOCK_PROPERTY_ID' => $DOC_AND_FILE_FILE_PROP_ID,
        'PROPERTY_' . $PRODUCERID_PROP_ID . '.IBLOCK_PROPERTY_ID' => $PRODUCERID_PROP_ID,

        'PROPERTY_' . $SEARCH_PROP_ID . '.IBLOCK_PROPERTY_ID' => $SEARCH_PROP_ID,
        [
            "LOGIC" => "OR",
            '%PROPERTY_' . $SEARCH_PROP_ID . '.VALUE' => $_REQUEST['search'],
            '%PROPERTY_' . $ARTICLE_PROP_ID . '.VALUE' => $_REQUEST['search']
        ],
    ],
    'order' => [
        'ID' => 'ASC'
    ],
    'runtime' => [
        'PROPERTY_' . $SEARCH_PROP_ID => [
            'data_type' => $entityProp,
            'reference' => [
                '=this.ID' => 'ref.IBLOCK_ELEMENT_ID',
            ]
        ],

        'PROPERTY_' . $ARTICLE_PROP_ID => [
            'data_type' => $entityProp,
            'reference' => [
                '=this.ID' => 'ref.IBLOCK_ELEMENT_ID',
            ]
        ],

        'PROPERTY_' . $PURCHQUANTITY_PROP_ID => [
            'data_type' => $entityProp,
            'reference' => [
                '=this.ID' => 'ref.IBLOCK_ELEMENT_ID',
            ]
        ],

        'PROPERTY_' . $DOC_AND_FILE_PROP_ID => [
            'data_type' => $entityProp,
            'reference' => [
                '=this.ID' => 'ref.IBLOCK_ELEMENT_ID',
            ],
            'limit' => 1
        ],
        'PROPERTY_' . $DOC_AND_FILE_FILE_PROP_ID => [
            'data_type' => $entityProp,
            'reference' => [
                '=this.' . 'PROPERTY_' . $DOC_AND_FILE_PROP_ID . '.VALUE' => 'ref.IBLOCK_ELEMENT_ID',
            ]
        ],

        'PROPERTY_' . $PRODUCERID_PROP_ID => [
            'data_type' => $entityProp,
            'reference' => [
                '=this.ID' => 'ref.IBLOCK_ELEMENT_ID',
            ],
            'limit' => 1
        ],
        'PROPERTY_' . $PRODUCERID_IBLOCK_ID => new \Bitrix\Main\Entity\ReferenceField(
            'PROPERTY_' . $PRODUCERID_IBLOCK_ID,
            \Bitrix\Iblock\ElementTable::class,
            [
                '=this.' . 'PROPERTY_' . $PRODUCERID_PROP_ID . '.VALUE' => 'ref.ID',
            ]
        ),

        'PRODUCT' => [
            'data_type' => $entityProduct,
            'reference' => [
                '=this.ID' => 'ref.ID',
            ]
        ]
    ]
]);

if ($res->getCount() > 0) {
    ?>
    <data version="2.0">
    <?

    $result = [];
    while ($row = $res->fetch()) {
        $result[$row['ID']] = [
            'part' => $row['IBLOCK_ELEMENT_PROPERTY_' . $ARTICLE_PROP_ID . "_VALUE"],
            'img' => CFile::GetPath($row['DETAIL_PICTURE']),
            'url' => "https://www.symmetron.ru" . CIBlock::ReplaceDetailUrl($row['DETAIL_PAGE_URL'], $row, false, 'E'),
            'pdf' => CFile::GetPath($row['IBLOCK_ELEMENT_PROPERTY_' . $DOC_AND_FILE_FILE_PROP_ID . '_VALUE']),
            'mfg' => $row['IBLOCK_ELEMENT_PROPERTY_' . $PRODUCERID_IBLOCK_ID . '_NAME'],
            'note' => unserialize($row['IBLOCK_ELEMENT_PROPERTY_' . $SEARCH_PROP_ID . '_VALUE'])['TEXT'],
            'moq' => $row['IBLOCK_ELEMENT_PROPERTY_' . $PURCHQUANTITY_PROP_ID . '_VALUE'],
            'stock' => $row['IBLOCK_ELEMENT_PRODUCT_QUANTITY'],
            'pb' => []
        ];
    }

    $res = \Bitrix\Catalog\PriceTable::getList([
        'select' => [
            'PRODUCT_ID',
            'PRICE',
            'QUANTITY_FROM'
        ],
        'filter' => \Bitrix\Main\Entity\Query::filter()
            ->whereIn('PRODUCT_ID', array_keys($result))
    ]);

    while ($row = $res->fetch()) {
        $result[$row['PRODUCT_ID']]['pb'][$row['QUANTITY_FROM']] = $row['PRICE'];
    }

    foreach ($result as $row) {
        ?>
        <item>
            <part><?= $row['part'] ?></part>
            <img><?= $row['img'] ?></img>
            <url><?= $row['url'] ?></url>
            <pdf><?= $row['pdf'] ?></pdf>
            <mfg><?= $row['mfg'] ?></mfg>
            <note><?= $row['note'] ?></note>
            <moq><?= $row['moq'] ?></moq>
            <stock><?= $row['stock'] ?></stock>
            <cur>RUR</cur>
            <?
            foreach ($row['pb'] as $key => $price) {
                ?>
                <pb qty="<?= $key ?>"><?= $price ?></pb>
                <?
            }
            ?>
        </item>
        <?
    }
    ?>
    </data>
    <?
} else {
    $text = file_get_contents_curl("http://old.symmetron.ru/cgi-bin/search4efind.cgi?q=" . $_REQUEST['search']);
}

if ($_REQUEST['from'] !== "efind") {
    ?>
    </textarea>
    <?
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
}

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
