<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'js' => '/local/js/mcart/symmetrondeclination/dist/SymmetronDeclination.bundle.js',
	'rel' => [
        'main.polyfill.core',
		'main.core',
	],
	'skip_core' => false,
];