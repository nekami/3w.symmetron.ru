// @flow

import "main.polyfill.core";
import {Tag} from 'main.core';

export class SymmetronDeclination
{
	name: string;

	constructor(wordsDict: object = {})
	{
		this.arDeclensions = wordsDict;
	}

    declinationIn(word: string): string
	{
        let arWord = this.arDeclensions[word];
        if(!arWord)
            return word;

		return arWord["UF_WORD_DEC_IN"];
	}

    declinationNum(word: string, num: int): string
	{
        let arWord = this.arDeclensions[word];
        if(!arWord)
            return word;

        return this.declension(num, [arWord["UF_WORD_DEC_0"], arWord["UF_WORD_DEC_2"], arWord["UF_WORD_DEC_1"]]);
	}

    declension(n: int, f: object): string {
        return f[8>>(828>>((30>>(!(n%100-10>>3)<<3)&1<<n%10)>>1)&3)&3];
    }
}