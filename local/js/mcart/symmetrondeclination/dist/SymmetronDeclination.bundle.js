this.BX = this.BX || {};
(function (exports,main_polyfill_core,main_core) {
    'use strict';

    var SymmetronDeclination =
    /*#__PURE__*/
    function () {
      function SymmetronDeclination() {
        var wordsDict = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        babelHelpers.classCallCheck(this, SymmetronDeclination);
        this.arDeclensions = wordsDict;
      }

      babelHelpers.createClass(SymmetronDeclination, [{
        key: "declinationIn",
        value: function declinationIn(word) {
          var arWord = this.arDeclensions[word];
          if (!arWord) return word;
          return arWord["UF_WORD_DEC_IN"];
        }
      }, {
        key: "declinationNum",
        value: function declinationNum(word, num) {
          var arWord = this.arDeclensions[word];
          if (!arWord) return word;
          return this.declension(num, [arWord["UF_WORD_DEC_0"], arWord["UF_WORD_DEC_2"], arWord["UF_WORD_DEC_1"]]);
        }
      }, {
        key: "declension",
        value: function declension(n, f) {
          return f[8 >> (828 >> ((30 >> (!(n % 100 - 10 >> 3) << 3) & 1 << n % 10) >> 1) & 3) & 3];
        }
      }]);
      return SymmetronDeclination;
    }();

    exports.SymmetronDeclination = SymmetronDeclination;

}((this.BX.Mcart = this.BX.Mcart || {}),BX,BX));
//# sourceMappingURL=SymmetronDeclination.bundle.js.map
