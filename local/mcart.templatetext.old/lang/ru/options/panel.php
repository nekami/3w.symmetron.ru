<?
global $MODULE_PREFIX;
require(dirname(__FILE__)."/../../../const.php");
$MESS[$MODULE_PREFIX.""] = "";

/* Main tab */
$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS"] = "Основные";
$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS_TITLE"] = "Основные настройки";

$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS_REQUIREMENTS_NOTE"] = "Необходимые для работы зависимости";

$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS_IBLOCK"] = "Инфоблок и свойство";
$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS_HTML"] = "Текст по умолчанию";

$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS_PATH_AND_NAME"] = "Сопоставление пути к наименованию";
$MESS[$MODULE_PREFIX."O_MAIN_SETTINGS_POSSIBLE_PATH"] = "Возможные пути";
?>