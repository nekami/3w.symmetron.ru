<?
IncludeModuleLangFile( __FILE__);

if(class_exists("mcart_templatetext"))
	return;

Class mcart_templatetext extends CModule
{
	var $MODULE_ID = "mcart.templatetext";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_GROUP_RIGHTS = "Y";

	
	
	function mcart_templatetext(){
		$arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)){
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage("templatetext_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("templatetext_MODULE_DESCRIPTION");
        
        $this->PARTNER_NAME = GetMessage("MCART_PARTNER_NAME");
        $this->PARTNER_URI  = "http://mcart.ru/";
	}
	
	function DoInstall(){
        global $APPLICATION;

		if (!IsModuleInstalled("mcart.templatetext"))
		{
			$this->InstallDB();
			$this->InstallFiles();
			
		}
		return true;
	}

	function DoUninstall(){
		$this->UnInstallDB();
		$this->UnInstallFiles();
		return true;
	}

	
	function InstallDB(){
		RegisterModule("mcart.templatetext");
        RegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", "mcart.templatetext", "\Mcart\TemplateText\Text", "OnProductAdd");
		return true;
	}

	function UnInstallDB(){
        UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", "mcart.templatetext", "\Mcart\TemplateText\Text", "OnProductAdd");
		UnRegisterModule("mcart.templatetext");
		return true;
	}

    function InstallFiles(){
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.templatetext/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true);
        return true;
    }

    function UnInstallFiles(){
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.templatetext/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/");
        return true;
    }
}
?>