<?php

namespace Mcart\TemplateText;

class Text
{
    static $filter = '/\[([\w!=<>\'"]*)\]/mu';
    static $filterSV = '/["\']([\w\s[:punct:]]*?)["\']/um';
    static $superFilter = "/(\w*)\/\[([\w!=<>'\"]*)\]\[([\w\/]*)\]*/mu";
    static $firstSuperFilter = "/(\w*)\/\[([\w!=<>'\"]*)\]/mu";
    static $notSuperFilter = "/(\[(\w*\/\w*)\])/mu";

    public static function GenerateText($idel, $text = '')
    {
        $arResult = [];

        $arSelect = Array();
        $arFilter = Array("ID" => IntVal($idel), "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);

        if ($ob = $res->GetNextElement()) {
            $arResult = $ob->GetFields();
            $arResult["PROPERTIES"] = $ob->GetProperties();

            $prop = \CIBlockSectionPropertyLink::GetArray($arResult["IBLOCK_ID"], $SECTION_ID = $arResult["IBLOCK_SECTION_ID"], $bNewSection = false);

            foreach ($arResult["PROPERTIES"] as $key => $value) {
                $additionProp = array_diff_assoc($prop[$value["ID"]], $value);
                $arResult["PROPERTIES"][$key] = array_merge($value, $additionProp);
            }
        }

        $res = \CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
        if ($ar_res = $res->GetNext())
            $arResult["CATEGORY_PATH"] = $ar_res['NAME'];

        $res = \CCatalogProduct::GetByID($arResult["ID"]);
        foreach ($res as $key => $value)
            $arResult["PRODUCT"][$key] = $value;

        // del comments
        $re = '/\(\(([\w\s[:punct:]]*?)\)\)/um';

        if (preg_match_all($re, $text, $matches, PREG_SET_ORDER, 0)) {
            foreach ($matches as $value) {
                $text = str_replace($value[0], "", $text);
            }
        }

        $arr = unserialize(static::GetOption("PATH_AND_NAME"));

        // replace if
        $re = '/(\[\[([\w\s[:punct:]]*?)\]\])\s*([{\[]{2}([\w\s[:punct:]]*?)[}\]]{2})\s*\/\s*([{\[]{2}([\w\s[:punct:]]*?)[}\]]{2})/um';
        if (preg_match_all($re, $text, $matches, PREG_SET_ORDER, 0)) {
            foreach ($matches as $value) {
                if (count($value) == 7 && preg_match_all(static::$filterSV, $value[2], $matches2, PREG_SET_ORDER, 0)) {
                    $searchVal = $matches2[0][1];
                    $value[2] = mb_strtoupper(trim(preg_replace('/\s+/', '', $value[2])));
                    $del = false;

                    $delimiter = array("!=", "<=", ">=", "=", "<", ">");
                    foreach ($delimiter as $val) {
                        $filter = explode($val, $value[2]);
                        if (count($filter) == 2) {
                            $del = $val;
                            break;
                        }
                    }

                    if ($value[3][0] == "{" && !is_array($val = static::GetValue($arResult, $arr[mb_strtoupper(trim(preg_replace('/\s+/', ' ', $value[4])))]["PATH"]))) {
                        $valTrue = $val;
                    } elseif ($value[3][0] == "[") {
                        $valTrue = $value[4];
                    } else {
                        $valTrue = "";
                    }

                    if ($value[5][0] == "{" && !is_array($val = static::GetValue($arResult, $arr[mb_strtoupper(trim(preg_replace('/\s+/', ' ', $value[6])))]["PATH"]))) {
                        $valFalse = $val;
                    } elseif ($value[5][0] == "[") {
                        $valFalse = $value[6];
                    } else {
                        $valFalse = "";
                    }

                    if ($del && $arr[$filter[0]] && !is_array($val = static::GetValue($arResult, $arr[$filter[0]]["PATH"]))) {
                        if (($val == $searchVal && $del == "=") || ($val != $searchVal && $del == "!=") || ($val > $searchVal && $del == ">") || ($val >= $searchVal && $del == ">=") || ($val < $searchVal && $del == "<") || ($val <= $searchVal && $del == "<=")) {
                            $text = str_replace($value[0], $valTrue, $text);
                        } else {
                            $text = str_replace($value[0], $valFalse, $text);
                        }

                    } else {
                        $text = str_replace($value[0], "", $text);
                    }

                } else {
                    $text = str_replace($value[0], "", $text);
                }
            }
        }


        // replace variable
        $re = '/{{([\w,\s[:punct:]]*?)}}/um';

        if (preg_match_all($re, $text, $matches, PREG_SET_ORDER, 0)) {
            foreach ($matches as $value) {
                $path = $arr[mb_strtoupper(trim(preg_replace('/\s+/', ' ', $value[1])))]["PATH"];
                if ($path) {
                    $text = str_replace($value[0], static::GetValue($arResult, $path), $text);

                } else {
                    $text = str_replace($value[0], "", $text);
                }
            }
        }

        return $text;
    }

    public static function array_msort($array, $cols)
    {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;

    }

    public static function GetValue($arResult, $path, $returnArr = false, $skipArr = [])
    {
        if (preg_match_all(static::$superFilter, $path, $matches1, PREG_SET_ORDER, 0)) {
            $arFilterSection = ["ID" => $arResult["IBLOCK_SECTION_ID"], "ACTIVE" => "Y"];

            $sectionsList = [];
            $rsSectionInfo = \CIBlockSection::GetList([], $arFilterSection, false, ["ID", "LEFT_MARGIN", "IBLOCK_ID", "RIGHT_MARGIN"]);
            if ($arSectionInfo = $rsSectionInfo->Fetch()) {
                $sectionsList[] = $arSectionInfo["ID"];
                $sectionsList = \Mcart\SortCatalogSM\Helper::GetCatalogParentSectionsId(array(
                    "IBLOCK_ID" => $arSectionInfo["IBLOCK_ID"],
                    '<LEFT_MARGIN' => $arSectionInfo["LEFT_MARGIN"],
                    '>RIGHT_MARGIN' => $arSectionInfo["RIGHT_MARGIN"],
                ), $sectionsList);
            }

            $iblock_id = \COption::GetOptionString("mcart.sortcatalogsm", "SORTCATALOGSM_REG_IBLOCK_ID");

            $arSelectPropsNoSort = [];
            $arPropsCodeID = \Mcart\SortCatalogSM\Helper::GetPropertyID($iblock_id);

            if ($arPropsCodeID["REG_CATALOG_SECTION"]) {
                $propertyId = $arPropsCodeID["REG_CATALOG_SECTION"];
                $propertyIdSection = $arPropsCodeID["CATALOG_SECTION"];
                $propRegElr = \CIBlockElement::GetPropertyValues(
                    $iblock_id,
                    array('ACTIVE' => 'Y', "PROPERTY_CATALOG_SECTION" => $sectionsList),
                    true, array("ID" => [$propertyId, $propertyIdSection])
                );

                $arAllReg = array();
                while ($propRegEl = $propRegElr->Fetch()) {
                    $arAllReg[] = $propRegEl;
                }

                foreach ($sectionsList as $id) {
                    foreach ($arAllReg as $propRegEl) {
                        if (in_array($id, $propRegEl[$propertyIdSection])) {
                            $arSelectPropsNoSort = array_merge($arSelectPropsNoSort, array_map(
                                \Mcart\SortCatalogSM\Helper::class . "::GetArrayPair",
                                $propRegEl[$propertyId]
                            ));
                            break;
                        }
                    }
                }
            }

            foreach ($arSelectPropsNoSort as $key => $value) {
                if (IntVal($value["SORT"]) < 100 || IntVal($value["SORT"]) > 499) {
                    unset($arSelectPropsNoSort[$key]);
                }
            }

            if (!empty($arSelectPropsNoSort)) {
                $arSelectPropsSort = array_column($arSelectPropsNoSort, "VALUE");

                $property = array_fill_keys($arSelectPropsSort, "");
                $result = \Bitrix\Iblock\PropertyTable::GetList(array(
                    "select" => array("ID", "CODE"),
                    "filter" => array("ID" => $arSelectPropsSort),
                ));
                while ($prop = $result->fetch())
                    $property[$prop["ID"]] = $prop["CODE"];

                $arSelectPropsSort = array_filter($property, function ($elem) {
                    return !empty($elem) ;
                });

                $propertiesOnList = array_filter($arSelectPropsNoSort, function ($el) {
                    return $el["SHOW"] == "Y";
                });

                $propertiesOnList = self::array_msort($propertiesOnList, array('SORT' => SORT_ASC));
                $propertiesOnList = array_column($propertiesOnList, "VALUE");

                $result = [];

                $skipProp = [];
                if (preg_match_all(static::$notSuperFilter, $path, $matches3, PREG_SET_ORDER, 0)) {
                    foreach ($matches3 as $value) {
                        $skipProp[] = strtolower(explode('/', $value[2])[1]);
                    }
                }

                foreach ($propertiesOnList as $key => $value) {
                    if (!key_exists($value, $arSelectPropsSort)) {
                        continue;
                    }

                    if(array_search(strtolower($arSelectPropsSort[$value]), $skipProp) !== false){
                        continue;
                    }

                    $v = static::GetValue($arResult, "PROPERTIES/" . $arSelectPropsSort[$value]);

                    if (empty($v)) {
                        continue;
                    } elseif (preg_match('/^\d+\.\d+$/m', $v, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        $result[$arSelectPropsSort[$value]] = (float)$v;
                    } else {
                        $result[$arSelectPropsSort[$value]] = $v;
                    }
                }

                if ($returnArr === false && is_array($result)) {
                    return implode(", ", $result);
                } elseif ($returnArr === true && !is_array($result)) {
                    return [$result];
                }

                return $result;
            } else {
                if (preg_match_all(static::$firstSuperFilter, $path, $matches2, PREG_SET_ORDER, 0)) {
                    $skipProp = [];
                    if (preg_match_all(static::$notSuperFilter, $path, $matches3, PREG_SET_ORDER, 0)) {
                        foreach ($matches3 as $value) {
                            $skipProp[] = strtolower(explode('/', $value[2])[1]);
                        }
                    }

                    $result = static::GetValue($arResult, $matches2[0][0], true, $skipProp);

                    if ($returnArr === false && is_array($result)) {
                        foreach ($result as &$value) {
                            if (preg_match('/^\d+\.\d+$/m', $value, $matches, PREG_OFFSET_CAPTURE, 0)) {
                                $value = (float)$value;
                            }
                        }

                        return implode(", ", $result);
                    } elseif ($returnArr === true && !is_array($result)) {
                        return [$result];
                    }

                    if (preg_match('/^\d+\.\d+$/m', $result, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        $result = (float)$result;
                    }

                    return $result;
                };
            }
        } else {
            $arrPath = explode('/', $path);

            $result = $arResult;

            foreach ($arrPath as $p) {
                if (preg_match_all(static::$filter, $p, $matches2, PREG_SET_ORDER, 0)) {
                    $delimiter = array("!=", "<=", ">=", "=", "<", ">");
                    foreach ($delimiter as $val) {
                        $filter = explode($val, $matches2[0][1]);
                        if (count($filter) == 2) {
                            $del = $val;
                            break;
                        }
                    }

                    if (count($filter) == 2) {
                        $pre_filter = array_column($result, $filter[0], "CODE");

                        preg_match_all(static::$filterSV, $matches2[0][1], $matches, PREG_SET_ORDER, 0);
                        $searchVal = $matches[0][1];

                        $newResult = [];

                        foreach ($pre_filter as $key => $val) {
                            if(array_search(strtolower($key), $skipArr) !== false){
                                continue;
                            }

                            if (array_key_exists("VALUE", $result[$key]) && (($val == $searchVal && $del == "=") || ($val != $searchVal && $del == "!=") || ($val > $searchVal && $del == ">") || ($val >= $searchVal && $del == ">=") || ($val < $searchVal && $del == "<") || ($val <= $searchVal && $del == "<="))) {
                                if (is_array($result[$key]["VALUE"])) {
                                    if ($result[$key]["USER_TYPE"] == 'mcart_property_with_measure_units') {
                                        if (!empty(trim($result[$key]["VALUE"]['VALUE']))) {
                                            $vvv = $result[$key]["VALUE"]['VALUE'];

                                            $arFilter = Array("IBLOCK_ID" => $result[$key]["LINK_IBLOCK_ID"], "ID" => $result[$key]["VALUE"]['ELEMENT_ID']);
                                            $resList = \CIBlockElement::GetList(array(), $arFilter);
                                            if ($obList = $resList->GetNext()) {
                                                $vvv .= $obList["NAME"];
                                            }
                                            $newResult[] = $vvv;
                                        }
                                    } elseif ($result[$key]["LINK_IBLOCK_ID"] > 0 && !empty($result[$key]["VALUE"])) {
                                        $arr = array();
                                        $arFilter = Array("IBLOCK_ID" => $result[$key]["LINK_IBLOCK_ID"], "ID" => $result[$key]["VALUE"], "ACTIVE" => "Y");
                                        $resList = \CIBlockElement::GetList(array(), $arFilter);
                                        while ($obList = $resList->GetNext()) {
                                            $arr[] = $obList["NAME"];
                                        }
                                        $newResult = array_merge($newResult, $arr);
                                    } elseif ($result[$key]["USER_TYPE"] == "HTML") {
                                        $newResult[] = $result[$key]["VALUE"]["TEXT"];
                                    } else {
                                        $newResult = array_merge($newResult, $result[$key]["VALUE"]);
                                    }
                                } elseif ($result[$key]["VALUE"] != 'NaN') {
                                    if ($result[$key]["LINK_IBLOCK_ID"] > 0 && !empty($result[$key]["VALUE"])) {
                                        $arr = array();
                                        $arFilter = Array("IBLOCK_ID" => $result[$key]["LINK_IBLOCK_ID"], "ID" => $result[$key]["VALUE"], "ACTIVE" => "Y");
                                        $resList = \CIBlockElement::GetList(array(), $arFilter);
                                        while ($obList = $resList->GetNext()) {
                                            $arr[] = $obList["NAME"];
                                        }
                                        $newResult = array_merge($newResult, $arr);
                                    } else {
                                        $newResult[] = $result[$key]["VALUE"];
                                    }
                                }
                            }
                        }

                        $result = array_diff($newResult, array(''));

                        break;
                    } else {
                        $result = '';

                        break;
                    }
                } else {
                    $result = $result[$p];
                }

                if (array_key_exists("VALUE", $result)) {
                    if (is_array($result["VALUE"])) {
                        if ($result["USER_TYPE"] == 'mcart_property_with_measure_units' && !empty(trim($result["VALUE"]['VALUE']))) {
                            if (!empty(trim($result["VALUE"]['VALUE']))) {
                                $newResult = $result["VALUE"]['VALUE'];

                                $arFilter = Array("IBLOCK_ID" => $result["LINK_IBLOCK_ID"], "ID" => $result["VALUE"]['ELEMENT_ID']);
                                $resList = \CIBlockElement::GetList(array(), $arFilter);
                                if ($obList = $resList->GetNext()) {
                                    $newResult .= $obList["NAME"];
                                }

                                $result = $newResult;
                            }
                        } elseif ($result["LINK_IBLOCK_ID"] > 0 && !empty($result["VALUE"])) {
                            $arr = array();
                            $arFilter = Array("IBLOCK_ID" => $result["LINK_IBLOCK_ID"], "ID" => $result["VALUE"], "ACTIVE" => "Y");
                            $resList = \CIBlockElement::GetList(array(), $arFilter);
                            while ($obList = $resList->GetNext()) {
                                $arr[] = $obList["NAME"];
                            }
                            $result = $arr;
                        } elseif ($result["USER_TYPE"] == "HTML") {
                            $result = $result["VALUE"]["TEXT"];
                        } else {
                            $result = $result["VALUE"];
                        }
                    } else {
                        if ($result["LINK_IBLOCK_ID"] > 0 && !empty($result["VALUE"])) {
                            $arr = array();
                            $arFilter = Array("IBLOCK_ID" => $result["LINK_IBLOCK_ID"], "ID" => $result["VALUE"], "ACTIVE" => "Y");
                            $resList = \CIBlockElement::GetList(array(), $arFilter);
                            while ($obList = $resList->GetNext()) {
                                $arr[] = $obList["NAME"];
                            }
                            $result = $arr;
                        } else {
                            $result = $result["VALUE"];
                        }
                    }

                    break;
                }
            }

            if ($returnArr === false && is_array($result)) {
                foreach ($result as &$value) {
                    if (preg_match('/^\d+\.\d+$/m', $value, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        $value = (float)$value;
                    }
                }

                return implode(", ", $result);
            } elseif ($returnArr === true && !is_array($result)) {
                return [$result];
            }

            if (preg_match('/^\d+\.\d+$/m', $result, $matches, PREG_OFFSET_CAPTURE, 0)) {
                $result = (float)$result;
            }

            return $result;
        }
    }

    public static function GetOption($code, $default = "")
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;
        require(dirname(__FILE__) . "/../const.php");
        $option = \Bitrix\Main\Config\Option::get($MODULE_ID, $MODULE_PREFIX . $code, $default);
        if ($option == "")
            return false;
        return $option;
    }

    public static function OutputNoteOfArResult()
    {
        $id = unserialize(static::GetOption("IBLOCK"));

        $result = '';

        $arSelect = Array();
        $arFilter = Array("IBLOCK_ID" => IntVal($id[0]), "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);

        if ($ob = $res->GetNextElement()) {
            $arrEl = $ob->GetFields();
            $arrEl["PROPERTIES"] = $ob->GetProperties();

        }

        foreach ($arrEl as $key => $value) {
            if ($key == "PROPERTIES" || $key[0] == "~")
                continue;
            $result .= $key . "\n";
        }

        foreach ($arrEl["PROPERTIES"] as $key => $value) {
            $result .= "PROPERTIES/" . $key . "\n";
        }

        $result .= "CATEGORY_PATH\n";

        $res = \CCatalogProduct::GetByID($arrEl['ID']);
        foreach ($res as $key => $value)
            $result .= "PRODUCT/" . $key . "\n";

        return $result;
    }

    function OnProductAdd(&$arFields)
    {
        $id = unserialize(static::GetOption("IBLOCK"));
        if ($arFields["IBLOCK_ID"] == $id[0]) {
            $res = \CIBlockProperty::GetByID($id[1], $id[0]);
            $ar_res = $res->GetNext();
            $text = \Bitrix\Main\Config\Option::get("mcart.templatetext", "MCART_TEMPLATE_TEXTHTML", false);
            $arFields["PROPERTY_VALUES"][$ar_res["ID"]] = $text;
        }
    }
}


//\Bitrix\Main\Loader::includeModule("iblock");
//\Bitrix\Main\Loader::includeModule("mcart.sortcatalogsm");
//
//$arFilterSection = ["ID" => 156, "ACTIVE" => "Y"];
//
//$rsSectionInfo = \CIBlockSection::GetList([], $arFilterSection, false, ["ID", "LEFT_MARGIN", "IBLOCK_ID", "RIGHT_MARGIN"]);
//if ($arSectionInfo = $rsSectionInfo->Fetch()) {
//    print_r($arSectionInfo);
//    $sectionsList[] = $arSectionInfo["ID"];
//    $sectionsList = \Mcart\SortCatalogSM\Helper::GetCatalogParentSectionsId(array(
//        "IBLOCK_ID" => $arSectionInfo["IBLOCK_ID"],
//        '<LEFT_MARGIN' => $arSectionInfo["LEFT_MARGIN"],
//        '>RIGHT_MARGIN' => $arSectionInfo["RIGHT_MARGIN"],
//    ), $sectionsList);
//
//    print_r($sectionsList);
//
//    $arSelectPropsAll = \Mcart\SortCatalogSM\Helper::GetDefaultProperties($sectionsList);
//    $arSelectProps = $arSelectPropsAll["PROPERTY_ON_LIST"];
//}
//
//var_dump($arSelectProps);