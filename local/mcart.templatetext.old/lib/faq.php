<?php
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

if(IntVal($_REQUEST['id']) > 0 && $_REQUEST['prop']=='t') {
    \Bitrix\Main\Loader::includeModule("iblock");

	$arr = [];

	$arSelect = Array();
   	$arFilter = Array("IBLOCK_ID" => IntVal($_REQUEST['id']), "ACTIVE" => "Y");
   	$res = \CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);

   	if ($ob = $res->GetNextElement()) {
		foreach($ob->GetProperties() as $key => $value){
			$arr[$key] = $value["NAME"];
		}

   	}

   print_r(json_encode($arr));
}elseif(IntVal($_REQUEST['id']) > 0) {
   \Bitrix\Main\Loader::includeModule("iblock");
   \Bitrix\Main\Loader::includeModule("catalog");

    $result = '';

    $arSelect = Array();
    $arFilter = Array("IBLOCK_ID" => IntVal($_REQUEST['id']), "ACTIVE" => "Y");
    $res = \CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);

    if ($ob = $res->GetNextElement()) {
        $arrEl = $ob->GetFields();
        $arrEl["PROPERTIES"] = $ob->GetProperties();
    }

    foreach($arrEl as $key => $value){
        if($key == "PROPERTIES" || $key[0] == "~")
            continue;
        $result .= $key."\n";
    }

    foreach($arrEl["PROPERTIES"] as $key => $value){
        $result .= "PROPERTIES/".$key."\n";
    }

    $result .= "CATEGORY_PATH\n";

    $res = \CCatalogProduct::GetByID($arrEl['ID']);
    foreach ($res as $key => $value)
        $result .= "PRODUCT/" . $key . "\n";

    echo $result;
}elseif($_REQUEST['prop']=='f'){
   \Bitrix\Main\Loader::includeModule("fileman");

   $text = \Bitrix\Main\Config\Option::get("mcart.templatetext", "MCART_TEMPLATE_TEXTHTML", false);

   \CFileMan::AddHTMLEditorFrame(
       "HTML",
       $text,
       "PREVIEW_TEXT_TYPE",
       "html",
       array(
           'height' => '450',
           'width' => '100%'
       ),
       "N",
       0,
       "",
       "",
       false,
       true,
       false,
       array()
   );
}elseif($_REQUEST['progress']>=0){
   $arr = [];
   $startTime = time();
   $arr["count"] = IntVal($_REQUEST['progress']);

   \Bitrix\Main\Loader::includeModule("iblock");
   $id = unserialize(\Bitrix\Main\Config\Option::get("mcart.templatetext", "MCART_TEMPLATE_TEXTIBLOCK", false));

   if($_REQUEST['progress'] == 0){
       $cnt = \CIBlockElement::GetList(
           array(),
           array('IBLOCK_ID' => $id[0]),
           array(),
           false,
           array('ID', 'NAME')
       );

       $arr["max"] = $cnt;
   }

   $text = \Bitrix\Main\Config\Option::get("mcart.templatetext", "MCART_TEMPLATE_TEXTHTML", false);

   $rs = \CIBlockElement::GetList (
       Array("ID" => "ASC"),
       Array("IBLOCK_ID" => $id[0], ">ID" => IntVal($_REQUEST['elem'])),
       false,
       Array ()
   );

   while($arParent = $rs->Fetch())
   {

       CIBlockElement::SetPropertyValues($arParent["ID"], $id[0], $text, $id[1]);

       $arr["count"]++;
       $arr["elem"] = $arParent["ID"];

       if((time() - $startTime) > 30)
           break;
   }

   echo json_encode($arr);
}