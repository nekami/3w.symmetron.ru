<?
namespace Mcart;
use \Bitrix\Main\Localization\Loc;

global $MODULE_PREFIX;
global $MCART_MESS;
require(dirname(__FILE__)."/const.php");
include(dirname(__FILE__)."/mess.php");

if (!function_exists("\Mcart\GetMessage")) {
    function GetMessage($code, $replace = null, $language = null)
    {
        global $MODULE_PREFIX;
        global $MCART_MESS;

        if (isset($MCART_MESS[$MODULE_PREFIX . $code]))
            return $MCART_MESS[$MODULE_PREFIX . $code];
        $text = Loc::GetMessage($MODULE_PREFIX . $code, $replace, $language);
        if ($text != "")
            return $text;
        return Loc::GetMessage($MODULE_PREFIX . $code, $replace, "ru");
    }
}
?>