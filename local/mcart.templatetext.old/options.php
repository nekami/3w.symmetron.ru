<?if (!$USER->IsAdmin()) return;?>
<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_NAME;
global $MODULE_PREFIX;
global $PARTNER_CODE;

require_once(dirname(__FILE__)."/lang.php");
require(dirname(__FILE__)."/const.php");

/* On module classes */
\Bitrix\Main\Loader::includeModule($MODULE_ID);
$classFiles = array();
require_once(dirname(__FILE__)."/options/loadclasses.php");

/* Init tabs */
$aTabs = array();
require(dirname(__FILE__)."/options/panel.php");
$funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options::InitTabs";
if ($funcName($aTabs) === false)
    return;

/* Init options content */
$funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options::InitContent";
if ($funcName($aTabs, $classFiles) === false)
    return;

/* Get request */
global $APPLICATION;
$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();
$funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options::Request";
$funcName($aTabs, $APPLICATION->GetCurPage(), $request);

/* Load current value */
$funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options::LoadValues";
$funcName($aTabs, $request);

/* Get HTML code of options page and display it */
?><form method="POST" action="<?=$APPLICATION->GetCurPage()?>?lang=<?=LANGUAGE_ID?>&mid=<?=$MODULE_ID?>" name="<?=$MODULE_PREFIX?>_sform" id="<?=$MODULE_PREFIX?>_sform"><?
    echo bitrix_sessid_post();
    $aTabs["CONTROL"]->Begin();
    $funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options::GetHTML";
    echo $funcName($aTabs);
    $aTabs["CONTROL"]->Buttons(array("back_url" => $request["back_url"]));
    $aTabs["CONTROL"]->End();
?></form>

