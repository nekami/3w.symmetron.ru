<?
$arClasses = array(
    "\\Mcart\\TemplateText\\Text" => "lib/text.php",
);

\Bitrix\Main\Loader::registerAutoLoadClasses("mcart.templatetext", $arClasses);

\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule("fileman");
\Bitrix\Main\Loader::includeModule("mcart.sortcatalogsm");
CUtil::InitJSCore(array('window', 'ajax', 'fx', 'popup'));