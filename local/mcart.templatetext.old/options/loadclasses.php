<?
global $MODULE_ID;
global $MODULE_NAME;
global $PARTNER_CODE;
require(dirname(__FILE__)."/../const.php");

/* Get module position */
if (stripos(dirname(__FILE__), "/local/") === false)
    $moduleFolder = "/bitrix/modules/".$MODULE_ID."/";
else
    $moduleFolder = "/local/modules/".$MODULE_ID."/";


$classFiles = array();
/* Get installation classes */
$classFolder = dirname(__FILE__)."/classes/";
$files = scandir($classFolder);
foreach ($files as $filename)
{
    if($filename == "." || $filename == ".." || is_dir($classFolder.$filename))
        continue;
    if (pathinfo($filename,  PATHINFO_EXTENSION) == "php")
        $classFiles["\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options\\".pathinfo($filename, PATHINFO_FILENAME)] =
            $moduleFolder."options/classes/".$filename;
}
$classFiles["\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options"] = $moduleFolder."options/options.php";
\Bitrix\Main\Loader::registerAutoloadClasses(null, $classFiles);
