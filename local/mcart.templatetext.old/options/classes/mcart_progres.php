<?
namespace Mcart\_TemplateText\Options;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_PREFIX;
global $recordCounter;
require_once(dirname(__FILE__)."/../../lang.php");
require(dirname(__FILE__)."/../../const.php");
\CJSCore::Init("jquery");
class mcart_html
{
    public static function Init(&$arContent)
    {
        return true;
    }
    
    public static function Save($arContent, $request)
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;

        if ($request["HTML"])
            $arr = $request["HTML"];

        $option = $arr;
        \Bitrix\Main\Config\Option::set($MODULE_ID, $MODULE_PREFIX.$arContent["CODE"], $option);
    }
    
    public static function CustomAction(&$arContent, $request)
    {
    }
    
    public static function LoadValues(&$arContent, $request)
    {
    }
    
    public static function GetHTML($arContent)
    {

        global $MODULE_PREFIX;

        $result = "";
        $result .= "<tr><td class='adm-detail-content-cell-l' width='50%'>".$arContent["NAME"]."</td><td class='adm-detail-content-cell-r' name='HTML_EDITOR' width='50%'>";
        $result .= "<progress value=\"30\" max=\"100\">30</progress>";
        $result .= "<input type=\"submit\" name=\"apply\" value=\"Применить для всех\">";
        $result .= "</td>";
        $result .= "</tr>";

        $LHE = new \CHTMLEditor;
        $LHE->Show(array("display"=>false));

		return $result;
    }

    public static function GetRedirectParams($arContent, $request, &$arParams)
    {
    }
}
?>
<!--<script>-->
<!--    $(document).ready(function(){-->
<!--        $.ajax({-->
<!--            url: "/bitrix/tools/mcart_ajax_templatetext.php?prop=f",-->
<!--            cache: false,-->
<!--            success: function(text){-->
<!--                $('#mcart_template_textmain_settings_settings_edit_table > tbody > tr:nth-child(3) > td.adm-detail-content-cell-r').html(text);-->
<!--            }-->
<!--        });-->
<!--    });-->
<!--</script>-->
