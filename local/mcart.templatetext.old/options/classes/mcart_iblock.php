<?
namespace Mcart\_TemplateText\Options;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_PREFIX;
global $recordCounter;
require_once(dirname(__FILE__)."/../../lang.php");
require(dirname(__FILE__)."/../../const.php");
\CJSCore::Init("jquery");
class mcart_iblock
{
    public static function Init(&$arContent)
    {
        return true;
    }
    
    public static function Save($arContent, $request)
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;

        $arr = [];

        $arr[] = $request["IBLOCK_TEMPLATE"];
        $arr[] = $request["IBLOCK_PROP"];

        $option = serialize($arr);
        \Bitrix\Main\Config\Option::set($MODULE_ID, $MODULE_PREFIX.$arContent["CODE"], $option);
    }
    
    public static function CustomAction(&$arContent, $request)
    {
    }
    
    public static function LoadValues(&$arContent, $request)
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;

        $arContent["VALUE"] = [];
        $value = unserialize(\Bitrix\Main\Config\Option::get($MODULE_ID, $MODULE_PREFIX.$arContent["CODE"], false));
        $arContent["VALUE"] = $value;
	
    }
    
    public static function GetHTML($arContent)
    {

        global $MODULE_PREFIX;

        $result = "";
        $result .= "<tr><td class='adm-detail-content-cell-l' width='50%'>".$arContent["NAME"]."</td><td class='adm-detail-content-cell-r' width='50%'>";

        $res = \CIBlock::GetList(
            Array(),
            Array(
                'TYPE'=>'catalog',
                'ACTIVE'=>'Y'
            ), true
        );
        while($arRes = $res->Fetch())
        {
            $arIblock[$arRes['ID']] = $arRes['NAME'];
        }

		$select = '<select onchange="changeIblockItem(this)" name="IBLOCK_TEMPLATE">';
		foreach($arIblock as $key=>$value){
		    if($key == $arContent["VALUE"][0]) {
                $select .= '<option selected="selected" value ="' . $key . '">' . $value . '</option>';
            }else{
                $select .= '<option value ="' . $key . '">' . $value . '</option>';
            }
		}
		$select .= '</select>';
		$result .= $select;

        $arSelect = Array("ID", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_ID"=>$arContent["VALUE"][0], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
        if($ob = $res->GetNextElement()){
            $arrEl = $ob->GetProperties();
        }

        $select = '<select name="IBLOCK_PROP">';
        foreach($arrEl as $key=>$value){
            if($key == $arContent["VALUE"][1]) {
                $select .= '<option selected="selected" value ="' . $key . '">' . $value["NAME"] . '</option>';
            }else{
                $select .= '<option value ="' . $key . '">' . $value["NAME"] . '</option>';
            }
        }
        $select .= '</select>';
        $result .= $select;

        $result .= "</td>";
        $result .= "</tr>";
		return $result;
    }

    public static function GetRedirectParams($arContent, $request, &$arParams)
    {
    }
}
?>

<script>
    function changeIblockItem(value) {
        $(document).ready(function(){
            $.ajax({
                url: "/bitrix/tools/mcart_ajax_templatetext.php?id="+value[value.selectedIndex].value,
                cache: false,
                success: function(text){
                    $('#mcart_template_textmain_settings_settings_edit_table > tbody > tr:nth-child(5) > td > div > div > pre').text(text);
                }
            });
        });

        $(document).ready(function(){
            $.ajax({
                url: "/bitrix/tools/mcart_ajax_templatetext.php?prop=t&id="+value[value.selectedIndex].value,
                cache: false,
                success: function(date){
                    select = $('#mcart_template_textmain_settings_settings_edit_table > tbody > tr:nth-child(2) > td.adm-detail-content-cell-r > select:nth-child(2)')[0];

                    arr = JSON.parse(date);

                    select.options.length = 0;

                    $.each(arr,function(index,value){
                        select.options.add(new Option(value, index));
                    });
                }
            });
        });
    }
</script>
