<?
namespace Mcart\_TemplateText\Options;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_PREFIX;
global $recordCounter;
require_once(dirname(__FILE__)."/../../lang.php");
require(dirname(__FILE__)."/../../const.php");
\CJSCore::Init("jquery");
class mcart_progress
{
    public static function Init(&$arContent)
    {
        return true;
    }
    
    public static function Save($arContent, $request)
    {
    }
    
    public static function CustomAction(&$arContent, $request)
    {
    }
    
    public static function LoadValues(&$arContent, $request)
    {
    }
    
    public static function GetHTML($arContent)
    {

        global $MODULE_PREFIX;

        $result = "";
        $result .= "<tr><td class='adm-detail-content-cell-l' width='50%'>".$arContent["NAME"]."</td><td class='adm-detail-content-cell-r' name='HTML_EDITOR' width='50%'>";
        $result .= "<progress value=\"0\" max=\"0\">0</progress>";
        $result .= "<pre class='progress'>0/0</pre>";
        $result .= "<input class='bprogress' type='button' onclick='updateProduct(0, 0)' value='Применить для всех'>";

        $result .= "</td>";
        $result .= "</tr>";

        $LHE = new \CHTMLEditor;
        $LHE->Show(array("display"=>false));

		return $result;
    }

    public static function GetRedirectParams($arContent, $request, &$arParams)
    {
    }
}
?>
<script>
    function updateProduct(count, elem) {
        $(document).ready(function(){
            $.ajax({
                url: "/bitrix/tools/mcart_ajax_templatetext.php?progress="+count+"&elem="+elem,
                cache: false,
                success: function(date){
                    arr = JSON.parse(date);

                    progress = $('#mcart_template_textmain_settings_settings_edit_table > tbody > tr:nth-child(1) > td.adm-detail-content-cell-r > progress')[0];

                    if(arr['max']){
                        progress.max = arr['max'];
                    }

                    progress.text = arr['count'];
                    progress.value = arr['count'];

                    progress.parentNode.getElementsByTagName('pre')[0].innerText = arr['count'] + "/" + progress.max;

                    if(progress.max != arr['count']){
                        updateProduct(arr['count'], arr['elem']);
                    }
                }
            });
        });
    }
</script>
