<?
namespace Mcart\_TemplateText\Options;
use \Bitrix\Main\Localization\Loc;
use Mcart\TemplateText\Text;

Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_PREFIX;
global $recordCounter;
require_once(dirname(__FILE__)."/../../lang.php");
require(dirname(__FILE__)."/../../const.php");
\CJSCore::Init("jquery");
class mcart_text_pare
{
    public static function Init(&$arContent)
    {
        return true;
    }
    
    public static function Save($arContent, $request)
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;

        $option = "";
        	if (count($arContent["OPTIONS"]["NAMES"])>1)
			{
				foreach ($request[$arContent["OPTIONS"]["NAMES"][0]] as $key=>$value)
				{
					foreach ($arContent["OPTIONS"]["NAMES"] as $name)
					    if($name == "PATH"){
                            if(preg_match_all(Text::$filterSV, $request[$name][$key], $matches, PREG_SET_ORDER, 0)){
                                $arr_[$name] = strtoupper(trim(preg_replace('/\s+/', '', $request[$name][$key])));
                                $arr_[$name] = str_replace(strtoupper(trim(preg_replace('/\s+/', '', $matches[0][0]))), $matches[0][0], $arr_[$name]);
                            }else{
                                $arr_[$name] = strtoupper(trim(preg_replace('/\s+/', '', $request[$name][$key])));
                            }
                        }else {
                            $arr_[$name] = strtoupper(trim(preg_replace('/\s+/', ' ', $request[$name][$key])));
                        }
					if (!empty($value))
						$arr[strtoupper(trim(preg_replace('/\s+/', ' ', $value)))] = $arr_;
				}
			}
		else
			{
		foreach ($arContent["OPTIONS"]["NAMES"] as $name)
			if ($request[$name])			
				$arr = $request[$name];
			}

            $option = serialize($arr);
        \Bitrix\Main\Config\Option::set($MODULE_ID, $MODULE_PREFIX.$arContent["CODE"], $option);
    }
    
    public static function CustomAction(&$arContent, $request)
    {
    }
    
    public static function LoadValues(&$arContent, $request)
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;

	
        $value = unserialize(\Bitrix\Main\Config\Option::get($MODULE_ID, $MODULE_PREFIX.$arContent["CODE"], false));
        if ($value !== false)
            $arContent["VALUE"] = $value;
	
    }
    
    public static function GetHTML($arContent)
    {

        global $MODULE_PREFIX;
        $php_array = $arContent["OPTIONS"]["NAMES"];
	$jsStr = "";
	foreach ($php_array as $name)
	{
	if (empty($jsStr))
		$jsStr  = "['".trim($name)."'";
	else
		$jsStr  = $jsStr.",'".trim($name)."'";
	}
	$jsStr = $jsStr."]";
        $result = "";
        $result .= "<tr><td class='adm-detail-content-cell-l' width='50%'>".$arContent["NAME"]."</td><td class='adm-detail-content-cell-r' width='50%'>
       <table id='mcart1cze_addrow_table1".$arContent["CODE"]."'>";
	$result .= "<tr>";
	foreach ($arContent["OPTIONS"]["NAMES"] as $title)
		$result .= "<td>".GetMessage("MCART_".$title)."</td>";
	$result .= "</tr>";
        $i = 1;

        foreach ($arContent["VALUE"] as $key=>$val)
        {

		$result .= "<tr id='delete_row_log_".$i."'>";
		if (is_array($val))
			foreach ($arContent["OPTIONS"]["NAMES"] as $key2)
		        {
				if (isset($val[$key2]))
					$val_ = $val[$key2];
				else
					$val_ = "";
		            $result .= "<td><input type='text' name='".$key2."[".$i."]' value='".$val_."'></td>";
		            
		        }
		else
			{
			if (isset($arContent["OPTIONS"]["NAMES"][0]))
				$result .= "<td><input type='text' name='".$arContent["OPTIONS"]["NAMES"][0]."[".$i."]' value=".$val."></td>";
			}
		$result .= '<td><a href="#" onclick="MCART1CZEDeleteRow('.$i.',\''.$arContent["CODE"].'\'); return false;">X</a></td>';

		$i++;
		$result .= "</tr>";
        }
	
        $result .= "</table>";
        $result.="<a href=\"javascript:void(0);\" onclick=\"MCART1CZUPAddCondition(".$jsStr.",'".$arContent["CODE"]."')\">".GetMessage("MCART_FIELD_ADD")."</a></td></tr>";
        return $result;
    }

    public static function GetRedirectParams($arContent, $request, &$arParams)
    {
    }
}
?>
<script type="text/javascript">
    var b_log_counter = 30;
    function MCART1CZUPAddCondition(arrValues, tablePrefix)
    {

        var addrowTable = document.getElementById('mcart1cze_addrow_table1'+tablePrefix);

        b_log_counter++;
        var newRow = addrowTable.insertRow(-1);

        newRow.id = "delete_row_log_" + b_log_counter;

        for (var i=0; i<=arrValues.length-1; i++) {

            var name = arrValues[i];
            var newCell = newRow.insertCell(-1);
            newCell.innerHTML = '<input type="text" name="' + name + '[' + b_log_counter +']">';
        }




        var newCell = newRow.insertCell(-1);

        newCell.innerHTML = '<a href="#" onclick="MCART1CZEDeleteRow(' + b_log_counter + ', \'' + tablePrefix + '\'); return false;">X</a>';


    }
    function MCART1CZEDeleteRow(ind, tablePrefix)
    {
        var addrowTable = document.getElementById('mcart1cze_addrow_table1'+tablePrefix);

        var cnt = addrowTable.rows.length;
        for (i = 0; i < cnt; i++)
        {
            if (addrowTable.rows[i].id != 'delete_row_log_' + ind)
                continue;

            addrowTable.deleteRow(i);

            break;
        }

    }
</script>

