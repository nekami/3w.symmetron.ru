<?
namespace Mcart\_TemplateText;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_NAME;
global $MODULE_PREFIX;
global $PARTNER_CODE;

require_once(dirname(__FILE__)."/../lang.php");
require(dirname(__FILE__)."/../const.php");

class Options
{
    /* Init functions */
    public static function InitTabs(&$aTabs)
    {
        global $MODULE_PREFIX;
        $code = "";
        foreach ($aTabs as $key => &$tab)
        {
            if (is_string($tab))
            {
                $code = $tab;
                unset($aTabs[$key]);
                continue;
            }
            if (!isset($tab["TAB"]) || !isset($tab["TITLE"]) || !isset($tab["CODE"]) || !isset($tab["TAB_CONTENT"]))
                return false;
            $tab["DIV"] = strtolower($MODULE_PREFIX.$tab["CODE"]."_SETTINGS");
            $tab["ICON"] = strtolower($MODULE_PREFIX.$tab["CODE"]."_ICON_SETTINGS");
            foreach ($tab["TAB_CONTENT"] as &$arContent)
                if (!is_string($arContent) && isset($arContent[0]) && is_string($arContent[0])
                    && !static::InitTabs($arContent))
                    return false;
        }
        $aTabs = array_values($aTabs);
        $aTabs["CONTROL"] = new \CAdminTabControl($MODULE_PREFIX.$code, $aTabs, false, true);
        return true;
    }
    public static function InitContent(&$aTabs, $classFiles)
    {
        global $MODULE_NAME;
        global $PARTNER_CODE;
        foreach ($aTabs as $key => &$tab)
        {
            if ($key."" == "CONTROL") continue;
            foreach ($tab["TAB_CONTENT"] as &$arContent)
            {
                if (isset($arContent["CONTROL"]) && !static::InitContent($arContent, $classFiles))
                    return false;
                if (isset($arContent["CONTROL"]) || is_string($arContent))
                    continue;
                if (!isset($arContent["TYPE"]) || !isset($arContent["OPTIONS"]) ||
                    !isset($arContent["NAME"]) || !isset($arContent["CODE"]))
                    return false;
                if (!isset($arContent["VALUE"]))
                    $arContent["VALUE"] = "";
                $arContent["TYPE"] = strtolower($arContent["TYPE"]);

                if ($arContent["TYPE"] == "note" || $arContent["TYPE"] == "html")
                {
                    if (!isset($arContent["OPTIONS"]["TEXT"]))
                        return false;
                    continue;
                }

                if ($arContent["TYPE"] == "custom")
                {
                    if (!isset($arContent["OPTIONS"]["CLASS"]))
                        return false;
                    $className = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options\\".$arContent["OPTIONS"]["CLASS"];
                    if (!isset($classFiles[$className]))
                    {
                        unset($arContent);
                        continue;
                    }
                    $className .= "::Init";
                    if ($className($arContent) === false)
                        return false;
                    continue;
                }
            }
        }
        static::OpenArrays($aTabs);
        return true;
    }
    
    /* Request functions */
    private static function ClearBuffer()
    {
        ob_start();
        echo " ";
        ob_end_clean();
    }
    private static function Redirect($aTabs, $page, $request)
    {
        global $MODULE_ID;
        if ($request["back_url"] && $request["save"])
            LocalRedirect($request["back_url"]);
        $arParams = array(
            "mid" => urlencode($MODULE_ID),
            "lang" => urlencode(LANGUAGE_ID),
        );
        $activeTab = $aTabs["CONTROL"]->ActiveTabParam();
        $activeTab = explode("=", $activeTab);
        $arParams[$activeTab[0]] = $activeTab[1];
        static::GetRedirectParams($aTabs, $request, $arParams);
        $params = http_build_query($arParams);
        LocalRedirect($page."?".$params);
    }
    private static function GetRedirectParams($aTabs, $request, &$arParams)
    {
        global $MODULE_NAME;
        global $PARTNER_CODE;
    
        foreach ($aTabs as $key => $tab)
        {
            if ($key."" == "CONTROL") continue;
            foreach ($tab["TAB_CONTENT"] as $arContent) {
                if (isset($arContent["CONTROL"]))
                    static::GetRedirectParams($arContent, $request, $arParams);
                if ($arContent["TYPE"] != "custom")
                    continue;
                $funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options\\".$arContent["OPTIONS"]["CLASS"]."::GetRedirectParams";
                $funcName($arContent, $request, $arParams);
            }
        }
    }
    
    private static function Save($aTabs, $request)
    {
        global $MODULE_ID;
        global $MODULE_NAME;
        global $MODULE_PREFIX;
        global $PARTNER_CODE;
    
        $activeTab = $aTabs["CONTROL"]->ActiveTabParam();
        $activeTab = explode("=", $activeTab);
        if (!($aTab = $request[$activeTab[0]]))
            $aTab = "";
        foreach ($aTabs as $key => $tab)
        {
            if ($key."" == "CONTROL") continue;
            foreach ($tab["TAB_CONTENT"] as $arContent) {
                if (isset($arContent["CONTROL"]))
                    static::Save($arContent, $request);
                if (isset($arContent["CONTROL"]) || is_string($arContent) ||
                    $arContent["TYPE"] == "note" || $arContent["TYPE"] == "html")
                    continue;
                if ($arContent["TYPE"] == "custom")
                {
                    $funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options\\".$arContent["OPTIONS"]["CLASS"]."::Save";
                    $funcName($arContent, $request);
                    continue;
                }
                $value = false;
                if ($request[$MODULE_PREFIX.$arContent["CODE"]])
                    $value = $request[$MODULE_PREFIX.$arContent["CODE"]];
                \Bitrix\Main\Config\Option::set(
                    $MODULE_ID,
                    $MODULE_PREFIX.$arContent["CODE"],
                    $value
                );
            }
        }
    }
    private static function CustomAction(&$aTabs, $request)
    {
        global $MODULE_NAME;
        global $PARTNER_CODE;
    
        foreach ($aTabs as $key => &$tab) {
            if ($key."" == "CONTROL") continue;
            foreach ($tab["TAB_CONTENT"] as &$arContent) {
                if (isset($arContent["CONTROL"]))
                    static::CustomAction($arContent, $request);
                if ($arContent["TYPE"] != "custom")
                    continue;
                $funcName = "\\" . $PARTNER_CODE . "\\" . $MODULE_NAME . "\\Options\\" . $arContent["OPTIONS"]["CLASS"] . "::CustomAction";
                $funcName($arContent, $request);
            }
        }
        static::OpenArrays($aTabs);
    }
    public static function Request(&$aTabs, $page, $request)
    {
        if(!$request->isPost() || !check_bitrix_sessid())
            return;
        if ($request["save"] || $request["apply"])
            static::Save($aTabs, $request);
        else
            static::CustomAction($aTabs, $request);
        static::ClearBuffer();
        static::Redirect($aTabs, $page, $request);
    }
    
    /* Load values functions */
    public static function LoadValues(&$aTabs, $request)
    {
        global $MODULE_ID;
        global $MODULE_NAME;
        global $MODULE_PREFIX;
        global $PARTNER_CODE;
    
        foreach ($aTabs as $key => &$tab)
        {
            if ($key."" == "CONTROL") continue;
            foreach ($tab["TAB_CONTENT"] as &$arContent) {
                if (isset($arContent["CONTROL"]))
                    static::LoadValues($arContent, $request);
                if (isset($arContent["CONTROL"]) || is_string($arContent) || $arContent["TYPE"] == "note" || $arContent["TYPE"] == "html")
                    continue;
                if ($arContent["TYPE"] == "custom")
                {
                    $funcName = "\\".$PARTNER_CODE."\\".$MODULE_NAME."\\Options\\".$arContent["OPTIONS"]["CLASS"]."::LoadValues";
                    $funcName($arContent, $request);
                    continue;
                }
                $value = \Bitrix\Main\Config\Option::get(
                    $MODULE_ID,
                    $MODULE_PREFIX.$arContent["CODE"],
                    false
                );
                if ($value !== false)
                    $arContent["VALUE"] = $value;
            }
        }
        static::OpenArrays($aTabs);
    }
    
    /* Get HTML */
    private static function GetHTMLTitle($text)
    {
        return
<<<HTML
<tr class="heading">
	<td colspan="2"><b>{$text}</b></td>
</tr>
HTML;
    }
    private static function GetHTMLNote($text)
    {
        return
<<<HTML
<tr><td colspan="2" align="center">
    <div class="adm-info-message-wrap" align="center">
      	<div class="adm-info-message">
      	    {$text}
	    </div>
    </div>
</td></tr>
HTML;
    }
    private static function GetHTMLBase($arContent)
    {
        global $MODULE_ID;
        global $MODULE_PREFIX;

        $option = array(array(
            $MODULE_PREFIX.$arContent["CODE"],
            $arContent["NAME"],
            $arContent["VALUE"],
            $arContent["OPTIONS"]
        ));
        ob_start();
        __AdmSettingsDrawList($MODULE_ID, $option);
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }
    public static function GetHTML($aTabs)
    {
        global $MODULE_NAME;
        global $PARTNER_CODE;
    
        $result = "";
        foreach ($aTabs as $key => $tab) {
            if ($key."" == "CONTROL") continue;
            ob_start();
            $aTabs["CONTROL"]->BeginNextTab();
            $result .= ob_get_contents();
            ob_end_clean();
            foreach ($tab["TAB_CONTENT"] as $arContent) {
                if (isset($arContent["CONTROL"]))
                {
                    ob_start();
                    $arContent["CONTROL"]->Begin();
                    echo static::GetHTML($arContent);
                    $arContent["CONTROL"]->End();
                    $result .= ob_get_contents();
                    ob_end_clean();
                }
                elseif (is_string($arContent))
                    $result .= static::GetHTMLTitle($arContent);
                elseif ($arContent["TYPE"] == "note")
                    $result .= static::GetHTMLNote($arContent["OPTIONS"]["TEXT"]);
                elseif ($arContent["TYPE"] == "base")
                    $result .= static::GetHTMLBase($arContent);
                elseif ($arContent["TYPE"] == "html")
                    $result .= $arContent["OPTIONS"]["TEXT"];
                else {
                    $funcName = "\\" . $PARTNER_CODE . "\\" . $MODULE_NAME . "\\Options\\" . $arContent["OPTIONS"]["CLASS"] . "::GetHTML";
                    $result .= $funcName($arContent);
                }
            }
        }
        return $result;
    }
    
    private static function OpenArray(&$newContent, $arContent)
    {
        foreach ($arContent as $content)
        {
            if (isset($content["CONTROL"]) || is_string($content)) {
                $newContent[] = $content;
                continue;
            }
            if (!isset($content["TYPE"]) || !isset($content["OPTIONS"]) ||
                !isset($content["NAME"]) || !isset($content["CODE"]))
                continue;
            if (!isset($content["VALUE"]))
                $content["VALUE"] = "";
            $content["TYPE"] = strtolower($content["TYPE"]);
    
            if ($arContent["TYPE"] == "note" || $arContent["TYPE"] == "html")
                if (!isset($arContent["OPTIONS"]["TEXT"]))
                    continue;
    
            if ($arContent["TYPE"] == "custom")
                if (!isset($arContent["OPTIONS"]["CLASS"]))
                    continue;
            $newContent[] = $content;
        }
    }
    private static function OpenArrays(&$aTabs)
    {
        foreach ($aTabs as $key => &$tab)
        {
            if ($key."" == "CONTROL") continue;
            $newContent = array();
            foreach ($tab["TAB_CONTENT"] as $arContent)
            {
                if ($arContent["TYPE"] == "array") {
                    unset($arContent["TYPE"]);
                    static::OpenArray($newContent, $arContent);
                }
                else
                    $newContent[] = $arContent;
            }
            $tab["TAB_CONTENT"] = $newContent;
        }
    }
}
?>
<style>
.adm-detail-content-table {
    width: inherit;
}
.adm-workarea input[type="text"] {
    width: 300px;
}
.adm-workarea select {
    width: inherit;
}
progress{
    width: inherit;
    text-align: center;
}
.progress{
    display: inline;
}
.bprogress{
    float: right;
}
.progress{
    margin-left: 10px;
}
</style>