<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $MODULE_ID;
global $MODULE_NAME;
global $MODULE_PREFIX;
global $PARTNER_CODE;

require_once(dirname(__FILE__)."/../lang.php");
require(dirname(__FILE__)."/../const.php");

$aTabs = array(
    "MAIN",
    array(
        "TAB" => \Mcart\GetMessage("O_MAIN_SETTINGS"),
        "TITLE" => \Mcart\GetMessage("O_MAIN_SETTINGS_TITLE"),
        "CODE" => "MAIN_SETTINGS",
        "TAB_CONTENT" => array(
            array(
                "NAME" => "",
                "CODE" => "PROGRESS",
                "TYPE" => "custom",
                "VALUE" => "",
                "OPTIONS" => array("CLASS" => "mcart_progress")
            ),
            array(
                "NAME" => \Mcart\GetMessage("O_MAIN_SETTINGS_IBLOCK"),
                "CODE" => "IBLOCK",
                "TYPE" => "custom",
                "VALUE" => "",
                "OPTIONS" => array("CLASS" => "mcart_iblock")
            ),
            array(
                "NAME" => \Mcart\GetMessage("O_MAIN_SETTINGS_PATH_AND_NAME"),
                "CODE" => "PATH_AND_NAME",
                "TYPE" => "custom",
                "VALUE" => "",
                "OPTIONS" => array("CLASS" => "mcart_text_pare", "NAMES" => array("NAME", "PATH"))
            ),
            array(
                "NAME" => \Mcart\GetMessage("O_MAIN_SETTINGS_HTML"),
                "CODE" => "HTML",
                "TYPE" => "custom",
                "VALUE" => "",
                "OPTIONS" => array("CLASS" => "mcart_html")
            )
        )
    )
);
?>