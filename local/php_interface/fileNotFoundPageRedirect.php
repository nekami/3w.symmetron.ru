<?
AddEventHandler("main", "OnBeforeProlog", Array("fileNotFoundPageRedirect", "redirect"));


class fileNotFoundPageRedirect{

    function redirect() {
        global $USER, $APPLICATION;

        $page = $APPLICATION->GetCurPage(true);

        if (!$USER->IsAuthorized() && $page == "/bitrix/tools/form_show_file.php"){
            LocalRedirect("/?auth_red=yes&error_file=yes");
        }
    }
}