<?
include_once(__DIR__."/include/functions.php");
include_once(__DIR__."/include/const.php");
include_once(__DIR__."/include/handlers.php");

AddEventHandler("main", "OnBeforeEventAdd", array("MainHandlers", "OnBeforeEventAddHandler"));
class MainHandlers{
   function OnBeforeEventAddHandler(&$event, &$lid, &$arFields, &$messageId, &$files, &$languageId){
      if ($event == "SALE_NEW_ORDER" or $event == "SALE_NEW_ORDER_BUY" or $event == "SALE_NEW_ORDER_EXAMPLE" or $event == "SALE_NEW_ORDER_REQUEST" ){
        //$files = Array("/order.txt");
		//CEvent::Send("SALE_NEW_ORDER_BUY_WITH_ATTACH", array("s1"), $arFields);
		
		$_R['number']=$arFields["ORDER_ID"];
		if(file_exists($_SERVER["DOCUMENT_ROOT"]."/work/sendemail.php")){ require_once($_SERVER["DOCUMENT_ROOT"]."/work/sendemail.php"); }
		if(file_exists($_SERVER["DOCUMENT_ROOT"]."/work/property.orders.new.php")){ require_once($_SERVER["DOCUMENT_ROOT"]."/work/property.orders.new.php"); }
		//Отправляем письмо
		$title = "Заказ с сайта № ".$_R['number'];
	    $message = "Поступил новый заказ № ".$_R['number'].", подробности во вложении.<br><br>
		Заказчик: ".$arFields["ORDER_USER"]."<br>
		Для компании: ".$orderCOMPANY."<br>
		ИНН: ".$orderINN."<br><br>
		Комментарий: ".$orderCOMMENT."<br>
		";
		//$toemail = "Быстрова Анастасия<anastasia.bystrova@symmetron.ru>, Загорельская Кристина<kristina.zagorelskay@symmetron.ru>";
		//$replyto   = "Владимиров Игорь<igor.vladimirov@symmetron.ru>, Христенкин Максим<maksim.khristenkin@symmetron.ru>";
		//$toemail   = "Владимиров Игорь<igor.vladimirov@symmetron.ru>, Христенкин Максим<maksim.khristenkin@symmetron.ru>, moscow@symmetron.ru";
		$toemail   = "moscow@symmetron.ru";
		//$toemail   = "website@symmetron.ru";
		$replyto   = "website@symmetron.ru";
		$fromemail = "wwwstat@symmetron.ru";
		$filesend = Array("/work/output/order.".$_R['number'].".xlsx");
		sendMessageMail($toemail,$replyto, $fromemail, $title, $message, $filesend);
		unlink($_SERVER["DOCUMENT_ROOT"]."/work/output/order.".$_R['number'].".xlsx");
      }
   }
}

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/CCustomTypes.php"))
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/CCustomTypes.php");

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/fileNotFoundPageRedirect.php"))
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/fileNotFoundPageRedirect.php");

AddEventHandler("sale", "OnBeforeBasketAdd", "OnBeforeBasketAdd");
function  OnBeforeBasketAdd(&$arFields){
	if(Cmodule::IncludeModule("mcart.basketsymmetron") && Cmodule::IncludeModule("sale")){
		if(key_exists("PROPS", $arFields) && key_exists("EXAMPLE", $arFields["PROPS"])){
			if($arFields["PROPS"]["EXAMPLE"]["VALUE"] === "Y"){
				$arFields["QUANTITY"] = 1;
			}
		}
	}
}

AddEventHandler("sale", "OnBeforeBasketUpdate", "OnBeforeBasketUpdate");
function  OnBeforeBasketUpdate($ID, &$arFields){
	if(Cmodule::IncludeModule("mcart.basketsymmetron") && Cmodule::IncludeModule("sale")){
		if(key_exists("ID", $arFields)){
			$db_res = CSaleBasket::GetPropsList(
				array(
						"SORT" => "ASC",
						"NAME" => "ASC"
				),
				array(
					"BASKET_ID" => $ID
				)
			);

			while ($ar_res = $db_res->Fetch())
			{
				if($ar_res["CODE"] === "EXAMPLE" && $ar_res["VALUE"] === "Y"){
					$arFields["QUANTITY"] = 1;
				}
			}
		}
	}
}

/*
 * TODO: put to onPageStart event
 */
if (CModule::IncludeModule("sale") && CModule::IncludeModule("mcart.basketsymmetron"))
{

    $arInstances = array(
        Bitrix\Sale\Registry::ENTITY_ORDER => '\Mcart\BasketSymmetron\SymmetronOrder',
        Bitrix\Sale\Registry::ENTITY_PAYMENT => '\Bitrix\Sale\Payment',
        Bitrix\Sale\Registry::ENTITY_PAYMENT_COLLECTION => '\Bitrix\Sale\PaymentCollection',
        Bitrix\Sale\Registry::ENTITY_SHIPMENT => '\Bitrix\Sale\Shipment',
        Bitrix\Sale\Registry::ENTITY_SHIPMENT_COLLECTION => '\Bitrix\Sale\ShipmentCollection',
        Bitrix\Sale\Registry::ENTITY_SHIPMENT_ITEM => '\Bitrix\Sale\ShipmentItem',
        Bitrix\Sale\Registry::ENTITY_SHIPMENT_ITEM_COLLECTION => '\Bitrix\Sale\ShipmentItemCollection',
        Bitrix\Sale\Registry::ENTITY_SHIPMENT_ITEM_STORE => '\Bitrix\Sale\ShipmentItemStore',
        Bitrix\Sale\Registry::ENTITY_SHIPMENT_ITEM_STORE_COLLECTION => '\Bitrix\Sale\ShipmentItemStoreCollection',
        Bitrix\Sale\Registry::ENTITY_PROPERTY_VALUE_COLLECTION => '\Bitrix\Sale\PropertyValueCollection',
        Bitrix\Sale\Registry::ENTITY_PROPERTY_VALUE => '\Bitrix\Sale\PropertyValue',
        Bitrix\Sale\Registry::ENTITY_TAX => '\Bitrix\Sale\Tax',
        Bitrix\Sale\Registry::ENTITY_BASKET_PROPERTY_ITEM => '\Bitrix\Sale\BasketPropertyItem',
        Bitrix\Sale\Registry::ENTITY_BUNDLE_COLLECTION => '\Bitrix\Sale\BundleCollection',
        Bitrix\Sale\Registry::ENTITY_BASKET => '\Mcart\BasketSymmetron\SymmetronBasket',
        Bitrix\Sale\Registry::ENTITY_BASKET_ITEM => '\Bitrix\Sale\BasketItem',
        Bitrix\Sale\Registry::ENTITY_BASKET_PROPERTIES_COLLECTION => '\Bitrix\Sale\BasketPropertiesCollection',
        Bitrix\Sale\Registry::ENTITY_DISCOUNT => '\Bitrix\Sale\Discount',
        Bitrix\Sale\Registry::ENTITY_OPTIONS => 'Bitrix\Main\Config\Option',
        Bitrix\Sale\Registry::ENTITY_PERSON_TYPE => 'Bitrix\Sale\PersonType',
    );


    Bitrix\Sale\Registry::setRegistry(Bitrix\Sale\Registry::REGISTRY_TYPE_ORDER, $arInstances);

}


//js for declination
\Bitrix\Main\UI\Extension::load("mcart.symmetrondeclination");