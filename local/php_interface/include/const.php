<?
// ID ИБ Новости
define("IBLOCK_NEWS_ID", 8);

// ID свойства "Мероприятие" у ИБ Новости
define("PROPERTY_ID_ACTIVITY", 3685);

// ID свойства "Тип мероприятия" у ИБ Новости
define("PROPERTY_ID_TYPE_ACTIVITY", 3693);

// ID ИБ Мероприятия
define("IBLOCK_ACTIVITY_ID", 28);

// ID свойства "Тип мероприятия" у ИБ Мероприятия
define("PROPERTY_TYPE_ACTIVITY", 3674);

// ID Highload-блока с докладчиками, которые выводятся на детальной странице мероприятия
define("ID_HIGHLOAD_REPORTER", 31);
?>