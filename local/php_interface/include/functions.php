<?
function s($var, $userID = 0)
{
    global $USER;
    
    if (($userID > 0 && $USER->GetID() == $userID) || $userID == 0) {
        if ($var === false) {
            $var = "false";
        }
        echo "<xmp>";
        print_r($var);
        echo "</xmp>";
    }
}

function p($var, $url = false, $delimiter = false)
{
    if (empty($url)) {
        $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
    }

    if (!empty($delimiter)) {
        $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
    } else {
        $delimiter = "\n\n-------------------------------------------------\n\n";
    }
    file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
}

function uni_declension($num, $str)
{
   $exp = explode(',', $str);
   $num = (($num < 0) ? $num-$num*2 : $num)%100;
   $dig = ($num > 20) ? $num%10 : $num;
   return trim((($dig == 1) ? $exp[0] : (($dig > 4 || $dig < 1) ? $exp[2] : $exp[1])));
}
?>