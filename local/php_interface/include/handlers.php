<?
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementAddHandler");

function OnBeforeIBlockElementAddHandler(&$arFields)
{	
	if ($arFields["IBLOCK_ID"] == IBLOCK_NEWS_ID && !empty($arFields["PROPERTY_VALUES"][PROPERTY_ID_ACTIVITY][key($arFields["PROPERTY_VALUES"][PROPERTY_ID_ACTIVITY])]["VALUE"]))
	{		
		$idActivity = $arFields["PROPERTY_VALUES"][PROPERTY_ID_ACTIVITY][key($arFields["PROPERTY_VALUES"][PROPERTY_ID_ACTIVITY])]["VALUE"];
				
		$res = CIBlockElement::GetList([], ['IBLOCK_CODE' => 'activity', 'ID' => $idActivity, 'ACTIVE' => 'Y'], false, ["nTopCount"=>1], ["ID", "PROPERTY_type"]);
	
		if ($arActivity = $res->fetch())
		{		
			$arFields["PROPERTY_VALUES"][PROPERTY_ID_TYPE_ACTIVITY][key($arFields["PROPERTY_VALUES"][PROPERTY_ID_TYPE_ACTIVITY])]["VALUE"] = $arActivity["PROPERTY_TYPE_VALUE"];
		}
	}
	
	if (($arFields["IBLOCK_ID"] == IBLOCK_ACTIVITY_ID) && !empty($arFields["PROPERTY_VALUES"][PROPERTY_TYPE_ACTIVITY][key($arFields["PROPERTY_VALUES"][PROPERTY_TYPE_ACTIVITY])]["VALUE"]))
	{			
		$res = CIBlockElement::GetList([], ['IBLOCK_CODE' => 'news', 'PROPERTY_activity' => $arFields["ID"], 'ACTIVE' => 'Y'], false, false, ["ID"]);
				
		while ($arNews = $res->fetch())		
		{ 
			if (!empty($arNews["ID"]))
			{
				CIBlockElement::SetPropertyValuesEx($arNews["ID"], false, array('typeactivity' => $arFields["PROPERTY_VALUES"][PROPERTY_TYPE_ACTIVITY][key($arFields["PROPERTY_VALUES"][PROPERTY_TYPE_ACTIVITY])]["VALUE"]));			
			}
		}
	}
}
?>