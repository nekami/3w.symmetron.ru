<?
@set_time_limit(0);
@ignore_user_abort(true);

define("NOT_CHECK_PERMISSIONS",true); 
define("BX_CRONTAB_SUPPORT", true);
define("NO_KEEP_STATISTIC", true);
define("BX_CRONTAB", true);
define('CHK_EVENT', true);

$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CAgent::CheckAgents();
CEvent::CheckEvents();

if(CModule::IncludeModule('sender'))
{
    \Bitrix\Sender\MailingManager::checkPeriod(false);
    \Bitrix\Sender\MailingManager::checkSend();
}
