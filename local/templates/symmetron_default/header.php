<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
    <? $APPLICATION->ShowMeta("title") ?>

	<meta charset="<?=LANG_CHARSET?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/jquery.bxslider.css");?>
	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/jquery.modal.css");?>
	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/owl.carousel.min.css");?>
	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/owl.theme.default.css");?>

	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/magnific-popup.css");?>

	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/gray.min.css");?>
	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/tooltipster.bundle.min.css");?>
	<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/style.css");?>
	<?Asset::getInstance()->addString("<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700' rel='stylesheet'>");?>

	<?$APPLICATION->ShowHead();?>

    <!--show link canonical-->
    <?Asset::getInstance()->addString($APPLICATION->ShowViewContent('link_canonical'));?>

    <!--show link pagination-->
    <?Asset::getInstance()->addString($APPLICATION->ShowViewContent('link_pagination'));?>

	<title><?$APPLICATION->ShowTitle()?></title>
	<script>
		var fio,email,phone,favsIds;
		<?
		global $USER;
		if (!is_object($USER)) $USER = new CUser;
		define('AUTH_USER', $USER->IsAuthorized() );
		if ( AUTH_USER ){
	    define('USER_ID', $USER->GetID() );
	    $res = CUser::GetList($by = "id", $order = "asc", ['ID'=>USER_ID],['SELECT'=>["UF_FAVS"]])->Fetch();
    	echo "email = '{$res['EMAIL']}';";
      echo "fio = '".($res['LAST_NAME']? $res['LAST_NAME'] :'') . ($res['NAME']? ' '.$res['NAME'] :'').($res['SECOND_NAME']? ' '.$res['SECOND_NAME'] :'')."';";
      echo "phone = '{$res['PERSONAL_PHONE']}';";
      $groups = CUser::GetUserGroup($USER->GetID());

	    echo 'favsIds = ['.implode(',', $res["UF_FAVS"]).'];';
	    ?>
			$(document).ready(function() {
				$('div.fio input').each(function(index, el) {
					$(el).val(fio);
				});
				$('div.mail input').each(function(index, el) {
					$(el).val(email);
				});
				$('div.phone input').each(function(index, el) {
					$(el).val(phone);
				});
				$('.favorite,.sidebar-favorite').each(function(index, el) {
					if( favsIds.length && favsIds.indexOf( $(el).data('id') )>=0 ){
						$(el).addClass('act');
						$(el).find('.fav_status').text('В избранном');
					} else {
						$(el).removeClass('act');
						$(el).find('.fav_status').text('Добавить в избранное');
					}
				});
			});
    	<?
		}
		?>
	</script>

<!-- Google Tag Manager -->
<script data-skip-moving="true">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVHNGGM');</script>
<!-- End Google Tag Manager -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WVHNGGM";
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<?$APPLICATION->ShowPanel();?>
	<header>
		<div class="header-top">
			<div class="wrapper">
				<?require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/include_areas/header/nav/burger.php")?>
				<div class="top-nav__wrapper">
					<ul>
					
					<li>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_RECURSIVE" => "Y",
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "",
								"COMPOSITE_FRAME_MODE" => "A",
								"COMPOSITE_FRAME_TYPE" => "AUTO",
								"EDIT_TEMPLATE" => "",
								"PATH" => SITE_TEMPLATE_PATH."/include_areas/header/nav/link_1.php"
							)
						);?>
					</li>
					
					<li>
						<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_RECURSIVE" => "Y",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_TEMPLATE_PATH."/include_areas/header/nav/link_2.php"
						)
						);?>
					</li>					
					
					<li><a href="/activity/"><?=GetMessage("ACTIVITY")?></a></li>					
					
					<li><?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_RECURSIVE" => "Y",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_TEMPLATE_PATH."/include_areas/header/nav/link_3.php"
						)
						);?>
					</li>						
						
					<li><?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_RECURSIVE" => "Y",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_TEMPLATE_PATH."/include_areas/header/nav/link_4.php"
						)
						);?>
					</li>
						
					</ul>
				</div>
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket.line", 
					"symmetron", 
					array(
						"COMPOSITE_FRAME_MODE" => "A",
						"COMPOSITE_FRAME_TYPE" => "AUTO",
						"HIDE_ON_BASKET_PAGES" => "N",
						"PATH_TO_AUTHORIZE" => "",
						"PATH_TO_BASKET" => SITE_DIR."cart/",
						"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",
						"PATH_TO_PROFILE" => SITE_DIR."personal/",
						"PATH_TO_REGISTER" => SITE_DIR."login/",
						"PATH_TO_FAVORITE" => SITE_DIR."personal/favorites/",
						"POSITION_FIXED" => "N",
						"SHOW_AUTHOR" => "Y",
						"SHOW_DELAY" => "N",
						"SHOW_EMPTY_VALUES" => "N",
						"SHOW_IMAGE" => "N",
						"SHOW_NOTAVAIL" => "N",
						"SHOW_NUM_PRODUCTS" => "N",
						"SHOW_PERSONAL_LINK" => "N",
						"SHOW_PRICE" => "Y",
						"SHOW_PRODUCTS" => "Y",
						"SHOW_SUMMARY" => "Y",
						"SHOW_TOTAL_PRICE" => "N",
						"AJAX" => "N",
						"COMPONENT_TEMPLATE" => "symmetron"
					),
					false
				);?>

			</div>
		</div>
		<div class="header-bot">
			<div class="wrapper">
				<div class="header-bot__info">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_RECURSIVE" => "Y",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_TEMPLATE_PATH."/include_areas/header/logo.php"
						)
					);?>
					<div class="header-bot nav-wrapper">
						<ul>
							<li>
								<?$APPLICATION->IncludeComponent(
									"bitrix:catalog.section.list",
									"symmetron_header_menu",
									Array(
										"ADD_SECTIONS_CHAIN" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "36000000",
										"CACHE_TYPE" => "A",
										"COMPOSITE_FRAME_MODE" => "A",
										"COMPOSITE_FRAME_TYPE" => "AUTO",
										"COUNT_ELEMENTS" => "N",
										"IBLOCK_ID" => "4",
										"IBLOCK_TYPE" => "catalog",
										"CATALOG_URL" => "/catalog/",
										"SECTION_USER_FIELDS" => array("",""),
										"SHOW_PARENT_NAME" => "N",
										"TOP_DEPTH" => "1",
									)
								);?>
							</li>
							<li>
								<?$APPLICATION->IncludeComponent(
                                    "bitrix:news.list",
                                    "symmetron_brand_menu",
                                    array(
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_ADDITIONAL" => "",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_TYPE" => "A",
                                        "CHECK_DATES" => "Y",
                                        "COMPOSITE_FRAME_MODE" => "A",
                                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                                        "DETAIL_URL" => "",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "N",
                                        "DISPLAY_PICTURE" => "N",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "FIELD_CODE" => array(
                                            0 => "NAME",
                                            1 => "",
                                        ),
                                        "FILTER_NAME" => "",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "IBLOCK_ID" => "26",
                                        "IBLOCK_TYPE" => "lists",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "INCLUDE_SUBSECTIONS" => "N",
                                        "MESSAGE_404" => "",
                                        "NEWS_COUNT" => "0",
                                        "PAGER_BASE_LINK_ENABLE" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "Y",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => ".default",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "PROPERTY_CODE" => array(
                                            0 => "",
                                            1 => "ELEM_BRAND",
                                            2 => "",
                                        ),
                                        "SET_BROWSER_TITLE" => "N",
                                        "SET_LAST_MODIFIED" => "N",
                                        "SET_META_DESCRIPTION" => "N",
                                        "SET_META_KEYWORDS" => "N",
                                        "SET_STATUS_404" => "N",
                                        "SET_TITLE" => "N",
                                        "SHOW_404" => "N",
                                        "SORT_BY1" => "SORT",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER1" => "ASC",
                                        "SORT_ORDER2" => "ASC",
                                        "STRICT_SECTION_CHECK" => "N",
                                        "COMPONENT_TEMPLATE" => "symmetron_brand_menu",
                                        "PAGER_TITLE" => "Новости"
                                    ),
                                    false
                                );?>
							</li>							
						</ul>
					</div>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_RECURSIVE" => "Y",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_TEMPLATE" => "",
							"PATH" => SITE_TEMPLATE_PATH."/include_areas/header/contacts.php"
						)
					);?>
				</div>
				<?$APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"symmetron", 
	array(
		"COMPONENT_TEMPLATE" => "symmetron",
		"NUM_CATEGORIES" => "5",
		"TOP_COUNT" => "7",
		"ORDER" => "rank",
		"USE_LANGUAGE_GUESS" => "N",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => "#SITE_DIR#search/",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "title-search",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CATEGORY_0_TITLE" => "Каталог",
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_main" => "",
		"CATEGORY_0_forum" => array(
			0 => "all",
		),
		"CATEGORY_0_iblock_catalog" => array(
			0 => "4",
		),
		"CATEGORY_0_iblock_news" => array(
			0 => "all",
		),
		"CATEGORY_0_iblock_offers" => array(
			0 => "all",
		),
		"CATEGORY_0_iblock_services" => array(
			0 => "all",
		),
		"CATEGORY_0_iblock_references" => array(
			0 => "all",
		),
		"CATEGORY_0_blog" => array(
			0 => "all",
		),
		"CATEGORY_0_socialnetwork" => array(
			0 => "all",
		),
		"CATEGORY_1_TITLE" => "Новости",
		"CATEGORY_1" => array(
			0 => "iblock_news",
		),
		"CATEGORY_2_TITLE" => "Производители",
		"CATEGORY_2" => array(
			0 => "iblock_lists",
		),
		"CATEGORY_3_TITLE" => "Документы",
		"CATEGORY_3" => array(
			0 => "iblock_documents",
		),
		"CATEGORY_4_TITLE" => "Мероприятия",
		"CATEGORY_4" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_1_socialnetwork_user" => "",
		"CATEGORY_1_main" => "",
		"CATEGORY_1_forum" => array(
			0 => "all",
		),
		"CATEGORY_1_iblock_catalog" => array(
			0 => "all",
		),
		"CATEGORY_1_iblock_news" => array(
			0 => "8",
		),
		"CATEGORY_1_iblock_offers" => array(
			0 => "all",
		),
		"CATEGORY_1_iblock_services" => array(
			0 => "all",
		),
		"CATEGORY_1_iblock_references" => array(
			0 => "all",
		),
		"CATEGORY_1_blog" => array(
			0 => "all",
		),
		"CATEGORY_1_socialnetwork" => array(
			0 => "all",
		),
		"CATEGORY_2_iblock_catalog" => array(
			0 => "all",
		),
		"CATEGORY_4_forum" => array(
			0 => "all",
		),
		"CATEGORY_3_main" => "",
		"CATEGORY_4_main" => "",
		"CATEGORY_2_iblock_lists" => array(
			0 => "11",
		),
		"CATEGORY_3_iblock_catalog" => array(
			0 => "9",
		),
		"CATEGORY_4_iblock_lists" => array(
			0 => "11",
		),
		"CATEGORY_3_iblock_documents" => array(
			0 => "16",
		),
		"CATEGORY_4_iblock_catalog" => array(
			0 => "28",
		)
	),
	false
);?>
			</div>
		</div>
	</header>
	<main>
		<? if( $APPLICATION->GetCurDir()!='/' 
			&& mb_strpos($APPLICATION->GetCurDir(), 'catalog') === FALSE 
			&& ( mb_strpos($APPLICATION->GetCurDir(), 'brands') === FALSE 
				|| mb_strpos($APPLICATION->GetCurDir(), 'brands') !== FALSE 
				&& (count(explode("/", $_SERVER['REQUEST_URI'])) <= 3) ) 
		)
			$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", Array('START_FROM'=>1));
