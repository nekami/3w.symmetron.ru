<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
?>

</main>


<footer>
    <?$pos = strpos($APPLICATION->GetCurDir(), "/personal/");?>
    <?if($pos === false):?>
        <?$APPLICATION->IncludeComponent(
	"mcart:sender.subscribe",
	"footer", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONFIRMATION" => "Y",
		"HIDE_MAILINGS" => "N",
		"SET_TITLE" => "N",
		"SHOW_HIDDEN" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "N",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_PERSONALIZATION" => "Y",
		"COMPONENT_TEMPLATE" => "footer",
		"EVERY_DAY" => array(
			0 => "3",
			1 => "4",
		),
		"SHOW" => array(
			0 => "5",
			1 => "6",
			2 => "7",
		),
		"PRODUCTS" => array(
			0 => "8",
			1 => "9",
			2 => "10",
		)
	),
	false
);?>
    <?endif;?>
    <?/*$APPLICATION->IncludeComponent(
    "bitrix:subscribe.form",
    "",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "PAGE" => "#SITE_DIR#personal/subscribe.php",
        "SHOW_HIDDEN" => "N",
        "USE_PERSONALIZATION" => "N"
    )
);*/?>
    <section class="services">
        <div class="wrapper">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_RECURSIVE" => "Y",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/include_areas/services/modal/info.php"
                )
            );?>
            <div class="services-items">
                <a href="/bom/" class="service-item bom">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/services/bom.php"
                        )
                    );?>
                </a>

                <a href="#modal-file" class="service-item support" rel="modal:open">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/services/exel.php"
                        )
                    );?>
                </a>
                <a href="#modal-callback" class="service-item callback" rel="modal:open">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/services/callback.php"
                        )
                    );?>
                </a>
            </div>
        </div>
    </section>
    <div class="wrapper">
        <div class="footer-head">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_RECURSIVE" => "Y",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/include_areas/footer/logo.php"
                )
            );?>
        </div>
        <div class="footer-content">
            <div class="row">
                <div class="col-3">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/footer/contact_1.php"
                        )
                    );?>
                </div>
                <div class="col-3">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/footer/contact_2.php"
                        )
                    );?>
                </div>
                <div class="col-3">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/footer/contact_3.php"
                        )
                    );?>
                </div>
                <div class="col-3">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_RECURSIVE" => "Y",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/include_areas/footer/contact_4.php"
                        )
                    );?>
                </div>
            </div>
			<div class="row">
				<div class="col-9"> &nbsp; </div>
				<div class="col-3">
                    <div class="old-site__wrapper">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_RECURSIVE" => "Y",
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "",
                                "COMPOSITE_FRAME_MODE" => "A",
                                "COMPOSITE_FRAME_TYPE" => "AUTO",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_TEMPLATE_PATH."/include_areas/footer/old_site.php"
                            )
                        );?>
                    </div>
				</div>
                <div class="copyright">
                    <noindex><a rel="nofollow" href="https://www.mcart.ru/" class="copyright-link" target="blank">
                        <span>Разработано в</span>
                        <!--<img src="http://285624.selcdn.ru//syms1/logo.svg" alt="Разработано в Эм Си Арт">-->
                        <img src="/local/templates/symmetron_default/images/logo-MCART.png" alt="Разработано в Эм Си Арт">
                    </a></noindex>
                </div>
			</div>
        </div>
    </div>
</footer>
<?require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/include_areas/services/modal/exel.php")?>
<?require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/include_areas/services/modal/callback.php")?>
<?require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/include_areas/services/modal/result.php")?>

<?\CJSCore::Init(array("jquery"));?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.bxslider.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/owl.carousel.min.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.modal.js");?>

<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.magnific-popup.min.js");?>

<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.mask.min.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.gray.min.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.stickybits.min.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.floatThead.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/tooltipster.bundle.min.js");?>
<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/script.js");?>

<?global $USER;?>
<?if (isset($_GET["register"]) && $_GET["register"] = "yes" && !$USER->IsAuthorized()):?>
    <script>
        BX.ready(function () {
            $("#modal-registration").modal();
        });
    </script>
<?elseif (isset($_GET["auth_red"]) && $_GET["auth_red"] = "yes" && !$USER->IsAuthorized()):?>
    <script>
        BX.ready(function () {
            $("#modal-login").modal();
        });
    </script>
<?endif;?>

<?
if($_GET["formresult"] == "addok" && ($_GET["WEB_FORM_ID"] == 2 || $_GET["WEB_FORM_ID"] == 1))
{?>
<script>
    //history.replaceState(3, "Title 2", url);
    var url = window.location.search;
    var params = url.replace("?", "").split("&");
    url = '?';
    for(var i in params){
        if(params[i] == "formresult=addok"){
            continue;
        }
        url += params[i];
    }
    history.replaceState(false, false, url);
    BX.ready(function () {
        BX.Mcart.modals.result_off.open(["Уведомление", "Спасибо, Ваши данные успешно отправлены."]);
    });
</script>
<?
}
?>


<?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/mobile.css");?>
</body>
</html>
