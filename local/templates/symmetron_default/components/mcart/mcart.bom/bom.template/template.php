<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




<?if($arResult["FORM"] == true):?>
<?/*<section class="card">
    <div class="wrapper">
        <?if(!empty($arResult["ERROR"])):?>
            <?=$arResult["ERROR"];?>
        <?endif;?>
        <form enctype="multipart/form-data" method="post">
            <input type="file" name="file"> <br>
            <br>
            Столбец артикула: <input type="text" value="A" name="art"> Столбец количества: <input type="text" value="B" name="quant"> <br>
            <br>

            <input name="submit" type="submit" value="Отправить">
        </form>
    </div>
</section>*/?>


    <section class="bom">
        <div class="wrapper">
            <h1>Загрузка BOM</h1>
            <div class="bom-form">

                <form enctype="multipart/form-data" method="post" id="form-for-bom">
                    <div class="bom-file">
						<label for="bom-file">
							<span class="bom__label">Прикрепите файл</span>
							<span class="bom__grey">(*.csv, *.xlsx, *.xls до 1 Мб, без формул в ячейках)</span>
						</label>
                        <div class="bomfile-holder">
                            <div class="fake-input">Выберите файл</div>
                            <input type="file" name="file" id="bom-file" required/>
                            <span class="chose-file">Файл отсутствует</span>
                        </div>
                    </div>
                    <div class="bom-article">
                        <label for="bom-article">
							<span class="bom__label">Укажите столбец</span>
							<span class="bom__grey">с кодом товара производителя (MPN)</span>
						 </label>
						<div class="bom-input">
							<input type="text" name="art" id="bom-article" placeholder="A" required/>
							<span class="bom__grey">Только латиница</span>
 						</div>

                    </div>
                    <div class="bom-quantity">
                        <label for="bom-quantity">Укажите столбец с количеством</label>
						<div class="bom-input">
							<input type="text" name="quant" id="bom-quantity" placeholder="B" required/> 
							<span class="bom__grey">Только латиница</span>
						</div>
                    </div>
                    <input type="submit" name="submit" value="Загрузить" class="default-button upload-bom">
                </form>

                <span style='color: red' id="bom-errors">
                    <?if(!empty($arResult["ERROR"])):?>
                        <?=$arResult["ERROR"];?>
                    <?endif;?>
                </span>

            </div>
        </div>
    </section>

<?else:?>

    <?
    //echo "<pre>";
    //print_r($arResult);
    //echo "</pre>";

    ?>
<?/*<section class="card">
    <div class="wrapper">
        <table>
            <tr>
                <td>Артикул</td><td>Количество для добавления</td><td>Статус</td><td>Количество на складе</td><td>Доступность добавления</td>
            </tr>
            <?foreach ($arResult["DATE"] as $val):?>

                <tr>
                    <?if(!empty($arResult["STAFFS"][$val[0]]) && !empty($arResult["STAFFS"][$val[0]]["QUANTITY"]) && !empty($arResult["STAFFS"][$val[0]]["AVAILABLE"])):?>
                        <td><?=$val[0]?></td><td><?=$val[1]?></td><td><?=GetMessage("BOM_RASP");?></td><td><?=$arResult["STAFFS"][$val[0]]["QUANTITY"];?></td><td><button onclick="addStaff(<?=$arResult["STAFFS"][$val[0]]["ID"]?>, <?=$val[1]?>, '<?=$arParams["BASK_ID"]?>');"><?=GetMessage("BOM_ADD");?></button></td>
                    <?else:?>
                        <td><?=$val[0]?></td><td><?=$val[1]?></td><td><?=GetMessage("BOM_NERASP");?></td><td></td><td>Не доступен</td>
                    <?endif;?>
                </tr>

            <?endforeach;?>
        </table>
    </div>
</section>*/?>



    <section class="bom">
        <div class="wrapper">
            <h1>Загрузка BOM</h1>
            <div class="bom-table__wrapper">
                <table class="bom-table">
                    <tr>
                        <th>Номер</th>
                        <th>Статус</th>
                        <th>Название</th>
                        <th>Артикул</th>
                        <th>Производитель</th>
                        <th>Статус на складе</th>
                        <th>Количество</th>
                    </tr>

                    <?
                    //echo "<pre>";
                    //print_r($arResult["STAFFS"]);
                    //echo "</pre>";
                    ?>

                    <?$countNo = 0;?>
                    <?$countYes = 0;?>
                    <?$arJs = array();?>
                    <?foreach ($arResult["DATE"] as $key => $val):?>
                        <?if(!empty($arResult["STAFFS"][$val[0]])):?>
                            <?if($val[1] > 0):?>
                                <tr>
                                    <?
                                    if(!empty($arResult["STAFFS"][$val[0]]["PRICE"]))
                                        $priceFlag = "true";
                                    else
                                        $priceFlag = "false";
                                    ?>
                                    <td><?=++$key;?></td>
                                    <?if(!empty($arResult["STAFFS"][$val[0]]["PRICE"])):?>
                                        <td id="<?="staff_".$key?>"><div class="status-add" onclick="addStaff(<?=$key?>, <?=$arResult["STAFFS"][$val[0]]["ID"]?>, <?=$val[1]?>, '<?=$arParams["BASK_ID"]?>', '<?=$arResult["STAFFS"][$val[0]]["NAME"]?>', '<?=$priceFlag;?>');">+ Добавить</div></td>
                                    <?else:?>
                                        <td id="<?="staff_".$key?>"><div class="status-add" onclick="addStaff(<?=$key?>, <?=$arResult["STAFFS"][$val[0]]["ID"]?>, <?=$val[1]?>, '<?=$arParams["BASK_ID"]?>', '<?=$arResult["STAFFS"][$val[0]]["NAME"]?>', '<?=$priceFlag;?>');">+ Запросить цену</div></td>
                                    <?endif;?>
                                    <td><?=$arResult["STAFFS"][$val[0]]["NAME"];?></td>
                                    <td><?=$val[0];?></td>
                                    <td><?=$arResult["STAFFS"][$val[0]]["PROPERTY_PRODUCERID_VALUE"];?></td>
                                    <td>

                                        <?if( !empty($arResult["STAFFS"][$val[0]]["AVAILABLE"]) ) {?>
                                            <span class="favorite-status <?=$arResult["STAFFS"][$val[0]]["QUANTITY"] ? 'stock' : 'out'?>">
                                                <?=$arResult["STAFFS"][$val[0]]["QUANTITY"] ? 'В наличии '.$arResult["STAFFS"][$val[0]]["QUANTITY"].' шт.' : 'Нет в наличии'?>
                                            </span>
                                        <? } else { ?>
                                            <span class="favorite-status out">Товар недоступен</span>
                                        <? } ?>

                                    </td>
                                    <td><?=$val[1];?></td>
                                </tr>
                                <?$arJs[$key] = array("ID" => $arResult["STAFFS"][$val[0]]["ID"], "QUANT" => $val[1], "NAME" => $arResult["STAFFS"][$val[0]]["NAME"], "PRICE" => $priceFlag, "NUM" => $key);?>
                            <?elseif($val[1] == 0):?>
                                <tr>
                                    <td><?=++$key;?></td>
                                    <td><div class="nrecognized">Нулевое количество</div></td>
                                    <td><?=$arResult["STAFFS"][$val[0]]["NAME"];?></td>
                                    <td><?=$val[0];?></td>
                                    <td><?=$arResult["STAFFS"][$val[0]]["PROPERTY_PRODUCERID_VALUE"];?></td>
                                    <td>

                                        <?if( !empty($arResult["STAFFS"][$val[0]]["AVAILABLE"]) ) {?>
                                            <span class="favorite-status <?=$arResult["STAFFS"][$val[0]]["QUANTITY"] ? 'stock' : 'out'?>">
                                                <?=$arResult["STAFFS"][$val[0]]["QUANTITY"] ? 'В наличии '.$arResult["STAFFS"][$val[0]]["QUANTITY"].' шт.' : 'Нет в наличии'?>
                                            </span>
                                        <? } else { ?>
                                            <span class="favorite-status out">Товар недоступен</span>
                                        <? } ?>

                                    </td>
                                    <td><?=$val[1];?></td>
                                </tr>
                            <?elseif($val[1] < 0):?>
                                <tr>
                                    <td><?=++$key;?></td>
                                    <td><div class="nrecognized">Отрицательное количество</div></td>
                                    <td><?=$arResult["STAFFS"][$val[0]]["NAME"];?></td>
                                    <td><?=$val[0];?></td>
                                    <td><?=$arResult["STAFFS"][$val[0]]["PROPERTY_PRODUCERID_VALUE"];?></td>
                                    <td>

                                        <?if( !empty($arResult["STAFFS"][$val[0]]["AVAILABLE"]) ) {?>
                                            <span class="favorite-status <?=$arResult["STAFFS"][$val[0]]["QUANTITY"] ? 'stock' : 'out'?>">
                                                <?=$arResult["STAFFS"][$val[0]]["QUANTITY"] ? 'В наличии '.$arResult["STAFFS"][$val[0]]["QUANTITY"].' шт.' : 'Нет в наличии'?>
                                            </span>
                                        <? } else { ?>
                                            <span class="favorite-status out">Товар недоступен</span>
                                        <? } ?>

                                    </td>
                                    <td><?=$val[1];?></td>
                                </tr>
                            <?endif;?>

                            <?$countYes++;?>
                        <?else:?>
                            <tr>
                                <td><?=++$key;?></td>
                                <td><div class="nrecognized">Не распознано</div></td>
                                <td><?=$arResult["STAFFS"][$val[0]]["NAME"];?></td>
                                <td><div class="vendor-code"><?=$val[0];?></div></td>
                                <td><?=$arResult["STAFFS"][$val[0]]["PROPERTY_PRODUCERID_VALUE"];?></td>
                                <td></td>
                                <td><?=$val[1];?></td>
                            </tr>
                            <?$countNo++;?>
                        <?endif;?>

                    <?endforeach;?>

                </table>
                <div class="bom-footer">
                    <div class="bom-status">
                        <div class="bom-added">Распознано: <?=intval($countYes);?></div>
                        <div class="bom-unrecognized">Не распознано: <?=intval($countNo);?></div>
                    </div>
                    <div class="bom-buttons" id="bom-buttons-panel">
                        <a href="" class="default-button cancel">Отмена</a>
                        <a  class="default-button add-to-cart" onclick="addStaffAll(false);" id="button_add">Добавить в корзину</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script>
        var countArrP = "<?=intval($countYes);?>";
        function addStaff(number, id, quant, baskId, name, price) {

            var url = "/local/components/mcart/mcart.bom/ajax.php?id=" + id + "&quant=" + quant + "&name=" + name + "&price=" + price;

            BX.ajax.get(
                url,
                function(data){
                    if(baskId != false){
                        if(document.getElementById(baskId)){
                            document.getElementById(baskId).innerHTML = data;
                        }
                    }

			        //console.log(data);
                    if(data != 0) {
                        document.getElementById("staff_"+number).innerHTML = "<div class='nrecognized'>Добавлено</div>";
                        delete arJs[number];
                        countArrP--;
                        if(countArrP <= 0){
                            document.getElementById("button_add").remove();
                        }
                    }
                }
            );

        }

        function loading() {
            var timer = setInterval(function(){
                if(document.getElementById("button_add")){
                    //console.log(document.getElementById("button_add").innerHTML);
                    if(document.getElementById("button_add").innerHTML == 'Загрузка'){
                        document.getElementById("button_add").innerHTML = 'Загрузка.'
                    }
                    else if(document.getElementById("button_add").innerHTML == 'Загрузка.'){
                        document.getElementById("button_add").innerHTML = 'Загрузка..'
                    }
                    else if(document.getElementById("button_add").innerHTML == 'Загрузка..'){
                        document.getElementById("button_add").innerHTML = "Загрузка..."
                    }
                    else if(document.getElementById("button_add").innerHTML == 'Загрузка...'){
                        document.getElementById("button_add").innerHTML = "Загрузка"
                    }
                } else {
                    clearInterval(timer);
                }
            }, 1000);
        }

        var arJs = <?=CUtil::PhpToJSObject($arJs, false, true)?>;
        //console.log(arJs);

        function addStaffAll(baskId) {

            //console.log("ddddddddd");
            //console.log(id);
            //console.log(quant);
            //console.log("ssssssssssssssssss");

            var url = "/local/components/mcart/mcart.bom/ajax.php";

            var portion = 50;
            var countArr = Object.keys( arJs ).length;
            //console.log(countArr);
            if(countArr <= portion) {

                var objAjax = {};
                objAjax["ALL"] = "Y";

                objAjax["PROD"] = arJs;

                BX.ajax.post(
                    url,
                    objAjax,
                    function (data) {
                        if (baskId != false) {
                            if (document.getElementById(baskId)) {
                                document.getElementById(baskId).innerHTML = data;
                            }
                        }

                        //console.log(arJs);
                        if (data != 0) {
                            for (var prod in arJs) {
                                //console.log(arJs[prod]);
                                document.getElementById("staff_" + arJs[prod].NUM).innerHTML = "<div class='nrecognized'>Добавлено</div>";
                            }
                            document.getElementById("button_add").style.display = "none";
                        }
                    }
                );
            } else {
                var countPortion = 0;
                var countProdInPortion = 0;
                var odjAjaxAll = {};
                odjAjaxAll[countPortion] = {};
                odjAjaxAll[countPortion]["ALL"] = "Y";
                odjAjaxAll[countPortion]["PROD"] = {};
                for(var index in arJs){
                    countProdInPortion++;
                    odjAjaxAll[countPortion]["PROD"][index] = arJs[index];
                    if(countProdInPortion >= portion){
                        countProdInPortion = 0;
                        countPortion++;
                        odjAjaxAll[countPortion] = {};
                        odjAjaxAll[countPortion]["ALL"] = "Y";
                        odjAjaxAll[countPortion]["PROD"] = {};
                    }
                }

                //console.log(odjAjaxAll);
                //var wait = BX.showWait('button_add');
                document.getElementById("bom-buttons-panel").innerHTML = '<a href="" class="default-button cancel">Отмена</a> <div  class="default-button add-to-cart fix-w" id="button_add">Загрузка</div>';
                loading();
                reqursionAjax(odjAjaxAll, 0, url, baskId, countPortion)

                /*for(var portionIndex in odjAjaxAll){
                    BX.ajax.post(
                        url,
                        odjAjaxAll[portionIndex],
                        function (data) {
                            if (baskId != false) {
                                if (document.getElementById(baskId)) {
                                    document.getElementById(baskId).innerHTML = data;
                                }
                            }

                            //console.log(arJs);
                            if (data != 0) {
                                for (var prod in odjAjaxAll[portionIndex]["PROD"]) {
                                    //console.log(arJs[prod]);
                                    document.getElementById("staff_" + odjAjaxAll[portionIndex]["PROD"][prod].ID).innerHTML = "<div class='nrecognized'>Добавлено</div>";
                                }

                                //if(countPortion == portionIndex) {
                                //    document.getElementById("button_add").remove();
                                //}
                            }
                        }
                    );
                }*/

            }

        }

        function reqursionAjax(odjAjaxAll, portionIndex, url, baskId, countPortion) {
            //console.log(portionIndex);
            BX.ajax.post(
                url,
                odjAjaxAll[portionIndex],
                function (data) {
                    if (baskId != false) {
                        if (document.getElementById(baskId)) {
                            document.getElementById(baskId).innerHTML = data;
                        }
                    }

                    //console.log(arJs);
                    if (data != 0) {
                        for (var prod in odjAjaxAll[portionIndex]["PROD"]) {
                            //console.log(arJs[prod]);
                            document.getElementById("staff_" + odjAjaxAll[portionIndex]["PROD"][prod].NUM).innerHTML = "<div class='nrecognized'>Добавлено</div>";
                        }

                        if(countPortion == portionIndex) {
                            document.getElementById("button_add").style.display = "none";
                            //BX.closeWait('button_add', wait);
                        } else {
                            portionIndex = portionIndex + 1;
                            reqursionAjax(odjAjaxAll, portionIndex, url, baskId, countPortion)
                        }
                    }
                }
            );
        }


    </script>

<?endif?>
