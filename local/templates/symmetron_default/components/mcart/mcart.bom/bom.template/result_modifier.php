<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//echo "<pre>";
//print_r($arResult);
//echo "</pre>";
CModule::IncludeModule("iblock");
$arProdIDs = array();
$arIDs = array();
foreach ($arResult["STAFFS"] as $staff){
    $arProdIDs[] = $staff["PROPERTY_PRODUCERID_VALUE"];
    $arIDs[] = $staff["ID"];
}

if(!empty($arProdIDs)) {
    $arSelect = Array("ID", "NAME");
    $arFilter = Array("IBLOCK_ID" => 11, "ID" => $arProdIDs);

    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    $arAllProd = array();
    while ($arPr = $res->Fetch()) {
        $arAllProd[$arPr["ID"]] = $arPr["NAME"];
    }
}

//get prices
if(!empty($arIDs)) {
    Cmodule::IncludeModule("catalog");

    $db_res = CPrice::GetListEx(
        array(),
        array(
            "PRODUCT_ID" => $arIDs,
        )
    );

    $arPrices = array();

    while ($ar_res = $db_res->Fetch()) {

        $arPrices[$ar_res["PRODUCT_ID"]][] = $ar_res;

    }
}

foreach ($arResult["STAFFS"] as $key => $staff){
    $arResult["STAFFS"][$key]["PROPERTY_PRODUCERID_VALUE"] = $arAllProd[$staff["PROPERTY_PRODUCERID_VALUE"]];
    $arResult["STAFFS"][$key]["PRICE"] = $arPrices[$staff["ID"]];
}
