<?php
$prefix = "SYMMETRON_SEARCH_COMPONENT_TEMPLATE_";
$MESS[$prefix.""] = "";

$MESS[$prefix."CATEGORY_TITLE"] = "Категории";
$MESS[$prefix."CATEGORY_PLH"] = "Компонент, производитель, решение";
$MESS[$prefix."CATEGORY_ALL"] = "на всем сайте";
$MESS[$prefix."SEARCH_START"] = "Искать ";

$MESS[$prefix."CATEGORY_0_TITLE"] = "в Каталоге";
$MESS[$prefix."CATEGORY_1_TITLE"] = "в Новостях";
$MESS[$prefix."CATEGORY_2_TITLE"] = "в Производителях";
$MESS[$prefix."CATEGORY_3_TITLE"] = "в Документах";


