<?
$prefix = "SYMMETRON_SEARCH_COMPONENT_TEMPLATE_MORE_";

$MESS[$prefix."CATALOG"] = "Все результаты в Каталоге";
$MESS[$prefix."NEWS"] = "Все результаты в Новостях";
$MESS[$prefix."BRANDS"] = "Все результаты в Производителях";
$MESS[$prefix."DOC"] = "Все результаты в Документах";
$MESS[$prefix."ALL"] = "Все результаты";