$('.select-wrapper').on('click', function(event){
    event.preventDefault();
    if ($('.search-select').hasClass('open')) {
        $('.search-select').removeClass('open')
    } else {
        $('.search-select').addClass('open')
    }
});

// $('.select-list').find("input:checked").parent().trigger('click');

var curCat;
$('.select-list').children("span").on('click', function(event) {
	//event.preventDefault();

    var id = $(this).children('input').val();
    if( curCat == id) return;
    curCat = id;
    
    //var selectedText = $(this).children('label').text();
    var selectedText = $(this).children('label').attr("data-val");
    $('.select-category').text(selectedText)

    $('.search-select').removeClass('open')
	//event.stopPropognation();

    var url = document.location.pathname
    var params = [];
    if (document.location.search.length > 0) {
        params = document.location.search.substr(1);
        params = params.split("&");
        for (var i = 0; i < params.length;)
        {
            var p = params[i].split("=");
            if (p[0] === "category") {
                params.splice(i, 1)
                continue;
            }
            i++;
        }
    }
    params.push("category=" + id);
    params = params.join("&");
    history.pushState(null, null, url+"?"+params);
    Symmetron_search_title.arParams.AJAX_PAGE = url+"?"+params;
    Symmetron_search_title.cache = [];
});