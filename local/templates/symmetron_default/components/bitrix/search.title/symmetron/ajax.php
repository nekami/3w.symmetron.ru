<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult["CATEGORIES"]))
	return;

$category = "all";
if (!empty($_REQUEST["category"]))
    $category = $_REQUEST["category"];
if ($category !== "all") {
    for ($i = 0; $i < $arParams["NUM_CATEGORIES"]; $i++)
    {
        if (!empty($arParams["CATEGORY_".$i][0]) && $category === $i)
            break;
        if ($arParams["CATEGORY_".$i][0] === $category)
        {
            $category = $i;
            break;
        }
    }
}



function getArraySectionsForCatalogCategory($arCategory){
    $arProductsIDs = [];
    $arSectionsIDs = [];
    foreach ($arCategory["ITEMS"] as $arItem){
        if($arItem["ITEM_ID"][0] == "S")
            $arSectionsIDs[] = preg_replace('~[^0-9]+~','', $arItem["ITEM_ID"]);
        else
            $arProductsIDs[] = $arItem["ITEM_ID"];
    }

    $arAllSectionsCatalog = [];
    $arSectionFirstLevelIDs = [];
    if(CModule::IncludeModule("iblock")) {

        if(!empty($arProductsIDs)) {
            $rsElements = Bitrix\Iblock\ElementTable::getList(array(
                "filter" => ["ID" => $arProductsIDs],
                "select" => ["ID", "IBLOCK_SECTION_ID"]
            ));

            while ($arElement = $rsElements->Fetch()) {
                $arSectionFirstLevelIDs[$arElement["ID"]] = $arElement["IBLOCK_SECTION_ID"];
            }

            if(!empty($arSectionFirstLevelIDs)) {
                $rsSections = Bitrix\Iblock\SectionTable::getList(array(
                    "filter" => ["ID" => $arSectionFirstLevelIDs],
                    "select" => ["ID", "NAME", "IBLOCK_SECTION_ID"]
                ));

                $arSectionsSecondLevelIDs = [];
                while ($arSection = $rsSections->Fetch()) {
                    $arAllSectionsCatalog[$arSection["ID"]] = $arSection;
                    $arSectionsSecondLevelIDs[] = $arSection["IBLOCK_SECTION_ID"];
                }

                if(!empty($arSectionsSecondLevelIDs)) {
                    $rsSections = Bitrix\Iblock\SectionTable::getList(array(
                        "filter" => ["ID" => $arSectionsSecondLevelIDs],
                        "select" => ["ID", "NAME", "IBLOCK_SECTION_ID"]
                    ));

                    while ($arSection = $rsSections->Fetch()) {
                        $arAllSectionsCatalog[$arSection["ID"]] = $arSection;
                    }
                }
            }
        }


        if(!empty($arSectionsIDs)) {


            $rsSections = Bitrix\Iblock\SectionTable::getList(array(
                "filter" => ["ID" => $arSectionsIDs],
                "select" => ["ID", "NAME", "IBLOCK_SECTION_ID"]
            ));

            $arSectionsFirstLevelIDsForSec = [];
            $arSectionsSecondLevelIDsForSec = [];
            while ($arSection = $rsSections->Fetch()) {
                $arAllSectionsCatalog[$arSection["ID"]] = $arSection;
                $arSectionsFirstLevelIDsForSec[] = $arSection["IBLOCK_SECTION_ID"];
            }

            if(!empty($arSectionsFirstLevelIDsForSec)) {
                $rsSections = Bitrix\Iblock\SectionTable::getList(array(
                    "filter" => ["ID" => $arSectionsFirstLevelIDsForSec],
                    "select" => ["ID", "NAME", "IBLOCK_SECTION_ID"]
                ));

                while ($arSection = $rsSections->Fetch()) {
                    $arAllSectionsCatalog[$arSection["ID"]] = $arSection;
                    $arSectionsSecondLevelIDsForSec[] = $arSection["IBLOCK_SECTION_ID"];
                }

                if(!empty($arSectionsSecondLevelIDsForSec)){
                    $rsSections = Bitrix\Iblock\SectionTable::getList(array(
                        "filter" => ["ID" => $arSectionsSecondLevelIDsForSec],
                        "select" => ["ID", "NAME", "IBLOCK_SECTION_ID"]
                    ));

                    while ($arSection = $rsSections->Fetch()) {
                        $arAllSectionsCatalog[$arSection["ID"]] = $arSection;
                    }
                }

            }

        }
    }

    return [$arAllSectionsCatalog, $arSectionFirstLevelIDs];

}

function getPathForPastSection($path){
    $arPath = explode("/", $path);
    $lenArPath = count($arPath);
    unset($arPath[$lenArPath - 2]);

    return implode("/", $arPath);
}

function getArrayDateForNewsCategory($arCategory){
    $arNewsIDs = [];
    foreach ($arCategory["ITEMS"] as $arItem){
        $arNewsIDs[] = $arItem["ITEM_ID"];
    }

    $arDateNews = [];

    if(!empty($arNewsIDs)) {
        if(CModule::IncludeModule("iblock")) {
            $rsNews = \CIBlockElement::GetList([], ["ID" => $arNewsIDs], false, false, ["ID", "PROPERTY_DATE"]);

            while ($arNew = $rsNews->Fetch()) {

                $arDateNews[$arNew["ID"]] = \CIBlockFormatProperties::DateFormat(
                    "j F Y",
                    MakeTimeStamp($arNew["PROPERTY_DATE_VALUE"], \CSite::GetDateFormat())
                );

            }
        }
    }

    return $arDateNews;
}

function getArrayTextForBrandCategory($arCategory){
    $arBrandIDs = [];
    foreach ($arCategory["ITEMS"] as $arItem){
        $arBrandIDs[] = $arItem["ITEM_ID"];
    }

    $arBrandText = [];

    if(!empty($arBrandIDs)) {
        if(CModule::IncludeModule("iblock")) {
            $rsBrands = \CIBlockElement::GetList([], ["ID" => $arBrandIDs], false, false, ["ID", "PROPERTY_SEARCH_TEXT"]);

            while ($arBrand = $rsBrands->Fetch()) {
                $arBrandText[$arBrand["ID"]] = $arBrand["PROPERTY_SEARCH_TEXT_VALUE"];
            }
        }
    }

    return $arBrandText;
}

function strPosAll($haystack, $needle) {
    $offset = 0;
    $allPos = [];
    while (($pos = stripos($haystack, $needle, $offset)) !== false) {
        $offset   = $pos + 1;
        $allPos[] = $pos;
    }
    return $allPos;
}


function getArrayTextCatalogProductsInfo($arProducts, $q){
    $arProductsIDs = [];
    foreach ($arProducts["ITEMS"] as $arProduct){
        $arProductsIDs[] = $arProduct["ITEM_ID"];
    }

    $arProductsInfo = [];

    if(!empty($arProductsIDs)) {
        if(CModule::IncludeModule("iblock")) {
            $rsProducts = \CIBlockElement::GetList([], ["ID" => $arProductsIDs], false, false, ["ID", "NAME", "PROPERTY_ARTICLE", "PROPERTY_SERIES", "PROPERTY_DESCRIPTION"]);

            while ($arProduct = $rsProducts->Fetch()) {

                $resultString = $arProduct["NAME"];

                $dopString = "";
                if(strlen(trim($arProduct["PROPERTY_ARTICLE_VALUE"])) > 0 && trim($arProduct["NAME"]) != trim($arProduct["PROPERTY_ARTICLE_VALUE"]))
                    $dopString .= trim($arProduct["PROPERTY_ARTICLE_VALUE"]);

                if(strlen(trim($arProduct["PROPERTY_SERIES_VALUE"])) > 0
                    && trim($arProduct["NAME"]) != trim($arProduct["PROPERTY_SERIES_VALUE"])
                    && trim($arProduct["PROPERTY_SERIES_VALUE"]) != trim($arProduct["PROPERTY_ARTICLE_VALUE"])
                )
                    $dopString .= ", " . trim($arProduct["PROPERTY_SERIES_VALUE"]);

                if(strlen(trim($arProduct["PROPERTY_DESCRIPTION_VALUE"])) > 0
                    && trim($arProduct["NAME"]) != trim($arProduct["PROPERTY_DESCRIPTION_VALUE"])
                    && trim($arProduct["PROPERTY_DESCRIPTION_VALUE"]) != trim($arProduct["PROPERTY_ARTICLE_VALUE"])
                    && trim($arProduct["PROPERTY_DESCRIPTION_VALUE"]) != trim($arProduct["PROPERTY_SERIES_VALUE"])
                )
                    $dopString .= ", " . trim($arProduct["PROPERTY_DESCRIPTION_VALUE"]);

                if(strlen($dopString) > 0)
                    $resultString .= " (" . $dopString . ")";

                $tmpString = $resultString;

                $qLen = strlen($q);

                $arSubStrUsed = [];
                foreach (strPosAll($tmpString, $q) as $pos){
                    $subStr = substr($tmpString, $pos, $qLen);
                    if(strlen($subStr) > 0 && !in_array($subStr, $arSubStrUsed))
                        $resultString = str_replace($subStr, "<b>" . $subStr . "</b>", $resultString);

                    $arSubStrUsed[] = $subStr;
                }
                $arProductsInfo[$arProduct["ID"]] = $resultString;
            }
        }
    }

    return $arProductsInfo;
}


function printShowMore($allPath, $arCategory, $categoryId){

    if(reset($arCategory["ITEMS"])["PARAM1"] == "catalog"){
        $allPath .= "&category=iblock_catalog";
    }

    if(reset($arCategory["ITEMS"])["PARAM1"] == "news"){
        $allPath .= "&category=iblock_news";
    }

    if(reset($arCategory["ITEMS"])["PARAM1"] == "lists"){
        $allPath .= "&category=iblock_lists";
    }

    if(reset($arCategory["ITEMS"])["PARAM1"] == "documents"){
        $allPath .= "&category=iblock_documents";
    }
    ?>

    <a href="<?=$allPath?>" class="all-results">
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve">
          <g>
              <g id="arrow-back">
                  <path d="M408,178.5H96.9L239.7,35.7L204,0L0,204l204,204l35.7-35.7L96.9,229.5H408V178.5z"></path>
              </g>
          </g>

        </svg>
        <span>
            <?

            $prefix = "SYMMETRON_SEARCH_COMPONENT_TEMPLATE_MORE_";

            if(reset($arCategory["ITEMS"])["PARAM1"] == "catalog"){
                echo GetMessage($prefix."CATALOG");
            }

            if(reset($arCategory["ITEMS"])["PARAM1"] == "news"){
                echo GetMessage($prefix."NEWS");
            }

            if(reset($arCategory["ITEMS"])["PARAM1"] == "lists"){
                echo GetMessage($prefix."BRANDS");
            }

            if(reset($arCategory["ITEMS"])["PARAM1"] == "documents"){
                echo GetMessage($prefix."DOC");
            }

            if($categoryId === "all"){
                echo GetMessage($prefix."ALL");
            }
            ?>
        </span>
    </a>
<?
}




foreach ($arResult["CATEGORIES"] as $categoryId => $arCategory){
    //get sections for catalog

    if(reset($arCategory["ITEMS"])["PARAM1"] == "catalog"){

        //products go down, sections go up

        usort($arCategory["ITEMS"], function($a, $b){
            return ($a['ITEM_ID'] < $b['ITEM_ID']);
        });
        $arResult["CATEGORIES"][$categoryId] = $arCategory;

        $arParsCatalogCategory = getArraySectionsForCatalogCategory($arCategory);
        $arAllSectionsCatalog = $arParsCatalogCategory[0];
        $arSectionFirstLevelIDs = $arParsCatalogCategory[1];

        $arCatalogProducts = getArrayTextCatalogProductsInfo($arCategory, $arResult["query"]);

    }

    if(reset($arCategory["ITEMS"])["PARAM1"] == "news"){

        $arDateNews = getArrayDateForNewsCategory($arCategory);

    }

    if(reset($arCategory["ITEMS"])["PARAM1"] == "lists"){

        $arBrandsTexts = getArrayTextForBrandCategory($arCategory);
    }

}


$urlAll = $arResult["CATEGORIES"]["all"]["ITEMS"][0]["URL"];
?>
<?foreach($arResult["CATEGORIES"] as $categoryId => $arCategory):?>
    <?if ($category === "all" || $categoryId === "all" || $category == $categoryId):?>
        <div class="search-category">
            <?if ($categoryId !== "all"):?>
                <div class="search-category__header"><?=$arCategory["TITLE"]?></div>
            <?endif;?>
			<div class="search-list">
				<ul class="search-category__list">
                    <?$flagShowMore = false;?>
                    <?if ($categoryId !== "all"):?>
                        <?foreach($arCategory["ITEMS"] as $i => $arItem):?>
                            <?if($arItem["TYPE"] == "all"):?>
                                <?$flagShowMore = true;?>
                                <?continue;?>
                            <?endif;?>
                            <li>
                                <?if($arItem["PARAM1"] == "catalog" && $arItem["ITEM_ID"][0] != "S"):?>
                                    <a class="search-category__link" href="<?echo $arItem["URL"]?>"><?echo $arCatalogProducts[$arItem["ITEM_ID"]];?></a>
                                <?else:?>
                                    <a class="search-category__link" href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a>
                                <?endif;?>

                                <!--create section path-->
                                <?if($arItem["PARAM1"] == "catalog"):?>

                                    <?if($arItem["ITEM_ID"][0] != "S"):?>
                                        <?$section = "";
                                        if(!empty($arSectionFirstLevelIDs[$arItem["ITEM_ID"]])) {
                                            $idSection = $arSectionFirstLevelIDs[$arItem["ITEM_ID"]];
                                            $section = " / " . $arAllSectionsCatalog[$idSection]["NAME"];
                                            if(!empty($arAllSectionsCatalog[$idSection]["IBLOCK_SECTION_ID"])){
                                                $idSectionSecond = $arAllSectionsCatalog[$idSection]["IBLOCK_SECTION_ID"];
                                                $section = "/ " . $arAllSectionsCatalog[$idSectionSecond]["NAME"] . $section;
                                                if(!empty($arAllSectionsCatalog[$idSectionSecond]["IBLOCK_SECTION_ID"])){
                                                    $section = "... " . $section;
                                                }
                                            }

                                        }?>
                                    <?else:?>
                                        <?$section = "";
                                        $sectionId = preg_replace('~[^0-9]+~','', $arItem["ITEM_ID"]);

                                        $firstParentSection = $arAllSectionsCatalog[$sectionId]["IBLOCK_SECTION_ID"];
                                        if(!empty($arAllSectionsCatalog[$firstParentSection])) {
                                            $section = " / " . $arAllSectionsCatalog[$firstParentSection]["NAME"];
                                            if(!empty($arAllSectionsCatalog[$firstParentSection]["IBLOCK_SECTION_ID"])){
                                                $idSectionSecond = $arAllSectionsCatalog[$firstParentSection]["IBLOCK_SECTION_ID"];
                                                $section = "/ " . $arAllSectionsCatalog[$idSectionSecond]["NAME"] . $section;
                                                if(!empty($arAllSectionsCatalog[$idSectionSecond]["IBLOCK_SECTION_ID"])){
                                                    $section = "... " . $section;
                                                }
                                            }

                                        }?>
                                    <?endif;?>

                                    <a class="search-category__path" href="<?=getPathForPastSection($arItem["URL"])?>"><?=$section?></a>
                                    <?/*<a class="search-category__path" href="<?echo $arItem["URL"]?>">… / Компоненты для идентификации и аутентификации / Антенны для RFID</a>*/?>
                                <?endif;?>

                                <?if($arItem["PARAM1"] == "news"):?>
                                    <?if(!empty($arDateNews[$arItem["ITEM_ID"]])):?>
                                        <a class="search-category__path" href="<?echo $arItem["URL"]?>"><?=$arDateNews[$arItem["ITEM_ID"]]?></a>
                                    <?endif;?>
                                <?endif;?>

                                <?if($arItem["PARAM1"] == "lists"):?>
                                    <?if(!empty($arBrandsTexts[$arItem["ITEM_ID"]])):?>
                                        <a class="search-category__path" href="<?echo $arItem["URL"]?>"><?=$arBrandsTexts[$arItem["ITEM_ID"]]?></a>
                                    <?endif;?>
                                <?endif;?>

                            </li>
                        <?endforeach;?>
                    <?else:?>
                        <?$flagShowMore = true;?>
                    <?endif;?>
                    <?if($flagShowMore):?>
                        <?printShowMore($urlAll, $arCategory, $categoryId)?>
                    <?endif;?>
				</ul>
			</div>
        </div>
    <?endif;?>
<?endforeach;?>