<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$prefix = "SYMMETRON_SEARCH_COMPONENT_TEMPLATE_";
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);
$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>
    <div id="<?echo $CONTAINER_ID?>" class="header-bot__search">
        <form action="<?echo $arResult["FORM_ACTION"]?>">
            <input id="<?echo $INPUT_ID?>" type="search"  maxlength="100" name="q" value="<?=htmlspecialcharsbx($_REQUEST["q"])?>" autocomplete="off" placeholder="<?=Loc::GetMessage($prefix."CATEGORY_PLH")?>">
            <?
            $style = "";
            if ($arParams["NUM_CATEGORIES"] < 1)
                $style = "style='display:none'";
            $category = "all";
            if ( isset($_REQUEST["category"]) && $_REQUEST["category"] !== 'all')
                $category = $_REQUEST["category"];
            ?>

            <div class="search-select" <?=$style?>>
				<?if ($category === "all"):?>
					<div class="select-wrapper">
						<span class="select-searchIn"><?=Loc::GetMessage($prefix."SEARCH_START")?></span>
						<span class="select-category"><?=Loc::GetMessage($prefix."CATEGORY_ALL")?></span>
					</div>
					<?else:?>
					<?for ($i = 0; $i < $arParams["NUM_CATEGORIES"]; $i++):?>
						<?if (empty($arParams["CATEGORY_".$i][0]) && $category !== $i
							|| $arParams["CATEGORY_".$i][0] !== $category) continue;?>
                        <div class="select-wrapper">
                            <span class="select-searchIn"><?=Loc::GetMessage($prefix."SEARCH_START")?></span>
                            <span class="select-category"><?=Loc::GetMessage($prefix."CATEGORY_".$i."_TITLE")?:$arParams["CATEGORY_".$i."_TITLE"]?></span>
                        </div>
					<?endfor;?>
				<?endif;?>
				<div class="select-list">
					<span>
						<label for="all-category" data-val="<?=Loc::GetMessage($prefix."CATEGORY_ALL")?>"><?=Loc::GetMessage($prefix."CATEGORY_ALL")?></label>
						<input type="radio" name="category" id="all-category" value="all" <?= $category === 'all' ? "checked":''?> />
					</span>
					<?for ($i = 0; $i < $arParams["NUM_CATEGORIES"]; $i++):?>
					<? $val = empty($arParams["CATEGORY_".$i][0]) ? $i : $arParams["CATEGORY_".$i][0];?>
					<span>
						<label for="<?=$i?>-category" data-val="<?=Loc::GetMessage($prefix."CATEGORY_".$i."_TITLE")?:$arParams["CATEGORY_".$i."_TITLE"]?>"><?=$arParams["CATEGORY_".$i."_TITLE"]?></label>
						<input type="radio" name="category" id="<?=$i?>-category" value="<?=$val?>" <?= ($i===$category || $val===$category) ? "checked" : ""?>>
					</span>
					<?endfor;?>
				</div>
            </div>
            <button class="search-submit" type="submit">
				<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					viewBox="0 0 410.23 410.23" style="enable-background:new 0 0 410.23 410.23;"
					 xml:space="preserve">
				<g>
					<path d="M401.625,364.092l-107.1-107.1c19.125-26.775,30.6-59.288,30.6-93.713c0-89.888-72.675-162.562-162.562-162.562
						S0,73.392,0,163.279s72.675,162.562,162.562,162.562c34.425,0,66.938-11.475,93.713-30.6l107.1,107.1
						c9.562,9.562,26.775,9.562,38.25,0l0,0C413.1,390.867,413.1,375.566,401.625,364.092z M162.562,287.592
						c-68.85,0-124.312-55.463-124.312-124.312c0-68.85,55.462-124.312,124.312-124.312c68.85,0,124.312,55.462,124.312,124.312
						C286.875,232.129,231.412,287.592,162.562,287.592z"/>
				</g>
				</svg>
			</button>
        </form>
    </div>
<?endif?>
<script>
    BX.ready(function(){
        Symmetron_search_title = new JCTitleSearch({
            'AJAX_PAGE' : '<?echo CUtil::JSEscape(str_replace("&amp;", "&",POST_FORM_ACTION_URI))?>',
            'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
            'INPUT_ID': '<?echo $INPUT_ID?>',
            'MIN_QUERY_LEN': 2
        });
    });
</script>
