<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult["CATEGORIES"]))
	return;

$category = "all";
if (!empty($_REQUEST["category"]) && intval($_REQUEST["category"]) <= $arParams["NUM_CATEGORIES"])
    $category = intval($_REQUEST["category"]);

?>

<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
    <?if ($category === "all" || $category_id === "all" || $category == $category_id):?>
        <div class="search-category">
            <?if ($category_id !== "all"):?>
                <span class="search-category__header"><?=$arCategory["TITLE"]?></span>
            <?endif;?>
            <ul class="search-category__list">
                <?foreach($arCategory["ITEMS"] as $i => $arItem):?>
                    <li><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a></li>
                <?endforeach;?>
            </ul>
        </div>
    <?endif;?>
<?endforeach;?>
