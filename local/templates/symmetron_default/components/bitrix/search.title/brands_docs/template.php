<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$prefix = "SYMMETRON_SEARCH_COMPONENT_TEMPLATE_";
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);
$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);
$curCategory = $_REQUEST['category'];

if($arParams["SHOW_INPUT"] !== "N"):?>
    <div id="<?echo $CONTAINER_ID?>" class="docs-search">
        <div class="docs-serach__wrapper">
            <form action="<?echo $arResult["FORM_ACTION"]?>">
                <input id="<?echo $INPUT_ID?>" type="search" name="q" value="<?=htmlspecialcharsbx($_REQUEST["q"])?>" autocomplete="off" placeholder="<?=Loc::GetMessage($prefix."BRAND_DOCS")?>" >
                <?if (!empty($arParams["DOCS_WHERE"])):?>
                    <input type="hidden" name="where" value="<?=$arParams["DOCS_WHERE"]?>">
                <?endif;?>
                <input type="radio" name="category" id="3-category" value="3" style="display: none" checked>
                <button class="submit-docsearch">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 410.23 410.23" style="enable-background:new 0 0 410.23 410.23;" xml:space="preserve">
                    <g>
                        <path d="M401.625,364.092l-107.1-107.1c19.125-26.775,30.6-59.288,30.6-93.713c0-89.888-72.675-162.562-162.562-162.562
                            S0,73.392,0,163.279s72.675,162.562,162.562,162.562c34.425,0,66.938-11.475,93.713-30.6l107.1,107.1
                            c9.562,9.562,26.775,9.562,38.25,0l0,0C413.1,390.867,413.1,375.566,401.625,364.092z M162.562,287.592
                            c-68.85,0-124.312-55.463-124.312-124.312c0-68.85,55.462-124.312,124.312-124.312c68.85,0,124.312,55.462,124.312,124.312
                            C286.875,232.129,231.412,287.592,162.562,287.592z"></path>
                    </g>
                    </svg>
                </button>
            </form>

        </div>
    </div>
<?endif?>
<script>
    BX.ready(function(){
        Symmetron_search_title = new JCTitleSearch({
            'AJAX_PAGE' : '<?echo CUtil::JSEscape(str_replace("&amp;", "&",POST_FORM_ACTION_URI))?>',
            'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
            'INPUT_ID': '<?echo $INPUT_ID?>',
            'MIN_QUERY_LEN': 2
        });
    });
</script>
