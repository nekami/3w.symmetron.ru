<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$arProducIDs = array();
foreach ($arResult["ITEMS"] as $arProp){
    $arProdIBID = $arProp["PROPERTIES"]["PRODUCERID"]["LINK_IBLOCK_ID"];
    $arProducIDs[] = $arProp["PROPERTIES"]["PRODUCERID"]["VALUE"];

    foreach ($arProp["PROPERTIES"] as $prop){
        
    }

}


if(!empty($arProducIDs)) {
    $res = CIBlockElement::GetList(Array(), array("ID" => $arProducIDs, "IBLOCK_ID" => $arProdIBID), false, false, array("ID", "NAME"));
    $arProducNames = array();
    while ($ob = $res->Fetch()) {
        $arProducNames[$ob["ID"]] = $ob["NAME"];
        //print_r($arFields);
    }
}

if(!empty($arProducNames)) {
    foreach ($arResult["ITEMS"] as $key => $arProp) {
        $arResult["ITEMS"][$key]["PROPERTIES"]["PRODUCERID"]["VALUE"] = $arProducNames[$arProp["PROPERTIES"]["PRODUCERID"]["VALUE"]];
    }
} else {
    foreach ($arResult["ITEMS"] as $key => $arProp) {
        $arResult["ITEMS"][$key]["PROPERTIES"]["PRODUCERID"]["VALUE"] = "";
    }
}


//echo "<pre>";
//print_r($templateData);
//print_r($arResult);
//echo "</pre>";
CJSCore::Init(["currency"]);
foreach ($arResult["ITEMS"] as $key => $arItem) {
    if (!empty($arParams["PROPERTY_CODE_FAST_SHOW_LEFT"])) {
        $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_LEFT"] = array();
        foreach ($arParams["PROPERTY_CODE_FAST_SHOW_LEFT"] as $code) {
            if (!empty($code)) {

                $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_LEFT"][] = $arItem["PROPERTIES"][$code];

            }

        }
    }
    
}


foreach ($arResult["ITEMS"] as $key => $arItem) {
    if (!empty($arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"])) {
        $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_RIGHT"] = array();
        foreach ($arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $code) {
            if (!empty($code)) {

                $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_RIGHT"][] = $arItem["PROPERTIES"][$code];

            }

        }
    }
}

//CUserOptions::SetOption("catalog.section", "all_prod", "");

//echo "<pre>";
//print_r($_GET["OPTIONS_MODAL"]);
//echo "</pre>";

foreach(CIBlockSectionPropertyLink::GetArray($arResult["IBLOCK_ID"], $arResult["ID"]) as $PID => $arLink)  {
    //if($arLink["SMART_FILTER"] !== "Y")
    //    continue;

    $arPropAval[] = $PID;
}

$arResult["PROP_AVAL"] = $arPropAval;

global $USER;
if($USER->IsAuthorized()) {
    if (!empty($_POST["OPTIONS_MODAL"])) {
        $aGridOptionsDef = $_POST["OPTIONS_MODAL"];
        CUserOptions::SetOption("catalog.section", $arResult["ID"], $aGridOptionsDef, false, $USER->GetID());

    } else {
        $aGridOptionsDef = CUserOptions::GetOption("catalog.section", $arResult["ID"], false, $USER->GetID());
    }

    $arResult["CHANGE_OPTIONS"] = array();
    if (empty($aGridOptionsDef)) {
        $arResult["CHANGE_OPTIONS"] = array("PRODUCERID", "ARTICLE");
    } else {
        $arResult["CHANGE_OPTIONS"] = $aGridOptionsDef;
    }
} else {
    if (!empty($_POST["OPTIONS_MODAL"])) {
        $aGridOptionsDef = $_POST["OPTIONS_MODAL"];
        $_SESSION["catalog.section"][$arResult["ID"]] = $aGridOptionsDef;

    } else {
        $aGridOptionsDef = $_SESSION["catalog.section"][$arResult["ID"]];
    }

    $arResult["CHANGE_OPTIONS"] = array();
    if (empty($aGridOptionsDef)) {
        $arResult["CHANGE_OPTIONS"] = array("PRODUCERID", "ARTICLE");
    } else {
        $arResult["CHANGE_OPTIONS"] = $aGridOptionsDef;
    }
}
//$arResult["CHANGE_OPTIONS"] = $arParams["PROPERTY_CODE"];
/*
foreach ($arParams["PROPERTY_CODE"] as $rrr) {
    $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "CODE" => $rrr));
    while ($prop_fields = $properties->GetNext()) {
        $arResult['PROPS_FOR_OPTIONS'][] = $prop_fields;
    }
}
echo "<pre>";
print_r($arResult["PROPS_FOR_OPTIONS"]);
echo "</pre>";
*/



    /*if (!empty($arResult["ITEMS"])) {
        foreach ($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"] as $prop) {

            if (!empty($prop)) {
                $props = $arResult["ITEMS"][0]["PROPERTIES"];
                if (!empty($props[$prop]["NAME"]))
                    $arResult["GRID"]["NAMES_PROP"][] = array('id' => $props[$prop]["CODE"], 'name' => $props[$prop]["NAME"], 'sort' => $props[$prop]["SORT"], 'default' => true);

            }
        }


        foreach ($arResult["ITEMS"] as $item) {
            $arResult["GRID"]["PROD_NAMES"][] = $item["NAME"];
            $arValProps = array();

            foreach ($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"] as $prop) {
                if (!empty($prop)) {
                    $arValProps["data"][$prop] = $item["DISPLAY_PROPERTIES"][$prop]["VALUE"];
                }
            }

            $arResult["GRID"]["VAL_PROP"][] = $arValProps;


            //echo "<pre>";
            //print_r($item);
            //echo "</pre>";


            $arResult["GRID"]["SHOW_IN_MODAL"][] = array("SEC_ID" => $item["IBLOCK_SECTION_ID"], "ELEM_ID" => $item["ID"]);

        }

    }*/

//echo "<pre>";
//print_r($arResult);
//echo "</pre>";