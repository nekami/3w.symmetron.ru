<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

global $USER;
if (!is_object($USER)) $USER = new CUser;
define('AUTH_USER', $USER->IsAuthorized() );
if ( AUTH_USER ){
    define('USER_ID', $USER->GetID() );
    $res = CUser::GetList($by = "id", $order = "asc", ['ID'=>[USER_ID]],['SELECT'=>["UF_FAVS"]])->Fetch();
    $favs = $res["UF_FAVS"];
}

if($_POST[0] == "addelem"):?>
    <?//echo "kkkkkkkkkkkkkkkkkkkkkkkkkkkk";
    //echo "<pre>";
    //print_r($arResult);
    //echo "</pre>";
    //die;?>
    <?$counterCol = 1;?>
    <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
        <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arResult["ITEMS"][0]["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
            <?continue;?>
        <?endif;?>
        <?if(!empty(trim($prop)) && !empty(trim($arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"]))):?>
            <?$counterCol++;?>
        <?endif;?>
    <?endforeach;?>

    <?foreach ($arResult["ITEMS"] as $arItem):?>
        <?

        //$item = $arItem;
        //$item["CAN_BUY"] = true;
        $areaId = $this->GetEditAreaId($arItem['ID']);
        $itemIds = array(
            'ID' => $areaId,
            'PICT' => $areaId.'_pict',
            'SECOND_PICT' => $areaId.'_secondpict',
            'PICT_SLIDER' => $areaId.'_pict_slider',
            'STICKER_ID' => $areaId.'_sticker',
            'SECOND_STICKER_ID' => $areaId.'_secondsticker',
            'QUANTITY' => $areaId.'_quantity',
            'QUANTITY_DOWN' => $areaId.'_quant_down',
            'QUANTITY_UP' => $areaId.'_quant_up',
            'QUANTITY_MEASURE' => $areaId.'_quant_measure',
            'QUANTITY_LIMIT' => $areaId.'_quant_limit',
            'BUY_LINK' => $areaId.'_buy_link',
            'BASKET_ACTIONS' => $areaId.'_basket_actions',
            'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
            'SUBSCRIBE_LINK' => $areaId.'_subscribe',
            'COMPARE_LINK' => $areaId.'_compare_link',
            'PRICE' => $areaId.'_price',
            'PRICE_OLD' => $areaId.'_price_old',
            'PRICE_TOTAL' => $areaId.'_price_total',
            'DSC_PERC' => $areaId.'_dsc_perc',
            'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
            'PROP_DIV' => $areaId.'_sku_tree',
            'PROP' => $areaId.'_prop_',
            'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
            'BASKET_PROP_DIV' => $areaId.'_basket_prop',
        );
        ?>

        <tr class="mini-card__select" >
            <td id = "name_prod_<?=$arItem["ID"];?>"><?=$arItem["NAME"];?></td>

            <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arItem["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
                    <?continue;?>
                <?endif;?>
                <?if(!empty($arItem["PROPERTIES"][$prop]) && !empty(trim($arItem["PROPERTIES"][$prop]["NAME"]))):?>
                    <td><?=$arItem["PROPERTIES"][$prop]["VALUE"];?></td>
                <?endif;?>
            <?endforeach;?>
            <!--<td><span class="analogue-status ordered">Заказано </span></td>-->
        </tr>
        <tr class="mini-card" id="<?=$itemIds["ID"]?>" data-id="<?=$arItem['ID']?>">
            <?//$arItem["DETAIL_PAGE_URL"];?>
            <td  colspan="<?=$counterCol;?>">
                <div class="card-holder">
                    <div class="mini-cart__wrapper">
                        <div class="item-img">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"];?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                            <?
                            $k = empty($favs) ? false : array_search( $arItem["ID"], $favs);
                            $is_favs = $k!==FALSE ? true : false;
                            ?>
                            <span class="favorite <?=$is_favs ? 'act':''?>">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                        </span>
                        </div>
                        <div class="item-info">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="item-info__head" onclick="console.log('no')">
                                <?=$arItem["NAME"].", ".$arItem["PREVIEW_TEXT"];?>
                            </a>
                            <div class="item-info__content">
                                <?if($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"]):?>
                                    <ul>
                                        <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"] as $prop):?>
                                            <?if(!empty(trim($prop["VALUE"]))):?>
                                                <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                            <?endif;?>
                                        <?endforeach;?>
                                    </ul>
                                <?endif;?>
                                <ul>
                                    <?if($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"]):?>
                                        <ul>
                                            <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $prop):?>
                                                <?if(!empty(trim($prop["VALUE"]))):?>
                                                    <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                <?endif;?>
                                            <?endforeach;?>
                                        </ul>
                                    <?endif;?>
                                </ul>
                            </div>
                        </div>
                        <div class="item-more">
                            <?if($arItem["CATALOG_AVAILABLE"] == "Y"):?>
                                <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                <?if($arItem["CATALOG_QUANTITY"] > 0):?>
                                    <span class="avalibilty">В наличии <?=$arItem["CATALOG_QUANTITY"]?> единиц</span>
                                <?else:?>
                                    <span class="avalibilty out">Нет в наличии</span>
                                <?endif;?>

                                <?//if(!empty($arItem["ITEM_PRICES"])):?>
                                <div class="wholesale">
                                    <?foreach ($arItem["ITEM_PRICES"] as $arPrice):?>
                                        <?if(!empty($arPrice["QUANTITY_TO"])):?>
                                            <div class="quantity">
                                                <span class="quantity-items"><?=$arPrice["QUANTITY_FROM"]."-".$arPrice["QUANTITY_TO"]?> штук </span>
                                                <span class="quantity-separator"></span>
                                                <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                            </div>
                                        <?else:?>
                                            <div class="quantity">
                                                <?if(empty($arPrice["QUANTITY_FROM"])){
                                                    $arPrice["QUANTITY_FROM"] = 1;
                                                }?>
                                                <span class="quantity-items">от <?=$arPrice["QUANTITY_FROM"]?> штук </span>
                                                <span class="quantity-separator"></span>
                                                <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                            </div>
                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                                <?//endif;?>


                                <div class="quantity-num">
                                    <span class="quantity-button" id="<? echo $itemIds['QUANTITY_DOWN']; ?>" onclick="quantChange('-', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">–</span>
                                    <input value="1" max="99999" name = "quant_elems_<?=$arItem["ID"];?>" id="<? echo $itemIds['QUANTITY']; ?>" onchange="quantityVal(<?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>)" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
                                    <span class="quantity-button" id="<? echo $itemIds['QUANTITY_UP']; ?>" onclick="quantChange('+', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">+</span>
                                </div>

                                <form action="" onkeydown="if(event.keyCode==13){return false;}">
                                    <?if(!empty($arItem["ITEM_PRICES"])):?>
                                        <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">
                                            <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                        </div>
                                        <div class="price" id="<? echo $itemIds['PRICE_TOTAL']; ?>">
                                            <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                        </div>
                                        <div class="cb"></div>
                                        <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBasket('<?=$arItem["ID"];?>', '');" href="javascript:void(0)" rel="nofollow">Добавить в корзину</a>
                                    <?else:?>
                                        <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">0</div>
                                        <div class="price" style="display: none" id="<? echo $itemIds['PRICE_TOTAL']; ?>">0</div>
                                        <div class="cb"></div>
                                        <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'N');" href="javascript:void(0)" rel="nofollow">Запросить цену</a>
                                    <?endif;?>
                                </form>

                                <a <?if ($USER->IsAuthorized()):?>onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'Y');"<?else:?>onclick="noAuth();"<?endif;?> class="sidebar-sample" href="javascript:void(0)">Запросить образец</a>
                            <?else:?>
                                <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                <span class="avalibilty out">Товар недоступен</span>
                            <?endif;?>
                        </div>
                    </div>
                </div>
            </td>
        </tr>



    <?endforeach;?>
    <?echo "------------============------------";?>
    <?$arCount = $arResult["NAV_RESULT"]->NavRecordCount - $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer;?>
    <?if($arCount > 0):?>
        <button id = "button_elem_q" class="show-all" onclick="ajaxUpdateList('<?=$_GET["sort"];?>', '<?=$_GET["method"]?>', 'addelem', '<?=$arResult["NAV_RESULT"]->NavPageNomer + 1?>');">Показать ещё (<span id="element_count"><?=$arCount;?></span>)</button>
    <?endif;?>
    <?=$arResult["NAV_STRING"];?>
    <?unset($_POST[0]);?>
    <?die;?>
<?endif;?>

<?/*<script>
    //BX.ready(function() {
    function noAuth() {
        alert("Для добавления образца нужно авторизоваться")
    }


    function addToBasket(id, baskProp) {
        var quant = document.getElementsByName("quant_elems_" + id)[0].value;
        var baskPar = {};
        baskPar["ajax_basket"] = "Y";
        if (baskProp != "")
            baskPar["basket_props"] = baskProp;
        baskPar["quantity"] = quant;
        BX.ajax.loadJSON(
            "?action=ADD2BASKET&id=" + id,
            baskPar,
            function (data) {
                //console.log("added!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111");
                if (data.STATUS == "OK")
                    alert(data.MESSAGE);
                else
                    alert(data.MESSAGE);
            }
        );

    }


    var path = "<?=$templateFolder;?>";

    function addToBaskEx(prodId, pathProd, example) {
        if (document.getElementsByName("quant_elems_" + prodId)[0]) {
            var quant = document.getElementsByName("quant_elems_" + prodId)[0].value;
        } else {
            var quant = 1;
        }
        var arData = {};
        arData["id"] = prodId;
        arData["quant"] = quant;
        arData["name"] = document.getElementById("name_prod_" + prodId).innerHTML;
        arData["url"] = pathProd;
        arData["example"] = example;
        BX.ajax.get(
            path + "/ajax_add_ex.php",
            arData,
            function (data) {
                //console.log(data);
                if (data > 0) {
                    if (example == "Y")
                        alert("Образец добавлен в корзину");
                    else
                        alert("Товар добавлен в корзину");
                }
            }
        );
    }

    function __getUriToReload(paramsToOff) {
        var url = window.location.search;
        var params = url.replace("?", "").split("&");
        var delimiter = "?";
        url = '';
        var off;
        for (var i = 0; i < params.length; i++) {
            off = false;
            for (var j = 0; j < paramsToOff.length; j++)
                if (params[i].indexOf(paramsToOff[j]) >= 0) {
                    off = true;
                    break;
                }
            if (off) continue;
            url += delimiter + params[i];
            delimiter = "&";
        }

        return url;
    }



    //console.log("ddddddddddddddddddddddddddddddd");
    //console.log(arParamsBase64);

    function ajaxUpdateList(sort, method, ajax, page_1) {
        var url = __getUriToReload(["PAGEN_1", "sort", "method"]);

        var flaggood = false;
        if (sort != "" && method != "") {
            if (url == "")
                url = "?";
            else
                url += "&";
            url += "sort=" + sort + "&method=" + method;
            flaggood = true;
        }

        if (page_1 != "") {
            if (url == "")
                url = "?";
            else
                url += "&";

            url += "PAGEN_1=" + page_1;
            flaggood = true;
        }

        if (flaggood == true) {
            //url += "&ajax=" + ajax;
            arrAjax = {};
            arrAjax[0] = ajax;
            arrAjax["PARAMS"] = arParams;
            if (ajax == "1") {
                //console.log("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
                $("#analogue > .wrapper > .section-content").load(url + " .section-content .to-reload-by-filter", arrAjax);
                history.replaceState(3, "Title 2", url);
            } else if (ajax == "addelem") {
                //console.log(url);
                var arrAjax = {};
                arrAjax[0] = ajax;
                arrAjax["PARAMS"] = arParams;

                BX.ajax.post(
                    path + "/ajax_update.php" + url,
                    arrAjax,
                    function (data) {
                        //console.log(data);
                        data = data.split("------------============------------");
                        document.getElementById("tableLastProd").innerHTML += data[0];
                        document.getElementById("buttomsList").innerHTML = data[1];
                    }
                );
                history.replaceState(3, "Title 2", url);
            }
        }
    }
    //});

    BX.ready(function() {

        $(document).on('click', function (e){
            var searchSelect = $('.search-select')
            var projectSelect = $('.project-select')
            var subscribeBlock = $('.subscribe-wrapper')

            if (!projectSelect.is(e.target) && projectSelect.has(e.target).length === 0 && projectSelect.hasClass('open')) {
                projectSelect.removeClass('open')
            }

            if (!searchSelect.is(e.target) && searchSelect.has(e.target).length === 0 && searchSelect.hasClass('open')) {
                searchSelect.removeClass('open')
            }

            if (!subscribeBlock.is(e.target) && subscribeBlock.has(e.target).length === 0 && subscribeBlock.hasClass('open')) {
                subscribeBlock.removeClass('open')
            }
        });

        $('#analogue').on('click', '.close-mini', function(){
            if ($(this).parent().parent().hasClass('open')) {
                $(this).parent().parent().removeClass('open')
            }
        });

        $('#analogue').on('click', '.mini-card__select', function(e){
            if ($(this).next('.mini-card').hasClass('open')) {
                $(this).next('.mini-card').removeClass('open')
                $(this).next('.mini-card').children().children().slideUp(300)
            } else {
                $(this).next('.mini-card').addClass('open')
                $(this).next('.mini-card').children().children().slideDown(300)
            }
        });


        $('#analogue').on('click', '.change-all', function(){
            $('input[type="checkbox"]').prop("checked", false)
        });

        $('#analogue').on('click', '.favorite-heart', function(){
            $(this).parent().parent().remove()
        });

        $('#analogue').on('click', '.favorite', function(){
            let el = $(this), id = el.parents('.mini-card').data('id');
            $.ajax({
                url: '/ajax.php',
                type: 'POST',
                data: {action: 'fav', id : id},
            }).done(function(data) {
                data = JSON.parse(data);
                if( data.noAuth ) {
                    alert("Ошибка, Вам нужно авторизоваться!");
                    return;
                }
                if( el.hasClass('act') )
                    alert("Удалено из избранное");
                else
                    alert("Добавлено в избранное");
                el.toggleClass('act');
                console.log(data);
            });
        });

        $('#analogue').on('click', '.items-selector', function(event){
            contetWindowOptions();
            event.preventDefault();
            if ($('.items-selector').parent().hasClass('open')) {
                $('.items-selector').parent().removeClass('open')
            } else {
                $('.items-selector').parent().addClass('open')
            }
            addOptionModal()
        });

    });

    <?
    $arAllProps = array();
    foreach ($arParams["PROPERTY_CODE"] as $code) {
        if (in_array($arResult["ITEMS"][0]["PROPERTIES"][$code]["ID"], $arResult["PROP_AVAL"]))
            $arAllProps[] = array("CODE" => $code, "NAME" => trim($arResult["ITEMS"][0]["PROPERTIES"][$code]["NAME"]));
    }

    global $APPLICATION;
    $uri = $APPLICATION->GetCurUri("", false);
    ?>


    var arPropsChange = <?=CUtil::PhpToJSObject($arResult["CHANGE_OPTIONS"], false, true)?>;
    var arPropsAll = <?=CUtil::PhpToJSObject($arAllProps, false, true)?>;

    function addOptionModal() {
        $('#analogue').on('click', '.add-table-filter', function () {

            var arChangeObject = {"0": "1"};

            var itemsselector = document.getElementsByName("select_option");
            var counterArr = 0;
            var items = {};
            for (var item in itemsselector) {
                if (itemsselector[item].checked) {
                    items[counterArr] = itemsselector[item].id;
                    counterArr++;
                }
            }

            arChangeObject["OPTIONS_MODAL"] = items;
            arPropsChange = items;
            if(counterArr == 0)
                arChangeObject["OPTIONS_MODAL"] = false;


            //console.log(arChangeObject);
            var url = __getUriToReload([]);

            $("#analogue > .wrapper > .section-content").load(url + " .section-content .to-reload-by-filter", arChangeObject);
            //history.replaceState(3, "Title 34", url);
            if ($(this).parent().parent().hasClass('open')) {
                $(this).parent().parent().removeClass('open')
            } else {
                return false
            }

        });
    }

    function in_object(elem, arr) {
        //console.log(arr);
        for (var key in arr){
            if(arr[key] == elem){
                //console.log(arr[key]+"||||"+arr);
                return true;
            }
        }
        return false;
    }

    function contetWindowOptions() {
        var strProps = "";
        for (var prop in arPropsAll){
            if(in_object(arPropsAll[prop].CODE, arPropsChange))
                strProps += '<div class="table-filter__item"><label ><input type="checkbox" name="select_option" id="' + arPropsAll[prop].CODE + '" checked>' + arPropsAll[prop].NAME + '</label></div>';
            else
                strProps += '<div class="table-filter__item"><label ><input type="checkbox" name="select_option" id="' + arPropsAll[prop].CODE + '">' + arPropsAll[prop].NAME + '</label></div>';

        }
        strProps +='<button class="default-button add-table-filter">Применить</button>';
        document.getElementById("option_window").innerHTML = strProps;
    }

    <?$currencyFormat = CCurrencyLang::GetFormatDescription('RUB');?>
    BX.Currency.setCurrencyFormat('RUB', <? echo CUtil::PhpToJSObject($currencyFormat, false, true); ?>);

    function quantChange(symbol, price, ids, limit) {

        if(symbol == "+") {
            if(limit.CATALOG_CAN_BUY_ZERO == "N"){
                if(Number.parseInt(document.getElementById(ids.QUANTITY).value) < Number.parseInt(limit.CATALOG_QUANTITY)){
                    document.getElementById(ids.QUANTITY).value++;
                    quantityVal(price, ids, limit);
                }
            } else {
                document.getElementById(ids.QUANTITY).value++;
                quantityVal(price, ids, limit);
            }

        }else {
            if(document.getElementById(ids.QUANTITY).value > 1) {
                document.getElementById(ids.QUANTITY).value--;
                quantityVal(price, ids, limit);
            }
        }
    }


    function curPrice(price, quant) {
        for (var p in price){
            if(price[p].QUANTITY_FROM == "")
                return price[p];

            if(Number.parseInt(price[p].QUANTITY_FROM) <= Number.parseInt(quant) && price[p].QUANTITY_TO == "0")
                return price[p];

            if(Number.parseInt(price[p].QUANTITY_FROM) <= Number.parseInt(quant) && Number.parseInt(price[p].QUANTITY_TO) >= Number.parseInt(quant))
                return price[p];

        }
    }


    function quantityVal(price, ids, limit) {

        //console.log(price);
        var quantCurrent = document.getElementById(ids.QUANTITY).value;
        if(quantCurrent < 1){
            document.getElementById(ids.QUANTITY).value = 1;
            quantCurrent = 1;
        }

        if(limit.CATALOG_CAN_BUY_ZERO == "N") {
            if (Number.parseInt(document.getElementById(ids.QUANTITY).value) > Number.parseInt(limit.CATALOG_QUANTITY)) {
                document.getElementById(ids.QUANTITY).value = Number.parseInt(limit.CATALOG_QUANTITY);
                quantCurrent = Number.parseInt(limit.CATALOG_QUANTITY);
            }
        }

        if(price.length > 0) {
            var currentPrice = curPrice(price, quantCurrent);
            //console.log(currentPrice);
            document.getElementById(ids.PRICE_TOTAL).innerHTML = BX.Currency.currencyFormat(currentPrice.RATIO_PRICE * quantCurrent, "RUB", true)
        }
    }
</script>*/?>

<section class="analogue" id="analogue">
    <div class="wrapper">

        <div class="section-header">
            <h2><?=$arResult["NAME"];?></h2>
        </div>
        <?$APPLICATION->ShowViewContent('catalog_section_filter');?>
        <div class="section-content">
            <div class="to-reload-by-filter analogue-table__holder">
                <div class="scroll-table">
                    <table class="analogue-table" id="tableLastProd">
                        <tr>
                            <!--окно параметров-->
                            <th >
                                <div class="table-filter">
                                    <div class="table-filter__selector">
                                            <span class="items-selector">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 533.333 533.333" style="enable-background:new 0 0 533.333 533.333;"
                                                     xml:space="preserve">
                                                    <g>
                                                        <path d="M533.333,316.716V216.618l-76.444-12.74c-3.104-9.444-6.888-18.577-11.301-27.344l45.029-63.039l-70.779-70.781
                                                            l-62.931,44.951c-8.807-4.456-17.987-8.275-27.482-11.408L316.715,0H216.618l-12.709,76.258
                                                            c-9.495,3.132-18.676,6.952-27.482,11.408l-62.932-44.951l-70.78,70.781l45.028,63.04c-4.412,8.766-8.195,17.899-11.298,27.343
                                                            L0,216.618v100.098l76.624,12.771c3.104,9.358,6.88,18.41,11.271,27.101l-45.181,63.254l70.78,70.779l63.362-45.258
                                                            c8.646,4.349,17.651,8.088,26.96,11.163l12.801,76.808h100.098l12.801-76.81c9.312-3.076,18.313-6.814,26.962-11.162l63.36,45.257
                                                            l70.779-70.779l-45.18-63.253c4.392-8.689,8.166-17.741,11.271-27.1L533.333,316.716z M266.667,333.333
                                                            c-36.819,0-66.667-29.846-66.667-66.666C200,229.848,229.848,200,266.667,200c36.82,0,66.667,29.848,66.667,66.667
                                                            C333.333,303.487,303.487,333.333,266.667,333.333z"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        <div class="table-filter__list" id="option_window">
                                            <?/*if(in_array($prop, $arResult["CHANGE_OPTIONS"])):?>
                                                <?continue;?>
                                            <?endif;*/?>

                                        </div>
                                    </div>


                                    <!--окно параметров-->
                                    <?if($_GET["sort"] == "NAME"):?>
                                        <?if($_GET["method"] == "" || $_GET["method"] == "desc"):?>
                                            <div class="table-filter__sorting sort-val-down" onclick="ajaxUpdateList('NAME', 'asc', '1', '')">Название</div>
                                        <?elseif ($_GET["method"] == "asc"):?>
                                            <div class="table-filter__sorting sort-val-up" onclick="ajaxUpdateList('NAME', 'desc', '1', '')">Название</div>
                                        <?endif;?>

                                    <?else:?>
                                        <div class="table-filter__sorting sort-val-down" onclick="ajaxUpdateList('NAME', 'asc', '1', '')">Название</div>

                                    <?endif;?>
                                </div>
                            </th>



                            <?$counterCol = 1;?>
                            <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                                <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arResult["ITEMS"][0]["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
                                    <?continue;?>
                                <?endif;?>
                                <?if(!empty(trim($prop)) && !empty(trim($arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"]))):?>
                                    <?$counterCol++;?>
                                    <?if($_GET["sort"] == "PROPERTY_".$prop):?>
                                        <?if($_GET["method"] == "" || $_GET["method"] == "desc"):?>
                                            <th class ="sort-val-up" onclick="ajaxUpdateList('<?='PROPERTY_'.$prop?>', 'asc', '1', '')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                        <?elseif ($_GET["method"] == "asc"):?>
                                            <th class ="sort-val-down" onclick="ajaxUpdateList('<?='PROPERTY_'.$prop?>', 'desc', '1', '')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                        <?endif;?>
                                    <?else:?>
                                        <th onclick="ajaxUpdateList('<?='PROPERTY_'.$prop?>', 'asc', '1', '')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                    <?endif;?>
                                <?endif;?>
                            <?endforeach;?>

                        </tr>

                        <?foreach ($arResult["ITEMS"] as $arItem):?>
                            <?


                            //$item = $arItem;
                            //$item["CAN_BUY"] = true;
                            $areaId = $this->GetEditAreaId($arItem['ID']);
                            $itemIds = array(
                                'ID' => $areaId,
                                'PICT' => $areaId.'_pict',
                                'SECOND_PICT' => $areaId.'_secondpict',
                                'PICT_SLIDER' => $areaId.'_pict_slider',
                                'STICKER_ID' => $areaId.'_sticker',
                                'SECOND_STICKER_ID' => $areaId.'_secondsticker',
                                'QUANTITY' => $areaId.'_quantity',
                                'QUANTITY_DOWN' => $areaId.'_quant_down',
                                'QUANTITY_UP' => $areaId.'_quant_up',
                                'QUANTITY_MEASURE' => $areaId.'_quant_measure',
                                'QUANTITY_LIMIT' => $areaId.'_quant_limit',
                                'BUY_LINK' => $areaId.'_buy_link',
                                'BASKET_ACTIONS' => $areaId.'_basket_actions',
                                'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
                                'SUBSCRIBE_LINK' => $areaId.'_subscribe',
                                'COMPARE_LINK' => $areaId.'_compare_link',
                                'PRICE' => $areaId.'_price',
                                'PRICE_OLD' => $areaId.'_price_old',
                                'PRICE_TOTAL' => $areaId.'_price_total',
                                'DSC_PERC' => $areaId.'_dsc_perc',
                                'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
                                'PROP_DIV' => $areaId.'_sku_tree',
                                'PROP' => $areaId.'_prop_',
                                'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
                                'BASKET_PROP_DIV' => $areaId.'_basket_prop',
                            );

                            ?>


                            <tr class="mini-card__select" >
                                <td id = "name_prod_<?=$arItem["ID"];?>"><?=$arItem["NAME"];?></td>
                                <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                                    <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arItem["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
                                        <?continue;?>
                                    <?endif;?>
                                    <?if(!empty($arItem["PROPERTIES"][$prop]) && !empty(trim($arItem["PROPERTIES"][$prop]["NAME"]))):?>
                                        <td><?=$arItem["PROPERTIES"][$prop]["VALUE"];?></td>
                                    <?endif;?>
                                <?endforeach;?>
                                <!--<td><span class="analogue-status ordered">Заказано </span></td>-->
                            </tr>
                            <tr class="mini-card" id="<?=$itemIds["ID"]?>" data-id="<?=$arItem['ID']?>">
                                <?//$arItem["DETAIL_PAGE_URL"];?>
                                <td  colspan="<?=$counterCol;?>">
                                    <div class="card-holder">
                                        <div class="mini-cart__wrapper">
                                            <div class="item-img">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"];?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                                                <?
                                                $k = empty($favs) ? false : array_search( $arItem["ID"], $favs);
                                                $is_favs = $k!==FALSE ? true : false;
                                                ?>
                                                <span class="favorite <?=$is_favs ? 'act':''?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                                        </span>
                                            </div>
                                            <div class="item-info">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="item-info__head" onclick="console.log('no')">
                                                    <?=$arItem["NAME"].", ".$arItem["PREVIEW_TEXT"];?>
                                                </a>
                                                <div class="item-info__content">
                                                    <?if($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"]):?>
                                                        <ul>
                                                            <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"] as $prop):?>
                                                                <?if(!empty(trim($prop["VALUE"]))):?>
                                                                    <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                                <?endif;?>
                                                            <?endforeach;?>
                                                        </ul>
                                                    <?endif;?>
                                                    <ul>
                                                        <?if($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"]):?>
                                                            <ul>
                                                                <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $prop):?>
                                                                    <?if(!empty(trim($prop["VALUE"]))):?>
                                                                        <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                                    <?endif;?>
                                                                <?endforeach;?>
                                                            </ul>
                                                        <?endif;?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="item-more">
                                                <?if($arItem["CATALOG_AVAILABLE"] == "Y"):?>
                                                    <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                                    <?if($arItem["CATALOG_QUANTITY"] > 0):?>
                                                        <span class="avalibilty">В наличии <?=$arItem["CATALOG_QUANTITY"]?> единиц</span>
                                                    <?else:?>
                                                        <span class="avalibilty out">Нет в наличии</span>
                                                    <?endif;?>

                                                    <?//if(!empty($arItem["ITEM_PRICES"])):?>
                                                    <div class="wholesale">
                                                        <?foreach ($arItem["ITEM_PRICES"] as $arPrice):?>
                                                            <?if(!empty($arPrice["QUANTITY_TO"])):?>
                                                                <div class="quantity">
                                                                    <span class="quantity-items"><?=$arPrice["QUANTITY_FROM"]."-".$arPrice["QUANTITY_TO"]?> штук </span>
                                                                    <span class="quantity-separator"></span>
                                                                    <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                                                </div>
                                                            <?else:?>
                                                                <div class="quantity">
                                                                    <?if(empty($arPrice["QUANTITY_FROM"])){
                                                                        $arPrice["QUANTITY_FROM"] = 1;
                                                                    }?>
                                                                    <span class="quantity-items">от <?=$arPrice["QUANTITY_FROM"]?> штук </span>
                                                                    <span class="quantity-separator"></span>
                                                                    <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                                                </div>
                                                            <?endif;?>
                                                        <?endforeach;?>
                                                    </div>
                                                    <?//endif;?>

                                                    <div class="quantity-num">
                                                        <span class="quantity-button" id="<? echo $itemIds['QUANTITY_DOWN']; ?>" onclick="quantChange('-', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">–</span>
                                                        <input value="1" max="99999" name = "quant_elems_<?=$arItem["ID"];?>" id="<? echo $itemIds['QUANTITY']; ?>" onchange="quantityVal(<?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>)" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
                                                        <span class="quantity-button" id="<? echo $itemIds['QUANTITY_UP']; ?>" onclick="quantChange('+', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">+</span>
                                                    </div>
                                                    <form action="" onkeydown="if(event.keyCode==13){return false;}">
                                                        <?if(!empty($arItem["ITEM_PRICES"])):?>
                                                            <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">
                                                                <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                                            </div>
                                                            <div class="price" id="<? echo $itemIds['PRICE_TOTAL']; ?>">
                                                                <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                                            </div>
                                                            <div class="cb"></div>
                                                            <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBasket('<?=$arItem["ID"];?>', '');" href="javascript:void(0)" rel="nofollow">Добавить в корзину</a>
                                                        <?else:?>
                                                            <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">0</div>
                                                            <div class="price" style="display: none" id="<? echo $itemIds['PRICE_TOTAL']; ?>">0</div>
                                                            <div class="cb"></div>
                                                            <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'N');" href="javascript:void(0)" rel="nofollow">Запросить цену</a>
                                                        <?endif;?>
                                                    </form>

                                                    <a <?if ($USER->IsAuthorized()):?>onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'Y');"<?else:?>onclick="noAuth();"<?endif;?> class="sidebar-sample" href="javascript:void(0)">Запросить образец</a>
                                                <?else:?>
                                                    <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                                    <span class="avalibilty out">Товар недоступен</span>
                                                <?endif;?>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                        <?endforeach;?>

                    </table>
                </div>
            </div>
            <span class="to-reload-by-filter" id="buttomsList">
            <?$arCount = $arResult["NAV_RESULT"]->NavRecordCount - $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer;?>
                <?if($arCount > 0):?>
                    <button id = "button_elem_q" class="show-all" onclick="ajaxUpdateList('<?=$_GET["sort"];?>', '<?=$_GET["method"]?>', 'addelem', '<?=$arResult["NAV_RESULT"]->NavPageNomer + 1?>');">Показать ещё (<span id="element_count"><?=$arCount;?></span>)</button>
                <?endif;?>
                <?=$arResult["NAV_STRING"]?>
        </span>
        </div>
    </div>
</section>
