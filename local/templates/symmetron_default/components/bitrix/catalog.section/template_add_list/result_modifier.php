<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
//echo "<pre>";
//print_r($arParams);
//print_r($arResult);
//echo "</pre>";
    if (!empty($arResult["ITEMS"])) {
        foreach ($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"] as $prop) {

            if (!empty($prop)) {
                $props = $arResult["ITEMS"][0]["PROPERTIES"];
                if (!empty($props[$prop]["NAME"]))
                    $arResult["GRID"]["NAMES_PROP"][] = array('id' => $props[$prop]["CODE"], 'name' => $props[$prop]["NAME"], 'sort' => $props[$prop]["SORT"], 'default' => true);

            }
        }


        foreach ($arResult["ITEMS"] as $item) {
            $arResult["GRID"]["PROD_NAMES"][] = $item["NAME"];
            $arValProps = array();

            foreach ($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"] as $prop) {
                if (!empty($prop)) {
                    $arValProps["data"][$prop] = $item["DISPLAY_PROPERTIES"][$prop]["VALUE"];
                }
            }

            $arResult["GRID"]["VAL_PROP"][] = $arValProps;


            //echo "<pre>";
            //print_r($item);
            //echo "</pre>";


            $arResult["GRID"]["SHOW_IN_MODAL"][] = array("SEC_ID" => $item["IBLOCK_SECTION_ID"], "ELEM_ID" => $item["ID"]);

        }

    }
    $prod_names = "";
     foreach ($arResult["GRID"]["PROD_NAMES"] as $name):
                                $prod_names .= "<tr>
                                    <td>$name</td>
                                </tr>";
                            endforeach;?>

<?
$GLOBALS['APPLICATION']->RestartBuffer();
$strs = "";
foreach ($arResult["GRID"]["VAL_PROP"] as $key => $row):
    //echo "<pre>";
    //print_r($arResult["GRID"]["VAL_PROP"]);
    //echo "</pre>";
    $strs .= "<tr onclick='modalInfo(".$arResult["ITEMS"][$key]["ID"].", ".$arParams["SECTION_ID"].");'>";
        foreach ($arResult["GRID"]["NAMES_PROP"] as $key2 => $col):
            $strs .= "<td>".$row["data"][$col["id"]]."</td>";
        endforeach;
    $strs .= "</tr>";

endforeach;


echo $prod_names."-+-+".$strs."-+-+".count($arResult["ITEMS"]);
die;