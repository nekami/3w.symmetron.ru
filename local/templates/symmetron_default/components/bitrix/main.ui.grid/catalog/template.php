
    <!--<section class="analogue" id="analogue">
        <div class="wrapper">

            <div class="section-content">
                <div class="analogue-table__holder">
                    <div class="left-side">
                        <table class="analogue-table">
                            <tr><th>International</th></tr>
                            <tr>
                                <td>International Rectifier International Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                            </tr>
                        </table>
                    </div>
                    <div class="right-side">
                        <table class="analogue-table">
                            <tr>
                                <th>International Rectifier</th>
                                <th>International Rectifier</th>
                                <th>International Rectifier</th>
                                <th>International Rectifier</th>
                                <th>International Rectifier</th>
                                <th>Статус</th>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td><span class="analogue-status ordered">Заказано </span></td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td><span class="analogue-status stock">На складе</span></td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td><span class="analogue-status order">Под заказ</span></td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                            </tr>
                            <tr>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                                <td>International Rectifier</td>
                            </tr>


                        </table>
                    </div>
                </div>
                <button class="show-all">Показать все аналогичные позиции (274)</button>
            </div>
        </div>
    </section>-->






<?

//echo "<pre>";
//print_r($arResult['COLUMNS']);
//print_r($arResult['ROWS']);
//echo "</pre>";

//echo "fsdfsdfsdfsdfsdfsdf";
/**
 * @var $arParams
 * @var $arResult
 */

use \Bitrix\Main\Text;
use \Bitrix\Main\Grid;
use \Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

CJSCore::Init(array('popup', 'ui', 'resize_observer', 'loader'));

\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/main/dd.js');

global $APPLICATION;
$bodyClass = $APPLICATION->GetPageProperty("BodyClass");
$APPLICATION->SetPageProperty("BodyClass", ($bodyClass ? $bodyClass." " : "")."grid-mode");

if ($arParams['FLEXIBLE_LAYOUT'])
{
	$bodyClass = $APPLICATION->getPageProperty('BodyClass', false);
	$APPLICATION->setPageProperty('BodyClass', trim(sprintf('%s %s', $bodyClass, 'flexible-layout')));
}

$additionalColumnsCount = 1;

if ($arParams["SHOW_ROW_CHECKBOXES"])
{
	$additionalColumnsCount += 1;
}

if ($arParams["SHOW_GRID_SETTINGS_MENU"] || $arParams["SHOW_ROW_ACTIONS_MENU"])
{
	$additionalColumnsCount += 1;
}

if ($arParams["ALLOW_ROWS_SORT"])
{
	$additionalColumnsCount += 1;
}

$displayedCount = count(
	array_filter(
		$arParams["ROWS"],
		function($val)
		{
			return $val["not_count"] !== true;
		}
	)
);

?>


    <section class="analogue" id="analogue">
        <div class="wrapper">
            <div class="section-content">
                <div class="analogue-table__holder">
                    <div class="left-side">
                        <table class="analogue-table">
                            <tr><th> <span <?=!$arParams["ALLOW_HORIZONTAL_SCROLL"] ? " main-grid-full" : ""?><?=$arParams["ALLOW_ROWS_SORT"] ? " main-grid-rows-sort-enable" : ""?>" id="<?=$arParams["GRID_ID"]?>" data-ajaxid="<?=$arParams["AJAX_ID"]?>"<?=$arResult['IS_AJAX'] ? " style=\"display: none;\"" : ""?>><?
                                        ?>
                                        <span class="main-grid-interface-settings-icon"></span>
                                        <form style="display: none"  name="form_<?=$arParams["GRID_ID"]?>" action="<?=POST_FORM_ACTION_URI; ?>" method="POST"><?
                                            ?><?=bitrix_sessid_post() ?><?
                                            ?><div class="main-grid-settings-window"><?
                                                ?><div class="main-grid-settings-window-select-links"><?
                                                    ?><span class="main-grid-settings-window-select-link main-grid-settings-window-select-all"><?=Loc::getMessage("interface_grid_settings_select_all_columns")?></span><?
                                                    ?><span class="main-grid-settings-window-select-link main-grid-settings-window-unselect-all"><?=Loc::getMessage("interface_grid_settings_unselect_all_columns")?></span><?
                                                    ?></div><?
                                                ?><div class="main-grid-settings-window-list"><?
                                                    foreach ($arResult["COLUMNS_ALL"] as $key => $column) : ?><?
                                                        ?><div data-name="<?=Text\HtmlFilter::encode($column["id"])?>" class="main-grid-settings-window-list-item"><?
                                                        ?><input id="<?=Text\HtmlFilter::encode($column["id"])?>-checkbox" type="checkbox" class="main-grid-settings-window-list-item-checkbox" <?=array_key_exists($column["id"], $arResult["COLUMNS"]) ? " checked" : ""?>><?
                                                        ?><label for="<?=Text\HtmlFilter::encode($column["id"])?>-checkbox" class="main-grid-settings-window-list-item-label"><?=Text\HtmlFilter::encode($column["name"])?></label><?
                                                        ?><span class="main-grid-settings-window-list-item-edit-button"></span><?
                                                        ?></div><?
                                                    endforeach;
                                                    ?></div><?
                                                ?><div class="popup-window-buttons"><?
                                                    ?><span class="main-grid-settings-window-buttons-wrapper"><?
                                                        ?><span class="main-grid-settings-window-actions-item-button main-grid-settings-window-actions-item-reset" id="<?=$arParams["GRID_ID"]?>-grid-settings-reset-button"><?=Loc::getMessage("interface_grid_restore_to_default")?></span><?
                                                        if ($USER->CanDoOperation("edit_other_settings")) :
                                                            ?><span class="main-grid-settings-window-actions-item-button main-grid-settings-window-for-all">
                                                            <input name="grid-settings-window-for-all" type="checkbox" id="<?=$arParams["GRID_ID"]?>-main-grid-settings-window-for-all-checkbox" class="main-grid-settings-window-for-all-checkbox">
                                                            <label for="<?=$arParams["GRID_ID"]?>-main-grid-settings-window-for-all-checkbox" class="main-grid-settings-window-for-all-label"><?=Loc::getMessage("interface_grid_settings_for_all_label")?></label><?
                                                            ?></span><?
                                                        endif;
                                                        ?></span><?
                                                    ?><span class="main-grid-settings-window-actions-item-button webform-small-button" id="<?=$arParams["GRID_ID"]?>-grid-settings-apply-button" onclick="location.reload();"><?=Loc::getMessage("interface_grid_apply_settings")?></span><?
                                                    ?><span class="main-grid-settings-window-actions-item-button webform-small-button webform-small-button-transparent" id="<?=$arParams["GRID_ID"]?>-grid-settings-cancel-button"><?=Loc::getMessage("interface_grid_cancel_settings")?></span><?
                                                    ?></div><?
                                                ?></div><?
                                            ?><?
                                            ?></form><?
                                        ?>
                                    </span>Название</th></tr>
                            <?foreach ($arParams["NAMES_PROD"] as $name):?>
                            <tr>
                                <td><?=$name;?></td>
                            </tr>
                            <?endforeach;?>

                        </table>
                    </div>
                    <div class="right-side">
                        <table class="analogue-table">

                            <tr>

                                <?
                                foreach ($arResult['COLUMNS'] as $id => $header) : ?>
                                    <th><?=$header["name"];?></th>

                                <?
                                endforeach ?>
                            </tr>
                            <?foreach ($arResult['ROWS'] as $row):?>

                                <tr>
                                    <?foreach ($arResult['COLUMNS'] as $key => $col):?>
                                        <td>
                                            <?=$row["data"][$key];?>
                                            <?//=$header["name"];?>
                                        </td>
                                    <?endforeach;?>
                                </tr>

                            <?endforeach ?>


                        </table>
                    </div>
                </div>
                <button class="show-all">Показать все аналогичные позиции (274)</button>
            </div>
        </div>
    </section>




<?
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if (\Bitrix\Main\Grid\Context::isInternalRequest()) :
?><script>
	(function() {
		var action = '<?=\CUtil::JSEscape($request->get("grid_action"))?>';
		var editableData = eval(<?=CUtil::phpToJSObject($arResult["DATA_FOR_EDIT"])?>);
		var defaultColumns = eval(<?=CUtil::phpToJSObject($arResult["DEFAULT_COLUMNS"])?>);
		var Grid = BX.Main.gridManager.getById('<?=$arParams["GRID_ID"]?>');
		var messages = eval(<?=CUtil::phpToJSObject($arResult["MESSAGES"])?>);

		Grid = Grid ? Grid.instance : null;

		if (Grid)
		{
			Grid.arParams.DEFAULT_COLUMNS = defaultColumns;
			Grid.arParams.MESSAGES = messages;

			if (action !== 'more')
			{
				Grid.arParams.EDITABLE_DATA = editableData;
			}
			else
			{
				var editableDataKeys = Object.keys(editableData);
				editableDataKeys.forEach(function(key) {
					Grid.arParams.EDITABLE_DATA[key] = editableData[key];
				});
			}

			BX.onCustomEvent(window, 'BX.Main.grid:paramsUpdated', []);
		}
	})();
</script><?
endif; ?>

<? if (!$arResult['IS_AJAX'] || !$arResult['IS_INTERNAL']) : ?><?
?><script>
		BX(function() { BX.Main.dropdownManager.init(); });
		BX(function() {
			BX.Main.gridManager.push(
				'<?=$arParams["GRID_ID"]?>',
				new BX.Main.grid(
					'<?=$arParams["GRID_ID"]?>',
					<?=CUtil::PhpToJSObject(
                        array(
                            "ALLOW_COLUMNS_SORT" => $arParams["ALLOW_COLUMNS_SORT"],
                            "ALLOW_ROWS_SORT" => $arParams["ALLOW_ROWS_SORT"],
                            "ALLOW_COLUMNS_RESIZE" => $arParams["ALLOW_COLUMNS_RESIZE"],
                            "SHOW_ROW_CHECKBOXES" => $arParams["SHOW_ROW_CHECKBOXES"],
                            "ALLOW_HORIZONTAL_SCROLL" => $arParams["ALLOW_HORIZONTAL_SCROLL"],
                            "ALLOW_PIN_HEADER" => $arParams["ALLOW_PIN_HEADER"],
                            "SHOW_ACTION_PANEL" => $arParams["SHOW_ACTION_PANEL"],
                            "PRESERVE_HISTORY" => $arParams["PRESERVE_HISTORY"],
                            "BACKEND_URL" => $arResult["BACKEND_URL"],
                            "ALLOW_CONTEXT_MENU" => $arResult["ALLOW_CONTEXT_MENU"],
                            "DEFAULT_COLUMNS" => $arResult["DEFAULT_COLUMNS"],
                            "ENABLE_COLLAPSIBLE_ROWS" => $arParams["ENABLE_COLLAPSIBLE_ROWS"],
                            "EDITABLE_DATA" => $arResult["DATA_FOR_EDIT"],
                            "SETTINGS_TITLE" => Loc::getMessage("interface_grid_settings_title"),
                            "APPLY_SETTINGS" => Loc::getMessage("interface_grid_apply_settings"),
                            "CANCEL_SETTINGS" => Loc::getMessage("interface_grid_cancel_settings"),
                            "CONFIRM_APPLY" => Loc::getMessage("interface_grid_confirm_apply"),
                            "CONFIRM_CANCEL" => Loc::getMessage("interface_grid_confirm_cancel"),
                            "CONFIRM_MESSAGE" => Loc::getMessage("interface_grid_confirm_message"),
                            "CONFIRM_FOR_ALL_MESSAGE" => Loc::getMessage("interface_grid_confirm_for_all_message"),
                            "CONFIRM_RESET_MESSAGE" => Loc::getMessage("interface_grid_settings_confirm_message"),
                            "RESET_DEFAULT" => Loc::getMessage("interface_grid_restore_to_default"),
                            "SETTINGS_FOR_ALL_LABEL" => Loc::getMessage("interface_grid_settings_for_all_label"),
                            "SETTINGS_FOR_ALL_CONFIRM_MESSAGE" => Loc::getMessage("interface_grid_settings_for_all_confirm_message"),
                            "SETTINGS_FOR_ALL_CONFIRM_APPLY" => Loc::getMessage("interface_grid_settings_for_all_apply"),
                            "SETTINGS_FOR_ALL_CONFIRM_CANCEL" => Loc::getMessage("interface_grid_settings_for_all_cancel"),
                            "CLOSE" => Loc::getMessage("interface_grid_settings_close"),
                            "IS_ADMIN" => $USER->CanDoOperation("edit_other_settings"),
                            "MESSAGES" => $arResult["MESSAGES"],
                            "LAZY_LOAD" => $arResult["LAZY_LOAD"]
                        )
					)?>,
					<?=CUtil::PhpToJSObject($arResult["OPTIONS"])?>,
					<?=CUtil::PhpToJSObject($arResult["OPTIONS_ACTIONS"])?>,
					'<?=$arResult["OPTIONS_HANDLER_URL"]?>',
					<?=CUtil::PhpToJSObject($arResult["PANEL_ACTIONS"])?>,
					<?=CUtil::PhpToJSObject($arResult["PANEL_TYPES"])?>,
					<?=CUtil::PhpToJSObject($arResult["EDITOR_TYPES"])?>,
					<?=CUtil::PhpToJSObject($arResult["MESSAGE_TYPES"])?>
				)
			);
        });
	</script>
<? endif; ?>