<? if ($_GET["ajax_call"] == "Y") {
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "----------------=======================------------------";
} ?>

    <script>
        var POST_FORM_ACTION_URI = "<?=POST_FORM_ACTION_URI?>";

        function isExists() {
            var dataForm = {};
            dataForm["LOGIN"] = document.getElementById("email").value;
            dataForm["EMAIL"] = document.getElementById("email").value;
            dataForm["PERSONAL_PHONE"] = document.getElementById("typePhone").value;
            dataForm["PASSWORD"] = document.getElementById("password").value;
            dataForm["CONFIRM_PASSWORD"] = document.getElementById("password_con").value;

            var dataPost = {};
            dataPost["REGISTER"] = dataForm;
            dataPost["UF_DOB"] = document.getElementById("numDob").value;
            dataPost["personal-data"] = "on";
            dataPost["register_submit_button"] = "Зарегистрироваться";
            var pathReg = POST_FORM_ACTION_URI.split("?");

            BX.ajax.post(
                pathReg[0] + "?ajax_call=Y",
                dataPost,
                function (data) {
                    var arData = data.split("----------------=======================------------------");
                    document.getElementById("modal-registration").innerHTML = arData[1];
                    //return false;
                }
            );


        }
    </script>

    <div class="registration main-modal">
        <a class="reg-link" href="#modal-login" rel="close-modal" onclick='$("#modal-login").modal();'>Войти</a>
        <h2>Регистрация</h2>
        <? if ($USER->IsAuthorized()): ?>
            <? echo "Мы не можем вас зарегистрировать, пока вы авторизованы..." ?>
        <? elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && $_GET["ajax_call"] == "Y" && count($arResult["ERRORS"]) == 0): ?>
            <? echo "Спасибо! На указанный в форме e-mail придет запрос на подтверждение регистрации." ?>
        <? else: ?>
            <?
            $arDistrEr = explode("<br>", $arResult["ERRORS"][0]);
            $arErrors = array();

            if (!empty($arDistrEr)) {
                foreach ($arDistrEr as $key => $error) {

                    if (strpos($error, "Пользователь с таким") === false) {
                    } else {
                        $error = str_replace("логином", "e-mail", $error);
                        $arErrors["ERRORS_LOGIN"][] = $error;
                        unset($arResult["ERRORS"][$key]);
                    }


                    if (strpos($error, "подтверждение") === false) {
                    } else {
                        $arErrors["ERRORS_PASS_CONF"][] = $error;
                        unset($arResult["ERRORS"][$key]);
                    }


                    if (strpos($error, "не менее") === false) {
                    } else {
                        $arErrors["ERRORS_PASS"][] = $error;
                        unset($arResult["ERRORS"][$key]);
                    }

                }
            }
            ?>
            <form method="post" name="regform" enctype="multipart/form-data" onsubmit="isExists(); return false;">
                <div class="email">
                    <input placeholder="E-mail" type="email" name="REGISTER[LOGIN]" id="email"
                           <? if (!empty($arErrors["ERRORS_LOGIN"])): ?>class="error"<? endif; ?>
                           <? if (!empty($_POST["REGISTER"]["EMAIL"])): ?>value="<?= $_POST["REGISTER"]["EMAIL"]; ?>"<? endif; ?>
                           required>
                    <? if (!empty($arErrors["ERRORS_LOGIN"])): ?>
                        <? foreach ($arErrors["ERRORS_LOGIN"] as $mess): ?>
                            <span class="error-message"><?= $mess; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <div class="phone">
                    <input class="reg-phone__main" type="text" name="REGISTER[PERSONAL_PHONE]" id="typePhone"
                           <? if (!empty($_POST["REGISTER"]["PERSONAL_PHONE"])): ?>value="<?= $_POST["REGISTER"]["PERSONAL_PHONE"]; ?>" <? else: ?> value="" <? endif; ?>
                           placeholder="+7" required>
                    <input class="reg-phone__additional" type="text" name="UF_DOB" id="numDob"
                           <? if (!empty($_POST["UF_DOB"])): ?>value="<?= $_POST["UF_DOB"]; ?>" <? else: ?> value="" <? endif; ?>
                           placeholder="Доб.">
                </div>
                <div class="password">
                    <input placeholder="Пароль" type="password" name="REGISTER[PASSWORD]"
                           <? if (!empty($arErrors["ERRORS_PASS"])): ?>class="error"<? endif; ?> autocomplete="off"
                           id="password" required>
                    <? if (!empty($arErrors["ERRORS_PASS"])): ?>
                        <? foreach ($arErrors["ERRORS_PASS"] as $mess): ?>
                            <span class="error-message"><?= $mess; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <div class="password">
                    <input placeholder="Подтверждение пароля" type="password"
                           <? if (!empty($arErrors["ERRORS_PASS_CONF"])): ?>class="error"<? endif; ?>
                           name="REGISTER[CONFIRM_PASSWORD]" id="password_con" autocomplete="off" required>
                    <? if (!empty($arErrors["ERRORS_PASS_CONF"])): ?>
                        <? foreach ($arErrors["ERRORS_PASS_CONF"] as $mess): ?>
                            <span class="error-message"><?= $mess; ?></span>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <div class="submit">
                    <div class="personal-data">
                        <input type="checkbox" name="personal-data" id="personal-data" required>
                        <label for="personal-data">Я ознакомлен с <a href="/agreements/">положением ООО «Симметрон ЭК»
                                об обработке персональных данных</a>, предоставляемых на сайте ООО «Симметрон ЭК», и
                            согласен на обработку своих персональных данных в соответствии с указанным положением и
                            федеральным законом от 27.07.2006 №152-ФЗ «О персональных данных».</label>
                    </div>
                    <input type="submit" name="register_submit_button" value="Зарегистрироваться">
                </div>
            </form>

        <? endif; ?>

    </div>
<? if ($_GET["ajax_call"] == "Y") { ?>
    <a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>
    <? die; ?>
<? } ?>
