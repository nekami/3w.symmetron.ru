<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

echo "<pre>";
print_r($arResult);
echo "</pre>";


/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

$documentRoot = Main\Application::getDocumentRoot();

if (empty($arParams['TEMPLATE_THEME']))
{
	$arParams['TEMPLATE_THEME'] = Main\ModuleManager::isModuleInstalled('bitrix.eshop') ? 'site' : 'blue';
}

if ($arParams['TEMPLATE_THEME'] === 'site')
{
	$templateId = Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', $component->getSiteId());
	$templateId = preg_match('/^eshop_adapt/', $templateId) ? 'eshop_adapt' : $templateId;
	$arParams['TEMPLATE_THEME'] = Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', $component->getSiteId());
}

if (!empty($arParams['TEMPLATE_THEME']))
{
	if (!is_file($documentRoot.'/bitrix/css/main/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
	{
		$arParams['TEMPLATE_THEME'] = 'blue';
	}
}

if (!isset($arParams['DISPLAY_MODE']) || !in_array($arParams['DISPLAY_MODE'], array('extended', 'compact')))
{
	$arParams['DISPLAY_MODE'] = 'extended';
}

$arParams['USE_DYNAMIC_SCROLL'] = isset($arParams['USE_DYNAMIC_SCROLL']) && $arParams['USE_DYNAMIC_SCROLL'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_FILTER'] = isset($arParams['SHOW_FILTER']) && $arParams['SHOW_FILTER'] === 'N' ? 'N' : 'Y';

$arParams['PRICE_DISPLAY_MODE'] = isset($arParams['PRICE_DISPLAY_MODE']) && $arParams['PRICE_DISPLAY_MODE'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['TOTAL_BLOCK_DISPLAY']) || !is_array($arParams['TOTAL_BLOCK_DISPLAY']))
{
	$arParams['TOTAL_BLOCK_DISPLAY'] = array('top');
}

if (empty($arParams['PRODUCT_BLOCKS_ORDER']))
{
	$arParams['PRODUCT_BLOCKS_ORDER'] = 'props,sku,columns';
}

if (is_string($arParams['PRODUCT_BLOCKS_ORDER']))
{
	$arParams['PRODUCT_BLOCKS_ORDER'] = explode(',', $arParams['PRODUCT_BLOCKS_ORDER']);
}

$arParams['USE_PRICE_ANIMATION'] = isset($arParams['USE_PRICE_ANIMATION']) && $arParams['USE_PRICE_ANIMATION'] === 'N' ? 'N' : 'Y';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

if ($arParams['USE_GIFTS'] === 'Y')
{
	$giftParameters = array(
		'SHOW_PRICE_COUNT' => 1,
		'PRODUCT_SUBSCRIPTION' => 'N',
		'PRODUCT_ID_VARIABLE' => 'id',
		'PARTIAL_PRODUCT_PROPERTIES' => 'N',
		'USE_PRODUCT_QUANTITY' => 'N',
		'ACTION_VARIABLE' => 'actionGift',
		'ADD_PROPERTIES_TO_BASKET' => 'Y',

		'BASKET_URL' => $APPLICATION->GetCurPage(),
		'APPLIED_DISCOUNT_LIST' => $arResult['APPLIED_DISCOUNT_LIST'],
		'FULL_DISCOUNT_LIST' => $arResult['FULL_DISCOUNT_LIST'],

		'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
		'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_SHOW_VALUE'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],

		'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
		'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
		'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],
		'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
		'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
		'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
		'SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
		'SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
		'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
		'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
		'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
		'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
		'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

		'LINE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],

		'DETAIL_URL' => isset($arParams['GIFTS_DETAIL_URL']) ? $arParams['GIFTS_DETAIL_URL'] : null
	);
}

\CJSCore::Init(array('fx', 'popup', 'ajax'));

$this->addExternalCss('/bitrix/css/main/bootstrap.css');
$this->addExternalCss($templateFolder.'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');

//$this->addExternalJs($templateFolder.'/js/mustache.js');
//$this->addExternalJs($templateFolder.'/js/action-pool.js');
//$this->addExternalJs($templateFolder.'/js/filter.js');
//$this->addExternalJs($templateFolder.'/js/component.js');

//$mobileColumns = isset($arParams['COLUMNS_LIST_MOBILE'])
//	? $arParams['COLUMNS_LIST_MOBILE']
//	: $arParams['COLUMNS_LIST'];
//$mobileColumns = array_fill_keys($mobileColumns, true);

//$jsTemplates = new Main\IO\Directory($documentRoot.$templateFolder.'/js-templates');
/** @var Main\IO\File $jsTemplate */
//foreach ($jsTemplates->getChildren() as $jsTemplate)
//{
//	include($jsTemplate->getPath());
//}

$displayModeClass = $arParams['DISPLAY_MODE'] === 'compact' ? ' basket-items-list-wrapper-compact' : '';

if (empty($arResult['ERROR_MESSAGE']))
{
	/*if ($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'TOP')
	{
		$APPLICATION->IncludeComponent(
			'bitrix:sale.gift.basket',
			'.default',
			$giftParameters,
			$component
		);
	}

	if ($arResult['BASKET_ITEM_MAX_COUNT_EXCEEDED'])
	{
		?>
		<div id="basket-item-message">
			<?=Loc::getMessage('SBB_BASKET_ITEM_MAX_COUNT_EXCEEDED', array('#PATH#' => $arParams['PATH_TO_BASKET']))?>
		</div>
		<?
	}
	?>
	<div id="basket-root" class="bx-basket bx-<?=$arParams['TEMPLATE_THEME']?> bx-step-opacity" style="opacity: 0;">
		<?
		if (
			$arParams['BASKET_WITH_ORDER_INTEGRATION'] !== 'Y'
			&& in_array('top', $arParams['TOTAL_BLOCK_DISPLAY'])
		)
		{
			?>
			<div class="row">
				<div class="col-xs-12" data-entity="basket-total-block"></div>
			</div>
			<?
		}
		?>

		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-warning alert-dismissable" id="basket-warning" style="display: none;">
					<span class="close" data-entity="basket-items-warning-notification-close">&times;</span>
					<div data-entity="basket-general-warnings"></div>
					<div data-entity="basket-item-warnings">
						<?=Loc::getMessage('SBB_BASKET_ITEM_WARNING')?>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="basket-items-list-wrapper basket-items-list-wrapper-height-fixed basket-items-list-wrapper-light<?=$displayModeClass?>"
					id="basket-items-list-wrapper">
					<div class="basket-items-list-header" data-entity="basket-items-list-header">
						<div class="basket-items-search-field" data-entity="basket-filter">
							<div class="form has-feedback">
								<input type="text" class="form-control"
									placeholder="<?=Loc::getMessage('SBB_BASKET_FILTER')?>"
									data-entity="basket-filter-input">
								<span class="form-control-feedback basket-clear" data-entity="basket-filter-clear-btn"></span>
							</div>
						</div>
						<div class="basket-items-list-header-filter">
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item active"
								data-entity="basket-items-count" data-filter="all" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item"
								data-entity="basket-items-count" data-filter="similar" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item"
								data-entity="basket-items-count" data-filter="warning" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item"
								data-entity="basket-items-count" data-filter="delayed" style="display: none;"></a>
							<a href="javascript:void(0)" class="basket-items-list-header-filter-item"
								data-entity="basket-items-count" data-filter="not-available" style="display: none;"></a>
						</div>
					</div>
					<div class="basket-items-list-container" id="basket-items-list-container">
						<div class="basket-items-list-overlay" id="basket-items-list-overlay" style="display: none;"></div>
						<div class="basket-items-list" id="basket-item-list">
							<div class="basket-search-not-found" id="basket-item-list-empty-result" style="display: none;">
								<div class="basket-search-not-found-icon"></div>
								<div class="basket-search-not-found-text">
									<?=Loc::getMessage('SBB_FILTER_EMPTY_RESULT')?>
								</div>
							</div>

							<table class="basket-items-list-table" id="basket-item-table"></table>

                            <!--<div class="purchase-content open" id="basket-item-table"></div>-->
						</div>

					</div>
				</div>
			</div>
		</div>

		<?
		if (
			$arParams['BASKET_WITH_ORDER_INTEGRATION'] !== 'Y'
			&& in_array('bottom', $arParams['TOTAL_BLOCK_DISPLAY'])
		)
		{
			?>
			<div class="row">
				<div class="col-xs-12" data-entity="basket-total-block"></div>
			</div>
			<?
		}
		?>
	</div>
	<?
	if (!empty($arResult['CURRENCIES']) && Main\Loader::includeModule('currency'))
	{
		CJSCore::Init('currency');

		?>
		<script>
			BX.Currency.setCurrencies(<?=CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)?>);
		</script>
		<?
	}
*/
	$signer = new \Bitrix\Main\Security\Sign\Signer;
	$signedTemplate = $signer->sign($templateName, 'sale.basket.basket');
	$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.basket.basket');
	$messages = Loc::loadLanguageFile(__FILE__);
	?>
	<script>
		BX.message(<?=CUtil::PhpToJSObject($messages)?>);
		BX.Sale.BasketComponent.init({
			result: <?=CUtil::PhpToJSObject($arResult, false, false, true)?>,
			params: <?=CUtil::PhpToJSObject($arParams)?>,
			template: '<?=CUtil::JSEscape($signedTemplate)?>',
			signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
			siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
			ajaxUrl: '<?=CUtil::JSEscape($component->getPath().'/ajax.php')?>',
			templateFolder: '<?=CUtil::JSEscape($templateFolder)?>'
		});
	</script>
	<?
	if ($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'BOTTOM')
	{
		$APPLICATION->IncludeComponent(
			'bitrix:sale.gift.basket',
			'.default',
			$giftParameters,
			$component
		);
	}
	?>

    <section class="cart">
        <div class="wrapper">
            <h1>Корзина</h1>
            <div class="purchase">
                <div class="purchase-head">
                    <h2>Покупка</h2>
                    <button class="show-btn purchase-block">Свернуть</button>
                </div>
                <div class="purchase-content open">

                    <?foreach ($arResult["BASKET_ITEM_RENDER_DATA"] as $arItem):?>

                        <div class="purchase-content__item">
                            <div class="item-img">
                                <?if(!empty($arItem["IMAGE_URL"])):?>
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["IMAGE_URL"]?>" alt=""></a>
                                <?else:?>
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$templateFolder?>/images/no_photo.png" alt=""></a>
                                <?endif;?>
                                <span class="favorite">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                                    </span>
                            </div>
                            <div class="item-info">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-info__head">
                                    <?$descr = "";?>
                                    <?foreach ($arItem["COLUMN_LIST"] as $prop) {
                                        if($prop["CODE"] == "PREVIEW_TEXT"){
                                            $descr = ", ".$prop["VALUE"];
                                        }
                                    }?>
                                    <?=$arItem["NAME"].$descr;?>
                                </a>
                                <div class="item-info__content">
                                    <ul>
                                        <li>Артикул: B25667-C3497-A375</li>
                                        <li>PartNumber: B25667C3497A375</li>
                                        <li>Ном. номер: 9000145214</li>
                                        <li>Производитель: intel</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item-available">
                                <span class="item-section__head">Доступно</span>
                                <div class="avaliable"><span><?=$arItem["AVAILABLE_QUANTITY"];?></span> шт.</div>
                            </div>
                            <div class="item-pricends">
                                <span class="item-section__head">Цена с НДС</span>
                                <div class="prices">
                                    <div class="item-price__quantity">
                                        <ul>
                                            <li>1 шт.</li>
                                            <li>50 шт.</li>
                                            <li>100 шт.</li>
                                        </ul>
                                    </div>
                                    <div class="item-price__cost">
                                        <ul>
                                            <li>50,50</li>
                                            <li>46,30</li>
                                            <li>40,00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-quantity">
                                <span class="item-section__head">Количество</span>
                                <form action="">
                                    <div class="quantity-num" data-entity="basket-item-quantity-block">
                                        <span class="decr quantity-button"  data-entity="basket-item-quantity-minus">&ndash;</span>
                                        <input value="<?=$arItem["QUANTITY"];?>" max="<?=$arItem["AVAILABLE_QUANTITY"];?>" id="basket-item-quantity-<?=$arItem["AVAILABLE_ID"];?>" data-entity="basket-item-quantity-field" data-value="<?=$arItem["QUANTITY"];?>" onchange="BX.proxy(this.quantityChange, this)">
                                        <span class="inc quantity-button" data-entity="basket-item-quantity-plus" onclick="skuPropClickHandler();">+</span>
                                    </div>

                                </form>
                            </div>
                            <div class="item-total">
                                <span class="item-section__head">Итого</span>
                                <div>
                                    <span class="item-total__price" id="basket-item-sum-price-14"><?=$arItem["SUM_FULL_PRICE_FORMATED"];?></span>
                                    <span class="item-total__value">руб.</span>
                                </div>
                            </div>
                            <div class="item-delete">
                                <button class="close-btn">
                                    <span></span><span></span>
                                </button>
                            </div>
                        </div>

                    <?endforeach;?>

                </div>
                <div class="total-cost">
                    <span>Итого: </span><span>4 позиции на сумму <span>77564,43</span> руб.</span>
                </div>

            </div>
            <div class="samples">
                <div class="samples-head">
                    <h2>Заказ образцов</h2>
                    <button class="show-btn samples-block">Свернуть</button>
                </div>
                <div class="samples-content open">
                    <div class="sample-content__item">
                        <div class="item-img">
                            <a href="#"><img src="images/itemimg.png" alt=""></a>
                            <span class="favorite">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                                </span>
                        </div>
                        <div class="item-info">
                            <a href="#" class="item-info__head">
                                A3977SLPTR-T, DMOS-драйвер шагового двигателя с переводчиком встроенныуй в плату [TSSOP-28]
                            </a>
                            <div class="item-info__content">
                                <ul>
                                    <li>Артикул: B25667-C3497-A375</li>
                                    <li>PartNumber: B25667C3497A375</li>
                                    <li>Ном. номер: 9000145214</li>
                                    <li>Производитель: intel</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item-quantity">
                            <span class="item-section__head">Количество</span>
                            <form action="">
                                <div class="quantity-num ">
                                    <span class="decr quantity-button">&ndash;</span>
                                    <input value="1" max="99999">
                                    <span class="inc quantity-button">+</span>
                                </div>

                            </form>
                        </div>
                        <div class="item-project">
                            <span class="item-section__head">Проект</span>
                            <div class="nothing">Нет ни одного проекта</div>
                            <div class="project-footer">
                                <a href="#modal-project" rel="modal:open" >Создать</a>

                            </div>
                        </div>

                        <div class="item-delete">
                            <button class="close-btn">
                                <span></span><span></span>
                            </button>
                        </div>
                    </div>
                    <div class="sample-content__item">
                        <div class="item-img">
                            <a href="#"><img src="images/itemimg.png" alt=""></a>
                            <span class="favorite">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                                </span>
                        </div>
                        <div class="item-info">
                            <a href="#" class="item-info__head">
                                A3977SLPTR-T, DMOS-драйвер шагового двигателя с переводчиком встроенныуй в плату [TSSOP-28]
                            </a>
                            <div class="item-info__content">
                                <ul>
                                    <li>Артикул: B25667-C3497-A375</li>
                                    <li>PartNumber: B25667C3497A375</li>
                                    <li>Ном. номер: 9000145214</li>
                                    <li>Производитель: intel</li>
                                </ul>
                            </div>
                        </div>
                        <div class="item-quantity">
                            <span class="item-section__head">Количество</span>
                            <form action="">
                                <div class="quantity-num">
                                    <span class="decr quantity-button">&ndash;</span>
                                    <input value="1" max="99999">
                                    <span class="inc quantity-button">+</span>
                                </div>

                            </form>
                        </div>
                        <div class="item-project">
                            <span class="item-section__head">Проект</span>
                            <div class="project-select">
                                <!-- Добавить класс error к элементу ниже для подсвечивания красным  -->
                                <div class="project-select__btn">Выберите проект</div>

                                <div class="project-select__options">
                                    <div class="options">
                                            <span class="option">
                                                <label><input type="radio" name="project-option" >Новый проект 1</label>
                                            </span>
                                        <span class="option">
                                                <label><input type="radio" name="project-option" >Новый проект 2</label>
                                            </span>
                                        <span class="option">
                                                <label><input type="radio" name="project-option" >Новый проект 3</label>
                                            </span>
                                        <span class="option">
                                                <label><input type="radio" name="project-option" >Новый проект 4</label>
                                            </span>
                                    </div>
                                    <div class="new-option">
                                        <a href="#modal-project" rel="modal:open" class="new-option__btn"><span>+</span>Добавить проект</a>
                                    </div>
                                </div>
                                <div class="project-footer">
                                    <a href="#modal-project" rel="modal:open">Создать</a>
                                    <span>Редактировать</span>
                                </div>
                            </div>
                            <div class="project-select__error">Для заказа образцов необходимо указать проект</div>
                        </div>

                        <div class="item-delete">
                            <button class="close-btn">
                                <span></span><span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cart-total">
                <div class="total-cost__head">Итого:</div>
                <div class="total-cost__wrapper">
                    <div class="total-col">
                        <div class="total-col__head">Закупка:</div>
                        <div class="total-col__content">
                            <span>4 позиции</span>
                            <span> 7 999,99 руб</span>
                        </div>
                    </div>
                    <div class="total-col">
                        <div class="total-col__head">Запрос образца:</div>
                        <div class="total-col__content">
                            <span>4 позиции</span>
                        </div>
                    </div>
                </div>
                <a href="#" class="ordering-btn">Перейти к оформлению</a>
                <a href="#modal-thanks" class="ordering-btn" rel="modal:open">Вызывать модалку с спасибо</a>
            </div>
        </div>


    </section>
	<?

}
else
{
	ShowError($arResult['ERROR_MESSAGE']);
}