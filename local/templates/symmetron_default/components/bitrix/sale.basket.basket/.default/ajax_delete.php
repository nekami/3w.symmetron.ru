<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("mcart_catalog_quantity");

if(CModule::IncludeModule("sale"))
{
    $id = htmlspecialchars($_POST["DELETE"]["ID"]);
    if(!empty($id)){
        CSaleBasket::Delete($id);
    }

    $idparent = htmlspecialchars($_POST["DELETE"]["PARENT_ID"]);
    if(!empty($idparent)){
        CSaleBasket::Delete($idparent);
    }

}

$_POST["PARAMS"]["AJAX_NOW"] = true;
$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "",
    $_POST["PARAMS"]
);

die;