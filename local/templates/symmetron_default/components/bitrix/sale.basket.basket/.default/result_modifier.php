<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */

/** @var array $arResult */

use Bitrix\Main;

$defaultParams = array(
    'TEMPLATE_THEME' => 'blue'
);
$arParams = array_merge($defaultParams, $arParams);
unset($defaultParams);

$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME']) {
    $arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
    if ('site' == $arParams['TEMPLATE_THEME']) {
        $templateId = (string)Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', SITE_ID);
        $templateId = (preg_match("/^eshop_adapt/", $templateId)) ? 'eshop_adapt' : $templateId;
        $arParams['TEMPLATE_THEME'] = (string)Main\Config\Option::get('main', 'wizard_' . $templateId . '_theme_id', 'blue', SITE_ID);
    }
    if ('' != $arParams['TEMPLATE_THEME']) {
        if (!is_file($_SERVER['DOCUMENT_ROOT'] . $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css'))
            $arParams['TEMPLATE_THEME'] = '';
    }
}
if ('' == $arParams['TEMPLATE_THEME'])
    $arParams['TEMPLATE_THEME'] = 'blue';

//вытаскиваем все цены


if (!empty($arParams["CODE_ARTIKUL"])) {
    $arParams["CODE_ARTIKUL"] = trim($arParams["CODE_ARTIKUL"]);
}
if (!empty($arParams["CODE_PARTNUMBER"])) {
    $arParams["CODE_PARTNUMBER"] = trim($arParams["CODE_PARTNUMBER"]);
}
if (!empty($arParams["CODE_NOMNUMBER"])) {
    $arParams["CODE_NOMNUMBER"] = trim($arParams["CODE_NOMNUMBER"]);
}
if (!empty($arParams["CODE_PRODUC"])) {
    $arParams["CODE_PRODUC"] = trim($arParams["CODE_PRODUC"]);
}

$arProductsIDs = array();

$arBrandIDs = [];
foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key => $arItem) {
    $arProductsIDs[] = $arItem["PRODUCT_ID"];
    $arPropItem = array();
    if (!empty($arParams["CODE_ARTIKUL"])) {
        foreach ($arItem["COLUMN_LIST"] as $arProp) {
            if ($arProp["CODE"] == "PROPERTY_" . $arParams["CODE_ARTIKUL"] . "_VALUE") {
                $arPropItem[$arProp["CODE"]] = $arProp;
                break;
            }
        }
    }

    if (!empty($arParams["CODE_PARTNUMBER"])) {
        foreach ($arItem["COLUMN_LIST"] as $arProp) {
            if ($arProp["CODE"] == "PROPERTY_" . $arParams["CODE_PARTNUMBER"] . "_VALUE") {
                $arPropItem[$arProp["CODE"]] = $arProp;
                break;
            }
        }
    }

    if (!empty($arParams["CODE_NOMNUMBER"])) {
        foreach ($arItem["COLUMN_LIST"] as $arProp) {
            if ($arProp["CODE"] == "PROPERTY_" . $arParams["CODE_NOMNUMBER"] . "_VALUE") {
                $arPropItem[$arProp["CODE"]] = $arProp;
                break;
            }
        }
    }


    if (!empty($arParams["CODE_PRODUC"])) {
        foreach ($arItem["COLUMN_LIST"] as $arProp) {

            if ($arProp["CODE"] == "PROPERTY_" . $arParams["CODE_PRODUC"] . "_VALUE") {
                $arPropItem[$arProp["CODE"]] = $arProp;
                $arBrandIDs[] = $arProp["VALUE"];
                break;
            }

        }
    }

    $arResult['BASKET_ITEM_RENDER_DATA'][$key]["PROPS_ITEM"] = $arPropItem;
    unset($arPropItem);

}

//get brands
if (!empty($arBrandIDs)) {

    $arBrands = [];

    $arSelect = ["ID", "NAME", "ACTIVE", "PROPERTY_ACTIVE", "CODE"];
    $arFilter = ["ID" => $arBrandIDs];
    $rsBrands = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($arBrand = $rsBrands->Fetch()) {

        if ($arBrand["ACTIVE"] == "Y") {
            $arBrands[$arBrand["ID"]]["VALUE"] = $arBrand["NAME"];
            $arBrands[$arBrand["ID"]]["LINK"] = "/brands/" . $arBrand["CODE"] . "/";
        } elseif ($arBrand["PROPERTY_ACTIVE_VALUE"] == "Y") {
            $arBrands[$arBrand["ID"]]["VALUE"] = $arBrand["NAME"];
        }
    }

    $arResult["BRANDS_DATA"] = $arBrands;
}


$arResult["CHECK_PRICE_ARRAY"] = array();
if (!empty($arResult["BASKET_ITEM_RENDER_DATA"])) {
    foreach ($arResult["BASKET_ITEM_RENDER_DATA"] as $arItem) {
        $arResult["CHECK_PRICE_ARRAY"][$arItem["ID"]]["FLAG"] = $arItem["CHECK_MAX_QUANTITY"];
        $arResult["CHECK_PRICE_ARRAY"][$arItem["ID"]]["AVAILABLE_QUANTITY"] = $arItem["AVAILABLE_QUANTITY"];

    }
}

//counters for show quantity
$arResult["BASKET_ITEM_RENDER_DATA_COUNT"] = 0;
$arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"] = 0;
$arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"] = 0;

if (!empty($arResult['BASKET_ITEM_RENDER_DATA'])) {
    $arIDs = array();

    $arResult["BASKET_ITEM_RENDER_DATA_EX"] = array();
    $arResult["BASKET_ITEM_RENDER_DATA_REQ"] = array();

    foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key => $arData) {

        if ($arData["PROPS_ALL"]["EXAMPLE"]["VALUE"] == "Y") {

            $arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"]++;
            $arResult["BASKET_ITEM_RENDER_DATA_EX"][] = $arData;
            unset($arResult['BASKET_ITEM_RENDER_DATA'][$key]);
            continue;

        } elseif (!empty($arData["PROPS_ALL"]["REQUEST"]["VALUE"])) {
            $arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"]++;
        } else {
            if ($arData["PROPS_ALL"]["HIDDEN"]["VALUE"] !== "Y")
                $arResult["BASKET_ITEM_RENDER_DATA_COUNT"]++;
        }

        /*if(!empty($arData["PROPS_ALL"]["REQUEST"]["VALUE"])){

            $arResult["BASKET_ITEM_RENDER_DATA_REQ"][] = $arData;
            unset($arResult['BASKET_ITEM_RENDER_DATA'][$key]);
            continue;

        }*/


        $arIDs[] = $arData["PRODUCT_ID"];
    }

    if (!empty($arIDs)) {

        if (!Cmodule::IncludeModule("catalog"))
            return false;

        $db_result_lang = CCurrencyLang::GetByID($arResult["CURRENCY"], LANGUAGE_ID);
        //print_r($db_result_lang);
        $format = $db_result_lang["FORMAT_STRING"];

        $db_res = CPrice::GetListEx(
            array("QUANTITY_FROM" => "ASC"),
            array(
                "PRODUCT_ID" => $arIDs,
                //"CATALOG_GROUP_ID" => 1, //ID типа цены
                //"CURRENCY" => "RUB" // Валюта
            )
        );

        $arPrices = array();

        while ($ar_res = $db_res->Fetch()) {

            $ar_res["PRICE"] = round(CCurrencyRates::ConvertCurrency($ar_res["PRICE"], $ar_res["CURRENCY"], $arResult["CURRENCY"]), $db_result_lang["DECIMALS"]);
            $ar_res["CURRENCY"] = $arResult["CURRENCY"];
            $ar_res["PRICE_PRINT"] = str_replace("#", $ar_res["PRICE"], $format);
            $arPrices[$ar_res["PRODUCT_ID"]][] = $ar_res;

        }
    }

    $items = &$arResult['BASKET_ITEM_RENDER_DATA'];
    $itemIds = [];
    foreach ($items as $key => $arData) {
        $itemIds[$key] = $arData["PRODUCT_ID"];
    }

    //get measure info and data of packing
    $arPropValues = array();
    if (!empty($itemIds)) {
        $result = \Bitrix\Catalog\ProductTable::getList([
            "filter" => ["ID" => $itemIds],
            "select" => ["ID", "MEASURE"],
        ]);
        while ($row = $result->fetch()) {
            $index = array_search($row["ID"], $itemIds);
            foreach ($items as $index => $item) {
                if ($item["PRODUCT_ID"] != $row["ID"]) {
                    continue;
                }
                $items[$index]["ITEM_MEASURE"] = ["ID" => $row["MEASURE"]];
                //break;
            }
        }

        if (CModule::IncludeModule("iblock")) {

            $IBLOCK_ID = 4;

            $rsPacEnumValues = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => "UPAK"));
            $arPacEnumValues = array();
            while ($arPacEnumValue = $rsPacEnumValues->Fetch()) {
                $arPacEnumValues[$arPacEnumValue["ID"]] = $arPacEnumValue;
            }

            $rsProps = Bitrix\Iblock\PropertyTable::getList(array(
                "filter" => array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => array("SYMPACKAGE", "UPAK")),
                "select" => array("ID", "CODE")
            ));

            $arPropsIDs = array();
            while ($arProp = $rsProps->Fetch()) {
                $arPropsIDs[$arProp["ID"]] = $arProp["CODE"];
            }

            if (!empty($arPropsIDs)) {
                $rsPropValues = Mcart\BasketSymmetron\ElementPropertyTable::getList(array(
                    "filter" => array("IBLOCK_PROPERTY_ID" => array_keys($arPropsIDs), "IBLOCK_ELEMENT_ID" => $itemIds),
                    "select" => array("ID", "VALUE", "IBLOCK_ELEMENT_ID", "IBLOCK_PROPERTY_ID")
                ));

                while ($arPropValue = $rsPropValues->Fetch()) {
                    if ($arPropsIDs[$arPropValue["IBLOCK_PROPERTY_ID"]] == "UPAK") {
                        $arPropValues[$arPropValue["IBLOCK_ELEMENT_ID"]][$arPropsIDs[$arPropValue["IBLOCK_PROPERTY_ID"]]]
                            = $arPacEnumValues[$arPropValue["VALUE"]];
                    } else {
                        $arPropValues[$arPropValue["IBLOCK_ELEMENT_ID"]][$arPropsIDs[$arPropValue["IBLOCK_PROPERTY_ID"]]]
                            = $arPropValue["VALUE"];
                    }
                }

            }
        }

    }


    //get detail measure data
    $measure = [];
    $arMeasure = [];
    if (\Bitrix\Main\Loader::includeModule("mcart.symmetronsyncdb")) {
        $measure_fractional = \Bitrix\Main\Config\Option::get('mcart.symmetronsyncdb', 'measure_fractional', '');
        $measure_fractional = explode(",", $measure_fractional);

        $resMeasure = \CCatalogMeasure::getList(array(), array(), false);
        while ($row = $resMeasure->fetch()) {
            $arMeasure[$row["ID"]] = $row;
            $measure[$row["ID"]] = array();
            foreach ($measure_fractional as $code) {
                if ($row["CODE"] == $code) {
                    $measure[$row["ID"]] = $row;
                    break;
                }
            }
        }
    }

    foreach ($items as $key => $arData) {

        $items[$key]["MEASURE_INFO"] = $arMeasure[$arData["ITEM_MEASURE"]["ID"]] ?
            $arMeasure[$arData["ITEM_MEASURE"]["ID"]] :
            array("MEASURE_TITLE" => "Штука", "SYMBOL_RUS" => "шт.");

        $items[$key]["PACKING"] = $arPropValues[$arData["PRODUCT_ID"]];

        if (!isset($arData["ITEM_MEASURE"]["ID"]) || empty($measure[$arData["ITEM_MEASURE"]["ID"]]))
            $items[$key]["MEASURE_RATIO"] = intval($arData["MEASURE_RATIO"]);
        $items[$key]["PRICES_PROD"] = $arPrices[$arData["PRODUCT_ID"]];

        $min_quantity = 1;
        $ratio_quantity = $arData["MEASURE_RATIO"] > 0 ? $arData["MEASURE_RATIO"] : 1;
        if (is_array($ar = reset($arPrices[$arData["PRODUCT_ID"]])))
            $min_quantity = $ar["MIN_QUANTITY"];
        foreach ($arData["COLUMN_LIST"] as $property)
            if ($property["CODE"] == "PROPERTY_PURCHQTYLOWEST_VALUE") {
                if ($min_quantity < $property["VALUE"])
                    $min_quantity = $property["VALUE"];
                break;
            }
        if (empty($ratio_quantity)) $ratio_quantity = 1;
        $min_quantity = ceil($min_quantity / $ratio_quantity) * $ratio_quantity;
        if (empty($min_quantity)) $min_quantity = $ratio_quantity;
        $items[$key]["MIN_QUANTITY"] = $min_quantity;

        if (count($arPrices[$arData["PRODUCT_ID"]]) == 1 && empty(reset($arPrices[$arData["PRODUCT_ID"]])["PRICE"]))
            unset($items[$key]["PRICES_PROD"]);

        if (!empty($arData["PROPS_ALL"]["REQUEST"]["VALUE"])) {

            $arResult["BASKET_ITEM_RENDER_DATA_REQ"][] = $items[$key];
            unset($items[$key]);
            continue;

        }

    }

    //echo "<pre>";
    //print_r($arResult['BASKET_ITEM_RENDER_DATA']);
    //echo "</pre>";

    /*foreach ($arResult['BASKET_ITEM_RENDER_DATA_EX'] as $key=>$arData) {
        $min_quantity = 1;
		$ratio_quantity = $arData["MEASURE_RATIO"] > 0 ? $arData["MEASURE_RATIO"] : 1;
        foreach ($arData["COLUMN_LIST"] as $property)
            if ($property["CODE"] == "PROPERTY_PURCHQTYLOWEST_VALUE")
            {
                if ($min_quantity < $property["VALUE"])
                    $min_quantity = $property["VALUE"];
                break;
            }
		if (empty($ratio_quantity)) $ratio_quantity = 1;
        $min_quantity = ceil($min_quantity / $ratio_quantity) * $ratio_quantity;
		if (empty($min_quantity)) $min_quantity = $ratio_quantity;
        $arResult["BASKET_ITEM_RENDER_DATA_EX"][$key]["MIN_QUANTITY"] = $min_quantity;
    } */


    /*foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key => $arData) {


        if(!empty($arData["PROPS_ALL"]["REQUEST"]["VALUE"])){

            $arResult["BASKET_ITEM_RENDER_DATA_REQ"][] = $arData;
            unset($arResult['BASKET_ITEM_RENDER_DATA'][$key]);
            continue;

        }


        //$arIDs[] = $arData["PRODUCT_ID"];
    }*/


}

if (!empty($arParams["IBLOCK_PROJ_ID"])) {
    $arParams["IBLOCK_PROJ_ID"] = intval(trim($arParams["IBLOCK_PROJ_ID"]));
} else {
    $arParams["IBLOCK_PROJ_ID"] = 13;
}

global $USER;
if ($USER->IsAuthorized()) {
    //$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", $USER->GetID());
    //$arValuesProj = $arUF["UF_PROJECTS"]["VALUE"];

    //if (!empty($arValuesProj)) {

    $arProjValuesForSelect = array();
    $arProjValuesForSelectFull = array();
    if (CModule::IncludeModule("iblock") && !empty($arParams["IBLOCK_PROJ_ID"])) {
        global $USER;
        $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_PROJ_ID"], "CREATED_BY" => $USER->GetID()), false, false, Array("NAME", "ID", "PROPERTY_PRODUCT", "PROPERTY_START", "PROPERTY_QUANT_EX", "PREVIEW_TEXT"));
        while ($ob = $res->Fetch()) {

            $arProjValuesForSelect[$ob["ID"]] = $ob["NAME"];
            $arProjValuesForSelectFull[$ob["ID"]] = $ob;

        }

        $arResult["PROJ_VAL_FOR_SELECT"] = $arProjValuesForSelect;
        $arResult["PROJ_VAL_FOR_SELECT_FULL"] = $arProjValuesForSelectFull;
    }

    if (empty($arResult["PROJ_VAL_FOR_SELECT"]))
        $arResult["PROJ_VAL_FOR_SELECT"] = "";

    if (empty($arResult["PROJ_VAL_FOR_SELECT_FULL"]))
        $arResult["PROJ_VAL_FOR_SELECT_FULL"] = "";
    //}
}

//echo "<pre>";
//print_r($arResult);
//echo "</pre>";


//get sections names

if (!empty($arProductsIDs)) {
    //echo "<pre>";
    //print_r($arProductsIDs);
    //echo "</pre>";

    $rsElemSect = Bitrix\Iblock\SectionElementTable::getList(array(
        "filter" => array("IBLOCK_ELEMENT_ID" => $arProductsIDs),
        "select" => array("IBLOCK_ELEMENT_ID", "IBLOCK_SECTION_ID")
    ));

    $arElemSect = array();
    while ($arR = $rsElemSect->Fetch()) {
        $arElemSect[$arR["IBLOCK_ELEMENT_ID"]] = $arR["IBLOCK_SECTION_ID"];
    }

    if (!empty($arElemSect)) {

        $rsSections = Bitrix\Iblock\SectionTable::getList(array(
            "filter" => array("ID" => $arElemSect),
            "select" => array("ID", "NAME")
        ));

        $arSections = array();
        while ($arSection = $rsSections->Fetch()) {
            $arSections[$arSection["ID"]] = $arSection["NAME"];

        }

        $arResult["SECTIONS"] = array("NAMES" => $arSections, "IDS" => $arElemSect);

    }


}
//$_SESSION["BASKET"]["PRODUCTS_BLOCK"] = "Y";
if ($_SESSION["BASKET"]["PRODUCTS_BLOCK"] == "Y") {
    $arResult["PRODUCTS_BLOCK"] = true;
}
if ($_SESSION["BASKET"]["SAMPLES_BLOCK"] == "Y") {
    $arResult["SAMPLES_BLOCK"] = true;
}
if ($_SESSION["BASKET"]["REQUESTS_BLOCK"] == "Y") {
    $arResult["REQUESTS_BLOCK"] = true;
}

// get information for declination

\Bitrix\Main\Loader::includeModule("mcart.basketsymmetron");

$arWords = [];

$arWords[] = GetMessage("SBB_R_POSITION_1");

$arResult["ITEMS_JS"] = [];
$products = [];



if(!empty($arResult["BASKET_ITEM_RENDER_DATA"]) || !empty($arResult["BASKET_ITEM_RENDER_DATA_REQ"]) || !empty($arResult["BASKET_ITEM_RENDER_DATA_EX"])){
    $res = CIBlockProperty::GetByID("PRODUCERID");
    $PRODUCERID_IBLOCK_ID = false;
    if($ar_res = $res->GetNext()){
        $PRODUCERID_IBLOCK_ID = $ar_res["LINK_IBLOCK_ID"];
    }

    $arrIds = array_merge(array_column($arResult["BASKET_ITEM_RENDER_DATA"], 'PRODUCT_ID'), array_column($arResult["BASKET_ITEM_RENDER_DATA_REQ"], 'PRODUCT_ID'), array_column($arResult["BASKET_ITEM_RENDER_DATA_EX"], 'PRODUCT_ID'));
    $res = CIBlockElement::GetList(Array("SORT" => "ASC", "PROPERTY_PRIORITY" => "ASC"), ["ID" => $arrIds], ["ID", "IBLOCK_SECTION_ID", "PROPERTY_PRODUCERID"]);
    while ($ar_fields = $res->Fetch()) {
        $products[$ar_fields["ID"]] = $ar_fields;
    }

    $PRODUCERID_IDs = array_column($products, 'PROPERTY_PRODUCERID_VALUE');
    $PRODUCERID_NAMES = [];
    if($PRODUCERID_IBLOCK_ID !== false && !empty($PRODUCERID_IDs)){
        $res = CIBlockElement::GetList(Array(), array("ID" => $PRODUCERID_IDs, "IBLOCK_ID" => $PRODUCERID_IBLOCK_ID), false, false, array("ID", "NAME"));
        while ($ob = $res->Fetch()) {
            $PRODUCERID_NAMES[$ob["ID"]] = $ob["NAME"];
        }
    }

    foreach ($products as $value){
        $arFilterSection = ["ID" => $value["IBLOCK_SECTION_ID"], "ACTIVE" => "Y"];
        $products[$value["ID"]]["BREAD"] = [];
        $rsSectionInfo = \CIBlockSection::GetList([], $arFilterSection, false, ["ID", "NAME", "LEFT_MARGIN", "IBLOCK_ID", "RIGHT_MARGIN"]);
        if ($arSectionInfo = $rsSectionInfo->Fetch()) {
            $rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), array(
                'IBLOCK_ID' => $arSectionInfo["IBLOCK_ID"],
                "<=LEFT_BORDER" => $arSectionInfo["LEFT_MARGIN"],
                ">=RIGHT_BORDER" => $arSectionInfo["RIGHT_MARGIN"],
            ));

            while($arSect = $rsSect->Fetch())
            {
                $products[$value["ID"]]["BREAD"][] = $arSect['NAME'];
            }
        }
        $products[$value["ID"]]["BREAD"] = join("/", $products[$value["ID"]]["BREAD"]);

        $products[$value["ID"]]["BRAND"] = $PRODUCERID_NAMES[$value["PROPERTY_PRODUCERID_VALUE"]];
    }
}

foreach ($arResult["BASKET_ITEM_RENDER_DATA"] as $arItem) {
    $arResult["ITEMS_JS"][$arItem["ID"]] = [
        "NAME" => $arItem["NAME"],
        "ID" => $arItem["PRODUCT_ID"],
        "PRICE" => $arItem["PRICE"],
        "BRAND" => $products[$arItem["PRODUCT_ID"]]["BRAND"],
        "BREAD" => $products[$arItem["PRODUCT_ID"]]["BREAD"],
        "QUANTITY" => $arItem["QUANTITY"],
    ];

    if (!empty($arItem["MEASURE_INFO"]))
        $arWords[] = strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]);

    if (!empty($arItem["PACKING"]["UPAK"]))
        $arWords[] = strtolower($arItem["PACKING"]["UPAK"]["VALUE"]);
}

foreach ($arResult["BASKET_ITEM_RENDER_DATA_REQ"] as $arItem) {
    $arResult["ITEMS_JS"][$arItem["ID"]] = [
        "NAME" => $arItem["NAME"],
        "ID" => $arItem["PRODUCT_ID"],
        "PRICE" => $arItem["PRICE"],
        "BRAND" => $products[$arItem["PRODUCT_ID"]]["BRAND"],
        "BREAD" => $products[$arItem["PRODUCT_ID"]]["BREAD"],
        "QUANTITY" => $arItem["QUANTITY"],
    ];

    if (!empty($arItem["MEASURE_INFO"]))
        $arWords[] = strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]);

    if (!empty($arItem["PACKING"]["UPAK"]))
        $arWords[] = strtolower($arItem["PACKING"]["UPAK"]["VALUE"]);
}

foreach ($arResult["BASKET_ITEM_RENDER_DATA_EX"] as $arItem) {
    $arResult["ITEMS_JS"][$arItem["ID"]] = [
        "NAME" => $arItem["NAME"],
        "ID" => $arItem["PRODUCT_ID"],
        "PRICE" => $arItem["PRICE"],
        "BRAND" => $products[$arItem["PRODUCT_ID"]]["BRAND"],
        "BREAD" => $products[$arItem["PRODUCT_ID"]]["BREAD"],
        "QUANTITY" => $arItem["QUANTITY"],
    ];

    if (!empty($arItem["MEASURE_INFO"]))
        $arWords[] = trim(strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]));

    if (!empty($arItem["PACKING"]["UPAK"]))
        $arWords[] = trim(strtolower($arItem["PACKING"]["UPAK"]["VALUE"]));
}

$arResult["DECLINATION_OBJ"] = new \Mcart\BasketSymmetron\SymmetronDeclination($arWords);
