function declineWordPosition(quantVal, arPsositionVariants, arPsositionVariantsExaption) {
    var mod = parseInt(quantVal) % 10;
    var posWord = "";
    if (arPsositionVariantsExaption.indexOf(quantVal) != -1)
        posWord = arPsositionVariants["5"];
    else
        posWord = arPsositionVariants[mod];

    return posWord;
}


function priceDistr(str) {

    var arPrice = str.split(" ");
    var elemVal = arPrice.length - 1;
    var valuta = arPrice[elemVal];
    delete arPrice[elemVal];
    var summa = arPrice.join(" ");
    return [summa, valuta];
}

function getCheckedRadioValue(name) {
    var elements = document.getElementsByName(name);
    for (var i = 0, len = elements.length; i < len; ++i)
        if (elements[i].checked) return elements[i].value;
}

getCheckedRadioValue("project-option");

function projUpdate(id, value) {

    BX.ajax.get(
        templateFolder + "/ajax_proj_add_for_elem.php",
        {"id": id, "value": value},
        function (data) {
            document.getElementById("select-proj-value-naw-" + id).setAttribute("id-val", value);
            document.getElementById("redact-link-" + id).style.display = "";
            document.getElementById("project-select-error-" + id).style.display = "none";
        }
    );

}

/*
*
*  function for delete element
*
* */

function reqDel(id, idparent) {
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        'ecommerce': {
            'remove': {
                'products': [{
                    'name': mcart_basket_items[id]["NAME"],
                    'id': Number(id),
                    'price': Number(mcart_basket_items[id]["PRICE"]),
                    'brand': mcart_basket_items[id]["BRAND"],
                    'category': mcart_basket_items[id]["BREAD"],
                    'quantity': Number(mcart_basket_items[id]["QUANTITY"])
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',
    });

    //console.log("sdfsdfsdf")
    /*var quantObj = {};
    quantObj["DELETE_"+id] = "Y";

    if(idparent !== "")
        quantObj["DELETE_"+idparent] = "Y";

    var d = {
        basket: quantObj,
        basketAction: 'recalculateAjax',
        lastAppliedDiscounts: '1',
        sessid: sessID,
        signedParamsString: signedParams,
        site_id: 's1',
        template: signedTemplate,
        via_ajax: 'Y'

    };

    sendReq('recalculateAjax', d, id, true);*/

    var arReq = {"PARAMS": arParams, "DELETE": {"ID": id}}
    if (idparent) {
        arReq["DELETE"]["PARENT_ID"] = idparent
    }

    BX.ajax.post(
        templateFolder + "/ajax_delete.php",
        arReq,
        function (html) {
            if (html) {
                document.getElementById("cart-id").innerHTML = html
            }
        }
    )
}

/*
*
*  function for update element
*
* */

function req(id) {
    var quantity = document.getElementById("basket-item-quantity-d-" + id)
    var val = parseFloat(quantity.value)
    var min = parseFloat(document.getElementById("basket-item-quantity-min-" + id).value);
    var ratio = parseFloat(document.getElementById("basket-item-quantity-ratio-" + id).value);

    if (ratio > 0) {
        var mod = val % ratio
        if (mod) {
            val = val - mod + ratio
        }
    }

    if (val < 0 || !val) val = 0;
    if (min <= 0) min = 1;

    if (arCheckPrice[id].FLAG == "Y") {
        if (val > arCheckPrice[id].AVAILABLE_QUANTITY) {
            val = arCheckPrice[id].AVAILABLE_QUANTITY;
        }
    }

    if (val < min) {
        quantity.value = min;
        val = min;
    }

    quantity.value = val

    var quantObj = {};
    quantObj["QUANTITY_" + id] = val;
    //quantObj["PRICE_"+id] = 100;

    var d = {
        basket: quantObj,
        basketAction: 'refreshAjax',
        lastAppliedDiscounts: '1',
        sessid: sessID,
        signedParamsString: signedParams,
        site_id: 's1',
        template: signedTemplate,
        via_ajax: 'Y'

    };
    sendReq('recalculateAjax', d, id, false);
}

//pars basket
function sendReq(action, data, id, flagDelete) {
    //console.log(data)
    //console.log(id)
    var lastAction = action;

    BX.ajax({
        method: 'POST',
        dataType: 'json',
        url: componentPath + '/ajax.php',
        data: data,
        onsuccess: function (result) {
            //console.log(result)
            var countProd = 0;
            var countEx = 0;
            var countReq = 0;
            if (!flagDelete) {

                //for (var property1 in result.BASKET_DATA.BASKET_ITEM_RENDER_DATA) {

                if (!result.BASKET_DATA.GRID.ROWS[id].PROPS_ALL.EXAMPLE) {
                    var arItem = result.BASKET_DATA.GRID.ROWS[id]

                    var flagRequest = arItem.PROPS_ALL.REQUEST ? true : false

                    var arPriceElem = priceDistr(arItem.SUM_FULL_PRICE_FORMATED);
                    var item = document.getElementById("basket-item-sum-price-d-" + arItem.ID)
                    if (item)
                        item.innerHTML = arPriceElem[0];

                    //if quantity very big
                    var noQuantityText = document.getElementById(arItem.ID + "_no_quantity")
                    var yesQuantityText = document.getElementById(arItem.ID + "_yes_quantity")
                    var priceBlock = document.getElementById(arItem.ID + "_price_block")
                    var requestButton = document.getElementById(arItem.ID + "_request_button")
                    var requestAbortButton = document.getElementById(arItem.ID + "_request_abort_button")

                    var calculator = flagRequest ? document.getElementById(arItem.ID + "_calculator_req")
                        : document.getElementById(arItem.ID + "_calculator")
                    if (calculator) {
                        var dataPacQuant = calculator.getAttribute("data-pac-quant")
                        var dataPacType = calculator.getAttribute("data-pac-type")
                        var dataMeasure = calculator.getAttribute("data-measure")
                    }

                    if (arItem.AVAILABLE_QUANTITY >= arItem.QUANTITY) {

                        // calculator

                        if (calculator && !flagRequest) {

                            if (dataPacQuant > 0 && arItem.QUANTITY >= dataPacQuant) {
                                var htmlCalculator = parseInt(arItem.QUANTITY / dataPacQuant) + " " + objDel.declinationNum(dataPacType.toLowerCase(), parseInt(arItem.QUANTITY / dataPacQuant))
                                var mod = arItem.QUANTITY % dataPacQuant
                                if (mod > 0)
                                    htmlCalculator += "<br>" + mod + " " + objDel.declinationNum(dataMeasure.toLowerCase(), parseInt(mod))

                                calculator.innerHTML = htmlCalculator

                                calculator.style.display = ""
                            } else {
                                calculator.style.display = "none"
                            }

                        } else {
                            calculator.style.display = "none"
                        }


                        if (noQuantityText) {
                            if (noQuantityText.style.display !== "none") {
                                if (arItem.PROPS_ALL["HIDDEN"]) {
                                    if (arItem.PROPS_ALL.HIDDEN["VALUE"] !== "Y") {
                                        countMaxQuantity--;
                                    }
                                } else {
                                    countMaxQuantity--;
                                }
                                noQuantityText.style.display = "none"
                            }
                        }

                        if (yesQuantityText) {
                            if (yesQuantityText.style.display === "none") {
                                countMinQuantity++;
                                yesQuantityText.style.display = ""
                            }
                        }

                        if (priceBlock)
                            priceBlock.style.display = ""

                        if (requestButton)
                            requestButton.style.display = "none"

                        if (requestAbortButton)
                            requestAbortButton.style.display = ""

                    } else {

                        if (calculator && flagRequest) {

                            if (dataPacQuant > 0 && arItem.QUANTITY >= dataPacQuant) {
                                var htmlCalculator = "<span class='request-quantity-calculator__head'>В заказе</span>" +
                                    "<span class='request-quantity-calculator'>" + parseInt(arItem.QUANTITY / dataPacQuant) +
                                    " " + objDel.declinationNum(dataPacType.toLowerCase(), parseInt(arItem.QUANTITY / dataPacQuant))

                                var mod = arItem.QUANTITY % dataPacQuant
                                if (mod > 0)
                                    htmlCalculator += " и " + mod + " " + objDel.declinationNum(dataMeasure.toLowerCase(), parseInt(mod))
                                htmlCalculator += "</span>"

                                calculator.innerHTML = htmlCalculator

                                calculator.style.display = ""
                            } else {
                                calculator.style.display = "none"
                            }

                        } else {
                            calculator.style.display = "none"
                        }


                        if (noQuantityText) {
                            if (noQuantityText.style.display == "none") {
                                if (arItem.PROPS_ALL["HIDDEN"]) {
                                    if (arItem.PROPS_ALL.HIDDEN["VALUE"] !== "Y") {
                                        countMaxQuantity++;
                                    }
                                } else {
                                    countMaxQuantity++;
                                }
                                noQuantityText.style.display = ""
                            }
                        }

                        if (yesQuantityText) {
                            if (yesQuantityText.style.display !== "none") {
                                countMinQuantity--;
                                yesQuantityText.style.display = "none"
                            }
                        }

                        if (priceBlock)
                            priceBlock.style.display = "none"

                        if (requestButton)
                            requestButton.style.display = ""

                        if (requestAbortButton)
                            requestAbortButton.style.display = "none"
                    }


                    //quantities and prices

                    var arQuantities = document.getElementById(arItem.ID + "_all_quantities")

                    if (arQuantities) {

                        arQuantities = arQuantities.childNodes

                        for (var i in arQuantities) {

                            if (arQuantities[i].nodeName !== "LI")
                                continue

                            var quantityFrom = arQuantities[i].getAttribute("quantity-from")
                            var quantityTo = arQuantities[i].getAttribute("quantity-to")

                            if (quantityTo > 0) {
                                if ((quantityFrom <= arItem.QUANTITY) && (arItem.QUANTITY <= quantityTo))
                                    arQuantities[i].style = "font-weight: bold"
                                else
                                    arQuantities[i].style = "font-weight: normal"
                            } else {
                                if (quantityFrom <= arItem.QUANTITY)
                                    arQuantities[i].style = "font-weight: bold"
                                else
                                    arQuantities[i].style = "font-weight: normal"
                            }

                        }

                    }


                    var arPrises = document.getElementById(arItem.ID + "_all_prices")

                    if (arPrises) {

                        arPrises = arPrises.childNodes

                        for (var i in arPrises) {

                            if (arPrises[i].nodeName !== "LI")
                                continue

                            var quantityFrom = arPrises[i].getAttribute("quantity-from")
                            var quantityTo = arPrises[i].getAttribute("quantity-to")

                            if (quantityTo > 0) {
                                if ((quantityFrom <= arItem.QUANTITY) && (arItem.QUANTITY <= quantityTo))
                                    arPrises[i].style = "font-weight: bold"
                                else
                                    arPrises[i].style = "font-weight: normal"
                            } else {
                                if (quantityFrom <= arItem.QUANTITY)
                                    arPrises[i].style = "font-weight: bold"
                                else
                                    arPrises[i].style = "font-weight: normal"
                            }

                        }

                    }


                    //countProd++;
                } else {
                    //countEx++;
                }
                //}
            } else {
                //document.getElementById("item-line-"+id).remove();
                document.getElementById("item-line-" + id).style.display = "none"
            }

            for (var property1 in result.BASKET_DATA.BASKET_ITEM_RENDER_DATA) {
                if (!result.BASKET_DATA.BASKET_ITEM_RENDER_DATA[property1].PROPS_ALL.EXAMPLE) {
                    if (result.BASKET_DATA.BASKET_ITEM_RENDER_DATA[property1].PROPS_ALL.REQUEST) {
                        countReq++;
                    } else {
                        //countProd++;
                        if (!result.BASKET_DATA.BASKET_ITEM_RENDER_DATA[property1].PROPS_ALL.HIDDEN)
                            countProd++;
                        else if (result.BASKET_DATA.BASKET_ITEM_RENDER_DATA[property1].PROPS_ALL.HIDDEN.VALUE == "N")
                            countProd++;
                    }
                } else {
                    countEx++;
                }
            }

            quantityBuy = countProd
            quantityExample = countEx
            quantityRequest = countReq


            if (countEx === 0 && countProd === 0 && countReq === 0)
                location.reload();


            var arFullSumm = priceDistr(result.BASKET_DATA.allSum_FORMATED);

            var fullSumm = countProd + " " + objDel.declinationNum(BX.message("SBB_POSITION_1"), parseInt(countProd)) + " на сумму <span id='full-sum'>" + arFullSumm[0] + "</span> " + arFullSumm[1];
            document.getElementById("full-sum-str").innerHTML = fullSumm;

            var fullSummReq = countReq + " " + objDel.declinationNum(BX.message("SBB_POSITION_1"), parseInt(countReq))
            document.getElementById("full-sum-str-req").innerHTML = fullSummReq;

            var purchase = "<span>" + countProd + " " + objDel.declinationNum(BX.message("SBB_POSITION_1"), parseInt(countProd)) + "</span><span>" + " " + result.BASKET_DATA.allSum_FORMATED + "</span>";
            document.getElementById("purchase").innerHTML = purchase;
            var example = "<span>" + countEx + " " + objDel.declinationNum(BX.message("SBB_POSITION_1"), parseInt(countEx)) + "</span>";
            document.getElementById("example").innerHTML = example;
            var reqprice = "<span>" + countReq + " " + objDel.declinationNum(BX.message("SBB_POSITION_1"), parseInt(countReq)) + "</span>";
            document.getElementById("reqprice").innerHTML = reqprice;
        },
        onfailure: BX.delegate(function () {

        }, this)
    });
}

$("#cart-id").on("click", ".quantity-button", function () {

    var $button = $(this);
    var id = $button.attr("data-id");
    var oldValue = parseFloat($button.siblings("input#basket-item-quantity-d-" + id).val());
    var ratio = parseFloat($button.siblings("input#basket-item-quantity-ratio-" + id).val());

    if (ratio <= 0) ratio = 1;

    if ($button.text() == "+") {
        var newVal = oldValue + ratio;
    } else {
        if (oldValue > 0) {
            var newVal = oldValue - ratio;
        } else {
            newVal = 0;
        }
    }
    $button.parent().children("input#basket-item-quantity-d-" + id).val(newVal);
    req($button.attr("data-id"));
});

$("#cart-id").on('click', '.purchase-head__additional', function () {
    if ($(this).hasClass('open') && $(this).hasClass('open')) {
        $(this).parent().siblings('.purchase-content').removeClass('open');
        $(this).removeClass('open');
        $(this).parent().siblings('.purchase-content').slideUp(300)
    } else {
        $(this).parent().siblings('.purchase-content').addClass('open');
        $(this).addClass('open');
        $(this).parent().siblings('.purchase-content').slideDown(300)
    }
})

$("#cart-id").on('click', '.samples-head__additional ', function () {
    if ($('.samples-content').hasClass('open') && $(this).hasClass('open')) {
        $('.samples-content').removeClass('open');
        $(this).removeClass('open');
        $('.samples-content').slideUp(300)
    } else {
        $('.samples-content').addClass('open');
        $(this).addClass('open');
        $('.samples-content').slideDown(300)
    }
})


$("#cart-id").on('click', '.project-select__btn', function () {
    if ($(this).parent().hasClass('open')) {
        $(this).parent().removeClass('open')
    } else {
        $(this).parent().addClass('open')
    }
})

$("#cart-id").on('click', '.option', function () {
    var selectedText = $(this).children('label').text()
    $(this).parent().parent().siblings('.project-select__btn').text(selectedText)
    if ($(this).parent().parent().parent().hasClass('open')) {
        $(this).parent().parent().parent().removeClass('open')
    }
})

$("#cart-id").on('click', '.favorite', function () {
    let el = $(this), id = parseInt(el.data('id'));
    if (id)
        $.ajax({
            url: '/ajax.php',
            type: 'POST',
            data: {action: 'fav', id: id},
        }).done(function (data) {
            data = JSON.parse(data);
            if (data.noAuth) {
                BX.Mcart.modals.result_off.open(["Ошибка!", "Вам нужно авторизоваться"]);
                //alert("Ошибка, Вам нужно авторизоваться!");
                return;
            }
            if (data.active) {
                //BX.Mcart.modals.result_off.open(["Уведомление", "Добавлено в избранное", {url:"/personal/favorites/", text:"Перейти в избранное"}]);
                //alert("Добавлено в избранное");
            } else {
                //BX.Mcart.modals.result_off.open(["Уведомление", "Удалено из избранного", {url:"/personal/favorites/", text:"Перейти в избранное"}]);
                //alert("Удалено из избранное");
            }
            el.toggleClass('act');
        });
});

$(document).on('click touch', function (e) {
    var dropdowns = $('.search-select, .project-select, .subscribe-wrapper, .add-filter, .add-filter__item')

    if (!dropdowns.is(e.target) && dropdowns.has(e.target).length === 0 && dropdowns.hasClass('open')) {
        dropdowns.removeClass('open')
    }

})

$(document).on('click touch', function (e) {
    var dropdowns = $('.search-select, .project-select, .subscribe-wrapper, .add-filter, .add-filter__item')

    if (!dropdowns.is(e.target) && dropdowns.has(e.target).length === 0 && dropdowns.hasClass('open')) {
        dropdowns.removeClass('open')
    }

})

//buttons for open and close blocks
$("#cart-id").on("click", "#products_block", function (e) {
    BX.ajax.post(
        templateFolder + "/ajax_open_blocks.php",
        {"BLOCK": "PRODUCTS_BLOCK", "fixed_session_id": sesId}
    )
})

$("#cart-id").on("click", "#samples_block", function (e) {
    BX.ajax.post(
        templateFolder + "/ajax_open_blocks.php",
        {"BLOCK": "SAMPLES_BLOCK", "fixed_session_id": sesId}
    )
})

$("#cart-id").on("click", "#requests_block", function (e) {
    BX.ajax.post(
        templateFolder + "/ajax_open_blocks.php",
        {"BLOCK": "REQUESTS_BLOCK", "fixed_session_id": sesId}
    )
})


function insertDataProjInForm(id) {

    var id = document.getElementById("select-proj-value-naw-" + id).getAttribute("id-val");
    if (id != "") {
        if (!arProj[id].NAME)
            arProj[id].NAME = "";
        document.getElementById("project-name-red").value = arProj[id].NAME;
        if (!arProj[id].PROPERTY_PRODUCT_VALUE)
            arProj[id].PROPERTY_PRODUCT_VALUE = "";
        document.getElementById("project-product-red").value = arProj[id].PROPERTY_PRODUCT_VALUE;
        if (!arProj[id].PROPERTY_START_VALUE)
            arProj[id].PROPERTY_START_VALUE = "";
        document.getElementById("project-start-red").value = arProj[id].PROPERTY_START_VALUE;
        if (!arProj[id].PROPERTY_QUANT_EX_VALUE)
            arProj[id].PROPERTY_QUANT_EX_VALUE = "";
        document.getElementById("project-quant-red").value = arProj[id].PROPERTY_QUANT_EX_VALUE;
        if (!arProj[id].PREVIEW_TEXT)
            arProj[id].PREVIEW_TEXT = "";
        document.getElementById("project-comment-red").value = arProj[id].PREVIEW_TEXT;
        document.getElementById("id-proj-for-update").value = arProj[id].ID;
    }
}


/*
*
* check order and send
*
* */

function checkOrderGo(part) {

    if (part == "") {
        if (countMaxQuantity == 0 && countMinQuantity == 0) {
            var flagNoOrd = false;

            if ($('.project-select__btn').length > 0) {
                $('.project-select__btn').each(function () {
                    if ($(this).attr('id-val') == 0) {
                        flagNoOrd = true;
                    }

                })
            } else {
                if (parseInt(quantityExample) > 0)
                    flagNoOrd = true;
                else
                    flagNoOrd = false;
            }

            if (flagNoOrd === false) {
                document.location.href = arParams["PATH_TO_ORDER"];
                console.log("22222222")
            } else {
                console.log("sdfsdfsdfsdfsdfsdf")
                BX.Mcart.modals.result_off.open([BX.message("SBB_BASKET_ORDER_ERROR")
                    , BX.message("SBB_BASKET_ORDER_ERROR_EXAMPLE")]);
                //alert("Не для всех образцов выбраны проекты");
            }
        } else {
            BX.Mcart.modals.result_off.open([BX.message("SBB_BASKET_ORDER_ERROR"), BX.message("SBB_BASKET_ORDER_ERROR_ALL")]);
        }
    } else if (part == "BUY") {

        if (quantityBuy > 0) {
            if (countMaxQuantity == 0) {
                document.location.href = arParams["PATH_TO_ORDER"] + "?part=" + part;
            } else {
                BX.Mcart.modals.result_off.open([BX.message("SBB_BASKET_ORDER_ERROR"), BX.message("SBB_BASKET_ORDER_ERROR_ALL")]);
            }
        } else {
            BX.Mcart.modals.result_off.open([BX.message("SBB_BASKET_ORDER_ERROR"), BX.message("SBB_BASKET_ORDER_ERROR_BLOCK_EMPTY")]);
        }
    } else if (part == "REQUEST") {
        if (quantityRequest > 0) {
            if (countMinQuantity == 0) {
                document.location.href = arParams["PATH_TO_ORDER"] + "?part=" + part;
            } else {
                BX.Mcart.modals.result_off.open([BX.message("SBB_BASKET_ORDER_ERROR"), BX.message("SBB_BASKET_ORDER_ERROR_ALL")]);
            }
        } else {
            BX.Mcart.modals.result_off.open([BX.message("SBB_BASKET_ORDER_ERROR"), BX.message("SBB_BASKET_ORDER_ERROR_BLOCK_EMPTY")]);
        }
    } else if (part == "EXAMPLE") {
        if (quantityExample > 0) {
            var flagNoOrd = false;

            if ($('.project-select__btn').length > 0) {
                $('.project-select__btn').each(function () {
                    if ($(this).attr('id-val') == 0) {
                        flagNoOrd = true;
                    }

                })
            } else {
                if (parseInt(quantityExample) > 0)
                    flagNoOrd = true;
                else
                    flagNoOrd = false;
            }

            if (flagNoOrd === false)
                document.location.href = arParams["PATH_TO_ORDER"] + "?part=" + part;
            else {
                BX.Mcart.modals.result_off.open(["Ошибка!", "Не для всех образцов выбраны проекты"]);
                //alert("Не для всех образцов выбраны проекты");
            }
        } else {
            BX.Mcart.modals.result_off.open(["Ошибка!", "Этот блок пуст"]);
        }
    }
}

/*
*
*  request prices and quantity for product
*
* */

function requestProduct(id, value, valueparent) {

    BX.ajax.get(
        templateFolder + "/ajax_request.php",
        {"id": id, "value": value, "valueparent": valueparent},
        function (data) {
            if (data === "1") {
                BX.ajax.post(
                    templateFolder + "/ajax_all.php",
                    {"PARAMS": arParams, "fixed_session_id": sesId},
                    function (html) {
                        if (html) {
                            document.getElementById("cart-id").innerHTML = html
                        }
                    }
                )
            }
            //document.getElementById("select-proj-value-naw-" + id).setAttribute("id-val", value);
            //document.getElementById("redact-link-" + id).style.display = "";
            //document.getElementById("project-select-error-" + id).style.display = "none";
        }
    );
}