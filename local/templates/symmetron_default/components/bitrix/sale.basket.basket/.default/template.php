<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

\Bitrix\Main\UI\Extension::load("mcart.symmetrondeclination");

function priceDistr($str){
    $arPrice = explode(" ", $str);
    $cArP = count($arPrice) - 1;
    $arVal = $arPrice[$cArP];
    unset($arPrice[$cArP]);
    $price = implode(" ", $arPrice);
    unset($arPrice);
    return array($price, $arVal);
}


$arPsositionVariants = array(
    "1" => GetMessage("SBB_POSITION_1"),
    "2" => GetMessage("SBB_POSITION_2"),
    "3" => GetMessage("SBB_POSITION_2"),
    "4" => GetMessage("SBB_POSITION_2"),
    "5" => GetMessage("SBB_POSITION_5"),
    "6" => GetMessage("SBB_POSITION_5"),
    "7" => GetMessage("SBB_POSITION_5"),
    "8" => GetMessage("SBB_POSITION_5"),
    "9" => GetMessage("SBB_POSITION_5"),
    "0" => GetMessage("SBB_POSITION_5"),
);

$arPsositionVariantsExaption = array(11, 12, 13, 14);


function declineWordPosition($quantVal, $arPsositionVariants, $arPsositionVariantsExaption){
    //склоняем слово позиция
    $mod = $quantVal % 10;
    if(in_array($quantVal, $arPsositionVariantsExaption))
        $posWord = $arPsositionVariants[5];
    else
        $posWord = $arPsositionVariants[$mod];

    return $posWord;
}



global $USER;
if ( USER_ID ){
    $res = CUser::GetList($by = "id", $order = "asc", ['ID'=>USER_ID],['SELECT'=>["UF_FAVS"]])->Fetch();
    $favs = $res["UF_FAVS"];
}

/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

$documentRoot = Main\Application::getDocumentRoot();

if (empty($arParams['TEMPLATE_THEME']))
{
    $arParams['TEMPLATE_THEME'] = Main\ModuleManager::isModuleInstalled('bitrix.eshop') ? 'site' : 'blue';
}

if ($arParams['TEMPLATE_THEME'] === 'site')
{
    $templateId = Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', $component->getSiteId());
    $templateId = preg_match('/^eshop_adapt/', $templateId) ? 'eshop_adapt' : $templateId;
    $arParams['TEMPLATE_THEME'] = Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', $component->getSiteId());
}

if (!empty($arParams['TEMPLATE_THEME']))
{
    if (!is_file($documentRoot.'/bitrix/css/main/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
    {
        $arParams['TEMPLATE_THEME'] = 'blue';
    }
}

if (!isset($arParams['DISPLAY_MODE']) || !in_array($arParams['DISPLAY_MODE'], array('extended', 'compact')))
{
    $arParams['DISPLAY_MODE'] = 'extended';
}

$arParams['USE_DYNAMIC_SCROLL'] = isset($arParams['USE_DYNAMIC_SCROLL']) && $arParams['USE_DYNAMIC_SCROLL'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_FILTER'] = isset($arParams['SHOW_FILTER']) && $arParams['SHOW_FILTER'] === 'N' ? 'N' : 'Y';

$arParams['PRICE_DISPLAY_MODE'] = isset($arParams['PRICE_DISPLAY_MODE']) && $arParams['PRICE_DISPLAY_MODE'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['TOTAL_BLOCK_DISPLAY']) || !is_array($arParams['TOTAL_BLOCK_DISPLAY']))
{
    $arParams['TOTAL_BLOCK_DISPLAY'] = array('top');
}

if (empty($arParams['PRODUCT_BLOCKS_ORDER']))
{
    $arParams['PRODUCT_BLOCKS_ORDER'] = 'props,sku,columns';
}

if (is_string($arParams['PRODUCT_BLOCKS_ORDER']))
{
    $arParams['PRODUCT_BLOCKS_ORDER'] = explode(',', $arParams['PRODUCT_BLOCKS_ORDER']);
}

$arParams['USE_PRICE_ANIMATION'] = isset($arParams['USE_PRICE_ANIMATION']) && $arParams['USE_PRICE_ANIMATION'] === 'N' ? 'N' : 'Y';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

if ($arParams['USE_GIFTS'] === 'Y')
{
    $giftParameters = array(
        'SHOW_PRICE_COUNT' => 1,
        'PRODUCT_SUBSCRIPTION' => 'N',
        'PRODUCT_ID_VARIABLE' => 'id',
        'PARTIAL_PRODUCT_PROPERTIES' => 'N',
        'USE_PRODUCT_QUANTITY' => 'N',
        'ACTION_VARIABLE' => 'actionGift',
        'ADD_PROPERTIES_TO_BASKET' => 'Y',

        'BASKET_URL' => $APPLICATION->GetCurPage(),
        'APPLIED_DISCOUNT_LIST' => $arResult['APPLIED_DISCOUNT_LIST'],
        'FULL_DISCOUNT_LIST' => $arResult['FULL_DISCOUNT_LIST'],

        'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
        'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_SHOW_VALUE'],
        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],

        'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
        'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
        'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],
        'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
        'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
        'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
        'SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
        'SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
        'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
        'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
        'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
        'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
        'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

        'LINE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],

        'DETAIL_URL' => isset($arParams['GIFTS_DETAIL_URL']) ? $arParams['GIFTS_DETAIL_URL'] : null
    );
}

\CJSCore::Init(array('fx', 'popup', 'ajax'));

//$this->addExternalCss('/bitrix/css/main/bootstrap.css');
//$this->addExternalCss($templateFolder.'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');

//$this->addExternalJs($templateFolder.'/js/mustache.js');
//$this->addExternalJs($templateFolder.'/js/action-pool.js');
//$this->addExternalJs($templateFolder.'/js/filter.js');
//$this->addExternalJs($templateFolder.'/js/component.js');

$mobileColumns = isset($arParams['COLUMNS_LIST_MOBILE'])
    ? $arParams['COLUMNS_LIST_MOBILE']
    : $arParams['COLUMNS_LIST'];
$mobileColumns = array_fill_keys($mobileColumns, true);

$displayModeClass = $arParams['DISPLAY_MODE'] === 'compact' ? ' basket-items-list-wrapper-compact' : '';

//counters for got to order

$countMaxQuantity = 0;
$countMinQuantity = 0;

if (empty($arResult['ERROR_MESSAGE']))
{

    if (!empty($arResult['CURRENCIES']) && Main\Loader::includeModule('currency'))
    {
        CJSCore::Init('currency');

        ?>
        <script>

            BX.message({

                SBB_BASKET_ORDER_ERROR: "<?=Loc::getMessage("SBB_BASKET_ORDER_ERROR")?>",
                SBB_BASKET_ORDER_ERROR_EXAMPLE: "<?=Loc::getMessage("SBB_BASKET_ORDER_ERROR_EXAMPLE")?>",
                SBB_BASKET_ORDER_ERROR_BLOCK_EMPTY: "<?=Loc::getMessage("SBB_BASKET_ORDER_ERROR_BLOCK_EMPTY")?>",
                SBB_BASKET_ORDER_ERROR_ALL: "<?=Loc::getMessage("SBB_BASKET_ORDER_ERROR_ALL")?>",
                SBB_POSITION_1: "<?=Loc::getMessage("SBB_POSITION_1")?>",
            });
            
            BX.Currency.setCurrencies(<?=CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)?>);
        </script>
        <?
    }

    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedTemplate = $signer->sign($templateName, 'sale.basket.basket');
    $signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.basket.basket');
    $messages = Loc::loadLanguageFile(__FILE__);
    ?>

    <script>
        BX.message(<?=CUtil::PhpToJSObject($messages)?>);
        var arPsositionVariants = <?=CUtil::PhpToJSObject($arPsositionVariants)?>;
        var arPsositionVariantsExaption = [11,12,13,14];
        var arCheckPrice = <?=CUtil::PhpToJSObject($arResult["CHECK_PRICE_ARRAY"]);?>;
        var arProj = <?=CUtil::PhpToJSObject($arResult["PROJ_VAL_FOR_SELECT_FULL"]);?>;
        var arParams = <?=CUtil::PhpToJSObject($arParams)?>;
        var sessID = "<?=$_SESSION["fixed_session_id"];?>";
        var signedParams = '<?=CUtil::JSEscape($signedParams)?>';
        var signedTemplate = '<?=CUtil::JSEscape($signedTemplate)?>';
        var componentPath = '<?=$component->getPath()?>';
        var templateFolder = '<?=$templateFolder?>';
        var sesId = "<?=$_SESSION["fixed_session_id"]?>";
    </script>

    <?if($arParams["AJAX_NOW"] !== true){

        Bitrix\Main\Page\Asset::getInstance()->addJs($templateFolder . "/script_basket.js");

    }?>

    <?
    if ($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'BOTTOM')
    {
        $APPLICATION->IncludeComponent(
            'bitrix:sale.gift.basket',
            '.default',
            $giftParameters,
            $component
        );
    }

    ?>

    <?if($arParams["AJAX_NOW"] !== true):?>
    <section class="cart" id="cart-id">
    <?endif;?>
        <div class="wrapper">
            <div class="cart-header__wrap">
                <h1>Корзина</h1>
                <span><svg   xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400.67 354.5"><path class="cart-svg" d="M428.4,310.9H198.8l-5-19.8,237.3-33.2a17,17,0,0,0,13.2-13.7l19.4-106.9a17.36,17.36,0,0,0-3.7-14,16.86,16.86,0,0,0-13.1-6.1H153.6L144.3,77a17.1,17.1,0,0,0-16.6-13.2H80.3a17,17,0,0,0,0,34h33.9l54.4,233.8a17.1,17.1,0,0,0,16.6,13.2H428.3a17,17,0,1,0,.1-33.9ZM160.8,147H417.6a8.11,8.11,0,0,1,8,9.2l-9.7,68.3a9.36,9.36,0,0,1-7.4,7.8L187.3,261.1Z" transform="translate(-63.3 -63.8)"/><path class="cart-svg" d="M187,365.9a26.2,26.2,0,1,0,26.2,26.2A26.23,26.23,0,0,0,187,365.9Z" transform="translate(-63.3 -63.8)"/><path class="cart-svg" d="M386,365.9a26.2,26.2,0,1,0,26.2,26.2A26.16,26.16,0,0,0,386,365.9Z" transform="translate(-63.3 -63.8)"/></svg></span>
            </div>
            <div class="purchase">
                <div class="purchase-head">
                    <div class="purchase-head__additional <?=$arResult["PRODUCTS_BLOCK"]?"open":""?>" id="products_block">
                        <button class="show-btn purchase-block ">
                            <span></span>
                        </button>
                        <h2>Покупка товаров</h2>
						<div class="total-cost">
                            <?$arPriceD = priceDistr($arResult["allSum_FORMATED"]);?>
							<span id="full-sum-str">
                                <?=$arResult["BASKET_ITEM_RENDER_DATA_COUNT"];?> <?=$arResult["DECLINATION_OBJ"]->declinationNum(GetMessage("SBB_POSITION_1"), (int)$arResult["BASKET_ITEM_RENDER_DATA_COUNT"])?> на сумму <span id="full-sum"><?=$arPriceD[0];?></span> <?=$arPriceD[1];?></span>
						</div>
                    </div>
                    
                    <?$arPriceD = priceDistr($arResult["allSum_FORMATED"]);?>

                    <div class="card-checkout">
                        <a <?if($USER->IsAuthorized()):?>onclick="checkOrderGo('BUY');" <?else:?>href="#modal-login" rel="modal:open"<?endif;?> class="card-checkout__button">Оформить покупку</a>
                    </div>
                </div>
                <div class="purchase-content <?=$arResult["PRODUCTS_BLOCK"]?"open":""?>">

                    <div class="tableItem-naming">
                        <div class="item-naming">Наименование</div>
                        <div class="item-avaliability">Наличие, штук</div>
                        <div class="item-boxes">Количество штук</div>
                        <div class="item-boxCost">Цена за штуку</div>
                        <div class="item-quantityNow">Заказ</div>
                        <div class="item-totalPrice">Итого, ₽</div>
                    </div>
                    <?$iterationData = 0;?>
                    <?foreach ($arResult["BASKET_ITEM_RENDER_DATA"] as $arItem):?>
                        <?//$iterationData++;?>
						<!-- к элементу ниже добавить класс moved что бы перекрасить элементы в серый -->
                        <?
                        $moved = "";
                        if($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] == "Y")
                            $moved = "moved";
                        else
                            $iterationData++
                        ?>
                        <div class="purchase-content__item <?=$moved?>"
                             id="item-line-<?=$arItem["ID"];?>"
                        >
                            <div class="item-delete" >
                                <button <?if($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] == "Y"):?>style="display: none" <?endif;?> class="close-btn" onclick="reqDel('<?=$arItem["ID"];?>', '')">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 203.9 250" style="enable-background:new 0 0 203.9 250;" xml:space="preserve">

                                    <g id="trash">
                                        <path class="trash" d="M193.01,36.36h-43.2l-6.7-28c-1.22-4.9-5.61-8.34-10.65-8.35h-61C66.42,0,62.03,3.46,60.86,8.36l-6.7,28H10.9
                                            C4.88,36.36,0,41.24,0,47.26c0,6.02,4.88,10.9,10.9,10.9h182.11c6.02,0,10.9-4.88,10.9-10.9
                                            C203.91,41.24,199.03,36.36,193.01,36.36z M76.56,36.36l3.5-14.55h43.85l3.5,14.55H76.56z"/>
                                        <path class="trash" d="M180.01,76c-6.01,0.02-10.88,4.89-10.9,10.9v141.3h-32.1V86.9C137,80.88,132.12,76,126.1,76.01
                                            c-6.01,0.01-10.88,4.88-10.89,10.89v141.3h-32.9V86.9C82.3,80.88,77.42,76,71.4,76.01c-6.01,0.01-10.88,4.88-10.89,10.89v141.3
                                            h-25.8V86.9c0.18-6.01-4.54-11.02-10.55-11.2c-6.01-0.18-11.02,4.54-11.2,10.55c-0.01,0.22-0.01,0.44,0,0.66v152.2
                                            c0.02,6.01,4.89,10.88,10.9,10.9h156.15c6.01-0.02,10.88-4.89,10.9-10.9V86.9c0.01-6.01-4.87-10.89-10.88-10.9
                                            C180.02,76,180.02,76,180.01,76z"/>
                                    </g>
                                    </svg>
                                </button>
                            </div>
                            <div class="item-numPosition" ><span <?if($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] == "Y"):?>style="display: none" <?endif;?>><?=$iterationData?>.</span></div>
                                <!--<span class="favorite <?//=$is_favs!==FALSE ? 'act':''?>" data-id="<?//=$arItem['PRODUCT_ID']?>">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="5 1 28 35" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"/>
										<style>
											#fill-heart {
												fill: transparent;
											}
										</style>
															
										<g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""/>
										<path  id="svg_22" d="M-29.97297297297297,2.4594594594594668 " /><path  id="svg_2" d="M19.324324324324337,8.837837837837846 " />
										<path  id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"  /></g>
										
									</svg>
                                  </span>-->
                            <div class="item-info">
                                <!--show category name-->
                                <div class="item-info__name"><?=$arResult["SECTIONS"]["NAMES"][ $arResult["SECTIONS"]["IDS"][ $arItem["PRODUCT_ID"] ] ];?></div>

                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-info__head">
                                    <?//$descr = "";?>
                                    <?/*foreach ($arItem["COLUMN_LIST"] as $prop) {
                                        if($prop["CODE"] == "PREVIEW_TEXT"){
                                            $descr = ", ".$prop["VALUE"];
                                        }
                                    }*/?>
                                    <?=$arItem["NAME"];?>
                                </a>
                                <?if(!empty($arItem["PROPS_ITEM"])):?>
                                    <div class="item-info__content">
                                        <ul>
                                            <?foreach ($arItem["PROPS_ITEM"] as $code => $arProp):?>
                                                <?if($code == $arParams["CODE_PRODUC"]):?>
                                                    <li>
                                                        <?=$arProp["NAME"]?>:
                                                        <?
                                                            $tmpProp = $arResult["BRANDS_DATA"][$arProp["VALUE"]];
                                                            if(!empty($tmpProp["LINK"])){
                                                                echo "<a href='" . $tmpProp["LINK"] . "'>" . $tmpProp["VALUE"] . "</a>";
                                                            } else {
                                                                echo $tmpProp["VALUE"];
                                                            }
                                                        ?>
                                                    </li>
                                                <?else:?>
                                                    <li><?=$arProp["NAME"]?>: <?=$arProp["VALUE"]?></li>
                                                <?endif;?>
                                            <?endforeach;?>
                                        </ul>
                                    </div>
                                <?endif;?>
                            </div>

                            <?$multiplicity = true;
                                if($arItem["AVAILABLE_QUANTITY"] > 0 && $arItem["MEASURE_RATIO"] > 0){
                                    if($arItem["AVAILABLE_QUANTITY"] % $arItem["MEASURE_RATIO"] > 0 && $arItem["AVAILABLE_QUANTITY"] < $arItem["MEASURE_RATIO"]) {
                                        $multiplicity = false;
                                    }
                                }
                            ?>
                            <div class="item-available">
                                <div class="avaliable"><span><?=$arItem["AVAILABLE_QUANTITY"];?></span> <?=$arResult["DECLINATION_OBJ"]->declinationNum(mb_strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]), (int)$arItem["AVAILABLE_QUANTITY"]);?></div>
                                <?if(empty($arItem["PACKING"]["UPAK"]) || empty($arItem["PACKING"]["SYMPACKAGE"])):?>
								    <div class="in-box">Поштучная продажа</div>
                                <?else:?>
                                    <div class="in-box">
                                        <?$upac = trim(strtolower($arItem["PACKING"]["UPAK"]["VALUE"]));?>
                                        <?$sumPac = (int)$arItem["PACKING"]["SYMPACKAGE"];?>
                                        <?=$sumPac . " " . $arResult["DECLINATION_OBJ"]->declinationNum(mb_strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]), $sumPac) . " в " . $arResult["DECLINATION_OBJ"]->declinationIn($upac)?>
                                    </div>
                                <?endif;?>
								<div class="in-box not-multiples" <?if($multiplicity):?>style="display: none;"<?endif;?>>
                                    <span><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.74 16.36"><circle class="not-equal--svg" cx="9.37" cy="3.6" r="3.6"/>
                                            <circle class="not-equal--svg" cx="15.14" cy="12.77" r="3.6"/><circle class="not-equal--svg" cx="3.6" cy="12.77" r="3.6"/>
                                        </svg>Имеются некратные остатки.</span>
                                    <a href="javascript:void(0)" id="<?=$arItem["ID"];?>_request_button_mult" onclick="requestProduct('<?=$arItem["ID"];?>', 'MULTIPLICITY')">Запросить</a>
                                </div>
                            </div>

                            <!--show prices for product-->


                            <?if (count($arItem["PRICES_PROD"])):?>
                            <div class="item-pricends">
                                <div class="prices">
                                    <div class="item-price__quantity">
                                        <ul id="<?=$arItem["ID"]?>_all_quantities">
                                            <?foreach ($arItem["PRICES_PROD"] as $arPrice):?>
                                                <?
                                                    if(!empty($arPrice["QUANTITY_FROM"])) {
                                                        $priceQuant = $arPrice["QUANTITY_FROM"];
                                                    }else{
                                                        $priceQuant = "1";
                                                    }
                                                ?>
                                                <li quantity-from="<?=$priceQuant?>" quantity-to="<?=$arPrice["QUANTITY_TO"]?>"
                                                    <?if($arPrice["QUANTITY_TO"] > 0):?>
                                                        <?if(($priceQuant <= $arItem["QUANTITY"]) && ($arItem["QUANTITY"] <= $arPrice["QUANTITY_TO"])):?>
                                                            style="font-weight: bold"
                                                        <?endif;?>
                                                        >
                                                        <?=$priceQuant . " - " . $arPrice["QUANTITY_TO"];?>

                                                    <?else:?>
                                                        <?if(($priceQuant <= $arItem["QUANTITY"])):?>
                                                            style="font-weight: bold"
                                                        <?endif;?>
                                                        >
                                                        <?="от " . $priceQuant;?>

                                                    <?endif;?>
                                                </li>
                                            <?endforeach;?>
                                        </ul>
                                    </div>
                                    <div class="item-price__cost">
                                        <ul id="<?=$arItem["ID"]?>_all_prices">
                                            <?foreach ($arItem["PRICES_PROD"] as $arPrice):?>
                                                <?
                                                    if(!empty($arPrice["QUANTITY_FROM"])) {
                                                        $priceQuant = $arPrice["QUANTITY_FROM"];
                                                    }else{
                                                        $priceQuant = "1";
                                                    }
                                                ?>
                                                <li quantity-from="<?=$priceQuant?>" quantity-to="<?=$arPrice["QUANTITY_TO"]?>"
                                                    <?if($arPrice["QUANTITY_TO"] > 0):?>
                                                        <?if(($priceQuant <= $arItem["QUANTITY"]) && ($arItem["QUANTITY"] <= $arPrice["QUANTITY_TO"])):?>
                                                            style="font-weight: bold"
                                                        <?endif;?>
                                                    <?else:?>
                                                        <?if(($priceQuant <= $arItem["QUANTITY"])):?>
                                                            style="font-weight: bold"
                                                        <?endif;?>
                                                    <?endif;?>
                                                >


                                                    <?=$arPrice["PRICE"];?>&nbsp;руб.

                                                </li>
                                            <?endforeach;?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-quantity" <?if($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] == "Y"):?>style="display: none" <?endif;?>>
                                <div class="quantity-num" data-entity="basket-item-quantity-block">
                                    <span class="decr quantity-button" data-id="<?=$arItem["ID"];?>">&ndash;</span>
                                    <input type="hidden" disabled="disabled" id="basket-item-quantity-min-<?=$arItem["ID"];?>" value="<?=$arItem["MIN_QUANTITY"]?>">
                                    <input type="hidden" disabled="disabled" id="basket-item-quantity-ratio-<?=$arItem["ID"];?>" value="<?=$arItem["MEASURE_RATIO"]?>">
                                    <input value="<?=$arItem["QUANTITY"];?>" type="number" min="1" max="<?=$arItem["AVAILABLE_QUANTITY"];?>" id="basket-item-quantity-d-<?=$arItem["ID"];?>" data-entity="basket-item-quantity-field" data-value="<?=$arItem["QUANTITY"];?>" onchange="req('<?=$arItem["ID"];?>')">
                                    <span class="inc quantity-button" data-id="<?=$arItem["ID"];?>">+</span>
                                </div>

                                    <!-- calculator -->

                                    <?$calculatorShow = false;
                                    $quantityNow = $arItem["QUANTITY"];
                                    $quantityInPac = intval($arItem["PACKING"]["SYMPACKAGE"]);
                                    if($arItem["QUANTITY"] >= $arItem["PACKING"]["SYMPACKAGE"] && $arItem["AVAILABLE_QUANTITY"] >= $arItem["QUANTITY"]){
                                        $calculatorShow = $quantityInPac > 0 ? true : false;
                                    }?>
                                        <div id="<?=$arItem["ID"];?>_calculator"
                                             class="quantity-num__info quantity-num__instock"
                                             data-pac-quant="<?=$quantityInPac?>"
                                             data-pac-type="<?=$arItem["PACKING"]["UPAK"]["VALUE"]?>"
                                             data-measure="<?=$arItem["MEASURE_INFO"]["MEASURE_TITLE"]?>"
                                            <?if(!$calculatorShow):?>style="display: none" <?endif;?>
                                        >

                                            <?if($calculatorShow):?>
                                                <?$upac = trim(strtolower($arItem["PACKING"]["UPAK"]["VALUE"]));?>
                                                <?$quantityCal = (int)($quantityNow / $quantityInPac);?>
                                                <?=$quantityCal . " " . $arResult["DECLINATION_OBJ"]->declinationNum($upac, $quantityCal);?>
                                                <?if($mod = $quantityNow % $quantityInPac):
                                                    echo "<br>";
                                                    echo $mod . " " . $arResult["DECLINATION_OBJ"]->declinationNum(mb_strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]), (int)$mod);
                                                endif;?>
                                            <?endif;?>
                                        </div>

                                <!-- message no quantity -->

                                <div id="<?=$arItem["ID"];?>_no_quantity" class="quantity-num__info quantity-num__error" <?if($arItem["AVAILABLE_QUANTITY"] >= $arItem["QUANTITY"]):?> style="display: none" <?elseif($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] != "Y"): $countMaxQuantity++;?><?endif;?> >Такого количества<br/>нет в наличии</div>
                            </div>
                            <div class="item-total" <?if($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] == "Y"):?>style="display: none" <?endif;?>>
                                <div id="<?=$arItem["ID"];?>_price_block" <?if($arItem["AVAILABLE_QUANTITY"] < $arItem["QUANTITY"]):?> style="display: none" <?endif;?>>

                                    <?$arPriceDistr = priceDistr($arItem["SUM_PRICE_FORMATED"]);?>

                                    <span class="item-total__price" id="basket-item-sum-price-d-<?=$arItem["ID"];?>"><?=$arPriceDistr[0];?></span>
                                    <span class="item-total__value"><?=$arPriceDistr[1];?></span>
                                </div>
                                <a id="<?=$arItem["ID"];?>_request_button" onclick="requestProduct('<?=$arItem["ID"];?>', 'QUANTITY')" href="javascript:void(0)" class="more-please btn-grey" <?if($arItem["AVAILABLE_QUANTITY"] >= $arItem["QUANTITY"]):?> style="display: none" <?endif;?> >Запросить</a>
                            </div>
                            <div <?if($arItem["PROPS_ALL"]["HIDDEN"]["VALUE"] != "Y"):?>style="display: none" <?endif;?> class="itemInRequest">Перемещено в запрос количества</div>
                            <?else:?>
                            <div class="item-check">
                            </div>
                            <?endif;?>

                        </div>

                    <?endforeach;?>

                </div>


            </div>
            <div class="samples">
                <div class="samples-head">
					<div class="samples-head__additional <?=$arResult["SAMPLES_BLOCK"]?"open":""?>" id="samples_block">
						<button class="show-btn samples-block " >
							<span></span>
						</button>
						<h2>Запросить образцы</h2>
						<div class="samples-positions">
                            <span id="full-sum-str-ex"><?=$arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"];?> <?=$arResult["DECLINATION_OBJ"]->declinationNum(GetMessage("SBB_POSITION_1"), (int)$arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"])?></span>
                        </div>
					</div>
                    <div class="card-checkout">
                        <a <?if($USER->IsAuthorized()):?>onclick="checkOrderGo('EXAMPLE');" <?else:?>href="#modal-login" rel="modal:open"<?endif;?> class="card-checkout__button">Запросить образцы</a>
                    </div>
                </div>
				<?$countNoAuth = 0;?>
                <?$iterationDataEx = 0;?>
				<div class="samples-content <?=$arResult["SAMPLES_BLOCK"]?"open":""?>">
                <?foreach ($arResult["BASKET_ITEM_RENDER_DATA_EX"] as $arItem):?>
                    <?$iterationDataEx++;?>

                        <div class="sample-content__item" id="item-line-<?=$arItem["ID"];?>">
							<div class="item-delete">
                                <button class="close-btn" onclick="reqDel('<?=$arItem["ID"];?>', '')">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 203.9 250" style="enable-background:new 0 0 203.9 250;" xml:space="preserve">

                                    <g id="trash">
                                        <path class="trash" d="M193.01,36.36h-43.2l-6.7-28c-1.22-4.9-5.61-8.34-10.65-8.35h-61C66.42,0,62.03,3.46,60.86,8.36l-6.7,28H10.9
                                            C4.88,36.36,0,41.24,0,47.26c0,6.02,4.88,10.9,10.9,10.9h182.11c6.02,0,10.9-4.88,10.9-10.9
                                            C203.91,41.24,199.03,36.36,193.01,36.36z M76.56,36.36l3.5-14.55h43.85l3.5,14.55H76.56z"/>
                                        <path class="trash" d="M180.01,76c-6.01,0.02-10.88,4.89-10.9,10.9v141.3h-32.1V86.9C137,80.88,132.12,76,126.1,76.01
                                            c-6.01,0.01-10.88,4.88-10.89,10.89v141.3h-32.9V86.9C82.3,80.88,77.42,76,71.4,76.01c-6.01,0.01-10.88,4.88-10.89,10.89v141.3
                                            h-25.8V86.9c0.18-6.01-4.54-11.02-10.55-11.2c-6.01-0.18-11.02,4.54-11.2,10.55c-0.01,0.22-0.01,0.44,0,0.66v152.2
                                            c0.02,6.01,4.89,10.88,10.9,10.9h156.15c6.01-0.02,10.88-4.89,10.9-10.9V86.9c0.01-6.01-4.87-10.89-10.88-10.9
                                            C180.02,76,180.02,76,180.01,76z"/>
                                    </g>
                                    </svg>
                                </button>
                            </div>
                            <div class="item-numPosition"><span><?=$iterationDataEx?>.</span></div>
                                <!--<span class="favorite <?//=$is_favs!==FALSE ? 'act':''?>" data-id="<?//=$arItem['PRODUCT_ID']?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="5 1 28 35" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"/>
                                            <style>
                                                    #fill-heart {
                                                            fill: transparent;
                                                    }
                                            </style>

                                            <g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""/>
                                            <path  id="svg_22" d="M-29.97297297297297,2.4594594594594668 " /><path  id="svg_2" d="M19.324324324324337,8.837837837837846 " />
                                            <path  id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"  /></g>

                                    </svg>
                                </span> -->                      
                            <div class="item-info">
                                <div class="item-info__name"><?=$arResult["SECTIONS"]["NAMES"][ $arResult["SECTIONS"]["IDS"][ $arItem["PRODUCT_ID"] ] ];?></div>
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-info__head">
                                    <?//$descr = "";?>
                                    <?/*foreach ($arItem["COLUMN_LIST"] as $prop) {
                                        if($prop["CODE"] == "PREVIEW_TEXT"){
                                            $descr = ", ".$prop["VALUE"];
                                        }
                                    }*/?>
                                    <?=$arItem["NAME"];?>
                                </a>
                                <?if(!empty($arItem["PROPS_ITEM"])):?>
                                    <div class="item-info__content">
                                        <?if(!empty($arItem["PROPS_ITEM"])):?>
                                            <div class="item-info__content">
                                                <ul>
                                                    <?foreach ($arItem["PROPS_ITEM"] as $code => $arProp):?>
                                                        <?if($code == $arParams["CODE_PRODUC"]):?>
                                                            <li>
                                                                <?=$arProp["NAME"]?>:
                                                                <?
                                                                $tmpProp = $arResult["BRANDS_DATA"][$arProp["VALUE"]];
                                                                if(!empty($tmpProp["LINK"])){
                                                                    echo "<a href='" . $tmpProp["LINK"] . "'>" . $tmpProp["VALUE"] . "</a>";
                                                                } else {
                                                                    echo $tmpProp["VALUE"];
                                                                }
                                                                ?>
                                                            </li>
                                                        <?else:?>
                                                            <li><?=$arProp["NAME"]?>: <?=$arProp["VALUE"]?></li>
                                                        <?endif;?>
                                                    <?endforeach;?>
                                                </ul>
                                            </div>
                                        <?endif;?>
                                    </div>
                                <?endif;?>
                            </div>
                            <div class="item-quantity">
                            </div>
                            <div class="item-project">
                                <span class="item-section__head">Проект</span>
                                <?if(empty($arResult["PROJ_VAL_FOR_SELECT"])):?>
                                    <div class="nothing">Нет ни одного проекта</div>
                                    <div class="project-footer">
                                        <?global $USER;?>

                                        <a <?if($USER->IsAuthorized()):?>href="#modal-project"<?else:?>href="#modal-login"<?endif;?> rel="modal:open" >Создать</a>

                                    </div>
                                <?else:?>
                                    <div class="project-select">
                                        <!-- Добавить класс error к элементу ниже для подсвечивания красным  -->
                                        <div class="project-select__btn" id="select-proj-value-naw-<?=$arItem["ID"];?>" <?if(!empty($arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]])):?>id-val="<?=$arItem["PROPS_ALL"]["PROJECT"]["VALUE"];?>"<?else:?>id-val="0"<?endif;?> <?if(!empty($arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]])):?>name="<?=$arItem["PROPS_ALL"]["PROJECT"]["VALUE"];?>"<?else:?>name="0"<?endif;?>>
                                            <?if(empty($arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]])):?>
                                                <?$countNoAuth++;?>
                                                Выберите проект
                                            <?else:?>
                                                <?=$arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]];?>
                                            <?endif;?>
                                        </div>

                                        <div class="project-select__options">
                                            <div class="options">

                                                <?foreach ($arResult["PROJ_VAL_FOR_SELECT"] as $key => $sel):?>
                                                    <span class="option" onclick="projUpdate('<?=$arItem["ID"];?>', '<?=$key?>')">
                                                        <label><input type="radio" name="project-option" value="<?=$key?>" ><?=$sel;?></label>
                                                    </span>
                                                <?endforeach;?>
                                            </div>
                                            <div class="new-option">
                                                <a href="#modal-project" rel="modal:open" class="new-option__btn"><span>+</span>Добавить проект</a>
                                            </div>
                                        </div>
                                        <div class="project-footer">
                                            <a href="#modal-project" rel="modal:open">Создать</a>
                                            <a href="#modal-project-red" id="redact-link-<?=$arItem["ID"];?>" rel="modal:open" <?if(empty($arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]])):?>style="display: none"<?endif;?> <?/*if(!empty($arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]])):*/?>onclick='insertDataProjInForm("<?=$arItem["ID"];?>")'<?/*endif;*/?>>Редактировать</a>
                                            <!--<span>Редактировать</span>-->
                                        </div>
                                        <div class="project-select__error" id="project-select-error-<?=$arItem["ID"];?>" <?if(!empty($arResult["PROJ_VAL_FOR_SELECT"][$arItem["PROPS_ALL"]["PROJECT"]["VALUE"]])):?>style="display: none"<?endif;?>>Для заказа образцов необходимо указать проект</div>
                                    </div>
                                <?endif;?>

                            </div>


                        </div>


                <?endforeach;?>
				  </div>
                </div>

                <div class="purchase purchase-request">
                <div class="purchase-head">
                    <div class="purchase-head__additional price-and-value <?=$arResult["REQUESTS_BLOCK"]?"open":""?>" id="requests_block">
                        <button class="show-btn purchase-block ">
                            <span></span>
                        </button>
                        <h2>Условия поставки</h2>
						<div class="total-cost">
							<span id="full-sum-str-req"><?=$arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"];?> <?=$arResult["DECLINATION_OBJ"]->declinationNum(GetMessage("SBB_POSITION_1"), (int)$arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"])?></span>
						</div>
                    </div>
                    <div class="card-checkout">
                        <a <?if($USER->IsAuthorized()):?>onclick="checkOrderGo('REQUEST');" <?else:?> href="#modal-login" rel="modal:open"<?endif;?> class="card-checkout__button">Оформить запрос</a>
                    </div>
                </div>
                <div class="purchase-content <?=$arResult["REQUESTS_BLOCK"]?"open":""?>" style="overflow: hidden;">
                    <div class="tableItem-naming">
                        <div class="item-naming">Наименование</div>
                        <div class="item-avaliability">Наличие, упаковок</div>
                        <div class="item-boxes">Количество упаковок</div>
                        <div class="item-boxCost">Цена за упаковку</div>
                        <div class="item-quantityNow">Запрос</div>
                    </div>


                    <?$iterationData = 0;?>
                    <?foreach ($arResult["BASKET_ITEM_RENDER_DATA_REQ"] as $arItem):?>
                        <?$iterationData++;?>

                        <!-- к элементу ниже добавить класс moved что бы перекрасить элементы в серый -->
                        <div class="purchase-content__item" id="item-line-<?=$arItem["ID"];?>">
                            <div class="item-delete">
                                <button class="close-btn" onclick="reqDel('<?=$arItem["ID"];?>', '<?=$arItem["PROPS_ALL"]["PARENT_PRODUCT"]["VALUE"];?>')">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 203.9 250" style="enable-background:new 0 0 203.9 250;" xml:space="preserve">
                                    
                                    <g id="trash">
                                        <path class="trash" d="M193.01,36.36h-43.2l-6.7-28c-1.22-4.9-5.61-8.34-10.65-8.35h-61C66.42,0,62.03,3.46,60.86,8.36l-6.7,28H10.9
                                            C4.88,36.36,0,41.24,0,47.26c0,6.02,4.88,10.9,10.9,10.9h182.11c6.02,0,10.9-4.88,10.9-10.9
                                            C203.91,41.24,199.03,36.36,193.01,36.36z M76.56,36.36l3.5-14.55h43.85l3.5,14.55H76.56z"/>
                                        <path class="trash" d="M180.01,76c-6.01,0.02-10.88,4.89-10.9,10.9v141.3h-32.1V86.9C137,80.88,132.12,76,126.1,76.01
                                            c-6.01,0.01-10.88,4.88-10.89,10.89v141.3h-32.9V86.9C82.3,80.88,77.42,76,71.4,76.01c-6.01,0.01-10.88,4.88-10.89,10.89v141.3
                                            h-25.8V86.9c0.18-6.01-4.54-11.02-10.55-11.2c-6.01-0.18-11.02,4.54-11.2,10.55c-0.01,0.22-0.01,0.44,0,0.66v152.2
                                            c0.02,6.01,4.89,10.88,10.9,10.9h156.15c6.01-0.02,10.88-4.89,10.9-10.9V86.9c0.01-6.01-4.87-10.89-10.88-10.9
                                            C180.02,76,180.02,76,180.01,76z"/>
                                    </g>
                                    </svg>
                                </button>
                            </div>
                            <div class="item-numPosition"><span><?=$iterationData?>.</span></div>
                            <!--<span class="favorite <?//=$is_favs!==FALSE ? 'act':''?>" data-id="<?//=$arItem['PRODUCT_ID']?>">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="5 1 28 35" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"/>
										<style>
											#fill-heart {
												fill: transparent;
											}
										</style>

										<g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""/>
										<path  id="svg_22" d="M-29.97297297297297,2.4594594594594668 " /><path  id="svg_2" d="M19.324324324324337,8.837837837837846 " />
										<path  id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"  /></g>

									</svg>
                                  </span>-->
                            <div class="item-info">
                                <!--show category name-->
                                <div class="item-info__name"><?=$arResult["SECTIONS"]["NAMES"][ $arResult["SECTIONS"]["IDS"][ $arItem["PRODUCT_ID"] ] ];?></div>

                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-info__head">
                                    <?//$descr = "";?>
                                    <?/*foreach ($arItem["COLUMN_LIST"] as $prop) {
                                        if($prop["CODE"] == "PREVIEW_TEXT"){
                                            $descr = ", ".$prop["VALUE"];
                                        }
                                    }*/?>
                                    <?=$arItem["NAME"];?>
                                </a>
                                <?if(!empty($arItem["PROPS_ITEM"])):?>
                                    <div class="item-info__content">
                                        <ul>
                                            <?if(!empty($arItem["PROPS_ITEM"])):?>
                                                <div class="item-info__content">
                                                    <ul>
                                                        <?foreach ($arItem["PROPS_ITEM"] as $code => $arProp):?>

                                                            <?if($code == "PROPERTY_" . $arParams["CODE_PRODUC"] . "_VALUE"):?>

                                                                <li>
                                                                    <?
                                                                        $tmpProp = $arResult["BRANDS_DATA"][$arProp["VALUE"]];
                                                                        if(!empty($tmpProp)) {
                                                                            if (!empty($tmpProp["LINK"])) {
                                                                                echo $arProp["NAME"] . ": <a href='" . $tmpProp["LINK"] . "'>" . $tmpProp["VALUE"] . "</a>";
                                                                            } else {
                                                                                echo $arProp["NAME"] . ": " . $tmpProp["VALUE"];
                                                                            }
                                                                        }
                                                                    ?>
                                                                </li>

                                                            <?else:?>
                                                                <li><?=$arProp["NAME"]?>: <?=$arProp["VALUE"]?></li>
                                                            <?endif;?>
                                                        <?endforeach;?>
                                                    </ul>
                                                </div>
                                            <?endif;?>
                                        </ul>
                                    </div>
                                <?endif;?>
                            </div>
                            <?if($arItem["PROPS_ALL"]["REQUEST"]["VALUE"] != "MULTIPLICITY"):?>
                                <?$multiplicity = true;
                                if($arItem["AVAILABLE_QUANTITY"] > 0 && $arItem["MEASURE_RATIO"] > 0){
                                    if($arItem["AVAILABLE_QUANTITY"] % $arItem["MEASURE_RATIO"] > 0 && $arItem["AVAILABLE_QUANTITY"] < $arItem["MEASURE_RATIO"]) {
                                        $multiplicity = false;
                                    }
                                }
                                ?>
                                <div class="item-available">
                                    <?if($arItem["AVAILABLE_QUANTITY"] > 0):?>
                                        <div class="avaliable"><span><?=$arItem["AVAILABLE_QUANTITY"];?></span> <?=$arResult["DECLINATION_OBJ"]->declinationNum(mb_strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]), (int)$arItem["AVAILABLE_QUANTITY"])?></div>
                                        <?if(empty($arItem["PACKING"]["UPAK"]) || empty($arItem["PACKING"]["SYMPACKAGE"])):?>
                                            <div class="in-box">Поштучная продажа</div>
                                        <?else:?>
                                            <div class="in-box">
                                                <?$upac = trim(strtolower($arItem["PACKING"]["UPAK"]["VALUE"]));?>
                                                <?$sumPac = (int)$arItem["PACKING"]["SYMPACKAGE"];?>
                                                <?=$sumPac . " " . $arResult["DECLINATION_OBJ"]->declinationNum(mb_strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]), $sumPac) . " в " . $arResult["DECLINATION_OBJ"]->declinationIn($upac)?>
                                            </div>
                                        <?endif;?>
                                        <div class="in-box not-multiples" <?if($multiplicity):?>style="display: none;"<?endif;?>>
                                            <span><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.74 16.36"><circle class="not-equal--svg" cx="9.37" cy="3.6" r="3.6"/>
                                                <circle class="not-equal--svg" cx="15.14" cy="12.77" r="3.6"/><circle class="not-equal--svg" cx="3.6" cy="12.77" r="3.6"/>
                                            </svg>Имеются некратные остатки.</span>
                                            <a href="javascript:void(0)" id="<?=$arItem["ID"];?>_request_button_mult" onclick="requestProduct('<?=$arItem["ID"];?>', 'MULTIPLICITY')">Запросить</a>
                                        </div>
                                    <?else:?>
                                        <div class="avaliable"><span>Нет в наличии</span></div>
                                    <?endif;?>
                                </div>
                            <?endif;?>
                            <!--show prices for product-->

                            <?if (count($arItem["PRICES_PROD"]) && $arItem["AVAILABLE_QUANTITY"] > 0 && $arItem["PROPS_ALL"]["REQUEST"]["VALUE"] != "MISSING" && $arItem["PROPS_ALL"]["REQUEST"]["VALUE"] != "MULTIPLICITY"):?>
                                <div class="item-pricends">
                                    <div class="prices">
                                        <div class="item-price__quantity">
                                            <ul id="<?=$arItem["ID"]?>_all_quantities">
                                                <?foreach ($arItem["PRICES_PROD"] as $arPrice):?>
                                                    <?
                                                    if(!empty($arPrice["QUANTITY_FROM"])) {
                                                        $priceQuant = $arPrice["QUANTITY_FROM"];
                                                    }else{
                                                        $priceQuant = "1";
                                                    }
                                                    ?>
                                                    <li quantity-from="<?=$priceQuant?>" quantity-to="<?=$arPrice["QUANTITY_TO"]?>"
                                                        <?if($arPrice["QUANTITY_TO"] > 0):?>
                                                        <?if(($priceQuant <= $arItem["QUANTITY"]) && ($arItem["QUANTITY"] <= $arPrice["QUANTITY_TO"])):?>
                                                            style="font-weight: bold"
                                                        <?endif;?>
                                                    >
                                                        <?=$priceQuant . " - " . $arPrice["QUANTITY_TO"];?>

                                                        <?else:?>
                                                            <?if(($priceQuant <= $arItem["QUANTITY"])):?>
                                                                style="font-weight: bold"
                                                            <?endif;?>
                                                            >
                                                            <?="от " . $priceQuant;?>

                                                        <?endif;?>
                                                    </li>
                                                <?endforeach;?>
                                            </ul>
                                        </div>
                                        <div class="item-price__cost">
                                            <ul id="<?=$arItem["ID"]?>_all_prices">
                                                <?foreach ($arItem["PRICES_PROD"] as $arPrice):?>
                                                    <?
                                                    if(!empty($arPrice["QUANTITY_FROM"])) {
                                                        $priceQuant = $arPrice["QUANTITY_FROM"];
                                                    }else{
                                                        $priceQuant = "1";
                                                    }
                                                    ?>
                                                    <li quantity-from="<?=$priceQuant?>" quantity-to="<?=$arPrice["QUANTITY_TO"]?>"
                                                        <?if($arPrice["QUANTITY_TO"] > 0):?>
                                                        <?if(($priceQuant <= $arItem["QUANTITY"]) && ($arItem["QUANTITY"] <= $arPrice["QUANTITY_TO"])):?>
                                                            style="font-weight: bold"
                                                        <?endif;?>
                                                    >
                                                        <?else:?>
                                                            <?if(($priceQuant <= $arItem["QUANTITY"])):?>
                                                                style="font-weight: bold"
                                                            <?endif;?>
                                                            >
                                                        <?endif;?>


                                                        <?=$arPrice["PRICE"];?>&nbsp;руб.

                                                    </li>
                                                <?endforeach;?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-quantity">
                                    <div class="quantity-num" data-entity="basket-item-quantity-block">
                                        <span class="decr quantity-button" data-id="<?=$arItem["ID"];?>">&ndash;</span>
                                        <input type="hidden" disabled="disabled" id="basket-item-quantity-min-<?=$arItem["ID"];?>" value="<?=$arItem["MIN_QUANTITY"]?>">
                                        <input type="hidden" disabled="disabled" id="basket-item-quantity-ratio-<?=$arItem["ID"];?>" value="<?=$arItem["MEASURE_RATIO"]?>">
                                        <input value="<?=$arItem["QUANTITY"];?>" type="number" min="1" max="<?=$arItem["AVAILABLE_QUANTITY"];?>" id="basket-item-quantity-d-<?=$arItem["ID"];?>" data-entity="basket-item-quantity-field" data-value="<?=$arItem["QUANTITY"];?>" onchange="req('<?=$arItem["ID"];?>')">
                                        <span class="inc quantity-button" data-id="<?=$arItem["ID"];?>">+</span>
                                    </div>

                                    <div id="<?=$arItem["ID"];?>_yes_quantity" class="quantity-num__info quantity-num__haveinstock" <?if($arItem["AVAILABLE_QUANTITY"] < $arItem["QUANTITY"]):?> style="display: none" <?else: $countMinQuantity++;?><?endif;?>>Это количество<br/>есть в наличии</div>
                                </div>
                                <div class="item-total">
                                    <!-- calculator -->

                                    <?$calculatorShow = false;
                                    $quantityNow = $arItem["QUANTITY"];
                                    $quantityInPac = intval($arItem["PACKING"]["SYMPACKAGE"]);
                                    if($arItem["QUANTITY"] >= $arItem["PACKING"]["SYMPACKAGE"] && $arItem["AVAILABLE_QUANTITY"] < $arItem["QUANTITY"]){
                                        $calculatorShow = $quantityInPac > 0 ? true : false;
                                    }?>
                                    <div id="<?=$arItem["ID"];?>_calculator_req"

                                         data-pac-quant="<?=$quantityInPac?>"
                                         data-pac-type="<?=$arItem["PACKING"]["UPAK"]["VALUE"]?>"
                                         data-measure="<?=$arItem["MEASURE_INFO"]["MEASURE_TITLE"]?>"
                                         <?if(!$calculatorShow):?>style="display: none" <?endif;?>
                                    >

                                        <?if($calculatorShow):?>
                                        <?$upac = trim(strtolower($arItem["PACKING"]["UPAK"]["VALUE"]));?>
                                        <?$quantityCal = (int)($quantityNow / $quantityInPac);?>
                                            <?="<span class='request-quantity-calculator__head'>В заказе</span><span class='request-quantity-calculator'>" . $quantityCal . " " . $arResult["DECLINATION_OBJ"]->declinationNum($upac, $quantityCal);?>
                                            <?if($mod = $quantityNow % $quantityInPac) {
                                                echo " и " . $mod . " " . $arResult["DECLINATION_OBJ"]->declinationNum(mb_strtolower($arItem["MEASURE_INFO"]["MEASURE_TITLE"]), (int)$mod);
                                            }
                                            echo "</span>";
                                            ?>
                                        <?endif;?>
                                    </div>

                                    <a id="<?=$arItem["ID"];?>_request_abort_button" href="javascript:void(0)" onclick="requestProduct('<?=$arItem["ID"];?>', 'ABORT', '<?=$arItem["PROPS_ALL"]["PARENT_PRODUCT"]["VALUE"];?>')" class="more-please btn-grey" <?if($arItem["AVAILABLE_QUANTITY"] < $arItem["QUANTITY"]):?> style="display: none" <?endif;?> >Купить</a>
                                </div>

                            <?endif;?>
                            <?if($arItem["PROPS_ALL"]["REQUEST"]["VALUE"] == "PRICE" || $arItem["PROPS_ALL"]["REQUEST"]["VALUE"] == "MISSING"):?>
                                <div class="itemInRequest-info">Условия поставки</div>
                            <?endif;?>
                            <?if($arItem["PROPS_ALL"]["REQUEST"]["VALUE"] == "MULTIPLICITY"):?>
                                <div class="itemInRequest-info">Некратные остатки</div>
                            <?endif;?>
                        </div>

                    <?endforeach;?>



                </div>
            </div>
            </div>



            <div class="cart-total">
                <div class="total-cost__head">Итого:</div>
                <div class="total-cost__wrapper">
                    <div class="total-col">
                        <div class="total-col__head">Товары из каталога:</div>
                        <div class="total-col__content" id="purchase">
                            <span><?//count($arResult["BASKET_ITEM_RENDER_DATA"]);?><?=$arResult["BASKET_ITEM_RENDER_DATA_COUNT"]?> <?=$arResult["DECLINATION_OBJ"]->declinationNum(GetMessage("SBB_POSITION_1"), (int)$arResult["BASKET_ITEM_RENDER_DATA_COUNT"])?></span>
                            <span> <?=$arResult["allSum_FORMATED"];?></span>
                        </div>
                    </div>
                    <div class="total-col">
                        <div class="total-col__head">Запрос образцов:</div>
                        <div class="total-col__content" id="example">
                            <span><?=$arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"];?> <?=$arResult["DECLINATION_OBJ"]->declinationNum(GetMessage("SBB_POSITION_1"), (int)$arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"])?></span>
                        </div>
                    </div>
                    <div class="total-col">
                        <div class="total-col__head">Условия поставки:</div>
                        <div class="total-col__content" id="reqprice">
                            <span><?=$arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"];?> <?=$arResult["DECLINATION_OBJ"]->declinationNum(GetMessage("SBB_POSITION_1"), (int)$arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"])?></span>
                        </div>
                    </div>
                </div>
                <?global $USER;?>
                <a <?if($USER->IsAuthorized()):?>onclick="checkOrderGo('');" <?else:?>href="#modal-login" rel="modal:open"<?endif;?> class="ordering-btn" id="order-go" ><span>Оформить все</span><svg id=""  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400.67 391.32"><path class="cart-full" d="M428.4,310.9H198.8l-5-19.8,237.3-33.2a17,17,0,0,0,13.2-13.7l19.4-106.9a17.36,17.36,0,0,0-3.7-14,16.86,16.86,0,0,0-13.1-6.1H153.6L144.3,77a17.1,17.1,0,0,0-16.6-13.2H80.3a17,17,0,0,0,0,34h33.9l54.4,233.8a17.1,17.1,0,0,0,16.6,13.2H428.3a17,17,0,1,0,.1-33.9ZM160.8,147H417.6a8.11,8.11,0,0,1,8,9.2l-9.7,68.3a9.36,9.36,0,0,1-7.4,7.8L187.3,261.1Z" transform="translate(-63.3 -26.98)"/><path class="cart-full" d="M187,365.9a26.2,26.2,0,1,0,26.2,26.2A26.23,26.23,0,0,0,187,365.9Z" transform="translate(-63.3 -26.98)"/><path class="cart-full" d="M386,365.9a26.2,26.2,0,1,0,26.2,26.2A26.16,26.16,0,0,0,386,365.9Z" transform="translate(-63.3 -26.98)"/><path class="cart-full" d="M394.09,201l-4.39-33.73h-193l7.61,58.44a14.17,14.17,0,0,0,15.88,12.22l161.69-21A14.18,14.18,0,0,0,394.09,201Zm-21-161.69A14.17,14.17,0,0,0,357.16,27.1l-161.69,21A14.18,14.18,0,0,0,183.25,64l4.33,33.25h193Z" transform="translate(-63.3 -26.98)"/></svg></a>
                <?/*<a <?if($USER->IsAuthorized()):?>onclick="checkOrderGo();" <?else:?>href="#modal-login" rel="modal:open"<?endif;?> class="ordering-btn" id="order-go" >Перейти к оформлению</a>*/?>
            </div>
        </div>

    <?if($arParams["AJAX_NOW"] !== true):?>
    </section>

    <div id="modal-project" class="modal">
        <div class="project main-modal">
            <h2>Создать проект</h2>
            <form action="<?=$templateFolder."/ajax_proj_add.php"?>" method="post">
                <div class="project-name">
                    <label for="project-name">Название проекта </label>
                    <input type="text" name="project-name" id="project-name" required>
                </div>
                <div class="project-product">
                    <label for="product">Проектируемое изделие </label>
                    <input type="text" name="project-product" id="project-product" required>
                </div>
                <div class="project-start">
                    <label for="email">Планируемый старт выпуска</label>
                    <input type="text" name="project-start" id="project-start" required>
                </div>
                <div class="project-quant">
                    <label for="project-quant">Количество образцов</label>
                    <input type="number" min="1" name="project-quant" id="project-quant" required>
                </div>
                <div class="project-description">
                    <label for="comment">Описание</label>
                    <textarea name="project-comment" id="project-comment" ></textarea>
                </div>
                <div class="submit">
                    <input type="submit" name="proj-add-form" value="Добавить проект">
                </div>

                <input type="text" name="page-restart" id="page-restart" value="<?=$arParams["PATH_TO_BASKET"];?>" style="display: none">

                <input type="text" name="ib-proj" id="ib-proj" value="<?=$arParams["IBLOCK_PROJ_ID"];?>" style="display: none">

            </form>
        </div>
    </div>



    <!--редактирование проекта-->

    <div id="modal-project-red" class="modal">
        <div class="project main-modal">
            <h2>Редактировать проект</h2>
            <form action="<?=$templateFolder."/ajax_proj_add.php"?>" method="post">
                <div class="project-name">
                    <label for="project-name">Название проекта </label>
                    <input type="text" name="project-name-red" id="project-name-red" required>
                </div>
                <div class="project-product">
                    <label for="product">Проектируемое изделие </label>
                    <input type="text" name="project-product-red" id="project-product-red" required>
                </div>
                <div class="project-start">
                    <label for="email">Планируемый старт выпуска</label>
                    <input type="text" name="project-start-red" id="project-start-red" required>
                </div>
                <div class="project-quant">
                    <label for="project-quant-red">Количество образцов</label>
                    <input type="number" min="1" name="project-quant-red" id="project-quant-red" required>
                </div>
                <div class="project-description">
                    <label for="comment">Описание</label>
                    <textarea name="project-comment-red" id="project-comment-red" ></textarea>
                </div>
                <div class="submit">
                    <input type="submit" name="proj-add-form-red" value="Применить">
                </div>

                <input type="text" name="page-restart" id="page-restart" value="<?=$arParams["PATH_TO_BASKET"];?>" style="display: none">

                <input type="text" name="ib-proj" id="ib-proj" value="<?=$arParams["IBLOCK_PROJ_ID"];?>" style="display: none">

                <input type="text" name="id-proj-for-update" id="id-proj-for-update" style="display: none">

            </form>
        </div>
    </div>
    <?endif;?>
    <script>
        var countMaxQuantity = "<?=$countMaxQuantity;?>";
        var countMinQuantity = "<?=$countMinQuantity;?>";

        var quantityBuy = "<?=$arResult["BASKET_ITEM_RENDER_DATA_COUNT"];?>";
        var quantityExample = "<?=$arResult["BASKET_ITEM_RENDER_DATA_EX_COUNT"];?>";
        var quantityRequest = "<?=$arResult["BASKET_ITEM_RENDER_DATA_REQ_COUNT"];?>";

        var objDel = new BX.Mcart.SymmetronDeclination(<?=CUtil::PhpToJSObject($arResult["DECLINATION_OBJ"]->getDeclinationsArray());?>)
    </script>
<?
}
else
{
    ?>
    <section class="cart-empty">
        <div class="wrapper">
            <div class="cart-empty__header">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640"><defs><path d="M640 148.74L576 388.74L208 424.74L184 468.74L560 468.74L560 510L120 510L172 404.74L88 68.74L0 68.74L0 28.74L120 28.74L148 148.74L640 148.74ZM208 384.74L544 348.74L588 188.74L160 188.74L208 384.74Z" id="a4hFTNc6yA"></path><path d="M155 557.46C155 577.28 139.32 593.37 120 593.37C100.68 593.37 85 577.28 85 557.46C85 537.64 100.68 521.55 120 521.55C139.32 521.55 155 537.64 155 557.46Z" id="i5XpF6zBME"></path><path d="M505 555.91C505 575.73 489.32 591.82 470 591.82C450.68 591.82 435 575.73 435 555.91C435 536.09 450.68 520 470 520C489.32 520 505 536.09 505 555.91Z" id="a7zIP5CG5"></path></defs><g><g><g><use xlink:href="#a4hFTNc6yA" opacity="1" fill-opacity="1"></use><g><use xlink:href="#a4hFTNc6yA" opacity="1" fill-opacity="0" stroke-width="0" stroke-opacity="0"></use></g></g><g><use xlink:href="#i5XpF6zBME" opacity="1" fill-opacity="1"></use></g><g><use xlink:href="#a7zIP5CG5" opacity="1" fill-opacity="1"></use></g></g></g>
                </svg>
                <span class="empty-cart-message">Корзина пуста</span>
            </div>
            <div class="cart-empty__content">
                <div class="empty-cart-content">
                    Чтобы добавить сюда товары или образцы используйте кнопку<br/><span class="add-to-cart"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640"><defs><path d="M640 148.74L576 388.74L208 424.74L184 468.74L560 468.74L560 510L120 510L172 404.74L88 68.74L0 68.74L0 28.74L120 28.74L148 148.74L640 148.74ZM208 384.74L544 348.74L588 188.74L160 188.74L208 384.74Z" id="a4hFTNc6yA"></path><path d="M155 557.46C155 577.28 139.32 593.37 120 593.37C100.68 593.37 85 577.28 85 557.46C85 537.64 100.68 521.55 120 521.55C139.32 521.55 155 537.64 155 557.46Z" id="i5XpF6zBME"></path><path d="M505 555.91C505 575.73 489.32 591.82 470 591.82C450.68 591.82 435 575.73 435 555.91C435 536.09 450.68 520 470 520C489.32 520 505 536.09 505 555.91Z" id="a7zIP5CG5"></path></defs><g><g><g><use xlink:href="#a4hFTNc6yA" opacity="1" fill-opacity="1"></use><g><use xlink:href="#a4hFTNc6yA" opacity="1" fill-opacity="0" stroke-width="0" stroke-opacity="0"></use></g></g><g><use xlink:href="#i5XpF6zBME" opacity="1" fill-opacity="1"></use></g><g><use xlink:href="#a7zIP5CG5" opacity="1" fill-opacity="1"></use></g></g></g>
                       </svg>Добавить в корзину</span> или <span class="take-sample">Запросить образец</span> на странице нужного вам товара.
                </div>
            </div>
            <div class="cart-empty__footer">
                Перейти в <a href="<?=$arParams["URL_TO_CATALOG"]?>">Каталог</a>
            </div>
        </div>
    </section>
    <?
}
?>
<script>
    var mcart_basket_items = <?=CUtil::PhpToJSObject($arResult["ITEMS_JS"])?>;
</script>
