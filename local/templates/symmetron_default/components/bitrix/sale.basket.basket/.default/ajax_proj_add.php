<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("mcart_catalog_quantity");

if(CModule::IncludeModule("iblock"))
{
    //echo "sdfsdfsfdsdf";
    //echo "<pre>";
    //print_r($_POST);
    //echo "</pre>";


        $el = new CIBlockElement;
    if(!empty($_POST["proj-add-form"])) {
        $PROP = array();
        $PROP["PRODUCT"] = $_POST["project-product"];  // свойству с кодом 12 присваиваем значение "Белый"
        $PROP["START"] = $_POST["project-start"];
        $PROP["QUANT_EX"] = $_POST["project-quant"];

        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID" => $_POST["ib-proj"],
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $_POST["project-name"],
            "ACTIVE" => "Y",            // активен
            "PREVIEW_TEXT" => $_POST["project-comment"],
        );

        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            /*global $USER;
            $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("USER", $USER->GetID());
            $arValuesProj = $arUF["UF_PROJECTS"]["VALUE"];

            if(empty($arValuesProj)){
                $arValuesProj = array();
            }

            $arValuesProj[] = $PRODUCT_ID;

            $GLOBALS["USER_FIELD_MANAGER"]->Update("USER", $USER->GetID(), Array ("UF_PROJECTS" => $arValuesProj));*/
        }else {
           // echo "Error: " . $el->LAST_ERROR;
        }
    }

    if(!empty($_POST["proj-add-form-red"])){

        $PROP = array();
        $PROP["PRODUCT"] = $_POST["project-product-red"];  // свойству с кодом 12 присваиваем значение "Белый"
        $PROP["START"] = $_POST["project-start-red"];
        $PROP["QUANT_EX"] = $_POST["project-quant-red"];

        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID" => $_POST["ib-proj"],
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $_POST["project-name-red"],
            "ACTIVE" => "Y",            // активен
            "PREVIEW_TEXT" => $_POST["project-comment-red"],
        );

        if ($PRODUCT_ID = $el->Update(intval($_POST["id-proj-for-update"]), $arLoadProductArray)) {

        }
    }

}

header("Location: ". $_POST["page-restart"]);
exit;
