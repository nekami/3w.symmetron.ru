<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("mcart_catalog_quantity");

if(CModule::IncludeModule("sale"))
{
    $id = htmlspecialchars($_GET["id"]);
    $value = htmlspecialchars($_GET["value"]);
    $valueparent = htmlspecialchars($_GET["valueparent"]);

    if (!empty($id) && !empty($value)) {

        if($value == "MULTIPLICITY") {
            $arItem = CSaleBasket::GetList(
                array("NAME" => "ASC", "ID" => "ASC"),
                array("ID" => $id),
                false,
                false
            //array("ID", "PRICE")
            )->Fetch();

            if (!empty($arItem)) {

                $flagExisting = false;
                $rsBasketItems = CSaleBasket::GetList(
                    array("NAME" => "ASC", "ID" => "ASC"),
                    array(
                        "PRODUCT_ID" => $arItem["PRODUCT_ID"],
                        "!ID" => $arItem["ID"],
                        "FUSER_ID" => $arItem["FUSER_ID"],
                        "ORDER_ID" => ""
                        ),
                    false,
                    false
                //array("ID", "PRICE")
                );

                $arBasketItems = array();
                while ($arBasketItem = $rsBasketItems->Fetch()){
                    $arBasketItems[] = $arBasketItem["ID"];
                }

                if(!empty($arBasketItems)) {
                    $rsProps = CSaleBasket::GetPropsList(
                        array(),
                        array("BASKET_ID" => $arBasketItems)
                    );

                    while ($arProp = $rsProps->Fetch()) {
                        if ($arProp["CODE"] == "REQUEST" && $arProp["VALUE"] == "MULTIPLICITY") {
                            $flagExisting = true;
                        }
                    }

                }

                if (!$flagExisting) {

                    $arItem["PRICE"] = 0;
                    $arItem["PROPS"] = array(
                        array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => $value, "SORT" => 100)
                    );

                    unset($arItem["ID"]);

                    if (CSaleBasket::Add($arItem)) {
                        echo 1;
                    }
                }

            }
        } elseif ($value == "ABORT") {

            if(Cmodule::IncludeModule("catalog") && Cmodule::IncludeModule("currency")) {

                $arItem = CSaleBasket::GetList(
                    array("NAME" => "ASC", "ID" => "ASC"),
                    array("ID" => $id),
                    false,
                    array("ID", "CURRENCY", "QUANTITY", "PRODUCT_ID")
                //array("ID", "PRICE")
                )->Fetch();

                //$baseCurrency = CCurrency::GetBaseCurrency();
                $baseCurrency = $arItem["CURRENCY"];
                $productQuantity = $arItem["QUANTITY"];

                $db_res = CPrice::GetListEx(
                    array("QUANTITY_FROM" => "ASC"),
                    array(
                        "PRODUCT_ID" => $arItem["PRODUCT_ID"],
                        //"CATALOG_GROUP_ID" => 1, //ID типа цены
                        //"CURRENCY" => "RUB" // Валюта
                    )
                );

                $db_result_lang = CCurrencyLang::GetByID($baseCurrency, LANGUAGE_ID);

                $price = 0;
                while ($r = $db_res->Fetch()) {
                    if (($r["QUANTITY_FROM"] <= $productQuantity && $productQuantity <= $r["QUANTITY_TO"]) || ($r["QUANTITY_FROM"] <= $productQuantity && empty($r["QUANTITY_TO"]))) {
                        if ($baseCurrency == $r["CURRENCY"]) {
                            $price = $r["PRICE"];
                        } else {
                            if (empty($db_result_lang["DECIMALS"])) {
                                $db_result_lang["DECIMALS"] = 2;
                            }

                            $price = round(CCurrencyRates::ConvertCurrency($r["PRICE"], $r["CURRENCY"],
                                $baseCurrency), $db_result_lang["DECIMALS"]);
                        }
                        break;
                    }
                }


                if (!empty($price)) {
                    $arFields = array(
                        "PRICE" => $price,
                        "QUANTITY" => $arItem["QUANTITY"],
                    );

                    if(!empty($valueparent)) {
                        $arFields["PROPS"] = array(
                            array("NAME" => "Скрытый", "CODE" => "HIDDEN", "VALUE" => "N", "SORT" => 100),
                        );
                        if(CSaleBasket::Update($valueparent, $arFields))
                            if(CSaleBasket::Delete($id))
                                echo 1;
                    } else {
                        $arFields["PROPS"] = array(
                            array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "", "SORT" => 100),
                        );
                        if (CSaleBasket::Update($arItem["ID"], $arFields)) {
                            echo 1;
                        }
                    }
                }
            }

        } else {

            $arItem = CSaleBasket::GetList(
                array("NAME" => "ASC", "ID" => "ASC"),
                array("ID" => $id),
                false,
                false
            //array("ID", "PRICE")
            )->Fetch();

            if (!empty($arItem)) {

                $arFields = array(
                    "PRICE" => 0,
                    //"QUANTITY" => 0,
                    "PROPS" => array(
                        /*array(
                            "NAME" => "Временная цеана на случай запроса",
                            "CODE" => "PRICE_REQ",
                            "VALUE" => $arItem["PRICE"],
                            "SORT" => 100,
                            //"QUANTITY" => 0,
                        ),*/
                        array("NAME" => "Скрытый", "CODE" => "HIDDEN", "VALUE" => "Y", "SORT" => 100),
                        //array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => $value, "SORT" => 100)
                    )
                );


                $arItem["PRICE"] = 0;
                $arItem["PROPS"] = array(
                    array(
                        "NAME" => "Родительский товар",
                        "CODE" => "PARENT_PRODUCT",
                        "VALUE" => $arItem["ID"],
                        "SORT" => 100
                    ),
                    array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => $value, "SORT" => 100)
                );

                unset($arItem["ID"]);

                /*$arFieldsNew = array(
                    "PRICE" => 0,
                    "PROPS" => array(
                        //array("NAME" => "Временная цеана на случай запроса", "CODE" => "PRICE_REQ", "VALUE" => $arItem["PRICE"], "SORT" => 100),
                        //array("NAME" => "Скрытый", "CODE" => "HIDDEN", "VALUE" => "Y", "SORT" => 100),
                        array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => $value, "SORT" => 100)
                    )
                );*/

                if(CSaleBasket::Update($id, $arFields))
                    if(CSaleBasket::Add($arItem))
                        echo 1;
            }


        }
    }
}
die;