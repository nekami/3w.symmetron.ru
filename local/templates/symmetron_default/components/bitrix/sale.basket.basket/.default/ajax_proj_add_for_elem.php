<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if(CModule::IncludeModule("sale"))
{

    $id = htmlspecialchars($_GET["id"]);
    $value = htmlspecialchars($_GET["value"]);
    if (!empty($id) && !empty($value)) {
        $arFields = array(
            "PROPS" => array(
                array("NAME" => "Образец", "CODE" => "EXAMPLE", "VALUE" => "Y", "SORT" => 100),
                array("NAME" => "Проект", "CODE" => "PROJECT", "VALUE" => $value, "SORT" => 100)
            )
        );
        CSaleBasket::Update($id, $arFields);
    }
}
die;