<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $CACHE_MANAGER;
$CACHE_MANAGER->ClearByTag("mcart_catalog_quantity");
?>

<?
$_POST["PARAMS"]["AJAX_NOW"] = true;
$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "",
    $_POST["PARAMS"]
);
?>
