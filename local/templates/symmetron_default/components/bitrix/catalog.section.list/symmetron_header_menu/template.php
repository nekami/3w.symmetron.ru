<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$prefix = "SYMMETRON_HEADER_MENU_COMPONENT_TEMPLATE_";
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


$countItems = count($arResult["SECTIONS"]);
$distribution = 0;
if($mod = $countItems % 3)
    $distribution = ($countItems - $mod) / 3 + 1;
else
    $distribution = $countItems / 3;

?>
<a href="<?=$arParams["CATALOG_URL"]?>"><?=Loc::GetMessage($prefix."CATALOG_TITLE")?></a>
<div class="inner-menu inner-catalog">
    <div class="wrapper">
        <div class="catalog-top__menu">
            <?$counter = 0;?>
            <?
            $lastElem = end($arResult["SECTIONS"])["ID"];
            ?>
            <?foreach ($arResult["SECTIONS"] as $section):?>
                <?if($counter == 0):?>
                    <ul>
                <?endif;?>

                        <li><a href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a></li>

                <?$counter++;
                if($counter == $distribution || $lastElem == $section["ID"]):
                    $counter = 0;?>
                    </ul>
                <?endif;?>

            <?endforeach;?>
        </div>
    </div>
</div>
