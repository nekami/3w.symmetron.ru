$('.favorite-select').on('click', function(e){
    if ($(this).next('.mini-card').hasClass('open')) {
        $(this).next('.mini-card').removeClass('open')
        $(this).next('.mini-card').children().children().slideUp(300)
    } else {
        $(this).next('.mini-card').addClass('open')
        $(this).next('.mini-card').children().children().slideDown(300)
    }
})