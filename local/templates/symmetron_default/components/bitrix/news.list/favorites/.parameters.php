<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_NAME" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_NAME"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
);



if(!CModule::IncludeModule("iblock"))
    return false;
$IBLOCK_ID = $arCurrentValues["IBLOCK_ID"];
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$IBLOCK_ID));

$arPropList = array();
while ($prop_fields = $properties->GetNext())
{
    $arPropList[$prop_fields["CODE"]] = $prop_fields["NAME"];
    //echo $prop_fields["ID"]." - ".$prop_fields["NAME"]."<br>";
}


$arTemplateParameters["PROPERTY_CODE_FAST_SHOW_LEFT"] = array(
    //"PARENT" => "LIST_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FAST_SHOW_LEFT_FAV"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FAST_SHOW_LEFT']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);

$arTemplateParameters["PROPERTY_CODE_FAST_SHOW_RIGHT"] = array(
    //"PARENT" => "LIST_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FAST_SHOW_RIGHT_FAV"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FAST_SHOW_RIGHT']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);

?>
