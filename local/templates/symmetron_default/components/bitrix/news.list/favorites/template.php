<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
global $USER;

if (USER_ID) {

    $res = CUser::GetList($by = "id", $order = "asc", ['ID' => USER_ID], ['SELECT' => ["UF_FAVS"]])->Fetch();
    $favs = $res["UF_FAVS"];

}

$prefix = "SYMMETRON_DEFAULT_SITE_TEMPLATE_";
$this->setFrameMode(true);

CJSCore::Init(array('currency'));
?>
<script>

    function addToBasket(id, baskProp, url) {
        var quant = document.getElementById("quant_elems_" + id).value;
        var baskPar = {};
        baskPar["ajax_basket"] = "Y";
        if (baskProp != "")
            baskPar["basket_props"] = baskProp;
        baskPar["quantity"] = quant;
        BX.ajax.loadJSON(
            url + "?action=ADD2BASKET&id=" + id,
            baskPar,
            function (data) {
                if (data.STATUS == "OK") {
                    BX.Mcart.modals.result_off.open(["Уведомление", data.MESSAGE, {
                        url: "/cart/",
                        text: "Перейти в корзину"
                    }]);
                } else {
                    BX.Mcart.modals.result_off.open(["Ошибка!", data.MESSAGE]);
                }
            }
        );

    }

    var path = "<?=$templateFolder;?>";

    function addToFavs(el, id, idmini) {
        if (id)
            BX.ajax.get(
                "/ajax.php?id=" + id + "&action=fav",
                function (data) {
                    data = JSON.parse(data);
                    if (!data.active) {
                        $('.favorite-table tr[data-id=' + id + ']').remove();
                        document.getElementById(idmini).remove();
                    }
                }
            );
    }

    function noAuth() {
        BX.Mcart.modals.result_off.open(["Ошибка!", "Вам нужно авторизоваться"]);
    }

    <?$currencyFormat = CCurrencyLang::GetFormatDescription('RUB');?>
    BX.Currency.setCurrencyFormat('RUB', <? echo CUtil::PhpToJSObject($currencyFormat, false, true); ?>);

</script>
<div class="favorite-table">
    <table>
        <tr>
            <th><?= Loc::getMessage($prefix . "ARTICLE"); ?></th>
            <th><?= Loc::getMessage($prefix . "MANUFACTURER"); ?></th>
            <th><?= Loc::getMessage($prefix . "DESCRIPTION"); ?></th>
            <th><?= Loc::getMessage($prefix . "ACTIVE_STATUS"); ?></th>
            <th><?= Loc::getMessage($prefix . "RETAIL_PRICE"); ?></th>
            <? if ( empty($arParams["NEWS_CARD"]) ) { ?> <th></th> <? } ?>
        </tr>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>

            <?
            $areaId = $this->GetEditAreaId($arItem['ID']);
            $itemIds = array(
                'ID' => $areaId,
                'PICT' => $areaId . '_pict',
                'SECOND_PICT' => $areaId . '_secondpict',
                'PICT_SLIDER' => $areaId . '_pict_slider',
                'STICKER_ID' => $areaId . '_sticker',
                'SECOND_STICKER_ID' => $areaId . '_secondsticker',
                'QUANTITY' => $areaId . '_quantity',
                'QUANTITY_DOWN' => $areaId . '_quant_down',
                'QUANTITY_UP' => $areaId . '_quant_up',
                'QUANTITY_MEASURE' => $areaId . '_quant_measure',
                'QUANTITY_LIMIT' => $areaId . '_quant_limit',
                'BUY_LINK' => $areaId . '_buy_link',
                'BASKET_ACTIONS' => $areaId . '_basket_actions',
                'NOT_AVAILABLE_MESS' => $areaId . '_not_avail',
                'SUBSCRIBE_LINK' => $areaId . '_subscribe',
                'COMPARE_LINK' => $areaId . '_compare_link',
                'PRICE' => $areaId . '_price',
                'PRICE_OLD' => $areaId . '_price_old',
                'PRICE_TOTAL' => $areaId . '_price_total',
                'DSC_PERC' => $areaId . '_dsc_perc',
                'SECOND_DSC_PERC' => $areaId . '_second_dsc_perc',
                'PROP_DIV' => $areaId . '_sku_tree',
                'PROP' => $areaId . '_prop_',
                'DISPLAY_PROP_DIV' => $areaId . '_sku_prop',
                'BASKET_PROP_DIV' => $areaId . '_basket_prop',
            );

            ?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

            $k = empty($favs) ? false : array_search($arItem["ID"], $favs);
            $is_favs = $k !== FALSE ? true : false;
            ?>
            <tr id="<?= $this->GetEditAreaId($arItem['ID']); ?>" data-id="<?= $arItem['ID'] ?>" class="favorite-select">
                <td><a id="name_prod_<?= $arItem["ID"]; ?>"
                       class="disabled"><?= (key_exists("ARTICLE", $arItem["PROPERTIES"]) &&
                        key_exists("VALUE", $arItem["PROPERTIES"]['ARTICLE']) &&
                        !empty($arItem["PROPERTIES"]['ARTICLE']['VALUE']) ?
                            $arItem["PROPERTIES"]["ARTICLE"]["VALUE"] :
                            "") ?></a></td>
                <td><?= (key_exists("PRODUCERID", $arItem["PROPERTIES"]) &&
                    key_exists("VALUE", $arItem["PROPERTIES"]['PRODUCERID']) &&
                    !empty($arItem["PROPERTIES"]['PRODUCERID']['VALUE']) &&
                    key_exists($arItem["PROPERTIES"]['PRODUCERID']["VALUE"], $arResult["brandsIds"]) ?
                        $arResult["brandsIds"][$arItem["PROPERTIES"]['PRODUCERID']["VALUE"]] :
                        "") ?></td>
                <td><?= (key_exists("description", $arItem["PROPERTIES"]) &&
                    key_exists("VALUE", $arItem["PROPERTIES"]['description']) &&
                    is_array($arItem["PROPERTIES"]['description']['VALUE']) &&
                    key_exists("TEXT", $arItem["PROPERTIES"]['description']['VALUE']) ?
                        $arItem["PROPERTIES"]['description']["VALUE"]["TEXT"] :
                        "") ?></td>
                <td><?= (key_exists("ACTIVE_STATUS", $arItem["PROPERTIES"]) &&
                    key_exists("DISPLAY_VALUE", $arItem["PROPERTIES"]['ACTIVE_STATUS']) &&
                    !empty($arItem["PROPERTIES"]['ACTIVE_STATUS']['DISPLAY_VALUE']) ?
                        $arItem["PROPERTIES"]['ACTIVE_STATUS']["DISPLAY_VALUE"] :
                        "") ?></td>
                <td><?= (key_exists("RETAIL_PRICE", $arItem["PROPERTIES"]) &&
                    key_exists("DISPLAY_VALUE", $arItem["PROPERTIES"]['RETAIL_PRICE']) &&
                    !empty($arItem["PROPERTIES"]['RETAIL_PRICE']['DISPLAY_VALUE']) ?
                        $arItem["PROPERTIES"]['RETAIL_PRICE']["DISPLAY_VALUE"] :
                        "") ?></td>
                
				
				<? if ( empty($arParams["NEWS_CARD"]) ) { ?>					
					<td>
						<span class="favorite-heart <?= $is_favs ? 'act' : '' ?>"
							  <? if (AUTH_USER): ?>onclick="event.stopPropagation(); addToFavs(this,'<?= $arItem['ID'] ?>','<?= $itemIds["ID"] ?>');"
							  <? else: ?>onclick="noAuth();"<? endif; ?>>
							<svg class="remove-fav" version="1.1" xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
								<g id="X">
									<line id="_x5C_" fill="none" stroke="black" stroke-width="10"
										  transform="translate(5, 5)"
										  stroke-miterlimit="10" x1="5" y1="5" x2="50" y2="50" stroke-linecap="round"/>
									<line id="_x2F_" fill="none" stroke="black" stroke-width="10"
										  transform="translate(5, 5)"
										  stroke-miterlimit="10" x1="5" y1="50" x2="50" y2="5" stroke-linecap="round"/>
								</g>
								<g id="circle">
									<circle id="XMLID_16_" opacity="0" fill="none" stroke-width="3" stroke="black"
											stroke-miterlimit="10" cx="30" cy="30" r="40"/>
								</g>
							</svg>
						</span>
					</td>					
				<? } ?>				
            </tr>
            <?
            $itemParams = [
                "RESULT" => [
                    "COLUMNS" => [
                        [], [], [], [], [], []
                    ],
                    "ITEM" => $arItem,
                    "AREA_ID" => "",
                    "FAV_COMPONENT" => true,
                    "IS_FAV" => in_array($arItem["ID"], $favs),
                    "SECTION_NAME" => $arResult["ALL_SECTIONS"][$arItem["IBLOCK_SECTION_ID"]],
                    "PACK_INFO" => $arItem["PACK_INFO"],
                    "ITEM_MEASURE" => $arItem["ITEM_MEASURE"],
                    "BASKET_PRODS" => $arResult["BASKET_PRODS"],
                    "DECLINATION_OBJ" => $arResult["DECLINATION_OBJ"]
                ],
                "PARAMS" => [
                    "PROPERTY_LEFT" => $arParams["PROPERTY_CODE_FAST_SHOW_LEFT"],
                    "PROPERTY_RIGHT" => $arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"],
                ],
            ];
            ?>
            <? $APPLICATION->IncludeComponent(
                'bitrix:catalog.item',
                'mini',
                $itemParams,
                $component,
                array('HIDE_ICONS' => 'Y')
            ); ?>
        <? endforeach; ?>
    </table>
</div>
