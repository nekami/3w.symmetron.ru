<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$itemIds = array();

$arResult["brandsIds"] = [];
$arResult["CUR_CURRENCY"] = 'RUB';
if (!empty($arResult["ITEMS"])) {
    $itemIds = array_column($arResult["ITEMS"], "ID");


    $resMeasure = \CCatalogMeasure::getList([], [], false);
    $arResult["MEASURE_DATA"] = [];
    while ($row = $resMeasure->fetch()) {
        $arResult["MEASURE_DATA"][$row["ID"]] = $row;
    }

    foreach ($arResult["ITEMS"] as $key => $arItem) {
        if (key_exists("PRODUCERID", $arItem["PROPERTIES"]) &&
            key_exists("VALUE", $arItem["PROPERTIES"]['PRODUCERID']) &&
            !empty($arItem["PROPERTIES"]['PRODUCERID']['VALUE'])) {

            $arResult["brandsIds"][] = $arItem["PROPERTIES"]['PRODUCERID']['VALUE'];
        }

        $arStatus = &$arResult["ITEMS"][$key]["PROPERTIES"]["ACTIVE_STATUS"];
        if ($arStatus["VALUE_XML_ID"] == "ACTIVE") {
            $arStatus["DISPLAY_VALUE"] = "<span class='active-status stock'>В наличии</span>";
        } else {
            $arStatus["DISPLAY_VALUE"] = "<span class='active-status out'>Нет в наличии</span>";
        }

        if (key_exists("RETAIL_PRICE", $arItem["PROPERTIES"]) &&
            key_exists("VALUE", $arItem["PROPERTIES"]['RETAIL_PRICE']) &&
            !empty($arItem["PROPERTIES"]['RETAIL_PRICE']['VALUE'])) {

            $arRetailPrice = &$arResult["ITEMS"][$key]["PROPERTIES"]["RETAIL_PRICE"];
            $measurePrice = $arResult["MEASURE_DATA"][$arItem["ITEM_MEASURE"]["SYMBOL_RUS"]] ?: Loc::getMessage("CT_BCS_TPL_MESS_SHT");

            $arRetailPrice["DISPLAY_VALUE"] = \CCurrencyLang::CurrencyFormat(
                    $arRetailPrice["VALUE"],
                    $arResult["CUR_CURRENCY"],
                    true
                ) . "/" . $measurePrice . ".";
        }
    }

    $arSelect = ["ID", "NAME"];
    $arFilter = [
        "ID" => $arResult["brandsIds"],
    ];
    $ob = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

    $arResult["brandsIds"] = [];
    while ($res = $ob->Fetch()) {
        $arResult["brandsIds"][$res["ID"]] = $res["NAME"];
    }
}


//    $arNoActiveBrand = [];
//    $brandIblockId = 0;
//
//    foreach ($arResult["ITEMS"] as $arItem) {
//        $brandsIDS[] = $arItem["PROPERTIES"]['PRODUCERID']['VALUE'];
//        $itemIds[] = $arItem["ID"];
//        $arBrand = $arItem["DISPLAY_PROPERTIES"]["PRODUCERID"];
//        $brandIblockId = $arBrand["LINK_IBLOCK_ID"];
//        if (empty($arBrand["LINK_ELEMENT_VALUE"]) && !empty($arBrand["~VALUE"])
//            && in_array("PRODUCERID", $arParams["PROPERTY_CODE"])) {
//            $arNoActiveBrand[] = $arBrand["~VALUE"];
//        }
//
//    }
//
//    //get no active brand
//    $arNoActiveBrands = [];
//    if (!empty($arNoActiveBrand)) {
//        $arSelect = ["ID", "NAME"];
//        $arFilter = [
//            "IBLOCK_ID" => IntVal($brandIblockId),
//            "ID" => $arNoActiveBrand,
//            "ACTIVE" => "N",
//            "PROPERTY_ACTIVE" => "Y"
//        ];
//        $rsNoActiveBrands = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
//        while ($arNoActiveBrand = $rsNoActiveBrands->Fetch()) {
//            $arNoActiveBrands[$arNoActiveBrand["ID"]] = $arNoActiveBrand["NAME"];
//        }
//    }
//
//
//    foreach ($arResult["ITEMS"] as $key => $arItem) {
//
//        $arBrand = &$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["PRODUCERID"];
//        if (empty($arBrand["LINK_ELEMENT_VALUE"]) && !empty($arBrand["~VALUE"])
//            && in_array("PRODUCERID", $arParams["PROPERTY_CODE"])) {
//            $arBrand["DISPLAY_VALUE"] = $arNoActiveBrands[$arBrand["~VALUE"]];
//        }
//
//    }


/*if( !empty($brandsIDS) ) {
    $arSelect = Array("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID"=>11, "ID"=>array( array_filter( $brandsIDS ) ), "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){
        $fields = $ob->GetFields();
        $arResult['brands'][$fields['ID']] = $fields;
    }
}*/
//}


/* Get prices */
/* Load */
$pricess = [];
$ratiossss = [];
$storage = [];
$storage['PRICES_CAN_BUY'][1] = "Y";
if (!empty($itemIds)) {
    \Bitrix\Main\Type\Collection::normalizeArrayValuesByInt($itemIds, true);
    $emptyRatioIds = array_fill_keys($itemIds, true);

    $iterator = \Bitrix\Catalog\MeasureRatioTable::getList(array(
        'select' => array('ID', 'RATIO', 'IS_DEFAULT', 'PRODUCT_ID'),
        'filter' => array('@PRODUCT_ID' => $itemIds),
        'order' => array('PRODUCT_ID' => 'ASC')// not add 'RATIO' => 'ASC' - result will be resorted after load prices
    ));
    while ($row = $iterator->fetch()) {
        $ratio = ((float)$row['RATIO'] > (int)$row['RATIO'] ? (float)$row['RATIO'] : (int)$row['RATIO']);
        if ($ratio > CATALOG_VALUE_EPSILON) {
            $row['RATIO'] = $ratio;
            $row['ID'] = (int)$row['ID'];
            $id = (int)$row['PRODUCT_ID'];
            if (!isset($ratiossss[$id]))
                $ratiossss[$id] = array();
            $ratiossss[$id][$row['ID']] = $row;
            unset($emptyRatioIds[$id]);
            unset($id);
        }
        unset($ratio);
    }
    unset($row, $iterator);
    if (!empty($emptyRatioIds)) {
        $emptyRatio = array(
            'ID' => 0,
            'RATIO' => 1,
            'IS_DEFAULT' => 'Y'
        );
        foreach (array_keys($emptyRatioIds) as $id) {
            $ratiossss[$id] = array(
                $emptyRatio['ID'] => $emptyRatio
            );
        }
        unset($id, $emptyRatio);
    }
    unset($emptyRatioIds);


    \Bitrix\Main\Type\Collection::normalizeArrayValuesByInt($itemIds, true);

    $ratioList = array_fill_keys($itemIds, array());
    $quantityList = array_fill_keys($itemIds, array());

    $select = array(
        'ID', 'PRODUCT_ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY',
        'QUANTITY_FROM', 'QUANTITY_TO'
    );

    $pagedItemIds = array_chunk($itemIds, 500);
    foreach ($pagedItemIds as $pageIds) {
        if (empty($pageIds))
            continue;

        $iterator = \Bitrix\Catalog\PriceTable::getList(array(
            'select' => $select,
            'filter' => array('@PRODUCT_ID' => $pageIds),
            'order' => array('PRODUCT_ID' => 'ASC', 'CATALOG_GROUP_ID' => 'ASC')
        ));
        while ($row = $iterator->fetch()) {
            $id = (int)$row['PRODUCT_ID'];
            unset($row['PRODUCT_ID']);
            if (!isset($pricess[$id])) {
                $pricess[$id] = array(
                    'RATIO' => array(),
                    'QUANTITY' => array(),
                    'SIMPLE' => array()
                );
            }

            if ($row['QUANTITY_FROM'] !== null || $row['QUANTITY_TO'] !== null) {
                $hash = ($row['QUANTITY_FROM'] === null ? 'ZERO' : $row['QUANTITY_FROM']) .
                    '-' . ($row['QUANTITY_TO'] === null ? 'INF' : $row['QUANTITY_TO']);
                if (!isset($quantityList[$id][$hash])) {
                    $quantityList[$id][$hash] = array(
                        'HASH' => $hash,
                        'QUANTITY_FROM' => $row['QUANTITY_FROM'],
                        'QUANTITY_TO' => $row['QUANTITY_TO'],
                        'SORT_FROM' => (int)$row['QUANTITY_FROM'],
                        'SORT_TO' => ($row['QUANTITY_TO'] === null ? INF : (int)$row['QUANTITY_TO'])
                    );
                }
                if (!isset($pricess[$id]['QUANTITY'][$hash])) {
                    $pricess[$id]['QUANTITY'][$hash] = array();
                }
                $pricess[$id]['QUANTITY'][$hash][$row['CATALOG_GROUP_ID']] = $row;
                unset($hash);
            } elseif ($row['MEASURE_RATIO_ID'] === null && $row['QUANTITY_FROM'] === null && $row['QUANTITY_TO'] === null) {
                $pricess[$id]['SIMPLE'][$row['CATALOG_GROUP_ID']] = $row;
            }
            $storage['CURRENCY_LIST'][$row['CURRENCY']] = $row['CURRENCY'];

            unset($id);
        }
        unset($row, $iterator);
    }
    unset($pageIds, $pagedItemIds);

    foreach ($itemIds as $id) {
        if (isset($pricess[$id])) {
            foreach ($pricess[$id] as $key => $data) {
                if (empty($data))
                    unset($pricess[$id][$key]);
            }
            unset($key, $data);

            if (count($pricess[$id]) !== 1) {
                unset($pricess[$id]);
            } else {
                if (!empty($pricess[$id]['QUANTITY'])) {
                    $productQuantity = $quantityList[$id];
                    \Bitrix\Main\Type\Collection::sortByColumn(
                        $productQuantity,
                        array('SORT_FROM' => SORT_ASC, 'SORT_TO' => SORT_ASC),
                        '', null, true
                    );
                    $quantityRanges[$id] = $productQuantity;
                    unset($productQuantity);

                    if (count($ratiossss[$id]) > 1)
                        compactItemRatios($id, $ratiossss);
                }
                if (!empty($pricess[$id]['SIMPLE'])) {
                    $range = array(
                        'HASH' => 'ZERO-INF',
                        'QUANTITY_FROM' => null,
                        'QUANTITY_TO' => null,
                        'SORT_FROM' => 0,
                        'SORT_TO' => INF
                    );
                    $quantityRanges[$id] = array(
                        $range['HASH'] => $range
                    );
                    unset($range);
                    if (count($ratiossss[$id]) > 1)
                        compactItemRatios($id, $ratiossss);
                }
            }
        }
    }
    unset($id);

    unset($quantityList, $ratioList);

    unset($enableCompatible);
}

function compactItemRatios($id, &$ratiossss)
{
    if (!isset($ratiossss[$id]))
        $ratioId = null;
    else {
        $minimal = null;
        $minimalRatio = null;
        $result = null;
        foreach ($ratiossss[$id] as $ratio) {
            if ($minimalRatio === null || $minimalRatio > $ratio['RATIO']) {
                $minimalRatio = $ratio['RATIO'];
                $minimal = $ratio['ID'];
            }
            if ($ratio['IS_DEFAULT'] === 'Y') {
                $result = $ratio['ID'];
                break;
            }
        }
        unset($ratio);
        $ratioId = ($result === null ? $minimal : $result);
    }

    if ($ratioId === null)
        return;
    $ratiossss[$id] = array(
        $ratioId => $ratiossss[$id][$ratioId]
    );
}

/* Prepare */
$items = &$arResult["ITEMS"];

$arSectionsIDs = [];

foreach (array_keys($items) as $index) {
    $arSectionsIDs[] = $items[$index]["IBLOCK_SECTION_ID"];
    $id = $items[$index]['ID'];
    if (empty($pricess[$id]))
        continue;
    $productPrices = $pricess[$id];
    $result = array(
        'ITEM_PRICE_MODE' => null,
        'ITEM_PRICES' => array(),
        'ITEM_PRICES_CAN_BUY' => false
    );
    $priceBlockIndex = 0;
    if (!empty($productPrices['QUANTITY'])) {
        $result['ITEM_PRICE_MODE'] = \Bitrix\Catalog\ProductTable::PRICE_MODE_QUANTITY;
        $ratio = current($ratiossss[$id]);

        foreach ($quantityRanges[$id] as $range) {
            $priceBlock = calculatePriceBlock(
                $items[$index],
                $productPrices['QUANTITY'][$range['HASH']],
                $ratio['RATIO'],
                ($range['QUANTITY_FROM'] === null && $range['QUANTITY_TO'] === null),
                $storage
            );

            if (!empty($priceBlock)) {
                $minimalPrice = $priceBlock;
                if ($minimalPrice['QUANTITY_FROM'] === null) {
                    $minimalPrice['MIN_QUANTITY'] = $ratio['RATIO'];
                } else {
                    $minimalPrice['MIN_QUANTITY'] = $ratio['RATIO'] * ((int)($minimalPrice['QUANTITY_FROM'] / $ratio['RATIO']));
                    if ($minimalPrice['MIN_QUANTITY'] < $minimalPrice['QUANTITY_FROM'])
                        $minimalPrice['MIN_QUANTITY'] += $ratio['RATIO'];
                }
                $result['ITEM_PRICES'][$priceBlockIndex] = $minimalPrice;
                if (isset($storage['PRICES_CAN_BUY'][$minimalPrice['PRICE_TYPE_ID']]))
                    $result['ITEM_PRICES_CAN_BUY'] = true;
                unset($minimalPrice);
                $priceBlockIndex++;
            }
            unset($priceBlock);
        }
        unset($range);
        unset($ratio);
    }
    if (!empty($productPrices['SIMPLE'])) {
        $result['ITEM_PRICE_MODE'] = \Bitrix\Catalog\ProductTable::PRICE_MODE_SIMPLE;
        $ratio = current($ratiossss[$id]);
        $priceBlock = calculatePriceBlock(
            $items[$index],
            $productPrices['SIMPLE'],
            $ratio['RATIO'],
            true,
            $storage
        );
        if (!empty($priceBlock)) {
            $minimalPrice = $priceBlock;
            $minimalPrice['MIN_QUANTITY'] = $ratio['RATIO'];
            $result['ITEM_PRICES'][$priceBlockIndex] = $minimalPrice;
            if (isset($storage['PRICES_CAN_BUY'][$minimalPrice['PRICE_TYPE_ID']]))
                $result['ITEM_PRICES_CAN_BUY'] = true;
            unset($minimalPrice);
            $priceBlockIndex++;
        }
        unset($priceBlock);
        unset($ratio);
    }
    $pricess[$id] = $result;
    $items[$index]["ITEM_PRICES"] = $result["ITEM_PRICES"];
    $items[$index]["CATALOG_AVAILABLE"] = $result["ITEM_PRICES_CAN_BUY"];
    $items[$index]["CAN_BUY_ZERO"] = $result["ITEM_PRICES_CAN_BUY"];
    $items[$index]["ITEM_MEASURE_RATIOS"] = $ratiossss[$id];
    $items[$index]["ITEM_MEASURE"] = ["ID" => -1];


    unset($priceBlockIndex, $result);
    unset($productPrices);
}

function calculatePriceBlock(array $product, array $priceBlock, $ratio, $defaultBlock = false, &$storage)
{
    if (empty($product) || empty($priceBlock))
        return null;

    global $USER;
    $result = array(2);
    if (isset($USER) && $USER instanceof \CUser) {
        $result = $USER->GetUserGroupArray();
        \Bitrix\Main\Type\Collection::normalizeArrayValuesByInt($result, true);
    }
    $userGroups = $result;

    $baseCurrency = \Bitrix\Currency\CurrencyManager::getBaseCurrency();
    /** @var null|array $minimalPrice */
    $minimalPrice = null;
    /** @var null|array $minimalBuyerPrice */
    $minimalBuyerPrice = null;
    $fullPrices = array();

    $currencyConvert = true;
    $resultCurrency = ($currencyConvert ? "RUB" : null);

    $vatRate = (float)$product['PRODUCT']['VAT_RATE'];
    $percentVat = $vatRate * 0.01;
    $percentPriceWithVat = 1 + $percentVat;
    $vatInclude = $product['PRODUCT']['VAT_INCLUDED'] === 'Y';

    $oldPrices = array();
    $oldMinPrice = false;
    $oldMatrix = false;

    foreach ($priceBlock as $rawPrice) {
        $priceType = (int)$rawPrice['CATALOG_GROUP_ID'];
        $price = (float)$rawPrice['PRICE'];
        if (!$vatInclude)
            $price *= $percentPriceWithVat;
        $currency = $rawPrice['CURRENCY'];

        $changeCurrency = $currencyConvert && $currency !== $resultCurrency;
        if ($changeCurrency) {
            $price = \CCurrencyRates::ConvertCurrency($price, $currency, $resultCurrency);
            $currency = $resultCurrency;
        }

        $discounts = array();
        if (\CIBlockPriceTools::isEnabledCalculationDiscounts()) {
            \CCatalogDiscountSave::Disable();
            $discounts = \CCatalogDiscount::GetDiscount(
                $product['ID'],
                $product['IBLOCK_ID'],
                array($priceType),
                $userGroups,
                'N',
                "s1",
                array()
            );
            \CCatalogDiscountSave::Enable();
        }
        $discountPrice = \CCatalogProduct::CountPriceWithDiscount(
            $price,
            $currency,
            $discounts
        );
        if ($discountPrice !== false) {
            $priceWithVat = array(
                'UNROUND_BASE_PRICE' => $price,
                'UNROUND_PRICE' => $discountPrice,
                'BASE_PRICE' => \Bitrix\Catalog\Product\Price::roundPrice(
                    $priceType,
                    $price,
                    $currency
                ),
                'PRICE' => \Bitrix\Catalog\Product\Price::roundPrice(
                    $priceType,
                    $discountPrice,
                    $currency
                )
            );

            $price /= $percentPriceWithVat;
            $discountPrice /= $percentPriceWithVat;

            $priceWithoutVat = array(
                'UNROUND_BASE_PRICE' => $price,
                'UNROUND_PRICE' => $discountPrice,
                'BASE_PRICE' => \Bitrix\Catalog\Product\Price::roundPrice(
                    $priceType,
                    $price,
                    $currency
                ),
                'PRICE' => \Bitrix\Catalog\Product\Price::roundPrice(
                    $priceType,
                    $discountPrice,
                    $currency
                )
            );

            $priceRow = $priceWithoutVat;
            $priceRow['ID'] = $rawPrice['ID'];
            $priceRow['PRICE_TYPE_ID'] = $rawPrice['CATALOG_GROUP_ID'];
            $priceRow['CURRENCY'] = $currency;

            if (
                empty($discounts)
                || ($priceRow['BASE_PRICE'] <= $priceRow['PRICE'])
            ) {
                $priceRow['BASE_PRICE'] = $priceRow['PRICE'];
                $priceRow['DISCOUNT'] = 0;
                $priceRow['PERCENT'] = 0;
            } else {
                $priceRow['DISCOUNT'] = $priceRow['BASE_PRICE'] - $priceRow['PRICE'];
                $priceRow['PERCENT'] = roundEx(100 * $priceRow['DISCOUNT'] / $priceRow['BASE_PRICE'], 0);
            }

            $priceRow['QUANTITY_FROM'] = $rawPrice['QUANTITY_FROM'];
            $priceRow['QUANTITY_TO'] = $rawPrice['QUANTITY_TO'];
            $priceRow['QUANTITY_HASH'] = ($rawPrice['QUANTITY_FROM'] === null ? 'ZERO' : $rawPrice['QUANTITY_FROM']) .
                '-' . ($rawPrice['QUANTITY_TO'] === null ? 'INF' : $rawPrice['QUANTITY_TO']);
            $priceRow['MEASURE_RATIO_ID'] = $rawPrice['MEASURE_RATIO_ID'];
            $priceRow['PRICE_SCALE'] = \CCurrencyRates::ConvertCurrency(
                $priceRow['PRICE'],
                $priceRow['CURRENCY'],
                $baseCurrency
            );

            if ($minimalPrice === null || $minimalPrice['PRICE_SCALE'] > $priceRow['PRICE_SCALE'])
                $minimalPrice = $priceRow;
            if (isset($storage['PRICES_CAN_BUY'][$priceRow['PRICE_TYPE_ID']])) {
                if ($minimalBuyerPrice === null || $minimalBuyerPrice['PRICE_SCALE'] > $priceRow['PRICE_SCALE'])
                    $minimalBuyerPrice = $priceRow;
            }
        }
        unset($discounts);
        unset($priceType);
    }
    unset($price);

    if (is_array($minimalBuyerPrice))
        $minimalPrice = $minimalBuyerPrice;
    if (is_array($minimalPrice)) {
        unset($minimalPrice['PRICE_SCALE']);
        $prepareFields = array(
            'BASE_PRICE', 'PRICE', 'DISCOUNT'
        );

        foreach ($prepareFields as $fieldName) {
            $minimalPrice['PRINT_' . $fieldName] = \CCurrencyLang::CurrencyFormat(
                $minimalPrice[$fieldName],
                $minimalPrice['CURRENCY'],
                true
            );
            $minimalPrice['RATIO_' . $fieldName] = $minimalPrice[$fieldName] * $ratio;
            $minimalPrice['PRINT_RATIO_' . $fieldName] = \CCurrencyLang::CurrencyFormat(
                $minimalPrice['RATIO_' . $fieldName],
                $minimalPrice['CURRENCY'],
                true
            );
        }
        unset($fieldName);

        unset($prepareFields);
    }

    unset($oldMatrix, $oldMinPrice, $oldPrices);

    return $minimalPrice;
}

/* END get prices */

/* Get measure id */
if (!empty($itemIds)) {
    $result = \Bitrix\Catalog\ProductTable::getList([
        "filter" => ["ID" => $itemIds],
        "select" => ["ID", "MEASURE", "AVAILABLE", "QUANTITY"],
    ]);
    while ($row = $result->fetch()) {
        $index = array_search($row["ID"], $itemIds);

        foreach ($items as $index => $item) {
            if ($item["ID"] != $row["ID"]) {
                continue;
            }
            $items[$index]["ITEM_MEASURE"] = ["ID" => $row["MEASURE"]];
            $items[$index]["CATALOG_QUANTITY"] = $row["QUANTITY"];
            //$items[$index]["CATALOG_CAN_BUY_ZERO"] = $row["CAN_BUY_ZERO"];
            $items[$index]["CATALOG_AVAILABLE"] = $row["AVAILABLE"];
            break;
        }
    }
}


if (\Bitrix\Main\Loader::includeModule("mcart.basketsymmetron")) {

    $arPackingMeasureData = CBasketSymmetron::getInfoMeasureAndPacking($itemIds, $arParams["IBLOCK_ID"]);

    foreach ($arResult["ITEMS"] as $key => $arItem) {
        $arResult["ITEMS"][$key]["PACK_INFO"] = $arPackingMeasureData["PACKING"][$arItem["ID"]];
        $arResult["ITEMS"][$key]["PACK_INFO"]["SYMPACKAGE"] = (int)$arResult["ITEMS"][$key]["PACK_INFO"]["SYMPACKAGE"];
        $arResult["ITEMS"][$key]["ITEM_MEASURE"] = $arPackingMeasureData["MEASURES"][$arItem["ID"]];
    }

}

//get sections
$rsSec = Bitrix\Iblock\SectionTable::getList(array(
    "filter" => array("ID" => $arSectionsIDs),
    "select" => array("ID", "NAME")
));
while ($arSec = $rsSec->Fetch()) {
    $arResult["ALL_SECTIONS"][$arSec["ID"]] = $arSec["NAME"];
}

//get products from basket
$arResult["BASKET_PRODS"] = \Bitrix\Sale\Basket::loadItemsForFUser(
    \Bitrix\Sale\Fuser::getId(),
    \Bitrix\Main\Context::getCurrent()->getSite()
);

// get information for declination

$arWords = [];
foreach ($arResult["ITEMS"] as $arItem) {

    $val = trim($arItem["ITEM_MEASURE"]["MEASURE_TITLE"]);
    if (!empty($val))
        $arWords[] = strtolower($val);

    $val = trim($arItem["PACK_INFO"]["UPAK"]["VALUE"]);
    if (!empty($val))
        $arWords[] = strtolower($val);
}

$arWords[] = GetMessage("SM_FAV_MESS_UNITS");
$arWords[] = GetMessage("SM_FAV_MESS_PRODUCTS");

$arResult["DECLINATION_OBJ"] = new \Mcart\BasketSymmetron\SymmetronDeclination($arWords);