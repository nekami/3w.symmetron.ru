<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
    return;

function sortPropLinkElem($a, $b){
    return intval($a[1]) > intval($b[1]);
}

if(!empty($arParams["CODE_PROP_LINC_ELEM"]))
    $arParams["CODE_PROP_LINC_ELEM"] = trim($arParams["CODE_PROP_LINC_ELEM"]);
else
    $arParams["CODE_PROP_LINC_ELEM"] = "ELEM_BRAND";

$linkElem = $arParams["CODE_PROP_LINC_ELEM"];

$arItems = array();
foreach ($arResult["ITEMS"] as $key => $arItem){

    $arItem["DISPLAY_PROPERTIES"][$linkElem]["ARRAY_SORT"] =
        array_map(null, $arItem["DISPLAY_PROPERTIES"][$linkElem]["VALUE"],
            $arItem["DISPLAY_PROPERTIES"][$linkElem]["DESCRIPTION"]);

    usort($arItem["DISPLAY_PROPERTIES"][$linkElem]["ARRAY_SORT"], "sortPropLinkElem");

    $arItems[$arItem["IBLOCK_SECTION_ID"]][] = $arItem;
}


//get sections
$arSections = array();
if(!empty($arParams["IBLOCK_ID"])) {
    $rsSections = Bitrix\Iblock\SectionTable::getList(
        array(
            "filter" => array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"),
            "select" => array("CODE", "NAME", "ID"),
            "order" => array("SORT" => "asc")
        )
    );

    while ($arSection = $rsSections->Fetch()) {
        if(!empty($arItems[$arSection["ID"]])) {
            $arSection["LINES"] = $arItems[$arSection["ID"]];
            $arSections[] = $arSection;
        }

    }
}

$arResult["ITEMS"] = $arSections;