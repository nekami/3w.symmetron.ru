<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$prefix = "SYMMETRON_HEADER_MENU_COMPONENT_TEMPLATE_";
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$page = str_replace("#SITE_DIR#", "", $arResult["LIST_PAGE_URL"]);

$linkElem = $arParams["CODE_PROP_LINC_ELEM"];

$countItems = count($arResult["ITEMS"]);
$distribution = 0;
if($mod = $countItems % 3)
    $distribution = ($countItems - $mod) / 3 + 1;
else
    $distribution = $countItems / 3;
?>
<a href="<?=$page?>"><?=GetMessage($prefix . "MANUF_MENU_NAME");?></a>
<div class="inner-menu">
    <div class="wrapper">
        <div class="brands-menu">
        <?$counter = 0;?>
        <?
        $lastElem = end($arResult["ITEMS"])["ID"];
        ?>
        <?foreach ($arResult["ITEMS"] as $arItem):?>
            <?if($counter == 0):?>
                <ul>
            <?endif;?>

            <li class="manufacturers-menu__item">
                <a class="menu-item__name"><?=$arItem["NAME"]?></a>
                <?foreach ($arItem["LINES"] as $arLine):?>
                    <div class="manufacturers">
                        <?foreach ($arLine["DISPLAY_PROPERTIES"][$linkElem]["ARRAY_SORT"] as $arElem):?>
                            <?$arEl = $arLine["DISPLAY_PROPERTIES"][$linkElem]["LINK_ELEMENT_VALUE"][$arElem[0]];?>
                            <span class="manufacturers-item">
                                <a href="<?=$arEl["DETAIL_PAGE_URL"]?>">
                                    <?
                                        $arEl["NAME"] = trim($arEl["NAME"]);
                                        $l = strlen($arEl["NAME"]);
                                    ?>
                                    <?=$arEl["NAME"]?><?if($arEl["NAME"][$l - 1] != "."):?>.<?endif;?>
                                </a>
                            </span>
                        <?endforeach;?>
                    </div>
                <?endforeach;?>
                <a href="<?=$page . "?key=" . $arItem["CODE"];?>" class="all-manufacturers">
                <svg  data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.39 40.02"><path class="arrow-rightMan" d="M505.94,335.09l2.11-2.11-2.11-2.12h0l-17-17a3,3,0,0,0-4.24,4.24L496.57,330h-28.9a3,3,0,0,0,0,6h28.9l-11.89,11.88A3,3,0,0,0,486.8,353a3,3,0,0,0,2.12-.88l17-17Z" transform="translate(-464.67 -312.97)"/></svg>
                <span>все производители</span></a>
            </li>


            <?$counter++;
            if($counter == $distribution || $lastElem == $arItem["ID"]):
                if($lastElem == $arItem["ID"]):
                    ?>
                        <li>
                            <span class="show-all"><a href="<?=$page?>"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.39 40.02"><path class="arrow-rightMan" d="M505.94,335.09l2.11-2.11-2.11-2.12h0l-17-17a3,3,0,0,0-4.24,4.24L496.57,330h-28.9a3,3,0,0,0,0,6h28.9l-11.89,11.88A3,3,0,0,0,486.8,353a3,3,0,0,0,2.12-.88l17-17Z" transform="translate(-464.67 -312.97)"></path></svg><?=Loc::GetMessage($prefix."SHOW_ALL")?></a></span>
                        </li>
                    <?
                endif;
                $counter = 0;?>
                </ul>
            <?endif;?>
        <?endforeach;?>
        <?if ($arParams["PAGER_SHOW_ALL"] == "Y"):?>
        </div>
        <?endif;?>
    </div>
</div>
