<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?
$arResult['FILTER_LIST_URL'] = str_replace ('//','/',CComponentEngine::makePathFromTemplate($arResult["LIST_PAGE_URL"]));

if ( !empty($arParams['LINK_PROPERTIES']) ) 
{	
	foreach($arResult["ITEMS"] as $index => $arItem) 
	{	
		$arDisplayLinkProperties = [];
		
		$trigger = 0;
		
		foreach ($arParams['LINK_PROPERTIES'] as $arLinkProperty) 
		{	
			$property = $arItem["DISPLAY_PROPERTIES"][$arLinkProperty];
			
			if(empty($property['VALUE'])) continue;						
			
			if ($property['PROPERTY_TYPE'] == 'S')
			{
				if (is_array($property['DISPLAY_VALUE']))
				{
					foreach ($property['VALUE'] as $key => $value)
					{						
						$arDisplayLinkProperties[] = 
						[
							"PROPERTY" => $arLinkProperty,
							"NAME"     => $property['DISPLAY_VALUE'][$key], 
							"CODE"     => $value
						];

						++$trigger;
						if ($trigger == 5) break;
					}
				}
				else
				{
					$arDisplayLinkProperties[] = 
					[
						"PROPERTY" => $arLinkProperty,
						"NAME"     => $property['DISPLAY_VALUE'], 
						"CODE"     => is_array($property['VALUE']) ? $property['VALUE'][0] : $property['VALUE']
					];
					
					++$trigger;
					if ($trigger == 5) break;
				}
			}
			else if ($property['PROPERTY_TYPE'] == 'E') 
			{
				foreach ($property['LINK_ELEMENT_VALUE'] as $key => $value)
				{						
					$arDisplayLinkProperties[] = 
					[
						"PROPERTY" => $arLinkProperty,
						"NAME"     => $value['NAME'], 
						"CODE"     => $value['CODE']
					];

					++$trigger;
					if ($trigger == 5) break;					
				}
			}
			
			if ($arLinkProperty != "cities")
				unset($arResult["ITEMS"][$index]['DISPLAY_PROPERTIES'][$arLinkProperty]);
		}
		
		$arResult["ITEMS"][$index]['DISPLAY_LINK_PROPERTIES'] = $arDisplayLinkProperties;
	}
	
}