<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="focus-section">
    <div class="wrapper">

        <h2><?=GetMessage("BRAND_BANNER_TITLE");?></h2>
        <div class="focus-slider__wrapper">
            <div class="focus-slider">
                <?foreach ($arResult["ITEMS"] as $arItem):?>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="focus-slider__item  <?=$arItem["PROPERTIES"]["TEXT_POSITION"]["VALUE_XML_ID"];?>" >
                        <div class="focus-image" style="background-image: url(<?=$arItem["FIELDS"]["DETAIL_PICTURE"]["SRC"]?>)"></div>
                        <div class="focus-item__info" style="background-color: rgba(240, 240, 240, 0.5);">
                            <span class="focus-item__text"><?=$arItem["NAME"]?></span>
                            <span class="focus-item__more">
                                <span><?=GetMessage("BRAND_BANNER_MORE");?></span>
                                <span class="focus-arrow">&#8595;</span>
                            </span>
                        </div>
                    </a>
                <?endforeach;?>
            </div>
        </div>
    </div>
    <style>
        .focus-item__text {
            /* тут пользователь сможет менять высоту и межстрочный интервал */
            font-size: 30px;
            line-height: 42px;
        }

        @media (max-width: 600px) {
            .focus-image {
                /* менять нужно только первое значение, left, right, center */
                background-position: center center;
            }
        }
    </style>
</section>