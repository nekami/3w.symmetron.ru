<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
if (empty($arParams["TEMPLATE_CLASS"]))
    $arParams["TEMPLATE_CLASS"] = "brand-series";
?>

<? if (!empty($arResult["LINK"])) { ?>

    <div class="series">
       <?/* <h1 class="series__title"><?=GetMessage("SYMMETRON_BRAND_SERIES_TITLE");?></h1>*/?>
        <div class="series-list">
		
			<? foreach ($arResult["LINK"] as $arLink) { ?>		
		
				<div class="series__item">
				
					<a class="series__item-link" href="<?=$arResult["DETAIL_PAGE_URLS"][$arLink["SERIA_NAME"]];?>" target="_blank">
						<?=$arLink["SERIA_NAME"];?>
					</a>				
							
					<span class="series__item-name"><?=$arLink["NAME"]?></span>
					
					<? if (!empty($arResult["DECCRIPTION_SERIA"][$arLink["SERIA_NAME"]])) { ?>
						<p class="series__item-description">
							<?=$arResult["DECCRIPTION_SERIA"][$arLink["SERIA_NAME"]];?>
						</p>
					<? } ?>
				</div>
				
			<? } ?>
			
        </div>
    </div>
	
<? } ?>	


<?/*
<section class="<?=$arParams["TEMPLATE_CLASS"]?> brand__blocks">
    <div class="wrapper">
        <?if ($arParams["DISPLAY_NAME"] == "Y"):?>         
            <h2><?=$arParams["DISPLAYED_NAME"] ?: $arResult["NAME"]?></h2>
        <?endif;?>
        <div class="<?=$arParams["TEMPLATE_CLASS"]?>__blocks brand__blocks">
            <ul>
            <?foreach ($arResult["ITEMS"] as $arItem):?>
                <?
                $date = \ParseDateTime($arItem["PROPERTIES"]["DATE"]["VALUE"]);
                $timestamp = \MakeTimeStamp($arItem["PROPERTIES"]["DATE"]["VALUE"]);
                $color = strtolower($arItem["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);
                ?>

                <li class="head-wrapper">
                    <h4><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["FIELDS"]["NAME"]?></a></h4>
                </li>

            <?endforeach;?>
            </ul>
        </div>
    </div>
</section>*/?>