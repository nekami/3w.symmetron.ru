<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if ( !empty($arResult["ITEMS"]) ) {

	$arFilter = ['IBLOCK_CODE' => 'pbd', 'ACTIVE' => 'Y'];

	$arResult["DECCRIPTION_SERIA"] = [];
    $arResult["DETAIL_PAGE_URLS"] = [];

	foreach ( $arResult["ITEMS"] as $item )
	{
		if ( !empty($item["PROPERTIES"]["brands"]["VALUE"]) )
			$arFilter['PROPERTY_PRODUCERID'][] = $item["PROPERTIES"]["brands"]["VALUE"];
		
		if ( !empty($item["NAME"]) )
			$arFilter['PROPERTY_series'][] = $item["NAME"];

		if(strlen($item["~DETAIL_TEXT"]) > 150)
            $item["~DETAIL_TEXT"] = substr($item["~DETAIL_TEXT"], 0, 150) . '...';

		$arResult["DECCRIPTION_SERIA"][$item["NAME"]] = $item["~DETAIL_TEXT"];
        $arResult["DETAIL_PAGE_URLS"][$item["NAME"]] = $item["DETAIL_PAGE_URL"];
	}

			// поиск элементов каталога с текущей серией и производителем

			$resElement = CIBlockElement::GetList(
				[], 
				$arFilter, 
				false, 
				false, 
				["ID", "IBLOCK_SECTION_ID", "PROPERTY_series"]
			);

			$arSectionId = [];
				
			while ($arElement = $resElement->fetch())
			{
				if (empty($arSectionId[$arElement["IBLOCK_SECTION_ID"]]))
				{
					$arSectionId[$arElement["IBLOCK_SECTION_ID"]] = $arElement["PROPERTY_SERIES_VALUE"];
				}
			}			
			
			// запрос на название категорий	
            if(!empty($arSectionId)) {
                $resSection = CIBlockSection::GetList(
                    [],
                    ["ID" => array_keys($arSectionId), "ACTIVE" => 'Y'],
                    false,
                    ["ID", "NAME", "SECTION_PAGE_URL"],
                    false
                );

                $arResult["LINK"] = [];

                while ($arSection = $resSection->GetNext()) {
                    $arResult["LINK"][$arSection["ID"]] = $arSection;

                    $arResult["LINK"][$arSection["ID"]]["SERIA_NAME"] = $arSectionId[$arSection["ID"]];
                }
            }
			
			unset($arSectionId);
			
			$this->__component->setResultCacheKeys(array("LINK", "DECCRIPTION_SERIA"));
			
}
?>