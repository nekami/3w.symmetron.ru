<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$prefix = "SYMMETRON_HEADER_MENU_COMPONENT_TEMPLATE_";
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$page = str_replace("#SITE_DIR#", "", $arResult["LIST_PAGE_URL"]);
?>
<a href="<?=$page?>"><?=$arResult["NAME"]?></a>
<div class="inner-menu">
    <div class="wrapper">
        <div class="catalog-top__menu">
            <ul>
                <?foreach ($arResult["ITEMS"] as $arItem):?>
                <li><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></li>
                <?endforeach;?>
                <?if ($arParams["PAGER_SHOW_ALL"] == "Y"):?>
                <span class="show-all"><a href="<?=$page?>"><?=Loc::GetMessage($prefix."NEWS_ALL")?></a></span>
                <?endif;?>
            </ul>
        </div>
    </div>
</div>
