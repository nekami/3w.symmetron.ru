<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
if (empty($arParams["TEMPLATE_CLASS"]))
    $arParams["TEMPLATE_CLASS"] = "brand-news";
?>
<?if (!empty($arResult["ITEMS"])):?>
<section class="<?=$arParams["TEMPLATE_CLASS"]?> brand__blocks">
    <div class="wrapper">
        <?if ($arParams["DISPLAY_NAME"] == "Y"):?>         
            <h2><?=$arParams["DISPLAYED_NAME"] ?: $arResult["NAME"]?></h2>
        <?endif;?>
        <div class="<?=$arParams["TEMPLATE_CLASS"]?>__blocks brand__blocks">
            <?foreach ($arResult["ITEMS"] as $arItem):?>
                <?
                $date = \ParseDateTime($arItem["PROPERTIES"]["DATE"]["VALUE"]);
                $timestamp = \MakeTimeStamp($arItem["PROPERTIES"]["DATE"]["VALUE"]);
                $color = strtolower($arItem["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);
                ?>
            <div class="<?=$arParams["TEMPLATE_CLASS"]?>__block brand__block <?=$color?>"  style="background-image: url('<?=$arItem["FIELDS"]["DETAIL_PICTURE"]["SRC"]?>')">
                <div class="date-wrapper">
                    <div class="num"><?=$date["DD"]?></div>
                    <div class="m-y">
                        <span><?=\FormatDate("F", $timestamp)?></span>
                        <span><?=$date["YYYY"]?></span>
                    </div>
                </div>
                <div class="head-wrapper">
                    <h3><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["FIELDS"]["NAME"]?></a></h3>
                </div>
                <div class="content-wrapper">
                    <p><?=$arItem["FIELDS"]["PREVIEW_TEXT"]?></p>
                </div>

                <div class="bottom-wrapper">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="detail"><?=Loc::GetMessage("SYMMETRON_BRAND_NEWS_MORE")?></a>
                </div>
            </div>
            <?endforeach;?>
        </div>
    </div>
</section>
<?endif;?>
