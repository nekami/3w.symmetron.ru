<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_SCHEME" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_DISPLAY_SCHEME"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
);
?>
