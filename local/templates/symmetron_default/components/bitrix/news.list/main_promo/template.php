<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (empty($arParams["DISPLAY_SCHEME"]))
    return;
try {
    $scheme = $arParams["DISPLAY_SCHEME"];
    $scheme = htmlspecialchars_decode( $arParams["DISPLAY_SCHEME"]);
    $scheme = \Bitrix\Main\Web\Json::decode($scheme);
} catch (Exception $e){
    echo "Incorrect json format of scheme array!<br>";
    echo $e->getMessage();
    return;
}
function __displayLevel($arItems, $schemeLevel, $class)
{
    /* Item... */
    if (!is_array($schemeLevel))
    {
        $id = intval($schemeLevel);
        if (isset($arItems[$id])):?>
        <?
            //урезаем текст для 2 и 3 элемента массива (два нижних маленьких кубика)

            if($id >= 2){
                //echo "ffff";
                //$arItems[$id]["FIELDS"]["NAME"] = $arItems[$id]["FIELDS"]["NAME"].$arItems[$id]["FIELDS"]["NAME"];

                if(strlen($arItems[$id]["FIELDS"]["NAME"]) > 120){

                    $arItems[$id]["FIELDS"]["NAME"] = mb_strimwidth($arItems[$id]["FIELDS"]["NAME"], 0, 115, "");
                    $tmpName = explode(" ", $arItems[$id]["FIELDS"]["NAME"]);

                    //if(trim($tmpName[count($tmpName) - 1])){
                    //    unset($tmpName[count($tmpName) - 1]);
                    //}

                    unset($tmpName[count($tmpName) - 1]);

                    $arItems[$id]["FIELDS"]["NAME"] = implode(" ", $tmpName)."...";
                    $arItems[$id]["FIELDS"]["PREVIEW_TEXT"] = "";
                }


                if(strlen($arItems[$id]["FIELDS"]["NAME"]) <= 120 && strlen($arItems[$id]["FIELDS"]["NAME"]) >= 115){
                    $arItems[$id]["FIELDS"]["PREVIEW_TEXT"] = "";
                }

                if(strlen($arItems[$id]["FIELDS"]["NAME"]) < 115){


                    $sizeDet = intval((120 - strlen($arItems[$id]["FIELDS"]["NAME"])) * 2);

                    //if($sizeDet > 60){
                    //    $sizeDet = 60;
                    //}

                    /*if($sizeDet < strlen($arItems[$id]["FIELDS"]["PREVIEW_TEXT"])) {
                        $arItems[$id]["FIELDS"]["PREVIEW_TEXT"] = mb_strimwidth($arItems[$id]["FIELDS"]["PREVIEW_TEXT"], 0, $sizeDet, "");
                        $tmpDet = explode(" ", $arItems[$id]["FIELDS"]["PREVIEW_TEXT"]);
                        if (count($tmpDet) > 1) {
                            //if(trim($tmpDet[count($tmpDet) - 1])){
                            //    unset($tmpDet[count($tmpDet) - 1]);
                            //}
                            unset($tmpDet[count($tmpDet) - 1]);
                            $arItems[$id]["FIELDS"]["PREVIEW_TEXT"] = implode(" ", $tmpDet) . "...";
                        } else {
                            $arItems[$id]["FIELDS"]["PREVIEW_TEXT"] = "";
                        }
                    }*/

                }

                //if(strlen($arItems[$id]["FIELDS"]["NAME"]) > 30 && strlen($arItems[$id]["FIELDS"]["PREVIEW_TEXT"]) > 50){
                    //echo "gggg";
                    //$arItems[$id]["FIELDS"]["PREVIEW_TEXT"] = mb_strimwidth($arItems[$id]["FIELDS"]["PREVIEW_TEXT"], 0, 20, "...");
                //}
                //$arItems[$id] = $arItems[$id]["NAME"];
            }

            $date = \ParseDateTime($arItems[$id]["PROPERTIES"]["DATE"]["VALUE"]);
            $timestamp = \MakeTimeStamp($arItems[$id]["PROPERTIES"]["DATE"]["VALUE"]);
            $color = strtolower($arItems[$id]["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);
            $linkFromParam = $arItems[$id]["DISPLAY_PROPERTIES"]["URL"]["VALUE"];
            $text2 = $arItems[$id]["FIELDS"]["PREVIEW_TEXT"];


            $nameText = preg_match('/<!--HEAD-->(.*?)<!--HEAD-->/si', $text2, $matches);
            $previewText = preg_match('/<!--CONTENT-->(.*?)<!--CONTENT-->/si', $text2, $description);
            ?>
            <a href="<?=$linkFromParam?:$arItems[$id]["DETAIL_PAGE_URL"]?>">
            <div class="<?=$class." ".$color?>" style="background-image: url('<?=$arItems[$id]["FIELDS"]["DETAIL_PICTURE"]["SRC"]?>')">
                <div class="date-wrapper">
                    <div class="num"><?=$date["DD"]?></div>
                    <div class="m-y">
                        <span><?=\FormatDate("F", $timestamp)?></span>
                        <span><?=$date["YYYY"]?></span>
                    </div>
                </div>
                <div class="head-wrapper">
                    <?if($nameText != '' && $previewText != ''):?>
                    <h3><?echo $matches[1];?></h3>
                    <?else:?>
                        <h3><?echo $arItems[$id]["FIELDS"]["NAME"]?></h3>
                    <?endif;?>
                    <?/*
                    <h3><a href="<?=$arItems[$id]["PROPERTIES"]["URL"]["VALUE"]?>"><?=$arItems[$id]["FIELDS"]["NAME"]?></a></h3>
                    */?>
                </div>
                <div class="content-wrapper">
                    <?if($previewText != '' && $nameText != ''):?>
                        <p><?echo $description[0];?></p>
                    <?else:?>
                        <p><?echo $arItems[$id]["FIELDS"]["PREVIEW_TEXT"];?></p>
                    <?endif;?>
                </div>
                <?/*if (!empty($arItems[$id]["FIELDS"]["TAGS"])):?>
                <div class="bottom-wrapper">
                    <div class="tags">
                        <?$tags = explode(",", $arItems[$id]["FIELDS"]["TAGS"])?>
                        <?foreach ($tags as $tag):?>
                            <a href=""><?=trim($tag)?></a>
                        <?endforeach;?>
                    </div>
                </div>
                <?endif;*/?>
                <?if (!empty($arItems[$id]["DISPLAY_PROPERTIES"]["TAGS"]["DISPLAY_VALUE"])):?>

                <div class="bottom-wrapper">
                    <div class="tags">
                        <?foreach ($arItems[$id]["DISPLAY_PROPERTIES"]["TAGS"]["DISPLAY_VALUE"] as $key => $tag):?>
                            <a href="/news/<?="?PROPERTY_TAGS[0]=".$arItems[$id]["DISPLAY_PROPERTIES"]["TAGS"]["VALUE_ENUM_ID"][$key];?>"><?=$tag?></a>
                        <?endforeach;?>
                    </div>
                </div>
                <?endif;?>
            </div>
            </a>
        <?endif;
        return;
    }
    if (!isset($schemeLevel["TYPE"]))
        return;
    if ($schemeLevel["TYPE"] == "COL")
    {
        foreach (array("ONE", "TWO") as $key) {
            ?>
            <div class="col-6"><?
            if (isset($schemeLevel[$key])) {
                if (is_array($schemeLevel[$key])):?>
                    <div class="sub-info">
                        <?__displayLevel($arItems, $schemeLevel[$key], "sub-info__item");?>
                    </div>
                <?else:
                    if($schemeLevel[$key] == 2 || $schemeLevel[$key] == 3) {
                        __displayLevel($arItems, $schemeLevel[$key], "sub-info__item");
                    } else {
                        __displayLevel($arItems, $schemeLevel[$key], "main-info");
                    }
                endif;
            }
            ?></div><?
        }
        return;
    }
    if ($schemeLevel["TYPE"] == "ROW")
    {
        unset($schemeLevel["TYPE"]);
        foreach ($schemeLevel as $level):?>

            <div class="row">
                <?if (!is_array($level)):?>
                    <div class="col-12">
                        <?__displayLevel($arItems, $level, "sub-info__item");?>
                    </div>
                <?else:
                    __displayLevel($arItems, $level, "main-info");
                endif;?>
            </div>
        <?endforeach;
    }
}
?>
<section class="info-blocks">
    <div class="wrapper">
        <?__displayLevel($arResult["ITEMS"], $scheme, "sub-info__item")?>
    </div>
</section>
