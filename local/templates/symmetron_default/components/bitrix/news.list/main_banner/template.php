<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
/*$colorTheme = $arItem["PROPERTIES"]["COLOR"]["VALUE_XML_ID"];
$colorTheme = strtolower($colorTheme);*/
?>
<section class="slider">
    <div class="wrapper">
        <div id="slider" >
            <ul class="slider-wrap">
                <?foreach ($arResult["ITEMS"] as $arItem):?>
                    <?
                    $linkFromProp = $arItem["DISPLAY_PROPERTIES"]["URL"]["VALUE"];
                    ?>
                    <li class="slide">
                        <a href="<?=$linkFromProp ?: $arItem["DETAIL_PAGE_URL"]?>">
                        <?/*
                        <a href="<?=$arItem["PROPERTIES"]["URL"]["VALUE"]?>">
                        */?>
                            <?if(!empty($arItem["DETAIL_TEXT"])):?>
                                <div class="slide-block">
                                    <?$color = strtolower($arItem["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);?>
                                    <div class="slide-content <?=$color;?>"><?=$arItem["DETAIL_TEXT"]?></div>
                                </div>
                            <?endif;?>
                            <?if($arItem["TAGS"]):?>
                                <?$arItem["TAGS"] = explode(", ", $arItem["TAGS"]);?>
                                <div class="slide-footer">
                                    <?foreach ($arItem["TAGS"] as $tag):?>
                                        <span class="slide-tag"><?=$tag;?></span>
                                    <?endforeach;?>
                                </div>
                            <?endif;?>
                            <img src="<?=$arItem["FIELDS"]["DETAIL_PICTURE"]["SRC"]?>" alt="">
                        </a>
                    </li>
                <?endforeach;?>
            </ul>
        </div>
    </div>
</section>