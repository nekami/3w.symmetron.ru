    let docs = document.querySelector('.documents')
    if(docs) {
        let docsItems = document.querySelectorAll('.docs-item')
        let buttonShow = document.querySelector('.documents-head-show')
        buttonShow.addEventListener('click', (e) => {
            if(buttonShow.classList.contains('documents-head-show--open') == false) {
                docsItems.forEach((item) => {
                    let docsList = item.querySelector('.document-list')
                    if (docsList.classList.contains('document-list--open') == false) {
                        docsList.classList.add('document-list--open')
                    }
                })
                e.target.innerHTML = 'свернуть все'
                buttonShow.classList.add('documents-head-show--open')
                docsItems.forEach((item) => {
                    item.querySelector('.document-control').classList.add('document-control--open')
                })
            } else {
                docsItems.forEach((item) => {
                    let docsList = item.querySelector('.document-list')
                    if (docsList.classList.contains('document-list--open')) {
                        docsList.classList.remove('document-list--open')
                    }
                })
                e.target.innerHTML = 'раскрыть все'
                buttonShow.classList.remove('documents-head-show--open')
                docsItems.forEach((item) => {
                    item.querySelector('.document-control').classList.remove('document-control--open')
                })
            }
        })

        docs.addEventListener('click', (e) => {
            docsItems.forEach((element) => {
                let openListButton = element.querySelector('.document-control')
                let documentList = element.querySelector('.document-list')
                if(e.target == openListButton) {
                    if(openListButton.classList.contains('document-control--open')) {
                        openListButton.classList.remove('document-control--open')
                        documentList.classList.remove('document-list--open')
                    } else {
                        openListButton.classList.add('document-control--open')
                        documentList.classList.add('document-list--open')
                    }
                }
            })
        })
    }

    let filter =document.querySelector('.filter.filter-news')
    if(filter) {
        let filterElements = filter.querySelectorAll('.filter-element');
        filter.addEventListener('click', (e) => {
            filterElements.forEach((item) => {
                let openList = item.querySelector('.filter-element-head__button')
                let openInput = item.querySelector('.filter-element-search')
                let checkBoxList = item.querySelector('.filter-checkbox-list')
                let closeSelect = item.querySelector('.filter-element-head__close')
                if(e.target == openList) {
                    if(openList.classList.contains('filter-element-head__button--close')) {
                        openInput.style.display = 'none'
                        checkBoxList.style.display = 'none'
                        openList.classList.remove('filter-element-head__button--close')
                    } else {
                        openInput.style.display = 'block'
                        checkBoxList.style.display = 'block'
                        openList.classList.add('filter-element-head__button--close')
                    }
                }

                if(e.target == closeSelect) {
                    item.querySelector('.filter-element-head__number').style.display = 'none'
                    closeSelect.style.display = 'none'
                }
            })
            let closeFilterList = document.querySelector('.filter-head__button')
            let filterWrapper = filter.querySelector('.filter-wrapper')
            if(e.target == closeFilterList) {
                if(closeFilterList.classList.contains('filter-head__button--open')) {
                    filterWrapper.style.display = 'none'
                    closeFilterList.classList.remove('filter-head__button--open')
                    filter.style.paddingBottom = '0px'
                } else {
                    filterWrapper.style.display = 'block'
                    closeFilterList.classList.add('filter-head__button--open')
                    filter.style.paddingBottom = '15px'
                }
            }
        })
    }

    let filterArticle = document.querySelector('.filter.filter-article')
    if(filterArticle) {
        let filterElements = filterArticle.querySelectorAll('.filter-element');
        filterArticle.addEventListener('click', (e) => {
            filterElements.forEach((item) => {
                let openList = item.querySelector('.filter-element-head__button')
                let openInput = item.querySelector('.filter-element-search')
                let checkBoxList = item.querySelector('.filter-checkbox-list')
                let closeSelect = item.querySelector('.filter-element-head__close')
                if(e.target == openList) {
                    if(openList.classList.contains('filter-element-head__button--close')) {
                        openInput.style.display = 'none'
                        checkBoxList.style.display = 'none'
                        openList.classList.remove('filter-element-head__button--close')
                    } else {
                        openInput.style.display = 'block'
                        checkBoxList.style.display = 'block'
                        openList.classList.add('filter-element-head__button--close')
                    }
                }

                if(e.target == closeSelect) {
                    item.querySelector('.filter-element-head__number').style.display = 'none'
                    closeSelect.style.display = 'none'
                }
            })
            let closeFilterList = filterArticle.querySelector('.filter-head__button')
            let filterWrapper = filterArticle.querySelector('.filter-wrapper')
            if(e.target == closeFilterList) {
                if(closeFilterList.classList.contains('filter-head__button--open')) {
                    filterWrapper.style.display = 'none'
                    closeFilterList.classList.remove('filter-head__button--open')
                    filterArticle.style.paddingBottom = '0px'
                } else {
                    filterWrapper.style.display = 'block'
                    closeFilterList.classList.add('filter-head__button--open')
                    filterArticle.style.paddingBottom = '15px'
                }
            }
        })
    }