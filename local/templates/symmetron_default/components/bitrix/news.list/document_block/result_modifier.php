<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<? $arNewResult = [];

foreach ($arResult["ITEMS"] as $arItem)
{
	if (!empty($arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE_XML_ID"]))
	{		
		$arNewResult[$arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE_XML_ID"]]["DOCUMENTS"][$arItem["ID"]] = $arItem;
		
		$arNewResult[$arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE_XML_ID"]]["NAME"] = $arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE"];
	}
}

$arResult["ITEMS"] = $arNewResult; ?>