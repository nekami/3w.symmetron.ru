<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<? if ( !empty($arResult["ITEMS"]) ) { ?>

<div class="documents">
    <div class="documents-head">
        <h2 class="documents__title"><?=getMessage('DOCUMENTATION');?></h2>
        <span class="documents-head-show"><?=getMessage('SHOW_ALL');?></span>
    </div>

    <div class="docs-wrapper">
	
	<? foreach($arResult["ITEMS"] as $arItem) { ?>
	
		<div class="docs-item">
            <div class="docs-item__head">
                <span class="document-control"></span>
                <p class="docs-item__head-name"><?=$arItem["NAME"]?></p>
            </div>
            <div class="document-list">		
			
			<? foreach($arItem["DOCUMENTS"] as $arDocument) { ?>
			
			<?
			$this->AddEditAction($arDocument['ID'], $arDocument['EDIT_LINK'], CIBlock::GetArrayByID($arDocument["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arDocument['ID'], $arDocument['DELETE_LINK'], CIBlock::GetArrayByID($arDocument["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>			
                
				<div class="document-list-element" id="<?=$this->GetEditAreaId($arDocument['ID']);?>">
					
					<a href='<?=$arDocument["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>' download>
					
						<? switch ($arDocument["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["CONTENT_TYPE"]) {
						
							case "application/pdf": echo '<img src="'.$this->GetFolder().'/images/file-pdf.svg">'; break;
							
							case "application/octet-stream": echo '<img src="'.$this->GetFolder().'/images/file-db.svg">'; break;
							
							case "application/msword": echo '<img src="'.$this->GetFolder().'/images/file-doc.svg">'; break;
						
							case "text/html": echo '<img src="'.$this->GetFolder().'/images/file-html.svg">'; break;
							
							case "image/jpeg": echo '<img src="'.$this->GetFolder().'/images/file-jpg.svg">'; break;
							
							case "application/vnd.ms-powerpoint": echo '<img src="'.$this->GetFolder().'/images/file-ppt.svg">'; break;
							
							case "text/plain": echo '<img src="'.$this->GetFolder().'/images/file-blank.svg">'; break;
							
							case "application/vnd.ms-excel": echo '<img src="'.$this->GetFolder().'/images/file-xls.svg">'; break;
							
							case "text/xml": echo '<img src="'.$this->GetFolder().'/images/file-xml.svg">'; break;
							
							case "application/x-zip-compressed": echo '<img src="'.$this->GetFolder().'/images/file-zip.svg">'; break;
						
						} ?>
						
                    </a>					
					
                    <div class="document-list-element-wrapper">
						<div class="document-element">	
							<a href='<?=$arDocument["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>' class="document-element__name" download>
								<?=$arDocument["NAME"]?>
							</a>						
							
							<? if (!empty($arDocument["DISPLAY_PROPERTIES"]["LANG"]["VALUE"])) { ?>
								<span class="document-element__lang">
									<?=$arDocument["DISPLAY_PROPERTIES"]["LANG"]["VALUE"]?>
								</span>
							<? } ?>
						</div>
						
						<div class="document-char">
							<? if (!empty($arDocument["DISPLAY_PROPERTIES"]["DATE"]["VALUE"])) { ?>
								<span class="document-element__date">
									<?=$arDocument["DISPLAY_PROPERTIES"]["DATE"]["VALUE"]?>
								</span>
							<? } ?>
							
							<? if (!empty($arDocument["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["CONTENT_TYPE"])) { ?>
								<span class="document-element__format">
									<?=str_replace("application/", "", $arDocument["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["CONTENT_TYPE"]);?>
								</span>
							<? } ?>	
								
							<span class="document-element__weight">
								<?=CFile::FormatSize($arDocument["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["FILE_SIZE"]);?>
							</span>
						</div>
                    </div>
                </div>	
				
			<? } ?>	
			
            </div>
        </div>
	<? } ?>
    </div>
</div>

<? } ?>