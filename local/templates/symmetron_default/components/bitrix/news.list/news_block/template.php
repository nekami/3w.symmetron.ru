<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<? if ( !empty($arResult["ITEMS"]) ) { ?>

<div class="news">

    <h2 class="news__title">
		<?=$arResult["NAME"]?> 
		<? if ( $arParams['DISPLAY_NAME_SERIES'] == "Y" ) 
			echo $arParams['SERIES']['NAME']; ?>
	</h2>
	
	<? foreach($arResult["ITEMS"] as $arItem) { ?>
        
		<?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<div class="news-element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		
			<? if ($arResult["CODE"] == "events") { // для статей ?>
				<ul class="articles-element-head">
					<li class="articles-element-head__date">
						<?=FormatDate("j F Y", MakeTimeStamp($arItem["DATE_CREATE"]));?>
					</li>				
					
					<? if (!empty($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])) { ?>
					
					<li class="articles-element-head__author">
						
						<?=getMessage('AVTOR');?>
						
						<? if (is_array($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])) { ?>
						
							<? for ($i=0; $i<count($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']); $i++) { ?>
							
								<a href="/events/filter/author-is-<?=strtolower($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'][$i])?>/apply/">
								
									<?=$arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'][$i]?>
									
								</a>

								<? if ( $i != (count($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']) - 1) )
									echo "|"; ?>
							<? } ?>
							
						<? } else echo "<a href='/events/filter/author-is-".strtolower($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])."/apply/'>".$arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']."</a>"; ?>
						
					</li>
					<? } ?>
					
					<? if (!empty($arItem['DISPLAY_PROPERTIES']['READING_TIME']['DISPLAY_VALUE'])) { ?>
						<li class="article-element-head__duration">
							<?=getMessage('PRIBLIZITELNOE-VREMYA-CHTENIYA');?>
							<?=$arItem['DISPLAY_PROPERTIES']['READING_TIME']['VALUE'];?>
							<?=uni_declension($arItem['DISPLAY_PROPERTIES']['READING_TIME']['VALUE'], getMessage('MINUTA')); ?>
						</li>
					<? } ?>	
				</ul>
			<? } else { // для новостей ?>
				<span class="news-element__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>				
			<? } ?>
			
			<? if ($arResult["CODE"] == "events") { // для статей ?>
				<h2 class="articles-element__title">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem["NAME"]?></a>
				</h2>
			<? } else { // для новостей ?>
				<h3 class="news-elements__title">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem["NAME"]?></a>
				</h3>
			<? } ?>			
			
			<? if ( !empty($arItem['PREVIEW_TEXT']) ) { ?>
				<p class="news-elements__text"><?=$arItem["PREVIEW_TEXT"]?></p>
			<? } ?>
			
			
			<? if ( !empty($arItem['DISPLAY_LINK_PROPERTIES']) ) { ?>
				<div class="cloud-tags">
					<? foreach ($arItem['DISPLAY_LINK_PROPERTIES'] as $propertyCode => $displayPropertyValue) { ?>
				
						<a href="<?=$arResult["FILTER_LIST_URL"]?>filter/<?=$displayPropertyValue["PROPERTY"]?>-is-<?=strtolower($displayPropertyValue["CODE"]);?>/apply/" class="cloud-tags__link">
						
							<?=$displayPropertyValue["NAME"]?>
							
						</a>
				
					<? } ?>
				</div>
			<? } ?>
		</div>	
	<? } ?>
	
	<? if (!empty($arParams['SERIES'])) { ?>	
		<a class="show-more-news" href="<?=$arResult["FILTER_LIST_URL"]?>filter/series-is-<?=$arParams['SERIES']['CODE'];?>/apply/">	
			<?=getMessage('SWOW_ALL_TITLE');?> <?=strtolower($arResult["NAME"]);?> <?=$arParams['SERIES']['NAME'];?>
		</a>
	<? } ?>	
</div>

<? } ?>