<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<section class="additional-info">
    <div class="wrapper">
		<h2><?=Loc::GetMessage("SYMMETRON_BRAND_ADDIT_VIDEO_TITLE")?></h2>
        <div class="additional">
            <div class="video-block">
                <iframe src="<?=$arResult["PROPERTIES"]["VIDEO_URL"]["VALUE"]?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="text-block">
                <?=$arResult["DETAIL_TEXT"]?>
            </div>
        </div>
    </div>
</section>
