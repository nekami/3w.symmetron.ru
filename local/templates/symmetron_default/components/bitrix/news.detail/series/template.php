<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<h1 class="title-page"><?=$arResult["NAME"]?></h1>

<? if(strlen($arResult["DETAIL_TEXT"])>0) { ?>
	<p class="page-text"><?=$arResult["DETAIL_TEXT"];?></p>
<? } else { ?>
	<p class="page-text"><?=$arResult["PREVIEW_TEXT"];?></p>
<? } ?>