<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (empty($arResult["EMAIL"]) && $USER->IsAuthorized())
    $arResult["EMAIL"] = $USER->GetEmail();
?>
<section class="subscribe">
    <div class="wrapper">
        <div class="subscribe-header">
             <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 358.08"><path d="M504,384.8H494.9V102.08A25.15,25.15,0,0,0,469.78,77H42.22A25.15,25.15,0,0,0,17.1,102.08V384.8H8a8,8,0,0,0-8,8v17.1A25.15,25.15,0,0,0,25.12,435H486.88A25.15,25.15,0,0,0,512,409.92v-17.1A8,8,0,0,0,504,384.8ZM33.14,102.08A9.1,9.1,0,0,1,42.22,93H469.78a9.1,9.1,0,0,1,9.09,9.09V384.8H33.14ZM496,409.92a9.1,9.1,0,0,1-9.09,9.09H25.12A9.1,9.1,0,0,1,16,409.92v-9.09H222.36c0,.18,0,.35,0,.53a8,8,0,0,0,8,8h51.31a8,8,0,0,0,8-8c0-.18,0-.36,0-.53H496Z" transform="translate(0 -76.96)"/><path d="M452.68,111.16H59.32a8,8,0,0,0-8,8V358.61a8,8,0,0,0,8,8H452.68a8,8,0,0,0,8-8V119.18A8,8,0,0,0,452.68,111.16Zm-8,239.43H67.34V127.2H444.66Z" transform="translate(0 -76.96)"/><path d="M298.76,170H409.92a8,8,0,1,0,0-16H298.76a8,8,0,1,0,0,16Z" transform="translate(0 -76.96)"/><path d="M298.76,204.16H409.92a8,8,0,1,0,0-16H298.76a8,8,0,0,0,0,16Z" transform="translate(0 -76.96)"/><path d="M144.84,195.61h68.41a8,8,0,0,0,0-16H144.84a8,8,0,1,0,0,16Z" transform="translate(0 -76.96)"/><path d="M204.69,213.78H153.39a8,8,0,1,0,0,16h51.31a8,8,0,0,0,0-16Z" transform="translate(0 -76.96)"/><path d="M298.76,246.92h17.1a8,8,0,0,0,0-16h-17.1a8,8,0,1,0,0,16Z" transform="translate(0 -76.96)"/><path d="M298.76,281.12h17.1a8,8,0,0,0,0-16h-17.1a8,8,0,0,0,0,16Z" transform="translate(0 -76.96)"/><path d="M298.76,315.32h17.1a8,8,0,0,0,0-16h-17.1a8,8,0,0,0,0,16Z" transform="translate(0 -76.96)"/><path d="M409.92,230.88H350.06a8,8,0,0,0,0,16h59.86a8,8,0,1,0,0-16Z" transform="translate(0 -76.96)"/><path d="M409.92,265.08H350.06a8,8,0,0,0,0,16h59.86a8,8,0,1,0,0-16Z" transform="translate(0 -76.96)"/><path d="M409.92,299.29H350.06a8,8,0,0,0,0,16h59.86a8,8,0,1,0,0-16Z" transform="translate(0 -76.96)"/><path d="M102.08,332.43H256a16.59,16.59,0,0,0,16.57-16.57V213.24A16.59,16.59,0,0,0,256,196.68h-.53V161.94a16.59,16.59,0,0,0-16.57-16.57H119.18a16.59,16.59,0,0,0-16.57,16.57v34.74h-.53a16.59,16.59,0,0,0-16.57,16.57V315.86A16.59,16.59,0,0,0,102.08,332.43Zm46.23-67.87-46.76,42.51V222ZM256.53,222v85l-46.77-42.51ZM243,316.39H115.12l52.06-47.32a17.57,17.57,0,0,1,23.73,0ZM118.65,161.94a.53.53,0,0,1,.53-.53H238.9a.53.53,0,0,1,.53.53v54L197.55,254a33.48,33.48,0,0,0-37,0l-41.88-38.07Z" transform="translate(0 -76.96)"/></svg>
            <?=GetMessage("mcart_symmetron_subscr_title");?>
        </div>
        <div class="subscribe-content">
            <form action="<?=$arResult["FORM_ACTION"]?>">
                <?=bitrix_sessid_post()?>
                <input type="hidden" name="sender_subscription" value="add">
                <input type="email" name="sf_EMAIL" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>">
                <div style="display: none;">
                    <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
                        <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" checked>
                    <?endforeach;?>
                </div>

                <button type="submit"><?=GetMessage("subscr_form_button")?></button>
            </form>
        </div>
    </div>
</section>
