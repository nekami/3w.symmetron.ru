<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//echo "<pre>";
//print_r($arResult);
//print_r($arParams["PROP_L"]);
//echo "</pre>";

if(!empty($arParams["PROP_L"])){
    $arResult["PROP_L"] = array();
    foreach ($arParams["PROP_L"] as $prop_l){

        if(!empty($prop_l) && !empty(trim($arResult["PROPERTIES"][$prop_l]["VALUE"]))){
            $arResult["PROP_L"][$prop_l]["NAME"] = $arResult["PROPERTIES"][$prop_l]["NAME"];
            $arResult["PROP_L"][$prop_l]["VALUE"] = $arResult["PROPERTIES"][$prop_l]["VALUE"];
        }


    }

}



if(!empty($arParams["PROP_R"])){
    $arResult["PROP_R"] = array();
    foreach ($arParams["PROP_R"] as $prop_r){

        if(!empty($prop_r) && !empty($arResult["PROPERTIES"][$prop_r]["VALUE"])){
            $arResult["PROP_R"][$prop_r]["NAME"] = $arResult["PROPERTIES"][$prop_r]["NAME"];
            $arResult["PROP_R"][$prop_r]["VALUE"] = $arResult["PROPERTIES"][$prop_r]["VALUE"];
        }


    }

}