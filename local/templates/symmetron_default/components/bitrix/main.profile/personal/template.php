<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="personal-form">
	<?=ShowError($arResult["strProfileError"]);?>
	<?
	if ($arResult['DATA_SAVED'] == 'Y')
		echo ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
	?>
	<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="ru" />
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('NAME')?></label>
			<input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
		</div>
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('LAST_NAME')?></label>
			<input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
		</div>
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('SECOND_NAME')?></label>
			<input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
		</div>
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('EMAIL')?><?if($arResult["EMAIL_REQUIRED"]):?><span class="starrequired">*</span><?endif?></label>
			<input type="email" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" <?= $arResult["arUser"]["EMAIL"] ? 'readonly':''?>/>
		</div>
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('PHONE')?></label>
			<input type="text" name="PERSONAL_PHONE" maxlength="20" id="typePhone" value="<? echo $arResult["arUser"]["PERSONAL_PHONE"]?>" />
		</div>

		<?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>
		<div class="pass-notification"><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></div>
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('NEW_PASSWORD_REQ')?></label>
			<input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input" />
		</div>
		<div class="personal-form__data">
			<label for=""><?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?></label>
			<input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
		</div>
		<?endif?>
		<div class="personal-form__submit">
      <a href="/personal/" class="default-button cancel"><?=Loc::getMessage("CANCEL")?></a>
			<!-- <input type="reset" class="default-button cancel" value="<?=Loc::getMessage("MAIN_RESET")?>"> -->
			<input type="submit" name="save" class="default-button personal-submit" value="<?=(($arResult["ID"]>0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD"))?>">
		</div>
	</form>
</div>