<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(false);

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

if (!\Bitrix\Main\Loader::includeModule("iblock"))
    return;

/* If we output sections list */
$isEndpoint = false;
$arFilter = Array(
    "ACTIVE" => "Y",
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
);
/*if (!empty($arResult["VARIABLES"]["SECTION_CODE"])) {
    $arFilter["CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
}*/
if (!empty($arResult["VARIABLES"]["SECTION_ID"])) {
    $arFilter["SECTION_ID"] = $arResult["VARIABLES"]["SECTION_ID"];
}
if (count($arFilter) > 2)
{
    $subSecCount = \CIBlockSection::GetCount($arFilter);
    $isEndpoint = $subSecCount == 0;
}
if (!$isEndpoint) {
    $this->getComponent()->includeComponentTemplate("sections");
} else {
    $this->getComponent()->includeComponentTemplate("section_horizontal");
}