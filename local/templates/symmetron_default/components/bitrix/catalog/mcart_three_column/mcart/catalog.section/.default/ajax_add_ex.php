<?php

define('STOP_STATISTICS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$GLOBALS['APPLICATION']->RestartBuffer();

//$hd = fopen(__DIR__ . "/log_order_parce_" . time() . ".txt", "a");
//fwrite($hd, print_r($_GET, 1));
//fclose($hd);
/*if (CModule::IncludeModule("catalog")) {
    if (!empty($_GET)) {
        $res = Add2BasketByProductID($_GET["id"], $_GET["quant"], array(), array(
            array("NAME" => "Образец", "CODE" => "EXAMPLE", "VALUE" => "Y"),
        ));
    }
}*/


if (!empty($_GET)) {
    $arFields = array(
        "PRODUCT_ID" => $_GET["id"],
        "PRICE" => 0,
        "CURRENCY" => "RUB",
        "QUANTITY" => $_GET["quant"],
        "LID" => LANG,
        "DELAY" => "N",
        "CAN_BUY" => "Y",
        "NAME" => $_GET["name"],
        "DETAIL_PAGE_URL" => $_GET["url"]
    );

    if($_GET["example"] == "Y") {
        $arProps = array(
            array("NAME" => "Образец", "CODE" => "EXAMPLE", "VALUE" => "Y")
        );
        $arFields["PROPS"] = $arProps;
    }

    $res = CSaleBasket::Add($arFields);
    echo $res;
}

die;