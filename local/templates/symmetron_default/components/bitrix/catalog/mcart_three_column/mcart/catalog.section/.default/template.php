<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

$arPsositionVariants = array(
    "1" => GetMessage("SBB_QUANT_1"),
    "2" => GetMessage("SBB_QUANT_2"),
    "3" => GetMessage("SBB_QUANT_2"),
    "4" => GetMessage("SBB_QUANT_2"),
    "5" => GetMessage("SBB_QUANT_5"),
    "6" => GetMessage("SBB_QUANT_5"),
    "7" => GetMessage("SBB_QUANT_5"),
    "8" => GetMessage("SBB_QUANT_5"),
    "9" => GetMessage("SBB_QUANT_5"),
    "0" => GetMessage("SBB_QUANT_5"),
);

$arPsositionVariantsExaption = array(11, 12, 13, 14);


function declineWordPositionAnalog($quantVal, $arPsositionVariants, $arPsositionVariantsExaption){
    //склоняем слово позиция
    $mod = $quantVal % 10;
    if(in_array($quantVal, $arPsositionVariantsExaption))
        $posWord = GetMessage("SBB_QUANT_5");
    else
        $posWord = $arPsositionVariants[$mod];

    return $posWord;
}

global $USER;
if ( USER_ID ){
    $res = CUser::GetList($by = "id", $order = "asc", ['=ID'=>USER_ID],['SELECT'=>["UF_FAVS"]])->Fetch();
    $favs = $res["UF_FAVS"];
}

//для дополнения элементов аяксом
if(count($arResult["ITEMS"]) > 0):

    if($_POST[0] == "addelem"):?>
        <?$counterCol = 1;?>
        <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
            <?if(!empty(trim($prop)) && !empty(trim($arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"]))):?>
                <?$counterCol++;?>
            <?endif;?>
        <?endforeach;?>

        <?foreach ($arResult["ITEMS"] as $arItem):?>

            <?

            //$item = $arItem;
            //$item["CAN_BUY"] = true;
            $areaId = $this->GetEditAreaId($arItem['ID']);
            $itemIds = array(
                'ID' => $areaId,
                'PICT' => $areaId.'_pict',
                'SECOND_PICT' => $areaId.'_secondpict',
                'PICT_SLIDER' => $areaId.'_pict_slider',
                'STICKER_ID' => $areaId.'_sticker',
                'SECOND_STICKER_ID' => $areaId.'_secondsticker',
                'QUANTITY' => $areaId.'_quantity',
                'QUANTITY_DOWN' => $areaId.'_quant_down',
                'QUANTITY_UP' => $areaId.'_quant_up',
                'QUANTITY_MEASURE' => $areaId.'_quant_measure',
                'QUANTITY_LIMIT' => $areaId.'_quant_limit',
                'BUY_LINK' => $areaId.'_buy_link',
                'BASKET_ACTIONS' => $areaId.'_basket_actions',
                'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
                'SUBSCRIBE_LINK' => $areaId.'_subscribe',
                'COMPARE_LINK' => $areaId.'_compare_link',
                'PRICE' => $areaId.'_price',
                'PRICE_OLD' => $areaId.'_price_old',
                'PRICE_TOTAL' => $areaId.'_price_total',
                'DSC_PERC' => $areaId.'_dsc_perc',
                'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
                'PROP_DIV' => $areaId.'_sku_tree',
                'PROP' => $areaId.'_prop_',
                'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
                'BASKET_PROP_DIV' => $areaId.'_basket_prop',
            );
            ?>

            <tr class="mini-card__select" >
                <td id = "name_prod_<?=$arItem["ID"];?>"><?=$arItem["NAME"];?></td>

                <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                    <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arItem["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
                        <?continue;?>
                    <?endif;?>
                    <?if(!empty($arItem["PROPERTIES"][$prop]) && !empty(trim($arItem["PROPERTIES"][$prop]["NAME"]))):?>
                        <td><?=$arItem["PROPERTIES"][$prop]["VALUE"];?></td>
                    <?endif;?>
                <?endforeach;?>
                <!--<td><span class="analogue-status ordered">Заказано </span></td>-->
            </tr>
            <tr class="mini-card" id="<?=$itemIds["ID"];?>">
                <?//$arItem["DETAIL_PAGE_URL"];?>
                <td  colspan="<?=$counterCol;?>">
                    <div class="card-holder">
                        <div class="mini-cart__wrapper">
                            <div class="item-img">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"];?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                                <?$is_favs = empty($favs) ? false : array_search( $arItem["ID"], $favs);?>
                                <span class="favorite <?=$is_favs ? 'act':''?>"  data-id="<?=$arItem['ID']?>">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="5 1 28 35" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"/>
									<style>
										#fill-heart {
											fill: transparent;
										}
									</style>
														
									<g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""/>
									<path  id="svg_22" d="M-29.97297297297297,2.4594594594594668 " /><path  id="svg_2" d="M19.324324324324337,8.837837837837846 " />
									<path  id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"  /></g>
									
								</svg>
                                </span>
                            </div>
                            <div class="item-info">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="item-info__head" onclick="console.log('no')">
                                    <?=$arItem["NAME"];?>
                                </a>
                                <div class="item-info__content">
                                    <?if($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"]):?>
                                        <ul>
                                            <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"] as $prop):?>
                                                <?if(!empty(trim($prop["VALUE"]))):?>
                                                    <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                <?endif;?>
                                            <?endforeach;?>
                                        </ul>
                                    <?endif;?>
                                    <ul>
                                        <?if($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"]):?>
                                            <ul>
                                                <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $prop):?>
                                                    <?if(!empty(trim($prop["VALUE"]))):?>
                                                        <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                    <?endif;?>
                                                <?endforeach;?>
                                            </ul>
                                        <?endif;?>
                                    </ul>
                                </div>
                            </div>
                            <?if($arItem["CATALOG_AVAILABLE"] == "Y"):?>

                                <div class="item-more">
                                    <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                    <?if($arItem["CATALOG_QUANTITY"] > 0):?>
                                        <span class="avalibilty">В наличии <?=$arItem["CATALOG_QUANTITY"]?> <?=declineWordPositionAnalog($arItem["CATALOG_QUANTITY"], $arPsositionVariants, $arPsositionVariantsExaption)?></span>
                                    <?else:?>
                                        <span class="avalibilty out">Нет в наличии</span>
                                    <?endif;?>

                                    <?//if(!empty($arItem["ITEM_PRICES"])):?>
                                    <div class="wholesale">
                                        <?foreach ($arItem["ITEM_PRICES"] as $arPrice):?>
                                            <?if(!empty($arPrice["QUANTITY_TO"])):?>
                                                <div class="quantity">
                                                    <span class="quantity-items"><?=$arPrice["QUANTITY_FROM"]."-".$arPrice["QUANTITY_TO"]?> штук </span>
                                                    <span class="quantity-separator"></span>
                                                    <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                                </div>
                                            <?else:?>
                                                <div class="quantity">
                                                    <?if(empty($arPrice["QUANTITY_FROM"])){
                                                        $arPrice["QUANTITY_FROM"] = 1;
                                                    }?>
                                                    <span class="quantity-items">от <?=$arPrice["QUANTITY_FROM"]?> штук </span>
                                                    <span class="quantity-separator"></span>
                                                    <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                                </div>
                                            <?endif;?>
                                        <?endforeach;?>
                                    </div>
                                    <?//endif;?>

                                    <div class="quantity-num">
                                        <span class="quantity-button" id="<? echo $itemIds['QUANTITY_DOWN']; ?>" onclick="quantChange('-', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">–</span>
                                        <input value="1" max="99999" name = "quant_elems_<?=$arItem["ID"];?>" id="<? echo $itemIds['QUANTITY']; ?>" onchange="quantityVal(<?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>)" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
                                        <span class="quantity-button" id="<? echo $itemIds['QUANTITY_UP']; ?>" onclick="quantChange('+', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">+</span>
                                    </div>
                                    <form action="" onkeydown="if(event.keyCode==13){return false;}">
                                        <?if(!empty($arItem["ITEM_PRICES"])):?>
                                            <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">
                                                <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                            </div>
                                            <div class="price" id="<? echo $itemIds['PRICE_TOTAL']; ?>">
                                                <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                            </div>
                                            <div class="cb"></div>
                                            <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBasket('<?=$arItem["ID"];?>', '');" href="javascript:void(0)" rel="nofollow">Добавить в корзину</a>
                                        <?else:?>
                                            <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">0</div>
                                            <div class="price" style="display: none" id="<? echo $itemIds['PRICE_TOTAL']; ?>">0</div>
                                            <div class="cb"></div>
                                            <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'N');" href="javascript:void(0)" rel="nofollow">Запросить цену</a>
                                        <?endif;?>
                                    </form>

                                    <a <?if ($USER->IsAuthorized()):?>onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'Y');"<?else:?>onclick="noAuth();"<?endif;?> class="sidebar-sample" href="javascript:void(0)">Запросить образец</a>
                                </div>
                            <?else:?>
                                <div class="item-more">
                                    <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                    <span class="avalibilty out">Товар недоступен</span>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                </td>
            </tr>

        <?endforeach;?>
        <?echo "------------============------------";?>
        <?$arCount = $arResult["NAV_RESULT"]->NavRecordCount - $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer;?>
        <?if($arCount > 0):?>
            <?if($arCount >= $arResult["NAV_RESULT"]->NavPageSize):?>
                <?$showMore = $arResult["NAV_RESULT"]->NavPageSize?>
            <?else:?>
                <?$showMore = $arCount?>
            <?endif;?>


            <button id = "button_elem_q" class="show-all" onclick="ajaxUpdateList('<?=$_GET["sort2"];?>', '<?=$_GET["method2"]?>', 'addelem', '<?=$arResult["NAV_RESULT"]->NavPageNomer + 1?>');">Показать ещё <?=$showMore;?> из <span id="element_count"><?=$arCount;?></span></button>
        <?endif;?>
        <?=$arResult["NAV_STRING"]?>
        <?unset($_POST[0]);?>
        <?die;?>
    <?endif;?>

    <script>
        //BX.ready(function() {
        function noAuth() {
            alert("Для добавления образца нужно авторизоваться")
        }


        function addToBasket(id, baskProp) {
            var quant = document.getElementsByName("quant_elems_" + id)[0].value;
            var baskPar = {};
            baskPar["ajax_basket"] = "Y";
            if (baskProp != "")
                baskPar["basket_props"] = baskProp;
            baskPar["quantity"] = quant;
            BX.ajax.loadJSON(
                "?action=ADD2BASKET&id=" + id,
                baskPar,
                function (data) {
                    //console.log("added!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111");
                    if (data.STATUS == "OK")
                        alert(data.MESSAGE);
                    else
                        alert(data.MESSAGE);
                }
            );

        }


        var path = "<?=$templateFolder;?>";

        function addToBaskEx(prodId, pathProd, example) {
            if (document.getElementsByName("quant_elems_" + prodId)[0]) {
                var quant = document.getElementsByName("quant_elems_" + prodId)[0].value;
            } else {
                var quant = 1;
            }
            var arData = {};
            arData["id"] = prodId;
            arData["quant"] = quant;
            arData["name"] = document.getElementById("name_prod_" + prodId).innerHTML;
            arData["url"] = pathProd;
            arData["example"] = example;
            BX.ajax.get(
                path + "/ajax_add_ex.php",
                arData,
                function (data) {
                    //console.log(data);
                    if (data > 0) {
                        if (example == "Y")
                            alert("Образец добавлен в корзину");
                        else
                            alert("Товар добавлен в карзину");
                    }
                }
            );
        }

        function __getUriToReload(paramsToOff) {
            var url = window.location.search;
            var params = url.replace("?", "").split("&");
            var delimiter = "?";
            url = '';
            var off;
            for (var i = 0; i < params.length; i++) {
                off = false;
                for (var j = 0; j < paramsToOff.length; j++)
                    if (params[i].indexOf(paramsToOff[j]) >= 0) {
                        off = true;
                        break;
                    }
                if (off) continue;
                url += delimiter + params[i];
                delimiter = "&";
            }

            return url;
        }

        function ajaxUpdateList(sort, method, ajax, page_1) {
            var url = __getUriToReload(["sort2", "method2", "PAGEN_1"]);

            var flaggood = false;
            if (sort != "" && method != "") {
                if (url == "")
                    url = "?";
                else
                    url += "&";
                url += "sort2=" + sort + "&method2=" + method;
                flaggood = true;
            }

            if (page_1 != "") {
                if (url == "")
                    url = "?";
                else
                    url += "&";

                url += "PAGEN_1=" + page_1;
                flaggood = true;
            }

            if (flaggood == true) {
                //url += "&ajax=" + ajax;
                var arrAjax = [ajax];
                if (ajax == "1") {
                    console.log("dddddddddddddddddddddddddddddddddddddddddd");
                    $("#analogue > .wrapper > .section-content").load(url + " .section-content .to-reload-by-filter", {"0":"1"});
                    history.replaceState(3, "Title 2", url);
                } else if (ajax == "addelem") {
                    //console.log(url);
                    var arrAjax = [ajax];
                    BX.ajax.post(
                        url,
                        arrAjax,
                        function (data) {
                            //console.log(data);
                            data = data.split("------------============------------");
                            document.getElementById("tableLastProd").innerHTML += data[0];
                            document.getElementById("buttomsList").innerHTML = data[1];
                        }
                    );
                    history.replaceState(3, "Title 2", url);
                }
            }
        }
        //});

        BX.ready(function() {

            $(document).on('click', function (e){
                var searchSelect = $('.search-select')
                var projectSelect = $('.project-select')
                var subscribeBlock = $('.subscribe-wrapper')

                if (!projectSelect.is(e.target) && projectSelect.has(e.target).length === 0 && projectSelect.hasClass('open')) {
                    projectSelect.removeClass('open')
                }

                if (!searchSelect.is(e.target) && searchSelect.has(e.target).length === 0 && searchSelect.hasClass('open')) {
                    searchSelect.removeClass('open')
                }

                if (!subscribeBlock.is(e.target) && subscribeBlock.has(e.target).length === 0 && subscribeBlock.hasClass('open')) {
                    subscribeBlock.removeClass('open')
                }
            });

            $('#analogue').on('click', '.close-mini', function(){
                if ($(this).parent().parent().hasClass('open')) {
                    $(this).parent().parent().removeClass('open')
                }
            });

            $('#analogue').on('click', '.mini-card__select', function(e){
                if ($(this).next('.mini-card').hasClass('open')) {
                    $(this).next('.mini-card').removeClass('open')
                    $(this).next('.mini-card').children().children().slideUp(300)
                } else {
                    $(this).next('.mini-card').addClass('open')
                    $(this).next('.mini-card').children().children().slideDown(300)
                }
            });


            $('#analogue').on('click', '.change-all', function(){
                $('input[type="checkbox"]').prop("checked", false)
            });

            $('#analogue').on('click', '.favorite-heart', function(){
                $(this).parent().parent().remove()
            });

            $('#analogue').on('click', '.favorite', function(){
                let el = $(this), id = parseInt( el.data('id') );
                if( id )
                $.ajax({
                    url: '/ajax.php',
                    type: 'POST',
                    data: {action: 'fav', id : id},
                }).done(function(data) {
                    data = JSON.parse(data);
                    if( data.noAuth ) {
                        alert("Ошибка, Вам нужно авторизоваться!");
                        return;
                    }
                    //if( data.active )
                        //alert("Добавлено в избранное");
                    //else
                        //alert("Удалено из избранное");
                    el.toggleClass('act');
                });
            });

            $('#analogue').on('click', '.items-selector', function(event){
                contetWindowOptions();
                event.preventDefault();
                if ($('.items-selector').parent().hasClass('open')) {
                    $('.items-selector').parent().removeClass('open')
                } else {
                    $('.items-selector').parent().addClass('open')
                }
                addOptionModal()
            });

        });

        <?
        $arAllProps = array();
        foreach ($arParams["PROPERTY_CODE"] as $code) {
            if (in_array($arResult["ITEMS"][0]["PROPERTIES"][$code]["ID"], $arResult["PROP_AVAL"]))
                $arAllProps[] = array("CODE" => $code, "NAME" => trim($arResult["ITEMS"][0]["PROPERTIES"][$code]["NAME"]));
        }

        global $APPLICATION;
        $uri = $APPLICATION->GetCurUri("", false);
        ?>


        var arPropsChange = <?=CUtil::PhpToJSObject($arResult["CHANGE_OPTIONS"], false, true)?>;
        var arPropsAll = <?=CUtil::PhpToJSObject($arAllProps, false, true)?>;

        function addOptionModal() {
            $('#analogue').on('click', '.add-table-filter', function () {

                var arChangeObject = {"0": "1"};

                //var itemsselector = document.getElementsByName("select_option");
                var itemsselector = $("[name=select_option]");
                var counterArr = 0;
                var items = {};
                for (var item in itemsselector) {
                    if (itemsselector[item].checked) {
                        items[counterArr] = itemsselector[item].id;
                        counterArr++;
                    }
                }

                arChangeObject["OPTIONS_MODAL"] = items;
                arPropsChange = items;
                if(counterArr == 0)
                    arChangeObject["OPTIONS_MODAL"] = false;


                //console.log(arChangeObject);
                var url = __getUriToReload([]);

                $("#analogue > .wrapper > .section-content").load(url + " .section-content .to-reload-by-filter", arChangeObject);
                //history.replaceState(3, "Title 34", url);
                if ($(this).parent().parent().hasClass('open')) {
                    $(this).parent().parent().removeClass('open')
                } else {
                    return false
                }

            });
        }

        function in_object(elem, arr) {
            //console.log(arr);
            for (var key in arr){
                if(arr[key] == elem){
                    //console.log(arr[key]+"||||"+arr);
                    return true;
                }
            }
            return false;
        }

        function contetWindowOptions() {
            var strProps = "";
            for (var prop in arPropsAll){
                if(in_object(arPropsAll[prop].CODE, arPropsChange))
                    strProps += '<div class="table-filter__item"><label ><input type="checkbox" name="select_option" id="' + arPropsAll[prop].CODE + '" checked>' + arPropsAll[prop].NAME + '</label></div>';
                else
                    strProps += '<div class="table-filter__item"><label ><input type="checkbox" name="select_option" id="' + arPropsAll[prop].CODE + '">' + arPropsAll[prop].NAME + '</label></div>';

            }
            var submitBtn = '<button class="default-button add-table-filter">Применить</button>';
            document.getElementById("option_window").innerHTML = strProps;
			document.getElementById("submit-holder").innerHTML = submitBtn;
        }

        <?$currencyFormat = CCurrencyLang::GetFormatDescription('RUB');?>
        BX.Currency.setCurrencyFormat('RUB', <? echo CUtil::PhpToJSObject($currencyFormat, false, true); ?>);

        function quantChange(symbol, price, ids, limit) {

            if(symbol == "+") {
                if(limit.CATALOG_CAN_BUY_ZERO == "N"){
                    if(Number.parseInt(document.getElementById(ids.QUANTITY).value) < Number.parseInt(limit.CATALOG_QUANTITY)){
                        document.getElementById(ids.QUANTITY).value++;
                        quantityVal(price, ids, limit);
                    }
                } else {
                    document.getElementById(ids.QUANTITY).value++;
                    quantityVal(price, ids, limit);
                }

            }else {
                if(document.getElementById(ids.QUANTITY).value > 1) {
                    document.getElementById(ids.QUANTITY).value--;
                    quantityVal(price, ids, limit);
                }
            }
        }


        function curPrice(price, quant) {
            for (var p in price){
                if(price[p].QUANTITY_FROM == "")
                    return price[p];

                if(Number.parseInt(price[p].QUANTITY_FROM) <= Number.parseInt(quant) && price[p].QUANTITY_TO == "0")
                    return price[p];

                if(Number.parseInt(price[p].QUANTITY_FROM) <= Number.parseInt(quant) && Number.parseInt(price[p].QUANTITY_TO) >= Number.parseInt(quant))
                    return price[p];

            }
        }


        function quantityVal(price, ids, limit) {

            //console.log(price);
            var quantCurrent = document.getElementById(ids.QUANTITY).value;
            if(quantCurrent < 1){
                document.getElementById(ids.QUANTITY).value = 1;
                quantCurrent = 1;
            }

            if(limit.CATALOG_CAN_BUY_ZERO == "N") {
                if (Number.parseInt(document.getElementById(ids.QUANTITY).value) > Number.parseInt(limit.CATALOG_QUANTITY)) {
                    document.getElementById(ids.QUANTITY).value = Number.parseInt(limit.CATALOG_QUANTITY);
                    quantCurrent = Number.parseInt(limit.CATALOG_QUANTITY);
                }
            }

            if(price.length > 0) {
                var currentPrice = curPrice(price, quantCurrent);
                //console.log(currentPrice);
                document.getElementById(ids.PRICE_TOTAL).innerHTML = BX.Currency.currencyFormat(currentPrice.RATIO_PRICE * quantCurrent, "RUB", true)
            }
        }
    </script>

    <section class="analogue" id="analogue">
        <div class="wrapper">
            <div class="section-header">
                <span class="current-product"><?=$arResult["NAME_ELEM_ANALOG"];?></span>
                <h2>Аналогичные решения</h2>
            </div>
            <?//$APPLICATION->ShowViewContent('catalog_section_filter');?>
            <div class="section-content">
                <div class="to-reload-by-filter analogue-table__holder">
                    <div class="scroll-table">
                        <table class="analogue-table" id="tableLastProd">
                            <tr>


                                <!--окно параметров-->
                                <th >
                                    <div class="table-filter">
                                        <div class="table-filter__selector">
                                            <span class="items-selector">
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   viewBox="0 0 510.66666 512"
   xml:space="preserve"
   id="svg2"
   version="1.1"><metadata
     id="metadata8"><rdf:RDF><cc:Work
         rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" /></cc:Work></rdf:RDF></metadata><defs
     id="defs6" /><g
     transform="matrix(1.3333333,0,0,-1.3333333,0,512)"
     id="g10"><g
       transform="scale(0.1)"
       id="g12"><path
         id="path14"
         style="fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none"
         d="m 3633.59,2326.79 -268.79,45.62 c -28,90.39 -64.02,177.58 -108,260.78 l 158.4,221.61 c 67.18,94.41 56.79,223.2 -25.59,304.8 l -238.4,238.4 c -44.8,44.8 -104.02,69.61 -167.23,69.61 -49.57,0 -96.79,-15.2 -136.79,-44.03 L 2624.8,3265.19 c -86.4,45.62 -176.79,83.2 -270.39,111.21 l -44.8,265.58 c -19.22,114.42 -117.62,197.62 -233.59,197.62 h -336.8 c -116.02,0 -214.42,-83.2 -233.63,-197.62 l -46.37,-271.99 c -89.61,-28.01 -176.84,-64.8 -260,-109.61 l -220.001,158.44 c -40,28.79 -88.008,43.98 -137.617,43.98 -63.204,0 -123.204,-24.8 -167.192,-69.61 L 435.199,3154.8 c -81.609,-81.61 -92.82,-210.4 -25.59,-304.81 l 160,-224.8 C 525.59,2541.2 490.391,2454.01 463.199,2363.58 L 197.621,2318.82 C 83.1992,2299.6 0,2201.2 0,2085.19 v -336.8 C 0,1632.41 83.1992,1534.01 197.621,1514.8 l 271.988,-46.41 c 28.012,-89.57 64.801,-176.8 109.61,-260 L 421.602,989.211 C 354.41,894.801 364.801,766.012 447.191,684.398 L 685.59,446.012 c 44.801,-44.813 104.019,-69.614 167.219,-69.614 49.57,0 96.8,15.192 136.8,43.981 l 224.801,160 C 1295.2,538 1380,503.578 1467.19,476.398 l 44.8,-268.789 C 1531.21,93.1914 1629.61,9.98828 1745.59,9.98828 h 337.61 c 116.02,0 214.42,83.20312 233.6,197.62072 l 45.58,268.789 c 90.43,28 177.62,63.981 260.82,108 l 221.6,-158.386 c 40,-28.793 88.01,-44.032 137.58,-44.032 63.21,0 122.42,24.809 167.23,69.61 l 238.4,238.398 c 81.6,81.602 92.77,210.391 25.58,304.813 L 3255.2,1217.22 c 44.02,83.98 80.82,171.17 108,260.78 l 268.79,44.8 c 114.42,19.18 197.62,117.58 197.62,233.6 v 336.79 c 1.6,116.02 -81.6,214.42 -196.02,233.6 z M 3615.2,1756.4 c 0,-10.39 -7.19,-19.18 -17.58,-20.78 L 3261.6,1679.6 c -42.38,-7.19 -76.01,-38.4 -86.4,-79.22 -30.4,-117.58 -76.8,-230.39 -139.18,-335.19 -21.61,-36.8 -20,-82.39 4.76,-117.58 L 3238.4,869.211 c 5.58,-8.012 4.8,-20 -2.38,-27.231 l -238.4,-238.402 c -5.63,-5.578 -11.21,-6.359 -15.24,-6.359 -4.76,0 -8.79,1.601 -11.99,3.98 L 2692.81,798.82 c -34.41,24.758 -80.82,26.371 -117.61,4.758 C 2470.39,741.199 2357.62,694.801 2240,664.398 2198.4,654.012 2167.19,619.602 2160.78,578 l -56.8,-336.02 c -1.6,-10.39 -10.39,-17.582 -20.78,-17.582 h -336.79 c -10.39,0 -19.22,7.192 -20.82,17.582 L 1669.61,578 c -7.23,42.379 -38.4,76.012 -79.22,86.398 -114.37,29.614 -224.8,75.204 -328.01,134.422 -16.79,9.571 -35.97,14.371 -54.37,14.371 -21.6,0 -44.03,-6.402 -62.42,-20 l -280,-199.179 c -3.988,-2.422 -7.969,-4.024 -12,-4.024 -3.199,0 -9.61,0.821 -15.192,6.41 L 600,834.801 c -7.191,7.179 -8.012,18.39 -2.379,27.179 L 794.41,1138 c 24.809,35.19 26.371,81.6 4.809,118.4 -62.418,103.98 -110.43,216.79 -140.821,334.41 -11.207,40.78 -44.808,71.99 -86.41,79.18 l -338.398,57.62 c -10.391,1.6 -17.57,10.39 -17.57,20.78 v 336.8 c 0,10.43 7.179,19.21 17.57,20.82 l 333.601,55.97 c 42.418,7.23 76.789,38.4 87.219,80 29.57,117.62 75.199,231.21 136.801,336.02 21.598,36.8 19.18,82.38 -5.621,116.8 l -199.18,280 c -5.629,8 -4.808,20 2.379,27.18 l 238.402,238.4 c 5.618,5.63 11.207,6.41 15.188,6.41 4.812,0 8.832,-1.6 12.031,-3.99 l 275.98,-196.79 c 35.2,-24.81 81.6,-26.41 118.4,-4.81 104.02,62.38 216.8,110.39 334.41,140.78 40.78,11.21 72,44.81 79.18,86.41 l 57.62,338.4 c 1.6,10.43 10.39,17.61 20.78,17.61 h 336.84 c 10.39,0 19.18,-7.18 20.78,-17.61 l 56.01,-333.6 c 7.19,-42.38 38.4,-76.79 80,-87.18 120.79,-30.39 235.98,-77.62 343.21,-140.82 36.79,-21.61 82.38,-20 117.58,4.8 l 276.01,198.4 c 3.99,2.42 8.01,4.02 11.99,4.02 3.21,0 9.61,-0.82 15.2,-6.4 l 238.4,-238.4 c 7.18,-7.23 8,-18.4 2.42,-27.23 L 3041.6,2702.8 c -24.8,-34.41 -26.4,-80.82 -4.8,-117.61 62.42,-104.81 108.79,-217.58 139.22,-335.2 10.39,-41.6 44.76,-72.77 86.36,-79.18 l 336.02,-56.8 c 10.39,-1.6 17.62,-10.43 17.62,-20.82 V 1756.4 Z" /><path
         id="path16"
         style="fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none"
         d="m 1915.2,2751.59 c -455.98,0 -826.41,-370.39 -826.41,-826.4 0,-455.98 370.43,-826.37 826.41,-826.37 456.01,0 826.4,370.39 826.4,826.37 0,456.01 -370.39,826.4 -826.4,826.4 z m 0,-1436.79 c -336.8,0 -610.4,273.59 -610.4,610.39 0,336.79 273.6,610.43 610.4,610.43 336.79,0 610.39,-273.64 610.39,-610.43 0,-336.8 -273.6,-610.39 -610.39,-610.39 z" /></g></g></svg>
                                            </span>
                                            <div class="table-filter__list" id="option_window">
												<div class="filter-list__wrapper" id="option_window">
                                                <?/*if(in_array($prop, $arResult["CHANGE_OPTIONS"])):?>
                                                <?continue;?>
                                            <?endif;*/?>
												</div>
												<div id="submit-holder"></div>
                                            </div>
                                        </div>


                                        <!--окно параметров-->



                                        <?if($_GET["sort2"] == "NAME"):?>
                                            <?if($_GET["method2"] == "" || $_GET["method2"] == "desc"):?>
                                                <div class="table-filter__sorting sort-val-up" onclick="ajaxUpdateList('NAME', 'asc', '1', '')">Название</div>
                                            <?elseif ($_GET["method2"] == "asc"):?>
                                                <div class="table-filter__sorting sort-val-down" onclick="ajaxUpdateList('NAME', 'desc', '1', '')">Название</div>
                                            <?endif;?>

                                        <?else:?>
                                            <div class="table-filter__sorting" onclick="ajaxUpdateList('NAME', 'asc', '1', '')">Название</div>

                                        <?endif;?>
                                    </div>
                                </th>



                                <?$counterCol = 1;?>
                                <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                                    <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arResult["ITEMS"][0]["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
                                        <?continue;?>
                                    <?endif;?>

                                    <?if($prop == "PRODUCERID"):
                                        $prop_filter = $prop.".NAME";
                                    else:
                                        $prop_filter = $prop;
                                    endif;?>

                                    <?if(!empty(trim($prop)) && !empty(trim($arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"]))):?>
                                        <?$counterCol++;?>

                                        <?if($_GET["sort2"] == "PROPERTY_".$prop_filter):?>
                                            <?if($_GET["method2"] == "" || $_GET["method2"] == "desc"):?>
                                                <th class ="sort-val-up" onclick="ajaxUpdateList('<?='PROPERTY_'.$prop_filter?>', 'asc', '1', '')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                            <?elseif ($_GET["method2"] == "asc"):?>
                                                <th class ="sort-val-down" onclick="ajaxUpdateList('<?='PROPERTY_'.$prop_filter?>', 'desc', '1', '')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                            <?endif;?>
                                        <?else:?>
                                            <th onclick="ajaxUpdateList('<?='PROPERTY_'.$prop_filter?>', 'asc', '1', '')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                        <?endif;?>
                                    <?endif;?>
                                <?endforeach;?>

                            </tr>

                            <?foreach ($arResult["ITEMS"] as $arItem):?>
                                <?


                                //$item = $arItem;
                                //$item["CAN_BUY"] = true;
                                $areaId = $this->GetEditAreaId($arItem['ID']);
                                $itemIds = array(
                                    'ID' => $areaId,
                                    'PICT' => $areaId.'_pict',
                                    'SECOND_PICT' => $areaId.'_secondpict',
                                    'PICT_SLIDER' => $areaId.'_pict_slider',
                                    'STICKER_ID' => $areaId.'_sticker',
                                    'SECOND_STICKER_ID' => $areaId.'_secondsticker',
                                    'QUANTITY' => $areaId.'_quantity',
                                    'QUANTITY_DOWN' => $areaId.'_quant_down',
                                    'QUANTITY_UP' => $areaId.'_quant_up',
                                    'QUANTITY_MEASURE' => $areaId.'_quant_measure',
                                    'QUANTITY_LIMIT' => $areaId.'_quant_limit',
                                    'BUY_LINK' => $areaId.'_buy_link',
                                    'BASKET_ACTIONS' => $areaId.'_basket_actions',
                                    'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
                                    'SUBSCRIBE_LINK' => $areaId.'_subscribe',
                                    'COMPARE_LINK' => $areaId.'_compare_link',
                                    'PRICE' => $areaId.'_price',
                                    'PRICE_OLD' => $areaId.'_price_old',
                                    'PRICE_TOTAL' => $areaId.'_price_total',
                                    'DSC_PERC' => $areaId.'_dsc_perc',
                                    'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
                                    'PROP_DIV' => $areaId.'_sku_tree',
                                    'PROP' => $areaId.'_prop_',
                                    'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
                                    'BASKET_PROP_DIV' => $areaId.'_basket_prop',
                                );
                                ?>

                                <tr class="mini-card__select" >
                                    <td id = "name_prod_<?=$arItem["ID"];?>"><?=$arItem["NAME"];?></td>

                                    <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                                        <?if(!in_array($prop, $arResult["CHANGE_OPTIONS"]) || !in_array($arItem["PROPERTIES"][$prop]["ID"], $arResult["PROP_AVAL"])):?>
                                            <?continue;?>
                                        <?endif;?>
                                        <?if(!empty($arItem["PROPERTIES"][$prop]) && !empty(trim($arItem["PROPERTIES"][$prop]["NAME"]))):?>
                                            <td><?=$arItem["PROPERTIES"][$prop]["VALUE"];?></td>
                                        <?endif;?>
                                    <?endforeach;?>
                                    <!--<td><span class="analogue-status ordered">Заказано </span></td>-->
                                </tr>
                                <tr class="mini-card" id="<?=$itemIds["ID"];?>">
                                    <?//$arItem["DETAIL_PAGE_URL"];?>
                                    <td  colspan="<?=$counterCol;?>">
                                        <div class="card-holder">
                                            <div class="mini-cart__wrapper">
                                                <div class="item-img">
                                                    <a href="<?=$arItem["DETAIL_PAGE_URL"];?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                                                    <?$is_favs = empty($favs) ? false : array_search( $arItem["ID"], $favs);?>
                                                    <span class="favorite <?=$is_favs ? 'act':''?>"  data-id="<?=$arItem['ID']?>">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="5 1 28 35" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"></rect>
															<style>
																#fill-heart {
																	fill: transparent;
																}
															</style>
																				
															<g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""></path>
															<path id="svg_22" d="M-29.97297297297297,2.4594594594594668 "></path><path id="svg_2" d="M19.324324324324337,8.837837837837846 "></path>
															<path id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"></path></g>
															
														</svg>
													</span>
                                                </div>
                                                <div class="item-info">
                                                    <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="item-info__head" onclick="console.log('no')">
                                                        <?=$arItem["NAME"];?>
                                                    </a>
                                                    <div class="item-info__content">
                                                        <?if($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"]):?>
                                                            <ul>
                                                                <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"] as $prop):?>
                                                                    <?if(!empty(trim($prop["VALUE"]))):?>
                                                                        <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                                    <?endif;?>
                                                                <?endforeach;?>
                                                            </ul>
                                                        <?endif;?>
                                                        <ul>
                                                            <?if($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"]):?>
                                                                <ul>
                                                                    <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $prop):?>
                                                                        <?if(!empty(trim($prop["VALUE"]))):?>
                                                                            <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                                        <?endif;?>
                                                                    <?endforeach;?>
                                                                </ul>
                                                            <?endif;?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <?if($arItem["CATALOG_AVAILABLE"] == "Y"):?>

                                                    <div class="item-more">
                                                        <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                                        <?if($arItem["CATALOG_QUANTITY"] > 0):?>
                                                            <span class="avalibilty">В наличии <?=$arItem["CATALOG_QUANTITY"]?> <?=declineWordPositionAnalog($arItem["CATALOG_QUANTITY"], $arPsositionVariants, $arPsositionVariantsExaption)?></span>
                                                        <?else:?>
                                                            <span class="avalibilty out">Нет в наличии</span>
                                                        <?endif;?>

                                                        <?//if(!empty($arItem["ITEM_PRICES"])):?>
                                                        <div class="wholesale">
                                                            <?foreach ($arItem["ITEM_PRICES"] as $arPrice):?>
                                                                <?if(!empty($arPrice["QUANTITY_TO"])):?>
                                                                    <div class="quantity">
                                                                        <span class="quantity-items"><?=$arPrice["QUANTITY_FROM"]."-".$arPrice["QUANTITY_TO"]?> штук </span>
                                                                        <span class="quantity-separator"></span>
                                                                        <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                                                    </div>
                                                                <?else:?>
                                                                    <div class="quantity">
                                                                        <?if(empty($arPrice["QUANTITY_FROM"])){
                                                                            $arPrice["QUANTITY_FROM"] = 1;
                                                                        }?>
                                                                        <span class="quantity-items">от <?=$arPrice["QUANTITY_FROM"]?> штук </span>
                                                                        <span class="quantity-separator"></span>
                                                                        <span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"];?></span>
                                                                    </div>
                                                                <?endif;?>
                                                            <?endforeach;?>
                                                        </div>
                                                        <?//endif;?>

                                                        <div class="quantity-num">
                                                            <span class="quantity-button" id="<? echo $itemIds['QUANTITY_DOWN']; ?>" onclick="quantChange('-', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">–</span>
                                                            <input value="1" max="99999" name = "quant_elems_<?=$arItem["ID"];?>" id="<? echo $itemIds['QUANTITY']; ?>" onchange="quantityVal(<?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>)" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
                                                            <span class="quantity-button" id="<? echo $itemIds['QUANTITY_UP']; ?>" onclick="quantChange('+', <?=CUtil::PhpToJSObject($arItem['ITEM_PRICES']);?>, <?=CUtil::PhpToJSObject($itemIds)?>, <?=CUtil::PhpToJSObject(array("CATALOG_CAN_BUY_ZERO" => $arItem["CATALOG_CAN_BUY_ZERO"], "CATALOG_QUANTITY" => $arItem["CATALOG_QUANTITY"]))?>);" href="javascript:void(0)" rel="nofollow">+</span>
                                                        </div>
                                                        <form action="" onkeydown="if(event.keyCode==13){return false;}">
                                                            <?if(!empty($arItem["ITEM_PRICES"])):?>
                                                                <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">
                                                                    <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                                                </div>
                                                                <div class="price" id="<? echo $itemIds['PRICE_TOTAL']; ?>">
                                                                    <?=$arItem["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?>
                                                                </div>
                                                                <div class="cb"></div>
                                                                <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBasket('<?=$arItem["ID"];?>', '');" href="javascript:void(0)" rel="nofollow">Добавить в корзину</a>
                                                            <?else:?>
                                                                <div class="price" style="display: none" id="<? echo $itemIds['PRICE']; ?>">0</div>
                                                                <div class="price" style="display: none" id="<? echo $itemIds['PRICE_TOTAL']; ?>">0</div>
                                                                <div class="cb"></div>
                                                                <a class="add-to-cart" id="<? echo $itemIds['BUY_LINK']; ?>" onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'N');" href="javascript:void(0)" rel="nofollow">Запросить цену</a>
                                                            <?endif;?>
                                                        </form>

                                                        <a <?if ($USER->IsAuthorized()):?>onclick="addToBaskEx('<?=$arItem["ID"];?>', '<?=$arItem["DETAIL_PAGE_URL"];?>', 'Y');"<?else:?>onclick="noAuth();"<?endif;?> class="sidebar-sample" href="javascript:void(0)">Запросить образец</a>

                                                    </div>
                                                <?else:?>
                                                    <div class="item-more">
                                                        <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                                        <span class="avalibilty out">Товар недоступен</span>
                                                    </div>
                                                <?endif;?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>


                            <?endforeach;?>


                        </table>
                    </div>
                </div>
                <span class="to-reload-by-filter" id="buttomsList">
                    <?$arCount = $arResult["NAV_RESULT"]->NavRecordCount - $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer;?>
                    <?if($arCount > 0):?>
                        <?if($arCount >= $arResult["NAV_RESULT"]->NavPageSize):?>
                            <?$showMore = $arResult["NAV_RESULT"]->NavPageSize?>
                        <?else:?>
                            <?$showMore = $arCount?>
                        <?endif;?>


                        <button id = "button_elem_q" class="show-all" onclick="ajaxUpdateList('<?=$_GET["sort2"];?>', '<?=$_GET["method2"]?>', 'addelem', '<?=$arResult["NAV_RESULT"]->NavPageNomer + 1?>');">Показать ещё <?=$showMore;?> из <span id="element_count"><?=$arCount;?></span></button>
                    <?endif;?>
                    <?=$arResult["NAV_STRING"]?>
            </span>
            </div>
        </div>
    </section>
<?endif;?>
