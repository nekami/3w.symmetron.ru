<?
$MESS["T_IBLOCK_DESC_SI_UNITS_IBLOCK_ID"] = "ID инфоблока с приставками разрядов";

$MESS["PROPERTY_CODE_FAST_SHOW_LEFT"] = "Левый блок параметров в мини карточке";
$MESS["PROPERTY_CODE_FAST_SHOW_RIGHT"] = "Правый блок параметров в мини карточке";
$MESS["QUANT_ADD_ELEM"] = "ID инфоблока для фильтра аналогов";


$MESS["PROPERTY_CODE_FOR_SHORT_DISCR_HEAD"] = "Краткое описание - шапка";
$MESS["PROPERTY_CODE_FOR_SHORT_DISCR_BLOCK"] = "Краткое описание - основной блок";
$MESS["PROPERTY_CODE_FOR_GALER"] = "Галерея";

$MESS["PROPERTY_CODE_SPEC_MAIN"] = "Спецификации - основной блок";
$MESS["PROPERTY_CODE_SPEC_BLOCK"] = "Спецификации - общий блок";

$MESS["PROPERTY_CODE_SPEC_CATEGORY_NAME"] = "Спецификации - название категории";
$MESS["PROPERTY_CODE_SPEC_CATEGORY"] = "Спецификации - выводимые свойства в категории";

$MESS["PROPERTY_CODE_IBLOCK_DOC"] = "Свойство библиотеки документации";
$MESS["PROPERTY_CODE_ANALOGS"] = "Свойство аналогов";
$MESS["PROPERTY_CODE_RELATED"] = "Свойство сопутсвующих товаров";

$MESS["PROPERTY_CODE_OTR_S"] = "Свойство отраслевых решений";

$MESS["PAGE_ELEMENT_COUNT_ALL"] = "Количиство выводимых товаров на страницу";
$MESS["PAGE_ELEMENT_COUNT_ANALOGS"] = "Количиство выводимых аналогов на страницу";
$MESS["PAGE_ELEMENT_COUNT_RELATED"] = "Количиство выводимых сопутствующих товаров на страницу";

?>