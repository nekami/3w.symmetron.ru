<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$APPLICATION->IncludeComponent(
    "mcart:catalog.sections",
    ".default",
    Array(
        "CACHE_FILTER"         => "N",
        "CACHE_GROUPS"         => $arParams["CACHE_GROUPS"],
        "CACHE_TIME"           => $arParams["CACHE_TIME"],
        "CACHE_TYPE"           => $arParams["CACHE_TYPE"],
        "COMPOSITE_FRAME_MODE" => $arParams["COMPOSITE_FRAME_MODE"],
        "COMPOSITE_FRAME_TYPE" => $arParams["COMPOSITE_FRAME_TYPE"],
        "FILTER"               => [],
        "FILTER_NAME"          => "",
        "IBLOCK_ID"            => $arParams["IBLOCK_ID"],
        "IBLOCK_TYPE"          => $arParams["IBLOCK_TYPE"],
        "SECTION_FOLDER"       => $arResult["FOLDER"],
        "SEF_FOLDER"           => $arResult["FOLDER"],
        "SEF_MODE"             => $arParams["SEF_MODE"],
        "SEF_URL_TEMPLATES"    => Array("sections" => "#SECTION_CODE_PATH#/"),
        "VARIABLE_ALIASES"     => Array(
            "SECTION_ID"   => "SECTION_ID",
            "SECTION_CODE" => "SECTION_CODE",
        ),
    )
);