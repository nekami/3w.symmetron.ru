<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $USER_FIELD_MANAGER;

$arParamEntity = $_POST["ENTITY_PARAMS"];
$arFilterGet = $_POST["FILTER_GET"];

$arUserFieldFilter = $USER_FIELD_MANAGER->GetUserFields(
    "IBLOCK_" . $arParamEntity["IBLOCK_ID"] . "_SECTION",
    $arParamEntity["SECTION_ID"]
)["UF_FILTER_PARAMS"];

if(!empty($arUserFieldFilter)){
    if($USER_FIELD_MANAGER->Update(
        "IBLOCK_" . $arParamEntity["IBLOCK_ID"] . "_SECTION",
        $arParamEntity["SECTION_ID"],
        array("UF_FILTER_PARAMS" => http_build_query($arFilterGet))
    )){
        echo "1";
    }
}