<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "",
    Array('START_FROM' => 1)
); ?>

<?
/* First save settings of visible columns if changed... */
if (isset($_REQUEST["OPTIONS_MODAL"]))
    $_REQUEST["OPTIONS_MODAL"] = array_unique($_REQUEST["OPTIONS_MODAL"]);
if (!empty($_REQUEST["OPTIONS_MODAL"])) {

    if ($USER->IsAuthorized()) {
        CUserOptions::SetOption(
            "catalog.section",
            $arResult["VARIABLES"]["SECTION_ID"],
            $_REQUEST["OPTIONS_MODAL"],
            false,
            $USER->GetID()
        );
    } else {
        $_SESSION["catalog.section"][$arResult["VARIABLES"]["SECTION_ID"]] = $_REQUEST["OPTIONS_MODAL"];
    }
    unset($_GET["OPTIONS_MODAL"]);
    unset($_POST["OPTIONS_MODAL"]);
    unset($_REQUEST["OPTIONS_MODAL"]);
}
?>
<?/* Title */?>
<?$APPLICATION->ShowViewContent('catalog_title');?>


<?/* Set filter from options */?>
<?
$arFilterGet = $_GET;

if(!empty($arFilterGet)){
    unset($arFilterGet["clear_cache"]);
    unset($arFilterGet["bxajaxid"]);
    unset($arFilterGet["OPTIONS_MODAL"]);

    $arFilterGet = array_filter($arFilterGet, function ($el) {
        return !empty($el);
    });
} else {
    $arFilterGet = array();
}

$arForAjax = array(
    "ENTITY_PARAMS" => array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"]
    ),
    "FILTER_GET" => $arFilterGet
);

if(empty($arFilterGet)){
    global $USER_FIELD_MANAGER;
    $arUserFieldFilter = $USER_FIELD_MANAGER->GetUserFields(
        "IBLOCK_" . $arParams["IBLOCK_ID"] . "_SECTION",
        $arResult["VARIABLES"]["SECTION_ID"]
    )["UF_FILTER_PARAMS"]["VALUE"];

    if(!empty($arUserFieldFilter)) {

        parse_str($arUserFieldFilter, $arUserFieldFilter);

        $_GET = array_merge($_GET, $arUserFieldFilter);
        $_GET["set_filter"] = "";
    }
}
?>

<? /* Get actual settings of visible columns */

$columnsProps = ["ARTICLE", "PRODUCERID", "DESCRIPTION"];
$isDefault = true;
if ($USER->IsAuthorized()) {
    $aGridOptionsDef = CUserOptions::GetOption(
        "catalog.section",
        $arResult["VARIABLES"]["SECTION_ID"],
        false,
        $USER->GetID()
    );
} else {
    $aGridOptionsDef = $_SESSION["catalog.section"][$arResult["VARIABLES"]["SECTION_ID"]];
}

if (!empty($aGridOptionsDef)) {
    $isDefault = false;
    $columnsProps = $aGridOptionsDef;
}
?>

<?
//sort catalog params
$sectionCode = $arResult["VARIABLES"]["SECTION_CODE"];

/* Properties visible options */
$obCache = new CPHPCache;
$lifeTime = 3600;
global $USER;
$cacheId = "section_" . $sectionCode . $USER->GetID();

$arSelectProps = array();
$fromCache = false;

if($obCache->InitCache($lifeTime, $cacheId, "/")) {
    $vars = $obCache->GetVars();

    if(empty($arResult["VARIABLES"]["SECTION_ID"])){
        $arResult["VARIABLES"]["SECTION_ID"] = $vars["SECTION_ID"];
    }

    if($vars["MODAL_SETTINGS"] == $columnsProps){
        $fromCache = true;
        $arSelectPropsAll = $vars["COLUMNS_PROPERTIES"];
        $arSelectProps = $arSelectPropsAll["PROPERTY_ON_LIST"];

        if ($isDefault) {
            if(empty($arSelectProps))
                $arSelectProps = $columnsProps;
        } else {
            if (!empty($columnsProps)) {
                $arSelectProps = [];
                foreach ($arSelectPropsAll["PROPERTY_ALL"] as $id => $code) {
                    if (in_array($code, $columnsProps)) {
                        $arSelectProps[$id] = $code;
                    }
                }
            }
        }

    } else {
        $obCache->CleanDir();
    }
}

$arSectionInfo = [];
if(!$fromCache) {
    if($obCache->StartDataCache()) {
        if (!empty($sectionCode)) {
            //if ($isDefault) {
            if (\Bitrix\Main\Loader::includeModule("mcart.sortcatalogsm")) {
                if(empty($arResult["VARIABLES"]["SECTION_ID"])) {
                    $rsSectionInfo = CIBlockSection::GetList([], ["CODE" => $sectionCode, "ACTIVE" => "Y"], false,
                        ["ID", "LEFT_MARGIN", "RIGHT_MARGIN"]);

                    $arSectionInfo = $rsSectionInfo->Fetch();
                } else {
                    $arSectionInfo["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
                }
                if (!empty($arSectionInfo)) {
                    $sectionsList[] = $arSectionInfo["ID"];
                    $sectionsList = \Mcart\SortCatalogSM\Helper::GetCatalogParentSectionsId(array(
                        "IBLOCK_ID" => $arSectionInfo["IBLOCK_ID"],
                        '<LEFT_MARGIN' => $arSectionInfo["LEFT_MARGIN"],
                        '>RIGHT_MARGIN' => $arSectionInfo["RIGHT_MARGIN"],
                    ), $sectionsList);

                    $arSelectPropsAll = \Mcart\SortCatalogSM\Helper::GetDefaultProperties($sectionsList);

                    $arSelectProps = $arSelectPropsAll["PROPERTY_ON_LIST"];
                }
            }
            //}
            if ($isDefault) {
                if(empty($arSelectProps))
                    $arSelectProps = $columnsProps;
            } else {
                if (!empty($columnsProps)) {
                    $arSelectProps = [];
                    foreach ($arSelectPropsAll["PROPERTY_ALL"] as $id => $code) {
                        if (in_array($code, $columnsProps)) {
                            $arSelectProps[$id] = $code;
                        }
                    }
                }
            }

        }

        if(empty($arResult["VARIABLES"]["SECTION_ID"])){
            $arResult["VARIABLES"]["SECTION_ID"] = $arSectionInfo["ID"];
        }

        $obCache->EndDataCache(array(
            "MODAL_SETTINGS"     => $columnsProps,
            "COLUMNS_PROPERTIES" => $arSelectPropsAll,
            "SECTION_ID" => $arSectionInfo["ID"]
        ));
    }
}


//get hints

$obCache = new CPHPCache;
$lifeTime = 3600 * 24;
$cacheId = "section_" . $sectionCode . "_hints";

$arHints = [];
if(CModule::IncludeModule("mcart.sortcatalogsm") && !empty($arResult["VARIABLES"]["SECTION_ID"])) {
    if ($obCache->InitCache($lifeTime, $cacheId, "/")) {
        $arHints = $obCache->GetVars();
    } else {
        if ($obCache->StartDataCache()) {
            $arHints = \Mcart\SortCatalogSM\Helper::getHints($arResult["VARIABLES"]["SECTION_ID"]);
            $obCache->EndDataCache($arHints);
        }
    }
}

?>

<?/* Filter */?>
<?if ($arParams["USE_FILTER"] == "Y") {
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.smart.filter",
        "",
        array(
            "SI_UNITS_IBLOCK_ID" => $arParams["SI_UNITS_IBLOCK_ID"],

            "IBLOCK_TYPE"         => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID"           => $arParams["IBLOCK_ID"],
            "SECTION_ID"          => $arResult["VARIABLES"]["SECTION_ID"],
            "FILTER_NAME"         => $arParams["FILTER_NAME"],
            "PRICE_CODE"          => $arParams["~PRICE_CODE"],
            "CACHE_TYPE"          => $arParams["CACHE_TYPE"],
            "CACHE_TIME"          => $arParams["CACHE_TIME"],
            "CACHE_GROUPS"        => $arParams["CACHE_GROUPS"],
            "SAVE_IN_SESSION"     => "N",
            "FILTER_VIEW_MODE"    => $arParams["FILTER_VIEW_MODE"],
            "XML_EXPORT"          => "N",
            "SECTION_TITLE"       => "NAME",
            "SECTION_DESCRIPTION" => "DESCRIPTION",
            'HIDE_NOT_AVAILABLE'  => $arParams["HIDE_NOT_AVAILABLE"],
            "TEMPLATE_THEME"      => $arParams["TEMPLATE_THEME"],
            'CONVERT_CURRENCY'    => $arParams['CONVERT_CURRENCY'],
            'CURRENCY_ID'         => $arParams['CURRENCY_ID'],
            "SEF_MODE"            => $arParams["SEF_MODE"],
            "SEF_RULE"            => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
            "SMART_FILTER_PATH"   => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
            "PAGER_PARAMS_NAME"   => $arParams["PAGER_PARAMS_NAME"],
            "INSTANT_RELOAD"      => "Y", //$arParams["INSTANT_RELOAD"],
            "PARAMS_SORT_CATALOG" => $arSelectPropsAll,
            "PROPERTIES_HINTS" => $arHints
        ),
        $component,
        array('HIDE_ICONS' => 'Y')
    );
}
?>

<?/* set filter button */?>

<?if ($USER->IsAdmin()){
    ?>
<section class="default-filter">
	<div class="wrapper">
		<button id="set-filter-button" class="set-filter-button">Установить фильтр по умолчанию</button> 
	</div>
</section>
    <script>
        function setFilterDefault(arParams, templateFolder) {
            BX.ajax.post(
                templateFolder + "/ajax_set_filter.php",
                arParams,
                function (data) {
                    if(data > 0){
                        BX.Mcart.modals.result_off.open([
                            "Уведомление",
                            "Фильтр успешно сохранён"
                        ]);
                    }
                }
            );
        }

        var paramsForAjax = <?=CUtil::PhpToJSObject($arForAjax)?>;
        document.getElementById("set-filter-button").onclick = function () {
            setFilterDefault(paramsForAjax, "<?=$templateFolder?>");
        }
    </script>
<?}?>

<?
$_SESSION["COUNT_ELEM_ON_PAGE_ALL"] = $arParams["PAGE_ELEMENT_COUNT_ALL"];
$_SESSION["COUNT_ELEM_ON_PAGE_SECTION_ID"] = $arResult["VARIABLES"]["SECTION_ID"];

$newCountElemOnPage = (int)htmlspecialchars($_POST["COUNT_ELEM_ON_PAGE"]);
if($newCountElemOnPage > 0){
    $_SESSION["COUNT_ELEM_ON_PAGE"][$arResult["VARIABLES"]["SECTION_ID"]] = $newCountElemOnPage;
}
unset($newCountElemOnPage);

$pageElemCount = $_SESSION["COUNT_ELEM_ON_PAGE"][$arResult["VARIABLES"]["SECTION_ID"]] ?:$arParams["PAGE_ELEMENT_COUNT_ALL"];

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "",
    array(
        "COLUMNS_PROPERTIES"              => $arSelectProps,
        "IBLOCK_TYPE"                     => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID"                       => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD"              => $_GET["sort"] ?: $arParams["ELEMENT_SORT_FIELD"],
        "ELEMENT_SORT_ORDER"              => $_GET["method"] ?: $arParams["ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD2"             => $arParams["ELEMENT_SORT_FIELD2"],
        "ELEMENT_SORT_ORDER2"             => $arParams["ELEMENT_SORT_ORDER2"],
        "PROPERTY_CODE"                   => array_merge(
            $arSelectPropsAll["PROPERTY_ALL"],
            $arParams["LIST_PROPERTY_CODE"],
            $arParams["PROPERTY_CODE_FAST_SHOW_LEFT"],
            $arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"]
        ),
        "PROPERTY_CODE_MOBILE"            => $arParams["LIST_PROPERTY_CODE_MOBILE"],
        "META_KEYWORDS"                   => $arParams["LIST_META_KEYWORDS"],
        "META_DESCRIPTION"                => $arParams["LIST_META_DESCRIPTION"],
        "BROWSER_TITLE"                   => $arParams["LIST_BROWSER_TITLE"],
        "SET_LAST_MODIFIED"               => $arParams["SET_LAST_MODIFIED"],
        "INCLUDE_SUBSECTIONS"             => $arParams["INCLUDE_SUBSECTIONS"],
        "BASKET_URL"                      => $arParams["BASKET_URL"],
        "ACTION_VARIABLE"                 => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE"             => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE"             => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE"       => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "PRODUCT_PROPS_VARIABLE"          => $arParams["PRODUCT_PROPS_VARIABLE"],
        "FILTER_NAME"                     => $arParams["FILTER_NAME"],
        "CACHE_TYPE"                      => $arParams["CACHE_TYPE"],
        "CACHE_TIME"                      => $arParams["CACHE_TIME"],
        "CACHE_FILTER"                    => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS"                    => $arParams["CACHE_GROUPS"],
        "SET_TITLE"                       => $arParams["SET_TITLE"],
        "MESSAGE_404"                     => $arParams["~MESSAGE_404"],
        "SET_STATUS_404"                  => $arParams["SET_STATUS_404"],
        "SHOW_404"                        => $arParams["SHOW_404"],
        "FILE_404"                        => $arParams["FILE_404"],
        "DISPLAY_COMPARE"                 => $arParams["USE_COMPARE"],
        "PAGE_ELEMENT_COUNT"              => $pageElemCount,
        "LINE_ELEMENT_COUNT"              => $arParams["LINE_ELEMENT_COUNT"],
        "PRICE_CODE"                      => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT"                 => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT"                => $arParams["SHOW_PRICE_COUNT"],
        "PRICE_VAT_INCLUDE"               => $arParams["PRICE_VAT_INCLUDE"],
        "USE_PRODUCT_QUANTITY"            => $arParams['USE_PRODUCT_QUANTITY'],
        "ADD_PROPERTIES_TO_BASKET"        => $arParams["ADD_PROPERTIES_TO_BASKET"] ?: '',
        "PARTIAL_PRODUCT_PROPERTIES"      => $arParams["PARTIAL_PRODUCT_PROPERTIES"] ?: '',
        "PRODUCT_PROPERTIES"              => $arParams["PRODUCT_PROPERTIES"],
        "DISPLAY_TOP_PAGER"               => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER"            => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE"                     => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS"               => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_TEMPLATE"                  => $arParams["PAGER_TEMPLATE"],
        "PAGER_DESC_NUMBERING"            => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL"                  => $arParams["PAGER_SHOW_ALL"],
        "PAGER_BASE_LINK_ENABLE"          => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_BASE_LINK"                 => $arParams["PAGER_BASE_LINK"],
        "PAGER_PARAMS_NAME"               => $arParams["PAGER_PARAMS_NAME"],
        "LAZY_LOAD"                       => $arParams["LAZY_LOAD"],
        "MESS_BTN_LAZY_LOAD"              => $arParams["~MESS_BTN_LAZY_LOAD"],
        "LOAD_ON_SCROLL"                  => $arParams["LOAD_ON_SCROLL"],
        "OFFERS_CART_PROPERTIES"          => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE"               => $arParams["LIST_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE"            => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD"               => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER"               => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_SORT_FIELD2"              => $arParams["OFFERS_SORT_FIELD2"],
        "OFFERS_SORT_ORDER2"              => $arParams["OFFERS_SORT_ORDER2"],
        "OFFERS_LIMIT"                    => $arParams["LIST_OFFERS_LIMIT"],
        "SECTION_ID"                      => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE"                    => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_URL"                     => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL"                      => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        "USE_MAIN_ELEMENT_SECTION"        => $arParams["USE_MAIN_ELEMENT_SECTION"],
        'CONVERT_CURRENCY'                => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID'                     => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE'              => $arParams["HIDE_NOT_AVAILABLE"],
        'HIDE_NOT_AVAILABLE_OFFERS'       => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
        'LABEL_PROP'                      => $arParams['LABEL_PROP'],
        'LABEL_PROP_MOBILE'               => $arParams['LABEL_PROP_MOBILE'],
        'LABEL_PROP_POSITION'             => $arParams['LABEL_PROP_POSITION'],
        'ADD_PICT_PROP'                   => $arParams['ADD_PICT_PROP'],
        'PRODUCT_DISPLAY_MODE'            => $arParams['PRODUCT_DISPLAY_MODE'],
        'PRODUCT_BLOCKS_ORDER'            => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
        'PRODUCT_ROW_VARIANTS'            => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
        'ENLARGE_PRODUCT'                 => $arParams['LIST_ENLARGE_PRODUCT'],
        'ENLARGE_PROP'                    => $arParams['LIST_ENLARGE_PROP'] ?: '',
        'SHOW_SLIDER'                     => $arParams['LIST_SHOW_SLIDER'],
        'SLIDER_INTERVAL'                 => $arParams['LIST_SLIDER_INTERVAL'] ?: '',
        'SLIDER_PROGRESS'                 => $arParams['LIST_SLIDER_PROGRESS'] ?: '',
        'OFFER_ADD_PICT_PROP'             => $arParams['OFFER_ADD_PICT_PROP'],
        'OFFER_TREE_PROPS'                => $arParams['OFFER_TREE_PROPS'],
        'PRODUCT_SUBSCRIPTION'            => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT'           => $arParams['SHOW_DISCOUNT_PERCENT'],
        'DISCOUNT_PERCENT_POSITION'       => $arParams['DISCOUNT_PERCENT_POSITION'],
        'SHOW_OLD_PRICE'                  => $arParams['SHOW_OLD_PRICE'],
        'SHOW_MAX_QUANTITY'               => $arParams['SHOW_MAX_QUANTITY'],
        'MESS_SHOW_MAX_QUANTITY'          => $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: '',
        'RELATIVE_QUANTITY_FACTOR'        => $arParams['RELATIVE_QUANTITY_FACTOR'] ?: '',
        'MESS_RELATIVE_QUANTITY_MANY'     => $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: '',
        'MESS_RELATIVE_QUANTITY_FEW'      => $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: '',
        'MESS_BTN_BUY'                    => $arParams['~MESS_BTN_BUY'] ?: '',
        'MESS_BTN_ADD_TO_BASKET'          => $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: '',
        'MESS_BTN_SUBSCRIBE'              => $arParams['~MESS_BTN_SUBSCRIBE'] ?: '',
        'MESS_BTN_DETAIL'                 => $arParams['~MESS_BTN_DETAIL'] ?: '',
        'MESS_NOT_AVAILABLE'              => $arParams['~MESS_NOT_AVAILABLE'] ?: '',
        'MESS_BTN_COMPARE'                => $arParams['~MESS_BTN_COMPARE'] ?: '',
        'USE_ENHANCED_ECOMMERCE'          => $arParams['USE_ENHANCED_ECOMMERCE'] ?: '',
        'DATA_LAYER_NAME'                 => $arParams['DATA_LAYER_NAME'] ?: '',
        'BRAND_PROPERTY'                  => $arParams['BRAND_PROPERTY'] ?: '',
        'TEMPLATE_THEME'                  => $arParams['TEMPLATE_THEME'] ?: '',
        'ADD_SECTIONS_CHAIN'              => $arParams['ADD_SECTIONS_CHAIN'] ?: '',
        'ADD_ELEMENT_CHAIN'               => $arParams['ADD_ELEMENT_CHAIN'] ?: '',
        'ADD_TO_BASKET_ACTION'            => $arParams['SECTION_ADD_TO_BASKET_ACTION'] ?: '',
        'SHOW_CLOSE_POPUP'                => $arParams['COMMON_SHOW_CLOSE_POPUP'] ?: '',
        'COMPARE_PATH'                    => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
        'COMPARE_NAME'                    => $arParams['COMPARE_NAME'],
        'USE_COMPARE_LIST'                => 'Y',
        'BACKGROUND_IMAGE'                => $arParams['SECTION_BACKGROUND_IMAGE'] ?: '',
        'COMPATIBLE_MODE'                 => $arParams['COMPATIBLE_MODE'] ?: '',
        'DISABLE_INIT_JS_IN_COMPONENT'    => $arParams['DISABLE_INIT_JS_IN_COMPONENT'] ?: '',
        'PROPERTY_CODE_FAST_SHOW_LEFT'    => $arParams["PROPERTY_CODE_FAST_SHOW_LEFT"],
        'PROPERTY_CODE_FAST_SHOW_RIGHT'   => $arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"],
        "AJAX_MODE"                       => $arParams["AJAX_MODE"],
        "AJAX_OPTION_ADDITIONAL"          => $arParams["AJAX_OPTION_ADDITIONAL"],
        "AJAX_OPTION_HISTORY"             => $arParams["AJAX_OPTION_HISTORY"],
        "AJAX_OPTION_JUMP"                => $arParams["AJAX_OPTION_JUMP"],
        "AJAX_OPTION_STYLE"               => $arParams["AJAX_OPTION_STYLE"],
        "NAV_SHOW_MORE_RULE"              => [
            "items"      => ".items-container",
            "pagination" => ".pagination-container",
        ],
        "PARAMS_SORT_CATALOG" => $arSelectPropsAll,
        "PROPERTIES_HINTS" => $arHints
    ),
    $component
);
