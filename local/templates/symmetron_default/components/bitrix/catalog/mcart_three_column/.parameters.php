<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$path = "/bitrix/components/bitrix/catalog/templates/.default/";
require($_SERVER["DOCUMENT_ROOT"].$path.".parameters.php");

if(!CModule::IncludeModule("iblock"))
    return false;
$IBLOCK_ID = $arCurrentValues["IBLOCK_ID"];
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$IBLOCK_ID));

$arPropList = [];
$arPropListHTML = [];
while ($prop_fields = $properties->GetNext())
{
    $arPropList[$prop_fields["CODE"]] = $prop_fields["NAME"];
    if($prop_fields["USER_TYPE"] == "HTML")
        $arPropListHTML[$prop_fields["CODE"]] = $prop_fields["NAME"];
    //echo $prop_fields["ID"]." - ".$prop_fields["NAME"]."<br>";
}

$arTemplateParameters["SI_UNITS_IBLOCK_ID"] = array(
    "NAME" => GetMessage("T_IBLOCK_DESC_SI_UNITS_IBLOCK_ID"),
    "TYPE" => "STRING",
);

//$hd = fopen(__DIR__ . "/log_order_parce_" . time() . ".txt", "a");
//fwrite($hd, print_r($arCurrentValues, 1));
//fclose($hd);
//для карточки в списке
$arTemplateParameters["PROPERTY_CODE_FAST_SHOW_LEFT"] = array(
    "PARENT" => "LIST_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FAST_SHOW_LEFT"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FAST_SHOW_LEFT']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);

$arTemplateParameters["PROPERTY_CODE_FAST_SHOW_RIGHT"] = array(
    "PARENT" => "LIST_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FAST_SHOW_RIGHT"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FAST_SHOW_RIGHT']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);



//для детальной карточки


$arTemplateParameters["PROPERTY_CODE_FOR_SHORT_DISCR_HEAD"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FOR_SHORT_DISCR_HEAD"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FOR_SHORT_DISCR_HEAD']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);


$arTemplateParameters["PROPERTY_CODE_FOR_SHORT_DISCR_BLOCK"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FOR_SHORT_DISCR_BLOCK"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FOR_SHORT_DISCR_BLOCK']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);

$arTemplateParameters["PROPERTY_CODE_FOR_GALER"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_FOR_GALER"),
    "TYPE" => "LIST",
    "MULTIPLE" => "N",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_FOR_GALER']) ? "Y" : "N",
    //"ADDITIONAL_VALUES" => "Y",
);


//specifications
$arTemplateParameters["PROPERTY_CODE_SPEC_MAIN"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_SPEC_MAIN"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropListHTML,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_BLOCK']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);

$arTemplateParameters["PROPERTY_CODE_SPEC_BLOCK"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_SPEC_BLOCK"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_BLOCK']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);


$arTemplateParameters["QUANT_ADD_ELEM"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("QUANT_ADD_ELEM"),
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "VALUES" => "22",
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
);


//категории

/*$arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_0_NAME"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY_NAME")." 0",
    "TYPE" => "STRING",
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_0_NAME']) ? "Y" : "N",
);

$arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_0"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_SPEC_BLOCK")." 0",
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_0']) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);*/


$countCategory = 0;
$arNumbers = array();
foreach ($arCurrentValues as $key => $val){

    $pos = strpos($key, "PROPERTY_CODE_SPEC_CATEGORY");
    if ($pos === false) {

    }else{
        $pos2 = strpos($key, "NAME");
        if($pos2 === false){
            if(!empty($arCurrentValues[$key."_NAME"])) {
                $arKey = explode("_", $key);
                $arNumbers[] = intval($arKey[4]);
                $countCategory++;
            }
        }

    }
}


//$arNumbers = sort($arNumbers);
$c = count($arNumbers);

if($c > 0){

    $newCatNum = $arNumbers[$c - 1] + 1;

}

/*for($i = 0; $i <= $countCategory; $i++){

    $arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_".$i."_NAME"] = array(
        "PARENT" => "DETAIL_SETTINGS",
        "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY_NAME")." ".$i,
        "TYPE" => "STRING",
        //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
        "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_'.$i.'_NAME']) ? "Y" : "N",
    );

    $arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_".$i] = array(
        "PARENT" => "DETAIL_SETTINGS",
        "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY")." ".$i,
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arPropList,
        //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
        "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_'.$i]) ? "Y" : "N",
        "ADDITIONAL_VALUES" => "Y",
    );

}*/

foreach ($arNumbers as $i){

    $arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_".$i."_NAME"] = array(
        "PARENT" => "DETAIL_SETTINGS",
        "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY_NAME")." ".$i,
        "TYPE" => "STRING",
        //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
        "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_'.$i.'_NAME']) ? "Y" : "N",
    );

    $arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_".$i] = array(
        "PARENT" => "DETAIL_SETTINGS",
        "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY")." ".$i,
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arPropList,
        //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
        "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_'.$i]) ? "Y" : "N",
        "ADDITIONAL_VALUES" => "Y",
    );

}



$arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_".$newCatNum."_NAME"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY_NAME")." ".$newCatNum,
    "TYPE" => "STRING",
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_'.$newCatNum.'_NAME']) ? "Y" : "N",
);

$arTemplateParameters["PROPERTY_CODE_SPEC_CATEGORY_".$newCatNum] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_SPEC_CATEGORY")." ".$newCatNum,
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_SPEC_CATEGORY_'.$newCatNum]) ? "Y" : "N",
    "ADDITIONAL_VALUES" => "Y",
);






$arTemplateParameters["PROPERTY_CODE_IBLOCK_DOC"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_IBLOCK_DOC"),
    "TYPE" => "LIST",
    "MULTIPLE" => "N",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_IBLOCK_DOC']) ? "Y" : "N",
    //"ADDITIONAL_VALUES" => "Y",
);

//$hd = fopen(__DIR__ . "/log_order_parce_" . time() . ".txt", "a");
//    fwrite($hd, print_r($arCurrentValues, 1));
//    fclose($hd);

//unset($arTemplateParameters["FILTER_VIEW_MODE"]);
$arTemplateParameters["FILTER_VIEW_MODE"]["HIDDEN"] = true;

unset($arTemplateParameters["SECTIONS_VIEW_MODE"]);



$arTemplateParameters["PROPERTY_CODE_ANALOGS"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_ANALOGS"),
    "TYPE" => "LIST",
    "MULTIPLE" => "N",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_ANALOGS']) ? "Y" : "N",
    //"ADDITIONAL_VALUES" => "Y",
);
$arTemplateParameters["PROPERTY_CODE_RELATED"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_RELATED"),
    "TYPE" => "LIST",
    "MULTIPLE" => "N",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_RELATED']) ? "Y" : "N",
    //"ADDITIONAL_VALUES" => "Y",
);

$arTemplateParameters["PROPERTY_CODE_OTR_S"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PROPERTY_CODE_OTR_S"),
    "TYPE" => "LIST",
    "MULTIPLE" => "N",
    "VALUES" => $arPropList,
    //"SIZE" => (count($arPropList) > 5 ? 8 : 3),
    "REFRESH" => isset($templateProperties['PROPERTY_CODE_OTR_S']) ? "Y" : "N",
    //"ADDITIONAL_VALUES" => "Y",
);

$arTemplateParameters["PAGE_ELEMENT_COUNT_ALL"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PAGE_ELEMENT_COUNT_ALL"),
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "DEFAULT" => 50,
);
$arTemplateParameters["PAGE_ELEMENT_COUNT_ANALOGS"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PAGE_ELEMENT_COUNT_ANALOGS"),
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "DEFAULT" => 50,
);
$arTemplateParameters["PAGE_ELEMENT_COUNT_RELATED"] = array(
    "PARENT" => "DETAIL_SETTINGS",
    "NAME" => GetMessage("PAGE_ELEMENT_COUNT_RELATED"),
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
    "DEFAULT" => 50,
);

