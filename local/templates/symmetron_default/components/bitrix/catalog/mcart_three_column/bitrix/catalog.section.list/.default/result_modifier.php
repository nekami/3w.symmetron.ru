<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!empty($arParams["STORE_SECTION_ID"]))
{
    $result = \CIBlockSection::GetNavChain($arParams["IBLOCK_ID"], $arParams["STORE_SECTION_ID"], array("ID"));
    $arResult["CHOSEN_SECTIONS"] = array();
    while ($section = $result->fetch())
        $arResult["CHOSEN_SECTIONS"][] = $section["ID"];
}

$this->__component->SetResultCacheKeys("BREADCRUMBS");