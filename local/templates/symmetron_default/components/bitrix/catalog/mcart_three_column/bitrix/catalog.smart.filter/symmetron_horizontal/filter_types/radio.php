<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @global array $currentItem */

use \Bitrix\Main\Localization\Loc;
?>
<div class="add-filter__item radio-choice__item">
    <h4>Наличие</h4>
    <div class="items-choice__radio">
        <label ><input type="radio" name="aw" value="1">Да</label>
        <label ><input type="radio" name="aw" value="2">Нет</label>
    </div>

    <button class="default-button add-filter__unit" >Добавить</button>
</div>

<div class="add-filter__item radio-choice__item <?=$arItem["CODE"]?>__item" itemid="<?=$arItem["CODE"]?>">
    <h4><?=$arItem["NAME"]?></h4>
    <div class="items-choice__radio">
        <?foreach($arItem["VALUES"] as $val => $ar):?>
            <?if (!empty($ar["DISABLED"]) && empty($ar["CHECKED"]))
                continue;?>
            <label><input
                        type="radio"
                        value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                        name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
                        id="<? echo $ar["CONTROL_ID"] ?>"
                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                /><?=$ar["VALUE"];?></label>
        <?endforeach;?>
    </div>

    <button type="button" class="default-button add-filter__unit"><?=Loc::GetMessage("SYMMETRON_CH_FILTER_BUTTON_ADD")?></button>
</div>