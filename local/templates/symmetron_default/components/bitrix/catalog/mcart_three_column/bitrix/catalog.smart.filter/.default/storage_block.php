<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$arElements = [];
if(\Bitrix\Main\Loader::includeModule("mcart.propertywithmeasureunits")) {

    $arMeasureProps = [];
    foreach ($arResult["ITEMS"] as $arItem) {

        if ($arItem["USER_TYPE"] == "mcart_property_with_measure_units" && empty($arItem["DISABLED"])) {
            $arMeasureProps[$arItem["USER_TYPE_SETTINGS"]["ELEMENT_ID"]] = $arItem["USER_TYPE_SETTINGS"];
        }

    }

    if(!empty($arMeasureProps)) {
        /*$arElements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasuresUnitsList(
            $arMeasureProps, ["PROPERTY_ALTERNATIVE"]);*/

        $obCache = new CPHPCache;
        $lifeTime = 3600;
        global $USER;
        $cacheId = md5(serialize($arMeasureProps) . $USER->GetID());

        if($obCache->InitCache($lifeTime, $cacheId, "/")) {
            $arElements = $obCache->GetVars();
        } else {
            if($obCache->StartDataCache()) {
                $arElements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasureUnitsListFromHL(array_keys($arMeasureProps));
                $obCache->EndDataCache($arElements);
            }
        }
    }
}

foreach ($arResult["ITEMS"] as $arItem) {
    if (empty($arItem["DISABLED"])) {
        if ($arItem["PRICE"]) {
            if (isset($arItem["VALUES"]["MIN"]["VALUE"]))
                $arItem["VALUES"]["MIN"]["VALUE"] = round($arItem["VALUES"]["MIN"]["VALUE"], 2);
            if (isset($arItem["VALUES"]["MAX"]["VALUE"]))
                $arItem["VALUES"]["MAX"]["VALUE"] = round($arItem["VALUES"]["MAX"]["VALUE"], 2);
            if (isset($arItem["VALUES"]["MIN"]["HTML_VALUE"]))
                $arItem["VALUES"]["MIN"]["HTML_VALUE"] = round($arItem["VALUES"]["MIN"]["HTML_VALUE"], 2);
            if (isset($arItem["VALUES"]["MAX"]["HTML_VALUE"]))
                $arItem["VALUES"]["MAX"]["HTML_VALUE"] = round($arItem["VALUES"]["MAX"]["HTML_VALUE"], 2);

        }
        $APPLICATION->IncludeComponent(
            "mcart:catalog.filter.form",
            "",
            array(
                "FULL"               => false,
                "CODE"               => $arItem["CODE"],
                "TYPE"               => $arItem["TEMPLATE_TYPE"],
                "VALUES"             => $arItem["VALUES"],
                "USER_TYPE"          => $arItem["USER_TYPE"],
                "USER_TYPE_SETTINGS" => $arItem["USER_TYPE_SETTINGS"],
                "RANGE_TYPE"         => $arItem["PRICE"] ? "money" : "",
                "MEASURES_UNITS"     => $arElements
            ),
            $component,
            array('HIDE_ICONS' => 'Y')
        );
    }
}
