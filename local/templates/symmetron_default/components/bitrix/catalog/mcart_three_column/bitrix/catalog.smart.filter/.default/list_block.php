<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <? if (empty($arItem["DISABLED"]) || !empty($arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_SEARCH"])): ?>
        <div itemid="<?= $arItem["CODE"] ?>"><span><?= $arItem["NAME"] ?></span></div>
    <? endif; ?>
<? endforeach;
