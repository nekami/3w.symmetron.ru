<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!\Bitrix\Main\Loader::includeModule("iblock"))
    return;

$isHaveExpandedRange = false;
foreach ($arResult["ITEMS"] as $key => $arItem)
{
    if (empty($arItem["VALUES"])) {
        unset($arResult["ITEMS"][$key]);
        continue;
    }
    if ($arItem["DISPLAY_TYPE"] != "A" &&
        $arItem["DISPLAY_TYPE"] != "B")
        continue;
    if ($arItem["DISPLAY_EXPANDED"] != "Y")
        continue;
    $isHaveExpandedRange = true;

    /* Get name of si unit */
    $result = \CIBlockProperty::GetList(array(), array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ID" => $arItem["ID"],
    ));
    if ($ar = $result->fetch())
        $arResult["ITEMS"][$key]["UNIT_NAME"] = $ar["HINT"];


    /* Get min\max degree of slider */
    /* Min */
    $min = floor(log($arItem["VALUES"]["MIN"]["VALUE"], 10));
    $sign = -1;//$min <=> 0;
    $min = $min + $sign * (3 - $sign * $min % 3) % 3;
    $arResult["ITEMS"][$key]["VALUES"]["MIN"]["DEGREE"] = $min;

    /* Max */
    $max = floor(log($arItem["VALUES"]["MAX"]["VALUE"], 10));
    $sign = -1;//$max <=> 0;
    $max = $max + $sign * (3 - $sign * $max % 3) % 3;
    $arResult["ITEMS"][$key]["VALUES"]["MAX"]["DEGREE"] = $max;

    /* Get value to display */
    if (empty($arItem["VALUES"]["MIN"]["HTML_VALUE"])
        || empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]))
        continue;
    /* Min */
    $min = floor(log($arItem["VALUES"]["MIN"]["HTML_VALUE"], 10));
    $sign = -1;//$min <=> 0;
    $min = $min + $sign * (3 - $sign * $min % 3) % 3;
    $arResult["ITEMS"][$key]["VALUES"]["MIN"]["HTML_DEGREE"] = $min;
    $arResult["ITEMS"][$key]["VALUES"]["MIN"]["HTML_VALUE_EX"] =
        floor($arItem["VALUES"]["MIN"]["HTML_VALUE"] / 10 ** $min);

    /* Max */
    $max = floor(log($arItem["VALUES"]["MAX"]["HTML_VALUE"], 10));
    $sign = -1;//$min <=> 0;
    $max = $max + $sign * (3 - $sign * $max % 3) % 3;
    $arResult["ITEMS"][$key]["VALUES"]["MAX"]["HTML_DEGREE"] = $max;
    $arResult["ITEMS"][$key]["VALUES"]["MAX"]["HTML_VALUE_EX"] =
        ceil($arItem["VALUES"]["MAX"]["HTML_VALUE"] / 10 ** $max);
}
foreach ($arResult["ITEMS"] as $key => $arItem)
{
    if (empty($arItem["PRICE"]))
        continue;
    $arResult["ITEMS"][$key]["VALUES"]["MIN"]["VALUE"] =
        roundEx($arResult["ITEMS"][$key]["VALUES"]["MIN"]["VALUE"], 2);
    $arResult["ITEMS"][$key]["VALUES"]["MAX"]["VALUE"] =
        roundEx($arResult["ITEMS"][$key]["VALUES"]["MAX"]["VALUE"], 2);
    if (isset($arResult["ITEMS"][$key]["VALUES"]["MIN"]["HTML_VALUE"])) {
        $arResult["ITEMS"][$key]["VALUES"]["MIN"]["HTML_VALUE"] =
            roundEx($arResult["ITEMS"][$key]["VALUES"]["MIN"]["HTML_VALUE"], 2);
        $arResult["ITEMS"][$key]["VALUES"]["MAX"]["HTML_VALUE"] =
            roundEx($arResult["ITEMS"][$key]["VALUES"]["MAX"]["HTML_VALUE"], 2);
    }
}

if ($isHaveExpandedRange && !empty($arParams["SI_UNITS_IBLOCK_ID"]))
{
    $arResult["SI_UNIT"] = [];
    $result = \CIBlockElement::GetList(array(), array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => $arParams["SI_UNITS_IBLOCK_ID"],
    ), false, false, array("NAME", "SORT"));
    while ($ar = $result->fetch()) {
        $arResult["SI_UNIT"][$ar["SORT"]] = array(
            "NAME" => $ar["NAME"],
            "DEGREE" => $ar["SORT"],
        );
    }
    ksort($arResult["SI_UNIT"]);
}



