<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/***
 * Input
 * $iblock_id
 * $section_id
 * $section_url_template
 *
 * Return
 * $sections
 * $level
 ***/

$depth = 1;
$arFilter = array(
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
    "IBLOCK_ID" => $iblock_id,
    "CNT_ACTIVE" => "Y",
);
$arSelect = array(
    "ID",
    "NAME",
    "LEFT_MARGIN",
    "RIGHT_MARGIN",
    "DEPTH_LEVEL",
    "SECTION_PAGE_URL",
    "CODE",
);

if (!empty($section_id)) {
    $arFilter["ID"] = $section_id;

    $rsSections = \CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
    $rsSections->SetUrlTemplates("", $section_url_template);
    $curSection = $rsSections->GetNext();
    if (!is_array($curSection))
        return;
    $level = $curSection["DEPTH_LEVEL"] + 1;

    unset($arFilter["ID"]);
    $arFilter["LEFT_MARGIN"] = $curSection["LEFT_MARGIN"]+1;
    $arFilter["RIGHT_MARGIN"] = $curSection["RIGHT_MARGIN"];
    $depth += $curSection["DEPTH_LEVEL"];
}

//Get SubSections
$arFilter["="."DEPTH_LEVEL"] = $depth;
$arSort = array(
    "left_margin"=>"asc",
);
$sections = array();
$rsSections = \CIBlockSection::GetList($arSort, $arFilter, true, $arSelect);
$rsSections->SetUrlTemplates("", $section_url_template);
while($arSection = $rsSections->GetNext()) {
    $sections["S_".$arSection["ID"]] = array(
        "ID" => $arSection["ID"],
        "NAME" => $arSection["NAME"],
        "ELEMENT_CNT" => $arSection["ELEMENT_CNT"],
        "IS_END" => $arSection["LEFT_MARGIN"] + 1 == $arSection["RIGHT_MARGIN"],
        "URL" => $arSection["SECTION_PAGE_URL"],
    );
}

//Get section Elements
/*$arSort = array(
    "sort"=>"asc",
);
$arFilter = array(
    "ACTIVE" => "Y",
    "SECTION_ACTIVE" => "Y",
    "SECTION_GLOBAL_ACTIVE" => "Y",
    "IBLOCK_ID" => $iblock_id,
    "SECTION_ID" => $section_id,
);
$arSelect = array(
    "ID",
    "NAME",
    "DETAIL_PAGE_URL",
    "CODE",
);
$rsElement = \CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
//$rsSections->SetUrlTemplates($request["ELEMENT_URL"], $request["SECTION_URL"]);
while($arElement = $rsElement->GetNext()) {
    $sections["E_".$arElement["ID"]] = array(
        "ID" => $arElement["ID"],
        "NAME" => $arElement["NAME"],
        "URL" => $arElement["DETAIL_PAGE_URL"],
        /*str_replace(
            array("#SECTION_ID#", "#SECTION_CODE#", "#ELEMENT_ID#", "#ELEMENT_CODE#"),
            array($curSection["ID"], $curSection["CODE"], $arElement["ID"], $arElement["CODE"]),
            $request["ELEMENT_URL"]
        ),* /
    );
} */
