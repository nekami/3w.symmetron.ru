$(document).ready(function()
{
    function openMenu($item, $level) {
        $item.addClass('chosen');
        openLevels($level);
    }
    function closeMenu($item, $level) {
        $item.removeClass('chosen');
        if ($level > 1)
            openLevels($level - 1);
    }
    function openLevels($levelTo) {
        //$('.catalog-lvl').removeClass('open');
        $('.catalog-lvl').removeClass('active-catalog-lvl');
        var breadcrumb = [];
        var i;
        var last;
        for (i = 1; i <= $levelTo; i++) {
            last = $('.catalog-lvl-' + i);
            if (i < $levelTo) {
                var item = $('.catalog-lvl-' + i).find('.catalog__item.chosen > div > a');
                if (item.length > 0)
                    breadcrumb.push([item[0].innerText, item[0].href]);
            }
        }
        last.find('ul > li.chosen').removeClass('chosen');
        $('.catalog-lvl-' + (i - 2)).addClass('active-catalog-lvl');
        while ($('.catalog-lvl-' + i).length > 0)
        {
            $('.catalog-lvl-' + i).remove();
            i++;
        }
        ReloadBreadcrumb(breadcrumb);
        $('.catalog-wrapper').animate({ scrollLeft: last.offset().left + last.width() }, 800);
    }

    function AddLevelNode($level) {
        $('.catalog-wrapper').append("<div class=\"catalog-lvl-"+$level+" catalog-lvl\"></div>")
        return $('.catalog-lvl-'+$level).filter(':first');
    }
    function GetLevelNode($level) {
        if ($('.catalog-lvl-'+$level).length !== 0)
            return $('.catalog-lvl-'+$level).filter(':first');
        return AddLevelNode($level);
    }

    function LoadLevel($nodeLevel, $id) {
        var $nextLevel = GetLevelNode($nodeLevel);

        var data = {
            "ID": $id,
            "IBLOCK_ID": BX.Mcart.catalog.iblockID,
            "SECTION_URL": BX.Mcart.catalog.sectionUrl,
            "ELEMENT_URL": BX.Mcart.catalog.elementUrl
        };
        /* Get object */
        BX.ajax({
            url: BX.Mcart.catalog.ajaxUrl + "ajax.php",
            data: data,
            method: "POST",
            dataType: "html",
            timeout: 30,
            //processData: true,
            scriptsRunFirst: false,
            emulateOnload: false,
            start: true,
            cache: true,
            //async: false,
            onsuccess: function (data) {
                $nextLevel.html(data);
            }
        });
    }
    function ReloadBreadcrumb(breadcrumb) {
        var $node = $('.catalog-breadcrumb');

        var data = {
            "BREADCRUMBS": breadcrumb
        };

        /* Get object */
        BX.ajax({
            url: BX.Mcart.catalog.ajaxUrl + "breadcrumb.php",
            data: data,
            method: "POST",
            dataType: "html",
            timeout: 30,
            //processData: true,
            scriptsRunFirst: false,
            emulateOnload: false,
            start: true,
            cache: true,
            //async: false,
            onsuccess: function (data) {
                $node.html(data);
            }
        });
    }


    function itemClick(e, $this) {
        var $level = $this.attr("element_level");
        if ($level === undefined)
            $level = 1;
        $level = parseInt($level) + 1;

        if($this.hasClass('chosen')) {
            if ($level <= 2)
                openMenu($this, 2);
            else
                closeMenu($this, $level);
            $level--;
        } else if (!$this.children("div").hasClass('endpoint')) {
            $this.parent().parent().find('.catalog__item.chosen').each(function() {
                closeMenu($(this), 0);
            });

            var $id = $this.attr("element_id");
            if ($id === undefined)
                return;

            LoadLevel($level, $id);
            openMenu($this, $level);
        }

        e.stopPropagation();
        if (!$this.children("div").hasClass('endpoint')) {
            e.preventDefault();
            if ($level > 1)
                $level--;
            var $node = GetLevelNode($level);
            history.pushState(null, null, $node.find(".catalog__item.chosen > div > a")[0].href);
        }
    }

    BX.bindDelegate(
        $('.catalog-wrapper')[0],
        "click",
        function(el) {
            return el.classList.contains('catalog__item');
        },
        function(e) {
            itemClick(e, $(this));
        }
    );
    $('.catalog-wrapper').animate({ scrollLeft: $('.catalog-wrapper').offset().left + $('.catalog-wrapper').width() }, 800);
    var i = $('.catalog-lvl').length - 1;
    $('.catalog-lvl-' + i).addClass('active-catalog-lvl');

    //$('.catalog-wrapper')[0].scrollLeft = $('.catalog-wrapper')[0].scrollWidth * 2;

	if ($('.catalog__item')) {
		$('.catalog__item').each(function(){
			if ($(this).children().children('a').innerWidth() >= 360) {
				$(this).addClass('chars-visible')
			}
		})
	}




});