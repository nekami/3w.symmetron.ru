<?
/*
 * @global array $arItem
 * @global array $arResult
 */

use \Bitrix\Main\Localization\Loc;
?>
<div class="add-filter__item range-choice__item <?=$arItem["CODE"]?>__item" itemid="<?=$arItem["CODE"]?>">
    <h4><?=$arItem["NAME"]?><?if (!empty($arItem["PRICE"]))
            echo "(".$arItem["CURRENCIES"][$arItem["VALUES"]["MAX"]["CURRENCY"]].")";?></h4>
    <div class="items-choice__range">
        <?/* Input range values */?>
        <input
            class="range-from"
            type="text"
            name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
            id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
            value="<?echo $arItem["VALUES"]["MIN"]["VALUE"]?>"
        />
        <input
            class="range-to"
            type="text"
            name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
            id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
            value="<?echo $arItem["VALUES"]["MAX"]["VALUE"]?>"
        />
        <?/* Unit names get */
        $names = [];
        if (!empty($arItem["UNIT_NAME"])) {
            if ($arItem["DISPLAY_EXPANDED"] != "Y")
                $names = [array("NAME" => $arItem["UNIT_NAME"])];
            else {
                foreach ($arResult["SI_UNIT"] as $degree => $si) {
                    if ($degree < $arItem["VALUES"]["MIN"]["DEGREE"] ||
                        $degree > $arItem["VALUES"]["MAX"]["DEGREE"])
                        continue;

                    $name = $si;
                    $name["NAME"] .= $arItem["UNIT_NAME"];
                    $names[] = $name;
                }
            }
        }
        ?>
        <?if ($arItem["DISPLAY_EXPANDED"] == "Y" && count($names) > 0):?>
            <input class="expanded" type="hidden">
        <?endif;?>
        <?/* Tag to connect range slider */?>
        <div class="range">
            <div class="range-v <?=$arItem["CODE"]?>__range"></div>
            <?if (count($names) > 0):?>
            <div class="label-from rangeLabel">
                <span class="current-choose invis"></span>
                <?if ($arItem["DISPLAY_EXPANDED"] == "Y"):?>
                <div class="choose-list">
                    <?foreach ($names as $name):?>
                        <span class="choose-item" degree="<?=$name["DEGREE"]?>"><?=$name["NAME"]?></span>
                    <?endforeach;?>
                </div>
                <?endif;?>
            </div>
            <div class="label-to rangeLabel">
                <span class="current-choose invis"></span>
                <?if ($arItem["DISPLAY_EXPANDED"] == "Y"):?>
                    <div class="choose-list">
                        <?foreach ($names as $name):?>
                            <span class="choose-item" degree="<?=$name["DEGREE"]?>"><?=$name["NAME"]?></span>
                        <?endforeach;?>
                    </div>
                <?endif;?>
            </div>
            <?endif;?>
        </div>
    </div>

    <button type="button" class="default-button add-filter__unit"><?=Loc::GetMessage("SYMMETRON_CH_FILTER_BUTTON_ADD")?></button>
</div>