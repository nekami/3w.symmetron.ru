<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

CModule::IncludeModule('mcart.templatetext');
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
    'ID' => $mainId,
    'DISCOUNT_PERCENT_ID' => $mainId . '_dsc_pict',
    'STICKER_ID' => $mainId . '_sticker',
    'BIG_SLIDER_ID' => $mainId . '_big_slider',
    'BIG_IMG_CONT_ID' => $mainId . '_bigimg_cont',
    'SLIDER_CONT_ID' => $mainId . '_slider_cont',
    'OLD_PRICE_ID' => $mainId . '_old_price',
    'PRICE_ID' => $mainId . '_price',
    'DISCOUNT_PRICE_ID' => $mainId . '_price_discount',
    'PRICE_TOTAL' => $mainId . '_price_total',
    'SLIDER_CONT_OF_ID' => $mainId . '_slider_cont_',
    'QUANTITY_ID' => $mainId . '_quantity',
    'QUANTITY_DOWN_ID' => $mainId . '_quant_down',
    'QUANTITY_UP_ID' => $mainId . '_quant_up',
    'QUANTITY_MEASURE' => $mainId . '_quant_measure',
    'QUANTITY_LIMIT' => $mainId . '_quant_limit',
    'BUY_LINK' => $mainId . '_buy_link',
    'ADD_BASKET_LINK' => $mainId . '_add_basket_link',
    'BASKET_ACTIONS_ID' => $mainId . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $mainId . '_not_avail',
    'COMPARE_LINK' => $mainId . '_compare_link',
    'TREE_ID' => $mainId . '_skudiv',
    'DISPLAY_PROP_DIV' => $mainId . '_sku_prop',
    'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
    'OFFER_GROUP' => $mainId . '_set_group_',
    'BASKET_PROP_DIV' => $mainId . '_basket_prop',
    'SUBSCRIBE_LINK' => $mainId . '_subscribe',
    'TABS_ID' => $mainId . '_tabs',
    'TAB_CONTAINERS_ID' => $mainId . '_tab_containers',
    'SMALL_CARD_PANEL_ID' => $mainId . '_small_card_panel',
    'TABS_PANEL_ID' => $mainId . '_tabs_panel'
);

global $USER;
if (USER_ID) {
    $res = CUser::GetList($by = "id", $order = "asc", ['ID' => USER_ID], ['SELECT' => ["UF_FAVS"]])->Fetch();
    $favs = $res["UF_FAVS"];
    $ret = ['success' => true, 'active' => 1];
    $k = empty($favs) ? false : array_search($arResult["ID"], $favs);
    if ($k !== FALSE)
        $is_favs = true;
}
?>
<script>

    $('.anchor').click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('html, body').animate({scrollTop: $(scroll_el).offset().top}, 1800);
        }
        return false;
    });

    $('.card-slider').bxSlider({
        pagerCustom: ($(".card-slider > div").length > 1) ? $('.card-pager') : false,
        /*pager: false,*/
        auto: false,
        touchEnabled: ($(".card-slider > div").length > 1) ? true : false,
    });
</script>
<?

$analogs = array();
foreach ($arResult["PROPERTIES"][$arParams["PROPERTY_CODE_ANALOGS"]]["VALUE"] as $val) {
    if (trim($val))
        $analogs[] = trim($val);
}
$analogs2 = array();
foreach ($arResult["PROPERTIES"][$arParams["PROPERTY_CODE_RELATED"]]["VALUE"] as $val) {
    if (trim($val))
        $analogs2[] = trim($val);
}
?>
<? $actualLink = "https://" . $_SERVER["SERVER_NAME"] . $arResult["DETAIL_PAGE_URL"]; ?>

<? $this->SetViewTarget('link_canonical'); ?>
<link rel="canonical" href="<?= $actualLink ?>"/>
<? $this->EndViewTarget(); ?>

<section class="card" data-id="<?= $arResult['ID'] ?>">
    <div class="wrapper">
        <h1 id="name_prod_<?= $arResult['ID'] ?>"><?= $arResult["PROPERTIES"]["ARTICLE"]["VALUE"] ?> <?= $arResult["PROPERTIES"]["PRODUCERID"]["VALUE"] ?></h1>
        <div class="card-wrapper">
            <div class="card-content">
                <div class="content-header">
                    <ul class="card-nav">
                        <li><a class="anchor"
                               href="#description"><?= Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION') ?></a></li>
                        <li><a class="anchor" href="#spec"><?= Loc::getMessage('CT_BCE_CATALOG_SPECIFICATION') ?></a>
                        </li>
                        <li><a class="anchor" href="#docs"><?= Loc::getMessage('CT_BCE_CATALOG_DOCUMENTATION') ?></a>
                        </li>
                        <? if (!empty($analogs)): ?>
                            <li><a class="anchor"
                                   href="#analogue-card"><?= Loc::getMessage('CT_BCE_CATALOG_ANALOGS') ?></a></li>
                        <? endif; ?>
                        <? if (!empty($analogs2)): ?>
                            <li><a class="anchor" href="#together"><?= Loc::getMessage('CT_BCE_CATALOG_TOGETHER') ?></a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
                <div class="short-description">
                    <div class="section-header">
                        <h2 id="description"><?= Loc::getMessage('CT_BCE_CATALOG_SHORT_DESCRIPTION') ?></h2>
                        <? if (!empty($arParams["PROPERTY_CODE_FOR_SHORT_DISCR_HEAD"])): ?>
                            <div class="section-header__description">
                                <? $countStop = 0; ?>
                                <? foreach ($arParams["PROPERTY_CODE_FOR_SHORT_DISCR_HEAD"] as $code): ?>
                                    <? $property = $arResult["DISPLAY_PROPERTIES"][$code]; ?>
                                    <? if (!empty(trim($property["DISPLAY_VALUE"]))): ?>
                                        <span><?= $property["NAME"] . ": " . $property["DISPLAY_VALUE"]; ?></span>
                                    <? endif; ?>
                                    <?
                                    $countStop++;
                                    if ($countStop == 4)
                                        break;
                                    ?>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                    </div>
                    <div class="section-content">
                        <?

                        if (empty($arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"])) {

                            if (empty($arResult["DETAIL_PICTURE"]["SRC"])) {
                                $arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"][] = array("SRC" => $templateFolder . "/images/no_photo.png");
                                $arResult["PROPERTY_CODE_FOR_GALER"]["SLIDE_DEL"] = true;
                            } else {

                                $arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"][] = array("SRC" => $arResult["DETAIL_PICTURE"]["SRC"]);
                                $arResult["PROPERTY_CODE_FOR_GALER"]["SLIDE_DEL"] = true;
                            }
                        }


                        ?>



                        <? if (!empty($arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"])): ?>

                            <div class="section-content__image">
                                <div class="card-slider">

                                    <? foreach ($arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"] as $pic): ?>

                                        <div class="card-slide"><img src="<?= $pic["SRC"]; ?>"
                                                                     alt="<?= \Mcart\TemplateText\Text::GenerateText($arResult["ID"], "{{код производителя}} {{вендор}} | {{код производителя}} купить на Symmetron.ru, спецификации, схемы {{код производителя}} {{вендор}}") ?>"
                                                                     title="<?= \Mcart\TemplateText\Text::GenerateText($arResult["ID"], "Купить {{код производителя}} {{вендор}} | {{код производителя}} на Symmetron.ru, спецификации, схемы {{код производителя}} {{вендор}}") ?>">
                                        </div>
                                    <? endforeach; ?>
                                </div>
                                <? if ($arResult["PROPERTY_CODE_FOR_GALER"]["SLIDE_DEL"] != true): ?>
                                    <div class="card-pager">
                                        <? foreach ($arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"] as $key => $pic): ?>
                                            <a data-slide-index="<?= $key; ?>" href="" class="card-slide"><img
                                                        src="<?= $pic["SRC"]; ?>"
                                                        alt="<?= \Mcart\TemplateText\Text::GenerateText($arResult["ID"], "{{код производителя}} {{вендор}} | {{код производителя}} купить на Symmetron.ru, спецификации, схемы {{код производителя}} {{вендор}}") ?>"
                                                        title="<?= \Mcart\TemplateText\Text::GenerateText($arResult["ID"], "Купить {{код производителя}} {{вендор}} | {{код производителя}} на Symmetron.ru, спецификации, схемы {{код производителя}} {{вендор}}") ?>"></a>
                                        <? endforeach; ?>
                                    </div>
                                <? endif; ?>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arParams["PROPERTY_CODE_FOR_SHORT_DISCR_BLOCK"])): ?>
                            <div class="section-content__description">
                                <ul>
                                    <? foreach ($arParams["PROPERTY_CODE_FOR_SHORT_DISCR_BLOCK"] as $code): ?>
                                        <? $property = $arResult["DISPLAY_PROPERTIES"][$code]; ?>
                                        <? if (!empty(trim($property["DISPLAY_VALUE"]))): ?>
                                            <li><?= $property["NAME"] . ": " . $property["DISPLAY_VALUE"]; ?></li>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="specs" id="spec">
                    <div class="section-header">
                        <h2>Спецификация</h2>
                    </div>
                    <div class="section-content">
                        <? if (!empty($arResult["SPEC_MAIN"])): ?>
                            <div class="spec-item__group">
                                <?= implode("<br><br>", $arResult["SPEC_MAIN"]); ?>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTY_CODE_SPEC_BLOCK"])): ?>
                            <div class="spec-item__group">
                                <? foreach ($arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_SPEC"] as $code): ?>
                                    <? $arParam = $arResult["PROPERTY_CODE_SPEC_BLOCK"][$code]; ?>

                                    <? if (!empty(trim($arParam["DISPLAY_VALUE"]))): ?>
                                        <div class="spec-item">
                                            <span class="spec-item__name">

                                                <? if (!empty($arResult["PROPERTIES_HINTS"][$arParam["ID"]]["SHORT"])): ?>
                                                    <?= $arResult["PROPERTIES_HINTS"][$arParam["ID"]]["SHORT"] ?>
                                                <? else: ?>
                                                    <?= $arParam["NAME"]; ?>
                                                <? endif ?>

                                                <? if (!empty($arResult["PROPERTIES_HINTS"][$arParam["ID"]]["LONG"])): ?>
                                                    <?= " (" . $arResult["PROPERTIES_HINTS"][$arParam["ID"]]["LONG"] . ")"; ?>
                                                <? elseif (!empty($arParam["HINT"])): ?>
                                                    <?= " (" . $arParam["HINT"] . ")"; ?>
                                                <? endif; ?>
                                            </span>
                                            <span class="spec-item__separator"></span>
                                            <span class="spec-item__value"><?= $arParam["DISPLAY_VALUE"]; ?></span>
                                        </div>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                        <?
                        //echo "<pre>";
                        //print_r($arResult["NEW_CATEGORIES_SPESH"]);
                        //echo "</pre>";
                        ?>
                        <? if (!empty($arResult["NEW_CATEGORIES_SPESH"])): ?>
                            <? foreach ($arResult["NEW_CATEGORIES_SPESH"] as $arCat): ?>
                                <? if (!empty($arCat["RES"])): ?>

                                    <? $flagCategoryGood = false; ?>
                                    <? foreach ($arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_SPEC"] as $code): ?>
                                        <? $arRes = $arCat["RES"][$code]; ?>
                                        <? if (!empty($arRes))
                                            $flagCategoryGood = true;
                                        ?>
                                    <? endforeach; ?>
                                    <? if (!$flagCategoryGood)
                                        continue; ?>

                                    <div class="spec-item__group">
                                        <div class="section-content__header"><?= $arCat["NAME"]; ?></div>
                                        <? foreach ($arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_SPEC"] as $code): ?>
                                            <? $arRes = $arCat["RES"][$code]; ?>
                                            <? if (!empty(trim($arRes["DISPLAY_VALUE"]))): ?>
                                                <div class="spec-item">
                                                    <span class="spec-item__name">

                                                        <? if (!empty($arResult["PROPERTIES_HINTS"][$arRes["ID"]]["SHORT"])): ?>
                                                            <?= $arResult["PROPERTIES_HINTS"][$arRes["ID"]]["SHORT"] ?>
                                                        <? else: ?>
                                                            <?= $arRes["NAME"]; ?>
                                                        <? endif ?>


                                                        <? if (!empty($arResult["PROPERTIES_HINTS"][$arRes["ID"]]["LONG"])): ?>
                                                            <?= " (" . $arResult["PROPERTIES_HINTS"][$arRes["ID"]]["LONG"] . ")"; ?>
                                                        <? elseif (!empty($arRes["HINT"])): ?>
                                                            <?= " (" . $arRes["HINT"] . ")"; ?>
                                                        <? endif; ?>
                                                    </span>

                                                    <span class="spec-item__separator"></span>
                                                    <span class="spec-item__value"><?= $arRes["DISPLAY_VALUE"]; ?></span>
                                                </div>
                                            <? endif; ?>

                                        <? endforeach; ?>
                                    </div>
                                <? endif; ?>
                            <? endforeach; ?>
                        <? endif; ?>

                    </div>
                </div>


                <div class="docs" id="docs">
                    <div class="section-header">
                        <h2>Документация и файлы для загрузки</h2>
                    </div>
                    <div class="section-content">
                        <? foreach ($arResult["PROPERTY_CODE_IBLOCK_DOC"] as $file): ?>
                            <div class="docs-item">
                                <h4 class="docs-item__header">
                                    <a target="_blank"
                                                                 href="<?= $file["PROPERTY_LINK_TO_RES_VALUE"]; ?>"><?= $file["NAME"]; ?></a>
                                </h4>
                                <p><?= $file["PREVIEW_TEXT"]; ?></p>
                                <? /*
                            <a target="_blank" href="<?=$file["PROPERTY_LINK_TO_RES_VALUE"];?>" class="docs-item__file"><?=$file["VALUE"]["name"]." ".round( $file["VALUE"]["size"] / 1024, 2)." КБ";?></a>
                            */ ?>
                            </div>
                        <? endforeach; ?>
                        <? foreach ($arResult["PROPERTY_CODE_IBLOCK_DOC_FILES"] as $file): ?>
                            <div class="docs-item">
                                <h4 class="docs-item__header">
                                    <noindex><a rel="nofollow" target="_blank"
                                                href="<?= $file["VALUE"]["SRC"]; ?>"><?= $file["NAME"]; ?></a></noindex>
                                </h4>
                                <p><?= $file["PREVIEW_TEXT"]; ?></p>
                                <? /*
                            <a target="_blank" href="<?=$file["VALUE"]["SRC"];?>" class="docs-item__file"><?=$file["VALUE"]["ORIGINAL_NAME"]." ".round( $file["VALUE"]["FILE_SIZE"] / 1024, 2)." КБ";?></a>
                            */ ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <? if (empty($arResult["PROPERTY_CODE_IBLOCK_DOC"]) && empty($arResult["PROPERTY_CODE_IBLOCK_DOC_FILES"])): ?>
                        <br>
                    <? endif; ?>
                    <div class="google-search">
                        <noindex><a rel="nofollow" target="_blank"
                                    href="https://www.google.com/search?q=filetype:pdf (<?= $arResult["PROPERTIES"]["ARTICLE"]["VALUE"] ?> <?= $arResult["PROPERTIES"]["PRODUCERID"]["VALUE"] ?>)"><img
                                        src="/images/google.png"/><span>Поиск документации в Google</span>
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 408 408"
                                     style="enable-background:new 0 0 408 408;" xml:space="preserve">
			  <g>
                  <g id="arrow-back">
                      <path d="M408,178.5H96.9L239.7,35.7L204,0L0,204l204,204l35.7-35.7L96.9,229.5H408V178.5z"></path>
                  </g>
              </g>
			  
			  </svg>
                            </a></noindex>
                    </div>
                </div>


            </div>
            <div class="card-sidebar">
                <div class="card-sidebar__content">
                    <? $APPLICATION->IncludeComponent(
                        "mcart:catalog.quantity",
                        "detail",
                        array(
                            "NAME" => $arResult["NAME"],
                            "ELEMENT_ID" => $arResult["ID"],
                            "CATALOG_AVAILABLE" => $arResult["CATALOG_AVAILABLE"],
                            "CATALOG_QUANTITY" => $arResult["CATALOG_QUANTITY"],

                            "PRICES" => $arResult["ITEM_PRICES"],
                            "ITEM_MEASURE_RATIO" =>
                                reset($arResult["ITEM_MEASURE_RATIOS"])
                                    ? reset($arResult["ITEM_MEASURE_RATIOS"])["RATIO"]
                                    : 1,
                            "ITEM_MEASURE_RATIO_ID" => is_array($arResult["ITEM_MEASURE"])
                                ? $arResult["ITEM_MEASURE"]["ID"]
                                : 0,
                            "MIN_QUANTITY" => $arResult["PROPERTIES"]["PURCHQTYLOWEST"]["VALUE"],

                            "FAVORITE" => $is_favs ? "Y" : "N",
                            "CALLBACK_FORM" => "Y",
                            "BASKET_PRODS" => $arResult["BASKET_PRODS"],
                            "DETAIL_PAGE" => $arResult["DETAIL_PAGE_URL"],
                            "CATALOG_CAN_BUY_ZERO" => $arResult["CATALOG_CAN_BUY_ZERO"],
                            "PACK_INFO" => $arResult["PACK_INFO"],
                            "ITEM_MEASURE" => $arResult["ITEM_MEASURE"],
                            "DECLINATION_OBJ" => $arResult["DECLINATION_OBJ"],
                            "BRAND" => $arResult["PROPERTIES"]["PRODUCERID"]["VALUE"],
                            "BREAD" => $arParams["BREAD"],
                        ),
                        $component
                    ); ?>
                </div>
            </div>
        </div>
    </div>

</section>

<? $this->SetViewTarget('otr_s'); ?>
<? if (!empty($arResult["PROPERTY_CODE_OTR_S"])): ?>
    <section class="industry-solutions">
        <div class="wrapper">
            <div class="section-header">
                <span class="current-product"><?= $arResult["NAME"]; ?></span>
                <h2>Отраслевые решения</h2>
            </div>
            <div class="solutions">
                <? foreach ($arResult["PROPERTY_CODE_OTR_S"] as $key => $arOtr): ?>

                    <style>
                        .industry-solutions .solutions .solution.solution-<?=$key;?> {
                            background-image: url(<?=$arOtr["PREVIEW_PICTURE"]["SRC"];?>);
                        }
                    </style>

                    <a href="<?= $arOtr["PROPERTY_LINK_O_VALUE"]; ?>"
                       class="solution solution-<?= $key; ?> <? if (!empty($arOtr["PROPERTY_COLOR_TEXT_VALUE"])): ?>black<? endif; ?>">
                        <span class="solution-header"><?= $arOtr["NAME"]; ?></span>
                        <span class="solution-content"><?= $arOtr["PREVIEW_TEXT"] ?></span>
                    </a>
                <? endforeach; ?>

            </div>
        </div>
    </section>
<? endif; ?>
<? $this->EndViewTarget(); ?>

<?

//$this->setFrameMode(true);
//$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList,
    'ITEM' => array(
        'ID' => $arResult['ID'],
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
        'JS_OFFERS' => $arResult['JS_OFFERS']
    )
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
/*$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);*/
$obName = $templateData['JS_OBJ'] = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
    : $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
    : $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
    : $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
        ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
        : reset($arResult['OFFERS']);
    $showSliderControls = false;

    foreach ($arResult['OFFERS'] as $offer) {
        if ($offer['MORE_PHOTO_COUNT'] > 1) {
            $showSliderControls = true;
            break;
        }
    }
} else {
    $actualItem = $arResult;
    $showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
    'left' => 'product-item-label-left',
    'center' => 'product-item-label-center',
    'right' => 'product-item-label-right',
    'bottom' => 'product-item-label-bottom',
    'middle' => 'product-item-label-middle',
    'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION'])) {
    foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos) {
        $discountPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
    }
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION'])) {
    foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos) {
        $labelPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
    }
}
?>

<?
if (\Bitrix\Main\Loader::includeModule("mcart.templatetext")) {

    $textGen = \Mcart\TemplateText\Text::GenerateText($arResult['ID'],
        htmlspecialcharsBack($arResult["PROPERTIES"]["TEXT_HTML"]["~VALUE"]["TEXT"]));

    if (!empty($textGen)) {
        ?>

        <section class="detail">
            <div class="wrapper">
                <?= $textGen; ?>
            </div>
        </section>
        <?
    }
}
?>
<?
if (!empty($arResult['NEAR_ELEMENTS'])) {
    ?>
    <section class="mcart-left-right">
        <div class="wrapper" style="display: flex; font-size: 16px; font-weight: bold;">
            <div class="mcart-left" style="margin-left: 0px;">
                <? if ($arResult['NEAR_ELEMENTS']["LEFT"]) { ?>
                    <noindex>
                        <a rel="nofollow" target="_blank"
                           style="display: flex; align-items: center;" class="mcart-left__link"
                           href="<?= $arResult["MCART_URL"] ?>/<?= $arResult['NEAR_ELEMENTS']["LEFT"]["CODE"] ?>/">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 55" width="52" height="55">
                                <defs>
                                    <image width="52" height="55" id="left" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA3CAMAAAB0Mpl3AAAAAXNSR0IB2cksfwAAAVNQTFRFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvQmCawAAAHF0Uk5TABOhHopaFcP/0SD2TxbJ1h+M+VURxRqFTMLTJItOwQTxyO0/ceo4czbsOol26crrEjx1xz2H7ohvF+gFpf1coyvb1CUQolco39er+inaIQapI9kmA6DgAlbVqiLcWN7dWSqmp/j89WvlMGg1cuZttjIzSsPqAAACFUlEQVR4nK2W51vCQAzGi6KeE0Vx4kJx494I7r0RxY17r///k0Jzl5NeepXHfHyb90n6u+RawyDDlZNr0dx5dH4q8gtYYVGmhxWX2HlKyxhjnvJfWsWP5K2kPVU+lorCakmrSUveWsrjqmNm1DcIzQ9ScaNNb2Y0NUu9mdGi7q1VJATaQHMLibWrPD/ceAQ7LHVYp6a3YJdDD3BLRXcPaL3o6VPWQU/IaZ38fvF8wKlnUGIwBFoNeoaVvY38nYELuQW4x62pU1UnnnePgjamO58suEmzM87nYEJzPoPYW5D35tbU+S9ufvRMKj3YW6jH6tHNdTbcAk65qXZOyy2LnZvCOZgGBmGca69y3owZkRDhV24U68wqPcbcvMhYWDSlpWUhja+oXatrImUd7u7whpA2iVt4a1ukeHZMaXcP34pw7cdEyvyBKcUP0RVVuw6OREoMvhMJrBU5JlxY62QfOjzVdniGDM+BYRgZei/UrtVLC8NmPcMG7NADNJwwTIqUJLxXfBZdV2rX9TnSKLfQoDqUzivGO7zRd7iGDIFG4hZrEQyvkaEHXLvI8O4PDCUaxGwoGCbQdU/MoT1DBzOvYnhFuGwZRgiXaiv1DOWtnAOXxJA4L2krffBvJG3lg9qEWznNv/ESw0fCxLfy6RklvpUvlAdutuCrLJlb+UZ70gzfu35Lqa38sPMYxuLnV6YUz0n/630DVf9nHrV0IREAAAAASUVORK5CYII="/>
                                </defs>
                                <style>
                                    tspan { white-space:pre }
                                </style>
                                <use id="лево" href="#left" x="0" y="0" />
                            </svg>
                            <span style="color: #008aff;"><?= $arResult['NEAR_ELEMENTS']["LEFT"]["PROPERTY_ARTICLE_VALUE"] ?></span>
                        </a>
                    </noindex>
                    <?
                } ?>
            </div>
            <div class="mcart-right" style="margin-left: auto;">
                <? if ($arResult['NEAR_ELEMENTS']["RIGHT"]) { ?>
                    <noindex>
                        <a rel="nofollow" target="_blank"
                           style="display: flex; align-items: center;" class="mcart-right__link"
                           href="<?= $arResult["MCART_URL"] ?>/<?= $arResult['NEAR_ELEMENTS']["RIGHT"]["CODE"] ?>/">
                            <span style="color: #008aff;"><?= $arResult['NEAR_ELEMENTS']["RIGHT"]["PROPERTY_ARTICLE_VALUE"] ?></span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 55" width="52" height="55">
                                <defs>
                                    <image width="52" height="55" id="right" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA3CAMAAAB0Mpl3AAAAAXNSR0IB2cksfwAAAVNQTFRFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvQmCawAAAHF0Uk5TAHGhAzK2JW3/qQY25topcqsENerfKGiiBTDl2yJrqjzo2Sb1AsXgWPmjKyNV+CHWWf2nINymH9UqXPzdViTe+tegV+4Q1FqldYU6F3OMyRaKOMMVdojtyBHpi8ITb8c9Eofs68qJP/HBTvbTTBrRTx7K7533AAACDUlEQVR4nLWUV1MCQRCE14SHmDArKoKeihhQEMWcA+acc87+/ycJszMLt8vdUeU8dm3X9H27fYwlpqCwiGVNcYkjW8qcUk1zlmVKrnKtojKXp0pLTLVblGpqE1JdvdrToKWmsYmk5paU5GlVedo0mHZM6O0AyedXmDq5SXN2pRW3jlK3wtXTi0cCsKvPg5IvKHf1D+CRanANDqGkYhgaxiMjkDAcQalOsStECUdhl99nnpBcnGF0DKXIuNwVmyAakDA4idKkYteUkaGfaPimFQmNDMPkmlEk7JnNh6FAww5D2mWDoXBfAWilBYZCQqcNhpSQtzI8RwwVrRTePG+lgxiqWil5UUGioWrlFL153sookVe1Mka75oGh30IrjQwttFJkCDQEhqo/28IiMVxKS45lYtgndwXwhBYDaYUArUo9a/RV65Av3oGSviHzbNKeLZBctSht75h4dkHaI8/+gcxzSJ4j7jlG6eTUarYz69nOQYqT5+JS5rkyelyUbVvqyc3t2iq3G/Jc3Mo8Mm70oPLhpptlu+PZ7imbdM+a8Xv+hZuwZx0kr/Debq3ueUDpUfpG8+Em3M8TSHHiplu+Hxud4x7TzhUY7+fZrHPsxbDH/WqSjbG3dzhwjtL0B0jyziXnszfjTpPz9Z2DW3pS/+6tDCk6lCNben7ekQGf8LeCG81Ktoex30Ll9zD2BxgnaNY1xoecAAAAAElFTkSuQmCC"/>
                                </defs>
                                <style>
                                    tspan { white-space:pre }
                                </style>
                                <use id="право" href="#right" x="0" y="0" />
                            </svg>
                        </a>
                    </noindex>
                    <?
                } ?>
            </div>
        </div>
    </section>
    <?
}
?>

<script>
    window.dataLayer = window.dataLayer || [];

    window.dataLayer.push({
        'ecommerce': {
            'detail': {
                'actionField': {'list': 'catalog'},
                'products': [{
                    'name': <?=CUtil::PhpToJSObject($arResult['NAME'])?>,
                    'id': <?=CUtil::PhpToJSObject($arResult['ID'])?>,
                    'price': <?=CUtil::PhpToJSObject($price['PRICE'])?>,
                    'brand':  <?= CUtil::PhpToJSObject($arResult["PROPERTIES"]["PRODUCERID"]["VALUE"]) ?>,
                    'category': <?= CUtil::PhpToJSObject($arParams["BREAD"]) ?>,
                }]
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Product Details',
            'gtm-ee-event-non-interaction': 'True',
        }
    });
</script>

