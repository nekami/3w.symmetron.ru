<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

function __LoadLevel($sections, $level)
{
    if (empty($sections))
        return false;
    ?><div class="catalog-lvl-<?=$level?> catalog-lvl"><?
    require(__DIR__."/catalog_level.php");
    ?></div><?
    return true;
}
?><div class="catalog-wrapper"><?
    $iblock_id = $arParams["IBLOCK_ID"];
    $section_url_template = $arParams["SECTION_URL"];
    $level = 1;

    $sections = array();
    require(__DIR__."/get_level_elements.php");
    do {
        $section_id = array_shift($arResult["CHOSEN_SECTIONS"]);
        if (empty($section_id) || empty($sections["S_".$section_id])) {
            reset($sections);
            $section_id = substr(key($sections), 2); // "S_"
        }
        $arResult["BREADCRUMBS"][] = array(
            $sections["S_".$section_id]["NAME"],
            $sections["S_".$section_id]["URL"]
        );
        $sections["S_".$section_id]["CHOSEN"] = true;
        __LoadLevel($sections, $level++);
        $sections = array();
        require(__DIR__."/get_level_elements.php");
    } while(!empty($arResult["CHOSEN_SECTIONS"]));
    __LoadLevel($sections, $level);

?></div>
<script>
    BX.ready(function () {
        if (BX.Mcart === undefined)
            BX.Mcart = {};
        BX.Mcart["catalog"] = {
            ajaxUrl: "<?=$templateFolder."/"?>",
            iblockID: <?=$arParams["IBLOCK_ID"]?>,
            sectionUrl: "<?=$arParams["SECTION_URL"]?>",
            elementUrl: "<?=$arParams["ELEMENT_URL"]?>"
        };
    })
</script>
