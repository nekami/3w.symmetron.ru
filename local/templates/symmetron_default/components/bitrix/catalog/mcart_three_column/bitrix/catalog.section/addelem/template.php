<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/*if($_GET["sort"] == "PROPERTY_ITEMID" && $_GET["method"] == "asc") {
    echo "<pre>";
    print_r($_GET);
    echo "</pre>";
}*/
//echo "sdfsdfsdfsdfsfdsfdsdfsdfsdf";

/*$APPLICATION->IncludeComponent(
    "bitrix:main.ui.grid",
    "catalog",
    Array(
        "AJAX_ID" => CAjax::GetComponentID('bitrix:voximplant.statistic.detail','.default',''),
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "ALLOW_COLUMNS_SORT" => true,
        "ALLOW_PIN_HEADER" => true,
        "ALLOW_SORT" => true,
        "COLUMNS" => $arResult["GRID"]["NAMES_PROP"],
        "GRID_ID" => "simm_list",
        "NAV_OBJECT" => $arResult["NAV_OBJECT"],
        "PAGE_SIZES" => array(array("NAME"=>"10","VALUE"=>"10"),array("NAME"=>"20","VALUE"=>"20"),array("NAME"=>"50","VALUE"=>"50"),array("NAME"=>"100","VALUE"=>"100"),),
        "ROWS" => $arResult["GRID"]["VAL_PROP"],
        "NAMES_PROD" => $arResult["GRID"]["PROD_NAMES"],
        "SHOW_ACTION_PANEL" => true,
        "SHOW_CHECK_ALL_CHECKBOXES" => false,
        "SHOW_PAGESIZE" => true,
        "SHOW_PAGINATION" => true,
        "SHOW_ROW_CHECKBOXES" => false,
        "SHOW_SELECTED_COUNTER" => false,
        "SORT" => $arResult["SORT"],
        "TOTAL_ROWS_COUNT" => $arResult["ROWS_COUNT"],

        "MODALS" => $arResult["GRID"]["SHOW_IN_MODAL"],

        "PROP_L" => $arParams["PROPERTY_CODE_FAST_SHOW_LEFT"],


        "PROP_R" => $arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"],

        "SECTION_PAGE_URL" => $arResult["SECTION_PAGE_URL"]
    ),
    $component,
    Array(
        'HIDE_ICONS' => 'Y'
    )
);*/

//echo "<pre>";
//print_r($arParams);
//echo "</pre>";

?>


<?
$arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"] = \Bitrix\Main\Web\Json::encode($arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"]);
$arParams["PROPERTY_CODE_FAST_SHOW_LEFT"] = \Bitrix\Main\Web\Json::encode($arParams["PROPERTY_CODE_FAST_SHOW_LEFT"]);
$params_json = \Bitrix\Main\Web\Json::encode($arParams);
?>



    <div style="display: none" id = "PROP_L"><?=$arParams["PROPERTY_CODE_FAST_SHOW_LEFT"];?></div>

    <div style="display: none" id = "PROP_R"><?=$arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"];?></div>

    <div style="display: none" id = "PARARMS"><?=$params_json;?></div>

    <div style="display: none" id = "PROPS_ARR"><?=\Bitrix\Main\Web\Json::encode($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"]);?></div>





    <script>

        var path = "<?=$templateFolder;?>";
        var pagenum = 2;
        var quantelem = "<?=$arParams["PAGE_ELEMENT_COUNT"];?>";
        function modalInfo(id, sec) {

            var propl = document.getElementById("PROP_L").innerHTML;
            var propr = document.getElementById("PROP_R").innerHTML;
            var url = path+"/ajax.php?id=" + id + "&sec=" + sec + "&propl=" + propl + "&propr=" + propr;
            console.log(sec);
            BX.ajax.get(
                url,
                function (data) {
                    //console.log("kkkkkkkkkkkkkkkk");

                    if (document.getElementById("modal_staff")) {
                        document.getElementById("modal_staff").innerHTML = data;
                    }

                    //console.log(data);
                }
            );
        }


        function addElemToList(sort, method, secid) {

            var params = document.getElementById("PARARMS").innerHTML;
            var propsarr = document.getElementById("PROPS_ARR").innerHTML;
            //console.log(propsarr);
            var url = path+"/ajax_list.php?PAGEN_1=" + pagenum + "&sort=" + sort + "&method=" + method + "&quantelem=" + quantelem + "&propsarr="+ propsarr +"&secid=" + secid+ "&params=" + params;
            pagenum++;
            BX.ajax.get(
                url,
                function (data) {
                    //console.log(data);
                    var table = data.split('-+-+');
                    document.getElementById("table_names").innerHTML += table[0];
                    document.getElementById("table_elem").innerHTML += table[1];
                    if(table[2] == quantelem)
                        document.getElementById("element_count").innerHTML -= table[2];
                    else
                        document.getElementById("button_elem_q").remove();
                    //console.log(table);

                    //console.log(data);
                }
            );
        }


        function ajaxUpdateList(sort, method){
            //var params = document.getElementById("arParamList").innerHTML;
            //var arr = [params];
            //var url = path+"/ajax_update.php?sort=" + sort + "&method=" + method + "&ajax=1";
            var url = "?sort=" + sort + "&method=" + method  + "&ajax=1";
            BX.ajax.get(
                url,
                //arr,
                function (data) {
                    //console.log(data);
                    document.getElementById("analogue").innerHTML = data;
                }
            );
            history.replaceState(3, "Title 2", "?sort=" + sort + "&method=" + method);
        }



        $(document).ready(function() {

            $(document).on('click', function (e){
                var searchSelect = $('.search-select')
                var projectSelect = $('.project-select')
                var subscribeBlock = $('.subscribe-wrapper')

                if (!projectSelect.is(e.target) && projectSelect.has(e.target).length === 0 && projectSelect.hasClass('open')) {
                    projectSelect.removeClass('open')
                }

                if (!searchSelect.is(e.target) && searchSelect.has(e.target).length === 0 && searchSelect.hasClass('open')) {
                    searchSelect.removeClass('open')
                }

                if (!subscribeBlock.is(e.target) && subscribeBlock.has(e.target).length === 0 && subscribeBlock.hasClass('open')) {
                    subscribeBlock.removeClass('open')
                }
            })

            $('.close-mini').on('click', function(){
                if ($(this).parent().parent().hasClass('open')) {
                    $(this).parent().parent().removeClass('open')
                }
            })

            $('.mini-card__select').on('click', function(e){
                if ($(this).next('.mini-card').hasClass('open')) {
                    $(this).next('.mini-card').removeClass('open')
                    $(this).next('.mini-card').children().children().slideUp(300)
                } else {
                    $(this).next('.mini-card').addClass('open')
                    $(this).next('.mini-card').children().children().slideDown(300)
                }
            })


            $('.change-all').on('click', function(){
                $('input[type="checkbox"]').prop("checked", false)
            })

            $('.favorite-heart').on('click', function(){
                $(this).parent().parent().remove()
            })

        })




    </script>

    <?
        //echo "<pre>";
        //print_r($arResult);
        //echo "</pre>";

    ?>

    <?/*<section class="analogue" id="analogue">
        <div class="wrapper">
            <div class="section-content">
                <div class="analogue-table__holder">
                    <div class="left-side">
                        <table class="analogue-table" id="table_names">
                            <tr><th><a
                                        <?if($_GET["sort"] == "NAME" && $_GET["method"] == "asc"):?>
                                            href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=<?="NAME";?>&method=desc"
                                        <?else:?>
                                            href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=<?="NAME";?>&method=asc"
                                        <?endif;?>>
                                        Название</a></th></tr>
                            <?foreach ($arResult["GRID"]["PROD_NAMES"] as $name):?>
                                <tr>
                                    <td><?=$name;?></td>
                                </tr>
                            <?endforeach;?>

                        </table>
                    </div>
                    <div class="right-side">
                        <table class="analogue-table" id="table_elem">

                            <tr>

                                <?
                                foreach ($arResult["GRID"]["NAMES_PROP"] as $id => $header) : ?>
                                    <th><a
                                                <?if($_GET["sort"] == "PROPERTY_".$header["id"] && $_GET["method"] == "asc"):?>
                                                    href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=<?="PROPERTY_".$header["id"];?>&method=desc"
                                                <?else:?>
                                                    href="<?=$arResult["SECTION_PAGE_URL"]?>?sort=<?="PROPERTY_".$header["id"];?>&method=asc"
                                                <?endif;?>
                                                >
                                            <?=$header["name"];?></a></th>

                                <?
                                endforeach ?>
                            </tr>
                            <?foreach ($arResult["GRID"]["VAL_PROP"] as $key => $row):?>
                                <tr onclick='modalInfo(<?=$arResult["ITEMS"][$key]["ID"];?>, <?=$arParams["SECTION_ID"];?>);'>
                                    <?foreach ($arResult["GRID"]["NAMES_PROP"] as $key2 => $col):?>
                                        <td>
                                            <?=$row["data"][$col["id"]];?>
                                            <?//=$header["name"];?>
                                        </td>
                                    <?endforeach;?>
                                </tr>

                            <?endforeach ?>


                        </table>
                    </div>
                </div>
                <?
                $num_page = 2;
                if(!empty($_GET["PAGEN_1"]))
                    $num_page = intval($_GET["PAGEN_1"]) + 1;

                $pathSection = str_replace("#SECTION_ID#", $arResult["SECTION_ID"], $arResult["SECTION_URL"])
                ?>
                <?$arCount = $arResult["NAV_RESULT"]->NavRecordCount - count($arResult["ITEMS"]);?>
                <?if($arCount > 0):?>
                    <button id = "button_elem_q" class="show-all" onclick="addElemToList('<?=$_GET["sort"];?>', '<?=$_GET["method"]?>', '<?=$arParams["SECTION_ID"];?>');">Показать ещё (<span id="element_count"><?=$arCount;?></span>)</button>
                <?endif;?>
            </div>
        </div>
    </section>*/?>
    <?
        //echo "<pre>";
        //print_r($arResult["ITEMS"]);
        //echo "</pre>";
    ?>


        <div class="wrapper">
            <div class="section-header">
                <span class="current-product">Процессор Intel Core i7 Coffee Lake</span>
                <h2>Аналогичные позиции</h2>
            </div>

            <div class="section-content">
                <div class="analogue-table__holder">
                    <div class="scroll-table">
                        <table class="analogue-table">
                            <tr>
                                <?if($_GET["sort"] == "NAME"):?>
                                    <?if($_GET["method"] == "" || $_GET["method"] == "desc"):?>
                                        <th onclick="ajaxUpdateList('NAME', 'asc')">Название</th>
                                    <?elseif ($_GET["method"] == "asc"):?>
                                        <th onclick="ajaxUpdateList('NAME', 'desc')">Название</th>
                                    <?endif;?>
                                <?else:?>
                                    <th onclick="ajaxUpdateList('NAME', 'asc')">Название</th>
                                <?endif;?>
                                <?$counterCol = 1;?>
                                <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                                    <?if(!empty(trim($prop)) && !empty(trim($arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"]))):?>
                                        <?$counterCol++;?>
                                        <?if($_GET["sort"] == "PROPERTY_".$prop):?>
                                            <?if($_GET["method"] == "" || $_GET["method"] == "desc"):?>
                                                <th onclick="ajaxUpdateList('<?='PROPERTY_'.$prop?>', 'asc')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                            <?elseif ($_GET["method"] == "asc"):?>
                                                <th onclick="ajaxUpdateList('<?='PROPERTY_'.$prop?>', 'desc')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                            <?endif;?>
                                        <?else:?>
                                            <th onclick="ajaxUpdateList('<?='PROPERTY_'.$prop?>', 'asc')"><?=$arResult["ITEMS"][0]["PROPERTIES"][$prop]["NAME"];?></th>
                                        <?endif;?>
                                    <?endif;?>
                                <?endforeach;?>

                            </tr>
                            <?foreach ($arResult["ITEMS"] as $arItem):?>
                                <tr class="mini-card__select" >
                                    <td><?=$arItem["NAME"];?></td>

                                    <?foreach ($arParams["PROPERTY_CODE"] as $prop):?>
                                        <?
                                        //echo "<pre>";
                                        //print_r($arProp);
                                        //echo "</pre>";
                                        ?>
                                        <?if(!empty($arItem["PROPERTIES"][$prop]) && !empty(trim($arItem["PROPERTIES"][$prop]["NAME"]))):?>
                                            <td><?=$arItem["PROPERTIES"][$prop]["VALUE"];?></td>
                                        <?endif;?>
                                    <?endforeach;?>
                                    <!--<td><span class="analogue-status ordered">Заказано </span></td>-->
                                </tr>
                                <tr class="mini-card">
                                    <td  colspan="<?=$counterCol;?>">
                                        <div class="card-holder">
                                            <div class="mini-cart__wrapper">
                                                <div class="item-img">
                                                    <a href="#"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                                                    <span class="favorite">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                                                        </span>
                                                </div>
                                                <div class="item-info">
                                                    <a href="#" class="item-info__head">
                                                        <?=$arItem["NAME"].", ".$arItem["PREVIEW_TEXT"];?>
                                                    </a>
                                                    <div class="item-info__content">
                                                        <?if($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"]):?>
                                                            <ul>
                                                                <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_LEFT"] as $prop):?>
                                                                    <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                                <?endforeach;?>
                                                            </ul>
                                                        <?endif;?>
                                                        <ul>
                                                            <?if($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"]):?>
                                                                <ul>
                                                                    <?foreach ($arItem["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $prop):?>
                                                                        <li><?=$prop["NAME"].": ".$prop["VALUE"];?></li>
                                                                    <?endforeach;?>
                                                                </ul>
                                                            <?endif;?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="item-more">
                                                    <!-- Если нет в наличии прибавить к элементу ниже класс out -->
                                                    <span class="avalibilty">В наличии 250 единиц</span>
                                                    <div class="wholesale">
                                                        <div class="quantity">
                                                            <span class="quantity-items">1—9 штук </span>
                                                            <span class="quantity-separator"></span>
                                                            <span class="quantity-price">30 000,00 ₽</span>
                                                        </div>
                                                        <div class="quantity">
                                                            <span class="quantity-items">10—99 штук </span>
                                                            <span class="quantity-separator"></span>
                                                            <span class="quantity-price">30 000,00 ₽</span>
                                                        </div>
                                                        <div class="quantity">
                                                            <span class="quantity-items">от 100 штук </span>
                                                            <span class="quantity-separator"></span>
                                                            <span class="quantity-price">30 000,00 ₽</span>
                                                        </div>
                                                    </div>
                                                    <form action="">
                                                        <div class="quantity-num">
                                                            <span class="quantity-button">–</span>
                                                            <input value="1" max="99999">
                                                            <span class="quantity-button">+</span>
                                                        </div>
                                                        <div class="price">150 000, 00 ₽</div>
                                                        <div class="cb"></div>
                                                        <button class="add-to-cart">Добавить в корзину</button>
                                                    </form>
                                                    <button class="sidebar-sample">Запросить образец</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?endforeach;?>


                        </table>
                    </div>
                </div>
                <?$arCount = $arResult["NAV_RESULT"]->NavRecordCount - count($arResult["ITEMS"]);?>
                <?if($arCount > 0):?>
                    <button id = "button_elem_q" class="show-all" onclick="addElemToList('<?=$_GET["sort"];?>', '<?=$_GET["method"]?>', '<?=$arParams["SECTION_ID"];?>');">Показать ещё (<span id="element_count"><?=$arCount;?></span>)</button>
                <?endif;?>
            </div>
        </div>




<div class="mini-card" id="modal_staff">
    <!--<div class="mini-cart__wrapper">
        <div class="item-img">
            <a href="#"><img src="images/itemimg.png" alt=""></a>
            <span class="favorite">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 8 38 34" enable-background="new 0 0 50 50"><path d="M25 39.7l-.6-.5C11.5 28.7 8 25 8 19c0-5 4-9 9-9 4.1 0 6.4 2.3 8 4.1 1.6-1.8 3.9-4.1 8-4.1 5 0 9 4 9 9 0 6-3.5 9.7-16.4 20.2l-.6.5zM17 12c-3.9 0-7 3.1-7 7 0 5.1 3.2 8.5 15 18.1 11.8-9.6 15-13 15-18.1 0-3.9-3.1-7-7-7-3.5 0-5.4 2.1-6.9 3.8L25 17.1l-1.1-1.3C22.4 14.1 20.5 12 17 12z"></path></svg>
                </span>
        </div>
        <div class="item-info">
            <a href="#" class="item-info__head">
                A3977SLPTR-T, DMOS-драйвер шагового двигателя с переводчиком встроенныуй в плату [TSSOP-28]
            </a>
            <div class="item-info__content">
                <ul>
                    <li>Артикул: B25667-C3497-A375</li>
                    <li>PartNumber: B25667C3497A375</li>
                    <li>Ном. номер: 9000145214</li>
                    <li>Производитель: intel</li>
                </ul>
                <ul>
                    <li>Признак соответствия ROHS: да</li>
                    <li>Кратность: 3</li>
                    <li>MOQ: 9 </li>
                    <li>Упаковка: есть</li>
                    <li>Тип упаковки: катушка</li>
                    <li>Корпус: TO-220</li>
                    <li>Температурный диапазон: 35</li>
                    <li>Статус: активен</li>
                </ul>
            </div>
        </div>
        <div class="item-more">-->
            <!-- Если нет в наличии прибавить к элементу ниже класс out -->
            <!--<span class="avalibilty">В наличии 250 единиц</span>
            <div class="wholesale">
                <div class="quantity">
                    <span class="quantity-items">1—9 штук </span>
                    <span class="quantity-separator"></span>
                    <span class="quantity-price">30 000,00 ₽</span>
                </div>
                <div class="quantity">
                    <span class="quantity-items">10—99 штук </span>
                    <span class="quantity-separator"></span>
                    <span class="quantity-price">30 000,00 ₽</span>
                </div>
                <div class="quantity">
                    <span class="quantity-items">от 100 штук </span>
                    <span class="quantity-separator"></span>
                    <span class="quantity-price">30 000,00 ₽</span>
                </div>
            </div>
            <form action="">
                <div class="quantity-num">
                    <span class="quantity-button">–</span>
                    <input value="1" max="99999">
                    <span class="quantity-button">+</span>
                </div>
                <div class="price">150 000, 00 ₽</div>
                <div class="cb"></div>
                <button class="add-to-cart">Добавить в корзину</button>
            </form>
            <button class="sidebar-sample">Запросить образец</button>
        </div>
        <div class="close-mini"></div>
    </div>-->

</div>

<?/*
//echo "sfdfsdsfdsfdsdfsdf";



$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$templateLibrary = array('popup', 'ajax', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_BUY');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCS_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_FEW');

$arParams['MESS_BTN_LAZY_LOAD'] = $arParams['MESS_BTN_LAZY_LOAD'] ?: Loc::getMessage('CT_BCS_CATALOG_MESS_BTN_LAZY_LOAD');

$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'LABEL_POSITION_CLASS' => $labelPositionClass,
	'DISCOUNT_POSITION_CLASS' => $discountPositionClass,
	'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
	'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
	'~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	'~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-'.$navParams['NavNum'];

if ($showTopPager)
{
	?>
	<div data-pagination-num="<?=$navParams['NavNum']?>">
		<!-- pagination-container -->
		<?=$arResult['NAV_STRING']?>
		<!-- pagination-container -->
	</div>
	<?
}

if ($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y')
{
	?>
	<div class="bx-section-desc bx-<?=$arParams['TEMPLATE_THEME']?>">
		<p class="bx-section-desc-post"><?=$arResult['DESCRIPTION']?></p>
	</div>
	<?
}
?>

<div class="catalog-section bx-<?=$arParams['TEMPLATE_THEME']?>" data-entity="<?=$containerName?>">
	<?
	if (!empty($arResult['ITEMS']) && !empty($arResult['ITEM_ROWS']))
	{
		$areaIds = array();

		foreach ($arResult['ITEMS'] as $item)
		{
			$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
			$areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
			$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
		}
		?>
		<!-- items-container -->
		<?
		foreach ($arResult['ITEM_ROWS'] as $rowData)
		{
			$rowItems = array_splice($arResult['ITEMS'], 0, $rowData['COUNT']);
			?>
			<div class="row <?=$rowData['CLASS']?>" data-entity="items-row">
				<?
				switch ($rowData['VARIANT'])
				{
					case 0:
						?>
						<div class="col-xs-12 product-item-small-card">
							<div class="row">
								<div class="col-xs-12 product-item-big-card">
									<div class="row">
										<div class="col-md-12">
											<?
											$item = reset($rowItems);
											$APPLICATION->IncludeComponent(
												'bitrix:catalog.item',
												'',
												array(
													'RESULT' => array(
														'ITEM' => $item,
														'AREA_ID' => $areaIds[$item['ID']],
														'TYPE' => $rowData['TYPE'],
														'BIG_LABEL' => 'N',
														'BIG_DISCOUNT_PERCENT' => 'N',
														'BIG_BUTTONS' => 'N',
														'SCALABLE' => 'N'
													),
													'PARAMS' => $generalParams
														+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
												),
												$component,
												array('HIDE_ICONS' => 'Y')
											);
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?
						break;

					case 1:
						?>
						<div class="col-xs-12 product-item-small-card">
							<div class="row">
								<?
								foreach ($rowItems as $item)
								{
									?>
									<div class="col-xs-6 product-item-big-card">
										<div class="row">
											<div class="col-md-12">
												<?
												$APPLICATION->IncludeComponent(
													'bitrix:catalog.item',
													'',
													array(
														'RESULT' => array(
															'ITEM' => $item,
															'AREA_ID' => $areaIds[$item['ID']],
															'TYPE' => $rowData['TYPE'],
															'BIG_LABEL' => 'N',
															'BIG_DISCOUNT_PERCENT' => 'N',
															'BIG_BUTTONS' => 'N',
															'SCALABLE' => 'N'
														),
														'PARAMS' => $generalParams
															+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
													),
													$component,
													array('HIDE_ICONS' => 'Y')
												);
												?>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
						break;

					case 2:
						?>
						<div class="col-xs-12 product-item-small-card">
							<div class="row">
								<?
								foreach ($rowItems as $item)
								{
									?>
									<div class="col-sm-4 product-item-big-card">
										<div class="row">
											<div class="col-md-12">
												<?
												$APPLICATION->IncludeComponent(
													'bitrix:catalog.item',
													'',
													array(
														'RESULT' => array(
															'ITEM' => $item,
															'AREA_ID' => $areaIds[$item['ID']],
															'TYPE' => $rowData['TYPE'],
															'BIG_LABEL' => 'N',
															'BIG_DISCOUNT_PERCENT' => 'N',
															'BIG_BUTTONS' => 'Y',
															'SCALABLE' => 'N'
														),
														'PARAMS' => $generalParams
															+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
													),
													$component,
													array('HIDE_ICONS' => 'Y')
												);
												?>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
						break;

					case 3:
						?>
						<div class="col-xs-12 product-item-small-card">
							<div class="row">
								<?
								foreach ($rowItems as $item)
								{
									?>
									<div class="col-xs-6 col-md-3">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $item,
													'AREA_ID' => $areaIds[$item['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N',
													'SCALABLE' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
						break;

					case 4:
						$rowItemsCount = count($rowItems);
						?>
						<div class="col-sm-6 product-item-big-card">
							<div class="row">
								<div class="col-md-12">
									<?
									$item = array_shift($rowItems);
									$APPLICATION->IncludeComponent(
										'bitrix:catalog.item',
										'',
										array(
											'RESULT' => array(
												'ITEM' => $item,
												'AREA_ID' => $areaIds[$item['ID']],
												'TYPE' => $rowData['TYPE'],
												'BIG_LABEL' => 'N',
												'BIG_DISCOUNT_PERCENT' => 'N',
												'BIG_BUTTONS' => 'Y',
												'SCALABLE' => 'Y'
											),
											'PARAMS' => $generalParams
												+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
										),
										$component,
										array('HIDE_ICONS' => 'Y')
									);
									unset($item);
									?>
								</div>
							</div>
						</div>
						<div class="col-sm-6 product-item-small-card">
							<div class="row">
								<?
								for ($i = 0; $i < $rowItemsCount - 1; $i++)
								{
									?>
									<div class="col-xs-6">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $rowItems[$i],
													'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N',
													'SCALABLE' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
						break;

					case 5:
						$rowItemsCount = count($rowItems);
						?>
						<div class="col-sm-6 product-item-small-card">
							<div class="row">
								<?
								for ($i = 0; $i < $rowItemsCount - 1; $i++)
								{
									?>
									<div class="col-xs-6">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $rowItems[$i],
													'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N',
													'SCALABLE' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<div class="col-sm-6 product-item-big-card">
							<div class="row">
								<div class="col-md-12">
									<?
									$item = end($rowItems);
									$APPLICATION->IncludeComponent(
										'bitrix:catalog.item',
										'',
										array(
											'RESULT' => array(
												'ITEM' => $item,
												'AREA_ID' => $areaIds[$item['ID']],
												'TYPE' => $rowData['TYPE'],
												'BIG_LABEL' => 'N',
												'BIG_DISCOUNT_PERCENT' => 'N',
												'BIG_BUTTONS' => 'Y',
												'SCALABLE' => 'Y'
											),
											'PARAMS' => $generalParams
												+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
										),
										$component,
										array('HIDE_ICONS' => 'Y')
									);
									unset($item);
									?>
								</div>
							</div>
						</div>
						<?
						break;

					case 6:
						?>
						<div class="col-xs-12 product-item-small-card">
							<div class="row">
								<?
								foreach ($rowItems as $item)
								{
									?>
									<div class="col-xs-6 col-sm-4 col-md-2">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $item,
													'AREA_ID' => $areaIds[$item['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N',
													'SCALABLE' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
						break;

					case 7:
						$rowItemsCount = count($rowItems);
						?>
						<div class="col-sm-6 product-item-big-card">
							<div class="row">
								<div class="col-md-12">
									<?
									$item = array_shift($rowItems);
									$APPLICATION->IncludeComponent(
										'bitrix:catalog.item',
										'',
										array(
											'RESULT' => array(
												'ITEM' => $item,
												'AREA_ID' => $areaIds[$item['ID']],
												'TYPE' => $rowData['TYPE'],
												'BIG_LABEL' => 'N',
												'BIG_DISCOUNT_PERCENT' => 'N',
												'BIG_BUTTONS' => 'Y',
												'SCALABLE' => 'Y'
											),
											'PARAMS' => $generalParams
												+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
										),
										$component,
										array('HIDE_ICONS' => 'Y')
									);
									unset($item);
									?>
								</div>
							</div>
						</div>
						<div class="col-sm-6 product-item-small-card">
							<div class="row">
								<?
								for ($i = 0; $i < $rowItemsCount - 1; $i++)
								{
									?>
									<div class="col-xs-6 col-md-4">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $rowItems[$i],
													'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N',
													'SCALABLE' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<?
						break;

					case 8:
						$rowItemsCount = count($rowItems);
						?>
						<div class="col-sm-6 product-item-small-card">
							<div class="row">
								<?
								for ($i = 0; $i < $rowItemsCount - 1; $i++)
								{
									?>
									<div class="col-xs-6 col-md-4">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $rowItems[$i],
													'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N',
													'SCALABLE' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>
							</div>
						</div>
						<div class="col-sm-6 product-item-big-card">
							<div class="row">
								<div class="col-md-12">
									<?
									$item = end($rowItems);
									$APPLICATION->IncludeComponent(
										'bitrix:catalog.item',
										'',
										array(
											'RESULT' => array(
												'ITEM' => $item,
												'AREA_ID' => $areaIds[$item['ID']],
												'TYPE' => $rowData['TYPE'],
												'BIG_LABEL' => 'N',
												'BIG_DISCOUNT_PERCENT' => 'N',
												'BIG_BUTTONS' => 'Y',
												'SCALABLE' => 'Y'
											),
											'PARAMS' => $generalParams
												+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
										),
										$component,
										array('HIDE_ICONS' => 'Y')
									);
									unset($item);
									?>
								</div>
							</div>
						</div>
						<?
						break;

					case 9:
						?>
						<div class="col-xs-12">
							<div class="row">
								<?
								foreach ($rowItems as $item)
								{
									?>
									<div class="col-xs-12 product-item-line-card">
										<?
										$APPLICATION->IncludeComponent(
											'bitrix:catalog.item',
											'',
											array(
												'RESULT' => array(
													'ITEM' => $item,
													'AREA_ID' => $areaIds[$item['ID']],
													'TYPE' => $rowData['TYPE'],
													'BIG_LABEL' => 'N',
													'BIG_DISCOUNT_PERCENT' => 'N',
													'BIG_BUTTONS' => 'N'
												),
												'PARAMS' => $generalParams
													+ array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
										?>
									</div>
									<?
								}
								?>

							</div>
						</div>
						<?
						break;
				}
				?>
			</div>
			<?
		}
		unset($generalParams, $rowItems);
		?>
		<!-- items-container -->
		<?
	}
	else
	{
		// load css for bigData/deferred load
		$APPLICATION->IncludeComponent(
			'bitrix:catalog.item',
			'',
			array(),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
	}
	?>
</div>
<?
if ($showLazyLoad)
{
	?>
	<div class="row bx-<?=$arParams['TEMPLATE_THEME']?>">
		<div class="btn btn-default btn-lg center-block" style="margin: 15px;"
			data-use="show-more-<?=$navParams['NavNum']?>">
			<?=$arParams['MESS_BTN_LAZY_LOAD']?>
		</div>
	</div>
	<?
}

if ($showBottomPager)
{
	?>
	<div data-pagination-num="<?=$navParams['NavNum']?>">
		<!-- pagination-container -->
		<?=$arResult['NAV_STRING']?>
		<!-- pagination-container -->
	</div>
	<?
}

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'catalog.section');
$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');
?>
<script>
	BX.message({
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BASKET_URL: '<?=$arParams['BASKET_URL']?>',
		ADD_TO_BASKET_OK: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_MESSAGE_SEND_PROPS: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCS_CATALOG_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		BTN_MESSAGE_LAZY_LOAD: '<?=CUtil::JSEscape($arParams['MESS_BTN_LAZY_LOAD'])?>',
		BTN_MESSAGE_LAZY_LOAD_WAITER: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_LAZY_LOAD_WAITER')?>',
		SITE_ID: '<?=CUtil::JSEscape(SITE_ID)?>'
	});
	var <?=$obName?> = new JCCatalogSectionComponent({
		siteId: '<?=CUtil::JSEscape(SITE_ID)?>',
		componentPath: '<?=CUtil::JSEscape($componentPath)?>',
		navParams: <?=CUtil::PhpToJSObject($navParams)?>,
		deferredLoad: false, // enable it for deferred load
		initiallyShowHeader: '<?=!empty($arResult['ITEM_ROWS'])?>',
		bigData: <?=CUtil::PhpToJSObject($arResult['BIG_DATA'])?>,
		lazyLoad: !!'<?=$showLazyLoad?>',
		loadOnScroll: !!'<?=($arParams['LOAD_ON_SCROLL'] === 'Y')?>',
		template: '<?=CUtil::JSEscape($signedTemplate)?>',
		ajaxId: '<?=CUtil::JSEscape($arParams['AJAX_ID'])?>',
		parameters: '<?=CUtil::JSEscape($signedParams)?>',
		container: '<?=$containerName?>'
	});
</script>

*/?>