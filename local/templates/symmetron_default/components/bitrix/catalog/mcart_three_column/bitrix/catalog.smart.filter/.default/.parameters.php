<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"SI_UNITS_IBLOCK_ID" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_SI_UNITS_IBLOCK_ID"),
		"TYPE" => "STRING",
	),
);
