<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
if (!\Bitrix\Main\Loader::includeModule("iblock")) {
    return;
}

/* Types of property displayed to template type */
$types = [
    "A" => "range",
    "B" => "range",
    "P" => "radio",
    "R" => "radio",
    "K" => "radio",
    "F" => "checkbox",
    "G" => "checkbox",
    "H" => "checkbox",
];

/* Set display type to price filter */
if (!empty($arResult["ITEMS"]["BASE"])) {
    $arResult["ITEMS"]["BASE"]["DISPLAY_TYPE"] = "A";
}

/* Clear waste properties data */
$arItems = [];
$arResult["FILTERED"] = false;

$propertyOnSearch = $arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_SEARCH"];
$arHints = $arParams["PROPERTIES_HINTS"];

//$arHints = [];
//if(CModule::IncludeModule("mcart.sortcatalogsm")){
    //$arHints = Mcart\SortCatalogSM\Helper::getHints($arResult["ID"]);
//}

if(!empty($propertyOnSearch)){
    $propertyOnSearchIDs = array_keys($propertyOnSearch);
}

foreach ($arResult["ITEMS"] as &$arItem) {

    if(!empty($arHints[$arItem["ID"]]["SHORT"])){
        $arItem["NAME"] = $arHints[$arItem["ID"]]["SHORT"];
    }

    if(!empty($propertyOnSearch)) {
        if (!in_array($arItem["ID"], $propertyOnSearchIDs)) {
            continue;
        } else {
            $arItem["DISABLED"] = "";
        }
    }
    
    $arItem["TEMPLATE_TYPE"] = "checkbox";
    if (isset($types[$arItem["DISPLAY_TYPE"]])) {
        $arItem["TEMPLATE_TYPE"] = $types[$arItem["DISPLAY_TYPE"]];
    }

    if ($arItem["DISPLAY_TYPE"] == "A" || $arItem["DISPLAY_TYPE"] == "B") {
        if (!isset($arItem["VALUES"]["MIN"]["VALUE"]) || !isset($arItem["VALUES"]["MAX"]["VALUE"])) {
            unset($arItem["VALUES"]);
        }
        if (isset($arItem["VALUES"]["MIN"]["HTML_VALUE"]) || isset($arItem["VALUES"]["MAX"]["HTML_VALUE"])) {
            $arItem["FILTERED"] = true;
        }
    } else {
        $disable = true;
        foreach ($arItem["VALUES"] as $key => $row) {
            if (!empty($row["CHECKED"])) {
                $arItem["FILTERED"] = true;
                $disable = false;
            } elseif (empty($row["DISABLED"])) {
                $disable = false;
            }
        }
        if ($disable) {
            $arItem["DISABLED"] = true;
        }
    }
    if (!empty($arItem["VALUES"]) || !empty($propertyOnSearch)) {
        $arItems[$arItem["CODE"]] = &$arItem;
    }
    if ($arItem["FILTERED"])
        $arResult["FILTERED"] = true;
}

$newArItems = [];
foreach ($propertyOnSearch as $code) {
    if(!empty($arItems[$code]))
        $newArItems[$code] = $arItems[$code];
}

if(!empty($newArItems))
    $arItems = $newArItems;

$arResult["ITEMS"] = $arItems;
if(empty($propertyOnSearch))
    ksort($arResult["ITEMS"]);