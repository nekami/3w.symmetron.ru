<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//echo "<pre>";
//print_r($arParams);
//print_r($arResult);
//echo "</pre>";
foreach ($arResult["ITEMS"] as $key => $arItem) {
    if (!empty($arParams["PROPERTY_CODE_FAST_SHOW_LEFT"])) {
        $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_LEFT"] = array();
        foreach ($arParams["PROPERTY_CODE_FAST_SHOW_LEFT"] as $code) {
            if (!empty($code)) {

                $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_LEFT"][] = $arItem["PROPERTIES"][$code];

            }

        }
    }
}


foreach ($arResult["ITEMS"] as $key => $arItem) {
    if (!empty($arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"])) {
        $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_RIGHT"] = array();
        foreach ($arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"] as $code) {
            if (!empty($code)) {

                $arResult["ITEMS"][$key]["PROPERTY_CODE_FAST_SHOW_RIGHT"][] = $arItem["PROPERTIES"][$code];

            }

        }
    }
}

    /*if (!empty($arResult["ITEMS"])) {
        foreach ($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"] as $prop) {

            if (!empty($prop)) {
                $props = $arResult["ITEMS"][0]["PROPERTIES"];
                if (!empty($props[$prop]["NAME"]))
                    $arResult["GRID"]["NAMES_PROP"][] = array('id' => $props[$prop]["CODE"], 'name' => $props[$prop]["NAME"], 'sort' => $props[$prop]["SORT"], 'default' => true);

            }
        }


        foreach ($arResult["ITEMS"] as $item) {
            $arResult["GRID"]["PROD_NAMES"][] = $item["NAME"];
            $arValProps = array();

            foreach ($arResult["ORIGINAL_PARAMETERS"]["PROPERTY_CODE"] as $prop) {
                if (!empty($prop)) {
                    $arValProps["data"][$prop] = $item["DISPLAY_PROPERTIES"][$prop]["VALUE"];
                }
            }

            $arResult["GRID"]["VAL_PROP"][] = $arValProps;


            //echo "<pre>";
            //print_r($item);
            //echo "</pre>";


            $arResult["GRID"]["SHOW_IN_MODAL"][] = array("SEC_ID" => $item["IBLOCK_SECTION_ID"], "ELEM_ID" => $item["ID"]);

        }

    }*/

