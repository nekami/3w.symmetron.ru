function SymmetronCatalogFilter(ajaxURL) {
    this.ajaxURL = ajaxURL;
    this.form = null;
    this.timer = null;
    this.cacheKey = '';
    this.cache = [];
    this.popups = [];

    this.filterNode = $(".parametric").first();
    this.formNode = this.filterNode.find("form").first();
    this.filterModalNode = this.filterNode.find(".catalog-filter__list").first();

    this.addFilterNode = this.filterNode.find(".add-filter").first();
    this.storageNode = this.filterNode.find(".storage-filter").first();
    this.bind();
}

SymmetronCatalogFilter.prototype.bind = function () {
    var _this = this;

    BX.ready(function () {

        _this.addFilterNode.children("button").on("click", function () {
            _this.togglePropertiesList();
        });

        /* Open event binding */
        _this.addFilterNode.children(".list__wrapper").on("click", "div", function () {
            _this.openPropertiesFilterFromList(this);
            SymmetronFilterFormObject.searchFilerVisible()
        });

        _this.addFilterNode.on("click", ".close-thisFilter", function () {
            //_this.closePropertiesFilter();
            _this.togglePropertiesList();
        });

        _this.formNode.on("click", ".filter-item__add, .filter-item__range", function (event) {
            _this.togglePropertiesFilterFromForm($(this).parent());
            SymmetronFilterFormObject.searchFilerVisible()
        });
        /* END: Open event binding */

        $("body").on("click", function (event) {
            var target = $(event.target)
            if (target.parents(".add-filter").length === 0
                && !target.hasClass("filter-item__add")
                && !target.parent().hasClass("filter-item__add")
                && !target.hasClass("filter-item__range")
                && !target.parent().hasClass("filter-item__range")) {
                _this.closeAllPropertiesModal()
            }
        });

        _this.filterModalNode.find("button").on("click", function () {
            _this.updatePropertyFilter();
        });

	_this.filterModalNode.find(".range-option").on("click", function () {
            _this.updateElementCount();
        });

        _this.filterModalNode.on("change", "input", function () {
            _this.updateElementCount();
        });

        _this.formNode.on("click", "div.filter-item__delete", function () {
            _this.deletePropertyFilter($(this).parent());
        });

        _this.formNode.on("click", "div.option-delete", function () {
            _this.deletePropertyFilterOption($(this).parent());
        });

        $("body").on("click", "div.remove-all", function () {
            _this.deleteAllPropertyFilter();
        });
    })
};

SymmetronCatalogFilter.prototype.closeAllPropertiesModal = function () {
    this.addFilterNode.children(".list__wrapper").removeClass("open")
    if (!this.filterModalNode.hasClass("hide")) {
        this.closePropertiesFilter()
    }
}

SymmetronCatalogFilter.prototype.togglePropertiesList = function () {
    this.closePropertiesFilter()
    this.addFilterNode.children(".list__wrapper").toggleClass("open")
}

SymmetronCatalogFilter.prototype.toggleDeleteAllButton = function () {
    if (this.formNode.children(".filter-item").length > 0) {
        this.filterNode.children("div.remove-all").removeClass("close")
    } else {
        this.filterNode.children("div.remove-all").addClass("close")
    }
}

SymmetronCatalogFilter.prototype.togglePropertiesFilterFromForm = function (element) {
    var propCode = element.attr("itemid")
    var currentProp = this.filterModalNode.children(".list__wrapper").attr("itemid")
    if (currentProp.length > 0 && currentProp != propCode) {
        this.closePropertiesFilter()
    }

    if (this.filterModalNode.hasClass("hide")) {
        this.openPropertiesFilterFromForm(element)
    } else {
        this.closePropertiesFilter()
    }
}

SymmetronCatalogFilter.prototype.closePropertiesFilter = function () {
    this.filterModalNode.addClass("hide")

    var propCode = this.filterModalNode.children(".list__wrapper").attr("itemid")
    if (propCode !== undefined && propCode.length > 0) {
        this.storageNode.children(".list__wrapper[itemid='" + propCode + "']").replaceWith(
            this.filterModalNode.children(".list__wrapper").first().clone()
        );
    }
}

SymmetronCatalogFilter.prototype.openPropertiesFilterFromList = function (listPoint) {
    var propCode = $(listPoint).attr("itemid")
    var propName = $(listPoint).html()
    this.openPropertiesFilter(propCode, propName)
}

SymmetronCatalogFilter.prototype.openPropertiesFilterFromForm = function (element) {
    var propCode = $(element).attr("itemid")
    var propName = this.addFilterNode.find(".list__wrapper > div[itemid='" + propCode + "']").first().html()
    this.openPropertiesFilter(propCode, propName)
}

SymmetronCatalogFilter.prototype.openPropertiesFilter = function (propCode, propName) {
    this.addFilterNode.children(".list__wrapper").removeClass("open")
    this.filterModalNode.children(".list__wrapper").replaceWith(
        this.storageNode.children(".list__wrapper[itemid='" + propCode + "']").first().clone()
    );
    this.updateElementCount(true);
    this.filterModalNode.children(".catalog-filter__header").html(propName)
    this.addFilterNode.children(".catalog-filter__list").removeClass("hide")
}

SymmetronCatalogFilter.prototype.updateElementCount = function () {
    this.reload("count", false)
}

SymmetronCatalogFilter.prototype.updatePropertyFilter = function () {
    this.filterModalNode.addClass("hide")
    this.reload("all", false)
}

SymmetronCatalogFilter.prototype.deleteAllPropertyFilter = function () {
    this.closeAllPropertiesModal()
    this.formNode.children(".filter-item").remove()
    this.toggleDeleteAllButton()
    this.reload("all", true)
}

SymmetronCatalogFilter.prototype.deletePropertyFilter = function (element) {
    this.closeAllPropertiesModal()
    $(element).remove()
    this.toggleDeleteAllButton()
    this.reload("all", true)
}

SymmetronCatalogFilter.prototype.deletePropertyFilterOption = function (element) {
    this.closeAllPropertiesModal()
    var count = element.siblings(".filter-item__option").length
    var more = element.siblings(".filter-item__addit").children("span").first()
    if (count > 0) {
        if (more.length > 0) {
            more.html(count - 3)
        }
        $(element).remove()
        this.reload("all", true)
    } else {
        this.deletePropertyFilter($(element).parent())
    }
}

SymmetronCatalogFilter.prototype.getFilterValues = function (mode, override) {
    var values = [];
    if (mode === "updateUrl") {
        values = [
            {name: 'set_filter', value: ''},
        ];
    } else if (mode === "updateCatalog") {
        values = [
            //{name: 'set_filter', value: ''},
            //{name: 'action', value: 'showMore'},
        ];
    } else {
        values = [
            {name: 'ajax', value: 'y'},
            {name: 'mode', value: mode},
        ];
    }

    var i = values.length;
    /* Get hidden inputs */
    this.formNode.children('input').each(function () {
        values[i++] = {name: $(this).attr("name"), value: $(this).val()};
    });

    var emptyFilterLenght = values.length

    /* Get filter inputs */
    this.formNode.children('div').find('input').each(function () {
        if ($(this).parents(".filter-item." + override + "__filter").length === 0) {
            values[i++] = {name: $(this).attr("name"), value: $(this).val()};
        }
    });
    if (override !== undefined && override.length > 0) {
        this.filterModalNode.find('input:checked, input[type=text], input[type=hidden], select:selected').each(function () {
            if ($(this).attr("name") !== undefined && $(this).val().length > 0) {
                values[i++] = {name: $(this).attr("name"), value: $(this).val()};
            }
        });
    }

    if (mode === "updateCatalog" && values.length != emptyFilterLenght) {
        values[values.length] = {name: 'set_filter', value: ''}
    }

    return values
}

SymmetronCatalogFilter.prototype.upgradeFilterCacheKey = function (values) {
    for (var i = 0; i < values.length; i++)
        this.cacheKey += values[i].name + ':' + values[i].value + '|';
}

SymmetronCatalogFilter.prototype.reload = function (mode, reload) {
    var code = "", values = [];
    if (this.cacheKey !== '') {
        //BX.Mcart.modals.result_off.open(["Error", "To many clicks"]);

        //Postprone backend query
        if (!!this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(BX.delegate(function () {
            this.reload(mode, reload);
        }, this), 1000);
        return;
    }

    if (this.formNode) {
        /* Start update content data */
        this.cacheKey = '|';
        /* Get filter values */
        if (!reload) {
            code = this.filterModalNode.children(".list__wrapper").attr("itemid")
        }
        values = this.getFilterValues(mode, code);
        this.upgradeFilterCacheKey(values)

        /* Get data to update */
        if (this.cache[this.cacheKey]) {
            result = this.cache[this.cacheKey];
            result.MODE = mode;
            this.postHandler(result, true);
        } else if (this.cache["|" + mode + this.cacheKey]) {
            this.cacheKey = "|" + mode + this.cacheKey;
            this.postHandler(this.cache[this.cacheKey], true);
        } else {
            if (mode !== "all")
                this.cacheKey = "|" + mode + this.cacheKey;
            BX.ajax.loadJSON(
                this.ajaxURL,
                this.values2post(values),
                BX.delegate(this.postHandler, this)
            );
        }
    }
};

SymmetronCatalogFilter.prototype.postHandler = function (result, fromCache) {
    var url, values;

    if (!!result) {
        this.filterModalNode.find(".catalog-list__footer span")
            .first().html(result.ELEMENT_COUNT);
        if (result.MODE === "all") {
            this.filterModalNode.children(".list__wrapper").attr("itemid", "")
            if (result.FORM_HTML.length > 0) {
                this.formNode.html(result.FORM_HTML)
            }
            if (result.LIST_HTML.length > 0) {
                this.addFilterNode.children(".list__wrapper").html(result.LIST_HTML)
            }
            if (result.STORAGE_HTML.length > 0) {
                this.storageNode.html(result.STORAGE_HTML)
            }

            this.toggleDeleteAllButton()

            //values = this.getFilterValues("updateUrl", "");
            //values = this.values2html(values)
            //history.pushState(null, null, values)

            if (result.FORM_ACTION && result.COMPONENT_CONTAINER_ID) {
                values = this.getFilterValues("updateCatalog", "");
                values = this.values2html(values, result.COMPONENT_CONTAINER_ID.substring(5))

                url = BX.util.htmlspecialcharsback(result.FORM_ACTION).split("?")[0] + values
                BX.ajax.insertToNode(url, result.COMPONENT_CONTAINER_ID);
            }
        }
    }

    if (!fromCache && this.cacheKey !== '') {
        this.cache[this.cacheKey] = result;
    }
    this.cacheKey = '';
};

SymmetronCatalogFilter.prototype.values2post = function (values) {
    var post = [];
    var current = post;
    var i = 0;

    while (i < values.length) {
        var p = values[i].name.indexOf('[');
        if (p == -1) {
            current[values[i].name] = values[i].value;
            current = post;
            i++;
        }
        else {
            var name = values[i].name.substring(0, p);
            var rest = values[i].name.substring(p + 1);
            if (!current[name])
                current[name] = [];

            var pp = rest.indexOf(']');
            if (pp == -1) {
                //Error - not balanced brackets
                current = post;
                i++;
            }
            else if (pp == 0) {
                //No index specified - so take the next integer
                current = current[name];
                values[i].name = '' + current.length;
            }
            else {
                //Now index name becomes and name and we go deeper into the array
                current = current[name];
                values[i].name = rest.substring(0, pp) + rest.substring(pp + 1);
            }
        }
    }
    return post;
};

SymmetronCatalogFilter.prototype.values2html = function (values, bxajaxid) {
    var parts = [];
    values = this.values2post(values)
    values["bxajaxid"] = bxajaxid
    for (key in values) {
        if (key === "AJAX_CALL")
            continue;
        parts[parts.length] = key + "=" + values[key]
    }
    return "?" + parts.join("&")
};