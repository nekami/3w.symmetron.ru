<?
//define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();
if (empty($request["ID"]) || empty($request["IBLOCK_ID"]))
    return;

if (!\Bitrix\Main\Loader::includeModule("iblock"))
    return;

$iblock_id = (int)$request["IBLOCK_ID"];
$section_id = (int)$request["ID"];
$section_url_template = (int)$request["SECTION_URL"];
$sections = array();
$level = 0;
require(__DIR__."/get_level_elements.php");
if (!empty($sections))
    require(__DIR__."/catalog_level.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");