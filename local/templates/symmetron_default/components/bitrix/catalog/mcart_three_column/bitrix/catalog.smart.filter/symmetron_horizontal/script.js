function SymmetronCatalogFilter(ajaxURL, viewMode, params)
{
	this.ajaxURL = ajaxURL;
	this.popups = [];
	this.sliders = [];
	this.viewMode = viewMode;
	if (params && params.SEF_SET_FILTER_URL)
	{
		this.bindUrlToButton('set_filter', params.SEF_SET_FILTER_URL);
		this.sef = true;
	}
	if (params && params.SEF_DEL_FILTER_URL)
	{
		this.bindUrlToButton('del_filter', params.SEF_DEL_FILTER_URL);
	}

    this.bind();
}

SymmetronCatalogFilter.prototype.bindUrlToButton = function (buttonId, url)
{
    var button = BX(buttonId);
    if (button)
    {
        var proxy = function(j, func)
        {
            return function()
            {
                return func(j);
            }
        };

        if (button.type == 'submit')
            button.type = 'button';

        BX.bind(button, 'click', proxy(url, function(url)
        {
            window.location.href = url;
            return false;
        }));
    }
};

SymmetronCatalogFilter.prototype.bind = function ()
{
    var cl = this;

    BX.ready(function () {
        $('#analogue').on('click', '.remove-all', function(){
            var items = $(this).parent().children('.filter-item');
            for (var i = 0; i < items.length; i++)
                cl.remove_filter_item($(items[i]));
            cl.reload();
        });

        $('#analogue').on('click', '.filter-item__delete', function(){
            cl.remove_filter_item($(this).parent());
            cl.reload();
        });

        $('#analogue').on('click', '.option-delete', function(){
            cl.remove_filter_option($(this).parent());
            cl.reload();
        });

        $('#analogue').on('click', '.add-filter > button', function(){
            if ($(this).parent().hasClass('open')) {
                $(this).parent().removeClass('open')
            } else {
                if ($(this).siblings('.add-filter__item.open').length > 0) {
                    $(this).siblings('.add-filter__item.open').removeClass('open')
                }
                $(this).parent().addClass('open')
            }
        });
        $('#analogue').on('click', '.add-filter__items > div', function(){
            cl.open_filter_add($(this));
        });
        $('#analogue').on('click', '.filter-item__add', function(){
            cl.open_filter_add_from_item($(this).parent());
        });
        $('#analogue').on('click', '.filter-item__addit', function(){
            cl.open_filter_add_from_item($(this).parent());
        });
        $('#analogue').on('click', '.filter-item__range', function(){
            cl.open_filter_add_from_item($(this).parent());
        });

        $('#analogue').on('click', '.add-filter__unit', function(){
            $(this).parent().removeClass('open');

            if ($(this).parent().hasClass('multi-choice__item')) {
                cl.add_multi(this);
            } else if ($(this).parent().hasClass('radio-choice__item')) {
                cl.add_radio(this);
            } else if ($(this).parent().hasClass('range-choice__item')) {
                cl.add_range(this);
            }
            cl.reload();
        });

        $('#analogue').on('click', '.rangeLabel', function() {
            cl.on_label_click($(this));
        });
        $('#analogue').on('click', '.choose-item', function(e) {
            e.stopPropagation();
            cl.on_choose_click($(this));
        });
    })
};

SymmetronCatalogFilter.prototype.open_filter_add_from_item = function (item) {
    var itemId =  $(item).attr("itemid");
    var filter_add = item.siblings('.add-filter').children('.add-filter__items').children('div[itemid='+itemId+']');

    item.siblings('.add-filter').children('.add-filter__item.open').removeClass('open');

    this.open_filter_add(filter_add);
};
SymmetronCatalogFilter.prototype.open_filter_add = function (item) {
    if (item.parent().parent().hasClass('open')) {
        item.parent().parent().removeClass('open');
    }

    var name = "." + item.attr("itemid") + "__item";
    item.parent().parent().children(name).addClass('open');
    if (item.hasClass("range-choice")) {
        this.initSlider(item.attr("itemid"));
    }
};

SymmetronCatalogFilter.prototype.remove_filter_item = function (item) {
    var itemId =  $(item).attr("itemid");
    item.siblings('form').children('input.' + itemId + '__input').remove();

    if (itemId in this.sliders)
    {
        var i = item.siblings('.add-filter').children('.add-filter__item.' + itemId + '__item');
        if (i = i.find("div.range-v").data("ionRangeSlider"))
            i.reset();
    }
    else
        item.siblings('.add-filter').children('.add-filter__item.' + itemId + '__item').find('.item-unit input:checked').prop('checked', false);
    item.remove();
};
SymmetronCatalogFilter.prototype.remove_filter_option = function (option) {
    var item = option.parent();
    var itemId = $(item).attr("itemid");
    var elId = $(option).attr("elid");

    if (item.children('.filter-item__option:not(".filter-item__addit")').length === 1)
        this.remove_filter_item(item);
    else
    {
        item.siblings('form').children('#' + elId + '__input').remove();
        item.siblings('.add-filter').children('.add-filter__item.' + itemId + '__item').find('#' + elId).prop('checked', false);
        option.remove();
    }
    var hide = item.children('.filter-item__option.hide:not(".filter-item__addit")');
    if (hide.length > 0)
    {
        if (hide.length === 1)
            item.children('.filter-item__option.filter-item__addit').addClass("hide");
        $(hide[0]).removeClass("hide");
    }
};

SymmetronCatalogFilter.prototype.add_filter = function (el, addOption, addInputs) {
    var itemFilterName =  $(el).parent().children('h4').text();
    var itemId =  $(el).parent().attr("itemid");
    var itemClass =  itemId + "__filter";


    $(el).parent().parent().siblings('form').children('input.' + itemId + "__input").remove();

    var innerHtml =
        '<div class="filter-item__delete">'+
            '<span></span>'+
            '<span></span>'+
        '</div>'+
        '<div class="filter-item__name">'+itemFilterName+':</div>'+
        addOption;

    var filterBlock = $(el).parent().parent().siblings('.' + itemClass);
    if (addOption === '')
    {
        if (filterBlock.length > 0)
            filterBlock.remove();
        return;
    }
    if (filterBlock.length > 0) {
        filterBlock[0].innerHTML = innerHtml;
    } else {
        $(el).parent().parent().before(
            '<div class="filter-item ' + itemClass + '" itemid="' + itemId + '">'+
            innerHtml +
            '</div>'
        );
    }

    var addInput = '';
    for(var i = 0; i < addInputs.length; i++)
    {
        addInput +=
            '<input' +
            ' class="' + itemId + '__input"' +
            ' type="hidden"' +
            ' id="' + addInputs[i].id + '__input"' +
            ' name="' + addInputs[i].name +'"' +
            ' value="' + addInputs[i].value + '"' +
            ' checked>'
    }
    $(el).parent().parent().siblings('form').append(addInput);
};

SymmetronCatalogFilter.prototype.add_range = function (el) {
    var range = $(el).parent().find(".range-v");

    if (range.parent().siblings('.expanded').length === 0)
        this.add_standard_range(el, range);
    else
        this.add_expanded_range(el, range);
};
SymmetronCatalogFilter.prototype.add_expanded_range = function (el, rangeNode) {
    var range = rangeNode.data("ionRangeSlider");
    var rangeFrom = this.getValueFromSlider(
        rangeNode.siblings(".label-from").children(".choose-list"), range.result.from, range.options.scope.min_degree);
    var rangeTo = this.getValueFromSlider(
        rangeNode.siblings(".label-to").children(".choose-list"), range.result.to, range.options.scope.max_degree);

    var rangeOption = '<div class="filter-item__option range-option">' +
        BX.message("SYMMETRON_CH_FILTER_RANGE_FROM")+ ' ' +
        '<span>'+rangeFrom.name+'</span>' +
        ' ' +BX.message("SYMMETRON_CH_FILTER_RANGE_TO")+ ' ' +
        '<span>'+rangeTo.name+'</span>'+'</div><div class="filter-item__range"></div>';

    var rangeMin = rangeNode.parent().siblings('.range-from')[0];
    var rangeMax = rangeNode.parent().siblings('.range-to')[0];
    var radioInputs = [
        {
            id: rangeMin.id,
            name: rangeMin.name,
            value: rangeFrom.value
        },
        {
            id: rangeMax.id,
            name: rangeMax.name,
            value: rangeTo.value
        }
    ];
    this.add_filter(el, rangeOption, radioInputs);
};
SymmetronCatalogFilter.prototype.add_standard_range = function (el, rangeNode) {
    var range = rangeNode.data("ionRangeSlider");
    var rangeFrom = range.result.from
    var rangeTo = range.result.to

    var rangeOption = '<div class="filter-item__option range-option">' +
        BX.message("SYMMETRON_CH_FILTER_RANGE_FROM")+ ' ' +
        '<span>'+rangeFrom+'</span>' +
        ' ' +BX.message("SYMMETRON_CH_FILTER_RANGE_TO")+ ' ' +
        '<span>'+rangeTo+'</span>'+'</div><div class="filter-item__range"></div>';

    var rangeMin = $(el).parent().find('.range-from')[0];
    var rangeMax = $(el).parent().find('.range-to')[0];
    var radioInputs = [
        {
            id: rangeMin.id,
            name: rangeMin.name,
            value: rangeFrom
        },
        {
            id: rangeMax.id,
            name: rangeMax.name,
            value: rangeTo
        }
    ];
    this.add_filter(el, rangeOption, radioInputs);
};

SymmetronCatalogFilter.prototype.add_radio = function (el) {
    var checkedOption = $(el).siblings('.items-choice__radio').find('input:checked');

    var radioOption = "";
    var radioInputs = [];
    if (checkedOption.length > 0) {
        radioOption =
            '<div class="filter-item__option " elid="' + checkedOption.prop('id') + '">' +
            '<div class="items-choice__radio">' +
            '<label>' +
            '<input class="" ltype="radio" name="' + checkedOption.prop('name') + '" checked>' +
            checkedOption.prop('value') +
            '</label>' +
            '</div>' +
            '</div>';
        radioOption += '<div class="filter-item__range"></div>';

        radioInputs = [{
            id: checkedOption.prop('id'),
            name: checkedOption.prop('name'),
            value: checkedOption.prop('value')
        }];
    }


    this.add_filter(el, radioOption, radioInputs);
};

SymmetronCatalogFilter.prototype.add_multi = function (el) {
    var itemUnitsChecked = $(el).siblings('.items-choice__checkbox').find('.item-unit input:checked');

    var multyOption = '';
    var multyInputs = [];
    if (itemUnitsChecked.length > 0) {
        for (var i = 0; i < itemUnitsChecked.length; i++) {
            var itemUnitName = itemUnitsChecked[i].parentNode.innerText;
            multyOption += '<div class="filter-item__option ' + ((i > 2) ? 'hide' : '') + '" elid="' + itemUnitsChecked[i].id + '">' +
                '<div class="option-name">'+ itemUnitName+ '</div><div class="option-delete"><span></span><span></span></div></div>';
            multyInputs[i] = {
                id: itemUnitsChecked[i].id,
                name: itemUnitsChecked[i].name,
                value: itemUnitsChecked[i].value
            };
        }
        multyOption +=
            '<div class="filter-item__option ' + (itemUnitsChecked.length < 3 ? 'hide' : '') + ' filter-item__addit">' +
                BX.message("SYMMETRON_CH_FILTER_RANGE_MORE") +
                '<span class="count">' + (itemUnitsChecked.length - 3) + '</span>' +
            '</div>' +
            '<div class="filter-item__add">' +
                '<span></span><span></span>' +
            '</div>';
    }
    this.add_filter(el, multyOption, multyInputs);
};

SymmetronCatalogFilter.prototype.initSlider = function (code) {
    var name = "." + code + "__range";
    var range = $(name);
    var cl = this;
    if (range.length > 0) {
        /* Get min\max */
        var rangeMin = range.parent().siblings('.range-from');
        var rangeMax = range.parent().siblings('.range-to');
        var rangeFrom = rangeMin.val();
        var rangeTo = rangeMax.val();

        /* Get current */
        var form = range.closest('.parametric').children('form');
        if (form.children('#' + rangeMin.prop('name') + "__input").length > 0)
            rangeFrom = form.children('#' + rangeMin.prop('name') + "__input").val();
        if (form.children('#' + rangeMax.prop('name') + "__input").length > 0)
            rangeTo = form.children('#' + rangeMax.prop('name') + "__input").val();

        /* Calculate to slider */
        this.sliders[code] = true;
        rangeMin = rangeMin.val();
        rangeMax = rangeMax.val();
        if (range.parent().siblings('.expanded').length === 0)
            cl.initStandardSlider(range, rangeMin, rangeMax, rangeFrom, rangeTo);
        else {
            cl.initExpandedSlider(range, rangeMin, rangeMax, rangeFrom, rangeTo);
        }
    }
};

SymmetronCatalogFilter.prototype.getValueFromSlider = function(list, value, degree)
{
    if (value > 1000) value -= 1000;
    var name = value + " " + list.children(".choose-item[degree="+degree+"]").text();
    value = value * Math.pow(10, degree);
    return {name: name, value: value};
};
SymmetronCatalogFilter.prototype.getValueArrayToSlider = function(value, isEnd)
{
    var degree = Math.floor(Math.log10(value));
    var sign = -1;//Math.sign(degree);

    degree = degree + sign * (3 - sign * degree % 3) % 3;
    value = value / Math.pow(10, degree);
        if (isEnd)  value = Math.ceil(value);
        else        value = Math.floor(value);
    return {degree: degree, value: value};
};
SymmetronCatalogFilter.prototype.getPrepareArrayToSlider = function(value, border)
{
    value.min = 0;
    value.max = 999;

    if (value.degree != border.degree)
        return value;
    value.min = border.value;
    value.max = border.value;
    return value;
};
SymmetronCatalogFilter.prototype.getPrettyToSlider = function(value, scope)
{
    var degree = value < 1000 ? scope.min_degree : scope.max_degree;
    if (value > 999) value -= 1000;
    var name = scope.input.siblings(".label-from").find(".choose-list > .choose-item[degree="+degree+"]").text();
    return value+" "+name + "<span class=\"current-choose\"></span>";
};
SymmetronCatalogFilter.prototype.initExpandedSlider = function (range, min, max, from, to) {
    var cl = this;
    from = cl.getPrepareArrayToSlider(cl.getValueArrayToSlider(from, false), cl.getValueArrayToSlider(min, false));
    to = cl.getPrepareArrayToSlider(cl.getValueArrayToSlider(to, true), cl.getValueArrayToSlider(max, true));
    var data = {
        type: "double",
        hide_min_max: true,
        force_edges: true,
        keyboard: true,

        min: 0,
        max: 999,
        from: from.value,
        from_min: from.min,
        from_max: 999,
        to: to.value,
        to_min: 1,
        to_max: to.max,
        scope: {
            min_degree: from.degree,
            max_degree: to.degree,
            input: range,
        }
    };
    if (from.degree !== to.degree)
        data = cl.shift_slider_max(data, 1);
    data.prettify = function (value) {
        return cl.getPrettyToSlider(value, this.scope);
    };
    data.onStart = function (data) {
        cl.watchThis(data.input);
    };
    data.onUpdate = function (data) {
        //cl.watchThis(data.input);
        setTimeout(function() {cl.watchThis(range)}, "0")
    };
    data.onChange = function (data) {
        cl.watchThis(data.input);
    };
    range.ionRangeSlider(data);
    setTimeout(function() {cl.watchThis(range)}, "200")
};
SymmetronCatalogFilter.prototype.initStandardSlider = function (range, min, max, from, to) {
    range.ionRangeSlider({
        type: "double",
        min: min,
        max: max,
        from: from,
        to: to,
        hide_min_max: true,
        force_edges: true,
        keyboard: true,
    });
};

SymmetronCatalogFilter.prototype.shift_slider_max = function (data, sign) {
    data.max    = data.max    + sign * 1000;
    data.to     = data.to     + sign * 1000;
    data.to_min = data.to_min + sign * 1000;
    data.to_max = data.to_max + sign * 1000;
    return data;
};
SymmetronCatalogFilter.prototype.on_label_click = function (el) {
    if (el.hasClass('open')) {
        el.removeClass('open')
    } else {
        el.siblings(".rangeLabel").removeClass('open')
        el.addClass('open')
    }
};
SymmetronCatalogFilter.prototype.on_choose_click = function (el) {
    if (el.hasClass('inactive'))
        return;
    var node = el.parent().parent();
    var range = node.siblings(".range-v").data("ionRangeSlider");

    var data = {
        scope: range.options.scope,

        max: range.result.max,
        to: range.result.to,
        to_min: range.options.to_min,
        to_max: range.options.to_max,
    };
    if (data.max > 999)
        data = this.shift_slider_max(data, -1);
    var border = 0, val = {};
    if (el.parent().parent().hasClass("label-from")) {
        data.from = 0;
        data.scope.min_degree = el.attr("degree");
        border = node.parent().siblings('.range-from').val();
        val = this.getPrepareArrayToSlider({degree: data.scope.min_degree}, this.getValueArrayToSlider(border, false));
        data.from_min = val.min;
        this.on_choose_click_deactivate(data.scope.min_degree, node.siblings(".label-to"), false);
    } else {
        data.to = 999;
        data.scope.max_degree = el.attr("degree");
        border = node.parent().siblings('.range-to').val();
        val = this.getPrepareArrayToSlider({degree: data.scope.max_degree}, this.getValueArrayToSlider(border, true));
        data.to_max = val.max;
        this.on_choose_click_deactivate(data.scope.max_degree, node.siblings(".label-from"), true);
    }
    if (data.scope.min_degree != data.scope.max_degree)
        data = this.shift_slider_max(data, 1);
    if (range.options.scope.min_degree != data.scope.min_degree ||
        range.options.scope.max_degree != data.scope.max_degree)
        range.update(data);
    node.removeClass('open');
};
SymmetronCatalogFilter.prototype.on_choose_click_deactivate = function (degree, list, isFrom) {
    list.find(".choose-item.inactive").removeClass("inactive");

    var items = list.find(".choose-item");
    for (var i = 0; i < items.length; i++)
    {
        var item = $(items[i]);
        if (!isFrom && item.attr("degree") < degree ||
            isFrom && item.attr("degree") > degree)
            item.addClass("inactive");
    }
};

SymmetronCatalogFilter.prototype.watchThis = function (range) {
    var rangeFrom = range.siblings('.irs').find('.irs-from[style*="visibility: visible"] > .current-choose');
    var rangeTo   = range.siblings('.irs').find('.irs-to[style*="visibility: visible"] > .current-choose');

    if (rangeFrom.length !== rangeTo.length) {
        range.siblings('.irs').find('.irs-from').css("visibility", "hidden");
        range.siblings('.irs').find('.irs-to').css("visibility", "hidden");
        range.siblings('.irs').find('.irs-single').css("visibility", "visible");
    }
    if (rangeFrom.length === 0 || rangeTo.length === 0)
    {
        rangeFrom = range.siblings('.irs').find('.irs-single[style*="visibility: visible"] > .current-choose');
        if (rangeFrom.length !== 2)
            return;
        rangeTo = $(rangeFrom[1]);
        rangeFrom = $(rangeFrom[0]);
    }

    range.siblings('.label-from').offset(rangeFrom.offset());
    range.siblings('.label-to').offset(rangeTo.offset());

/*    range.siblings('.label-from')[0].style.left = rangeFrom[0].style.left;
    range.siblings('.label-to')[0].style.left   = rangeTo[0].style.left;

    var innerFrom = rangeFrom.innerWidth() - 16;
    var innerTo =   rangeTo.innerWidth()   - 16;

    range.siblings('.label-from')[0].style.marginLeft = innerFrom + 5 + 'px';
    range.siblings('.label-to')[0].style.marginLeft   = innerTo   + 5 + 'px';*/
};

SymmetronCatalogFilter.prototype.initSliders = function () {
    var sliders = $('.add-filter__item.range-choice__item ');
    for (var i = 0; i < sliders.length; i++)
    {
        var code = $(sliders[i]).attr("itemid");
        this.initSlider(code);
    }
};

/* I dub you solemnly Ajax... */
SymmetronCatalogFilter.prototype.reload = function()
{
    var form = $('.parametric').children('form');
    if (form)
    {
        var ajaxValues = form.serializeArray();
        ajaxValues[ajaxValues.length] = {name: "set_filter", value: ""}
        var url = this.ajaxURL.split("?");
        var clearUrl = url[0];
        url = clearUrl + "?" + $.param(ajaxValues);
        //$(".analogue-table__holder > .scroll-table").load( url + " #tableLastProd");
        $("#analogue > .wrapper > .section-content").load( url + " .section-content .to-reload-by-filter");
        $("#analogue > .wrapper > #parametric").load( url + " .parametric");

        this.sliders = [];
        this.initSliders();

        for (var i = 0; i < ajaxValues.length; i++)
            if (ajaxValues[i].name == "bxajaxid")
            {
                ajaxValues.splice(i, 1);
                break;
            }

        history.replaceState(null, null, clearUrl + "?" + $.param(ajaxValues));
    }
};