<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @global array $currentItem */

use \Bitrix\Main\Localization\Loc;
?>
<div class="add-filter__item multi-choice__item <?=$arItem["CODE"]?>__item" itemid="<?=$arItem["CODE"]?>">
    <h4><?=$arItem["NAME"]?></h4>
    <div class="items-choice__checkbox">
        <?foreach($arItem["VALUES"] as $val => $ar):?>
            <?if (!empty($ar["DISABLED"]) && empty($ar["CHECKED"]))
                continue;?>

            <div class="item-unit">
            <label><input
                type="checkbox"
                value="<? echo $ar["HTML_VALUE"] ?>"
                name="<? echo $ar["CONTROL_NAME"] ?>"
                id="<? echo $ar["CONTROL_ID"] ?>"
                 <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                /><?=$ar["VALUE"];?></label>
        </div>
        <?endforeach;?>
    </div>

    <button type="button" class="default-button add-filter__unit"><?=Loc::GetMessage("SYMMETRON_CH_FILTER_BUTTON_ADD")?></button>
</div>