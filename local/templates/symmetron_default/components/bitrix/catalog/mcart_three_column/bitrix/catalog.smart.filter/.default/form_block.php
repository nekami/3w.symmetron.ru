<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<? foreach ($arResult["HIDDEN"] as $arItem): ?>
    <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>"
           value="<? echo $arItem["HTML_VALUE"] ?>"/>
<? endforeach; ?>
<?
$APPLICATION->IncludeComponent(
    "mcart:catalog.filter.form",
    "line",
    array(
        "FULL"   => false,
        "TYPE"   => "empty",
    ),
    $component,
    array('HIDE_ICONS' => 'Y')
);

/*echo "<pre>";
print_r($arResult["ITEMS"]);
echo "</pre>";*/
$arElements = [];
if(\Bitrix\Main\Loader::includeModule("mcart.propertywithmeasureunits")) {

    $arMeasureProps = [];
    foreach ($arResult["ITEMS"] as $arItem) {
        if ($arItem["USER_TYPE"] == "mcart_property_with_measure_units" && !empty($arItem["FILTERED"])) {
            $arMeasureProps[$arItem["USER_TYPE_SETTINGS"]["ELEMENT_ID"]] = $arItem["USER_TYPE_SETTINGS"];
        }
    }


    if(!empty($arMeasureProps)) {

        /*$arElements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasuresUnitsList(
            $arMeasureProps, ["PROPERTY_ALTERNATIVE"]);*/

        $obCache = new CPHPCache;
        $lifeTime = 3600;
        global $USER;
        $cacheId = md5(serialize($arMeasureProps) . $USER->GetID());

        if($obCache->InitCache($lifeTime, $cacheId, "/")) {
            $arElements = $obCache->GetVars();
        } else {
            if($obCache->StartDataCache()) {
                $arElements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasureUnitsListFromHL(array_keys($arMeasureProps));
                $obCache->EndDataCache($arElements);
            }
        }
    }
}

foreach ($arResult["ITEMS"] as $arItem) {
    if (empty($arItem["FILTERED"])) {
        continue;
    }
    $additName = "";
    if (!empty($arItem["PRICE"])) {
        $additName = " (".$arItem["CURRENCIES"][$arItem["VALUES"]["MAX"]["CURRENCY"]].")";
    }
    $APPLICATION->IncludeComponent(
        "mcart:catalog.filter.form",
        "line",
        array(
            "CODE"               => $arItem["CODE"],
            "TITLE"              => $arItem["NAME"].$additName,
            "TYPE"               => $arItem["TEMPLATE_TYPE"],
            "VALUES"             => $arItem["VALUES"],
            "USER_TYPE"          => $arItem["USER_TYPE"],
            "USER_TYPE_SETTINGS" => $arItem["USER_TYPE_SETTINGS"],
            "MEASURES_UNITS"     => $arElements
        ),
        $component,
        array('HIDE_ICONS' => 'Y')
    );
} ?>