<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/***
 * $sections = array(section)
 * $level
 ***/
?>
<ul>
    <?foreach ($sections as $section):?>
        <?
        $class = "catalog__item";
/*if (strlen($section["NAME"]) > 0) // 40)
$class .= " chars-visible";*/
        if (!empty($section["CHOSEN"]))
            $class .= " chosen";
        ?>
        <li class="<?=$class?>" element_id="<?=$section["ID"]?>" element_level="<?=$level?>">
            <div class="catalog-item__holder <?=$section["IS_END"] ? "endpoint" : ""?>">
                <a href="<?=!empty($section["URL"]) ? $section["URL"] : ""?>" <?=$section["IS_END"] ? "onclick='console.log()'" : ""?>">
                    <?=$section["NAME"]?>
                </a>
            </div>
        </li>
    <?endforeach;?>
</ul>
