<?
//define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();
if (empty($request["BREADCRUMBS"]) || !is_array($request["BREADCRUMBS"]))
    return;

global $APPLICATION;
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "N");

$APPLICATION->AddChainItem('Каталог', '/catalog/');
foreach ($request["BREADCRUMBS"] as $crumb)
    $APPLICATION->AddChainItem($crumb[0], $crumb[1]);
$pathTemplate = "/local/templates/symmetron_default/components/bitrix/breadcrumb/.default/template.php";

echo $APPLICATION->GetNavChain(false, 1, $pathTemplate, true, false);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");