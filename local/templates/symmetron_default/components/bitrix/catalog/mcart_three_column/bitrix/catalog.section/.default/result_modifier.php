<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Sale;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$path = "/bitrix/components/bitrix/catalog.section/templates/.default/";
require($_SERVER["DOCUMENT_ROOT"] . $path . "result_modifier.php");

//sort params
$arParams["SORT_CODE"] = $arParams["SORT_CODE"] ?: "sort";
$arParams["ORDER_CODE"] = $arParams["ORDER_CODE"] ?: "method";


//id for js component
$arParams["LIST_ID"] = trim($arParams["LIST_ID"]);
$arParams["LIST_ID"] = $arParams["LIST_ID"] ?: "analogue";

//for options modal
$arParams["OPTIONS_MODAL_CODE"] = trim($arParams["OPTIONS_MODAL_CODE"]);
$arParams["OPTIONS_MODAL_CODE"] = $arParams["OPTIONS_MODAL_CODE"] ?: "OPTIONS_MODAL";

//currency info
$arResult["CUR_CURRENCY"] = 'RUB';
$arResult["CURRENCY_INFO"] = \CCurrencyLang::GetFormatDescription($arResult["CUR_CURRENCY"]);

//get hints
$arHints = [];
if (empty($arParams["PROPERTIES_HINTS"])) {
    if (CModule::IncludeModule("mcart.sortcatalogsm")) {
        $arHints = Mcart\SortCatalogSM\Helper::getHints($arResult["ID"]);
    }
} else {
    $arHints = $arParams["PROPERTIES_HINTS"];
}

if (!empty($arHints)) {
    foreach ($arResult["ITEMS"][0]["DISPLAY_PROPERTIES"] as $key => $arProp) {

        if (!empty($arHints[$arProp["ID"]]["SHORT"])) {
            $arResult["ITEMS"][0]["DISPLAY_PROPERTIES"][$key]["NAME"] = $arHints[$arProp["ID"]]["SHORT"];
        }

        if (!empty($arHints[$arProp["ID"]]["LONG"])) {
            $arResult["ITEMS"][0]["DISPLAY_PROPERTIES"][$key]["HINT"] = $arHints[$arProp["ID"]]["LONG"];
        }

    }

    $arResult["PROPERTIES_HINTS"] = $arHints;
}


/*get measure info*/

$resMeasure = \CCatalogMeasure::getList([], [], false);
$arResult["MEASURE_DATA"] = [];
while ($row = $resMeasure->fetch()) {
    $arResult["MEASURE_DATA"][] = $row;
}

/*get measure info end*/


$arResult["COLUMNS"] = [
    //["CODE" => "NAME", "NAME" => Loc::getMessage("CT_BCS_TPL_MESS_NAME_COLUMN")],
];

$arSelectProps = $arParams["COLUMNS_PROPERTIES"];

$articleCode = "ARTICLE";
$articleKey = array_search($articleCode, $arSelectProps);
if ($articleKey !== false) {
    unset($arSelectProps[$articleKey]);
}

array_unshift($arSelectProps, $articleCode);

foreach ($arSelectProps as $code) {
    if (empty(trim($code))) {
        continue;
    }
    $arProp = $arResult["ITEMS"][0]["PROPERTIES"][$code];
    $propertyCode = $code;

    if (!empty($arHints[$arProp["ID"]]["SHORT"])) {
        $arProp["NAME"] = $arHints[$arProp["ID"]]["SHORT"];
    }
    $name = $arProp["NAME"];

    //hint
    if (!empty($arHints[$arProp["ID"]]["LONG"])) {
        $arProp["HINT"] = $arHints[$arProp["ID"]]["LONG"];
    }
    $hint = $arProp["HINT"];


    if ($code == "PRODUCERID") {
        $code .= ".NAME";
    }
    $code = "PROPERTY_" . $code;

    $arResult["COLUMNS"][] = ["CODE" => $code, "NAME" => $name, "PROPERTY_CODE" => $propertyCode, "HINT" => $hint];
}

foreach ($arResult["COLUMNS"] as &$row) {
    if ($row["CODE"] == $arParams["ELEMENT_SORT_FIELD"]) {
        $row["SORT"] = strtolower($arParams["ELEMENT_SORT_ORDER"]) == "desc" ? "desc" : "asc";
        $row["SORT_CLASS"] = "sort-val-" . $row["SORT"];
    } else {
        $row["SORT"] = "desc";
        $row["SORT_CLASS"] = "";//"asc";
    }
}
/* List with all */
$propertyColumnsMay = array_combine(
    array_column($arResult["ITEMS"][0]["PROPERTIES"], "ID"),
    array_keys($arResult["ITEMS"][0]["PROPERTIES"])
);
$propertyColumnsNames = array_combine(
    array_keys($arResult["ITEMS"][0]["PROPERTIES"]),
    array_column($arResult["ITEMS"][0]["PROPERTIES"], "NAME")
);
//$propertyColumnsMay = array_flip(array_diff_key($propertyColumnsMay, $arPropAval));
//$arResult["COLUMN_LIST"] = array_diff_key($propertyColumnsNames, $propertyColumnsMay);
$arResult["COLUMN_LIST"] = $propertyColumnsNames;
/* END: Properties visible options */

/* Prepare params */
foreach (["PROPERTY_CODE_FAST_SHOW_LEFT", "PROPERTY_CODE_FAST_SHOW_RIGHT"] as $key) {
    foreach ($arParams[$key] as &$code) {
        $code = trim($code);
        if (empty($code)) {
            unset($code);
        }
    }
    $arParams[$key] = array_unique($arParams[$key]);
}

CJSCore::Init(["currency"]);

//get measure and packing info

$templatetext = [];
if (\Bitrix\Main\Loader::includeModule("highloadblock") && \Bitrix\Main\Loader::includeModule("mcart.templatetext")) {
    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(\COption::GetOptionString("mcart.templatetext", "MCART_TT_HL", "0"))->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $rsData = $entity_data_class::getList(array(
        "select" => array("*"),
        "filter" => array("UF_ACTIVE" => 1, "UF_CONV_IMMED" => 0),
    ));
    while ($arData = $rsData->Fetch()) {
        $templatetext[$arData["UF_IBLOCK_PROP"]] = $arData;
    }
}

if (\Bitrix\Main\Loader::includeModule("mcart.basketsymmetron")) {

    $arPackingMeasureData = CBasketSymmetron::getInfoMeasureAndPacking($arResult["ELEMENTS"], $arParams["IBLOCK_ID"]);

    $arNoActiveBrand = [];
    $brandIblockId = 0;

    foreach ($arResult["ITEMS"] as $key => $arItem) {

        //$arResult["ITEMS"][$key]["ARTICLE"] = $arItem["DISPLAY_PROPERTIES"]["DISPLAY_VALUE"];

        // display the integer value of the package
        if ($arPackingMeasureData["PACKING"][$arItem["ID"]]['SYMPACKAGE'])
            $arPackingMeasureData["PACKING"][$arItem["ID"]]['SYMPACKAGE'] = round($arPackingMeasureData["PACKING"][$arItem["ID"]]['SYMPACKAGE']);

        $arResult["ITEMS"][$key]["PACK_INFO"] = $arPackingMeasureData["PACKING"][$arItem["ID"]];
        $arResult["ITEMS"][$key]["ITEM_MEASURE"] = $arPackingMeasureData["MEASURES"][$arItem["ID"]];
        $arBrand = $arItem["DISPLAY_PROPERTIES"]["PRODUCERID"];
        $brandIblockId = $arBrand["LINK_IBLOCK_ID"];
        if (empty($arBrand["LINK_ELEMENT_VALUE"]) && !empty($arBrand["~VALUE"])
            && in_array("PRODUCERID", $arParams["PROPERTY_CODE"])) {
            $arNoActiveBrand[] = $arBrand["~VALUE"];
        }

        //processing active param
        $arStatus = &$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["ACTIVE_STATUS"];

        if ($arStatus["VALUE_XML_ID"] == "ACTIVE") {
            $arStatus["DISPLAY_VALUE"] = "<span class='active-status stock'>В наличии</span>";
        } else {
            $arStatus["DISPLAY_VALUE"] = "<span class='active-status out'>Нет в наличии</span>";
        }

        //processing retail price

        $arRetailPrice = &$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["RETAIL_PRICE"];
        if (!empty($arRetailPrice["DISPLAY_VALUE"])) {

            $measurePrice = $arResult["ITEMS"][$key]["ITEM_MEASURE"]["SYMBOL_RUS"] ?:
                Loc::getMessage("CT_BCS_TPL_MESS_SHT");

            $arRetailPrice["DISPLAY_VALUE"] = \CCurrencyLang::CurrencyFormat(
                    $arRetailPrice["DISPLAY_VALUE"],
                    $arResult["CUR_CURRENCY"],
                    true
                ) . "/" . $measurePrice . ".";
        }

        if (!empty($templatetext)) {
            foreach ($templatetext as $keyT => $valT) {
                if (key_exists($keyT, $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"])) {
                    $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"][$keyT]['DISPLAY_VALUE'] = \Mcart\TemplateText\Text::GenerateText(
                        $arResult["ITEMS"][$key]["ID"],
                        htmlspecialcharsBack((key_exists("TEXT", $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"][$keyT]["~VALUE"]) ? $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"][$keyT]["~VALUE"]["TEXT"] : $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"][$keyT]["~VALUE"])),
                        $valT["ID"]
                    );
                }
            }
        }
    }

    //get no active brand
    $arNoActiveBrands = [];
    if (!empty($arNoActiveBrand)) {
        $arSelect = ["ID", "NAME"];
        $arFilter = [
            "IBLOCK_ID" => IntVal($brandIblockId),
            "ID" => $arNoActiveBrand,
            "ACTIVE" => "N",
            "PROPERTY_ACTIVE" => "Y"
        ];
        $rsNoActiveBrands = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($arNoActiveBrand = $rsNoActiveBrands->Fetch()) {
            $arNoActiveBrands[$arNoActiveBrand["ID"]] = $arNoActiveBrand["NAME"];
        }
    }

    foreach ($arResult["ITEMS"] as $key => $arItem) {

        $arBrand = &$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["PRODUCERID"];
        if (empty($arBrand["LINK_ELEMENT_VALUE"]) && !empty($arBrand["~VALUE"])
            && in_array("PRODUCERID", $arParams["PROPERTY_CODE"])) {
            $arBrand["DISPLAY_VALUE"] = $arNoActiveBrands[$arBrand["~VALUE"]];
        }

    }

    //$arResult["PACK_INFO"] = $arPackingMeasureData["PACKING"][$arResult["ID"]];
    //$arResult["ITEM_MEASURE"] = $arPackingMeasureData["MEASURES"][$arResult["ID"]];
}

//for analogs and together

$elementId = $GLOBALS['CATALOG_CURRENT_ELEMENT_ID'];
if (!empty($elementId)) {

    $arSelect = Array("ID", "NAME", "PROPERTY_ARTICLE");
    $arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $elementId);
    $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if ($ob = $res->Fetch()) {
        $arResult["CURRENT_ELEMENT"] = $ob;
        //$arResult["SECTION_"] = $ob;
    }
}

//get products from basket
$arResult["BASKET_PRODS"] = \Bitrix\Sale\Basket::loadItemsForFUser(
    \Bitrix\Sale\Fuser::getId(),
    \Bitrix\Main\Context::getCurrent()->getSite()
);

//measure data
$arResult["MEASURE_FRACTIONAL"] = [];
if (\Bitrix\Main\Loader::includeModule("mcart.symmetronsyncdb")) {
    $measure_fractional = \Bitrix\Main\Config\Option::get('mcart.symmetronsyncdb', 'measure_fractional', '');
    $arResult["MEASURE_FRACTIONAL"] = explode(",", $measure_fractional);
}


// get information for declination

$arWords = [];
foreach ($arResult["MEASURE_DATA"] as $arData) {
    $val = trim($arData["MEASURE_TITLE"]);
    if (!empty($val))
        $arWords[] = strtolower($val);
}

foreach ($arResult["ITEMS"] as $arItem) {
    $val = trim($arItem["PACK_INFO"]["UPAK"]["VALUE"]);
    if (!empty($val))
        $arWords[] = strtolower($val);
}

$arWords[] = Loc::getMessage("CT_BCS_TPL_MESS_SHT");
$arWords[] = Loc::getMessage("CT_BCS_TPL_MESS_UNITS");
$arWords[] = Loc::getMessage("CT_BCS_TPL_MESS_PRODUCTS");

$arResult["DECLINATION_OBJ"] = new \Mcart\BasketSymmetron\SymmetronDeclination($arWords);

$arFilterSection = ["ID" => $arParams["SECTION_ID"], "ACTIVE" => "Y"];
$breed = [];
$rsSectionInfo = \CIBlockSection::GetList([], $arFilterSection, false, ["ID", "NAME", "LEFT_MARGIN", "IBLOCK_ID", "RIGHT_MARGIN"]);
if ($arSectionInfo = $rsSectionInfo->Fetch()) {
    $rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), array(
        'IBLOCK_ID' => $arSectionInfo["IBLOCK_ID"],
        "<=LEFT_BORDER" => $arSectionInfo["LEFT_MARGIN"],
        ">=RIGHT_BORDER" => $arSectionInfo["RIGHT_MARGIN"],
    ));

    while ($arSect = $rsSect->Fetch()) {
        $breed[] = $arSect['NAME'];
    }
}
$arParams["BREAD"] = join("/", $breed);

$arResult['YA_RESULT'] = [
    'bread' => $arParams["BREAD"],
];

$res = CIBlockProperty::GetByID("PRODUCERID");
$PRODUCERID_IBLOCK_ID = false;
if ($ar_res = $res->GetNext()) {
    $PRODUCERID_IBLOCK_ID = $ar_res["LINK_IBLOCK_ID"];
}
$PRODUCERID_IDs = [];
foreach ($arResult["ITEMS"] as $key => $arItem) {
    if (!empty($arItem["PROPERTIES"]["PRODUCERID"]["VALUE"])) {
        $PRODUCERID_IDs[] = $arItem["PROPERTIES"]["PRODUCERID"]["VALUE"];
    }
}
$PRODUCERID_NAMES = [];
if ($PRODUCERID_IBLOCK_ID !== false && !empty($PRODUCERID_IDs)) {
    $res = CIBlockElement::GetList(Array(), array("ID" => $PRODUCERID_IDs, "IBLOCK_ID" => $PRODUCERID_IBLOCK_ID), false, false, array("ID", "NAME"));
    while ($ob = $res->Fetch()) {
        $PRODUCERID_NAMES[$ob["ID"]] = $ob["NAME"];
    }
}

foreach ($arResult["ITEMS"] as $key => $arItem) {
    $arResult['YA_RESULT'][$arItem["ID"]] = [
        'name' => $arItem["NAME"],
        'id' => $arItem["ID"],
        'brand' => (!empty($arItem["PROPERTIES"]["PRODUCERID"]["VALUE"]) && key_exists($arItem["PROPERTIES"]["PRODUCERID"]["VALUE"], $PRODUCERID_NAMES) ? $PRODUCERID_NAMES[$arItem["PROPERTIES"]["PRODUCERID"]["VALUE"]] : ""),
        'price' => (count($arItem["ITEM_PRICES"]) > 0 ? $arItem["ITEM_PRICES"][0]["PRICE"] : 0),
    ];
}
