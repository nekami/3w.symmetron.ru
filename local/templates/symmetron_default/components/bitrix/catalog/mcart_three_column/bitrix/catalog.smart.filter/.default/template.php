<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<? unset($arResult["COMBO"]) ?>
<? unset($arResult["PROPERTY_ID_LIST"]) ?>
<section class="catalog_filter">
    <div class="wrapper">
        <div class="parametric">
            <? /* Hidden inputs with real fields to filter */ ?>
            <form name="<? echo $arResult["FILTER_NAME"]."_form" ?>"
                  action="<? echo $arResult["FORM_ACTION"] ?>"
                  method="get"
                  class="smartfilter">
                <? $this->getComponent()->includeComponentTemplate("form_block"); ?>
            </form>


            <? /* Clear filter button */ ?>
            <div class="remove-all <?= !$arResult["FILTERED"] ? "close" : "" ?>">
                <span class="delete-red"></span><span><?= Loc::GetMessage("SYMMETRON_CH_FILTER_DELETE_ALL") ?></span>
            </div>

            <? /* Add filter button */ ?>
            <div class="add-filter">
                <button type="button"><span></span><span><?= Loc::GetMessage("SYMMETRON_CH_FILTER_ADD") ?></span>
                </button>
                <div class="list__wrapper">
                    <? $this->getComponent()->includeComponentTemplate("list_block"); ?>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "mcart:catalog.filter.form",
                    "",
                    array(
                        "CODE"   => "",
                        "TYPE"   => "empty",
                        "HIDDEN" => true,
                    ),
                    $component,
                    array('HIDE_ICONS' => 'Y')
                ); ?>
            </div>

            <? /* Array of filter form contents */ ?>
            <div class="storage-filter" style="display: none">
                <? $this->getComponent()->includeComponentTemplate("storage_block"); ?>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var smartFilter = new SymmetronCatalogFilter('<?= CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>