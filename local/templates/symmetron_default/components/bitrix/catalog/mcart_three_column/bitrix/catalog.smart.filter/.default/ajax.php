<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
$toResult = [];
$htmlNeed = [
    "FORM_HTML" => "form_block",
    "LIST_HTML" => "list_block",
    "STORAGE_HTML" => "storage_block",
];

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();
if ($request->get("mode") == "count")
    $htmlNeed = [];

ob_start();
foreach ($htmlNeed as $key => $template) {
    $this->getComponent()->includeComponentTemplate($template);
    $toResult[$key] = preg_replace("/\\s{2,}/", "", ob_get_contents());
    ob_clean();
}
ob_end_clean();

$keysNeed = [
    "ELEMENT_COUNT",
    "FORM_ACTION",
    "COMPONENT_CONTAINER_ID",
];

$toResult = array_merge(
    array_intersect_key($arResult, array_combine($keysNeed, $keysNeed)),
    $toResult
);
$toResult["MODE"] = $request->get("mode");

$APPLICATION->RestartBuffer();
echo CUtil::PHPToJSObject($toResult, true);
?>