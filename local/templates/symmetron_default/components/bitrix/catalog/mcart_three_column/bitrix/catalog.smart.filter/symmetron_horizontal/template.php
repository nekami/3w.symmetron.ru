<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$this->addExternalCss($templateFolder."/slider/ion.rangeSlider.css");
$this->addExternalJs($templateFolder."/slider/ion.rangeSlider.min.js");
?>
<div id="parametric"><div class="parametric">
    <?/* Hidden inputs with real fields to filter */?>
    <?$filteredItemCodes = array();?>
    <?if (!empty($arResult["ITEMS"]["BASE"]))
        $arResult["ITEMS"]["BASE"]["DISPLAY_TYPE"] = "A";?>
    <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
        <?foreach($arResult["HIDDEN"] as $arItem):?>
            <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
        <?endforeach;?>
        <?foreach ($arResult["ITEMS"] as $key => $arItem):?>
            <?
            switch ($arItem["DISPLAY_TYPE"])
            {
                case "A":
                case "B":
                    if (isset($arItem["VALUES"]["MIN"]["HTML_VALUE"])
                        && isset($arItem["VALUES"]["MAX"]["HTML_VALUE"])) {
                        $filteredItemCodes[$arItem["CODE"]] = true;
                        ?><input checked
                                 class="<?= $arItem["CODE"] ?>__input"
                                 type="hidden"
                                 id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>__input"
                                 name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                 value="<?= $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"><?
                        ?><input checked
                                 class="<?= $arItem["CODE"] ?>__input"
                                 type="hidden"
                                 id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>__input"
                                 name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                 value="<?= $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"><?
                    }
                    break;
                case "P":
                case "R":
                case "K":
                    $isDis = true;
                    foreach ($arItem["VALUES"] as $ar)
                    {
                        if (!empty($ar["DISABLED"]) && empty($ar["CHECKED"]))
                            continue;
                        $isDis = false;

                        if (empty($ar["CHECKED"]))
                            continue;
                        $filteredItemCodes[$arItem["CODE"]] = true;
                        ?><input checked
                            class="<?=$arItem["CODE"]?>__input"
                            type="hidden"
                            id="<?=$ar["CONTROL_ID"]?>__input"
                            name="<?=$ar["CONTROL_NAME_ALT"]?>"
                            value="<?=$ar["HTML_VALUE_ALT"]?>"><?
                    }
                    if ($isDis) unset($arResult["ITEMS"][$key]);
                    break;
                case "G":
                case "H":
                    //case "U": // calendar
                default:
                    $isDis = true;
                    foreach ($arItem["VALUES"] as $ar)
                    {
                        if (!empty($ar["DISABLED"]) && empty($ar["CHECKED"]))
                            continue;
                        $isDis = false;

                        if (empty($ar["CHECKED"]))
                            continue;
                        $filteredItemCodes[$arItem["CODE"]] = true;
                        ?><input checked
                             class="<?=$arItem["CODE"]?>__input"
                             type="hidden"
                             id="<?=$ar["CONTROL_ID"]?>__input"
                             name="<?=$ar["CONTROL_NAME"]?>"
                             value="<?=$ar["HTML_VALUE"]?>"><?
                    }
                    if ($isDis) unset($arResult["ITEMS"][$key]);
                    break;
            }
            ?>
        <?endforeach;?>
    </form>

    <div class="remove-all <?=empty($filteredItemCodes) ? "close": ""?>">
        <span class="delete-red"></span><span><?=Loc::GetMessage("SYMMETRON_CH_FILTER_DELETE_ALL")?></span>
    </div>

    <?/* Filter visual parts */?>
    <?foreach ($arResult["ITEMS"] as $key => $arItem):?>
        <?
        if (empty($filteredItemCodes[$arItem["CODE"]]))
            continue;
        $additName = "";
        if (!empty($arItem["PRICE"]))
            $additName = " (".$arItem["CURRENCIES"][$arItem["VALUES"]["MAX"]["CURRENCY"]].")";
        ?><div class="filter-item <?=$arItem["CODE"]?>__filter" itemid="<?=$arItem["CODE"]?>">
            <div class="filter-item__delete"><span></span><span></span></div>
            <div class="filter-item__name"><?=$arItem["NAME"].$additName?>:</div><?
        switch ($arItem["DISPLAY_TYPE"])
        {
            case "A":
            case "B":
                if ($arItem["DISPLAY_EXPANDED"] != "Y" || !empty($arItem["PRICE"])):?>
                <div class="filter-item__option range-option">
                    <?=Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_FROM")." "?>
                    <span><?=$arItem["VALUES"]["MIN"]["HTML_VALUE"]?></span>
                    <?=" ".Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_TO")." "?>
                    <span><?=$arItem["VALUES"]["MAX"]["HTML_VALUE"]?></span>
                </div>
                <?else:?>
                <div class="filter-item__option range-option">
                    <?=Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_FROM")." "?>
                    <span><?=$arItem["VALUES"]["MIN"]["HTML_VALUE_EX"]." ".
                        $arResult["SI_UNIT"][$arItem["VALUES"]["MIN"]["HTML_DEGREE"]]["NAME"].$arItem["UNIT_NAME"]?></span>
                    <?=" ".Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_TO")." "?>
                    <span><?=$arItem["VALUES"]["MAX"]["HTML_VALUE_EX"]." ".
                        $arResult["SI_UNIT"][$arItem["VALUES"]["MAX"]["HTML_DEGREE"]]["NAME"].$arItem["UNIT_NAME"]?></span>
                </div>
                <?endif;?>
                <div class="filter-item__range"></div><?
                break;
            case "P":
            case "R":
            case "K":
                foreach ($arItem["VALUES"] as $ar):?>
                    <?if (empty($ar["CHECKED"])) continue;?>
                    <div class="filter-item__option" elid="<?=$ar["CONTROL_ID"]?>">
                        <div class="items-choice__radio">
                            <label>
                                <input class="" type="radio" name="<?=$ar["CONTROL_NAME_ALT"] ?>" checked>
                                <?=$ar["VALUE"]?>
                            </label>
                        </div>
                    </div>
                    <?break;?>
                <?endforeach;?>
                <div class="filter-item__range"></div><?
                break;
            case "G":
            case "H":
                //case "U": // calendar
            default:
                $count = 0;
                foreach ($arItem["VALUES"] as $ar):?>
                    <?if (empty($ar["CHECKED"])) continue;?>
                    <?$count++;?>
                    <div class="filter-item__option <?=$count > 3 ? "hide" : ""?>" elid="<?=$ar["CONTROL_ID"]?>">
                        <div class="option-name"><?=$ar["VALUE"]?></div>
                        <div class="option-delete"><span></span><span></span></div>
                    </div>
                <?endforeach;?>
                <div class="filter-item__option <?=$count < 4 ? "hide" : ""?> filter-item__addit">
                    <?=Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_MORE")?><span class="count"><?=$count - 3?></span>
                </div>
                <div class="filter-item__add"><span></span><span></span></div><?
                break;
        }?></div>
    <?endforeach;?>

    <div class="add-filter">
        <button type="button"><span></span><span><?=Loc::GetMessage("SYMMETRON_CH_FILTER_ADD")?></span></button>
        <div class="add-filter__items">
            <?foreach ($arResult["ITEMS"] as $key => $arItem):?>
                <?
                if (empty($arItem["VALUES"])//|| isset($arItem["PRICE"]))
                    || $arItem["DISPLAY_TYPE"] == "A"
                    && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
                {
                    unset($arResult["ITEMS"][$key]);
                    continue;
                }
                ?>
                <?
                switch ($arItem["DISPLAY_TYPE"])
                {
                    case "A":
                    case "B":
                        $class = "range-choice";
                        break;
                    case "P":
                    case "R":
                    case "K":
                        $class = "radio-choice";
                        break;
                    case "G":
                    case "H":
                    //case "U": // calendar
                    default:
                        $class = "multi-choice";
                        break;
                }
                ?>
                <div class="<?=$class?>" itemid="<?=$arItem["CODE"]?>"><span><?=$arItem["NAME"]?></span></div>
            <?endforeach;?>
        </div>
        <?foreach ($arResult["ITEMS"] as $key => $arItem):?>
            <?
            switch ($arItem["DISPLAY_TYPE"])
            {
                case "A":
                case "B":
                    require(__DIR__."/filter_types/range.php");
                    break;
                case "P":
                case "R":
                case "K":
                    require(__DIR__."/filter_types/radio.php");
                    break;
                case "G":
                case "H":
                    //case "U": // calendar
                default:
                    require(__DIR__."/filter_types/multi.php");
                    break;
            }
            ?>
        <?endforeach;?>
    </div>

</div></div>
<script type="text/javascript">
    var smartFilter = new SymmetronCatalogFilter(
        '<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>',
        '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>',
        <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>
    );
    BX.message({
        SYMMETRON_CH_FILTER_RANGE_FROM: "<?=Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_FROM")?>",
        SYMMETRON_CH_FILTER_RANGE_TO: "<?=Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_TO")?>",
        SYMMETRON_CH_FILTER_RANGE_MORE: "<?=Loc::GetMessage("SYMMETRON_CH_FILTER_RANGE_MORE")?>"
    });
    BX.ready(function() {
        smartFilter.initSliders();
    });
</script>
