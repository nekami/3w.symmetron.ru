<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain                       $APPLICATION
 * @var array                          $arParams
 * @var array                          $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate       $this
 * @var string                         $templateName
 * @var string                         $componentPath
 * @var string                         $templateFolder
 */

$this->setFrameMode(true);

if (isset($arResult['ITEM'])) {
    $arItem = $arResult['ITEM'];
    $areaId = $arResult['AREA_ID'];
    $mainId = $arItem["ID"];
    $itemIds = array(
        'ID'                 => $areaId,
        'PICT'               => $areaId.'_pict',
        'SECOND_PICT'        => $areaId.'_secondpict',
        'PICT_SLIDER'        => $areaId.'_pict_slider',
        'STICKER_ID'         => $areaId.'_sticker',
        'SECOND_STICKER_ID'  => $areaId.'_secondsticker',
        'QUANTITY'           => $areaId.'_quantity',
        'QUANTITY_DOWN'      => $areaId.'_quant_down',
        'QUANTITY_UP'        => $areaId.'_quant_up',
        'QUANTITY_MEASURE'   => $areaId.'_quant_measure',
        'QUANTITY_LIMIT'     => $areaId.'_quant_limit',
        'BUY_LINK'           => $areaId.'_buy_link',
        'BASKET_ACTIONS'     => $areaId.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
        'SUBSCRIBE_LINK'     => $areaId.'_subscribe',
        'COMPARE_LINK'       => $areaId.'_compare_link',
        'PRICE'              => $areaId.'_price',
        'PRICE_OLD'          => $areaId.'_price_old',
        'PRICE_TOTAL'        => $areaId.'_price_total',
        'DSC_PERC'           => $areaId.'_dsc_perc',
        'SECOND_DSC_PERC'    => $areaId.'_second_dsc_perc',
        'PROP_DIV'           => $areaId.'_sku_tree',
        'PROP'               => $areaId.'_prop_',
        'DISPLAY_PROP_DIV'   => $areaId.'_sku_prop',
        'BASKET_PROP_DIV'    => $areaId.'_basket_prop',

        'FAVORITES'          => $mainId.'_favorites',
        'ADD_EXAMPLE_LINK'   => $mainId.'_add_example_link',

    );
    $obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId); ?>

    <tr class="mini-card" id="<?= $areaId ?>" data-entity="item">
        <td colspan="<?= count($arResult["COLUMNS"]); ?>">
            <div class="card-holder">
                <div class="mini-cart__wrapper">
                    <div class="item-info">
                        <div class="item-info__type"><?=$arResult['SECTION_NAME']?></div>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" class="item-info__head"><?= $arItem["NAME"]; ?></a>
                        <div class="item-info__inner">
                            <div class="item-info__actions">
                                <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" class="item-info__fullLink"><?=Loc::getMessage("MCART_MINI_CARD_ITEM_FULL_CARD")?></a>
                                <div class="cardmini-favorite <?= $arResult['IS_FAV'] ? 'act':''?>" id="<?=$itemIds["FAVORITES"]?>" data-id="<?=$arItem["ID"]?>" >
                                    <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="1 -3 36 40" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"></rect>
                                        <style>
                                            #fill-heart {
                                                fill: transparent;
                                            }
                                        </style>
                                        <g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""></path>
                                        <path id="svg_22" d="M-29.97297297297297,2.4594594594594668 "></path><path id="svg_2" d="M19.324324324324337,8.837837837837846 "></path>
                                        <path id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"></path></g>
                                    </svg>
                                    </span>
                                    <?$favLang = $arResult['IS_FAV'] ? "Y" : "N";?>
                                    <span class="fav_status"><?=Loc::getMessage("MCART_MINI_CARD_ITEM_FAVORITE_" . $favLang)?></span>
                                </div>
                                <div class="item-info__question">
                                    <a href="#modal-callback" rel="modal:open">
                                        <span><svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 448">
                                            <title><?=Loc::getMessage("MCART_MINI_CARD_QUESTION")?></title>
                                            <path d="M288,480a10.67,10.67,0,0,1-10.67-10.67v-32H138.67a32,32,0,0,1-32-32v-32H128v32A10.67,10.67,0,0,0,138.67,416H288a10.67,10.67,0,0,1,10.67,10.67v16.92l24.46-24.46a10.67,10.67,0,0,1,7.54-3.13H480a10.67,10.67,0,0,0,10.67-10.67v-256A10.67,10.67,0,0,0,480,138.67H426.67V117.33H480a32,32,0,0,1,32,32v256a32,32,0,0,1-32,32H335.08l-39.54,39.54A10.67,10.67,0,0,1,288,480Z" transform="translate(0 -32)"></path><path d="M192,394.67A10.67,10.67,0,0,1,181.33,384V352H32A32,32,0,0,1,0,320V64A32,32,0,0,1,32,32H373.33a32,32,0,0,1,32,32V320a32,32,0,0,1-32,32H239.08l-39.54,39.54a10.67,10.67,0,0,1-7.54,3.13ZM32,53.33A10.67,10.67,0,0,0,21.33,64V320A10.67,10.67,0,0,0,32,330.67H192a10.67,10.67,0,0,1,10.67,10.67v16.92l24.46-24.46a10.67,10.67,0,0,1,7.54-3.13H373.33A10.67,10.67,0,0,0,384,320V64a10.67,10.67,0,0,0-10.67-10.67Z" transform="translate(0 -32)"></path><path d="M53.33,117.33H170.67v21.33H53.33Z" transform="translate(0 -32)"></path><path d="M53.33,181.33H352v21.33H53.33Z" transform="translate(0 -32)"></path><path d="M53.33,245.33H352v21.33H53.33Z" transform="translate(0 -32)"></path><path d="M330.67,117.33H352v21.33H330.67Z" transform="translate(0 -32)"></path><path d="M288,117.33h21.33v21.33H288Z" transform="translate(0 -32)"></path>
                                        </svg></span>
                                        <span><?=Loc::getMessage("MCART_MINI_CARD_QUESTION")?></span>
                                    </a>
                                </div>
                                <button class="item-sample" id="<?=$itemIds['ADD_EXAMPLE_LINK']?>" data-id="<?=$arItem["ID"]?>"><?=Loc::getMessage("MCART_MINI_CARD_REQUEST_EX")?></button>
                            </div>
                            <div class="item-info__description">
                                <div class="item-info__text">
                                    <div class="item-info__textBold">
                                        <span><?=Loc::getMessage("MCART_MINI_CARD_PART_NUMBER")?>:	<?=$arItem["PROPERTIES"]["ARTICLE"]["VALUE"]?></span>
                                    </div>
                                    <div class="item-info__textDescrition">
                                        <?=$arItem["PREVIEW_TEXT"];?>
                                    </div>
                                    
                                </div>
                                <div class="item-info__img">
                                    <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt=""/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-more">
                        <? $APPLICATION->IncludeComponent(
                            "mcart:catalog.quantity",
                            "",
                            array(
                                "ELEMENT_ID"            => $arItem["ID"],
                                "CATALOG_AVAILABLE"     => $arItem["CATALOG_AVAILABLE"],
                                "CATALOG_QUANTITY"      => $arItem["CATALOG_QUANTITY"],
                                "PRICES"                => $arItem["ITEM_PRICES"],
                                "ITEM_MEASURE_RATIO"    =>
                                    reset($arItem["ITEM_MEASURE_RATIOS"])
                                        ? reset($arItem["ITEM_MEASURE_RATIOS"])["RATIO"] : 1,
                                "ITEM_MEASURE_RATIO_ID" =>
                                    is_array($arItem["ITEM_MEASURE"]) ? $arItem["ITEM_MEASURE"]["ID"] : 0,
                                "MIN_QUANTITY"          => $arItem["PROPERTIES"]["PURCHQTYLOWEST"]["VALUE"],
                                "CALLBACK_FORM"         => "N",
                                "DETAIL_PAGE"           => $arItem["DETAIL_PAGE_URL"],
                                "CATALOG_CAN_BUY_ZERO"  => $arItem["CATALOG_CAN_BUY_ZERO"],
                                "FAVORITE"              => $arResult['IS_FAV']  ? "Y" : "N",
                                "PACK_INFO"          => $arResult["PACK_INFO"],
                                "ITEM_MEASURE"       => $arResult["ITEM_MEASURE"],
                                "BASKET_PRODS"       => $arResult["BASKET_PRODS"]
                            ),
                            $component->getParent()
                        ); ?>

                    </div>
                </div>
            </div>
        </td>
    </tr>
    <?
    unset($arItem, $itemIds);
}