<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if ($arResult["CATALOG_CAN_BUY_ZERO"] == "N" && $arResult["CATALOG_QUANTITY"] == 0) {
    $arResult["CATALOG_AVAILABLE"] = "N";
}

if (!empty($arResult["PROPERTIES"]["PRODUCERID"]["VALUE"])) {
    $res = CIBlockElement::GetList(Array(), array("ID" => $arResult["PROPERTIES"]["PRODUCERID"]["VALUE"], "IBLOCK_ID" => $arResult["PROPERTIES"]["PRODUCERID"]["LINK_IBLOCK_ID"]), false, false, array("NAME"));
    while ($ob = $res->Fetch()) {
        $arResult["PROPERTIES"]["PRODUCERID"]["VALUE"] = $ob["NAME"];
    }
}

if (!empty($arParams['PROPERTY_CODE_FOR_GALER'])) {
    if (!empty($arResult["PROPERTIES"][$arParams["PROPERTY_CODE_FOR_GALER"]]["VALUE"])) {


        $arResult["PROPERTY_CODE_FOR_GALER"] = $arResult["PROPERTIES"][$arParams["PROPERTY_CODE_FOR_GALER"]];
        if (!is_array($arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"])) {
            $arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"] = array($arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"]);
        }

        $res = CFile::GetList(array("FILE_SIZE" => "desc"), array("@ID" => implode(",", $arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"])));
        $arPic = array();
        if (!empty($arResult["DETAIL_PICTURE"]))
            $arPic[] = $arResult["DETAIL_PICTURE"];

        $upload = COption::GetOptionString("main", "upload_dir", "upload");
        while ($res_arr = $res->GetNext()) {

            $res_arr["SRC"] = \CFile::GetPath($res_arr["ID"]);
            $arPic[] = $res_arr;
        }
        $arResult["PROPERTY_CODE_FOR_GALER"]["VALUE"] = $arPic;

    }
}

$arResult["PROPERTY_CODE_SPEC_BLOCK"] = array();
if (!empty($arParams['PROPERTY_CODE_SPEC_BLOCK'])) {

    foreach ($arParams['PROPERTY_CODE_SPEC_BLOCK'] as $elem) {
        if (!empty($elem))
            if (!empty(trim($arResult["DISPLAY_PROPERTIES"][$elem]["DISPLAY_VALUE"])))
                $arResult["PROPERTY_CODE_SPEC_BLOCK"][$elem] = $arResult["DISPLAY_PROPERTIES"][$elem];

    }

}

// display the integer value of the package
if ($arResult["DISPLAY_PROPERTIES"]['SYMPACKAGE']["DISPLAY_VALUE"])
    $arResult["DISPLAY_PROPERTIES"]['SYMPACKAGE']["DISPLAY_VALUE"] = round($arResult["DISPLAY_PROPERTIES"]['SYMPACKAGE']["DISPLAY_VALUE"]);

if ($arResult["DISPLAY_PROPERTIES"]['SYMROHSCONFORMITY']["DISPLAY_VALUE"] == 1)
    $arResult["DISPLAY_PROPERTIES"]['SYMROHSCONFORMITY']["DISPLAY_VALUE"] = GetMessage('SYMROHSCONFORMITY_DISPLAY_VALUE');

$arResult["NEW_CATEGORIES_SPESH"] = array();
foreach ($arParams as $key => $val) {
    $pos = strpos($key, "PROPERTY_CODE_SPEC_CATEGORY");
    if ($pos === false) {

    } else {
        $pos2 = strpos($key, "NAME");
        $pos3 = strpos($key, "~");
        if ($pos2 === false && $pos3 === false) {
            if (!empty($arParams[$key . "_NAME"])) {
                $raCategory = array();
                $raCategory["NAME"] = $arParams[$key . "_NAME"];

                $raCategory["DATA"] = $arParams[$key];
                if (!empty($raCategory["DATA"])) {

                    foreach ($raCategory["DATA"] as $elem) {
                        if (!empty($elem))
                            if (!empty(trim($arResult["DISPLAY_PROPERTIES"][$elem]["DISPLAY_VALUE"])))
                                $raCategory["RES"][$elem] = $arResult["DISPLAY_PROPERTIES"][$elem];

                    }

                }
                $arResult["NEW_CATEGORIES_SPESH"][] = $raCategory;


            }

        }

    }
}


if (!empty($arParams['PROPERTY_CODE_IBLOCK_DOC'])) {
    $arPropB = $arParams['PROPERTY_CODE_IBLOCK_DOC'];
    if (!empty($arResult["PROPERTIES"][$arPropB]["VALUE"])) {
        $propFiles = $arResult["PROPERTIES"][$arPropB]["VALUE"];
        $arResult["PROPERTY_CODE_IBLOCK_DOC"] = array();

        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PROPERTY_LINK_TO_RES", "PROPERTY_FILE");
        $arFilter = Array("IBLOCK_ID" => IntVal($arResult["PROPERTIES"][$arPropB]["LINK_IBLOCK_ID"]), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $propFiles);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        while ($ob = $res->Fetch()) {
            if (!empty($ob["PROPERTY_FILE_VALUE"])) {
                $ob["VALUE"] = CFile::GetFileArray($ob["PROPERTY_FILE_VALUE"]);
                $arResult["PROPERTY_CODE_IBLOCK_DOC_FILES"][] = $ob;


            } /*elseif(!empty($ob["PROPERTY_LINK_TO_RES_VALUE"])){
                $arFileLink = CFile::MakeFileArray($ob["PROPERTY_LINK_TO_RES_VALUE"]);
                $ob["VALUE"] = $arFileLink;

                $arResult["PROPERTY_CODE_IBLOCK_DOC"][] = $ob;
            }*/
        }


    }

}


if (!empty($arParams['PROPERTY_CODE_OTR_S'])) {
    $arPropO = $arParams['PROPERTY_CODE_OTR_S'];
    if (!empty($arResult["PROPERTIES"][$arPropO]["VALUE"])) {


        $propOtr = $arResult["PROPERTIES"][$arPropO]["VALUE"];
        $arResult["PROPERTY_CODE_OTR_S"] = array();

        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_LINK_O", "PROPERTY_COLOR_TEXT");
        $arFilter = Array("IBLOCK_ID" => IntVal($arResult["PROPERTIES"][$arPropO]["LINK_IBLOCK_ID"]), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $propOtr);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        $arFilesIDs = array();
        while ($ob = $res->Fetch()) {

            $ob["PREVIEW_PICTURE"] = CFile::GetFileArray($ob["PREVIEW_PICTURE"]);
            $arResult["PROPERTY_CODE_OTR_S"][] = $ob;
        }

        $res = CFile::GetList(array("FILE_SIZE" => "desc"), array("@ID" => implode(",", $arFilesIDs)));
    }
}

//specifications main

$arResult["SPEC_MAIN"] = [];
if (!empty($arParams["PROPERTY_CODE_SPEC_MAIN"])) {
    foreach ($arParams["PROPERTY_CODE_SPEC_MAIN"] as $text) {
        if (!empty($arResult["PROPERTIES"][$text]["~VALUE"]["TEXT"])) {
            $arResult["SPEC_MAIN"][] = $arResult["PROPERTIES"][$text]["~VALUE"]["TEXT"];
        } else {
            foreach ($arResult["PROPERTIES"][$text]["~VALUE"] as $value) {
                $arResult["SPEC_MAIN"][] = $value["TEXT"];
            }
        }
    }
}


//get measure and packing info

if (\Bitrix\Main\Loader::includeModule("mcart.basketsymmetron")) {
    $arPackingMeasureData = CBasketSymmetron::getInfoMeasureAndPacking($arResult["ID"], $arParams["IBLOCK_ID"]);

    // display the integer value of the package
    if ($arPackingMeasureData["PACKING"][$arResult["ID"]]['SYMPACKAGE'])
        $arPackingMeasureData["PACKING"][$arResult["ID"]]['SYMPACKAGE'] = round($arPackingMeasureData["PACKING"][$arResult["ID"]]['SYMPACKAGE']);

    $arResult["PACK_INFO"] = $arPackingMeasureData["PACKING"][$arResult["ID"]];
    $arResult["ITEM_MEASURE"] = $arPackingMeasureData["MEASURES"][$arResult["ID"]];
}

//no active brands

$arBrand = $arResult["DISPLAY_PROPERTIES"]["PRODUCERID"];
$brandIblockId = $arBrand["LINK_IBLOCK_ID"];
if (empty($arBrand["LINK_ELEMENT_VALUE"]) && !empty($arBrand["~VALUE"])
    && in_array("PRODUCERID", $arParams["PROPERTY_CODE"])) {
    $NoActiveBrand = $arBrand["~VALUE"];
}

//get no active brand
$NoActiveBrands = "";
if (!empty($NoActiveBrand)) {
    $arSelect = ["ID", "NAME"];
    $arFilter = [
        "IBLOCK_ID" => IntVal($brandIblockId),
        "ID" => $NoActiveBrand,
        "ACTIVE" => "N",
        "PROPERTY_ACTIVE" => "Y"
    ];
    $rsNoActiveBrands = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($arNoActiveBrand = $rsNoActiveBrands->Fetch()) {
        $NoActiveBrands = $arNoActiveBrand["NAME"];
    }
}

$arBrand = &$arResult["DISPLAY_PROPERTIES"]["PRODUCERID"];
if (empty($arBrand["LINK_ELEMENT_VALUE"]) && !empty($arBrand["~VALUE"])
    && in_array("PRODUCERID", $arParams["PROPERTY_CODE"])) {
    $arBrand["DISPLAY_VALUE"] = $NoActiveBrands;
}

//get products from basket
$arResult["BASKET_PRODS"] = \Bitrix\Sale\Basket::loadItemsForFUser(
    \Bitrix\Sale\Fuser::getId(),
    \Bitrix\Main\Context::getCurrent()->getSite()
);

//get hints

$arHints = [];
if (CModule::IncludeModule("mcart.sortcatalogsm")) {
    $arHints = Mcart\SortCatalogSM\Helper::getHints($arResult["IBLOCK_SECTION_ID"]);
}

$arResult["PROPERTIES_HINTS"] = $arHints;


// get information for declination

$arWords = [];

$val = trim($arResult["ITEM_MEASURE"]["MEASURE_TITLE"]);
if (!empty($val))
    $arWords[] = strtolower($val);

$val = trim($arResult["PACK_INFO"]["UPAK"]["VALUE"]);
if (!empty($val))
    $arWords[] = strtolower($val);

$arWords[] = GetMessage("CT_BCS_TPL_MESS_SHT");
$arWords[] = GetMessage("CT_BCS_TPL_MESS_UNITS");
$arWords[] = GetMessage("CT_BCS_TPL_MESS_PRODUCTS");

$arResult["DECLINATION_OBJ"] = new \Mcart\BasketSymmetron\SymmetronDeclination($arWords);

$arFilterSection = ["ID" => $arResult["IBLOCK_SECTION_ID"], "ACTIVE" => "Y"];
$breed = [];
$rsSectionInfo = \CIBlockSection::GetList([], $arFilterSection, false, ["ID", "NAME", "LEFT_MARGIN", "IBLOCK_ID", "RIGHT_MARGIN"]);
if ($arSectionInfo = $rsSectionInfo->Fetch()) {
    $rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), array(
        'IBLOCK_ID' => $arSectionInfo["IBLOCK_ID"],
        "<=LEFT_BORDER" => $arSectionInfo["LEFT_MARGIN"],
        ">=RIGHT_BORDER" => $arSectionInfo["RIGHT_MARGIN"],
    ));

    while ($arSect = $rsSect->Fetch()) {
        $breed[] = $arSect['NAME'];
    }
}
$arParams["BREAD"] = join("/", $breed);

$res = CIBlockElement::GetList(
    array(
        $arParams["ELEMENT_SORT_FIELD"] => $arParams["ELEMENT_SORT_ORDER"],
        $arParams["ELEMENT_SORT_FIELD2"] => $arParams["ELEMENT_SORT_ORDER2"],
    ),
    array(
        'IBLOCK_ID' => $arParams["IBLOCK_ID"],
        'ACTIVE' => 'Y',
        'SECTION_ID' => $arResult['IBLOCK_SECTION_ID']
    ),
    false,
    array(
        'nElementID' => $arResult['ID'],
        'nPageSize' => 1
    ),
    array(
        "ID",
        "IBLOCK_ID",
        "CODE",
        "PROPERTY_ARTICLE"
    )
);
$arResult["MCART_URL"] = explode('/', $arParams['CURRENT_BASE_PAGE']);
unset($arResult["MCART_URL"][count($arResult["MCART_URL"]) - 1]);
unset($arResult["MCART_URL"][count($arResult["MCART_URL"]) - 1]);
$arResult["MCART_URL"] = join('/', $arResult["MCART_URL"]);

$arResult['NEAR_ELEMENTS'] = [];
$nearElementsSide = 'LEFT';
while ($arElem = $res->GetNext()) {
    if ($arElem['ID'] == $arResult['ID']) {
        $nearElementsSide = 'RIGHT';
        continue;
    }
    $arResult['NEAR_ELEMENTS'][$nearElementsSide] = $arElem;
}