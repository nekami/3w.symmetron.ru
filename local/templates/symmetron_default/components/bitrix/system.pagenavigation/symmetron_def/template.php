<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$pageElemCount_All = $_SESSION["COUNT_ELEM_ON_PAGE_ALL"];
$pageElemCount = $_SESSION["COUNT_ELEM_ON_PAGE"][$_SESSION["COUNT_ELEM_ON_PAGE_SECTION_ID"]] ? : $pageElemCount_All;
$this->setFrameMode(true);
/*
if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
*/
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$href = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=";
?>
<div class="bx-pagination">
    <?if ($this->getComponent()->getParent()->arParams["LAZY_LOAD"] == "Y"
        && $arResult["NavPageNomer"] < $arResult["nEndPage"]):?>
    <?$randKey = "nav".randString(10);?>
    <div class="bx-pagination-container more">
        <noindex>
            <a class="load_more" id="<?=$randKey?>" href="<?=$href.($arResult["NavPageNomer"]+1)."&action=showMore"?>" onclick="">
                <?$count = $arResult["NavRecordCount"] - $arResult["NavLastRecordShow"]?>
                <?$size = $arResult["NavPageSize"] < $count ? $arResult["NavPageSize"] : $count?>
                <?=Loc::GetMessage("round_nav_more", ["#SIZE#" => $size, "#COUNT#" => $count]);?>
            </a>
        </noindex>
        <script>
            new SymmetronDefNavigation(
                "<?=$randKey?>",
                <?=CUtil::PhpToJSObject($this->getComponent()->getParent()->arParams["NAV_SHOW_MORE_RULE"])?>
            );
        </script>
    </div>
    <?endif;?>

    <?$href = str_replace("&action=showMore", "", $href);?>
    <?$strNavQueryStringFull = str_replace(["action=showMore", "&action=showMore"], ["", ""], $strNavQueryStringFull);?>
	<div class="bx-pagination-container row">

		<ul>
<?if($arResult["bDescPageNumbering"] === true):?>


	<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["bSavePage"]):?>
			<li class="bx-pag-prev"><a href="<?=$href?><?=($arResult["NavPageNomer"]+1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
			<li class=""><a href="<?=$href?><?=($arResult["NavPageNomer"]+1)?>"><span>1</span></a></li>
		<?else:?>
			<?if (($arResult["NavPageNomer"]+1) == $arResult["NavPageCount"]):?>
				<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
			<?else:?>
				<li class="bx-pag-prev"><a href="<?=$href?><?=($arResult["NavPageNomer"]+1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
			<?endif?>
			<li class=""><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span>1</span></a></li>
		<?endif?>
	<?else:?>
			<li class="bx-pag-prev"><span><?echo GetMessage("round_nav_back")?></span></li>
			<li class="bx-active"><span>1</span></li>
	<?endif?>

	<?
	$arResult["nStartPage"]--;
	while($arResult["nStartPage"] >= $arResult["nEndPage"]+1):
	?>
		<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<li class="bx-active"><span><?=$NavRecordGroupPrint?></span></li>
		<?else:?>
			<li class=""><a href="<?=$href?><?=$arResult["nStartPage"]?>"><span><?=$NavRecordGroupPrint?></span></a></li>
		<?endif?>

		<?$arResult["nStartPage"]--?>
	<?endwhile?>

	<?if ($arResult["NavPageNomer"] > 1):?>
		<?if($arResult["NavPageCount"] > 1):?>
			<li class=""><a href="<?=$href?>1"><span><?=$arResult["NavPageCount"]?></span></a></li>
		<?endif?>
			<li class="bx-pag-next"><a href="<?=$href?><?=($arResult["NavPageNomer"]-1)?>"><span><?echo GetMessage("round_nav_forward")?></span></a></li>

	<?else:?>
		<?if($arResult["NavPageCount"] > 1):?>
			<li class="bx-active"><span><?=$arResult["NavPageCount"]?></span></li>
		<?endif?>
			<li class="bx-pag-next"><span><?echo GetMessage("round_nav_forward")?></span></li>
	<?endif?>

<?else:?>

	<?if ($arResult["NavPageNomer"] > 1):?>
		<?if($arResult["bSavePage"]):?>
			<li class="bx-pag-prev"><a href="<?=$href?><?=($arResult["NavPageNomer"]-1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
			<li class=""><a href="<?=$href?>1"><span>1</span></a></li>
		<?else:?>
			<?if ($arResult["NavPageNomer"] > 2):?>
				<li class="bx-pag-prev"><a href="<?=$href?><?=($arResult["NavPageNomer"]-1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
			<?else:?>
				<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
			<?endif?>
			<li class=""><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span>1</span></a></li>
		<?endif?>
	<?else:?>
			<li class="bx-pag-prev"><span><?echo GetMessage("round_nav_back")?></span></li>
			<li class="bx-active"><span>1</span></li>
	<?endif?>

	<?
	$arResult["nStartPage"]++;
	while($arResult["nStartPage"] <= $arResult["nEndPage"]-1):
	?>
		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<li class="bx-active"><span><?=$arResult["nStartPage"]?></span></li>
		<?else:?>
			<li class=""><a href="<?=$href?><?=$arResult["nStartPage"]?>"><span><?=$arResult["nStartPage"]?></span></a></li>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["NavPageCount"] > 1):?>
			<li class=""><a href="<?=$href?><?=$arResult["NavPageCount"]?>"><span><?=$arResult["NavPageCount"]?></span></a></li>
		<?endif?>
			<li class="bx-pag-next"><a href="<?=$href?><?=($arResult["NavPageNomer"]+1)?>"><span><?echo GetMessage("round_nav_forward")?></span></a></li>
    <?else:?>
		<?if($arResult["NavPageCount"] > 1):?>
			<li class="bx-active"><span><?=$arResult["NavPageCount"]?></span></li>
		<?endif?>
			<li class="bx-pag-next"><span><?echo GetMessage("round_nav_forward")?></span></li>
	<?endif?>
<?endif?>


            <li class="bx-pag-form">
                    <form method="post" id="select_count">
                        <label>
                          <input name="COUNT_ELEM_ON_PAGE" type="text" value="<?echo $pageElemCount;?>" class="input-form-value" readonly>
                        </label>
                        <ul onchange="document.getElementById('select_count').submit()">
                            <li class="active" value="<?=$pageElemCount_All?>" <?if($pageElemCount == pageElemCount_All):?>selected<?endif;?>>
                                50
                            </li>
                            <li value="150" <?if($pageElemCount == 150):?>selected<?endif;?>>150</li>
                            <li value="250" <?if($pageElemCount == 250):?>selected<?endif;?>>250</li>
                        </ul>
                    </form>

            </li>
            <li class="bx-pag-form__link">
                на стр.
            </li>
		</ul>
        <?if ($arResult["bShowAll"]):?>
           <div style="visibility: hidden;"><noindex>
                <?if ($arResult["NavShowAll"]):?>
                    <li class="bx-pag-all"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><span><?echo GetMessage("round_nav_pages")?></span></a></li>
                <?else:?>
                    <li class="bx-pag-all"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><span><?echo GetMessage("round_nav_all")?></span></a></li>
                <?endif?>
               </div></noindex>
        <?endif?>
		<div style="clear:both"></div>
	</div>
</div>
<style>
    .bx-page-visibility {
        visibility: hidden;
    }
</style>
<script>
    var formId = document.querySelector('#select_count');
    var input = formId.querySelector('.input-form-value');
    var list = formId.querySelector('ul');
    var body = document.querySelector('body');

    input.addEventListener('click', function(event){
        list.style.display = 'flex';

    });

    list.addEventListener('click', function(event) {
        let target = event.target;
        let value = target.getAttribute('value');
        input.setAttribute('value', value);
        list.style.display = 'none';
        formId.submit();
    });

    document.addEventListener('mouseup', function () {
        list.style.display = 'none';
    });
</script>
