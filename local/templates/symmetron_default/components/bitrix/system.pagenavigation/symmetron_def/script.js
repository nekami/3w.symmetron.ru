function SymmetronDefNavigation(linkId, rule) {
    this.rule = rule;
    this.bind(linkId);
}

SymmetronDefNavigation.prototype.bind = function (linkId) {
    var _this = this;

    BX.ready(function () {
        $("#" + linkId).on("click", function () {
            _this.showMore(this)
            return false;
        })
    })
};

SymmetronDefNavigation.prototype.showMore = function (linkElement) {
    var _this = this;
    
    //var temporary = $(linkElement).parent().next().find("ul > li.bx-active").first().next()
    //temporary = temporary.children("a").attr("onclick").match(/bxajaxid=(.{32})/)
    var temporary = $("#analogue").parent().attr("id").split("_")

    if (temporary.length === 2) {
        var bxajaxid = temporary[1]
        var url = $(linkElement).attr("href") + "&bxajaxid=" + bxajaxid

        BX.ajax.loadJSON(
            url,
            function (data) {

                //var allTable = $($(data.items)[0])
                var allTable = data.items.split("<!-- items-split -->")
                //console.log(allTable);
                //var leftElements = allTable.find("#left_elements")
                //var rightElements = allTable.find("#right_elements")
                //data.left = leftElements[0].innerHTML
                //data.items = rightElements[0].innerHTML
                data.left = allTable[0]
                data.items = allTable[2]

                container = $("#comp_" + bxajaxid);

                _this.rule["left"] = ".left-container"
	
                //console.log(data.left)
                for (var code in _this.rule) {
                    data[code] = data[code].replace(/comp_/g, "comp_" + bxajaxid)
                    //data[code] = data[code].replace(/action=showMore/g, "")
                    if (data[code] !== undefined){
				container.find(_this.rule[code]).replaceWith(data[code])
			}
                        
                }

                var tableAtr = $('.analogue-table .mini-card__select');
                var tableAtrLeft = $('.analogue-table__left .mini-card__select')
                tableAtr.each(function(i){
                    var tableAtrHeight = $(this).height()
                    //console.log(tableAtrLeft[i])
                    $(tableAtrLeft[i]).css("height", tableAtrHeight)
                })


                //cards positions for scroll
                var tableScroll = $('.scroll-table').scrollLeft()
                if (tableScroll >= 0) {
                    $('.mini-cart__wrapper').css({
                        'margin-left': tableScroll
                    })
                }
            }
        );
    }
}