<?
$MESS["SEMINAR_TOPIC"] = "Темы семинара:";
$MESS["REPORTER"] = "Докладчик:";
$MESS["PHONE"] = "Тел:";
$MESS["REGISTRATION"] = "Регистрация";
$MESS["MANUFACTURERS"] = "Производители";
$MESS["ARTICLE"] = "Код товара производителя";
$MESS["MANUFACTURER"] = "Производитель";
$MESS["DESCRIPTION"] = "Описание";
$MESS["AVAILABILITY"] = "Наличие";
$MESS["PRICE_FROM"] = "Цена от";
$MESS["RUB_PCS"] = "руб./шт.";
?>