<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="events">
	<h1 class="events__title">Мероприятия</h1>       
    <div class="events-container">
	
        <div class="events-wrapper">
		
		<? foreach($arResult["ITEMS"] as $arItem) { ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
				
		<div class="event-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			
            <div class="event-date">
				
				<? if (!empty($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]) && !empty($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"])) 
				{ 
					$month1 = FormatDate("f", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]));
					$month2 = FormatDate("f", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"]));
					$year1  = FormatDate("Y", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]));
					$year2  = FormatDate("Y", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"]));
				?>		
					<div class="event-date__month">						
						<?						
						if ($month1 == $month2)
						{
							echo "<p>".FormatDate("f Y", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))."</p>";
						}
						else if (($month1 != $month2) && ($year1 == $year2))
						{
							echo "<p>".FormatDate("f", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))." ...</p><p>".FormatDate("f Y", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</p>";
						}
						else if ($year1 != $year2)
						{
							echo "<p>".FormatDate("f Y", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))." ...</p><p>".FormatDate("f Y", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</p>";
						}
						?>
					</div>
					<div class="event-date__day">
						<div class="event-date__day-wrapper">						
							<?						
							if (($month1 == $month2) && ($year1 == $year2))
							{
								echo "<span>".FormatDate("j", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))."</span><span>".FormatDate("j", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</span>";
							}
							else
							{
								echo "<span>".FormatDate("j", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))." ... ".FormatDate("j", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</span>";
							}						
							?>
						</div>
					</div>			
				<? } else { ?> 			
					<div class="event-date__month">
						<p>Пожалуйста, заполните дату начала и завершения мероприятия</p> 
					</div>
				<? } ?>		
			</div>
								
            <div class="event-description">
                
                <h4 class="event-description__name">
					<a href='<?=$arItem["DETAIL_PAGE_URL"]?>'>
						<?=$arItem["NAME"]?>
					</a>
				</h4>
					
				<? if (strlen($arItem["PREVIEW_TEXT"])>0) { ?>
					<p class="event-description__text"><?=$arItem["PREVIEW_TEXT"];?></p>
				<? } ?>	           
					
				<? if ( !empty($arItem['DISPLAY_LINK_PROPERTIES']) ) { ?>
					<div class="event-cloud-tags cloud-tags">
						<? foreach ($arItem['DISPLAY_LINK_PROPERTIES'] as $propertyCode => $displayPropertyValue) { ?>
							
							<a href="<?=$arItem["FILTER_LIST_URL"]?>filter/<?=$displayPropertyValue["PROPERTY"]?>-is-<?=strtolower($displayPropertyValue["CODE"]);?>/apply/" class="cloud-tags__link">
									
								<?=$displayPropertyValue["NAME"]?>
							</a>
							
						<? } ?>
					</div>
				<? } ?>					
					
            </div>
						
            <div class="event-address">
			
				<? if (!empty($arItem["DISPLAY_PROPERTIES"]["cities"]["DISPLAY_VALUE"]) || !empty($arItem["DISPLAY_PROPERTIES"]["location"]["DISPLAY_VALUE"])) { ?>
					<span class="event-address__city">
						<?=$arItem["DISPLAY_PROPERTIES"]["cities"]["DISPLAY_VALUE"]?>
							
						<? if (!empty($arItem["DISPLAY_PROPERTIES"]["cities"]["DISPLAY_VALUE"]) && !empty($arItem["DISPLAY_PROPERTIES"]["location"]["DISPLAY_VALUE"])) { ?>,<? } ?>
							
						<?=$arItem["DISPLAY_PROPERTIES"]["location"]["DISPLAY_VALUE"]?>
					</span>
				<? } ?>
				
				<? if (!empty($arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])) { ?>
					<span class="event-address__place">
						<?=$arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]?>
					</span>
				<? } ?>	
				
				<? if (!empty($arItem["DISPLAY_PROPERTIES"]["phone"]["VALUE"])) { ?>
					<span class="event-address__phone">Тел: <a href="tel:<?=$arItem["DISPLAY_PROPERTIES"]["phone"]["VALUE"]?>"><?=$arItem["DISPLAY_PROPERTIES"]["phone"]["VALUE"]?></a></span>
				<? } ?>
				
				<? if (!empty($arItem["DISPLAY_PROPERTIES"]["cost"]["DISPLAY_VALUE"])) { ?>
					<p class="event-address__price">
						<?=$arItem["DISPLAY_PROPERTIES"]["cost"]["DISPLAY_VALUE"]?>
					</p>
				<? } ?>	
				
				<? if (!empty($arItem["DISPLAY_PROPERTIES"]["link_registration"]["VALUE"])) { ?>
					<a href='<?=$arItem["DISPLAY_PROPERTIES"]["link_registration"]["VALUE"]?>' class="event-address__reg" target="_blank">
						Регистрация
					</a>
				<? } ?>	
				
            </div>
        </div>

		<? } ?>
		
        </div>
		
		<?
		if($arParams["USE_FILTER"]=="Y") 
		{					
			echo htmlspecialchars_decode($arParams["CONTENT_FILTER"]);
		} 
		?>
		
		<? if ($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>
			<?=$arResult["NAV_STRING"];?>
		<? } ?>	
	</div>
</div>