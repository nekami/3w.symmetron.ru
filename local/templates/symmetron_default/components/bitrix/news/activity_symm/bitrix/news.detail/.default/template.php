<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
ob_start();
?>
	
<div class="event event-detail">
	<div class="events-detail">
		<div class="event-item">
			
			<div class="event-date">
			
			<? if (!empty($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"])) 
			{ 
				$month1 = FormatDate("f", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]));
				$month2 = FormatDate("f", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"]));
				$year1  = FormatDate("Y", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]));
				$year2  = FormatDate("Y", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"]));
			?>		
				<div class="event-date__month">						
					<?						
					if ($month1 == $month2)
					{
						echo "<p>".FormatDate("f Y", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))."</p>";
					}
					else if (($month1 != $month2) && ($year1 == $year2))
					{
						echo "<p>".FormatDate("f", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))." ...</p><p>".FormatDate("f Y", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</p>";
					}
					else if ($year1 != $year2)
					{
						echo "<p>".FormatDate("f Y", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))." ...</p><p>".FormatDate("f Y", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</p>";
					}
					?>
				</div>
				<div class="event-date__day">
					<div class="event-date__day-wrapper">						
						<?						
						if (($month1 == $month2) && ($year1 == $year2))
						{
							echo "<span>".FormatDate("j", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))."</span><span>".FormatDate("j", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</span>";
						}
						else
						{
							echo "<span>".FormatDate("j", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date1"]["VALUE"]))." ... ".FormatDate("j", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["date2"]["VALUE"]))."</span>";
						}						
						?>
					</div>
				</div>			
			<? } else { ?> 			
				<div class="event-date__month">
					<p>Пожалуйста, заполните дату начала и завершения мероприятия</p> 
				</div>
			<? } ?>	
			</div>
			
			<div class="event-description">
				
				<h1 class="event-description__name"><?=$arResult["NAME"]?></h1>
				
				<? if (strlen($arResult["DETAIL_TEXT"])>0) { ?>				
					<p class="event-description__text"><?=$arResult["DETAIL_TEXT"];?></p>					
				<? } else if (strlen($arResult["PREVIEW_TEXT"])>0) { ?>
					<p class="event-description__text"><?=$arResult["PREVIEW_TEXT"];?></p>
				<? } ?>
									
				<? if ( !empty($arResult['DISPLAY_LINK_PROPERTIES']) ) { ?>
					<div class="event-cloud-tags cloud-tags">
						<? foreach ($arResult['DISPLAY_LINK_PROPERTIES'] as $propertyCode => $displayPropertyValue) { ?>
						
							<a href="<?=$arResult["FILTER_LIST_URL"]?>filter/<?=$displayPropertyValue["PROPERTY"]?>-is-<?=strtolower($displayPropertyValue["CODE"]);?>/apply/" class="cloud-tags__link">
								
								<?=$displayPropertyValue["NAME"]?>
							</a>
						
						<? } ?>
					</div>
				<? } ?>					
								
				<div class="event-content">
					
					<? if (!empty($arResult["DISPLAY_PROPERTIES"]["themes"]["DISPLAY_VALUE"])) { ?>					
						<h4 class="event-content__title"><?=GetMessage("SEMINAR_TOPIC")?></h4>
						<ul class="event-theme-list">
							<? if (is_array($arResult["DISPLAY_PROPERTIES"]["themes"]["DISPLAY_VALUE"])) { ?>
								<? foreach ($arResult["DISPLAY_PROPERTIES"]["themes"]["DISPLAY_VALUE"] as $theme) { ?>
									<li class="event-theme-item"><?=$theme?></li>
								<? } ?>
							<? } else { ?>
								<li class="event-theme-item"><?=$arResult["DISPLAY_PROPERTIES"]["themes"]["DISPLAY_VALUE"]?></li>
							<? } ?>
						</ul>
					<? } ?>					
					
					<div class="speakers">
						<? foreach ($arResult["DISPLAY_PROPERTIES"]["REPORTER"]["VALUE"] as $reporter_code) { ?>
							<div class="speakers__item">
								<div class="speakers__item-img">
									<img src='<?=$arResult["REPORTER"][$reporter_code]["UF_FILE"]?>'>
								</div>
								<span class="speakers__item-name"><?=GetMessage("REPORTER")?>
									<b><?=$arResult["REPORTER"][$reporter_code]["UF_NAME"]?>,</b>
								</span>
								<p class="speakers__item-descr"><?=$arResult["REPORTER"][$reporter_code]["UF_DESCRIPTION"]?></p>
							</div>
						<? } ?>						
					</div>
					
					
					
					
				</div>
			</div>		
			<div class="event-address">
				<? if (!empty($arResult["DISPLAY_PROPERTIES"]["cities"]["DISPLAY_VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["location"]["DISPLAY_VALUE"])) { ?>
					<span class="event-address__city">
						<?=$arResult["DISPLAY_PROPERTIES"]["cities"]["DISPLAY_VALUE"]?>
						
						<? if (!empty($arResult["DISPLAY_PROPERTIES"]["cities"]["DISPLAY_VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["location"]["DISPLAY_VALUE"])) { ?>,<? } ?>
						
						<?=$arResult["DISPLAY_PROPERTIES"]["location"]["DISPLAY_VALUE"]?>
					</span>
				<? } ?>
				<? if (!empty($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])) { ?>
					<span class="event-address__place">
						<?=$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]?>
					</span>
				<? } ?>
				<? if (!empty($arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"])) { ?>
					<span class="event-address__phone">
						<?=GetMessage("PHONE")?> <a href='tel:<?=$arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"]?>'><?=$arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"]?></a>
					</span>
				<? } ?>
				<? if (!empty($arResult["DISPLAY_PROPERTIES"]["cost"]["DISPLAY_VALUE"])) { ?>
					<p class="event-address__price">
						<?=$arResult["DISPLAY_PROPERTIES"]["cost"]["DISPLAY_VALUE"]?>
					</p>
				<? } ?>
				<? if (!empty($arResult["DISPLAY_PROPERTIES"]["link_registration"]["VALUE"])) { ?>
					<a href='<?=$arResult["DISPLAY_PROPERTIES"]["link_registration"]["VALUE"]?>' class="event-address__reg" target="_blank">
						<?=GetMessage("REGISTRATION")?>
					</a>
				<? } ?>
			</div>
		</div>
	</div>
</div>

EVENTS<?=$arResult["STR_ARTICLES_ID"]?>

<? if (!empty($arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"])) { ?>
	<div class="manufacturer">
		<h2 class="manufacturer__title"><?=GetMessage("MANUFACTURERS")?></h2>
		<ul class="manufacturer__list">	
			<? foreach ($arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"] as $item) { ?> 
				<li class="manufacturer__item">
					<a href='<?=$item["DETAIL_PAGE_URL"]?>' title='<?=$item["NAME"]?>' class="manufacturer__link">
						<?=$item["NAME"]?>
					</a>
				</li>
			<? } ?>
		</ul>
	</div>
<? } ?>

<? if (!empty($arResult["DISPLAY_PROPERTIES"]["components"]["VALUE"])) { ?>
	<section class="components">
		<table class="components-table">
			<tr class="components-head">
				<th class="components-head__code"><?=GetMessage("ARTICLE")?></th>
				<th class="components-head__manufacture"><?=GetMessage("MANUFACTURER")?></th>
				<th class="components-head__descr"><?=GetMessage("DESCRIPTION")?></th>
				<th class="components-head__avail"><?=GetMessage("AVAILABILITY")?></th>
				<th class="components-head__price"><?=GetMessage("PRICE_FROM")?></th>
			</tr>					
			<? foreach ($arResult["DISPLAY_PROPERTIES"]["components"]["VALUE"] as $item) { ?>
				<tr class="components-element">
					<td>
						<a class="components-element__link" href='<?=$item["DETAIL_PAGE_URL"]?>'>
							<?=$item["~PROPERTY_ARTICLE_VALUE"]?>
						</a>
					</td>
					<td>
						<? if ( !empty($arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"][$item["PROPERTY_PRODUCERID_VALUE"]]["NAME"]) ) { ?>				
							<?=$arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"][$item["PROPERTY_PRODUCERID_VALUE"]]["NAME"]?>
						<? } ?>
					</td>
					<td>				
						<? if ( !empty($item["~PROPERTY_DESCRIPTION_VALUE"]) ) { ?>
							<?=$item["~PROPERTY_DESCRIPTION_VALUE"]?>
						<? } ?>				
					</td>			
					<td class="component-element__quantity">			
						<? if ($item["PROPERTY_ACTIVE_STATUS_ENUM_ID"] == 328) { ?> 
							<span class="components-element__availability"></span>					
						<? } else { ?> 
							<span class="components-element__no-availability"></span>
						<? } ?>
						<span><?=$item["~PROPERTY_ACTIVE_STATUS_VALUE"]?></span>
					</td>			
					<td>
						<? if ( !empty($item["~PROPERTY_RETAIL_PRICE_VALUE"]) ) { ?>
							<?=$item["~PROPERTY_RETAIL_PRICE_VALUE"]?> <?=GetMessage("RUB_PCS")?>
						<? } ?>
					</td>
				</tr>
			<? } ?>			
		</table>
	</section>
<? } ?>

<?
$this->__component->SetResultCacheKeys(array("CACHED_TPL"));
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();						
?>