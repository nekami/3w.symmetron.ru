<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$resComponents = CIBlockElement::GetList(
	["PROPERTY_PRODUCERID"=>"DESC"],
	[   "IBLOCK_CODE" => "pbd", 
		"!PROPERTY_ARTICLE" => false, 
		"PROPERTY_PRODUCERID" => array_keys($arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"]),
		[
			"LOGIC" => "OR",
			["ID" => $arResult["DISPLAY_PROPERTIES"]["components"]["VALUE"]],
			["SECTION_ID" => $arResult["DISPLAY_PROPERTIES"]["sections"]["VALUE"]]
		]
	],
	false,
	["nTopCount" => 10],
	["ID", "DETAIL_PAGE_URL", "PROPERTY_ARTICLE", "PROPERTY_PRODUCERID", "PROPERTY_DESCRIPTION", "PROPERTY_ACTIVE_STATUS", "PROPERTY_RETAIL_PRICE"]
);

$arComponents = [];

while($arComponent = $resComponents->GetNext())
{
	$arComponents[$arComponent["ID"]] = $arComponent;
}

$arResult["DISPLAY_PROPERTIES"]["components"]["VALUE"] = $arComponents;

unset($arComponents);

//////////////////////////////////////////////////////////////////////////////////////

if (CModule::IncludeModule('highloadblock')) {
	
	$arResult["REPORTER"] = [];

    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(ID_HIGHLOAD_REPORTER)->fetch();
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $hlDataClass = $hldata["NAME"]."Table";

    $result = $hlDataClass::getList(array(
                "select" => array("ID", "UF_NAME", "UF_XML_ID", "UF_DESCRIPTION", "UF_FILE"),
                "filter" => array(),
    ));

    while ($res = $result->fetch()) 
	{
		$res["UF_FILE"] = CFile::GetPath($res["UF_FILE"]);		
		$arResult["REPORTER"][$res["UF_XML_ID"]] = $res;
    }
}

$arResult['FILTER_LIST_URL'] = str_replace ('//','/',CComponentEngine::makePathFromTemplate($arResult["LIST_PAGE_URL"]));

if ( !empty($arParams['LINK_PROPERTIES']) ) 
{	
	$arDisplayLinkProperties = [];
		
	$trigger = 0;
		
	foreach ($arParams['LINK_PROPERTIES'] as $arLinkProperty) 
	{	
			$property = $arResult["DISPLAY_PROPERTIES"][$arLinkProperty];
			
			if(empty($property['VALUE'])) continue;						
			
			if ($property['PROPERTY_TYPE'] == 'S')
			{
				if (is_array($property['DISPLAY_VALUE']))
				{
					foreach ($property['VALUE'] as $key => $value)
					{						
						$arDisplayLinkProperties[] = 
						[
							"PROPERTY" => $arLinkProperty,
							"NAME"     => $property['DISPLAY_VALUE'][$key], 
							"CODE"     => $value
						];

						++$trigger;
						if ($trigger == 5) break;
					}
				}
				else
				{
					$arDisplayLinkProperties[] = 
					[
						"PROPERTY" => $arLinkProperty,
						"NAME"     => $property['DISPLAY_VALUE'], 
						"CODE"     => is_array($property['VALUE']) ? $property['VALUE'][0] : $property['VALUE']
					];
					
					++$trigger;
					if ($trigger == 5) break;
				}
			}
			else if ($property['PROPERTY_TYPE'] == 'E') 
			{
				foreach ($property['LINK_ELEMENT_VALUE'] as $key => $value)
				{						
					$arDisplayLinkProperties[] = 
					[
						"PROPERTY" => $arLinkProperty,
						"NAME"     => $value['NAME'], 
						"CODE"     => $value['CODE']
					];

					++$trigger;
					if ($trigger == 5) break;					
				}
			}
	}
	
	$arResult["DISPLAY_LINK_PROPERTIES"] = $arDisplayLinkProperties;
}


$arResult["STR_ARTICLES_ID"] = "";

foreach ($arResult["DISPLAY_PROPERTIES"]["articles"]["VALUE"] as $item) 
{
	$arResult["STR_ARTICLES_ID"] .= "_".$item;
}


$this->__component->SetResultCacheKeys(["REPORTER", "DISPLAY_LINK_PROPERTIES", "STR_ARTICLES_ID"]);
?>