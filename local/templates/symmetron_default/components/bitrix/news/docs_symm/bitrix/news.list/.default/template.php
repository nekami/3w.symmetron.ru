<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<script>
    $(document).ready(function() {
        var rangeMin = $('.range-from').val()
        var rangeMax = $('.range-to').val()
        var curUrl = "<?=$arResult["NAV_RESULT"]->strListUrl?>";
        var codeTagsProp = "<?=$arParams["CODE_TAGS_PROP"];?>";

        if ($(".range-v").length) {
            $(".range-v").ionRangeSlider({
                type: "double",
                min: rangeMin,
                max: rangeMax,
                hide_min_max: true,
                force_edges: true,
                keyboard: true
            });
        }

        $('.cloud').on('click', function () {
            if ($(this).hasClass('chosen')) {
                $(this).removeClass('chosen')
            } else {
                $(this).addClass('chosen')
            }

            updateNews();
        })

        //$(document).ready(function() {
            if ($('.sticky-sidebar').length) {
                $('.sticky-sidebar').stickybits({useStickyClasses: true})
            }
        //});

        var templateFolder = "<?=$templateFolder?>";


        $('.news-list').on('click', ".switch-inline", function () {
            var dataPost = {};
            dataPost["FOLDER"] = "<?=$arResult["NAV_RESULT"]->strListUrl;?>";
            dataPost["METHOD_VIEW_IN_NEWS"] = "S";
            BX.ajax.post(
                templateFolder + "/ajax_switch.php",
                dataPost,
                function (data) {
                    if(data) {

                    }
                }
            );

            $(this).addClass('chosen')
            $('.news-blocks').addClass('inline')
            $('.switch-block').removeClass('chosen')

        });

        $('.news-list').on('click', ".switch-block", function () {
            var dataPost = {};
            dataPost["FOLDER"] = "<?=$arResult["NAV_RESULT"]->strListUrl;?>";
            dataPost["METHOD_VIEW_IN_NEWS"] = "C";
            BX.ajax.post(
                templateFolder + "/ajax_switch.php",
                dataPost,
                function (data) {
                    if (data) {

                    }
                }
            );

            $(this).addClass('chosen')
            $('.news-blocks').removeClass('inline')
            $('.switch-inline').removeClass('chosen')
        });


        function updateNews() {
            var url = curUrl;
            var tags = $(".news-tag.cloud.chosen");
            if(tags.length > 0) {
                url += "?";
                for (var i = 0; i < tags.length; i++) {
                    url += "PROPERTY_" + codeTagsProp + "[" + tags[i].id + "]=" + tags[i].id + "&";
                }

                console.log(url);
            }
            $("#news > .wrapper > .news-holder > .news-list").load(url + " .news-list .update-block-news", []);
            history.replaceState(false, false, url);
        }
    })

</script>

<section class="news" id = "news">
    <div class="wrapper">
        <div class="news-holder">
            <div class="news-list">
                <div class="update-block-news">
                    <?
                    $name = $arResult["NAME"];
                    if (!empty($arResult["SECTION"]) && count($arResult["SECTION"]["PATH"]) == 2)
                        $name = end($arResult["SECTION"]["PATH"])["NAME"];
                    ?>
                    <h1><?=$name?></h1>
                    <div class="switcher">
                        <div class="switch-inline <?if($arParams["METHOD_VIEW_IN_NEWS"] == 'S'):?>chosen<?endif;?>">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="switch-block <?if($arParams["METHOD_VIEW_IN_NEWS"] == 'C'):?>chosen<?endif;?>">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="news-blocks <?if($arParams["METHOD_VIEW_IN_NEWS"] == 'S'):?>inline<?endif;?>">

                        <?foreach($arResult["ITEMS"] as $arItem):?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                            <?/*
                            <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="news-block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            */?>
                            <a target="_blank" href="<?=$arItem["ATTACH"]["SRC"];?>" class="news-block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

                                <?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
                                    <div class="news-img">
                                        <img
                                                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                                alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                                        />
                                    </div>
                                <?endif;?>
                                <div class="news-content">
                                    <h3 class="news-header"><?echo $arItem["NAME"]?></h3>
                                    <span class="news-date"><?echo $arItem["DATE_TITLE"]?></span>
                                    <p class="news-preview">
                                        <?=$arItem["PREVIEW_TEXT"];?>
                                    </p>
                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"][$arParams["CODE_TAGS_PROP"]]["DISPLAY_VALUE"])):?>
                                        <div class="news-tags">
                                            <?foreach ($arItem["DISPLAY_PROPERTIES"][$arParams["CODE_TAGS_PROP"]]["DISPLAY_VALUE"] as $tag):?>
                                                <div class="news-tag"><?=$tag;?></div>
                                            <?endforeach;?>
                                        </div>
                                    <?endif;?>
                                </div>
                            </a>
                        <?endforeach;?>

                    </div>


                    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                        <br /><?=$arResult["NAV_STRING"]?>
                    <?endif;?>
                </div>


            </div>
            <div class="news-sidebar">
                <div class="sticky-sidebar">
                    <?if(!empty($arResult["ALL_TAGS"])):?>
                        <div class="tag-wrapper">
                            <h2>Облако тегов</h2>
                            <div class="tag-cloud" id="tag-cloud">
                                <?
                                    if(!empty($_GET["PROPERTY_".$arParams["CODE_TAGS_PROP"]])){
                                        $arChosenTags = $_GET["PROPERTY_".$arParams["CODE_TAGS_PROP"]];
                                    } else {
                                        $arChosenTags = array();
                                    }
                                ?>
                                <?foreach ($arResult["ALL_TAGS"] as $key => $tag):?>
                                    <div name = "news-tag-elem" class="news-tag cloud <?if(in_array($key, $arChosenTags)):?>chosen<?endif;?>" id="<?=$key?>" ><?=$tag;?></div>
                                <?endforeach;?>
                            </div>
                        </div>
                    <?endif;?>
                </div>
            </div>
        </div>
    </div>

</section>
