 $(document).ready(function () {
        var rangeMin = $('.range-from').val()
        var rangeMax = $('.range-to').val()
        var curUrl = BX.nav_result;
        var codeTagsProp = BX.codeTagsProp;

        if ($(".range-v").length) {
            $(".range-v").ionRangeSlider({
                type: "double",
                min: rangeMin,
                max: rangeMax,
                hide_min_max: true,
                force_edges: true,
                keyboard: true
            });
        }

        $('.cloud').on('click', function () {
            if ($(this).hasClass('chosen')) {
                $(this).removeClass('chosen')
            } else {
                $(this).addClass('chosen')
            }

            updateNews();
        });
       
        if ($('.sticky-sidebar').length) {
            $('.sticky-sidebar').stickybits({useStickyClasses: true});
        }        

        var templateFolder = BX.templateFolder;

        $('.news-list').on('click', ".switch-inline", function () {
            var dataPost = {};
            dataPost["FOLDER"] = BX.nav_result;
            dataPost["METHOD_VIEW_IN_NEWS"] = "S";
            BX.ajax.post(
                templateFolder + "/ajax_switch.php",
                dataPost,
                function (data) {
                    if (data) {

                    }
                }
            );

            $(this).addClass('chosen')
            $('.news-blocks').addClass('inline')
            $('.switch-block').removeClass('chosen')

        });

        $('.news-list').on('click', ".switch-block", function () {
            var dataPost = {};
            dataPost["FOLDER"] = BX.nav_result;
            dataPost["METHOD_VIEW_IN_NEWS"] = "C";
            BX.ajax.post(
                templateFolder + "/ajax_switch.php",
                dataPost,
                function (data) {
                    if (data) {

                    }
                }
            );

            $(this).addClass('chosen')
            $('.news-blocks').removeClass('inline')
            $('.switch-inline').removeClass('chosen')
        });


        function updateNews() {
            var url = curUrl;
            var tags = $(".news-tag.cloud.chosen");
            if (tags.length > 0) {
                url += "?";
                for (var i = 0; i < tags.length; i++) {
                    url += "PROPERTY_" + codeTagsProp + "[" + tags[i].id + "]=" + tags[i].id + "&";
                }

                console.log(url);
            }
            $("#news > .wrapper > .news-holder > .news-list").load(url + " .news-list .update-block-news", []);
            history.replaceState(false, false, url);
        }
    });