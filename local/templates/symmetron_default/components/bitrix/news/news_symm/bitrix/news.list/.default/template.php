<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<script>
	BX.nav_result = "<?=$arResult["NAV_RESULT"]->strListUrl?>";	
	BX.templateFolder = "<?=$templateFolder?>";
	BX.codeTagsProp = "<?=$arParams["CODE_TAGS_PROP"];?>";
</script>

<section class="news" id="news">
    <div class="wrapper">	
        <div class="news-holder">		
            <div class="news-list">
                <div class="update-block-news">
                    <h1><?= $arResult["NAME"] ?></h1>
                    <div class="switcher">
                        <div class="switch-inline <? if ($arParams["METHOD_VIEW_IN_NEWS"] == 'S'): ?>chosen<? endif; ?>">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="switch-block <? if ($arParams["METHOD_VIEW_IN_NEWS"] == 'C'): ?>chosen<? endif; ?>">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="news-blocks <? if ($arParams["METHOD_VIEW_IN_NEWS"] == 'S'): ?>inline<? endif; ?>">

                        <? foreach ($arResult["ITEMS"] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" class="news-block"
                               id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                <? if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])): ?>
                                    <div class="news-img">
                                        <img
                                                src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                        />
                                    </div>
                                <? endif; ?>
                                <div class="news-content">
                                    <h3 class="news-header"><? echo $arItem["NAME"] ?></h3>
                                    <span class="news-date"><? echo $arItem["DATE_TITLE"] ?></span>
                                    <p class="news-preview">
                                        <?= preg_match('/<!--CONTENT-->(.*?)<!--CONTENT-->/si', $arItem["DETAIL_TEXT"], $description) ? $description[0] : '' ?>
                                    </p>
                                    <? if (!empty($arItem["DISPLAY_PROPERTIES"][$arParams["CODE_TAGS_PROP"]]["DISPLAY_VALUE"])): ?>
                                        <div class="news-tags">
                                            <? foreach ($arItem["DISPLAY_PROPERTIES"][$arParams["CODE_TAGS_PROP"]]["DISPLAY_VALUE"] as $tag): ?>
                                                <div class="news-tag"><?= $tag; ?></div>
                                            <? endforeach; ?>
                                        </div>
                                    <? endif; ?>
                                </div>
                            </a>
                        <? endforeach; ?>

                    </div>


                    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                        <br/><?= $arResult["NAV_STRING"] ?>
                    <? endif; ?>
                </div>


            </div>
            <div class="news-sidebar">
                <div class="sticky-sidebar">
                    <? if (!empty($arResult["ALL_TAGS"])): ?>
                        <div class="tag-wrapper">
                            <h2><?=GetMessage('TAG_CLOUD');?></h2>
                            <div class="tag-cloud" id="tag-cloud">
                                <?
                                if (!empty($_GET["PROPERTY_" . $arParams["CODE_TAGS_PROP"]])) {
                                    $arChosenTags = $_GET["PROPERTY_" . $arParams["CODE_TAGS_PROP"]];
                                } else {
                                    $arChosenTags = array();
                                }
                                ?>
                                <? foreach ($arResult["ALL_TAGS"] as $key => $tag): ?>
                                    <div name="news-tag-elem"
                                         class="news-tag cloud <? if (in_array($key, $arChosenTags)): ?>chosen<? endif; ?>"
                                         id="<?= $key ?>"><?= $tag; ?></div>
                                <? endforeach; ?>
                            </div>
                        </div>

                    <? endif; ?>			             			
					
					<!-- умный фильтр -->				
					<? 
					if($arParams["USE_FILTER"]=="Y") 
					{					
						echo htmlspecialchars_decode($arParams["CONTENT_FILTER"]);
					} 
					?>
                </div>
            </div>
        </div>		
    </div>
</section>