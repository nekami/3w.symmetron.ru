<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
						/></a>
			<?else:?>
				<img
					class="preview_picture"
					border="0"
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					style="float:left"
					/>
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
			<?else:?>
				<b><?echo $arItem["NAME"]?></b><br />
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<?echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br />
		<?endforeach;?>
	</p>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>

<?
//echo "Arresult<br><br><pre>";echo mydump($arResult);echo "</pre>";
//dump ($arResult);
//echo $arResult['ITEMS']['0']['DETAIL_PAGE_URL']."<br>";
//echo $arResult['ITEMS']['0']['NAME']."<br>";
//echo dirname(__FILE__) ."<br>";
//echo $_SERVER['DOCUMENT_ROOT'] ."<br>";

echo "<br><br>";
	// Создание Excel файла
// Определения
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Подключение PHPExcel */
require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/PHPExcel.php';

// Создание нового обЪекта PHPExcel 
//echo date('H:i:s') , " Создание нового обЪекта PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
/*
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("PHPExcel Test Document")
							 ->setSubject("PHPExcel Test Document")
							 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file");
*/
// Вычисляем где больше количества свойств
$res1 = count($GLOBALS["Sort"]);
$res2 = count($GLOBALS["SortSpecif"]);
if($res1>$res2){$res=$res1;}
else{$res=$res2;}
$rescol=$res+9;
$resrow=$res;

// Установка ширины столбцов (ширина) и перенос строки
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
$x=1; 
while ($x<$resrow){
	$colString = PHPExcel_Cell::stringFromColumnIndex($x);
	$objPHPExcel->getActiveSheet()->getColumnDimension($colString)->setWidth(60);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'2')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'3')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'4')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'8')->getAlignment()->setWrapText(true);
	$x++;
}

// Установка высоты строк
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(21.);
$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(31.5);
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(31.5);
$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(31.5);
$objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(31.5);

$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(42);

// Стили ячеек 1
$styleArray1 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '12',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
		//'color'     => array(
		//           'rgb' => 'c00000'
		//       )
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
	/*   'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array (
                'rgb' => 'ffe799'
        )
),*/
	/*    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)*/
);

$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);

// Стили ячеек A2
$styleArrayA2 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '16',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
                 'color'     => array(
                     'rgb' => 'c00000'
                 )
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array (
                'rgb' => 'ffe799'
        )
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayA2);

// Стили ячеек B2E2
$styleArrayB2E2 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '12',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
		/*'color'     => array(
                     'rgb' => 'c00000'
				 )*/
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		 'color' => array (
                'rgb' => 'ffe799'
		)
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$x=1; 
while ($x<$resrow){
	$colString = PHPExcel_Cell::stringFromColumnIndex($x);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'2')->applyFromArray($styleArrayB2E2);
	$x++;
}



// Стили ячеек A3
$styleArrayA3 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '16',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
                 'color'     => array(
                     'rgb' => 'c00000'
                 )
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array (
                'rgb' => 'c5e0b2'
        )
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayA3);

// Стили ячеек B3E3
$styleArrayB3E3 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '12',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
		/*'color'     => array(
                     'rgb' => 'c00000'
				 )*/
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		 'color' => array (
                'rgb' => 'c5e0b2'
		)
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);
$x=1; 
while ($x<$resrow){
	$colString = PHPExcel_Cell::stringFromColumnIndex($x);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'3')->applyFromArray($styleArrayB3E3);
	$x++;
}

// Стили ячеек A4
$styleArrayA4 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '16',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
                 'color'     => array(
                     'rgb' => 'c00000'
                 )
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array (
                'rgb' => 'f8cbac'
        )
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($styleArrayA4);

// Стили ячеек B4E4
$styleArrayB4E4 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '12',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
		/*'color'     => array(
                     'rgb' => 'c00000'
				 )*/
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		 'color' => array (
                'rgb' => 'f8cbac'
		)
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);
$x=1; 
while ($x<$resrow){
	$colString = PHPExcel_Cell::stringFromColumnIndex($x);
	$objPHPExcel->getActiveSheet()->getStyle($colString.'4')->applyFromArray($styleArrayB4E4);
	$x++;
}

// Стили ячеек A5
$styleArrayA5 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '16',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
                 'color'     => array(
                     'rgb' => 'c00000'
                 )
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array (
                'rgb' => 'b3c6e7'
        )
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($styleArrayA5);


// Стили ячеек A8D8
$styleArrayA8D8 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '12',
                 'bold'      => true,
                 'italic'    => false,
                 'strike'    => false,
		/*'color'     => array(
                     'rgb' => 'c00000'
				 )*/
             ),
    'alignment' => array (
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		 'color' => array (
                'rgb' => 'd8d8d8'
		)
    ),
    'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$objPHPExcel->getActiveSheet()->getStyle('A8')->applyFromArray($styleArrayA8D8);
$objPHPExcel->getActiveSheet()->getStyle('B8')->applyFromArray($styleArrayA8D8);
$objPHPExcel->getActiveSheet()->getStyle('C8')->applyFromArray($styleArrayA8D8);
$objPHPExcel->getActiveSheet()->getStyle('D8')->applyFromArray($styleArrayA8D8);





// Стили ячеек A9
$styleArrayA9 = array(
	'font'    => array(
                 'name'      => 'Calibri',
                 'size'      => '12',
                 'bold'      => false,
                 'italic'    => false,
                 'strike'    => false,
		//'color'     => array(
		//           'rgb' => 'c00000'
		//       )
             ),
    'alignment' => array (
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
    ),
	/*   'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array (
                'rgb' => 'ffe799'
        )
),*/
	'borders' => array(
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
		'	rgb' => '808080'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_HAIR,
		'color' => array(
			'rgb' => '808080'
		)
	),
	)
);

$x=9; while ($x<$rescol){
$objPHPExcel->getActiveSheet()->getStyle('A'.$x)->applyFromArray($styleArrayA9);
$x++;
}
$x=9; while ($x<$rescol){
$objPHPExcel->getActiveSheet()->getStyle('B'.$x)->applyFromArray($styleArrayA9);
$x++;
}
$x=9; while ($x<$rescol){
$objPHPExcel->getActiveSheet()->getStyle('C'.$x)->applyFromArray($styleArrayA9);
$x++;
}
$x=9; while ($x<$rescol){
$objPHPExcel->getActiveSheet()->getStyle('D'.$x)->applyFromArray($styleArrayA9);
$x++;
}

//list($group, $item) = explode("/", $arResult['ITEMS']['0']['NAME']);
list($group, $item) = explode("/", $GLOBALS["VNAME"]);
// Добавляем данные
//echo date('H:i:s') , " Добавляем данные" , EOL;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', $item)
            ->setCellValue('A2', 'Название свойств')
            ->setCellValue('A3', 'Свойство в ветке')
            ->setCellValue('A4', 'Подсказка')
            ->setCellValue('A5', 'Примеры')
            ->setCellValue('A8', 'Отображаемые на листе номенклатуры')
			->setCellValue('B1', 'http://w3.symmetron.ru/catalog'.$GLOBALS["var_url"][$GLOBALS["VarPROP3372"]])
            ->setCellValue('B8', 'Отображаемые в спецификации')
            ->setCellValue('C8', 'Не отображаемые везде')
            ->setCellValue('D8', 'На удаление');

$objPHPExcel->getActiveSheet()->getCell('B1')->getHyperlink()->setUrl('http://w3.symmetron.ru/catalog'.$GLOBALS["var_url"][$GLOBALS["VarPROP3372"]]);
//->setCellValue('A9', 'Test');

//Отображаемые на листе
$i=9;
foreach ($GLOBALS["Sort"] as $k=>$v){
	$cell="A".$i;
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell, $GLOBALS["VarPROP3550Name"][$k]);
$i++;
}
echo "Temp1<br>";
$temp1=array();
foreach ($GLOBALS["Sort"] as $k=>$v){ $temp1[]=$GLOBALS["VarPROP3550Name"][$k]; }
//foreach ($temp1 as $k=>$v){  echo $k."=>".$v."<br>"; }

$objPHPExcel->getActiveSheet()
    ->fromArray(
        $temp1,   // The data to set
        NULL,        // Array values with this value will not be set
        'B2'         // Top left coordinate of the worksheet range where
                     //    we want to set these values (default is A1)
);




echo "Temp2<br>";
$temp2=array();
foreach ($GLOBALS["Sort"] as $k=>$v){ $temp2[]=$GLOBALS["VarPROP3550Hint"][$k];  }
//foreach ($temp2 as $k=>$v){  echo $k."=>".$v."<br>"; }
$objPHPExcel->getActiveSheet()
    ->fromArray(
        $temp2,   // The data to set
        NULL,        // Array values with this value will not be set
        'B4'         // Top left coordinate of the worksheet range where
                     //    we want to set these values (default is A1)
);



//Отображаемые в спецификации
$i=9;
foreach ($GLOBALS["SortSpecif"] as $k=>$v){
	$cell="B".$i;
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue($cell, $GLOBALS["VarPROP3550Name"][ $k]);
$i++;
}

//Не отображаемые
$i=9;
foreach ($GLOBALS["VarPROP3373NoShow"] as $k=>$v){
	$cell="C".$i;
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue($cell, $GLOBALS["VarPROP3550Name"][ $k]);
$i++;
}

// Переименовываем лист
//echo date('H:i:s') , " Переименовываем лист" , EOL;
$objPHPExcel->getActiveSheet()->setTitle('Лист1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Сохраняем файл Excel 2007
echo date('H:i:s') , " Сохраняем файл в формате Excel 2007" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

//echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
//echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;

$objPHPExcel->disconnectWorksheets();
unset($objPHPExcel);

?>
