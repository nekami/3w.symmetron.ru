<?  if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?
$replacer = function ($matches) use ($APPLICATION) 
{	
	ob_start();
	
	$matches = explode("_", $matches[1]);
	
	if (!$matches[0])
	{	
		unset($matches[0]);
		
		if ( $matches[1] == "Components" )
		{
			unset($matches[1]);
			
			$GLOBALS["arrFilterFavorites"] = ["=ID" => $matches]; ?>

			<section class="favorite">
			<div class="wrapper">
				<? $APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"favorites",
					array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "0",
						"CACHE_TYPE" => "N",
						"CHECK_DATES" => "Y",
						"COMPOSITE_FRAME_MODE" => "A",
						"COMPOSITE_FRAME_TYPE" => "AUTO",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array(
							0 => "ID",
							1 => "CODE",
							2 => "NAME",
							3 => "DETAIL_PICTURE",
							4 => "",
						),
						"FILTER_NAME" => "arrFilterFavorites",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "4",
						"IBLOCK_TYPE" => "catalog",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => 10,
						"PAGER_BASE_LINK" => "",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
						"PAGER_PARAMS_NAME" => "arrPager",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "modern",
						"PAGER_TITLE" => "Продукция",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array(
							0 => "ARTICLE",
							1 => "",
							2 => "manufacturer",
							3 => "article",
							4 => "",
						),
						"SET_BROWSER_TITLE" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"SORT_BY1" => "PROPERTY_ACTIVE_STATUS",
						"SORT_BY2" => "PROPERTY_ITEMID",
						"SORT_ORDER1" => "ASC",
						"SORT_ORDER2" => "ASC",
						"STRICT_SECTION_CHECK" => "N",
						"COMPONENT_TEMPLATE" => "favorites",
						"PROPERTY_CODE_FAST_SHOW_LEFT" => array(
							0 => "ARTICLE",
							1 => "",
							2 => "KPD",
							3 => "WATERPROOFNESS",
							4 => "",
						),
						"PROPERTY_CODE_FAST_SHOW_RIGHT" => array(
							0 => "BLUETOOTH_LOW_ENERGY",
							1 => "",
							2 => "APPLICATIONS",
							3 => "CAN",
							4 => "",
						),
						"NEWS_CARD" => "Y"
					),
					false
				); ?>				
			</div>			
			</section> <?
		}
	
		if ( $matches[1] == "Docs" )
		{
			unset($matches[1]);
			
			$GLOBALS["arFilterDocument"] = ["=ID" => $matches];

			$APPLICATION->IncludeComponent(
				"bitrix:news.list", 
				"document_block", 
				array(
					"ACTIVE_DATE_FORMAT" => "j F Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "N",
					"CHECK_DATES" => "Y",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array(
						0 => "NAME"
					),
					"FILTER_NAME" => "arFilterDocument",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "16",
					"IBLOCK_TYPE" => "documents",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "N",
					"MEDIA_PROPERTY" => "",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "100",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "300",
					"PROPERTY_CODE" => array(
						0 => "LINK_TO_RES",
						1 => "DATE",
						2 => "TYPE",
						3 => "LANG",
						4 => "FILE"
					),
					"SEARCH_PAGE" => "/search/",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SLIDER_PROPERTY" => "",
					"SORT_BY1" => "NAME",
					"SORT_BY2" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_ORDER2" => "ASC",
					"STRICT_SECTION_CHECK" => "N",
					"TEMPLATE_THEME" => "blue",
					"USE_RATING" => "N",
					"USE_SHARE" => "N",
					"COMPONENT_TEMPLATE" => "document_block",
					"USE_FILTER" => "Y",
					"DISPLAY_NAME_SERIES" => "N"
				),
				false
			);			
		}
	}
	else 
	{
		
		$GLOBALS["arFilterActivity"] = [
			">=PROPERTY_date2" => ConvertDateTime(date("d.m.Y"), "YYYY-MM-DD")." 00:00:00", 									 "=ID" => $matches
		];

		$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"activity_block", 
			array(
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"ADD_SECTIONS_CHAIN" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"COMPOSITE_FRAME_MODE" => "A",
				"COMPOSITE_FRAME_TYPE" => "AUTO",
				"DETAIL_URL" => "",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FIELD_CODE" => array(
					0 => "NAME",
					1 => "PREVIEW_TEXT"
				),
				"FILTER_NAME" => "arFilterActivity",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"IBLOCK_ID" => "28",
				"IBLOCK_TYPE" => "catalog",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"INCLUDE_SUBSECTIONS" => "N",
				"LINK_PROPERTIES" => array(
					0 => "type",
					1 => "themes",
					2 => "brands",
					3 => "cities"
				),
				"MEDIA_PROPERTY" => "",
				"MESSAGE_404" => "",
				"NEWS_COUNT" => "10",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => ".default",
				"PAGER_TITLE" => "",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"PREVIEW_TRUNCATE_LEN" => "300",
				"PROPERTY_CODE" => array(			
					0  => "type",
					1  => "themes",
					2  => "brands",
					3  => "cities",
					4  => "address",
					5  => "date2",
					6  => "date1",
					7  => "location",
					8  => "link_registration",
					9  => "cost",
					10 => "phone"
				),
				"SEARCH_PAGE" => "/search/",
				"SET_BROWSER_TITLE" => "N",
				"SET_LAST_MODIFIED" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"SHOW_404" => "N",
				"SLIDER_PROPERTY" => "",
				"SORT_BY1" => "PROPERTY_date1",
				"SORT_BY2" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_ORDER2" => "ASC",
				"STRICT_SECTION_CHECK" => "N",
				"TEMPLATE_THEME" => "blue",
				"USE_RATING" => "N",
				"USE_SHARE" => "N",			
				"COMPONENT_TEMPLATE" => "activity_block",
				"USE_FILTER" => "Y",
				"DISPLAY_NAME_SERIES" => "Y"
			),
			false
		);
		
	}
	
	return ob_get_clean();
};
	
echo preg_replace_callback(
	"|Inc(.*)|".BX_UTF_PCRE_MODIFIER,
	$replacer,
	$arResult["CACHED_TPL"]
);

?>