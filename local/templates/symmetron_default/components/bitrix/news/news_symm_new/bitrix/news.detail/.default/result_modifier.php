<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arParams["CODE_TAGS_PROP"]))
    $arParams["CODE_TAGS_PROP"] = trim($arParams["CODE_TAGS_PROP"]);
else
    $arParams["CODE_TAGS_PROP"] = "TAGS";


if(!empty($arParams["CODE_FILE_PROP"]))
    $arParams["CODE_FILE_PROP"] = trim($arParams["CODE_FILE_PROP"]);
else
    $arParams["CODE_FILE_PROP"] = "";


if(!empty($arParams["CODE_DATE_PROP"])){
    $arParams["CODE_DATE_PROP"] = trim($arParams["CODE_DATE_PROP"]);
}else{
    $arParams["CODE_DATE_PROP"] = "";
}


$arResult["ATTACH"] = array();
if(!empty($arParams["CODE_FILE_PROP"])){
    if(!empty($arResult["PROPERTIES"][$arParams["CODE_FILE_PROP"]]["VALUE"])){
        $arFile = CFile::GetFileArray($arResult["PROPERTIES"][$arParams["CODE_FILE_PROP"]]["VALUE"]);
        $arResult["ATTACH"] = $arFile;
    }
}


if($arParams["CODE_DATE_PROP"] != "") {

    if(!empty($arResult["DISPLAY_PROPERTIES"][$arParams["CODE_DATE_PROP"]])){

        $arResult["DATE_TITLE"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["DISPLAY_PROPERTIES"][$arParams["CODE_DATE_PROP"]]["VALUE"], CSite::GetDateFormat()));
    }

}

//////////////////////////////////////////////////////////////////////////////////////

if ( $arResult['LIST_PAGE_URL'] == "/events/")
	$arDocs = $arResult["DISPLAY_PROPERTIES"]["docs"]["VALUE"];	
else $arDocs = $arResult["DISPLAY_PROPERTIES"]["DOCS"]["VALUE"];

$arResult["STR_DOCS_ID"] = "";

foreach ($arDocs as $item) 
	$arResult["STR_DOCS_ID"] .= "_".$item;

//////////////////////////////////////////////////////////////////////////////////////

$arResult["STR_COMPONENTS_ID"] = "";

foreach ($arResult["DISPLAY_PROPERTIES"]["components"]["VALUE"] as $item)
	$arResult["STR_COMPONENTS_ID"] .= "_".$item;

//////////////////////////////////////////////////////////////////////////////////////