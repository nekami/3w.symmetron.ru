			let docs = document.querySelector('.documents')
            if(docs) {
                let docsItems = document.querySelectorAll('.docs-item')
                let buttonShow = document.querySelector('.documents-head-show')
                buttonShow.addEventListener('click', (e) => {
                    if(buttonShow.classList.contains('documents-head-show--open') == false) {
                        docsItems.forEach((item) => {
                            let docsList = item.querySelector('.document-list')
                            if (docsList.classList.contains('document-list--open') == false) {
                                docsList.classList.add('document-list--open')
                            }
                        })
                        e.target.innerHTML = 'свернуть все'
                        buttonShow.classList.add('documents-head-show--open')
                        docsItems.forEach((item) => {
                            item.querySelector('.document-control').classList.add('document-control--open')
                        })
                    } else {
                        docsItems.forEach((item) => {
                            let docsList = item.querySelector('.document-list')
                            if (docsList.classList.contains('document-list--open')) {
                                docsList.classList.remove('document-list--open')
                            }
                        })
                        e.target.innerHTML = 'раскрыть все'
                        buttonShow.classList.remove('documents-head-show--open')
                        docsItems.forEach((item) => {
                            item.querySelector('.document-control').classList.remove('document-control--open')
                        })
                    }
                })

                docs.addEventListener('click', (e) => {
                    docsItems.forEach((element) => {
                        let openListButton = element.querySelector('.document-control')
                        let documentList = element.querySelector('.document-list')
                        if(e.target == openListButton) {
                            if(openListButton.classList.contains('document-control--open')) {
                                openListButton.classList.remove('document-control--open')
                                documentList.classList.remove('document-list--open')
                            } else {
                                openListButton.classList.add('document-control--open')
                                documentList.classList.add('document-list--open')
                            }
                        }
                    })
                })
            }