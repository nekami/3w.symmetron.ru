<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arParams["CODE_TAGS_PROP"])){
    $arParams["CODE_TAGS_PROP"] = trim($arParams["CODE_TAGS_PROP"]);
} else {
    $arParams["CODE_TAGS_PROP"] = "TAGS";
}

if(!empty($arParams["METHOD_VIEW_IN_NEWS"])){
    $arParams["METHOD_VIEW_IN_NEWS"] = trim($arParams["METHOD_VIEW_IN_NEWS"]);
}else{
    $arParams["METHOD_VIEW_IN_NEWS"] = "C";
}

if(!empty($arParams["CODE_DATE_PROP"])){
    $arParams["CODE_DATE_PROP"] = trim($arParams["CODE_DATE_PROP"]);
}else{
    $arParams["CODE_DATE_PROP"] = "";
}


$db_enum_list = CIBlockProperty::GetPropertyEnum($arParams["CODE_TAGS_PROP"], Array(), Array("IBLOCK_ID"=>$arResult["ID"]));
$arResult["ALL_TAGS"] = array();
while($ar_enum_list = $db_enum_list->GetNext())
{
    $arResult["ALL_TAGS"][$ar_enum_list["ID"]] = $ar_enum_list["VALUE"];
}

if($arParams["CODE_DATE_PROP"] != "") {
    foreach ($arResult["ITEMS"] as $key => $atItem) {
        if(!empty($atItem["DISPLAY_PROPERTIES"][$arParams["CODE_DATE_PROP"]])){
            $arResult["ITEMS"][$key]["DATE_TITLE"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($atItem["DISPLAY_PROPERTIES"][$arParams["CODE_DATE_PROP"]]["VALUE"], CSite::GetDateFormat()));
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////


$arResult['FILTER_LIST_URL'] = str_replace ('//','/',CComponentEngine::makePathFromTemplate($arResult["LIST_PAGE_URL"]));

if ( !empty($arParams['LINK_PROPERTIES']) ) 
{	
	foreach($arResult["ITEMS"] as $index => $arItem) 
	{	
		$arDisplayLinkProperties = [];
		
		$trigger = 0;
		
		foreach ($arParams['LINK_PROPERTIES'] as $arLinkProperty) 
		{	
			$property = $arItem["DISPLAY_PROPERTIES"][$arLinkProperty];
						
			if(empty($property['VALUE'])) continue;			
			
			if ($property['PROPERTY_TYPE'] == 'S')
			{
				if (is_array($property['DISPLAY_VALUE']))
				{
					foreach ($property['VALUE'] as $key => $value)
					{						
						$arDisplayLinkProperties[] = 
						[
							"PROPERTY" => $arLinkProperty,
							"NAME"     => $property['DISPLAY_VALUE'][$key], 
							"CODE"     => $value
						];

						++$trigger;
						if ($trigger == 5) break;
					}
				}
				else
				{
					$arDisplayLinkProperties[] = 
					[
						"PROPERTY" => $arLinkProperty,
						"NAME"     => $property['DISPLAY_VALUE'], 
						"CODE"     => is_array($property['VALUE']) ? $property['VALUE'][0] : $property['VALUE']
					];
					
					++$trigger;
					if ($trigger == 5) break;
				}
			}
			else if ($property['PROPERTY_TYPE'] == 'E') 
			{
				foreach ($property['LINK_ELEMENT_VALUE'] as $key => $value)
				{						
					$arDisplayLinkProperties[] = 
					[
						"PROPERTY" => $arLinkProperty,
						"NAME"     => $value['NAME'], 
						"CODE"     => $value['CODE']
					];

					++$trigger;
					if ($trigger == 5) break;					
				}
			}
			
			unset($arResult["ITEMS"][$index]['DISPLAY_PROPERTIES'][$arLinkProperty]);
		}
		
		$arResult["ITEMS"][$index]['DISPLAY_LINK_PROPERTIES'] = $arDisplayLinkProperties;
	}
	
}