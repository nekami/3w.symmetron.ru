<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
ob_start();
?>

<? if ( $arResult['LIST_PAGE_URL'] == "/events/") { ?>

<div class="article-data">
    <ul class="article-data__list">
	
        <li class="article-data__item">
            <span class="article-data__text">
				<?=FormatDate("j F Y", MakeTimeStamp($arResult["DATE_CREATE"]));?>
			</span>
        </li>
		
		<? if ( !empty($arResult['DISPLAY_PROPERTIES']['themes']['DISPLAY_VALUE']) ) { ?>
				
			<? if (is_array($arResult['DISPLAY_PROPERTIES']['themes']['DISPLAY_VALUE'])) { ?>
								
				<? for ($i=0; $i<count($arResult['DISPLAY_PROPERTIES']['themes']['DISPLAY_VALUE']); $i++) { ?>
					
					<li class="article-data__item">
									
						<a href="/events/filter/themes-is-<?=strtolower($arResult['DISPLAY_PROPERTIES']['themes']['VALUE'][$i])?>/apply/" class="article-data__link" target="_blank">
										
							<?=$arResult['DISPLAY_PROPERTIES']['themes']['DISPLAY_VALUE'][$i]?>
											
						</a>

					<? if ( $i != (count($arResult['DISPLAY_PROPERTIES']['themes']['DISPLAY_VALUE']) - 1) )
						echo "</li>"; ?>
						
				<? } ?>
									
			<? } else echo "<li class='article-data__item'><a class='article-data__link' target='_blank' href='/events/filter/themes-is-".strtolower($arResult['DISPLAY_PROPERTIES']['themes']['VALUE'][0])."/apply/'>".$arResult['DISPLAY_PROPERTIES']['themes']['DISPLAY_VALUE']."</a></li>"; ?>
						
		<? } ?>			
		
		<? if (!empty($arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])) { ?>
		
			<li class="article-data__item">
			
				<span class="article-data__text"><?=getMessage('AVTOR');?> </span>           				
								
				<? if (is_array($arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])) { ?>
								
					<? for ($i=0; $i<count($arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']); $i++) { ?>
									
						<a href="/events/filter/author-is-<?=strtolower($arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'][$i])?>/apply/" class="article-data__link" target="_blank">
										
							<?=$arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'][$i]?>
											
						</a>

						<? if ( $i != (count($arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']) - 1) )
							echo "|"; ?>
						
					<? } ?>
									
				<? } else echo "<a class='article-data__link' target='_blank' href='/events/filter/author-is-".strtolower($arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])."/apply/'>".$arResult['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']."</a>"; ?>
				
			</li>
			
		<? } ?>
				
		<? if (!empty($arResult['DISPLAY_PROPERTIES']['READING_TIME']['DISPLAY_VALUE'])) { ?>
						
			<li class="article-data__item">
				<span class="article-data__text">						
					<?=getMessage('PRIBLIZITELNOE-VREMYA-CHTENIYA');?>
					<?=$arResult['DISPLAY_PROPERTIES']['READING_TIME']['VALUE'];?>
					<?=uni_declension($arResult['DISPLAY_PROPERTIES']['READING_TIME']['VALUE'], getMessage('MINUTA')); ?>
				</span>
			</li>
						
		<? } ?>	
			
    </ul>
</div>

<? } ?>


<div class="news-holder">
    <div class="news-page">

        <h1 class="article-detail__title">
			<?= $arResult["NAME"]; ?>
		</h1>
		
		<? if ( $arResult['LIST_PAGE_URL'] == "/news/") { ?>		
			<div class="news-page__date"><?= $arResult["DATE_TITLE"]; ?></div>
		<? } else echo "<br/>";?>

        <?= preg_match('/(.*)<!--CONTENT-->.*?<!--CONTENT-->(.*)/is', $arResult["DETAIL_TEXT"], $description, PREG_OFFSET_CAPTURE, 0) ? $description[1][0] . $description[2][0] : $arResult["DETAIL_TEXT"] ?>

        <? if (!empty($arResult["ATTACH"])): ?>
            <br><br>
            Ссылка:
            <a href="<?= $arResult["ATTACH"]["SRC"]; ?>"><?= $arResult["ATTACH"]["FILE_NAME"] ?></a>
        <? endif; ?>
    </div>
    <div class="news-sidebar">
        <div class="sticky-sidebar">
            <? if (!empty($arResult["DISPLAY_PROPERTIES"][$arParams["CODE_TAGS_PROP"]]["DISPLAY_VALUE"])): ?>
                <div class="tag-wrapper">
                    <div class="tag-cloud">
                        <? foreach ($arResult["DISPLAY_PROPERTIES"][$arParams["CODE_TAGS_PROP"]]["DISPLAY_VALUE"] as $tag): ?>
                            <div class="news-tag chosen"><?= $tag; ?></div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endif; ?>            
        </div>
    </div>
</div>


<? if (!empty($arResult["DISPLAY_PROPERTIES"]["components"]["VALUE"])) { ?>
	Inc_Components<?=$arResult["STR_COMPONENTS_ID"]?>
<? } ?>


<? if (!empty($arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"])) { ?>
	<div class="manufacturer">
		<h2 class="manufacturer__title"><?=GetMessage("MANUFACTURERS")?></h2>
		<ul class="manufacturer__list">	
			<? foreach ($arResult["DISPLAY_PROPERTIES"]["brands"]["LINK_ELEMENT_VALUE"] as $item) { ?> 
				<li class="manufacturer__item">
					<a href='<?=$item["DETAIL_PAGE_URL"]?>' title='<?=$item["NAME"]?>' class="manufacturer__link">
						<?=$item["NAME"]?>
					</a>
				</li>
			<? } ?>
		</ul>
	</div>
<? } ?>


<? if (!empty($arResult["DISPLAY_PROPERTIES"]["activity"]["VALUE"])) { ?>	
	Inc<?=$arResult["DISPLAY_PROPERTIES"]["activity"]["VALUE"]?>	
<? } ?>
	
	
<? if (!empty($arResult["STR_DOCS_ID"])) { ?>
	Inc_Docs<?=$arResult["STR_DOCS_ID"]?>
<? } ?>
	
	
<?
$this->__component->SetResultCacheKeys(array("CACHED_TPL"));
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();						
?>	