<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<script>
	BX.nav_result = "<?=$arResult["NAV_RESULT"]->strListUrl?>";	
	BX.templateFolder = "<?=$templateFolder?>";
	BX.codeTagsProp = "<?=$arParams["CODE_TAGS_PROP"];?>";
</script>

<div class="news news-page">

	<h2 class="news__title"><?=$arResult["NAME"]?></h2>
			
	<? if ( count($arResult["ITEMS"]) > 0 ) { ?>

		<!-- умный фильтр -->				
		<? if($arParams["USE_FILTER"]=="Y") {					
			echo htmlspecialchars_decode($arParams["CONTENT_FILTER"]);
		} ?>
			
		<div class="news-element-wrapper">

			<? foreach ($arResult["ITEMS"] as $arItem) { ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			
			<div class="news-element">
			
				<? if ($arResult["CODE"] == "events") { // для статей ?>
					<ul class="articles-element-head">
						<li class="articles-element-head__date">
							<?=FormatDate("j F Y", MakeTimeStamp($arItem["DATE_CREATE"]));?>
						</li>				
						
						<? if (!empty($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])) { ?>
						
						<li class="articles-element-head__author">
							
							<?=getMessage('AVTOR');?>
							
							<? if (is_array($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])) { ?>
							
								<? for ($i=0; $i<count($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']); $i++) { ?>
								
									<a href="/events/filter/author-is-<?=strtolower($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'][$i])?>/apply/">
									
										<?=$arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'][$i]?>
										
									</a>

									<? if ( $i != (count($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']) - 1) )
										echo "|"; ?>
								<? } ?>
								
							<? } else echo "<a href='/events/filter/author-is-".strtolower($arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE'])."/apply/'>".$arItem['DISPLAY_PROPERTIES']['author']['DISPLAY_VALUE']."</a>"; ?>
							
						</li>
						<? } ?>
						
						<? if (!empty($arItem['DISPLAY_PROPERTIES']['READING_TIME']['DISPLAY_VALUE'])) { ?>
							<li class="article-element-head__duration">
								<?=getMessage('PRIBLIZITELNOE-VREMYA-CHTENIYA');?>
								<?=$arItem['DISPLAY_PROPERTIES']['READING_TIME']['VALUE'];?>
								<?=uni_declension($arItem['DISPLAY_PROPERTIES']['READING_TIME']['VALUE'], getMessage('MINUTA')); ?>
							</li>
						<? } ?>	
					</ul>
				<? } else { // для новостей ?>
					<span class="news-element__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>				
				<? } ?>
					
				<h3 class="news-elements__title">
					<a href="<?=$arItem["DETAIL_PAGE_URL"];?>" title="<?=$arItem["NAME"];?>">					
						<?=$arItem["NAME"]?>
					</a>
				</h3>
				
				<? if ( !empty($arItem["PREVIEW_TEXT"]) ) { ?>			
					<p class="news-elements__text"><?=$arItem["PREVIEW_TEXT"];?></p>
				<? } ?>
					
				<? if ( !empty($arItem['DISPLAY_LINK_PROPERTIES']) ) { ?>
				
					<div class="cloud-tags">
					
						<? foreach ($arItem['DISPLAY_LINK_PROPERTIES'] as $propertyCode => $displayPropertyValue) { ?>
						
							<a href="<?=$arResult["FILTER_LIST_URL"]?>filter/<?=$displayPropertyValue["PROPERTY"]?>-is-<?=strtolower($displayPropertyValue["CODE"]);?>/apply/" class="cloud-tags__link">
								
								<?=$displayPropertyValue["NAME"]?>
									
							</a>
						
						<? } ?>
						
					</div>
					
				<? } ?>	
				
			</div>
			
			<? } ?>
		</div>
	<? } ?>

	<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
		<?= $arResult["NAV_STRING"] ?>
	<? endif; ?>
	
</div>