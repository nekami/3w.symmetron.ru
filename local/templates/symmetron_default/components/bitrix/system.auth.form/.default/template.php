<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo "dfgdgdfgdfgdfgdgdgdfg";
CJSCore::Init();
?>


    <script>
        var AUTH_PATH = "<?=$arResult["AUTH_URL"];?>";
        function isExistsAuth(){
            var dataForm = {};
            dataForm["AUTH_FORM"] = document.getElementById("AUTH_FORM_A").value;
            dataForm["TYPE"] = document.getElementById("TYPE_A").value;
            dataForm["USER_LOGIN"] = document.getElementById("USER_LOGIN_A").value;
            dataForm["USER_PASSWORD"] = document.getElementById("USER_PASSWORD_A").value;
            dataForm["Login"] = document.getElementById("Login").value;

            var pathReg = AUTH_PATH.split("?");
            //console.log(dataPost);

            //$("#analogue > .wrapper > .section-content").load( url + " .section-content .to-reload-by-filter", arrAjax);

            BX.ajax.post(
                pathReg[0] + "?ajax_call_auth=Y",
                dataForm,
                function (data) {

                    var arData = data.split("----------------=======================------------------");
                    //console.log(arData);
                    if (!arData[1]) {
                        location.reload();
                    } else {
                        document.getElementById("modal-login").innerHTML = arData[1];
                        console.log(arData[1]);
                    }
                    //return false;
                }
            );


        }

    </script>


<?if($_GET["ajax_call_auth"] == "Y"){
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "----------------=======================------------------";
}?>

<?global $USER;?>
<?if($USER->IsAuthorized()):?>
    <script>
        location.reload();
    </script>
<?endif;?>

<?$page = $APPLICATION->GetCurPage(false);?>
<?if($arResult["FORM_TYPE"] == "login"):?>

    <div class="login main-modal">
		<a class="reg-link" href="#modal-login" rel="close-modal" onclick='$("#modal-registration").modal();'><?=GetMessage("AUTH_REGISTER")?></a>
        <h2><?=GetMessage("AUTH_AUTH")?></h2>
        <?if(!empty($arResult["ERROR_MESSAGE"]["MESSAGE"])):?>
            <?if(strpos($arResult["ERROR_MESSAGE"]["MESSAGE"], GetMessage("AUTH_ERROR")) === false):?>
                <?if($_GET["error_file"] == "yes"):?>
                    <span class="error-message"><?=GetMessage("FILE_ERROR");?></span>
                    <script>
                        history.replaceState(false, false, "<?=$page?>");
                    </script>
                <?endif;?>
            <?else:?>
                <span class="error-message"><?=$arResult["ERROR_MESSAGE"]["MESSAGE"];?></span>
            <?endif;?>
        <?else:?>
            <?if($_GET["error_file"] == "yes"):?>
                <span class="error-message"><?=GetMessage("FILE_ERROR");?></span>
                <script>
                    history.replaceState(false, false, "<?=$page?>");
                </script>
            <?endif;?>
        <?endif;?>
        <form  name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" onsubmit="isExistsAuth(); return false;" action="<?=$arResult["AUTH_URL"]?>">
            <input type="hidden" name="AUTH_FORM" id="AUTH_FORM_A" value="Y" />
            <input type="hidden" name="TYPE" id="TYPE_A" value="AUTH" />
            <div class="login">
                <input type="text" placeholder="<?=GetMessage("AUTH_LOGIN")?>" name="USER_LOGIN" id="USER_LOGIN_A" maxlength="50" value="" size="17" required/>
                <script>
                    BX.ready(function() {
                        var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                        if (loginCookie)
                        {
                            var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                            var loginInput = form.elements["USER_LOGIN"];
                            loginInput.value = loginCookie;
                        }
                    });
                </script>
               <!-- <input type="text" name="login" id="login" required>-->
            </div>
            <div class="password">
                <input placeholder="<?=GetMessage("AUTH_PASSWORD")?>" type="password" name="USER_PASSWORD" id="USER_PASSWORD_A" maxlength="50" size="17" autocomplete="off" />
                <!--<input type="password" name="password" id="password" required>-->
            </div>
			<a class="forgot-mobile" href="#modal-login" rel="close-modal" onclick='$("#modal-forgot-pass").modal();'><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
            <div class="submit">
                <input type="submit" name="Login" id="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" />
            </div>
        </form>
    </div>
    <?if($_GET["ajax_call_auth"] == "Y"){?>
    <a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>
    <?}?>
<?endif?>
<?if($_GET["ajax_call_auth"] == "Y"):?>
    <?die;?>
<?//echo "----------------=======================------------------";?>
<?endif;?>
