<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
// $css = $APPLICATION->GetCSSArray();
// if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
// {
// 	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
// }

$strReturn .= '<li class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="/" title="Главная" itemprop="url">
			<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   viewBox="0 0 86.458 69.124931"

   xml:space="preserve"
   id="svg2"
   version="1.1"><metadata
     id="metadata8"><rdf:RDF><cc:Work
         rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" /></cc:Work></rdf:RDF></metadata><defs
     id="defs6"><clipPath
       id="clipPath18"
       clipPathUnits="userSpaceOnUse"><path
         id="path16"
         d="M 0,51.844 H 64.843 V 0 H 0 Z" /></clipPath></defs><g
     transform="matrix(1.3333333,0,0,-1.3333333,0,69.124933)"
     id="g10"><g
       id="g12"><g
         clip-path="url(#clipPath18)"
         id="g14"><g
           transform="translate(48.2774,22.1616)"
           id="g20"><path
             id="path22"
             style="fill:none;stroke:#008aff;stroke-width:5.31099987;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
             d="M 0,0 V -19.506 H -31.711 V 0" /></g><g
           transform="translate(3.0636,22.7285)"
           id="g24"><path
             id="path26"
             style="fill:none;stroke:#008aff;stroke-width:6.12699986;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
             d="M 0,0 29.358,26.052 58.716,0" /></g></g></g></g></svg>

			<meta itemprop="position" content="1">
		</a>
	</li>
';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
	    $class_b_end = "";
	    if($index == $itemSize-2)
            $class_b_end = "parent-crumb";

		$strReturn .= '
		<li class="bx-breadcrumb-item '.$class_b_end.'" id="bx_breadcrumb_'.$index.'" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">
		<span itemprop="name">'.$title.'</span>
		</a>
		<meta itemprop="position" content="'.($index + 2).'" />
		</li>';
	}
	else
	{
		$strReturn .= '
		<li class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name">'.$title.'</span>
		<meta itemprop="position" content="'.($index + 2).'" />
		</li>';
	}
}
return $strReturn;
