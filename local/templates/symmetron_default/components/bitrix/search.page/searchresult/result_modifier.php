<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["TAGS_CHAIN"] = array();
if($arResult["REQUEST"]["~TAGS"])
{
	$res = array_unique(explode(",", $arResult["REQUEST"]["~TAGS"]));
	$url = array();
	foreach ($res as $key => $tags)
	{
		$tags = trim($tags);
		if(!empty($tags))
		{
			$url_without = $res;
			unset($url_without[$key]);
			$url[$tags] = $tags;
			$result = array(
				"TAG_NAME" => htmlspecialcharsex($tags),
				"TAG_PATH" => $APPLICATION->GetCurPageParam("tags=".urlencode(implode(",", $url)), array("tags")),
				"TAG_WITHOUT" => $APPLICATION->GetCurPageParam((count($url_without) > 0 ? "tags=".urlencode(implode(",", $url_without)) : ""), array("tags")),
			);
			$arResult["TAGS_CHAIN"][] = $result;
		}
	}
}



if (!CModule::IncludeModule('iblock')) { die('iblock module is not included!'); };

$arItemId = array();
$arBrandsId = array();
$arBrand = array();

const PARAM_CATALOG = 'catalog';
const IB_MODULE = 'iblock';

foreach ($arResult['SEARCH'] as $key => $searchItem) {
    //only for catalog elements
    if($searchItem['MODULE_ID'] == IB_MODULE && $searchItem["PARAM1"] == PARAM_CATALOG)
        $arItemId[$searchItem['ITEM_ID']] = $key;
}

if (!empty($arItemId)) {
    $resItems = CIBlockElement::GetList(
        array(),
        array('ID' => array_keys($arItemId)),
        false,
        false,
        array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'PROPERTY_ARTICLE', 'PROPERTY_PRODUCERID')
    );

    while ($arItems = $resItems->Fetch())
    {
        $arChainBody = array();
        $resBreadcrumb = CIBlockSection::GetNavChain(
            $arItems['IBLOCK_ID'],
            $arItems['IBLOCK_SECTION_ID']
        );

        while ($arBreadcrumb = $resBreadcrumb->GetNext()) {
            $arChainBody[] = '<a href="'.$arBreadcrumb['SECTION_PAGE_URL'].'">'.htmlspecialcharsex($arBreadcrumb['NAME']).'</a>';
        }

        $key = $arItemId[$arItems['ID']];
        $arResult['SEARCH'][$key]['SECTION_CHAIN'] = " / ".implode('&nbsp;/&nbsp;', $arChainBody);
		$arResult['SEARCH'][$key]['TITLE'] = $arItems['PROPERTY_ARTICLE_VALUE'];
		$arResult['SEARCH'][$key]['TITLE_FORMATED'] = $arItems['PROPERTY_ARTICLE_VALUE'];
		$arResult['SEARCH'][$key]['PROPERTY_PRODUCERID_VALUE'] = $arItems['PROPERTY_PRODUCERID_VALUE'];

		$arBrandsId[] = $arItems['PROPERTY_PRODUCERID_VALUE'];
    }

    if (!empty($arBrandsId)) {

		$resBrands = CIBlockElement::GetList(
			array(),
			array('ID' => $arBrandsId, 'IBLOCK_CODE' => 'brands'),
			false,
			false,
			array('ID', 'NAME')
		);
		while ($obj = $resBrands->fetch()) {
			$arBrand[$obj['ID']] = $obj['NAME'];
		}

		foreach ($arResult['SEARCH'] as $key => $searchItem) {
			//only for catalog elements
			if($searchItem['MODULE_ID'] == IB_MODULE && $searchItem["PARAM1"] == PARAM_CATALOG) {
				$arResult['SEARCH'][$key]['TITLE'] .= ' '. $arBrand[$searchItem['PROPERTY_PRODUCERID_VALUE']];
				$arResult['SEARCH'][$key]['TITLE_FORMATED'] .= ' '. $arBrand[$searchItem['PROPERTY_PRODUCERID_VALUE']];
			}
		}
	}
}
?>

