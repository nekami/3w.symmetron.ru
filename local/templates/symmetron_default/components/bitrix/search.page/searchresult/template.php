<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<?
//echo "<pre>";
//print_r($arResult);
//echo "</pre>";
?>
<section class="searchResult">
	<div class="wrapper">
		<div class="search-page">
			<div class="search-advanced">
				<div class="search-advanced-result">
					<?if(is_object($arResult["NAV_RESULT"])):?>	<div class="search-result__full"><br/><?echo GetMessage("CT_BSP_FOUND")?>: <?echo $arResult["NAV_RESULT"]->SelectedRowsCount()?></div><?endif;?>
					<?
					// $arWhere = array();
					// if(!empty($arResult["TAGS_CHAIN"]))
					// {
					// 	$tags_chain = '';
					// 	foreach($arResult["TAGS_CHAIN"] as $arTag)
					// 		$tags_chain .= ' '.$arTag["TAG_NAME"].' [<a href="'.$arTag["TAG_WITHOUT"].'" class="search-tags-link" rel="nofollow">x</a>]';
					// 	$arWhere[] = GetMessage("CT_BSP_TAGS").' &mdash; '.$tags_chain;
					// }

					// if($arParams["SHOW_WHERE"])
					// {
					// 	$where = GetMessage("CT_BSP_EVERYWHERE");
					// 	foreach($arResult["DROPDOWN"] as $key=>$value)
					// 		if($arResult["REQUEST"]["WHERE"]==$key)
					// 			$where = $value;

					// 	$arWhere[] = GetMessage("CT_BSP_WHERE").' &mdash; '.$where;
					// }

					// if($arParams["SHOW_WHEN"])
					// {
					// 	if($arResult["REQUEST"]["FROM"] && $arResult["REQUEST"]["TO"])
					// 		$when = GetMessage("CT_BSP_DATES_FROM_TO", array("#FROM#" => $arResult["REQUEST"]["FROM"], "#TO#" => $arResult["REQUEST"]["TO"]));
					// 	elseif($arResult["REQUEST"]["FROM"])
					// 		$when = GetMessage("CT_BSP_DATES_FROM", array("#FROM#" => $arResult["REQUEST"]["FROM"]));
					// 	elseif($arResult["REQUEST"]["TO"])
					// 		$when = GetMessage("CT_BSP_DATES_TO", array("#TO#" => $arResult["REQUEST"]["TO"]));
					// 	else
					// 		$when = GetMessage("CT_BSP_DATES_ALL");

					// 	$arWhere[] = GetMessage("CT_BSP_WHEN").' &mdash; '.$when;
					// }

					 if(count($arWhere))
					 	echo GetMessage("CT_BSP_WHERE_LABEL"),': ',implode(", ", $arWhere);
					?>
					</div>
					<?//div class="search-advanced-result"?>

					<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):?>
					<div class="search-language-guess">
						<?= GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'));?>
					</div>
				<? endif;?>

				<div class="search-result__full">
					<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
					<? elseif($arResult["ERROR_CODE"]!=0):?>
						<p><?=GetMessage("CT_BSP_ERROR")?></p>
						<?ShowError($arResult["ERROR_TEXT"]);?>
						<p><?=GetMessage("CT_BSP_CORRECT_AND_CONTINUE")?></p>
						<br /><br />
						<p><?=GetMessage("CT_BSP_SINTAX")?><br /><b><?=GetMessage("CT_BSP_LOGIC")?></b></p>
						<table border="0" cellpadding="5">
							<tr>
								<td align="center" valign="top"><?=GetMessage("CT_BSP_OPERATOR")?></td><td valign="top"><?=GetMessage("CT_BSP_SYNONIM")?></td>
								<td><?=GetMessage("CT_BSP_DESCRIPTION")?></td>
							</tr>
							<tr>
								<td align="center" valign="top"><?=GetMessage("CT_BSP_AND")?></td><td valign="top">and, &amp;, +</td>
								<td><?=GetMessage("CT_BSP_AND_ALT")?></td>
							</tr>
							<tr>
								<td align="center" valign="top"><?=GetMessage("CT_BSP_OR")?></td><td valign="top">or, |</td>
								<td><?=GetMessage("CT_BSP_OR_ALT")?></td>
							</tr>
							<tr>
								<td align="center" valign="top"><?=GetMessage("CT_BSP_NOT")?></td><td valign="top">not, ~</td>
								<td><?=GetMessage("CT_BSP_NOT_ALT")?></td>
							</tr>
							<tr>
								<td align="center" valign="top">( )</td>
								<td valign="top">&nbsp;</td>
								<td><?=GetMessage("CT_BSP_BRACKETS_ALT")?></td>
							</tr>
						</table>
						<? elseif(count($arResult["SEARCH"])>0): ?>
							<?if($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
							<?foreach($arResult["SEARCH"] as $arItem):?>
							<div class="search-item">
								<h4><a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a></h4>

								<div class="search-preview]"><?echo $arItem["BODY_FORMATED"]?></div>
                                <?if(($_GET["category"] == "all" || $_GET["category"] == "iblock_catalog" || empty($_GET["category"])) && $arItem["CHAIN_PATH"]):?>
                                <?
                                    $arItem["CHAIN_PATH"] = explode("&nbsp;/&nbsp;", $arItem["CHAIN_PATH"]);
                                    unset($arItem["CHAIN_PATH"][count($arItem["CHAIN_PATH"]) - 1]);
                                    $arItem["CHAIN_PATH"] = implode(" / ", $arItem["CHAIN_PATH"]);
                                    ?>
                                <div class="search-preview">
                                    <small>
                                        <?=GetMessage("SEARCH_PATH")?>&nbsp;<?=$arItem["CHAIN_PATH"]?> <?=$arItem["SECTION_CHAIN"]?>
                                    </small>
                                </div><?
		                        endif;?>
								<?if(
									($arParams["SHOW_ITEM_DATE_CHANGE"] != "N")
									|| ($arParams["SHOW_ITEM_PATH"] == "Y" && $arItem["CHAIN_PATH"])
									|| ($arParams["SHOW_ITEM_TAGS"] != "N" && !empty($arItem["TAGS"]))
								):?>
								<div class="search-item-meta">
									<?if (
										$arParams["SHOW_RATING"] == "Y"
										&& strlen($arItem["RATING_TYPE_ID"]) > 0
										&& $arItem["RATING_ENTITY_ID"] > 0
									):?>
									<div class="search-item-rate">
										<?
										$APPLICATION->IncludeComponent(
											"bitrix:rating.vote", $arParams["RATING_TYPE"],
											Array(
												"ENTITY_TYPE_ID" => $arItem["RATING_TYPE_ID"],
												"ENTITY_ID" => $arItem["RATING_ENTITY_ID"],
												"OWNER_ID" => $arItem["USER_ID"],
												"USER_VOTE" => $arItem["RATING_USER_VOTE_VALUE"],
												"USER_HAS_VOTED" => $arItem["RATING_USER_VOTE_VALUE"] == 0? 'N': 'Y',
												"TOTAL_VOTES" => $arItem["RATING_TOTAL_VOTES"],
												"TOTAL_POSITIVE_VOTES" => $arItem["RATING_TOTAL_POSITIVE_VOTES"],
												"TOTAL_NEGATIVE_VOTES" => $arItem["RATING_TOTAL_NEGATIVE_VOTES"],
												"TOTAL_VALUE" => $arItem["RATING_TOTAL_VALUE"],
												"PATH_TO_USER_PROFILE" => $arParams["~PATH_TO_USER_PROFILE"],
											),
											$component,
											array("HIDE_ICONS" => "Y")
										);?>
									</div>
								<? endif;?>
								<?if($arParams["SHOW_ITEM_TAGS"] != "N" && !empty($arItem["TAGS"])):?>
								<div class="search-item-tags"><label><?echo GetMessage("CT_BSP_ITEM_TAGS")?>: </label><?
								foreach ($arItem["TAGS"] as $tags):
									?><a href="<?=$tags["URL"]?>"><?=$tags["TAG_NAME"]?></a> <?
								endforeach;
								?></div>
							<? endif;?>

							<?if($arParams["SHOW_ITEM_DATE_CHANGE"] != "N"):?>
							<div class="search-item-date"><label><?echo GetMessage("CT_BSP_DATE_CHANGE")?>: </label><span><?echo $arItem["DATE_CHANGE"]?></span></div>
						<? endif;?>
					</div>
				<? endif?>
			</div>
		<?	endforeach;?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
		<?if($arParams["SHOW_ORDER_BY"] != "N"):?>
		<div class="search-sorting"><label><?echo GetMessage("CT_BSP_ORDER")?>:</label>&nbsp;
			<?if($arResult["REQUEST"]["HOW"]=="d"):?>
			<a href="<?=$arResult["URL"]?>&amp;how=r"><?=GetMessage("CT_BSP_ORDER_BY_RANK")?></a>&nbsp;<b><?=GetMessage("CT_BSP_ORDER_BY_DATE")?></b>
			<? else:?>
				<b><?=GetMessage("CT_BSP_ORDER_BY_RANK")?></b>&nbsp;<a href="<?=$arResult["URL"]?>&amp;how=d"><?=GetMessage("CT_BSP_ORDER_BY_DATE")?></a>
			<? endif;?>
		</div>
		<?endif;?>
	<? else:?>
		<?ShowNote(GetMessage("CT_BSP_NOTHING_TO_FOUND"));?>
	<? endif;?>
	</div>
</div>
</div>
</section>