<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

global $APPLICATION;
$arResult['ERROR_MESSEGE'] = $APPLICATION->arAuthResult;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
//$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>


<script>
    var AUTH_PATH_PASS = "/?ajax_call_pass=Y";
    function isExistsPass(){
        var dataForm = {};
        dataForm["AUTH_FORM"] = document.getElementById("AUTH_FORM_PASS").value;
        dataForm["TYPE"] = document.getElementById("TYPE_PASS").value;
        dataForm["USER_EMAIL"] = document.getElementById("USER_EMAIL_PASS").value;
        dataForm["send_account_info"] = document.getElementById("send_account_info").value;

        //$("#analogue > .wrapper > .section-content").load( url + " .section-content .to-reload-by-filter", arrAjax);
        BX.ajax.post(
            AUTH_PATH_PASS,
            dataForm,
            function (data) {

                var arData = data.split("----------------=======================------------------");
                //console.log(arData);
                //if (!arData[1]) {
                //    location.reload();
                //} else {
                    document.getElementById("modal-forgot-pass").innerHTML = arData[1];
                //}
                //return false;
            }
        );


    }

</script>

<?if($_GET["ajax_call_pass"] == "Y"){
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "----------------=======================------------------";
}?>


<div class="forgot-pass main-modal">
    <h2>Восстановление пароля</h2>

<?if($arResult['ERROR_MESSEGE']["TYPE"] != "OK"):?>

    <?if($arResult['ERROR_MESSEGE']["TYPE"] == "ERROR"):?>
        <?$arResult['ERROR_MESSEGE']["MESSAGE"] = str_replace("Логин или EMail не найдены", "EMail не найден", $arResult['ERROR_MESSEGE']["MESSAGE"]);?>
        <span class="error-message"><?=$arResult['ERROR_MESSEGE']["MESSAGE"];?></span>
    <?endif;?>

	<form name="bform" method="post" target="_top" onsubmit="isExistsPass(); return false;" action="<?=$arResult["AUTH_URL"]?>">

		<input type="hidden" name="AUTH_FORM" id="AUTH_FORM_PASS" value="Y">
		<input type="hidden" name="TYPE" id="TYPE_PASS" value="SEND_PWD">


        <div class="mail">
            <label for="email">Введите ваш e-mail</label>

            <input type="email" name="USER_EMAIL" maxlength="255" value="" id="USER_EMAIL_PASS" required/>

        </div>

        <p>Укажите адрес эл. почты, который вы использовали при регистрации. Мы вышлем вам инструкцию по восстановлению пароля.</p>
        <div class="submit">
            <input type="submit" name="send_account_info" id="send_account_info" value="Восстановить пароль"/>
            <!--<button>Восстановить пароль</button>-->
        </div>

	</form>
<?else:?>

    <?if($arResult['ERROR_MESSEGE']["TYPE"] == "OK"):?>

        <p><?=$arResult['ERROR_MESSEGE']["MESSAGE"];?></p>
    <?endif;?>
<?endif;?>
</div>

<?if($_GET["ajax_call_pass"] == "Y"){
    die;
}?>
