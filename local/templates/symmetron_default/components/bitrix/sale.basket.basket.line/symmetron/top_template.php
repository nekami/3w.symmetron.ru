<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div class="bx-hdr-profile">
    <?if($arParams["SHOW_AUTHOR"] == "Y"):?>
        <div class="auth-wrapper">

            <?if ($USER->IsAuthorized()):
                $name = trim($USER->GetFullName());
                if (! $name)
                    $name = trim($USER->GetLogin());
                if (strlen($name) > 15)
                    $name = substr($name, 0, 12).'...';
                ?>
                <a class="auth-profile" href="<?=$arParams['PATH_TO_PROFILE']?>"><?=htmlspecialcharsbx($name)?></a>
                <a class="auth-logout" href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
            <?else:
                $arParamsToDelete = array(
                    "login",
                    "login_form",
                    "logout",
                    "register",
                    "forgot_password",
                    "change_password",
                    "confirm_registration",
                    "confirm_code",
                    "confirm_user_id",
                    "logout_butt",
                    "auth_service_id",
                    "clear_cache"
                );
                $currentUrl = urlencode($APPLICATION->GetCurPageParam("", $arParamsToDelete));

                if ($arParams['AJAX'] == 'N') {
                    ?><script type="text/javascript"><?=$cartId?>.currentUrl = '<?=$currentUrl?>';</script><?
                }
                else {
                    $currentUrl = '#CURRENT_URL#';
                }
                ?>
                <?//$APPLICATION->SetTitle("Новая страница");?>

            <a class="auth-login" rel="modal:open" href="#modal-login">Вход</a>
                <div id="modal-login" class="modal">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:system.auth.form",
                        "",
                        Array(
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "",
                            "REGISTER_URL" => "",
                            "SHOW_ERRORS" => "Y"
                        )
                    );?>

                </div>

                <div id="modal-forgot-pass" class="modal">

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:system.auth.forgotpasswd",
                        "redirect",
                        Array()
                    );?>
                </div>
            
                <?/*<a class="auth-registration" href="<?=$arParams['PATH_TO_REGISTER']?>?register=yes&backurl=<?=$currentUrl; ?>"><?=GetMessage('TSB1_REGISTER')?></a>*/?>

            <a class="auth-registration" href="#modal-registration" rel="modal:open">Регистрация</a>
            <?endif?>
        </div>
    <?endif?>

    <?if (!$arResult["DISABLE_USE_BASKET"]):?>
        <div class="cart-wrapper">
            <div class="top-cart">
                <a href="<?= $arParams['PATH_TO_BASKET'] ?>">
					<span>
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640"><defs><path d="M640 148.74L576 388.74L208 424.74L184 468.74L560 468.74L560 510L120 510L172 404.74L88 68.74L0 68.74L0 28.74L120 28.74L148 148.74L640 148.74ZM208 384.74L544 348.74L588 188.74L160 188.74L208 384.74Z" id="a4hFTNc6yA"></path><path d="M155 557.46C155 577.28 139.32 593.37 120 593.37C100.68 593.37 85 577.28 85 557.46C85 537.64 100.68 521.55 120 521.55C139.32 521.55 155 537.64 155 557.46Z" id="i5XpF6zBME"></path><path d="M505 555.91C505 575.73 489.32 591.82 470 591.82C450.68 591.82 435 575.73 435 555.91C435 536.09 450.68 520 470 520C489.32 520 505 536.09 505 555.91Z" id="a7zIP5CG5"></path></defs><g><g><g><use xlink:href="#a4hFTNc6yA" opacity="1" fill-opacity="1"></use><g><use xlink:href="#a4hFTNc6yA" opacity="1" fill-opacity="0" stroke-width="0" stroke-opacity="0"></use></g></g><g><use xlink:href="#i5XpF6zBME" opacity="1" fill-opacity="1"></use></g><g><use xlink:href="#a7zIP5CG5" opacity="1" fill-opacity="1"></use></g></g></g></svg>
					</span><span><?=GetMessage('TSB1_CART')?></span></a>
            </div>
        </div>
    <?endif;?>
    
    <?if ($USER->IsAuthorized()):?>
        <div class="favorite-wrapper">
            <div class="favorite-top">
                <a href="<?= $arParams['PATH_TO_FAVORITE'] ?>"><span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="5 1 28 35" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"/>
					<style>
						#fill-heart {
							fill: transparent;
						}
					</style>
										
					<g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""/>
					<path  id="svg_22" d="M-29.97297297297297,2.4594594594594668 " /><path  id="svg_2" d="M19.324324324324337,8.837837837837846 " />
					<path  id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"  /></g>
					
				</svg>
                </span><span><?=GetMessage("TSB1_FAVORITE")?></span></a>
            </div>
        </div>
    <?endif;?>

	<div id="modal-registration" class="modal">

                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.register",
                        "",
                        Array(
                            "AUTH" => "Y",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "REQUIRED_FIELDS" => array("EMAIL","PERSONAL_PHONE"),
                            "SET_TITLE" => "N",
                            "SHOW_FIELDS" => array("EMAIL","PERSONAL_PHONE"),
                            "SUCCESS_PAGE" => "",
                            "USER_PROPERTY" => array(),
                            "USER_PROPERTY_NAME" => "",
                            "USE_BACKURL" => "N"
                        )
                    );

                    ?>
                </div>
</div>