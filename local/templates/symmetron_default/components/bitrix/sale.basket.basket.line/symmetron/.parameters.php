<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arTemplateParameters = array(
	"PATH_TO_FAVORITE" => array(
		"NAME" => GetMessage("TSB1_PATH_TO_FAVORITE"),
		"TYPE" => "STRING",
		"DEFAULT" => "/",
		"GROUPE" => "BASE",
	),
);
?>
