<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
global $APPLICATION;
$arParams["~AUTH_RESULT"] = $APPLICATION->arAuthResult;
//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<section class="card" data-id="262403">
    <div class="wrapper">



<div class="changepass">
    <h2><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h2>

	<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
<?if (strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<? endif ?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="CHANGE_PWD">

		<div class="bx-authform-formgroup-container" style="display: none">
			<div class="bx-authform-label-container"><?=GetMessage("AUTH_LOGIN")?></div>
			<div class="bx-authform-input-container">
				<input type="hidden" name="USER_LOGIN" id="USER_LOGIN_CH_PASS" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
			</div>
		</div>

		<div class="bx-authform-formgroup-container" style="display: none">
			<div class="bx-authform-label-container"><?=GetMessage("AUTH_CHECKWORD")?></div>
			<div class="bx-authform-input-container">
				<input type="hidden" name="USER_CHECKWORD" maxlength="255" value="<?=$arResult["USER_CHECKWORD"]?>" />
			</div>
		</div>


        <!--пароль-->
        <div class="password">
            <label for="password"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?> *</label>
            <?if($arResult["SECURE_AUTH"]):?>
                <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

                <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = '';
                </script>
            <?endif?>
            <input type="password" name="USER_PASSWORD" id="password" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" required>
        </div>


		<?/*<div class="bx-authform-formgroup-container">
			<div class="bx-authform-label-container"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></div>
			<div class="bx-authform-input-container">
<?if($arResult["SECURE_AUTH"]):?>
				<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = '';
</script>
<?endif?>
				<input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" />
			</div>
		</div>*/?>


        <!--подтверждение-->


        <div class="password-repeat">
            <label for="password-repeat"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?> *</label>
            <?if($arResult["SECURE_AUTH"]):?>
                <div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

                <script type="text/javascript">
                    document.getElementById('bx_auth_secure_conf').style.display = '';
                </script>
            <?endif?>
            <input type="password" name="USER_CONFIRM_PASSWORD" id="password-repeat" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" required>
        </div>

		<?/*<div class="bx-authform-formgroup-container">
			<div class="bx-authform-label-container"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></div>
			<div class="bx-authform-input-container">
<?if($arResult["SECURE_AUTH"]):?>
				<div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

<script type="text/javascript">
document.getElementById('bx_auth_secure_conf').style.display = '';
</script>
<?endif?>
				<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" />
			</div>
		</div>*/?>


		<?/*<div class="bx-authform-formgroup-container">
			<input type="submit" class="btn btn-primary" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" />
		</div>*/?>

        <div class="submit">
            <input type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>">
        </div>
        <div class="min-chars"><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></div>


        <?
        if(!empty($arParams["~AUTH_RESULT"])):
            $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
            ?>
            <?/*<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>*/?>
            <?$text = explode("\n", $text);?>
            <?foreach ($text as $mess):?>
            <?if(strpos($mess, "Неверное подтверждение пароля") !== false) {
                $mess = "Пароли не совпадают!";
            }?>
            <?if(strpos($mess, "Неверное контрольное слово для логина") !== false) {
                $mess = "Некорректный адрес для восстановления пароля. Скопируйте адрес из письма полностью!";
            }?>

            <?$mess = str_replace(array("сменен", "EMail"), array("изменен", "e-mail"), $mess);?>

            <?$color = $arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "success":"error";?>
            <?echo "<div class='$color'>$mess</div>";?>
        <?endforeach;?>
        <?endif?>


	</form>

</div>


</section>

<script type="text/javascript">
document.bform.USER_LOGIN.focus();
</script>
<?if($arParams["~AUTH_RESULT"]["TYPE"] == "OK"):?>
<script>

    function func() {
        BX.ready(function () {
            $("#modal-login").modal();
        });
    }

    setTimeout(func, 2000);

</script>
<?endif;?>
<script>
   <?/* //BX.ready(function(){
    //    document.getElementById("USER_LOGIN_CH_PASS").value = "<?=$arResult["LAST_LOGIN"]?>";
    //});*/?>
</script>