<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if(!empty($arResult['ITEM']["PROPERTIES"]["PRODUCERID"]["VALUE"])){
    $res = CIBlockElement::GetByID($arResult['ITEM']["PROPERTIES"]["PRODUCERID"]["VALUE"]);
    if($ar_res = $res->GetNext())
        $arResult['ITEM']["PROPERTIES"]["PRODUCERID"]["VALUE"] = $ar_res['NAME'];
}