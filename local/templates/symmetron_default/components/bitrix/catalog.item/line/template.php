<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$areaId = $arResult['AREA_ID'];
?>
<tr class="mini-card__select" id="<?=$areaId?>">
    <?php
    foreach ($arResult["COLUMNS"] as $column) {
        $value = "";
        if (isset($column["PROPERTY_CODE"])) {
            $property = &$arResult["ITEM"]["DISPLAY_PROPERTIES"][$column["PROPERTY_CODE"]];
            if (!empty($property["DISPLAY_VALUE"])) {
                $value = $property["DISPLAY_VALUE"];
                if($column["PROPERTY_CODE"] == "PRODUCERID")
                    $value = trim(strip_tags($value));
            }
        } else {
            $value = $arResult["ITEM"][$column["CODE"]];
        }
        ?>
        <td <?= $column["CODE"] == "NAME" ? "id='name_prod_".$arResult["ITEM"]["ID"]."'" : "" ?>><?= $value ?></td>
        <?
    } ?>
</tr>
