<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$arMenuTypes=GetMenuTypes()?>
<a href="<?=$arParams["MENU_MAIN_LINK"]?>" class="nav-header"><?=$arMenuTypes[$arParams["ROOT_MENU_TYPE"]]?></a>
<ul class="nav-list">
    <?foreach ($arResult as $link):?>
        <li><a href="<?=$link["LINK"]?>"><?=$link["TEXT"]?></a></li>
    <?endforeach;?>
</ul>
