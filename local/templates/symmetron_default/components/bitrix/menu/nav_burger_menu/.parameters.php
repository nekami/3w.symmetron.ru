<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTemplateParameters = array(
	"MENU_MAIN_LINK"=>array(
		"NAME" => GetMessage("MENU_MAIN_LINK"),
		"TYPE" => "STRING",
		"DEFAULT" => "/",
		"PARENT" => "BASE",
	)
);
?>
