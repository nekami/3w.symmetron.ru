<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

?>

<section class="ordering">
    <div class="wrapper">

        <div class="bx-soa-empty-cart-container">
            <div class="bx-soa-empty-cart-image">
                <img src="" alt="">
            </div>
            <div class="bx-soa-empty-cart-text"><?= Loc::getMessage("SYMMETRON_SOA_CONFIRM") ?></div>
            <div class="bx-soa-empty-cart-desc"><?= Loc::getMessage(
                    'SYMMETRON_SOA_CONFIRM_BACK',
                    array(
                        '#A1#' => '<a style="color: #008aff" href="/">',
                        '#A2#' => '</a>'
                    )) ?>
            </div>
        </div>
    </div>
</section>

<script>
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        'ecommerce': {
            'purchase':
            <?=CUtil::PhpToJSObject($arResult['YA_RESULT'])?>
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Purchase',
        'gtm-ee-event-non-interaction': 'False',
    });
</script>