<?
$MESS["SYMMETRON_SOA_TITLE"] = "Оформление заказа";

$MESS["SYMMETRON_SOA_DELIVERY_TITLE"] = "Способ получения";
$MESS["SYMMETRON_SOA_STORES_TITLE"] = "Место получения";
$MESS["SYMMETRON_SOA_TOTAL"] = "Итого";
$MESS["SYMMETRON_SOA_SELL"] = "Покупка";
$MESS["SYMMETRON_SOA_CHECK"] = "Запрос цен";
$MESS["SYMMETRON_SOA_ASK"] = "Запрос образцов";
$MESS["SYMMETRON_SOA_POSITIONS"] = "поз.";

$MESS["SYMMETRON_SOA_ORGANISATION_ADDRESS"] = "Совпадает с юридическим адресом организации";

$MESS["SYMMETRON_SOA_SUBMIT_BUTTON"] = "Оформить заказ";

$MESS["EMPTY_BASKET_TITLE"] = "Ваша корзина пуста";
$MESS["EMPTY_BASKET_HINT"] = "#A1#Нажмите здесь#A2#, чтобы продолжить покупки";

$MESS["SYMMETRON_SOA_CONFIRM"] = "Заказ оформлен";
$MESS["SYMMETRON_SOA_CONFIRM_BACK"] = "#A1#Нажмите здесь#A2#, чтобы вернуться на главную";


$MESS["SBB_POSITION_1"] = "позиция";
$MESS["SBB_POSITION_2"] = "позиции";
$MESS["SBB_POSITION_5"] = "позиций";

$MESS['PERSONAL_DATA_LABEL'] = "Размещая заказ покупатель дает своё согласие на передачу своих персональных данных третьим лицам в целях организации обработки и доставки заказанного товара.";
$MESS['PERSONAL_DATA_ERROR'] = "Чтобы продолжить, установите этот флажок.";
?>
