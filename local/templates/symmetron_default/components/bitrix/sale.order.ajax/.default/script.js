BX.ready(function () {

    let campanyAddress = {
        "country": "",
        "region_with_type": "",
        "city_with_type": "",
        "settlement_with_type": "",
        "street_with_type": "",
        "house": "",
        "block": "",
        "flat": "",
        "postal_code": "",
    }

    let address = {
        "country": "",
        "region_with_type": "",
        "city_with_type": "",
        "settlement_with_type": "",
        "street_with_type": "",
        "house": "",
        "block": "",
        "flat": "",
        "postal_code": "",
    }

    if (('#ORDER_PROP_PHONE').length !== 0) {
        //$('#ORDER_PROP_PHONE').mask('+7 (000) 000-0000');
    }

    $('.delivery-input').on('click', "label", function () {

        var input = $(this).children('input');
        if (input.prop('checked')) {
            var id = input.val();
            var delivery = $("#delivery_" + id);
            if (delivery.length > 0) {
                if (!(delivery.hasClass('open'))) {
                    $(".place").addClass('close');
                    delivery.addClass('open');
                }

                $("#PARAM_ADDRESS_INPUT").css("display", "")

            } else {
                $(".place").removeClass('close');
                $(".delivery-fields").removeClass('open');
                $("#PARAM_ADDRESS_INPUT").css("display", "none")
            }
        }
    });


    function checkAddres() {
        if (document.getElementById("DELIVERY_TRANS_COMP").checked) {

            if (!document.getElementById('ORDER_PROP_ADDRESS_COUNTRY').value ||
                !document.getElementById('ORDER_PROP_ADDRESS_CITY').value ||
                !document.getElementById('ORDER_PROP_REGION').value ||
                !document.getElementById('ORDER_PROP_ADDRESS_BUILDING').value ||
                !document.getElementById('ORDER_PROP_ADDRESS_INDEX').value) {

                if (document.getElementById('ADDRESS_COMPANY_OR_NO').checked)
                    document.getElementById('ADDRESS_COMPANY_OR_NO_ERROR').innerHTML = "Компания не была выбрана из списка"
                else {
                    document.getElementById('ADDRESS_COMPANY_OR_NO_ERROR').innerHTML = "Адрес доставки заполнен не полностью"
                    document.getElementById('ORDER_PROP_ADDRESS_FROM_DADATA').classList.add("error")
                }
                return false
            }
        }else{
		$("#ORDER_PROP_ADDRESS_COUNTRY").val("");
		$("#ORDER_PROP_REGION").val("");
		$("#ORDER_PROP_ADDRESS_STREET").val("");
		$("#ORDER_PROP_ADDRESS_BUILDING").val("");
		$("#ORDER_PROP_ADDRESS_BLOCK").val("");
		$("#ORDER_PROP_ADDRESS_ROOM").val("");
		$("#ORDER_PROP_ADDRESS_INDEX").val("");
		$("#ORDER_PROP_ADDRESS_CITY").val("");
		$("#ORDER_PROP_ADDRESS_FROM_DADATA").val("");
	}

        return true
    }


    $('.submit').on('click', "button", function () {

        var form = $(".ordering-form").children("form");
        if (form) {
            if (!checkAddres() && !shortOrder) {
                return
            }

            if (shortOrder && !document.getElementById('ORDER_PROP_ADDRESS_INDEX').value) {
                $("#ORDER_PROP_ADDRESS_INDEX").val("0");
            }

            var url = form.prop("action");
            var ajaxValues = $(this.form).serializeArray();
            var data = {};
            for (var i = 0; i < ajaxValues.length; i++)
                data[ajaxValues[i].name] = ajaxValues[i].value;

            if (!data.hasOwnProperty('PERSONAL_DATA')) {
                document.getElementById('personal-data-input-error').style.display = 'block';
                return;
            } else {
                document.getElementById('personal-data-input-error').style.display = 'none';
            }

            BX.ajax({
                url: url,
                data: data,
                method: "POST",
                dataType: "json",
                timeout: 30,
                processData: true,
                scriptsRunFirst: false,
                emulateOnload: false,
                start: true,
                cache: false,
                //async: false,
                onsuccess: function (data) {
                    if (data.order["ID"] !== undefined) {
                        window.location.replace(data.order["REDIRECT_URL"]);
                    } else {
                        var errors = ""
                        var errorsIDs = []
                        var fields = [
                            "ORDER_PROP_COMPANY",
                            "ORDER_PROP_INN",
                            "ORDER_PROP_CONTACT_PERSON",
                            "ORDER_PROP_PHONE",
                            "ORDER_PROP_EMAIL",
                            "ORDER_PROP_ADDRESS_COUNTRY",
                            "ORDER_PROP_ADDRESS_CITY",
                            "ORDER_PROP_ADDRESS_STREET",
                            "ORDER_PROP_ADDRESS_BUILDING",
                            "ORDER_PROP_ADDRESS_BLOCK",
                            "ORDER_PROP_ADDRESS_ROOM",
                            "ORDER_PROP_ADDRESS_INDEX"
                        ]
                        for (var j = 0; j < data.order["ERROR"]["PROPERTY"].length; j++) {
                            console.log(data.order["ERROR"])

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Организация обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_NAME_FROM_DADATA").className = "error";
                                document.getElementById("ORDER_PROP_NAME_FROM_DADATA_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_NAME_FROM_DADATA")
                            }

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Название компании обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_COMPANY").className = "error";
                                document.getElementById("ORDER_PROP_COMPANY_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_COMPANY")
                            }

                            if ((data.order["ERROR"]["PROPERTY"][j].indexOf('ИНН обязательно для заполнения') + 1) || (data.order["ERROR"]["PROPERTY"][j].indexOf('Свойство заказа "ИНН" - не соответствует шаблону') + 1)) {
                                document.getElementById("ORDER_PROP_INN").className = "error";
                                document.getElementById("ORDER_PROP_INN_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_INN")
                            }

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Контактное лицо обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_CONTACT_PERSON").className = "error";
                                document.getElementById("ORDER_PROP_CONTACT_PERSON_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_CONTACT_PERSON")
                            }

                            if ((data.order["ERROR"]["PROPERTY"][j].indexOf('Телефон обязательно для заполнения') + 1) || (data.order["ERROR"]["PROPERTY"][j].indexOf('Свойство заказа "Телефон" - не соответствует шаблону') + 1)) {
                                document.getElementById("ORDER_PROP_PHONE").className = "error";
                                document.getElementById("ORDER_PROP_PHONE_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_PHONE")
                            }

                            if ((data.order["ERROR"]["PROPERTY"][j].indexOf('E-Mail обязательно для заполнения') + 1) || (data.order["ERROR"]["PROPERTY"][j].indexOf('в поле "E-Mail" не корректен') + 1)) {
                                document.getElementById("ORDER_PROP_EMAIL").className = "error";
                                document.getElementById("ORDER_PROP_EMAIL_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_EMAIL")
                            }

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Город / Населенный пункт обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_ADDRESS_COUNTRY").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_COUNTRY_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_COUNTRY")
                            }

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Город / Населенный пункт обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_ADDRESS_CITY").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_CITY_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_CITY")
                            }

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Улица обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_ADDRESS_STREET").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_STREET_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_STREET")
                            }

                            if ((data.order["ERROR"]["PROPERTY"][j].indexOf('Дом обязательно для заполнения') + 1) || (data.order["ERROR"]["PROPERTY"][j].indexOf('Свойство заказа "Дом" - не соответствует шаблону') + 1)) {
                                document.getElementById("ORDER_PROP_ADDRESS_BUILDING").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_BUILDING_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_BUILDING")
                            }

                            if (data.order["ERROR"]["PROPERTY"][j].indexOf('Корпус обязательно для заполнения') + 1) {
                                document.getElementById("ORDER_PROP_ADDRESS_BLOCK").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_BLOCK_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_BLOCK")
                            }

                            if ((data.order["ERROR"]["PROPERTY"][j].indexOf('Офис / Квартира обязательно для заполнения') + 1) || (data.order["ERROR"]["PROPERTY"][j].indexOf('Свойство заказа "Офис / Квартира" - не соответствует шаблону') + 1)) {
                                document.getElementById("ORDER_PROP_ADDRESS_ROOM").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_ROOM_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_ROOM")
                            }

                            if ((data.order["ERROR"]["PROPERTY"][j].indexOf('Индекс обязательно для заполнения') + 1) || (data.order["ERROR"]["PROPERTY"][j].indexOf('Свойство заказа "Индекс" - не соответствует шаблону') + 1)) {
                                document.getElementById("ORDER_PROP_ADDRESS_INDEX").className = "error";
                                document.getElementById("ORDER_PROP_ADDRESS_INDEX_ERROR").innerHTML = data.order["ERROR"]["PROPERTY"][j];
                                //delete data.order["ERROR"]["PROPERTY"][j];
                                errorsIDs.push("ORDER_PROP_ADDRESS_INDEX")
                            }
                        }


                        for (var i in fields) {
                            if (!(errorsIDs.indexOf(fields[i]) + 1)) {
                                document.getElementById(fields[i]).className = "";
                                document.getElementById(fields[i] + "_ERROR").innerHTML = "";
                            }
                        }

                        //for (var j = 0; j < data.order["ERROR"]["PROPERTY"].length; j++)
                        //    $errors += "<span class='error'>"+data.order["ERROR"]["PROPERTY"][j]+"</span>"
                        $("div#errors").html(errors);
                        $('html, body').animate({scrollTop: 0}, 'slow');

                        console.log(data.order["ERROR"]);
                        //for (var j = 0; j < data.order["ERROR"]["PROPERTY"].length; j++)
                        //console.log(data.order["ERROR"]["PROPERTY"]);
                    }
                }
            });
        }

    });


    function setAddress(campanyAddress) {
        $("#ORDER_PROP_ADDRESS_COUNTRY").val(campanyAddress.country ? campanyAddress.country : "")

        $("#ORDER_PROP_REGION").val(campanyAddress.region_with_type ? campanyAddress.region_with_type : "")

        $("#ORDER_PROP_ADDRESS_CITY").val(campanyAddress.city_with_type ? campanyAddress.city_with_type :
            campanyAddress.settlement_with_type ? campanyAddress.settlement_with_type : "")

        $("#ORDER_PROP_ADDRESS_STREET").val(campanyAddress.street_with_type ? campanyAddress.street_with_type : "")

        $("#ORDER_PROP_ADDRESS_BUILDING").val(campanyAddress.house ? campanyAddress.house : "")
        $("#ORDER_PROP_ADDRESS_BLOCK").val(campanyAddress.block ? campanyAddress.block : "")
        $("#ORDER_PROP_ADDRESS_ROOM").val(campanyAddress.flat ? campanyAddress.flat : "")
        $("#ORDER_PROP_ADDRESS_INDEX").val(campanyAddress.postal_code ? campanyAddress.postal_code : "")
    }

    //flag cheange organisation
    var goodOrganisation = false
    $("#ORDER_PROP_NAME_FROM_DADATA").suggestions({
        token: keyDadata,
        type: "PARTY",
        onSelect: function (suggestion) {
            $("#ORDER_PROP_COMPANY").val(suggestion.value)
            $("#ORDER_PROP_INN").val(suggestion.data.inn ? suggestion.data.inn : "")

            campanyAddress = suggestion.data.address.data

            if (document.getElementById("ADDRESS_COMPANY_OR_NO").checked)
                setAddress(campanyAddress)
            else
                setAddress(address)

            document.getElementById("ADDRESS_COMPANY_OR_NO").disabled = false
            goodOrganisation = true
            document.getElementById("ORDER_PROP_NAME_FROM_DADATA").classList.remove("error")
            document.getElementById("ORDER_PROP_NAME_FROM_DADATA_ERROR").innerHTML = ""
            console.log(suggestion);
        }
    });


    $("#ORDER_PROP_NAME_FROM_DADATA").on("input", function (ev) {

        campanyAddress = {
            "country": "",
            "region_with_type": "",
            "city_with_type": "",
            "settlement_with_type": "",
            "street_with_type": "",
            "house": "",
            "block": "",
            "flat": "",
            "postal_code": "",
        };

        goodOrganisation = false

        if (document.getElementById("ADDRESS_COMPANY_OR_NO").checked)
            setAddress(campanyAddress)


    });

    $("#ORDER_PROP_NAME_FROM_DADATA").on("blur", function (ev) {
        if (goodOrganisation) {
            document.getElementById("ORDER_PROP_NAME_FROM_DADATA").classList.remove("error")
            document.getElementById("ORDER_PROP_NAME_FROM_DADATA_ERROR").innerHTML = ""
        } else {
            document.getElementById("ORDER_PROP_NAME_FROM_DADATA").classList.add("error")
            document.getElementById("ORDER_PROP_NAME_FROM_DADATA_ERROR").innerHTML = "Организация не найдена"
        }
    });


    $("#ORDER_PROP_ADDRESS_FROM_DADATA").suggestions({
        token: keyDadata,
        type: "ADDRESS",
        onSelect: function (suggestion) {
            address = suggestion.data
            setAddress(address)
            //console.log(suggestion);
        }
    });


    $("#ADDRESS_COMPANY_OR_NO").on("click", function (ev) {
        if (ev.target.checked) {
            $("#ADDRESS_FIELD_DIV").css("display", "none")
            setAddress(campanyAddress)
        } else {
            $("#ADDRESS_FIELD_DIV").css("display", "")
            setAddress(address)
        }
    })


});


MapObject = function (storeId, gpsN, gpsS, allStores) {
    this.storeId = storeId
    this.gpsN = gpsN
    this.gpsS = gpsS
    this.allStores = allStores
    this.init()
}

MapObject.prototype.init = function () {

    var _this = this

    ymaps.ready(init);

    function init() {
        this.address = new ymaps.Map("address_" + _this.storeId, {
            center: [_this.gpsN, _this.gpsS],
            zoom: 17
        });

        this.address.geoObjects.add(
            new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [_this.gpsN, _this.gpsS]
                }
            })
        );
    }

    let curRadio = document.getElementById("radio_" + _this.storeId);
    let curDivCard = document.getElementById("address_" + _this.storeId);
    if (curRadio && curDivCard) {
        if (!curRadio.checked) {
            curDivCard.classList.add("grayscalemap")
        } else {
            curDivCard.classList.add("chosen")
        }
    }

    curRadio.onclick = function (ev) {

        for (let i in _this.allStores) {
            let divCard = document.getElementById("address_" + _this.allStores[i]);
            if (_this.storeId == _this.allStores[i]) {
                divCard.classList.remove("grayscalemap")
                divCard.classList.add("chosen")
            } else {
                divCard.classList.add("grayscalemap")
                divCard.classList.remove("chosen")
            }
        }
    }

    curDivCard.onclick = function (ev) {
        for (let i in _this.allStores) {
            let radio = document.getElementById("radio_" + _this.allStores[i]);
            let divCard = document.getElementById("address_" + _this.allStores[i]);
            if (radio) {
                if (_this.storeId == _this.allStores[i]) {
                    radio.checked = true
                    divCard.classList.remove("grayscalemap")
                    divCard.classList.add("chosen")
                } else {
                    radio.checked = false
                    divCard.classList.add("grayscalemap")
                    divCard.classList.remove("chosen")
                }
            }
        }
    }

    curDivCard.addEventListener("touchstart", function (ev) {
        for (let i in _this.allStores) {
            let radio = document.getElementById("radio_" + _this.allStores[i]);
            let divCard = document.getElementById("address_" + _this.allStores[i]);
            if (radio) {
                if (_this.storeId == _this.allStores[i]) {
                    radio.checked = true
                    divCard.classList.remove("grayscalemap")
                    divCard.classList.add("chosen")
                } else {
                    radio.checked = false
                    divCard.classList.add("grayscalemap")
                    divCard.classList.remove("chosen")
                }
            }
        }
    }, false);
}
