<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

//echo "<pre>";
//print_r($arResult["ORDER_PROP"]["RELATED"]);
//echo "</pre>";

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
function __getFieldHtml($field)
{
    ?>
    <label for="<?= trim($field["FIELD_ID"]) ?>"><?= $field["NAME"] ?><?= ($field["REQUIRED"] == "Y") ? "" : "" ?></label>
    <input type="<?= $field["TYPE"] ?>"
           id="<?= trim($field["FIELD_ID"]) ?>"
           name="<?= $field["FIELD_NAME"] ?>"
           placeholder="<?= $field["DESCRIPTION"] ?>"
           value='<?= str_replace("'", '"', $field["VALUE"]) ?>'
    >
    <span class="error-message" id="<?= trim($field["FIELD_ID"]) ?>_ERROR"></span>
    <?
}

$arPsositionVariants = array(
    "1" => GetMessage("SBB_POSITION_1"),
    "2" => GetMessage("SBB_POSITION_2"),
    "3" => GetMessage("SBB_POSITION_2"),
    "4" => GetMessage("SBB_POSITION_2"),
    "5" => GetMessage("SBB_POSITION_5"),
    "6" => GetMessage("SBB_POSITION_5"),
    "7" => GetMessage("SBB_POSITION_5"),
    "8" => GetMessage("SBB_POSITION_5"),
    "9" => GetMessage("SBB_POSITION_5"),
    "0" => GetMessage("SBB_POSITION_5"),
);

$arPsositionVariantsExaption = array(11, 12, 13, 14);


function declineWordPosition($quantVal, $arPsositionVariants, $arPsositionVariantsExaption)
{
    //склоняем слово позиция
    $mod = $quantVal % 10;
    if (in_array($mod, $arPsositionVariantsExaption))
        $posWord = GetMessage("SBB_POSITION_5");
    else
        $posWord = $arPsositionVariants[$mod];

    return $posWord;
}


?><?
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
if (strlen($request->get('ORDER_ID')) > 0) {
    include(\Bitrix\Main\Application::getDocumentRoot() . $templateFolder . '/confirm.php');

} elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET']) {
    include(\Bitrix\Main\Application::getDocumentRoot() . $templateFolder . '/empty.php');
} else { ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.7.1/dist/css/suggestions.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.7.1/dist/js/jquery.suggestions.min.js"></script>

    <section class="ordering">
        <div class="wrapper">
            <h1><?= Loc::getMessage("SYMMETRON_SOA_TITLE") ?></h1>
            <div id="errors"></div>
            <div class="ordering-form">
                <form action="<?= POST_FORM_ACTION_URI ?>" method="POST" name="ORDER_FORM">
                    <? if (!empty($arResult["PERSON_TYPE"])): ?>
                        <input type="hidden" name="PERSON_TYPE" value="<?= $arResult["PERSON_TYPE"]["ID"] ?>">
                    <? endif; ?>
                    <? if (!empty($arResult["PAY_SYSTEM"])): ?>
                        <input type="hidden" name="PAY_SYSTEM_ID" value="<?= $arResult["PAY_SYSTEM"]["ID"] ?>">
                    <? endif; ?>
                    <input type="hidden" name="<?= $arParams['ACTION_VARIABLE'] ?>" value="saveOrderAjax">
                    <input type="hidden" name="location_type" value="code">
                    <?
                    echo bitrix_sessid_post();

                    if (strlen($arResult['PREPAY_ADIT_FIELDS']) > 0) {
                        echo $arResult['PREPAY_ADIT_FIELDS'];
                    }
                    ?>


                    <? /* organisation */
                    ?>
                    <? $block = array_shift($arResult["JS_DATA"]["ORDER_PROP"]["groups"]); ?>

                    <div class="customer">

                        <div class="ordering-step__content">

                            <? foreach ($arResult["ORDER_PROP"]["USER_PROPS"] as $field): ?>
                                <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]];
                                if ($field["FIELD_ID"] == "ORDER_PROP_NAME_FROM_DADATA")
                                    $organisation = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                <? if ($field["PROPS_GROUP_ID"] != $block["ID"])
                                    continue; ?>
                                <div><? __getFieldHtml($field) ?></div>
                            <? endforeach; ?>
                        </div>
                    </div>


                    <? /* First block - company, inn */
                    ?>
                    <? $block = array_shift($arResult["JS_DATA"]["ORDER_PROP"]["groups"]); ?>

                    <div class="customer" style="display: none">

                        <div class="ordering-step__content">

                            <? foreach ($arResult["ORDER_PROP"]["USER_PROPS"] as $field): ?>
                                <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                <? if ($field["PROPS_GROUP_ID"] != $block["ID"])
                                    continue; ?>
                                <div><? __getFieldHtml($field) ?></div>
                            <? endforeach; ?>
                        </div>
                    </div>

                    <? /* Second block - fio, phone, email */
                    ?>
                    <? $block = array_shift($arResult["JS_DATA"]["ORDER_PROP"]["groups"]); ?>
                    <div class="contact-person">
                        <div class="ordering-step__content">
                            <?
                            $contactInfoData = []
                            ?>
                            <? foreach ($arResult["ORDER_PROP"]["USER_PROPS"] as $field): ?>
                                <? if ($field["PROPS_GROUP_ID"] != $block["ID"])
                                    continue; ?>
                                <? if ($field["FIELD_ID"] == "ORDER_PROP_PHONE"): ?>
                                    <? $field["DESCRIPTION"] = "+7"; ?>
                                <? endif; ?>
                                <? $contactInfoData[] = $field ?>
                            <? endforeach; ?>

                            <? $field = $contactInfoData[0]; ?>
                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                            <label for="<?= trim($field["FIELD_ID"]) ?>"><?= $field["NAME"] ?><?= ($field["REQUIRED"] == "Y") ? "" : "" ?></label>
                            <div class="contact-info__holder">
                                <div class="contact-info__input">
                                    <input type="<?= $field["TYPE"] ?>"
                                           id="<?= trim($field["FIELD_ID"]) ?>"
                                           name="<?= $field["FIELD_NAME"] ?>"
                                           placeholder="Имя и фамилия"
                                           value="<?= $field["VALUE"] ?>"
                                    >
                                    <span class="error-message" id="<?= trim($field["FIELD_ID"]) ?>_ERROR"></span>
                                </div>
                                <div class="contact-info__input">
                                    <? $field = $contactInfoData[1]; ?>
                                    <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                    <input type="<?= $field["TYPE"] ?>"
                                           id="<?= trim($field["FIELD_ID"]) ?>"
                                           name="<?= $field["FIELD_NAME"] ?>"
                                           placeholder="Номер телефона"
                                           value="<?= $field["VALUE"] ?>"
                                    >
                                    <span class="error-message" id="<?= trim($field["FIELD_ID"]) ?>_ERROR"></span>
                                </div>
                                <div class="contact-info__input">
                                    <? $field = $contactInfoData[2]; ?>
                                    <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                    <input type="<?= $field["TYPE"] ?>"
                                           id="<?= trim($field["FIELD_ID"]) ?>"
                                           name="<?= $field["FIELD_NAME"] ?>"
                                           placeholder="Электронная почта"
                                           value="<?= $field["VALUE"] ?>"
                                    >
                                    <span class="error-message" id="<?= trim($field["FIELD_ID"]) ?>_ERROR"></span>
                                </div>
                                <div class="contact-info__input">
                                    <? $field = $contactInfoData[3]; ?>
                                    <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                    <input type="<?= $field["TYPE"] ?>"
                                           id="<?= trim($field["FIELD_ID"]) ?>"
                                           name="<?= $field["FIELD_NAME"] ?>"
                                           placeholder="Доб."
                                           value="<?= $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", $GLOBALS["USER"]->GetID())["UF_DOB"]["VALUE"] ?>"
                                    >
                                    <span class="error-message" id="<?= trim($field["FIELD_ID"]) ?>_ERROR"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <? /* Third block - delivery type + address block */
                    ?>
                    <? $block = array_shift($arResult["JS_DATA"]["ORDER_PROP"]["groups"]); ?>

                    <div class="obtaining" <? if ($arResult["SHORT_ORDER"]): ?>style="display: none"<? endif;
                    ?>>
                        <h3>Способ получения заказа</h3>
                        <div class="ordering-step__content">

                            <? $isSelf = true; ?>
                            <? foreach ($arResult["JS_DATA"]["DELIVERY"] as $delivery): ?>

                                <? if (!empty($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]])
                                    && $delivery["CHECKED"] == "Y")
                                    $isSelf = false; ?>
                                <div class="delivery-input">
                                    <label>
                                        <input value="<?= $delivery["ID"] ?>" type="radio" name="DELIVERY_ID"
                                               id="DELIVERY_TRANS_COMP" <?= $delivery["CHECKED"] == "Y" ? "checked" : "" ?>>
                                        <?= $delivery["NAME"] ?>
                                    </label>
                                </div>
                            <? endforeach; ?>
                            <? //echo "<pre>";
                            //print_r($arResult["ORDER_PROP"]["RELATED"]);
                            //echo "</pre>";
                            ?>
                            <? foreach ($arResult["JS_DATA"]["DELIVERY"] as $delivery): ?>
                                <? if (!empty($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]])): ?>

                                    <div id="delivery_<?= $delivery["ID"] ?>"
                                         class="delivery-fields transport-company <?= $isSelf ? "" : "open" ?>"
                                         style="display: none">
                                        <h3><?= $block["NAME"] ?></h3>
                                        <div class="adress-place">
                                            <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                            <div class="adress-place__country"><? __getFieldHtml($field) ?></div>
                                            <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                            <div class="adress-place__city"><? __getFieldHtml($field) ?></div>

                                        </div>
                                        <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                        <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                        <div class="adress-street"><? __getFieldHtml($field) ?></div>
                                        <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                        <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                        <div class="adress-street"><? __getFieldHtml($field) ?></div>
                                        <div class="adress-last">
                                            <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                            <div class="adress-last__house"><? __getFieldHtml($field) ?></div>
                                            <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                            <div class="adress-last__housing"><? __getFieldHtml($field) ?></div>
                                            <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                            <div class="adress-last__apartment"><? __getFieldHtml($field) ?></div>
                                            <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                            <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]]; ?>
                                            <div class="adress-last__index"><? __getFieldHtml($field) ?></div>
                                        </div>
                                    </div>
                                    <? $field = array_shift($arResult["ORDER_PROP"]["RELATED"][$delivery["ID"]]) ?>
                                    <? $field["VALUE"] = $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]] && $arResult["FIELDS_VALUES"]["DELIVERY_ID"] == 2 && !$arResult["FIELDS_VALUES"]["ADDRESS_COMPANY_OR_NO"] ? $arResult["FIELDS_VALUES"][$field["FIELD_NAME"]] : ""; ?>
                                    <h3>Адрес доставки</h3>
                                    <div id="PARAM_ADDRESS_INPUT" class="ordering-step__content"
                                         <? if ($arResult["FIELDS_VALUES"]["DELIVERY_ID"] == 3): ?>style="display: none"<? endif;
                                    ?>>
                                        <div class="check-holder">
                                            <input type="checkbox" name="ADDRESS_COMPANY_OR_NO"
                                                   id="ADDRESS_COMPANY_OR_NO"

                                                   style="-webkit-appearance: checkbox !important;"
                                                <? if (!$arResult["FIELDS_VALUES"]["ADDRESS_COMPANY_OR_NO"]): ?>
                                                    <? if (/*!$arResult["SHORT_ORDER"] && */
                                                    empty($organisation)): ?>disabled<? else: ?><? endif; ?>
                                                <? else: ?>
                                                    checked
                                                <? endif ?>
                                            />
                                            <label for="ADDRESS_COMPANY_OR_NO"><?= GetMessage("SYMMETRON_SOA_ORGANISATION_ADDRESS") ?></label>
                                        </div>

                                        <span class="error-message" id="ADDRESS_COMPANY_OR_NO_ERROR"></span>

                                        <div id="ADDRESS_FIELD_DIV"
                                             <? if ($arResult["FIELDS_VALUES"]["ADDRESS_COMPANY_OR_NO"]): ?>style="display: none" <? endif;
                                        ?>>

                                            <input type="text"
                                                   id="<?= $field["FIELD_ID"] ?>"
                                                   name="<?= $field["FIELD_NAME"] ?>"
                                                   placeholder="<?= $field["DESCRIPTION"] ?>"
                                                   value="<?= $field["VALUE"] ?>"
                                            >
                                        </div>
                                    </div>
                                <? endif; ?>
                            <? endforeach; ?>
                        </div>
                    </div>


                    <?
                    $keyYandex = COption::GetOptionString("mcart.basketsymmetron", "BASKETSM_KEY_YANDEX");
                    $keyDadata = COption::GetOptionString("mcart.basketsymmetron", "BASKETSM_KEY_DADATA");
                    ?>
                    <script>
                        var keyDadata = "<?=$keyDadata?>";
                        var shortOrder = false;
                        <?if($arResult["SHORT_ORDER"]):?>
                        shortOrder = true;
                        <?endif;?>
                    </script>

                    <script src="https://api-maps.yandex.ru/2.1/?apikey=<?= $keyYandex ?>&lang=ru_RU"
                            type="text/javascript">
                    </script>

                    <? /* Fourth block - delivery places */
                    ?>
                    <div class="place <? if ($arResult["FIELDS_VALUES"]["DELIVERY_ID"] == 2 || !$isSelf): ?>close<? endif; ?><? /*=$isSelf ? "" : "close"*/
                    ?>" <? if ($arResult["SHORT_ORDER"]): ?>style="display: none"<? endif;
                    ?>>

                        <div class="ordering-step__content">
                            <? $flagNotCh = false; ?>

                            <? $buyStoreVal = $arResult["FIELDS_VALUES"]["BUYER_STORE"]; ?>
                            <? if (!empty($buyStoreVal)) {
                                $flagNotCh = true;
                            } ?>
                            <script>
                                var allPositions = [];
                                <?foreach ($arResult["JS_DATA"]["STORE_LIST"] as $store):?>
                                allPositions.push("<?=$store["ID"];?>");
                                <?endforeach;?>
                            </script>

                            <? foreach ($arResult["JS_DATA"]["STORE_LIST"] as $store): ?>
                                <?
                                //echo "<pre>";
                                //print_r($store);
                                //echo "</pre>";
                                ?>
                                <div class="map-order">
                                    <div id="address_<?= $store["ID"] ?>" data-id="<?= $store["ID"] ?>"
                                         class="yamap-wrapper" style="width: 100%; height: 200px"></div>

                                    <div class="order-mapWrap">
                                        <input name="BUYER_STORE" class="map-radio" type="radio"
                                               value="<?= $store["ID"] ?>" id="radio_<?= $store["ID"] ?>"
                                            <? if (!$flagNotCh):
                                                $flagNotCh = true;
                                                echo " checked";
                                            endif; ?>

                                            <? if ($buyStoreVal == $store["ID"]): ?>
                                                checked
                                            <? endif; ?>
                                        >
                                        <div class="order-mapInfo">
                                            <div class="order-mapCity"><?= $store["TITLE"] ?></div>
                                            <div class="order-mapAdress"><?= $store["ADDRESS"] ?></div>
                                            <a href="tel:<?= $store["PHONE"] ?>"
                                               class="order-mapPhone"><?= $store["PHONE"] ?></a>
                                        </div>
                                    </div>
                                    <div style="display: none">
                                        <label><?= $store["TITLE"] ?>: <?= $store["ADDRESS"] ?></label>
                                    </div>

                                    <script>
                                        //BX.ready(function() {
                                        new MapObject("<?=$store["ID"];?>", "<?=$store["GPS_N"]?>", "<?=$store["GPS_S"]?>", allPositions);
                                        //})
                                    </script>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>

                    <? /* Fifth block - comments */
                    ?>
                    <? $block = array_shift($arResult["JS_DATA"]["ORDER_PROP"]["groups"]); ?>
                    <div class="comment">
                        <? foreach ($arResult["ORDER_PROP"]["USER_PROPS"] as $field): ?>
                            <? if ($field["PROPS_GROUP_ID"] != $block["ID"])
                                continue; ?>
                            <h3><?= $field["NAME"] ?><?= ($field["REQUIRED"] == "Y") ? " *" : "" ?></h3>
                            <textarea type="<?= $field["TYPE"] ?>"
                                      id="<?= $field["FIELD_ID"] ?>"
                                      name="<?= $field["FIELD_NAME"] ?>"
                                      placeholder="<?= $field["DESCRIPTION"] ?>"
                                      value="<?= $field["VALUE"] ?>"
                            ></textarea>
                        <? endforeach; ?>
                    </div>

                    <? /* Sixth block - total of items */
                    ?>
                    <div class="total">
                        <div class="cart-total">
                            <div class="total-cost__head"><?= Loc::GetMessage("SYMMETRON_SOA_TOTAL") ?>:</div>
                            <div class="total-cost__wrapper">
                                <div class="total-col">
                                    <div class="total-col__head"><?= Loc::GetMessage("SYMMETRON_SOA_SELL") ?>:</div>
                                    <div class="total-col__content">
                                        <span><?= $arResult["JS_DATA"]["TOTAL"]["SELL_ITEMS"] ?> <?= declineWordPosition($arResult["JS_DATA"]["TOTAL"]["SELL_ITEMS"], $arPsositionVariants, $arPsositionVariantsExaption) ?></span>
                                        <span><?= $arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE_FORMATED"] ?></span>
                                    </div>
                                </div>
                                <div class="total-col">
                                    <div class="total-col__head"><?= Loc::GetMessage("SYMMETRON_SOA_CHECK") ?>:</div>
                                    <div class="total-col__content">
                                        <span><?= $arResult["JS_DATA"]["TOTAL"]["CHECK_ITEMS"] ?> <?= declineWordPosition($arResult["JS_DATA"]["TOTAL"]["CHECK_ITEMS"], $arPsositionVariants, $arPsositionVariantsExaption) ?></span>
                                    </div>
                                </div>
                                <div class="total-col">
                                    <div class="total-col__head"><?= Loc::GetMessage("SYMMETRON_SOA_ASK") ?>:</div>
                                    <div class="total-col__content">
                                        <span><?= $arResult["JS_DATA"]["TOTAL"]["ASK_ITEMS"] ?> <?= declineWordPosition($arResult["JS_DATA"]["TOTAL"]["ASK_ITEMS"], $arPsositionVariants, $arPsositionVariantsExaption) ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="personal-data">
                        <div>
                            <input type="checkbox" name="PERSONAL_DATA" id="personal-data-input" required checked>
                            <label for="personal-data-input"><?= Loc::GetMessage("PERSONAL_DATA_LABEL") ?></label>
                        </div>
                        <span class="error-message" id="personal-data-input-error"
                              style="display: none"><?= Loc::GetMessage("PERSONAL_DATA_ERROR") ?></span>
                    </div>
                    <div class="submit">
                        <button type="button"><?= Loc::GetMessage("SYMMETRON_SOA_SUBMIT_BUTTON") ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>

<? }
