<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["PERSON_TYPE"] = array_shift($arResult["PERSON_TYPE"]);
$arResult["PAY_SYSTEM"] = array_shift($arResult["PAY_SYSTEM"]);


$arResult["FIELDS_VALUES"] = CUserOptions::GetOption("order", $GLOBALS["USER"]->GetID());
/*echo "<pre>";
print_r($arResult["FIELDS_VALUES"]);
echo "</pre>";*/
//$arResult["FIELDS_VALUES"] = [];
$arResult["FIELDS_VALUES"]["DELIVERY_ID"] = $arResult["FIELDS_VALUES"]["DELIVERY_ID"] ?: 2;

foreach ($arResult["JS_DATA"]["ORDER_PROP"]["groups"] as $key => $block)
    if ($block["PERSON_TYPE_ID"] != $arResult["PERSON_TYPE"]["ID"])
        unset($arResult["JS_DATA"]["ORDER_PROP"]["groups"][$key]);

$related = [];
foreach ($arResult["JS_DATA"]["DELIVERY"] as $key => $delivery) {

    unset($arResult["JS_DATA"]["DELIVERY"][$key]["CHECKED"]);

    $arResult["JS_DATA"]["DELIVERY"][$key]["CHECKED"] = $arResult["JS_DATA"]["DELIVERY"][$key]["ID"] == $arResult["FIELDS_VALUES"]["DELIVERY_ID"] ? "Y" : "N";

    foreach ($arResult["ORDER_PROP"]["RELATED"] as $key => $block)
        foreach ($block["RELATION"] as $relation)
            if ($relation["ENTITY_TYPE"] == "D" && $relation["ENTITY_ID"] == $delivery["ID"])
                $related[$delivery["ID"]][] = $block;
}
$arResult["ORDER_PROP"]["RELATED"] = $related;

$arResult["ORDER_PROP"]["USER_PROPS"] = [];

foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"] as $prop)
    $arResult["ORDER_PROP"]["USER_PROPS"][$prop["SORT"]] = $prop;
foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"] as $prop)
    $arResult["ORDER_PROP"]["USER_PROPS"][$prop["SORT"]] = $prop;
ksort($arResult["ORDER_PROP"]["USER_PROPS"]);
unset($arResult["ORDER_PROP"]["USER_PROPS_Y"]);
unset($arResult["ORDER_PROP"]["USER_PROPS_N"]);


/* Calculate basket total count */
$arResult["JS_DATA"]["TOTAL"]["SELL_ITEMS"] = 0;
$arResult["JS_DATA"]["TOTAL"]["CHECK_ITEMS"] = 0;
$arResult["JS_DATA"]["TOTAL"]["ASK_ITEMS"] = 0;

foreach ($arResult["BASKET_ITEMS"] as $item) {
    foreach ($item["PROPS"] as $prop)
        if ($prop["CODE"] == "EXAMPLE") {
            $arResult["JS_DATA"]["TOTAL"]["ASK_ITEMS"]++;
            $item = false;
            break;
        }
    if (!$item) continue;
    if ($item["SUM_BASE"] == 0)
        $arResult["JS_DATA"]["TOTAL"]["CHECK_ITEMS"]++;
    else
        $arResult["JS_DATA"]["TOTAL"]["SELL_ITEMS"]++;
}

if ($arResult["JS_DATA"]["TOTAL"]["SELL_ITEMS"] == 0
    && $arResult["JS_DATA"]["TOTAL"]["CHECK_ITEMS"] > 0
    && $arResult["JS_DATA"]["TOTAL"]["ASK_ITEMS"] == 0) {

    $arResult["SHORT_ORDER"] = true;

}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (strlen($request->get('ORDER_ID')) > 0) {
    $res = CIBlockProperty::GetByID("PRODUCERID");
    $PRODUCERID_IBLOCK_ID = false;
    if ($ar_res = $res->GetNext()) {
        $PRODUCERID_IBLOCK_ID = $ar_res["LINK_IBLOCK_ID"];
    }

    $arResult['YA_RESULT'] = [
        'actionField' => [

        ],
        'products' => [

        ],
    ];

    $dbRes = \Bitrix\Sale\Order::getList([
        'select' => ['*'],
        'filter' => [
            'ID' => IntVal($request->get('ORDER_ID')),
        ]
    ]);
    while ($item = $dbRes->fetch()) {
        $arResult['YA_RESULT']['actionField']['id'] = $item['ID'];
        $arResult['YA_RESULT']['actionField']['revenue'] = $item['PRICE'];
        $arResult['YA_RESULT']['actionField']['tax'] = $item['TAX_VALUE'];
        $arResult['YA_RESULT']['actionField']['shipping'] = $item['PRICE_DELIVERY'];
    }


    $dbRes = \Bitrix\Sale\Basket::getList([
        'select' => ['*'],
        'filter' => [
            '=ORDER_ID' => IntVal($request->get('ORDER_ID')),
        ]
    ]);
    while ($item = $dbRes->fetch()) {
        $arResult['YA_RESULT']['products'][] = [
            'name' => $item['NAME'],
            'id' => $item['PRODUCT_ID'],
            'price' => $item['PRICE'],

            'quantity' => $item['QUANTITY'],
        ];
    }

    $products = [];
    $res = \CIBlockElement::GetList(Array("SORT" => "ASC", "PROPERTY_PRIORITY" => "ASC"), ["ID" => array_column($arResult['YA_RESULT']['products'], 'id')], ["ID", "IBLOCK_SECTION_ID", "PROPERTY_PRODUCERID"]);
    while ($ar_fields = $res->Fetch()) {
        $products[$ar_fields["ID"]] = $ar_fields;
    }

    $PRODUCERID_IDs = array_column($products, 'PROPERTY_PRODUCERID_VALUE');
    $PRODUCERID_NAMES = [];
    if ($PRODUCERID_IBLOCK_ID !== false && !empty($PRODUCERID_IDs)) {
        $res = CIBlockElement::GetList(Array(), array("ID" => $PRODUCERID_IDs, "IBLOCK_ID" => $PRODUCERID_IBLOCK_ID), false, false, array("ID", "NAME"));
        while ($ob = $res->Fetch()) {
            $PRODUCERID_NAMES[$ob["ID"]] = $ob["NAME"];
        }
    }

    foreach ($products as $value) {
        $arFilterSection = ["ID" => $value["IBLOCK_SECTION_ID"], "ACTIVE" => "Y"];
        $products[$value["ID"]]["BREAD"] = [];
        $rsSectionInfo = \CIBlockSection::GetList([], $arFilterSection, false, ["ID", "NAME", "LEFT_MARGIN", "IBLOCK_ID", "RIGHT_MARGIN"]);
        if ($arSectionInfo = $rsSectionInfo->Fetch()) {
            $rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), array(
                'IBLOCK_ID' => $arSectionInfo["IBLOCK_ID"],
                "<=LEFT_BORDER" => $arSectionInfo["LEFT_MARGIN"],
                ">=RIGHT_BORDER" => $arSectionInfo["RIGHT_MARGIN"],
            ));

            while ($arSect = $rsSect->Fetch()) {
                $products[$value["ID"]]["BREAD"][] = $arSect['NAME'];
            }
        }
        $products[$value["ID"]]["BREAD"] = join("/", $products[$value["ID"]]["BREAD"]);

        $products[$value["ID"]]["BRAND"] = $PRODUCERID_NAMES[$value["PROPERTY_PRODUCERID_VALUE"]];
    }

    foreach ($arResult['YA_RESULT']['products'] as &$product) {
        $product['brand'] = $products[$product["id"]]["BRAND"];
        $product['category'] = $products[$product["id"]]["BREAD"];
    }
}

/*$arFilter = Array(
    "USER_ID" => $USER->GetID(),
);

$rsSales = CSaleOrder::GetList(["ID" => "DESC"], $arFilter, false, ['nPageSize' => 1]);
if ($arSales = $rsSales->Fetch())
{

    $rsProps = CSaleOrderPropsValue::GetOrderProps($arSales["ID"]);
    while ($arProps = $rsProps->Fetch())
    {
        $arResult["FIELDS_VALUES"][$arProps["CODE"]] = $arProps["VALUE"];
    }
}

echo "<pre>";
print_r($arResult["FIELDS_VALUES"]);
echo "</pre>";*/
