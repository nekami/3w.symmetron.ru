<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="wrapper">
    <div class="footer-head">
        <div class="logo-wrapper"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="logo"></a></div>
        <div class="logo-text-wrapper">Комплексные поставки электронных компонентов</div>
    </div>
    <div class="footer-content">
        <div class="row">
            <div class="col-3">
                <div class="city">Москва</div>
                <div class="address">Ленинградское шоссе, дом 69, корпус 1 Бизнес-парк River City</div>
                <div class="metro">м. Речной вокзал</div>
                <div class="phone"><a href="tel:+74959612020">+7 (495) 961-2020</a></div>
                <div class="mail"><a href="mailto:moscow@symmetron.ru">moscow@symmetron.ru</a></div>
            </div>
            <div class="col-3">
                <div class="city">Санкт-Петербург</div>
                <div class="address">ул. Таллинская, 7</div>
                <div class="metro">м. Новочеркасская</div>
                <div class="phone"><a href="tel:+78124494000">+7 (812), 449-4000</a>, <a href="tel:+78124494005">449-4005</a>, <a href="tel:+78124494006">449-4006</a></div>
                <div class="mail"><a href="mailto:spb@symmetron.ru">spb@symmetron.ru</a></div>
            </div>
            <div class="col-3">
                <div class="city">Новосибирск</div>
                <div class="address">ул. Блюхера,  71б</div>
                <div class="phone"><a href="tel:+73833613424">+7 (383) 361-3424</a></div>
                <div class="mail"><a href="mailto:sibir@symmetron.ru">sibir@symmetron.ru</a></div>
            </div>
            <div class="col-3">
                <div class="city">Минск</div>
                <div class="address">ул. Веры Хоружей, 1А, офис 507</div>
                <div class="phone"><a href="tel:+375173360606 ">+375 (17) 336-0606</a></div>
                <div class="mail"><a href="mailto:minsk@symmetron.ru">minsk@symmetron.ru</a></div>
            </div>
        </div>
    </div>
</div>