<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="city">
	 Москва
</div>
<div class="address">
	 Ленинградское шоссе, дом 69, корпус 1 Бизнес-парк River City
</div>
<div class="metro">
	 м.&nbsp;Беломорская
</div>
<div class="phone">
 <a href="tel:+74959612020">+7 (495) 961-2020</a>
</div>
<div class="mail">
 <a href="mailto:moscow@symmetron.ru">moscow@symmetron.ru</a>
</div>
 <br>