<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<div class="city">Санкт-Петербург</div>
<div class="address">ул. Таллинская, 7</div>
<div class="metro">м. Новочеркасская</div>
<div class="phone"><a href="tel:+78124494000">+7 (812) 449-4000</a><br><a href="tel:+78124494005">+7 (812) 449-4005</a><br>
<a href="tel:+78124494006">+7 (812) 449-4006</a></div>
<div class="mail"><a href="mailto:spb@symmetron.ru">spb@symmetron.ru</a></div>