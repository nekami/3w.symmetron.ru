<div id="modal-result-off" class="modal">
    <div class="forgot-pass main-modal">
        <h2>Уведомление</h2>
        <p>Что-то произошло...<br>Вот только непонятно что...</p>
        <a class="modal-result" href="/"></a>
    </div>
</div>

<script>
    BX.ready(function () {
        if (BX.Mcart === undefined) BX.Mcart = {};
        if (BX.Mcart.modals === undefined) BX.Mcart.modals = {};
        BX.Mcart.modals.result_off = {
            open: function (params) {
                var modal = $("#modal-result-off");
                if (params[0] !== undefined && params[1] !== undefined) {
                    modal.find("div.main-modal > h2").html(params[0]);
                    modal.find("div.main-modal > p").html(params[1]);
                    if (params[2] !== undefined && params[2].url !== undefined && params[2].text) {
                        modal.find("div.main-modal > a").attr("href", params[2].url);
                        modal.find("div.main-modal > a").html(params[2].text);
                    }
                    modal.modal();
                }
            }
        };
    });
</script>
