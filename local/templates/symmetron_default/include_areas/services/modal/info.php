<div id="modal-info" class="modal">
    <!--<p><span>Внимание!</span><br>Сайт находится в процессе разработки...</p>
    <img src="/images/beta.jpg" alt="" width="100%">-->
    <p class="modal-text">Файлы cookie имеют важное значение для функционирования сайта. Мы просим вас разрешить нам использовать файлы cookies
    для хранения и защиты параметров вашей учетной записи, сбора статических данных, необходимых для оптимизации функционала
    сайта, и для предоставления вам контента, соответствующего вашим интересам</p>
    <a rel="modal:close" class="modal-button" type="button">Разрешить и продолжить</a>
</div>

<style>
    #modal-info.modal {
            z-index: 100;
	    position: fixed;
    }
    #modal-info p {
        text-align: left;
        font-size: 17px;
        margin-top: 40px;
        padding-left: 10px;
        padding-right: 10px;
    }
    #modal-info p span{
        font-size: 40px;
        color: red;
    }

    #modal-info .modal-button {
		border: 1px solid #ededed;
		margin: 0 auto;
		display: table;
		padding: 8px 35px;
		text-align: center;
		cursor: pointer;
    }

    #modal-info .close-modal {
        opacity: 0;
    }

    @media (min-width: 998px) {
        #modal-info.modal {
            width: 700px;
        }
    }
</style>
;
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
            require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
if (($USER->IsAuthorized() && CUserOptions::GetOption("mcart_info", "see", "N") !== "Y") || (!$USER->IsAuthorized() && empty($_SESSION["BETA_TEST_INFORMATION_SEE"]))):?>
    <script>
        BX.ready(function () {
            $('#modal-info').modal({
				escapeClose: false,
				clickClose: false,
				showClose: false,
	    	});
   
            let modalInfo = document.querySelector('#modal-info');
            let parentModalInfo = modalInfo.parentNode;
            parentModalInfo.style.backgroundColor = 'transparent';
	    parentModalInfo.style.position = 'unset';
			BX.bind($('#modal-info .modal-button')[0], 'click', function() {
				BX.ajax({
					url: "/local/templates/symmetron_default/include_areas/services/modal/info_ajax.php",
					method: "POST",
					onsuccess: function(data){
						console.log(data);
					},
					onfailure: function(data){
						console.log(data);
					}
				});
			});


        });
    </script>
<?endif;?>