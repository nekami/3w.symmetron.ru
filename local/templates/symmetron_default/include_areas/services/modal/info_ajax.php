<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
            require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;
if ($USER->IsAuthorized()){
	CUserOptions::SetOption("mcart_info", "see", "Y");
}else{
	$_SESSION["BETA_TEST_INFORMATION_SEE"] = true;
}