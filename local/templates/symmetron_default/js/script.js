$(document).ready(function () {
    $('.burger-menu').on('click', function () {
        if ($('.burger-nav').hasClass('open')) {
            $('.burger-nav').removeClass('open')
            $('body').removeClass('ov-hidden')
        } else {
            $('body').addClass('ov-hidden')
            $('.burger-nav').addClass('open')
        }
    })

    $('.close-nav ').on('click', function () {
        if ($('.burger-nav').hasClass('open')) {
            $('.burger-nav').removeClass('open')
            $('body').removeClass('ov-hidden')
        }
    })

    if($('.slider-wrap').length > 0) {
        $('.slider-wrap').bxSlider({
            controls: false,
            pager: ($(".slider-wrap li").length > 1) ? true : false,
            auto: ($(".slider-wrap li").length > 1) ? true : false,
            touchEnabled: false /*($(".slider-wrap li").length > 1) ? true : false*/,
            autoHover: true
        });
    }


    $('.inputfile').change(function () {
        if ($(this).val()) {
            var filename = $(this).val().replace(/.*(\/|\\)/, '');
            $(this).closest('.file').find('.chose-file').html(filename);
        }
    });

    $('.subscribe-trigger').on('click', function () {
        if ($('.subscribe-wrapper').hasClass('open')) {
            return false
        } else {
            $('.subscribe-wrapper').addClass('open')
        }
    })

    $(document).on('click touchstart', function (e) {
        var dropdowns = $('.search-select, .project-select, .subscribe-wrapper, .add-filter, .add-filter__item, .categories-select, .table-filter__selector,' +
            /* For catalog filter and dropdowns */ ' .filter-item__add, .filter-item__addit, .filter-item__range')

        if (!dropdowns.is(e.target) && dropdowns.has(e.target).length === 0 && dropdowns.hasClass('open')) {
            dropdowns.removeClass('open')
        }

    })


    if (('#typePhone').length !== 0) {
        //$('#typePhone').mask('+7 (000) 000-0000');
    }

    if ($('.project-start input').length !== 0) {
        $('.project-start input').mask('00/00/0000', {placeholder: "__/__/____"});
    }


    function topFunction(e) {
        var $target = $('html,body');
        $target.animate({scrollTop: $target.height()}, 500);
    }


	$('.brand-description').on('click', function () {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open')
        } else {
            $(this).addClass('open')
        }
    })

    if($('.focus-slider').length > 0) {
        $('.focus-slider').bxSlider({
            auto: true,
            adaptiveHeight: true,
            controls: false,
            autoHover: true,
            touchEnabled: false
        })
        
    }



	$('main').on('click', '.bx-pagination-container > ul > li', function() {
		if(this.classList.contains('bx-pag-form')){
			return;
		}

		$('html, body').animate({
			scrollTop: $(".catalog_filter").offset().top
		}, 500);
	})

	
	if($('.photo-slider').length > 0) {
       /* $('.photo-slider').bxSlider({
            touchEnabled: false /*($(".slider-wrap li").length > 1) ? true : false,
            autoHover: true,
			pager: false,
			adaptiveHeight: true,

        }); */

		$('.photo-slider').owlCarousel({
			margin:10,
			loop:true,
			autoWidth:true,
			items:2,
			dots: false,
			nav: true,
			navText: ["<img src='/local/templates/symmetron_default/images/owl-left.png'>","<img src='/local/templates/symmetron_default/images/owl-right.png'>"],
			/*responsive:{
				0:{
					items: 1,
					autoWidth: false,
				},
				768:{
					items: 2,
					autoWidth: true,
				},
    		}*/
		})

    }

    if($('.image-link').length > 0) {
        $('.image-link').magnificPopup({
            type:'image',
            gallery: {
                enabled: true
            },
        });
    }


})