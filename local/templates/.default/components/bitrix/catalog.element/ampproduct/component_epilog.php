<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */
//echo "<pre> params";
//print_r($arResult);
//echo '</pre>';
global $APPLICATION;

global $APPLICATION;
__IncludeLang($_SERVER["DOCUMENT_ROOT"].$templateFolder."/lang/".LANGUAGE_ID."/template.php");


?>
     
    </amp-accordion>
    </div>
    </div>
    </div>
<?php

if(isset($arResult['CANONICAL_URL'])){

    $canonical_url = $arResult['CANONICAL_URL'];

    if(isset($canonical_url) && !empty($canonical_url)){

        $canonical_url = (preg_match('~http(s*?)://~',$canonical_url) == 0 ? ((CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$canonical_url) : $canonical_url);
        $canonical_url = preg_replace('~\:\/\/(www\.)*m\.~','://',$canonical_url);

        $SERVER_PAGE_URL = (CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $SERVER_PAGE_URL = preg_replace('~\?.*?$~is'.IMPEL_PREG_MODIFIER,'',$SERVER_PAGE_URL);
        $DETAIL_PAGE_URL = preg_replace('~\?.*?$~is'.IMPEL_PREG_MODIFIER,'',$canonical_url);

        if($DETAIL_PAGE_URL != $SERVER_PAGE_URL){

            if(get_class($this->__template)!=="CBitrixComponentTemplate")
                $this->InitComponentTemplate();

            $this->__template->SetViewTarget("CANONICAL_PROPERTY");
            ?>
            <link href="<?=$canonical_url;?>" rel="canonical" />
            <?
            $this->__template->EndViewTarget();

        };

    };
};

if(file_exists(__DIR__.'/amp_style.css')){

    $amp_style = file_get_contents(__DIR__.'/amp_style.css');
    if(get_class($this->__template)!=="CBitrixComponentTemplate")
        $this->InitComponentTemplate();

    $this->__template->SetViewTarget("AMP_STYLE");
    echo $amp_style;
    $this->__template->EndViewTarget();

}

global $arrTFilter;

if(isset($arResult['LINKED_ELEMETS'])
    && !empty($arResult['LINKED_ELEMETS'])){

    $arrTFilter = array(
        "ID" => $arResult['LINKED_ELEMETS']
    );

} else {

    $arrTFilter = array(
        "!PROPERTY_VIEW_POPULAR" => false,
        "!ID" => $arResult["ID"]
    );

};



$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "ampsimilar",
    Array(
        "ACTION_VARIABLE" => "action",
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "BACKGROUND_IMAGE" => "-",
        "BASKET_URL" => $arParams["BASKET_URL"],
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COMPATIBLE_MODE" => "Y",
        "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
        "CURRENCY_ID" => $arParams["CURRENCY_ID"],
        "CUSTOM_FILTER" => "",
        "DETAIL_URL" => isset($arParams["SEF_URL_TEMPLATES"]) ? $arParams["SEF_URL_TEMPLATES"]["element"] : $arParams["DETAIL_URL"] ,
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_COMPARE" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_FIELD2" => "sort",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_ORDER2" => "desc",
        "ENLARGE_PRODUCT" => "STRICT",
        "FILTER_NAME" => "arrTFilter",
        "HIDE_NOT_AVAILABLE" => "N",
        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
        "IBLOCK_TYPE" => $arResult["IBLOCK_TYPE"],
        "INCLUDE_SUBSECTIONS" => "Y",
        "LABEL_PROP" => array(),
        "LAZY_LOAD" => "N",
        "LINE_ELEMENT_COUNT" => "3",
        "LOAD_ON_SCROLL" => "N",
        "MESSAGE_404" => "",
        "MESS_BTN_ADD_TO_BASKET" => GetMessage("CT_BCE_CATALOG_ADD"),
        "MESS_BTN_BUY" => GetMessage("CT_BCE_CATALOG_ADD"),
        "MESS_BTN_DETAIL" => GetMessage("CT_BCE_CATALOG_READ_MORE"),
        "MESS_BTN_SUBSCRIBE" => GetMessage("CT_BCE_CATALOG_SUBSCRIBE"),
        "MESS_NOT_AVAILABLE" => GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'),
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "OFFERS_LIMIT" => "8",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "������",
        "PAGE_ELEMENT_COUNT" => "18",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array(),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
        "PRODUCT_SUBSCRIPTION" => "N",
        "PROPERTY_CODE" => array("COM_BLACK", "NEWPRODUCT", "SALEPRODUCT", ""),
        "PROPERTY_CODE_MOBILE" => array(),
        "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
        "RCM_TYPE" => "personal",
        "SECTION_CODE" => "",
        "SECTION_CODE_PATH" => "",
        "SECTION_ID" => $arResult['IBLOCK_SECTION_ID'],
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        "SECTION_URL" => isset($arParams["SEF_URL_TEMPLATES"]) ? $arParams["SEF_URL_TEMPLATES"]["section"] : $arParams["SECTION_URL"],
        "SECTION_USER_FIELDS" => array("",""),
        "SEF_MODE" => "Y",
        "SEF_RULE" => isset($arParams["SEF_URL_TEMPLATES"]) ?$arParams["SEF_URL_TEMPLATES"]["element"] : $arParams["DETAIL_URL"],
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SHOW_ALL_WO_SECTION" => "Y",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_FROM_SECTION" => "N",
        "SHOW_MAX_QUANTITY" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "SHOW_SLIDER" => "Y",
        "SLIDER_INTERVAL" => "3000",
        "SLIDER_PROGRESS" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "N",
        "USE_MAIN_ELEMENT_SECTION" => "N",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "N",
        "BLOCK_TITLE" => GetMessage("TMPL_SIMILAR_PRODUCTS")
    )
);?>
    </div>
<?


