<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */


$component = $this->getComponent();

if(method_exists($component,'applyTemplateModifications'))
    $arParams = $component->applyTemplateModifications();

$haveOffers = !empty($arResult['OFFERS']);


$arSFilter = Array(
    'IBLOCK_ID' => $arResult['IBLOCK_ID']);

if(isset($arResult['IBLOCK_SECTION_ID'])
    && !empty($arResult['IBLOCK_SECTION_ID'])){

    $arSFilter['ID'] = $arResult['IBLOCK_SECTION_ID'];

} if(isset($arParams['SECTION_CODE'])
    && !empty($arParams['SECTION_CODE'])){

    $arSFilter['CODE'] = $arParams['SECTION_CODE'];

} else if(isset($arParams['SECTION_ID'])
    && !empty($arParams['SECTION_ID'])){

    $arSFilter['ID'] = $arParams['SECTION_ID'];

}

$rsSection = CIBlockSection::GetList(Array(), $arSFilter, false, array('SECTION_PAGE_URL','NAME'));
$rsSection->SetUrlTemplates("", $arParams['SECTION_URL']);

$arResult['SECTION_PAGE_URL'] = '';

if($rsSection){

    $arSection = $rsSection->GetNext();
    $arResult['SECTION_PAGE_URL'] = $arSection['SECTION_PAGE_URL'];
    $arResult['SECTION_NAME'] = $arSection['NAME'];

}

$arEmptyPreview = false;
$strEmptyPreview = $this->GetFolder().'/images/no_photo.png';
if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview))
{
    $arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);

    if (!empty($arSizes))
    {
        $arEmptyPreview = array(
            'src' => $strEmptyPreview,
            'width' => (int)$arSizes[0],
            'height' => (int)$arSizes[1],
            'alt' => $arResult['NAME']
        );

    }
    unset($arSizes);
}
unset($strEmptyPreview);

$arResult['DEFAULT_PICTURE'] = $arEmptyPreview;

if(isset($arResult['PROPERTIES'])
    &&isset($arResult['PROPERTIES']['LINKED_ELEMETS'])
    &&isset($arResult['PROPERTIES']['LINKED_ELEMETS']['VALUE'])
    &&!empty($arResult['PROPERTIES']['LINKED_ELEMETS']['VALUE'])){

    $arResult['LINKED_ELEMETS'] = $arResult['PROPERTIES']['LINKED_ELEMETS']['VALUE'];
    unset($arResult['PROPERTIES']['LINKED_ELEMETS']);
}

$obCache = new CPHPCache;
$cacheID = 'tabsdetail';
$tabs = array();

if($obCache->InitCache($cacheTime, $cacheID, "/impel/")){

    $tmp = array();
    $tmp = $obCache->GetVars();

    if(isset($tmp[$cacheID])){
        $tabs = $tmp[$cacheID];
    }

} else {

    $arFilter = Array(
        "SECTION_CODE" => "amptabs",
        "IBLOCK_CODE" => "amp_template_options",
        "ACTIVE" => "Y"
    );

    $tabsResDB = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID","NAME","PREVIEW_TEXT","PREVIEW_PICTURE"));

    if($tabsResDB){

        while($tabsResArr = $tabsResDB->Fetch()){

            if(
                isset($tabsResArr['ID'])
                && isset($tabsResArr['NAME'])
                && isset($tabsResArr['PREVIEW_TEXT'])
                && !empty($tabsResArr['PREVIEW_TEXT'])
            ){

                $tabs['tab_headers'][$tabsResArr['ID']]	= $tabsResArr['NAME'];

                $amp_content_obj = new AMP_Content( $tabsResArr['PREVIEW_TEXT'],
                    array(
                        //'AMP_YouTube_Embed_Handler' => array(),
                    ),
                    array(
                        'AMP_Style_Sanitizer' => array(),
                        'AMP_Blacklist_Sanitizer' => array(),
                        'AMP_Img_Sanitizer' => array(),
                        'AMP_Video_Sanitizer' => array(),
                        'AMP_Audio_Sanitizer' => array(),
                        'AMP_Iframe_Sanitizer' => array(
                            'add_placeholder' => true,
                        ),
                    ),
                    array(
                        'content_max_width' => 600,
                    )
                );

                $tabsResArr['PREVIEW_TEXT'] = $amp_content_obj->get_amp_content();
                $tabs['tab_panels'][$tabsResArr['ID']] = $tabsResArr['PREVIEW_TEXT'];


                if(	isset($tabsResArr['PREVIEW_PICTURE'])
                    && !empty($tabsResArr['PREVIEW_PICTURE'])
                ){

                    $image_path	= CFile::GetPath($tabsResArr['PREVIEW_PICTURE']);
                    if($image_path){
                        $tabs['tab_images'][$tabsResArr['ID']] = $image_path;
                    };

                };
            };
        };

    };

    if($obCache->StartDataCache()){

        $obCache->EndDataCache(
            array(
                $cacheID => $tabs
            )
        );

    };

};


$arResult['TABS'] = $tabs;

$gallery = array();

if(isset($arResult["DISPLAY_PROPERTIES"])
    && !empty($arResult["DISPLAY_PROPERTIES"])){

    foreach ($arResult['DISPLAY_PROPERTIES'] as $pid => $arOneProp){

        $arResult['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] = (is_array($arOneProp['DISPLAY_VALUE'])
            ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
            : $arOneProp['DISPLAY_VALUE']);


        $amp_content_obj = new AMP_Content($arResult['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'],
            array(
                //'AMP_YouTube_Embed_Handler' => array(),
            ),
            array(
                'AMP_Style_Sanitizer' => array(),
                'AMP_Blacklist_Sanitizer' => array(),
                'AMP_Img_Sanitizer' => array(),
                'AMP_Video_Sanitizer' => array(),
                'AMP_Audio_Sanitizer' => array(),
                'AMP_Iframe_Sanitizer' => array(
                    'add_placeholder' => true,
                ),
            ),
            array(
                'content_max_width' => 320,
            )
        );

        $arResult['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] = $amp_content_obj->get_amp_content();

    }

}

if(!$haveOffers) {

    $add_pict_prop = isset($arParams["ADD_PICT_PROP"])
    && !empty($arParams["ADD_PICT_PROP"])
        ? $arParams["ADD_PICT_PROP"]
        : "MORE_PHOTO";

    if (isset($arResult["PROPERTIES"])
        && isset($arResult["PROPERTIES"][$add_pict_prop])
        && is_array($arResult["PROPERTIES"][$add_pict_prop])
        && sizeof($arResult["PROPERTIES"][$add_pict_prop])
        && isset($arResult["PROPERTIES"][$add_pict_prop]["VALUE"])
        && !empty($arResult["PROPERTIES"][$add_pict_prop]["VALUE"])
    ) {

        foreach ($arResult["PROPERTIES"][$add_pict_prop]["VALUE"] as $number => $file_id) {

            if (is_numeric($file_id)) {
                $gallery[$number]['src'] = CFile::GetPath($file_id);
            } else {
                $gallery[$number]['src'] = $file_id;
            }

            if (!empty($gallery[$number]['src'])) {

                $gallery[$number]['width'] = 640;
                $gallery[$number]['height'] = 480;


                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src'])
                    && filesize($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src']) > 0
                    && is_readable($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src'])) {

                    $sizes = getimagesize($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src']);

                    if ($sizes
                        && is_array($sizes)
                        && isset($sizes[0])
                        && isset($sizes[1])
                        && !empty($sizes[0])
                        && !empty($sizes[1])
                    ) {

                        $gallery[$number]['width'] = $sizes[0];
                        $gallery[$number]['height'] = $sizes[1];

                    }


                }

                $srcSetHTML = '';
                $gallery[$number]['srcset'] = '';
                $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
                $gallery[$number]['srcset'] = $srcSetHTML;
                $gallery[$number]['alt'] = $arResult['NAME'];

            }

        }

    };

} else {

    $offerPropCode = '';
    $applicationJson = '';

    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

    $offersCounter = array();
    $offersGroupCounter = array();

    $keysToOffers = array();

    $counter = 0;
    $selectedOfferKey = '';

    foreach($arResult['OFFERS'] as $valueOffer) {

        $subProps = array();

        if(isset($valueOffer['TREE'])
            && !empty($valueOffer['TREE'])){

            $itemPrice = current($valueOffer["ITEM_PRICES"]);
            $offerPrice = ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']);
            $applicationJsonArr["price".$valueOffer['ID']] = trim(strip_tags($offerPrice));

            if(LANG_CHARSET != 'UTF-8'
                && function_exists('iconv')) {
                $applicationJsonArr["price".$valueOffer['ID']] = iconv(LANG_CHARSET,'UTF-8//IGNORE',$applicationJsonArr["price".$valueOffer['ID']]);
            }

            $applicationJsonArr["link".$valueOffer['ID']] = $APPLICATION->GetCurPage().'?action=add2basketamp&id='.$valueOffer['ID'].'';

            if(empty($selectedOfferKey))
                $selectedOfferKey = $valueOffer['ID'];

            foreach($valueOffer['TREE'] as $propName => $skuId){

                if($skuId == 0)
                    continue;

                $propId = (int)substr($propName, 5);

                $offersGroupCounter[] = $propId.'_'.$skuId;
                $offersCounter[] = $propId.'_'.$skuId;

                break;

            }

        }

    }

    $strKeyOfferArr = array();
    $strIdToGroup = array();
    $strIdToOffer = array();



    foreach ($arResult['SKU_PROPS'] as $skuProperty) {

        foreach ($skuProperty['VALUES'] as $vNumber => $value) {

            foreach ($arResult['OFFERS'] as $offer) {

                if (!empty($offer['TREE']) && is_array($offer['TREE'])) {

                    $keyProp = false;

                    foreach ($offer['TREE'] as $propName => $skuId) {

                        if($skuId == 0){
                            continue;
                        }

                        $propId = (int)substr($propName, 5);

                        if(!$keyProp){

                            $keyProp = $propId.'_'.$skuId;
                            if(!in_array($keyProp,$strIdToGroup[$keyProp])) {
                                $strIdToGroup[$keyProp][] = $keyProp;
                                $strIdToOffer[$keyProp][] = $keyProp.'_'.$offer['ID'];
                            }

                        } else {

                            if ($skuProperty['ID'] == $propId) {

                                if ($value['ID'] == $skuId) {

                                    if(!in_array($propId.'_'.$skuId,$strIdToGroup[$keyProp])){
                                        $strIdToGroup[$keyProp][] = $propId.'_'.$skuId;
                                        $strIdToOffer[$keyProp][] = $propId.'_'.$skuId.'_'.$offer['ID'];
                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

    }

    if(!empty($applicationJsonArr)) {

        $applicationJsonArr['selectedGroupOffer'] = join('_',$strIdToOffer[(isset($arResult['OFFERS_SELECTED']) ? $offersCounter[$arResult['OFFERS_SELECTED']] : $offersCounter[0])]);
        $applicationJsonArr['selectedOffer'] = join('_',$strIdToGroup[(isset($arResult['OFFERS_SELECTED']) ? $offersGroupCounter[$arResult['OFFERS_SELECTED']] : $offersGroupCounter[0])]);
        $applicationJsonArr['selectedOfferKey'] = $selectedOfferKey;

        $applicationJson = json_encode($applicationJsonArr);
    }


    $arResult['applicationJsonArr'] = $applicationJsonArr;
    $arResult['applicationJson'] = $applicationJson;
    $arResult['keysToOffers'] = $keysToOffers;
    $arResult['strIdToGroup'] = $strIdToGroup;
    $arResult['strIdToOffer'] = $strIdToOffer;

    foreach ($arResult['OFFERS'] as $offer)
    {
        if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0){

            if(isset($offer['PREVIEW_PICTURE'])
                && isset($offer['PREVIEW_PICTURE']['SRC'])
                && !empty($offer['PREVIEW_PICTURE']['SRC'])){

                $offer['MORE_PHOTO'][] = $offer['PREVIEW_PICTURE'];

            } else if(isset($offer['DETAIL_PICTURE'])
                && isset($offer['DETAIL_PICTURE']['SRC'])
                && !empty($offer['DETAIL_PICTURE']['SRC'])){

                $offer['MORE_PHOTO'][] = $offer['DETAIL_PICTURE'];

            }


        }

        $srcs = array();
        $arResult['OFFER_PROPS'][$offer['ID']] = $offer['DISPLAY_PROPERTIES'];

        foreach ($offer['MORE_PHOTO'] as $number => $photo)
        {

            if (!empty($photo['SRC'])) {

                $photo['width'] = 640;
                $photo['height'] = 480;

                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $photo['SRC'])
                    && filesize($_SERVER['DOCUMENT_ROOT'] . $photo['SRC']) > 0
                    && is_readable($_SERVER['DOCUMENT_ROOT'] . $photo['SRC'])) {

                    $sizes = getimagesize($_SERVER['DOCUMENT_ROOT'] . $photo['SRC']);

                    if ($sizes
                        && is_array($sizes)
                        && isset($sizes[0])
                        && isset($sizes[1])
                        && !empty($sizes[0])
                        && !empty($sizes[1])
                    ) {

                        $photo['width'] = $sizes[0];
                        $photo['height'] = $sizes[1];

                    }


                }

                $propKeysArr = key($offer['TREE']);
                $propValsArr = current($offer['TREE']);

                $sTreeKey = substr($propKeysArr,5).'_'.$propValsArr;

                if(isset($strIdToGroup[$sTreeKey])){

                    $subProps = $strIdToGroup[$sTreeKey];
                    $keyOffer = join('_',$subProps);

                }

                $srcs[$keyOffer] = !isset($srcs[$keyOffer]) ? array() : $srcs[$keyOffer];
                $srcs[$keyOffer][] = $photo['SRC'];

                $srcSetHTML = '';
                $photo['srcset'] = '';
                $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($photo['SRC']);

                $gallery[$keyOffer][$number]['srcset'] = $srcSetHTML;
                $gallery[$keyOffer][$number]['src'] = $photo['SRC'];

                $gallery[$keyOffer][$number]['width'] = $photo['width'];
                $gallery[$keyOffer][$number]['height'] = $photo['height'];
                $gallery[$keyOffer][$number]['alt'] = $photo['NAME'];

            }

        }

    }

}

if(isset($arResult["DETAIL_PICTURE"])
    && isset($arResult["DETAIL_PICTURE"]["SRC"])
    && !empty($arResult["DETAIL_PICTURE"]["SRC"])
    && empty($gallery)){

    $number = sizeof($gallery);
    $gallery[$number]['src'] = $arResult["DETAIL_PICTURE"]["SRC"];
    $gallery[$number]['width'] = $arResult["DETAIL_PICTURE"]["WIDTH"];
    $gallery[$number]['height'] = $arResult["DETAIL_PICTURE"]["HEIGHT"];

    $srcSetHTML = '';
    $gallery[$number]['srcset'] = '';
    $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
    $gallery[$number]['srcset'] = $srcSetHTML;

};

if(empty($gallery)){
    $number = sizeof($gallery);
    $gallery[$number] = $arEmptyPreview;
    $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($arEmptyPreview['src']);
    $gallery[$number]['srcset'] = $srcSetHTML;
}

$arResult['GALLERY'] = $gallery;

$canonical_url = '';
$canonicalResDB = CIBlockElement::GetList(Array("SORT" => "ASC"),Array('ID' => $arResult['ID'], 'IBLOCK_ID' => $arResult['IBLOCK_ID']), false, false, array('DETAIL_PAGE_URL'));

if($canonicalResDB
    && $canonicalResArr = $canonicalResDB->getNext()){

    if(isset($canonicalResArr['DETAIL_PAGE_URL']) && !empty($canonicalResArr['DETAIL_PAGE_URL'])){
        $canonical_url = $canonicalResArr['DETAIL_PAGE_URL'];
    };

};

if(!empty($arResult["PREVIEW_TEXT"])){
    $amp_content_obj = new AMP_Content( $arResult["PREVIEW_TEXT"],
        array(
            //'AMP_YouTube_Embed_Handler' => array(),
        ),
        array(
            'AMP_Style_Sanitizer' => array(),
            'AMP_Blacklist_Sanitizer' => array(),
            'AMP_Img_Sanitizer' => array(),
            'AMP_Video_Sanitizer' => array(),
            'AMP_Audio_Sanitizer' => array(),
            'AMP_Iframe_Sanitizer' => array(
                'add_placeholder' => true,
            ),
        ),
        array(
            'content_max_width' => 320,
        )
    );

    $arResult["PREVIEW_TEXT"] = $amp_content_obj->get_amp_content();
}

if(!empty($arResult["DETAIL_TEXT"])){

    $arResult["DETAIL_TEXT"] = (function_exists('tidy_repair_string') && LANG_CHARSET == 'UTF-8')
        ? tidy_repair_string($arResult["DETAIL_TEXT"], array('show-body-only' => true, 'drop-font-tags' => true, 'output-html' => true), "utf8")
        : $arResult["DETAIL_TEXT"];

    $amp_content_obj = new AMP_Content( $arResult["DETAIL_TEXT"],
        array(
            //'AMP_YouTube_Embed_Handler' => array(),
        ),
        array(
            'AMP_Style_Sanitizer' => array(),
            'AMP_Blacklist_Sanitizer' => array(),
            'AMP_Img_Sanitizer' => array(),
            'AMP_Video_Sanitizer' => array(),
            'AMP_Audio_Sanitizer' => array(),
            'AMP_Iframe_Sanitizer' => array(
                'add_placeholder' => true,
            ),
        ),
        array(
            'content_max_width' => 320
        )
    );

    $arResult["DETAIL_TEXT"] = $amp_content_obj->get_amp_content();
}

$arResult['CANONICAL_URL'] = $canonical_url;

if (is_object($this->__component))
{
    $resultCacheKeys = array_keys($arResult);

    $this->__component->SetResultCacheKeys(
        $resultCacheKeys
    );

    foreach($resultCacheKeys as $resultCacheKey){

        if (!isset($arResult[$resultCacheKey]))
            $arResult[$resultCacheKey] = $this->__component->arResult[$resultCacheKey];

    };

};
