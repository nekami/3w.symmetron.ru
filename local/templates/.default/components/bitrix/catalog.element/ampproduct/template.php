<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(false);
$haveOffers = !empty($arResult['OFFERS']);

if($haveOffers){

    $strKeyOfferArr = $arResult['strKeyOfferArr'];
    $strIdToGroup = $arResult['strIdToGroup'];

    $applicationJson = $arResult['applicationJson'];
    $applicationJsonArr = $arResult['applicationJsonArr'];
    $keysToOffers = $arResult['keysToOffers'];
    $strIdToOffer = $arResult['strIdToOffer'];
    $emptyOffers = !(empty($applicationJson) ? false : $haveOffers);

}

?>
<div class="product-detail" itemscope itemtype="http://schema.org/Product">
    <? if(isset($arResult['SECTION_PAGE_URL']) && !empty($arResult['SECTION_PAGE_URL'])): ?>
    <div class="bx_catalog_item_articul">
        <a class="section-link" href="<?=$arResult['SECTION_PAGE_URL'];?>">
            <?=sprintf(GetMessage('TMPL_SECTION_LIST_BACK'),$arResult['SECTION_NAME']);?>
        </a>
    </div>
    <?
    endif;

    if(!empty($arResult['GALLERY'])){

        ?>
        <div class="product-image">
            <?php if($haveOffers){ ?>
                <div class="product-gallery">
                    <?php $isntFirst = false; ?>
                    <?php foreach($arResult['GALLERY'] as $keyOffer => $photos) { ?>
                        <amp-carousel<?php if($isntFirst): ?> hidden<?php endif; ?> controls loop id="selectedOffer<?php echo $keyOffer; ?>" height="300" layout="fixed-height" type="slides"  [slide]="product.selectedSlideFor<?php echo $keyOffer; ?>"<?php if(!$emptyOffers): ?> on="slideChange: AMP.setState({product: {selectedSlideFor<?php echo $keyOffer; ?>: event.index}})"<?php endif; ?> class="fadeIn" [hidden]="product.selectedOffer != '<?php echo $keyOffer; ?>'">
                            <?php foreach($photos as $iNumber => $image) { ?>
                                <amp-img itemprop="image" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" layout="responsive"<?=$image["srcset"];?> tabindex="0">
                                    <noscript>
                                        <img src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["width"];?>" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" />
                                    </noscript>
                                </amp-img>
                            <?php } ?>
                        </amp-carousel>
                        <?php $isntFirst = true; ?>
                    <?php } ?>
                </div>
                <?php if(sizeof($arResult['GALLERY']) > 1){ ?>
                <div class="product-gallery sub-gallery">
                    <?php $isntFirst = false; ?>
                    <?php foreach($arResult['GALLERY'] as $keyOffer => $photos){ ?>
                        <?php if(sizeof($photos) > 1){ ?>
                        <ul<?php if($isntFirst): ?> hidden<?php endif; ?> [hidden]="product.selectedOffer != '<?php echo $keyOffer;?>'">
                            <?php foreach($photos as $iNumber => $image) { ?>
                                <li>
                                    <amp-img<?php if(!$emptyOffers): ?> on="tap:AMP.setState({product: {selectedSlideFor<?php echo $keyOffer;?>: <?php echo $iNumber; ?>}})"<?php endif; ?> src="<?=$image["src"];?>" height="100" width="<?=round($image["width"] / $image["height"] * 100,0);?>" class="selected" [class]="product.selectedSlideFor<?php echo $keyOffer;?> == <?php echo $iNumber; ?> ? 'selected' : '' " tabindex="<?=$iNumber;?>" role="button">
                                        <noscript>
                                            <img src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" />
                                        </noscript>
                                    </amp-img>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php $isntFirst = true; ?>
                        <?php }; ?>
                    <?php } ?>
                </div>
                <?php } ?>
            <?php } ?>
            <?php if(!$haveOffers){ ?>
                <amp-carousel width="640" height="480" layout="responsive" type="slides">
                    <?php foreach($arResult['GALLERY'] as $image){ ?>
                        <amp-img itemprop="image" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" layout="responsive" <?=$image["srcset"];?>>
                            <noscript>
                                <img src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" />
                            </noscript>
                        </amp-img>
                    <?php } ?>
                </amp-carousel>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if(!$emptyOffers && !empty($applicationJson)){ ?>
        <amp-state id="product">
            <script type="application/json"><?=$applicationJson;?></script>
        </amp-state>
    <?php } ?>
    <div class="main-content">
        <div class="products-detail">
            <div class="product-info">
                <?php

                if ($haveOffers && !empty($arResult['OFFERS_PROP']))
                {

                    $isntFirst = false;

                    if(!empty($arResult['OFFER_PROPS'])) {


                        foreach ($arResult['OFFER_PROPS'] as $keyOffer => $displayProps) {

                            ?>
                            <dl<?php if ($isntFirst): ?> hidden<?php endif; ?> class="offer-properties"
                                                                               [hidden]="product.selectedOfferKey != '<?php echo $keyOffer; ?>'">
                                <?php foreach ($displayProps as $arOneProp){ ?>
                                    <dt>
                                        <span>
                                            <? echo $arOneProp['NAME']; ?>
                                        </span>
                                    </dt>
                                    <dd>
                                        <?
                                        $display_value = (is_array($arOneProp['DISPLAY_VALUE'])
                                            ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                            : $arOneProp['DISPLAY_VALUE']
                                        );

                                        switch ($display_value) {
                                            case 'Y':
                                                echo GetMessage('CT_BCE_YES');
                                                break;
                                            case 'N':
                                                echo GetMessage('CT_BCE_NO');
                                                break;
                                            default:
                                                echo $display_value;
                                                break;
                                        }

                                        ?>
                                    </dd>
                                <?php }; ?>
                            </dl>
                            <?

                            $isntFirst = true;

                        };

                    };

                    ?>
                    <div>
                        <?

                        $isFirstProp = false;
                        $firstProp = array();

                        foreach ($arResult['SKU_PROPS'] as $skuProperty)
                        {

                            if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                continue;

                            $propertyId = $skuProperty['ID'];

                            ?>
                            <div class="items-center flex">
                                <label for="quantity">
                                    <?=htmlspecialcharsEx($skuProperty['NAME']);?>:
                                </label>
                                <amp-selector name="<?php echo $skuProperty['CODE']; ?>" layout="container" role="listbox">
                                    <ul class="p0 m1">
                                        <?

                                        $strKeyOffer = '';

                                        foreach ($skuProperty['VALUES'] as $vNumber => &$value)
                                        {
                                            $sTreeKey = $propertyId.'_'.$value['ID'];

                                            foreach ($arResult['JS_OFFERS'] as $offer)
                                            {
                                                $currentOffersList = array();

                                                if (!empty($offer['TREE']) && is_array($offer['TREE']))
                                                {
                                                    foreach ($offer['TREE'] as $propName => $skuId)
                                                    {
                                                        $propId = (int)substr($propName, 5);

                                                        if ($propertyId == $propId)
                                                        {

                                                            if ($value['ID'] == $skuId)
                                                            {

                                                                if(!$isFirstProp
                                                                    && isset($firstProp[$skuId])){

                                                                    continue;

                                                                }


                                                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                                if($value['NAME'] == '-') continue;

                                                                $strKeyOffer = '';
                                                                $strGroupOffer = '';


                                                                if(!$isFirstProp){

                                                                    if(isset($strIdToGroup[$sTreeKey])){

                                                                        $subProps = $strIdToGroup[$sTreeKey];
                                                                        $strKeyOffer = join('_',$subProps);

                                                                    }

                                                                    if(isset($strIdToOffer[$sTreeKey])){

                                                                        $subProps = $strIdToOffer[$sTreeKey];
                                                                        $strGroupOffer = join('_',$subProps);

                                                                    }

                                                                    if ($skuProperty['SHOW_MODE'] === 'PICT')
                                                                    {
                                                                        ?>
                                                                        <li on="tap:AMP.setState({ product: {selectedOfferKey: '<?=$offer['ID'];?>', selectedOffer: '<?=$strKeyOffer;?>', selectedGroupOffer: '<?=$strGroupOffer;?>'}})" role="option" tabindex="<?php echo $vNumber; ?>" class="product-item-scu-item-color-container" title="<?=$value['NAME']?>">
                                                                            <amp-img src="<?=$value['PICT']['SRC'];?>" height="<?=$value['PICT']['HEIGHT']?>" width="<?=$value['PICT']['WIDTH']?>">
                                                                            </amp-img>
                                                                        </li>
                                                                        <?
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <li on="tap:AMP.setState({ product: {selectedOfferKey: '<?=$offer['ID'];?>', selectedOffer: '<?=$strKeyOffer;?>', selectedGroupOffer: '<?=$strGroupOffer;?>'}})" role="option" tabindex="<?php echo $vNumber; ?>" class="product-item-scu-item-text-container">
                                                                            <?=html_entity_decode($value['NAME'],ENT_QUOTES,LANG_CHARSET);?>
                                                                        </li>
                                                                        <?
                                                                    }

                                                                    if(!isset($firstProp[$skuId])){
                                                                        $firstProp[$skuId] = $skuId;
                                                                    }

                                                                } else {

                                                                    $sGroupKey = $sTreeKey.'_'.$offer['ID'];
                                                                    $sTreeGroupKey = '';

                                                                    foreach($strIdToOffer as $strKey => $strValArr){

                                                                        if(in_array($sGroupKey,$strValArr)){
                                                                            $sTreeGroupKey = $strKey;
                                                                            break;
                                                                        }

                                                                    }

                                                                    if(isset($strIdToOffer[$sTreeGroupKey])){

                                                                        $subProps = $strIdToOffer[$sTreeGroupKey];
                                                                        $strGroupOffer = join('_',$subProps);

                                                                    }

                                                                    if(isset($strIdToGroup[$sTreeGroupKey])){

                                                                        $subProps = $strIdToGroup[$sTreeGroupKey];
                                                                        $strKeyOffer = join('_',$subProps);

                                                                    }

                                                                    if(!empty($strGroupOffer)){

                                                                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                                        if($value['NAME'] == '-') continue;

                                                                        if ($skuProperty['SHOW_MODE'] === 'PICT')
                                                                        {
                                                                            ?>
                                                                            <li <?php if(!empty($strGroupOffer) && !(isset($applicationJsonArr['selectedGroupOffer']) && $strGroupOffer == $applicationJsonArr['selectedGroupOffer'])): ?> hidden<?php endif; ?> on="tap:AMP.setState({ product: {selectedOfferKey: '<?=$offer['ID'];?>', selectedOffer: '<?=$strKeyOffer;?>', selectedGroupOffer: '<?=$strGroupOffer;?>'}})" [hidden]="product.selectedGroupOffer != '<?php echo $strGroupOffer;?>'" role="option" tabindex="<?php echo $vNumber; ?>" class="product-item-scu-item-color-container" title="<?=$value['NAME']?>">
                                                                                <amp-img src="<?=$value['PICT']['SRC'];?>" height="<?=$value['PICT']['HEIGHT']?>" width="<?=$value['PICT']['WIDTH']?>">
                                                                                </amp-img>
                                                                            </li>
                                                                            <?
                                                                        }
                                                                        else
                                                                        {
                                                                            ?>
                                                                            <li<?php if(!empty($strGroupOffer) && !(isset($applicationJsonArr['selectedGroupOffer']) && $strGroupOffer == $applicationJsonArr['selectedGroupOffer'])): ?> hidden<?php endif; ?> on="tap:AMP.setState({ product: {selectedOfferKey: '<?=$offer['ID'];?>', selectedOffer: '<?=$strKeyOffer;?>', selectedGroupOffer: '<?=$strGroupOffer;?>'}})" [hidden]="product.selectedGroupOffer != '<?php echo $strGroupOffer;?>'" role="option" tabindex="<?php echo $vNumber; ?>" class="product-item-scu-item-text-container">
                                                                                <?=html_entity_decode($value['NAME'],ENT_QUOTES,LANG_CHARSET);?>
                                                                            </li>
                                                                            <?
                                                                        }

                                                                    }

                                                                }

                                                            }

                                                        }

                                                    }

                                                }

                                            }

                                        }

                                        ?>
                                    </ul>
                                    <input type="hidden" name="quantity" value="1">
                                </amp-selector>
                            </div>
                            <?

                            $isFirstProp = true;
                        }
                        ?>
                    </div>
                    <?
                }

                ?>
                <h1 itemprop="name" id="title-page">
                    <?=$arResult['NAME'];?>
                </h1>

                <?php if(isset($arResult["PROPERTIES"])
                    &&isset($arResult["PROPERTIES"]["MANUFACTURER_DETAIL"])
                    &&isset($arResult["PROPERTIES"]["MANUFACTURER_DETAIL"]["VALUE"])
                    &&!empty($arResult["PROPERTIES"]["MANUFACTURER_DETAIL"]["VALUE"])
                ){ ?>
                    <meta itemprop="manufacturer" content="<?=htmlspecialchars(is_array($arResult["PROPERTIES"]["MANUFACTURER_DETAIL"]["VALUE"]) ? join(', ',$arResult["PROPERTIES"]["MANUFACTURER_DETAIL"]["VALUE"]) : $arResult["PROPERTIES"]["MANUFACTURER_DETAIL"]["VALUE"],ENT_QUOTES,LANG_CHARSET);?>" >
                <?php } ?>
                <?php if(!$haveOffers){ ?>
                    <div class="item_price<?php if(!$arResult['CAN_BUY']){ ?> not-buy<?php }; ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <?php

                        if(isset($arResult['ITEM_PRICES'])
                            && is_array($arResult['ITEM_PRICES'])) {

                            $minPrice = array_pop($arResult['ITEM_PRICES']);
                            foreach ($arResult['ITEM_PRICES'] as $itemPrice) {
                                if ($itemPrice['BASE_PRICE'] < $minPrice['BASE_PRICE']) {
                                    $minPrice = $itemPrice;
                                }
                            }

                        } elseif($retailPrice = $arResult['PROPERTIES']['RETAIL_PRICE']['VALUE']) {

                            $minPrice = [
                                'RATIO_BASE_PRICE' => $retailPrice,
                                'PRINT_RATIO_PRICE' => CCurrencyLang::CurrencyFormat(($retailPrice), $arResult['ORIGINAL_PARAMETERS']['CURRENCY_ID'], true),
                                'CURRENCY' => $arResult['ORIGINAL_PARAMETERS']['CURRENCY_ID']
                            ];

                        } else {
                            $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
                            $minPrice['RATIO_BASE_PRICE'] = $minPrice['DISCOUNT_VALUE'];
                            $minPrice['PRINT_RATIO_PRICE'] = $minPrice['PRINT_DISCOUNT_VALUE'];
                        }

                        if(!empty($minPrice['RATIO_BASE_PRICE'])){

                        ?>
                        <div class="item-price">
                            <?=getMessage('CT_PRICE_FROM', array('#PRICE#' => $minPrice['PRINT_RATIO_PRICE']))?>
                            <meta itemprop="price" content="<?=abs((float)$minPrice['RATIO_BASE_PRICE']); ?>" />
                            <? if(isset($minPrice['CURRENCY']) && !empty($minPrice['CURRENCY'])){?>
                                <meta itemprop="priceCurrency" content="<?=$minPrice['CURRENCY'];?>" />
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <div class="buy-buttons">

                            <link itemprop="availability" href="http://schema.org/InStock" />
                            <a rel="nofollow" href="<?=$arResult['CANONICAL_URL']?>" class="btn btn-block btn-large btn-buy">
                                <?=($arResult['ORIGINAL_PARAMETERS']['MESS_BTN_ADD_TO_BASKET']) ?: GetMessage('CT_TO_ITEM_CARD');?>
                            </a>

<!--                            --><?// if($arResult['CAN_BUY']){?>
<!--                                <link itemprop="availability" href="http://schema.org/InStock" />-->
<!--                                <a rel="nofollow" href="--><?//=$APPLICATION->GetCurPage();?><!--?action=add2basketamp&--><?php //if(isset($arResult["BUY_ID"]) && !empty($arResult["BUY_ID"])){ ?><!--id=--><?//=$arResult["BUY_ID"];?><!--&PRODUCT_BUY_ID=--><?//=$arResult["ID"];?><!----><?php //} else { ?><!--id=--><?//=$arResult["ID"];?><!----><?php //} ?><!--" class="btn btn-block btn-large btn-buy">-->
<!--                                    --><?//=GetMessage('CT_BCE_CATALOG_ADD');?>
<!--                                </a>-->
<!--                            --><?php //} ?>
                        </div>

                    </div>
                <?php } else { ?>
                    <?php

                    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
                        ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
                        : reset($arResult['OFFERS']);

                    $minPrice = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];

                    ?>
                    <div class="item_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <div class="item-price" [text]="product['price' + product.selectedOfferKey]">
                            <? echo $minPrice['PRINT_RATIO_PRICE']; ?>
                            <meta itemprop="price" content="<?=abs((float)$minPrice['RATIO_BASE_PRICE']); ?>" />
                            <? if(isset($minPrice['CURRENCY']) && !empty($minPrice['CURRENCY'])){?>
                                <meta itemprop="priceCurrency" content="<?=$minPrice['CURRENCY'];?>" />
                            <?php } ?>
                        </div>
                        <div class="buy-buttons">
                            <link itemprop="availability" href="http://schema.org/InStock" />
                            <a rel="nofollow" [href]="product['link' + product.selectedOfferKey]" href="<?=$APPLICATION->GetCurPage();?>?action=add2basketamp&id=<?php echo $actualItem["ID"]; ?>" class="btn btn-block btn-large btn-buy">
                                <?=GetMessage('CT_BCE_CATALOG_ADD');?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="detail-tabs">
                <amp-accordion>
                    <?php if(!empty($arResult['DETAIL_TEXT'])){ ?>
                        <section class="description" itemprop="description" expanded="expanded">
                            <h4>
                                <?=GetMessage('CT_BCE_CATALOG_DESCRIPTION');?>
                            </h4>
                            <div class="tab-content">
                                <?php if($arResult["PREVIEW_TEXT"]){ ?>
                                    <div class="preview text clearfix" itemprop="description">
                                        <?php echo ($arResult["PREVIEW_TEXT"]);?>
                                    </div>
                                <?php };?>
                                <?php if(!empty($arResult['DETAIL_TEXT'])){?>
                                    <div class="preview-text">
                                        <?=$arResult['DETAIL_TEXT'];?>
                                    </div>
                                <?php }?>
                            </div>
                        </section>
                    <?php } ?>
                    <?php if (!empty($arResult['DISPLAY_PROPERTIES'])){ ?>
                        <section class="description" itemprop="description">
                            <h4>
                                <?=GetMessage('CT_BCE_CATALOG_PROPERTIES');?>
                            </h4>
                            <div class="tab-content">
                                <dl class="properties">
                                    <? foreach ($arResult['DISPLAY_PROPERTIES'] as $pid => $arOneProp){ ?>
                                        <dt>
                                                <span>
                                                    <? echo $arOneProp['NAME']; ?>
                                                </span>
                                        </dt>
                                        <dd>
                                            <?
                                            $display_value = (is_array($arOneProp['DISPLAY_VALUE'])
                                                ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                                : $arOneProp['DISPLAY_VALUE']
                                            );

                                            switch ($display_value){
                                                case 'Y':
                                                    echo GetMessage('CT_BCE_YES');
                                                    break;
                                                case 'N':
                                                    echo GetMessage('CT_BCE_NO');
                                                    break;
                                                default:
                                                    echo $display_value;
                                                    break;
                                            }

                                            ?>
                                        </dd>
                                        <?

                                    }

                                    unset($arOneProp);

                                    ?>
                                </dl>
                            </div>
                        </section>
                        <?
                    }

                    ?>
                    <?php

                    if(isset($arResult['TABS'])
                        && !empty($arResult['TABS'])){

                        $tabs = $arResult['TABS'];

                        foreach($tabs['tab_headers'] as $tab_count => $tab_name){?>
                            <? if(isset($tabs['tab_panels'][$tab_count]) && !empty($tabs['tab_panels'][$tab_count])){?>
                                <section>
                                    <h4>
                                        <?=$tab_name;?>
                                    </h4>
                                    <div class="tab-content">
                                        <? if(isset($tabs['tab_panels'][$tab_count])){?>
                                            <?=$tabs['tab_panels'][$tab_count];?>
                                        <?php }?>
                                    </div>
                                </section>
                                <?php
                            }
                        }
                        ?>

                        <?php
                    }

                    ?>

