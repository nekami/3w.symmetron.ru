<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

if(file_exists(__DIR__.'/amp_style.css')){

    $amp_style = file_get_contents(__DIR__.'/amp_style.css');
    if(get_class($this->__template)!=="CBitrixComponentTemplate")
        $this->InitComponentTemplate();

    $this->__template->SetViewTarget("AMP_STYLE");
    echo $amp_style;
    $this->__template->EndViewTarget();

}

$filter_set = false;

global ${$arParams["FILTER_NAME"]};

if((isset(${$arParams["FILTER_NAME"]})
    && !empty(${$arParams["FILTER_NAME"]}))
){

    foreach(${$arParams["FILTER_NAME"]} as $filter_key => $filter_value){
        if(stripos($filter_key,'=PROPERTY_') !== false){
            $filter_set = true;
            break;
        };
    };

};


$CANONICAL_PROPERTY = '';
$canonical_url = $canonical_path = $filterPath = '';

if(isset($arResult['CANONICAL_URL'])){

    $canonical_url = $arResult['CANONICAL_URL'];

    if(isset($canonical_url) && !empty($canonical_url)){

        $canonical_path = $canonical_url;

        $canonical_url = (preg_match('~http(s*?)://~',$canonical_url) == 0 ? ((CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$canonical_url) : $canonical_url);
        $canonical_url = preg_replace('~\:\/\/(www\.)*m\.~','://',$canonical_url);

        if($filter_set){

            $filterPath = preg_replace('~^.*?/filter/~','filter/',$APPLICATION->GetCurPage());

        }

        $SERVER_PAGE_URL = (CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $SERVER_PAGE_URL = preg_replace('~\?.*?$~is'.IMPEL_PREG_MODIFIER,'',$SERVER_PAGE_URL);
        $DETAIL_PAGE_URL = preg_replace('~\?.*?$~is'.IMPEL_PREG_MODIFIER,'',$canonical_url);

        if($DETAIL_PAGE_URL != $SERVER_PAGE_URL){

            $CANONICAL_PROPERTY .= '<link href="'.$canonical_url.$filterPath.'" rel="canonical" />'.PHP_EOL;

        };

    };

};

if(!empty($canonical_path)){
    $canonical_url = $canonical_path;
    $APPLICATION->SetPageProperty('canonical_url', $canonical_url);
};

if(!empty($CANONICAL_PROPERTY)){

    if(get_class($this->__template)!=="CBitrixComponentTemplate")
        $this->InitComponentTemplate();


    $this->__template->SetViewTarget("CANONICAL_PROPERTY");

    echo $CANONICAL_PROPERTY;

    $this->__template->EndViewTarget();

}
