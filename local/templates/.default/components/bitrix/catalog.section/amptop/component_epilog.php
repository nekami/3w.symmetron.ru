<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

if(file_exists(__DIR__.'/amp_style.css')){

    $amp_style = file_get_contents(__DIR__.'/amp_style.css');
    if(get_class($this->__template)!=="CBitrixComponentTemplate")
        $this->InitComponentTemplate();

    $this->__template->SetViewTarget("AMP_STYLE");
    echo $amp_style;
    $this->__template->EndViewTarget();

}

$canonical_url = $canonical_path;
 
if(isset($arResult['CANONICAL_URL'])){

    $canonical_url = $arResult['CANONICAL_URL'];

    if(isset($canonical_url) && !empty($canonical_url)){

        $canonical_path = $canonical_url;

        $canonical_url = (preg_match('~http(s*?)://~',$canonical_url) == 0 ? ((CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME']).$canonical_url : $canonical_url);
        $canonical_url = preg_replace('~\:\/\/(www\.)*m\.~','://',$canonical_url);

        $SERVER_PAGE_URL = (CMain::IsHTTPS() ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $SERVER_PAGE_URL = preg_replace('~\?.*?$~is'.IMPEL_PREG_MODIFIER,'',$SERVER_PAGE_URL);
        $DETAIL_PAGE_URL = preg_replace('~\?.*?$~is'.IMPEL_PREG_MODIFIER,'',$canonical_url);

        if($DETAIL_PAGE_URL != $SERVER_PAGE_URL){

            if(get_class($this->__template)!=="CBitrixComponentTemplate")
                $this->InitComponentTemplate();

            $this->__template->SetViewTarget("CANONICAL_PROPERTY");
            ?>
            <link href="<?=$canonical_url;?>" rel="canonical" />
            <?
            $this->__template->EndViewTarget();

        };

    };

};

if(!empty($canonical_path)){
    $canonical_url = $canonical_path;
    $APPLICATION->SetPageProperty('canonical_url', $canonical_url);
};



