<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

$haveOffers = !empty($item['OFFERS']);

if($haveOffers){

    $offerPropCode = '';
    $applicationJson = '';

    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
    $firstOffer = true;

    foreach($item['OFFERS'] as $keyOffer => $valueOffer) {

        $applicationJsonArr["selectedSlideFor".$keyOffer] = 0;
        $itemPrice = current($valueOffer["ITEM_PRICES"]);
        $offerPrice = ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']);
        $applicationJsonArr['price'.$keyOffer] = $offerPrice;
        $applicationJsonArr['link'.$keyOffer] = ''.$APPLICATION->GetCurPage().'?action=add2basketamp&id='.$valueOffer['ID'].'';

    }

    if(!empty($applicationJsonArr)) {

        $applicationJsonArr['selectedOffer'] = isset($item['OFFERS_SELECTED']) ? $item['OFFERS_SELECTED'] : 0;
        $applicationJson = json_encode($applicationJsonArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    $emptyOffers = !(empty($applicationJson) ? false : $haveOffers);

}


?>
<div class="product-detail">
    <?
    if(!empty($item['GALLERY'])){
        ?>
        <div class="product-image">
            <?php if($haveOffers){ ?>
                <div class="product-gallery">
                    <a href="<?=$item['DETAIL_PAGE_URL']?>">
                        <?php $isntFirst = false; ?>
                        <?php foreach($item['GALLERY'] as $keyOffer => $photos) { ?>
                            <amp-carousel id="selectedOffer<?php echo $keyOffer; ?>" width="640" height="480" layout="responsive" type="slides" class="fadeIn">
                                <?php foreach($photos as $iNumber => $image) { ?>
                                    <amp-img itemprop="image" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" layout="responsive"<?=$image["srcset"];?> tabindex="0">
                                        <noscript>
                                            <img src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" />
                                        </noscript>
                                    </amp-img>
                                <?php } ?>
                            </amp-carousel>
                            <?php $isntFirst = true; break; ?>
                        <?php } ?>
                    </a>
                </div>
            <?php } ?>
            <?php if(!$haveOffers){ ?>
                <a href="<?=$item['DETAIL_PAGE_URL']?>">
                    <amp-carousel width="640" height="480" layout="responsive" type="slides">
                        <?php foreach($item['GALLERY'] as $image){ ?>
                            <amp-img itemprop="image" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" layout="responsive"<?=$image["srcset"];?>>
                                <noscript>
                                    <img src="<?=$image["src"];?>" width="<?=$image["width"];?>" height="<?=$image["height"];?>" alt="<?=htmlspecialchars($image['alt'],ENT_QUOTES,LANG_CHARSET);?>" />
                                </noscript>
                            </amp-img>
                        <?php } ?>
                    </amp-carousel>
                </a>
            <?php } ?>
        </div>
    <?php } ?>
    <h2>
        <a href="<?=$item['DETAIL_PAGE_URL']?>">
            <?=$item['NAME'];?>
        </a>
    </h2>
    <?php

    if(!$haveOffers) {

        if(empty($price)){

            $price = (isset($item['RATIO_PRICE']) ? $item['RATIO_PRICE'] : $item['MIN_PRICE']);
            $price['RATIO_BASE_PRICE'] = $price['DISCOUNT_VALUE'];
            $price['PRINT_RATIO_PRICE'] = $price['PRINT_DISCOUNT_VALUE'];

        }



        ?>
        <div class="item_price">
            <? if (!empty($price)
                && !empty($price['RATIO_BASE_PRICE'])){ ?>
                <div class="item-price">
                    <div><?=getMessage('CT_BCI_TPL_PRICE_FROM')?></div>&nbsp;
                    <?//=getMessage('CT_BCI_TPL_MESS_PRICE_FROM', array('#PRICE#' => $price['PRINT_RATIO_PRICE']))?>
                    <div><?echo $price['PRINT_RATIO_PRICE']; ?></div>
                </div>
            <? } ?>
            <?if(isset($arResult['BUTTON_TO_MAIN_CARD']) && $arResult['BUTTON_TO_MAIN_CARD'] == 'Y' && $item['CANONICAL_URL']):?>
                <a rel="nofollow" href="<?=$item['CANONICAL_URL']?>" class="btn btn-block btn-large btn-buy">
                    <?=($arParams['MESS_BTN_ADD_TO_BASKET']) ?: GetMessage('CT_BCE_CATALOG_ADD');?>
                </a>
            <?else:?>
                <a rel="nofollow" href="<?=$item['DETAIL_PAGE_URL']?>" class="btn btn-block btn-large btn-buy">
                    <?=GetMessage('CT_BCE_CATALOG_ADD');?>
                </a>
            <?endif;?>
        </div>
    <?php } else {

        $actualItem = isset($item['OFFERS'][$item['OFFERS_SELECTED']])
            ? $item['OFFERS'][$item['OFFERS_SELECTED']]
            : reset($item['OFFERS']);

        $minPrice = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];

        ?>
        <div class="item_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <div class="item-price">
                <? echo $minPrice['PRINT_RATIO_PRICE']; ?>
                <meta itemprop="price" content="<?=abs((float)$minPrice['RATIO_BASE_PRICE']); ?>" />
                <? if(isset($minPrice['CURRENCY']) && !empty($minPrice['CURRENCY'])){?>
                    <meta itemprop="priceCurrency" content="<?=$minPrice['CURRENCY'];?>" />
                <?php } ?>
            </div>
            <link itemprop="availability" href="http://schema.org/InStock" />
            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="btn btn-block btn-large btn-buy">
                <?=GetMessage('CT_BCE_CATALOG_ADD');?>
            </a>
        </div>
    <? } ?>
</div>
