<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

if (isset($arResult['ITEM']))
{
    $item = $arResult['ITEM'];

    if ($arResult['CANONICAL_URL'])
        $item['CANONICAL_URL'] = CComponentEngine::MakePathFromTemplate($arResult['CANONICAL_URL'], array('ELEMENT_CODE' => $item['CODE']));

    $areaId = $arResult['AREA_ID'];
    $itemIds = array(
        'ID' => $areaId,
        'PICT' => $areaId.'_pict',
        'SECOND_PICT' => $areaId.'_secondpict',
        'PICT_SLIDER' => $areaId.'_pict_slider',
        'STICKER_ID' => $areaId.'_sticker',
        'SECOND_STICKER_ID' => $areaId.'_secondsticker',
        'QUANTITY' => $areaId.'_quantity',
        'QUANTITY_DOWN' => $areaId.'_quant_down',
        'QUANTITY_UP' => $areaId.'_quant_up',
        'QUANTITY_MEASURE' => $areaId.'_quant_measure',
        'QUANTITY_LIMIT' => $areaId.'_quant_limit',
        'BUY_LINK' => $areaId.'_buy_link',
        'BASKET_ACTIONS' => $areaId.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
        'SUBSCRIBE_LINK' => $areaId.'_subscribe',
        'COMPARE_LINK' => $areaId.'_compare_link',
        'PRICE' => $areaId.'_price',
        'PRICE_OLD' => $areaId.'_price_old',
        'PRICE_TOTAL' => $areaId.'_price_total',
        'DSC_PERC' => $areaId.'_dsc_perc',
        'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
        'PROP_DIV' => $areaId.'_sku_tree',
        'PROP' => $areaId.'_prop_',
        'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
        'BASKET_PROP_DIV' => $areaId.'_basket_prop',
    );
    $obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
    $isBig = isset($arResult['BIG']) && $arResult['BIG'] === 'Y';

    $productTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
        ? $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
        : $item['NAME'];

    $imgTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
        ? $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
        : $item['NAME'];

    $skuProps = array();

    $haveOffers = !empty($item['OFFERS']);
    if ($haveOffers)
    {
        $actualItem = isset($item['OFFERS'][$item['OFFERS_SELECTED']])
            ? $item['OFFERS'][$item['OFFERS_SELECTED']]
            : reset($item['OFFERS']);
    }
    else
    {
        $actualItem = $item;
    }

    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
    {
        if ($retailPrice = $item['PROPERTIES']['RETAIL_PRICE']['VALUE']) {

            $price = [
                'RATIO_BASE_PRICE' => $retailPrice,
                'PRINT_RATIO_PRICE' => CCurrencyLang::CurrencyFormat(($retailPrice), 'RUB', true),
                'CURRENCY' => 'RUB'
            ];
        } else {
            $price = $item['ITEM_START_PRICE'];
        }
        $minOffer = $item['OFFERS'][$item['ITEM_START_PRICE_SELECTED']];
        $measureRatio = $minOffer['ITEM_MEASURE_RATIOS'][$minOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
        $morePhoto = $item['MORE_PHOTO'];
    }
    else
    {
        $price = array_pop($item['ITEM_PRICES']);
        foreach ($item['ITEM_PRICES'] as $itemPrice) {
            if ($itemPrice['BASE_PRICE'] < $price['BASE_PRICE']) {
                $price = $itemPrice;
            }
        }
        //$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];

        $measureRatio = $price['MIN_QUANTITY'];
        $morePhoto = $actualItem['MORE_PHOTO'];
    }

    if(empty($morePhoto)){
        $morePhoto = array();
    }

    if(empty($morePhoto)
        && isset($item['PREVIEW_PICTURE'])
        && isset($item['PREVIEW_PICTURE']['SRC'])){

        array_unshift($morePhoto,$item['PREVIEW_PICTURE']);

    }

    if(empty($morePhoto)
        && isset($item['PREVIEW_PICTURE_SECOND'])
        && isset($item['PREVIEW_PICTURE_SECOND']['SRC'])){

        array_unshift($morePhoto,$item['PREVIEW_PICTURE_SECOND']);

    }

    if(!empty($morePhoto)){

        foreach($morePhoto as $number => $photo){

            if(!empty($morePhoto[$number]['SRC'])){

                if(!isset($morePhoto[$number]['ID'])){
                    $morePhoto[$number]['ID'] = 0;
                }

                if(file_exists($_SERVER['DOCUMENT_ROOT'].$morePhoto[$number]['SRC'])
                    && filesize($_SERVER['DOCUMENT_ROOT'].$morePhoto[$number]['SRC']) > 0
                    && is_readable($_SERVER['DOCUMENT_ROOT'].$morePhoto[$number]['SRC'])){

                    $sizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$morePhoto[$number]['SRC']);

                    if($sizes
                        && is_array($sizes)
                        && isset($sizes[0])
                        && isset($sizes[1])
                        && !empty($sizes[0])
                        && !empty($sizes[1])
                    ){

                        $morePhoto[$number]['WIDTH'] = $sizes[0];
                        $morePhoto[$number]['HEIGHT'] = $sizes[1];

                    }


                }

                $srcSetHTML = '';
                $morePhoto[$number]['srcset'] = '';
                $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($morePhoto[$number]['SRC']);
                $morePhoto[$number]['srcset'] = $srcSetHTML;

            }

        }

    }

    $showSlider = is_array($morePhoto) && count($morePhoto) > 1;
    $showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($item['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

    $discountPositionClass = isset($arResult['BIG_DISCOUNT_PERCENT']) && $arResult['BIG_DISCOUNT_PERCENT'] === 'Y'
        ? 'product-item-label-big'
        : 'product-item-label-small';
    $discountPositionClass .= $arParams['DISCOUNT_POSITION_CLASS'];

    $labelPositionClass = isset($arResult['BIG_LABEL']) && $arResult['BIG_LABEL'] === 'Y'
        ? 'product-item-label-big'
        : 'product-item-label-small';
    $labelPositionClass .= $arParams['LABEL_POSITION_CLASS'];

    $buttonSizeClass = isset($arResult['BIG_BUTTONS']) && $arResult['BIG_BUTTONS'] === 'Y' ? 'btn-md' : 'btn-sm';

    if(!empty($item["PREVIEW_TEXT"])){
        $amp_content_obj = new AMP_Content($item["PREVIEW_TEXT"],
            array(
                //'AMP_YouTube_Embed_Handler' => array(),
            ),
            array(
                'AMP_Style_Sanitizer' => array(),
                'AMP_Blacklist_Sanitizer' => array(),
                'AMP_Img_Sanitizer' => array(),
                'AMP_Video_Sanitizer' => array(),
                'AMP_Audio_Sanitizer' => array(),
                'AMP_Iframe_Sanitizer' => array(
                    'add_placeholder' => true,
                ),
            ),
            array(
                'content_max_width' => 320,
            )
        );

        $item["PREVIEW_TEXT"] = $amp_content_obj->get_amp_content();
    }

    if(!empty($item["DETAIL_TEXT"])){
        $amp_content_obj = new AMP_Content( $item["DETAIL_TEXT"],
            array(
                //'AMP_YouTube_Embed_Handler' => array(),
            ),
            array(
                'AMP_Style_Sanitizer' => array(),
                'AMP_Blacklist_Sanitizer' => array(),
                'AMP_Img_Sanitizer' => array(),
                'AMP_Video_Sanitizer' => array(),
                'AMP_Audio_Sanitizer' => array(),
                'AMP_Iframe_Sanitizer' => array(
                    'add_placeholder' => true,
                ),
            ),
            array(
                'content_max_width' => 320
            )
        );

        $item["DETAIL_TEXT"] = $amp_content_obj->get_amp_content();
    }

    $arEmptyPreview = false;
    $strEmptyPreview = $templateFolder.'/images/no_photo.png';
    if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview))
    {

        $arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);

        if (!empty($arSizes))
        {
            $arEmptyPreview = array(
                'src' => $strEmptyPreview,
                'width' => (int)$arSizes[0],
                'height' => (int)$arSizes[1],
                'alt' => $arResult['NAME']
            );

        }
        unset($arSizes);
    }

    unset($strEmptyPreview);

    $gallery = array();

    if(!$haveOffers) {

        $add_pict_prop = isset($arParams["ADD_PICT_PROP"])
        && !empty($arParams["ADD_PICT_PROP"])
            ? $arParams["ADD_PICT_PROP"]
            : "MORE_PHOTO";

        if (isset($item["DISPLAY_PROPERTIES"])
            && isset($item["PROPERTIES"][$add_pict_prop])
            && is_array($item["PROPERTIES"][$add_pict_prop])
            && sizeof($item["PROPERTIES"][$add_pict_prop])
            && isset($item["PROPERTIES"][$add_pict_prop]["VALUE"])
            && !empty($item["PROPERTIES"][$add_pict_prop]["VALUE"])
        ) {

            foreach ($item["PROPERTIES"][$add_pict_prop]["VALUE"] as $number => $file_id) {

                if (is_numeric($file_id)) {
                    $gallery[$number]['src'] = CFile::GetPath($file_id);
                } else {
                    $gallery[$number]['src'] = $file_id;
                }

                if (!empty($gallery[$number]['src'])) {

                    $gallery[$number]['width'] = 640;
                    $gallery[$number]['height'] = 480;


                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src'])
                        && filesize($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src']) > 0
                        && is_readable($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src'])) {

                        $sizes = getimagesize($_SERVER['DOCUMENT_ROOT'] . $gallery[$number]['src']);

                        if ($sizes
                            && is_array($sizes)
                            && isset($sizes[0])
                            && isset($sizes[1])
                            && !empty($sizes[0])
                            && !empty($sizes[1])
                        ) {

                            $gallery[$number]['width'] = $sizes[0];
                            $gallery[$number]['height'] = $sizes[1];

                        }


                    }

                    $srcSetHTML = '';
                    $gallery[$number]['srcset'] = '';
                    $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
                    $gallery[$number]['srcset'] = $srcSetHTML;
                    $gallery[$number]['alt'] = $item['NAME'];

                }

            }

        };


        if(isset($item["PREVIEW_PICTURE"])
            && isset($item["PREVIEW_PICTURE"]["SRC"])
            && !empty($item["PREVIEW_PICTURE"]["SRC"])
            && empty($gallery)){

            $number = sizeof($gallery);
            $gallery[$number]['src'] = $item["PREVIEW_PICTURE"]["SRC"];
            $gallery[$number]['width'] = $item["PREVIEW_PICTURE"]["WIDTH"];
            $gallery[$number]['height'] = $item["PREVIEW_PICTURE"]["HEIGHT"];

            $srcSetHTML = '';
            $gallery[$number]['srcset'] = '';
            $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
            $gallery[$number]['srcset'] = $srcSetHTML;

        } else if(isset($item["DETAIL_PICTURE"])
            && isset($item["DETAIL_PICTURE"]["SRC"])
            && !empty($item["DETAIL_PICTURE"]["SRC"])
            && empty($gallery)){

            $number = sizeof($gallery);
            $gallery[$number]['src'] = $item["DETAIL_PICTURE"]["SRC"];
            $gallery[$number]['width'] = $item["DETAIL_PICTURE"]["WIDTH"];
            $gallery[$number]['height'] = $item["DETAIL_PICTURE"]["HEIGHT"];

            $srcSetHTML = '';
            $gallery[$number]['srcset'] = '';
            $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
            $gallery[$number]['srcset'] = $srcSetHTML;

        };

        if(empty($gallery)){
            $number = sizeof($gallery);
            $gallery[$number] = $arEmptyPreview;
            $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($arEmptyPreview['src']);
            $gallery[$number]['srcset'] = $srcSetHTML;
        }



    } else {

        foreach ($item['OFFERS'] as $keyOffer => $offer)
        {

            if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0){

                if(isset($offer['PREVIEW_PICTURE'])
                    && isset($offer['PREVIEW_PICTURE']['SRC'])
                    && !empty($offer['PREVIEW_PICTURE']['SRC'])){

                    $offer['MORE_PHOTO'][] = $offer['PREVIEW_PICTURE'];

                } else if(isset($offer['DETAIL_PICTURE'])
                    && isset($offer['DETAIL_PICTURE']['SRC'])
                    && !empty($offer['DETAIL_PICTURE']['SRC'])){

                    $offer['MORE_PHOTO'][] = $offer['DETAIL_PICTURE'];

                }


            }

            foreach ($offer['MORE_PHOTO'] as $number => $photo)
            {

                if (!empty($photo['SRC'])) {

                    $photo['width'] = 640;
                    $photo['height'] = 480;


                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $photo['SRC'])
                        && filesize($_SERVER['DOCUMENT_ROOT'] . $photo['SRC']) > 0
                        && is_readable($_SERVER['DOCUMENT_ROOT'] . $photo['SRC'])) {

                        $sizes = getimagesize($_SERVER['DOCUMENT_ROOT'] . $photo['SRC']);

                        if ($sizes
                            && is_array($sizes)
                            && isset($sizes[0])
                            && isset($sizes[1])
                            && !empty($sizes[0])
                            && !empty($sizes[1])
                        ) {

                            $photo['width'] = $sizes[0];
                            $photo['height'] = $sizes[1];

                        }


                    }

                    $srcSetHTML = '';
                    $photo['srcset'] = '';
                    $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($photo['SRC']);
                    $gallery[$keyOffer][$number]['srcset'] = $srcSetHTML;
                    $gallery[$keyOffer][$number]['src'] = $photo['SRC'];

                    $gallery[$keyOffer][$number]['width'] = $photo['width'];
                    $gallery[$keyOffer][$number]['height'] = $photo['height'];
                    $gallery[$keyOffer][$number]['alt'] = $photo['NAME'];

                }

            }

        }

        if(empty($gallery)){

            if(isset($item["PREVIEW_PICTURE"])
                && isset($item["PREVIEW_PICTURE"]["SRC"])
                && !empty($item["PREVIEW_PICTURE"]["SRC"])
                && empty($gallery)){

                $number = sizeof($gallery);
                $gallery[$number][0]['src'] = $item["PREVIEW_PICTURE"]["SRC"];
                $gallery[$number][0]['width'] = $item["PREVIEW_PICTURE"]["WIDTH"];
                $gallery[$number][0]['height'] = $item["PREVIEW_PICTURE"]["HEIGHT"];

                $srcSetHTML = '';
                $gallery[$number][0]['srcset'] = '';
                $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
                $gallery[$number][0]['srcset'] = $srcSetHTML;

            } else if(isset($item["DETAIL_PICTURE"])
                && isset($item["DETAIL_PICTURE"]["SRC"])
                && !empty($item["DETAIL_PICTURE"]["SRC"])
                && empty($gallery)){

                $number = sizeof($gallery);
                $gallery[$number][0]['src'] = $item["DETAIL_PICTURE"]["SRC"];
                $gallery[$number][0]['width'] = $item["DETAIL_PICTURE"]["WIDTH"];
                $gallery[$number][0]['height'] = $item["DETAIL_PICTURE"]["HEIGHT"];

                $srcSetHTML = '';
                $gallery[$number][0]['srcset'] = '';
                $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($gallery[$number]['src']);
                $gallery[$number][0]['srcset'] = $srcSetHTML;

            };
        }

        if(empty($gallery)){
            $number = sizeof($gallery);
            $gallery[$number][0] = $arEmptyPreview;
            $srcSetHTML = ImpelTemplateTools::createAMPSRCSetHTML($arEmptyPreview['src']);
            $gallery[$number][0]['srcset'] = $srcSetHTML;
        }


    }






    $item['GALLERY'] = $gallery;

    ?>
    <div class="product-item-container<?=(isset($arResult['SCALABLE']) && $arResult['SCALABLE'] === 'Y' ? ' product-item-scalable-card' : '')?>"
         id="<?=$areaId?>" data-entity="item">
        <?
        $documentRoot = Main\Application::getDocumentRoot();
        $templatePath = strtolower($arResult['TYPE']).'/template.php';
        $file = new Main\IO\File($documentRoot.$templateFolder.'/'.$templatePath);
        if ($file->isExists())
        {
            include($file->getPath());
        }


        ?>
    </div>
    <?
    unset($item, $actualItem, $minOffer, $itemIds);
}