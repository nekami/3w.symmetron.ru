<?php
//ini_set('error_reporting', E_ALL & ~E_NOTICE);
//ini_set('error_log', __DIR__ . '/php-errors.php');
//error_log('Запись в лог', 0);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

class CMailSymmetron {

	public static function updateMail(&$event, &$lid, &$arFields, &$messageId, &$files, &$languageId){


		

      if ($event == "SALE_NEW_ORDER" or $event == "SALE_NEW_ORDER_BUY" or $event == "SALE_NEW_ORDER_EXAMPLE" or $event == "SALE_NEW_ORDER_REQUEST" ){
	
		require($_SERVER["DOCUMENT_ROOT"]."/local/modules/mixart.symmetron/classes/arOptions.php");
		$arCurOptionValues = array();
		$module_id = 'mixart.symmetron';
		foreach($arOptions as $opt => $arOptParams){
			if($arOptParams['TYPE'] != 'CUSTOM'){
				$arCurOptionValues[$opt] = COption::GetOptionString($module_id, $opt, $arOptParams['DEFAULT']);
		
			if(in_array($arOptParams['TYPE'], array('MSELECT')))
				$arCurOptionValues[$opt] = unserialize($arCurOptionValues[$opt]);
			}
		}
	//$text=print_r($arCurOptionValues, 1);$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/modules/mixart.symmetron/classes/zlog.txt", "w");fwrite($fp, $text);fclose($fp);
		//error_log($arCurOptionValues, 0);
		// $text.=print_r($event, 1)."\n";
		// $text.=print_r($lid, 1)."\n";
		// $text.=print_r($arFields, 1)."\n";
		// $text.=print_r($messageId, 1)."\n";
		// $text.=print_r($files, 1)."\n";
		// $text.=print_r($languageId, 1)."\n";
		
		// $fp = fopen($_SERVER["DOCUMENT_ROOT"]."/zlog.txt", "w");fwrite($fp, $text);fclose($fp);

		$_R['number']=$arFields["ORDER_ID"];
		 require($_SERVER["DOCUMENT_ROOT"]."/local/modules/mixart.symmetron/classes/property.orders.new.php"); 
		//Отправляем письмо
		$title = "Заказ с сайта № ".$_R['number'];
	    $message = "Поступил новый заказ № ".$_R['number'].", подробности во вложении.<br><br>
		Заказчик: ".$arFields["ORDER_USER"]."<br>
		E-mail: ".$orderEMAIL."<br>
		Телефон: ".$tel."<br>
		Для компании: ".$orderCOMPANY."<br>
		ИНН: ".$orderINN."<br>
		Сумма заказа: ".$arFields["PRICE"]."<br><br>
		Комментарий: ".$orderCOMMENT."<br>
		Тип доставки: ".$typedelivery."<br>
		Адрес склада: ".$skladaddress."<br>
		Регион: ".$orderREGION."<br>
		Адрес: ".$orderADDRESS_CITY." ".$address."<br>
		";
		
		$http = new \Bitrix\Main\Web\HttpClient();
		$host=$arCurOptionValues[TEST_0];//"b24dev.symmetron.ru";
		$user=$arCurOptionValues[TEST_1];//"3349";
		$tokenID=$arCurOptionValues[TEST_2];//"cnjj4rv3gcrfhrb4";
	

		$sTITLE="Заказ с сайта №";
		$sNumber=$_R['number'];
		$sLAST_NAME=$arFields["ORDER_USER"];
		
		$sPhone=$tel;
		$sEmail=$orderEMAIL;
	
		$sCOMPANY_TITLE=$orderCOMPANY;
		$sINN=$orderINN;
		$sSumma=$arFields["PRICE"];
	
		$sComment="
		Комментарий: ".$orderCOMMENT."<br>
		Тип доставки: ".$typedelivery."<br>
		Адрес склада: ".$skladaddress."<br>
		Регион: ".$orderREGION."<br>
		Адрес: ".$orderADDRESS_CITY." ".$address."<br>";
	
		//Форматируем телефон и почту для сохранения
		$arPhone = (!empty($sPhone)) ? ['VALUE' => $sPhone, 'VALUE_TYPE' => 'MOBILE'] : [];
		$arEmail = (!empty($sEmail)) ? ['VALUE' => $sEmail, 'VALUE_TYPE' => 'WORK'] : [];
		
		// $filename из property.orders.new
		$data = file_get_contents($filename);
		$base64 = base64_encode($data);
		$name = urldecode(basename($filename));
		/*
		$json = $http->post(
		'https://'.$host.'/rest/'.$user.'/'.$tokenID.'/crm.lead.add/',
		[
         'fields' => [
				'TITLE' => $sTITLE.$sNumber,
				'LAST_NAME' => $sLAST_NAME,
				'PHONE' => [
					'n0'=>$arPhone
					],
				'EMAIL' => [
					'n0'=>$arEmail
					],
				'COMMENTS' => $sComment,
				'COMPANY_TITLE' => $sCOMPANY_TITLE,
				'UF_CRM_1576501434424' => $sINN,
				'UF_CRM_1584516757940' => $sSumma,
				'CURRENCY_ID' => 'RUB',
				'SOURCE_ID' => 'WEB',
				'UF_CRM_1576502174' => ['fileData' => [$name, $base64]],
		 ],
		]
		);
		$result = \Bitrix\Main\Web\Json::decode($json);
		*/
		//echo "<pre>";print_r($result);echo "</pre>";
		
		
		//  URL в переменной $queryUrl
		$queryUrl = 'https://'.$host.'/rest/'.$user.'/'.$tokenID.'/crm.lead.add/';
		// параметры лида $queryData
		$queryData = http_build_query(array(
		'fields' => array(
				'TITLE' => $sTITLE.$sNumber,
				'LAST_NAME' => $sLAST_NAME,
				'PHONE' => array('n0'=>$arPhone	),
				'EMAIL' => array('n0'=>$arEmail	),
				'COMMENTS' => $sComment,
				'COMPANY_TITLE' => $sCOMPANY_TITLE,
				'UF_CRM_1576501434424' => $sINN,
				'UF_CRM_1584516757940' => $sSumma,
				'CURRENCY_ID' => 'RUB',
				'SOURCE_ID' => 'WEB',
				'UF_CRM_1576502174' => array('fileData' => array($name, $base64)),
		 ),

 
		));
		
		// обращаемся к Битрикс24 при помощи функции curl_exec
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $queryUrl,
			CURLOPT_POSTFIELDS => $queryData,
		));
		$result = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($result, 1);
		//echo "<pre>";print_r($result);echo "</pre>";

		
		if(array_key_exists('error', $result)){      
			CEventLog::Add(array(
				"SEVERITY" => "SECURITY",
				"AUDIT_TYPE_ID" => "LID",
				"MODULE_ID" => "mixart.symmetron",
				"ITEM_ID" => 1,
				"DESCRIPTION" => "Модуль mixart.symmetron не добавил лид из-за ошибки:".$result['error_description']."",
			));
			//die;
		}else{	
			CEventLog::Add(array(
				"SEVERITY" => "SECURITY",
				"AUDIT_TYPE_ID" => "LID",
				"MODULE_ID" => "mixart.symmetron",
				"ITEM_ID" => 1,
				"DESCRIPTION" => "Модуль mixart.symmetron добавил лид.",
			));
		}
		
		
		//$toemail   = "moscow@symmetron.ru, kristina.zagorelskay@symmetron.ru, crm24@symmetron.ru";
		//$toemail   = "website@symmetron.ru";
		$toemail   = "e-maksss@mail.ru";
		$replyto   = "website@symmetron.ru";
		//$fromemail = "wwwstat@symmetron.ru";
		$fromemail = "website@symmetron.ru";
		$filesend = Array("/upload/mixart.symmetron/order.".$_R['number'].".xlsx");
		self::sendMessageMail($toemail,$replyto, $fromemail, $title, $message, $filesend);
		unlink($_SERVER["DOCUMENT_ROOT"]."/upload/mixart.symmetron/order.".$_R['number'].".xlsx");
      }

	}
	public static function sendMessageMail($to,$replyto, $from, $title, $html, $filename){


		$EOL = "\r\n"; // ограничитель строк, некоторые почтовые сервера требуют \n - подобрать опытным путём 
		$boundary     = "--".md5(uniqid(time()));  // любая строка, которой не будет ниже в потоке данных.  
		$headers    = "MIME-Version: 1.0;$EOL";   
		$headers   .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";  
		$headers   .= "From: $from"."\r\n";
		//$headers   .= "Reply-To: $replyto"."\r\n";
		//$headers .= "Bcc: $replyto"."\r\n";
		$headers .= "cc: $replyto"."\r\n";

		$multipart  = "--$boundary$EOL";   
		$multipart .= "Content-Type: text/html; charset=utf-8$EOL";   
		$multipart .= "Content-Transfer-Encoding: base64$EOL";
		$multipart .= $EOL; // раздел между заголовками и телом html-части 
		$multipart .= chunk_split(base64_encode($html));   

		foreach ($filename as $k=>$v){
			$path = $_SERVER['DOCUMENT_ROOT'].$v;
			if ($path) {
				$fp = fopen($path,"rb");   
				if (!$fp){ print "Cannot open file:".$path; exit(); }   
				$file = fread($fp, filesize($path));   
				fclose($fp);   
			}  
			$name = basename($v); // в этой переменной надо сформировать имя файла (без всякого пути)  

			$multipart .= "$EOL--$boundary$EOL";
			$multipart .= "Content-Type: application/vnd.ms-excel$EOL";   
			$multipart .= "Content-Transfer-Encoding: base64$EOL";   
			$multipart .= "Content-Disposition: attachment; filename=\"$name\"$EOL";   
			$multipart .= $EOL; // раздел между заголовками и телом прикрепленного файла 
			$multipart .= chunk_split(base64_encode($file));   
		}
		$multipart .= "$EOL--$boundary--$EOL";   

		if(!mail($to, $title, $multipart, $headers)){
			CEventLog::Add(array(
				"SEVERITY" => "SECURITY",
				"AUDIT_TYPE_ID" => "email",
				"MODULE_ID" => "mixart.symmetron",
				"ITEM_ID" => 1,
				"DESCRIPTION" => "Модуль mixart.symmetron письмо не отправил на $to с $from",
			));
			return False; //если не письмо не отправлено
		}else { //// если письмо отправлено
			CEventLog::Add(array(
				"SEVERITY" => "SECURITY",
				"AUDIT_TYPE_ID" => "email",
				"MODULE_ID" => "mixart.symmetron",
				"ITEM_ID" => 1,
				"DESCRIPTION" => "Модуль mixart.symmetron отправил письмо на адрес $to с $from",
			));
		return True;}  
		
	}

}