<?
use Bitrix\Sale;

CModule::IncludeModule('sale');
/** Подключение PHPExcel */
require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/PHPExcel.php';

//function dump($data){$text=print_r($data, 1);$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/zlog.txt", "w");fwrite($fp, $text);fclose($fp);}
/***************** Производители ******************/
$arFilter = array('IBLOCK_ID' => 11);
$res = CIBlockElement::GetList(array('NAME' => 'asc'), $arFilter, false, false, array('ID','NAME','CODE'));
while($el = $res->GetNext()){
	$el['NAME'] = str_replace("&quot;",'"',$el['NAME']);
	$el['NAME'] = str_replace("&lt;",'<',$el['NAME']);
	$el['NAME'] = str_replace("&gt;",'>',$el['NAME']);
	$arManufacturers[$el['ID']]['NAME'] = $el['NAME'];
	$arManufacturers[$el['ID']]['CODE'] = $el['CODE'];
}
/***************** Доставка       ******************/
$arFilter = array("LID" => "s1",);
$res = CSaleDelivery::GetList(array('NAME' => 'asc'), $arFilter, false, false, array('ID','NAME'));
while($el = $res->Fetch()){	$arDelivery[$el['ID']] = $el['NAME']; }

$domen="https://www.symmetron.ru/";
$domen2="https://www.symmetron.ru";
//$folder="output";
//$filemacros="Макрос.1.xlsm";
if(CModule::IncludeModule("iblock") and CModule::IncludeModule("sale")) {

	$filename=$_SERVER['DOCUMENT_ROOT']."/upload/mixart.symmetron/order.".$_R['number'].".xlsx";

	// Загрузка шаблона
	$template = $_SERVER["DOCUMENT_ROOT"]."/local/modules/mixart.symmetron/classes/template.orders.xlsx";
	$objReaderOrder = PHPExcel_IOFactory::createReader('Excel2007');
	$PHPExcelOrder  = $objReaderOrder->load($template); 

	// Стили ячеек для ссылок
	$styleArrayUri= array('font'=> array('name'=>'Roboto','size'=>'10','bold'=> false,'italic'=> false,'strike'=> false,'color' => array('rgb' => '0070c0')),);
	
	$styleArrayCenter= array(
	'alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
	'font'=> array('bold'=> true,),
	);

	$styleArrayOrder= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),);
	$styleArrayOrderCenter= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),);
	
	$styleArrayItem= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),);
	$styleArrayItemCenter= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),);

	$styleArrayOrderBorder= array('borders'=>array('bottom'=>array('style'=>PHPExcel_Style_Border::BORDER_THIN,'color'=>array('rgb'=>'000000')),),);

	$styleArrayOrderBorderRight= array('borders'=>array('right'=>array('style'=>PHPExcel_Style_Border::BORDER_HAIR,'color'=>array('rgb'=>'000000')),),);

	$parameters = [
		'order'  => ["ID" => "DESC"],
		'filter' => ["ID" => $_R['number']],
	];

	$dbRes = \Bitrix\Sale\Order::getList($parameters);
	$i=1;
	$irow=3;
	while ($order = $dbRes->fetch()){
		$oborder = \Bitrix\Sale\Order::load($order['ID']);
		$propertyCollection = $oborder->getPropertyCollection();

		$userid=$oborder->getUserId();
		$rsUser = CUser::GetByID($userid);
		$arUser = $rsUser->Fetch();
	
		$strUser=$arUser["LOGIN"];
		if($arUser["NAME"]!="" OR $arUser["LAST_NAME"]!=""){ $strUser=$arUser["NAME"]." ".$arUser["LAST_NAME"]; }

		$orderCOMPANY="";
		$orderINN="";
		$orderADDRESS_CITY="";
		$orderCONTACT_PERSON="";
		$orderPHONE="";
		$orderEMAIL="";
		$orderCOMMENT="";
		$orderADDRESS_FROM_DADATA="";
		$orderADDRESS_INDEX="";
		$orderADDRESS_COUNTRY="";
		$orderREGION="";
		$orderADDRESS_STREET="";
		$orderADDRESS_BUILDING="";
		$orderADDRESS_BLOCK="";
		$orderADDRESS_ROOM="";
		$orderUF_DOB="";

		foreach ($propertyCollection as $Prop){

			if($Prop->getField('CODE') == 'COMPANY') {$orderCOMPANY= $Prop->getValue();}
			if($Prop->getField('CODE') == 'INN') {$orderINN= $Prop->getValue();}
	
			if($Prop->getField('CODE') == 'CONTACT_PERSON') {$orderCONTACT_PERSON= $Prop->getValue();}
			if($Prop->getField('CODE') == 'PHONE') {$orderPHONE= $Prop->getValue();}
			if($Prop->getField('CODE') == 'EMAIL') {$orderEMAIL= $Prop->getValue();}
			if($Prop->getField('CODE') == 'COMMENT') {$orderCOMMENT= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_FROM_DADATA') {$orderADDRESS_FROM_DADATA= $Prop->getValue();}
	
			if($Prop->getField('CODE') == 'ADDRESS_INDEX') {$orderADDRESS_INDEX= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_COUNTRY') {$orderADDRESS_COUNTRY= $Prop->getValue();}
			if($Prop->getField('CODE') == 'REGION') {$orderREGION= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_CITY') {$orderADDRESS_CITY= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_STREET') {$orderADDRESS_STREET= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_BUILDING') {$orderADDRESS_BUILDING= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_BLOCK') {$orderADDRESS_BLOCK= $Prop->getValue();}
			if($Prop->getField('CODE') == 'ADDRESS_ROOM') {$orderADDRESS_ROOM= $Prop->getValue();}

			if($Prop->getField('CODE') == 'UF_DOB') {$orderUF_DOB= $Prop->getValue();}

		}
	
	$arDeliv = CSaleDelivery::GetByID($order["DELIVERY_ID"]);
	
	$rsDeliveryOrder = Bitrix\Sale\Internals\ShipmentTable::getList(["filter" => ["ORDER_ID" => $order['ID']]]);
	$arDeliveryOrder = [];
	while($deliveryOrder = $rsDeliveryOrder->Fetch()){$arDeliveryOrder[] = $deliveryOrder["ID"];}
	if(!empty($arDeliveryOrder)){$arDelivertOrderEx = Bitrix\Sale\Internals\ShipmentExtraServiceTable::getList(["filter" => ["SHIPMENT_ID" => $arDeliveryOrder]])->Fetch();
		if(!empty($arDelivertOrderEx)){$arResult = CCatalogStore::GetList([],['ACTIVE' => 'Y','ID'=>$arDelivertOrderEx["VALUE"]],false,false)->Fetch();
			$sklad=" ( ".$arResult["TITLE"]." )";
			$skladaddress="".$arResult["ADDRESS"]."";
		}
	}

	if ($arDelivery[$order["DELIVERY_ID"]]!="Самовывоз"){$sklad="";}

	// Добавляем данные
	$uri = "https://dadata.ru/find/party/".$orderINN."/";
	$PHPExcelOrder->getActiveSheet()->getCell('G'.$irow)->getHyperlink()->setUrl($uri);
	$PHPExcelOrder->getActiveSheet()->getStyle('G'.$irow)->applyFromArray($styleArrayUri);

	$strADDRESS_ROOM="";
	$strADDRESS_BLOCK="";
	$strADDRESS_BUILDING="";
	$strADDRESS_STREET="";
	$strUF_DOB="";
	
	if ($orderADDRESS_ROOM!=""){$strADDRESS_ROOM="офис ".$orderADDRESS_ROOM;}
	if ($orderADDRESS_BLOCK!=""){$strADDRESS_BLOCK="корп. ".$orderADDRESS_BLOCK;}
	if ($orderADDRESS_BUILDING!=""){$strADDRESS_BUILDING="дом ".$orderADDRESS_BUILDING;}
	if ($orderADDRESS_STREET!=""){$strADDRESS_STREET="".$orderADDRESS_STREET;}

	$address=$strADDRESS_STREET." ".$strADDRESS_BUILDING." ".$strADDRESS_BLOCK." ".$strADDRESS_ROOM." ";

	if ($orderUF_DOB!=""){$strUF_DOB="доб. ".$orderUF_DOB;}
	
	$tel=$orderPHONE." ".$strUF_DOB;
	
	$typedelivery=$arDelivery[$order["DELIVERY_ID"]].$sklad;
	
	$PHPExcelOrder->setActiveSheetIndex(0)
	->setCellValue('A'.$irow, $i)
	->setCellValue('B'.$irow, date("d.m.Y", strtotime($oborder->getDateInsert())))
	->setCellValue('C'.$irow, $oborder->getId())
	->setCellValue('D'.$irow, $strUser)
	->setCellValue('E'.$irow, $orderCOMPANY)
	->setCellValue('F'.$irow, $orderINN)
	->setCellValue('G'.$irow, $orderCOMPANY)
	->setCellValue('H'.$irow, '')
	->setCellValue('I'.$irow, '')
	->setCellValue('J'.$irow, '')
	->setCellValue('K'.$irow, "")
	->setCellValue('L'.$irow, '')
	->setCellValue('M'.$irow, '')
	->setCellValue('N'.$irow, $arBasketItemsName[$order['ID']]["NAME"])
	->setCellValue('O'.$irow, '')
	->setCellValue('P'.$irow, $oborder->getPrice())
	->setCellValue('Q'.$irow, $orderCOMMENT)
	->setCellValue('R'.$irow, $tel)
	->setCellValue('S'.$irow, $orderCONTACT_PERSON)
	->setCellValue('T'.$irow, $orderEMAIL)

	->setCellValue('U'.$irow, $orderADDRESS_INDEX)
	->setCellValue('V'.$irow, $orderREGION)
	->setCellValue('W'.$irow, $orderADDRESS_CITY)
	->setCellValue('X'.$irow, $address)
	->setCellValue('Y'.$irow, $orderADDRESS_FROM_DADATA)
	->setCellValue('Z'.$irow, $typedelivery);


	$PHPExcelOrder->getActiveSheet()->getStyle("A".$irow.":C".$irow."")->applyFromArray($styleArrayOrderCenter);
	$PHPExcelOrder->getActiveSheet()->getStyle("D".$irow.":H".$irow."")->applyFromArray($styleArrayOrder);


	$PHPExcelOrder->getActiveSheet()->getStyle("P".$irow)->applyFromArray($styleArrayCenter);
	$PHPExcelOrder->getActiveSheet()->getStyle("Q".$irow.":Z".$irow."")->applyFromArray($styleArrayOrder);

	$PHPExcelOrder->getActiveSheet()->getRowDimension($irow)->setRowHeight(21);
	$PHPExcelOrder->getActiveSheet()->getStyle("Q".$irow.":Z".$irow."")->getAlignment()->setWrapText(true);

	$start=$irow;
	$dbBasketItems = CSaleBasket::GetList(array("NAME" => "ASC"),array("ORDER_ID" => $order['ID']),false,false);
	$rowsItems=$dbBasketItems->SelectedRowsCount();
	$orderList="<table border=0 cellspacing=5 cellpadding=5><tr><td style='width:45px;text-align:center'>№<br>п/п</td><td style='text-align:center'>Код товара у производителя</td><td style='text-align:center'>Название</td><td style='text-align:center'>Кол-во</td><td style='text-align:center'>Цена за ед.</td><td style='text-align:center'>Стоимость</td><td style='text-align:center'>Тип</td></tr>";
	$n=1;
	while ($arItems = $dbBasketItems->Fetch()){
		$typeoforeder="";
		$db_res = CSaleBasket::GetPropsList(array("SORT" => "ASC","NAME" => "ASC"),array("BASKET_ID" =>$arItems["ID"]));
		$rowsProps=$db_res->SelectedRowsCount();
		if ($rowsProps>0){
			while ($ar_res = $db_res->Fetch()){
				if($ar_res["NAME"] == "Запрос" AND $ar_res["VALUE"]=="QUANTITY"){$typeoforeder="Большего количества";}
				if($ar_res["NAME"] == "Запрос" AND $ar_res["VALUE"]=="MULTIPLICITY"){$typeoforeder="Не кратных остатков";$arItems["QUANTITY"]=0;}
				if($ar_res["NAME"] == "Запрос" AND $ar_res["VALUE"]=="MISSING"){$typeoforeder="Условия поставки";}
				if($ar_res["NAME"] == "Образец" AND $ar_res["VALUE"]=="Y"){$typeoforeder="Образец";}
			}
		}else{
			$typeoforeder="Покупка";
		}
		$arFilter = Array("ID"=>$arItems["PRODUCT_ID"]);
		$res = CIBlockElement::GetList(false, $arFilter, false, false, Array("*"));
		$ob = $res->GetNextElement();
		// Поля товара 
		$arFieldsNew = $ob->GetFields();
		// Все свойства товара
		$arProps = $ob->GetProperties();

		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('H'.$irow, $arProps["ITEMID"]["VALUE"]);
		// Код товара у производителя
		$uri=$domen2.$arFieldsNew['DETAIL_PAGE_URL'];
		$orderList.="<tr><td>$n </td><td><a href='".$uri."' target='_blank'> ".$arProps["ARTICLE"]["VALUE"]."</a></td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('I'.$irow, $arProps["ARTICLE"]["VALUE"]);
		$PHPExcelOrder->getActiveSheet(0)->getCell('I'.$irow)->getHyperlink()->setUrl($uri);
		$PHPExcelOrder->getActiveSheet(0)->getStyle('I'.$irow)->applyFromArray($styleArrayUri);
		
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('J'.$irow, $arManufacturers[$arProps["PRODUCERID"]["VALUE"]]['NAME']);
		// Название
		$orderList.="<td>".$arItems["NAME"]."</td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('K'.$irow, $arItems["NAME"]);
		// Кол-во
		$orderList.="<td>".$arItems["QUANTITY"]."</td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('L'.$irow, $arItems["QUANTITY"]);
		// Цена за ед.
		$arItems["PRICE"] = substr($arItems["PRICE"], 0, -2);
		$orderList.="<td>".$arItems["PRICE"]."</td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('M'.$irow, $arItems["PRICE"]);
		// Стоимость
		$orderList.="<td>".$arItems["QUANTITY"]*$arItems["PRICE"]."</td>";
		// Тип
		$orderList.="<td>".$typeoforeder."</td></tr>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('N'.$irow, $typeoforeder);
		$PHPExcelOrder->setActiveSheetIndex(0)->getStyle('N'.$irow)->getAlignment()->setWrapText(true);

		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('O'.$irow, $arItems["QUANTITY"]*$arItems["PRICE"]);

		$PHPExcelOrder->getActiveSheet(0)->getRowDimension($irow)->setRowHeight(21);

		$PHPExcelOrder->getActiveSheet(0)->getStyle("H".$irow)->applyFromArray($styleArrayItemCenter);
		$PHPExcelOrder->getActiveSheet(0)->getStyle("I".$irow.":K".$irow."")->applyFromArray($styleArrayItem);
		$PHPExcelOrder->getActiveSheet(0)->getStyle("L".$irow.":O".$irow."")->applyFromArray($styleArrayItemCenter);

		$irow++;
		$n++;
	}
	if ($rowsItems>1){ 
		$finish=$irow-1;

		$PHPExcelOrder->getActiveSheet(0)->mergeCells("A".$start.":A".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("B".$start.":B".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("C".$start.":C".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("D".$start.":D".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("E".$start.":E".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("F".$start.":F".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("G".$start.":G".$finish."");

		$PHPExcelOrder->getActiveSheet(0)->mergeCells("P".$start.":P".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("Q".$start.":Q".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("R".$start.":R".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("S".$start.":S".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("T".$start.":T".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("U".$start.":U".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("V".$start.":V".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("W".$start.":W".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("X".$start.":X".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("Y".$start.":Y".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("Z".$start.":Z".$finish."");
	}

	$i++;
	if ($rowsItems==0){$irow++;}
	$line=$irow-1;
	$PHPExcelOrder->getActiveSheet()->getStyle("A".$line.":Z".$line."")->applyFromArray($styleArrayOrderBorder);

	}

	$orderList.="</table>";
	$arFields["ORDER_LIST"]=$orderList;//echo $orderList;

	// Сохраняем файл Excel 2007
	$objWriter = PHPExcel_IOFactory::createWriter($PHPExcelOrder, 'Excel2007');
	$objWriter->save($filename);
	$PHPExcelOrder->disconnectWorksheets();
	unset($PHPExcelOrder);

}
?>
