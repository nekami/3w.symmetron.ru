<?
IncludeModuleLangFile(__FILE__);

if (class_exists("mixart_symmetron")) return;




Class mixart_symmetron extends CModule {
	
	var $MODULE_ID = "mixart.symmetron";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";
	
	function __construct() {
        $arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = BREAKNOTIFICATION_VERSION;
            $this->MODULE_VERSION_DATE = BREAKNOTIFICATION_VERSION_DATE;
        }
		
        $this->MODULE_NAME = GetMessage("MIXART_SYMM_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MIXART_SYMM_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("MIXART_SYMM_PARTNER_NAME");
        $this->PARTNER_URI = "";
    }

    function DoInstall() {
		
        if (!IsModuleInstalled($this->MODULE_ID)) {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        }
        return true;
    }

    function DoUninstall() {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        return true;
    }

    function InstallDB() {
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function UnInstallDB() {
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function InstallEvents() {
		RegisterModuleDependences("main", "OnBeforeEventAdd", $this->MODULE_ID, 'CMailSymmetron', 'updateMail');

        return true;
    }

    function UnInstallEvents() {
        UnRegisterModuleDependences("main", "OnBeforeEventAdd", $this->MODULE_ID, 'CMailSymmetron', 'updateMail');
        return true;
    }

    function InstallFiles() {
		mkdir($_SERVER["DOCUMENT_ROOT"]."/upload/mixart.symmetron", 0775, true);
		chmod($_SERVER["DOCUMENT_ROOT"]."/upload/mixart.symmetron", 0775);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/mixart.symmetron/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin",true,true);
        return true;
    }

    function UnInstallFiles() {
		DeleteDirFilesEx("/bitrix/admin/mixart.symmetron.email.php");
        return true;
    }
	
}

?>