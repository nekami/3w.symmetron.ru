<?
\Bitrix\Main\Loader::registerAutoLoadClasses(
    "mcart.basketsymmetron",
    array(
        "CBasketSymmetron" => "classes/CBasketSymmetron.php",
        "\\Mcart\\BasketSymmetron\\SymmetronOrder" => "lib/SymmetronOrder.php",
        "\\Mcart\\BasketSymmetron\\SymmetronDeclination" => "lib/SymmetronDeclination.php",
        "\\Mcart\\BasketSymmetron\\SymmetronBasket" => "lib/SymmetronBasket.php",
        "\\Mcart\\BasketSymmetron\\ElementPropertyTable" => "lib/elementproperty.php",
    )
);  