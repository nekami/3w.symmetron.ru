<?php

class CBasketSymmetron {

    public function getIdExistingElementFromBasket($productId){
        $resBasketItems = CSaleBasket::GetList(
            array(),
            array(
                "PRODUCT_ID" => $productId,
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                //"LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            false,
            false,
            array("ID", "PRODUCT_ID")
        );

        $arBasketItems = array();
        while($arBasketItem = $resBasketItems->Fetch()){
            $arBasketItems[$arBasketItem["ID"]] = $arBasketItem["ID"];
        }

        $idInBasket = 0;
        if(!empty($arBasketItems)){
            $rsProps = CSaleBasket::GetPropsList(
                array(),
                array("BASKET_ID" => $arBasketItems)
            );

            while ($arProp = $rsProps->Fetch())
            {
                if($arProp["CODE"] == "HIDDEN" && $arProp["VALUE"] == "Y")
                    unset($arBasketItems[$arProp["BASKET_ID"]]);
                if($arProp["CODE"] == "EXAMPLE" && $arProp["VALUE"] == "Y")
                    unset($arBasketItems[$arProp["BASKET_ID"]]);
                if($arProp["CODE"] == "REQUEST" && !empty($arProp["VALUE"]))
                    unset($arBasketItems[$arProp["BASKET_ID"]]);
            }
        }

        if(count($arBasketItems) > 0)
            $idInBasket = reset($arBasketItems);

        return $idInBasket;
    }

    public function updateProductPrice(){

        //BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.section/");
        //BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.element/");
        //BXClearCache(true, "/" . SITE_ID . "/mcart/catalog.quantity/");
        //$arFields["PRICE"] = 1;

        /*if(Cmodule::IncludeModule("sale") && Cmodule::IncludeModule("catalog") && Cmodule::IncludeModule("currency") && !empty($_POST["basketAction"])) {

            $ID = 0;
            $quantity = -1;
            foreach ($_POST["basket"] as $key => $quantity){
                $arActive = explode("_", $key);
                if(trim($arActive[0]) == "QUANTITY") {
                    $ID = intval($arActive[1]);
                    $quantity = intval($quantity);
                }
                break;
            }


            if($ID > 0 && $quantity > -1) {

                $arItem = CSaleBasket::GetList(
                    array("NAME" => "ASC", "ID" => "ASC"),
                    array("ID" => $ID),
                    false,
                    array("ID", "CURRENCY", "QUANTITY", "PRODUCT_ID")
                //array("ID", "PRICE")
                )->Fetch();

                if (!empty($arItem["PRODUCT_ID"])) {
                    $arProducts = CCatalogProduct::GetList(
                        array("ID" => "DESC"),
                        array("ID" => $arItem["PRODUCT_ID"]),
                        false,
                        false,
                        array("QUANTITY")
                    )->Fetch();

                    $rsProps = CSaleBasket::GetPropsList(
                        array(),
                        array("BASKET_ID" => $arItem["ID"], "CODE" => "REQUEST")
                    );

                    $price = 0;
                    if (!$rsProps->Fetch())
                    {
                        if ($quantity <= $arProducts["QUANTITY"]) {

                            //$baseCurrency = CCurrency::GetBaseCurrency();
                            $baseCurrency = $arItem["CURRENCY"];
                            $productQuantity = $quantity;

                            $db_res = CPrice::GetListEx(
                                array("QUANTITY_FROM" => "ASC"),
                                array(
                                    "PRODUCT_ID" => $arItem["PRODUCT_ID"],
                                    //"CATALOG_GROUP_ID" => 1, //ID типа цены
                                    //"CURRENCY" => "RUB" // Валюта
                                )
                            );

                            $db_result_lang = CCurrencyLang::GetByID($baseCurrency, LANGUAGE_ID);


                            while ($r = $db_res->Fetch()) {
                                if (($r["QUANTITY_FROM"] <= $productQuantity && $productQuantity <= $r["QUANTITY_TO"]) || ($r["QUANTITY_FROM"] <= $productQuantity && empty($r["QUANTITY_TO"]))) {
                                    if ($baseCurrency == $r["CURRENCY"]) {
                                        $price = $r["PRICE"];
                                    } else {
                                        if (empty($db_result_lang["DECIMALS"])) {
                                            $db_result_lang["DECIMALS"] = 2;
                                        }

                                        $price = round(CCurrencyRates::ConvertCurrency($r["PRICE"], $r["CURRENCY"],
                                            $baseCurrency), $db_result_lang["DECIMALS"]);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    CSaleBasket::Update($ID, array("PRICE" => $price));
                    //$arFields["PRICE"] = $price;
                }
            }

        }*/


    }


    public function getInfoMeasureAndPacking($arItemsIDs, $iblockId){

        if(empty($arItemsIDs) || empty($iblockId)){
            return array(
                "PACKING" => array(),
                "MEASURES"=> array(),
                "ERROR"   => "input parameters is empty"
            );
        }

        $rsPacEnumValues = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"UPAK"));
        $arPacEnumValues = [];
        while($arPacEnumValue = $rsPacEnumValues->Fetch())
        {
            $arPacEnumValues[$arPacEnumValue["ID"]] = $arPacEnumValue;
        }

        $rsProps = Bitrix\Iblock\PropertyTable::getList(array(
            "filter" => array("IBLOCK_ID" => $iblockId, "CODE" => array("SYMPACKAGE", "UPAK")),
            "select" => array("ID", "CODE")
        ));

        $arPropsIDs = [];
        while ($arProp = $rsProps->Fetch()) {
            $arPropsIDs[$arProp["ID"]] = $arProp["CODE"];
        }

        $arPropValuesElems = [];
        if(!empty($arPropsIDs)) {
            if(Cmodule::IncludeModule("mcart.basketsymmetron")) {
                $rsPropValues = Mcart\BasketSymmetron\ElementPropertyTable::getList(array(
                    "filter" => array(
                        "IBLOCK_PROPERTY_ID" => array_keys($arPropsIDs),
                        "IBLOCK_ELEMENT_ID" => $arItemsIDs
                    ),
                    "select" => array("ID", "VALUE", "IBLOCK_ELEMENT_ID", "IBLOCK_PROPERTY_ID")
                ));

                while ($arPropValue = $rsPropValues->Fetch()) {
                    if ($arPropsIDs[$arPropValue["IBLOCK_PROPERTY_ID"]] == "UPAK") {
                        $arPropValuesElems[$arPropValue["IBLOCK_ELEMENT_ID"]][$arPropsIDs[$arPropValue["IBLOCK_PROPERTY_ID"]]]
                            = $arPacEnumValues[$arPropValue["VALUE"]];
                    } else {
                        $arPropValuesElems[$arPropValue["IBLOCK_ELEMENT_ID"]][$arPropsIDs[$arPropValue["IBLOCK_PROPERTY_ID"]]]
                            = $arPropValue["VALUE"];
                    }
                }
            }
        }

        //measures
        $result = \Bitrix\Catalog\ProductTable::getList([
            "filter" => ["ID" => $arItemsIDs],
            "select" => ["ID", "MEASURE"],
        ]);

        $arItemsMeasure = [];
        while ($row = $result->fetch())
        {
            $arItemsMeasure[$row["ID"]] = $row["MEASURE"];
        }

        $arMeasure = [];

        $resMeasure = \CCatalogMeasure::getList(array(), array("ID" => $arItemsMeasure), false);
        while ($row = $resMeasure->fetch()) {
            $arMeasure[$row["ID"]] = $row;
        }

        foreach ($arItemsMeasure as $key => $measureId){
            $arItemsMeasure[$key] = $arMeasure[$measureId];
        }


        return array(
            "PACKING" => $arPropValuesElems,
            "MEASURES"    => $arItemsMeasure
        );
    }


    public function changeTemplate($siteId, &$eventName, &$fields){
        $arAccess = ["BUY", "REQUEST", "EXAMPLE"];
        $part = htmlspecialchars(trim($_GET["part"]));
        if(!empty($part) && in_array($part, $arAccess))
            $eventName = $eventName . "_" . $part;
    }


    public function deleteHiddenPositionInOrder(&$arFields) {

        foreach ($arFields["BASKET_ITEMS"] as $arItem){
            if(!empty($arItem["PROPS"]["REQUEST"]) && !empty($arItem["PROPS"]["PARENT_PRODUCT"]["VALUE"])){
                CSaleBasket::delete(intval($arItem["PROPS"]["PARENT_PRODUCT"]["VALUE"]));
            }
        }

    }

    public function agentForNameDescription($limit, $currentId){

        if($currentId == 0 || !CModule::IncludeModule("iblock")){
            return "CBasketSymmetron::agentForNameDescription($limit, $currentId);";
        }

        $resId = CIBlock::GetList(
            [], ["CODE" => "pbd"]
        );

        if($arIb = $resId->Fetch())
            $ibID = $arIb["ID"];
        else
            return "CBasketSymmetron::agentForNameDescription($limit, $currentId);";

        $res = \Bitrix\Iblock\ElementTable::getList([
            "order" => ["ID" => "asc"],
            "filter" => ["IBLOCK_ID" => $ibID, "ACTIVE" => "Y", "ID" > $currentId],
            "select" => ["ID", "NAME"],
            "limit" => $limit
        ]);

        $count = 0;
        $endID = 0;
        while ($r = $res->fetch()) {
            $count++;
            if($count == $limit){
                $endID = $r["ID"];
            }

            CIBlockElement::SetPropertyValuesEx($r["ID"], $ibID, array("DESCRIPTION" => $r["NAME"]));
        };

        return "CBasketSymmetron::agentForNameDescription($limit, $endID);";

    }

    public function agentForFixUnits($iblockId){

        if(Cmodule::IncludeModule("mcart.basketsymmetron") && $iblockId > 0){

            $properties = CIBlockProperty::GetList(["sort"=>"asc", "name"=>"asc"], ["ACTIVE"=>"Y",
                "IBLOCK_ID"=>$iblockId,
                "USER_TYPE" => "mcart_property_with_measure_units"
            ]);
            $arPropsIDs = [];
            while ($propFields = $properties->Fetch())
            {
                $arPropsIDs[] = $propFields["ID"];
            }

            if(!empty($arPropsIDs)){
                $propElements = \Mcart\BasketSymmetron\ElementPropertyTable::getList([
                    "filter" => [
                        "IBLOCK_PROPERTY_ID" => $arPropsIDs,
                    ]
                ]);
                while($arProp = $propElements->Fetch()){
                    $arVal = explode("|", $arProp["VALUE"]);

                    if(count($arVal) == 2){
                        if((double)$arVal[0] != $arProp["VALUE_NUM"]){
                            \Mcart\BasketSymmetron\ElementPropertyTable::update($arProp["ID"], ["VALUE_NUM" => (double)$arVal[0]]);
                        }
                    }
                }
            }
        }

        return "CBasketSymmetron::agentForFixUnits($iblockId);";
    }

    public function searchTitleNew($arFields){
        if($arFields["PARAM1"] === "catalog" && (int)$arFields["ITEM_ID"] > 0){
            $arSelect = ["NAME", "PROPERTY_ARTICLE", "PROPERTY_SERIES", "PROPERTY_DESCRIPTION"];
            $arElem = CIBlockElement::GetList([], ["ID" => (int)$arFields["ITEM_ID"]], false, false, $arSelect)->Fetch();

            $arSearch = [
                $arElem["NAME"],
                $arElem["PROPERTY_ARTICLE_VALUE"],
                $arElem["PROPERTY_SERIES_VALUE"],
                $arElem["PROPERTY_DESCRIPTION_VALUE"]
            ];

            $arFields["TITLE"] = implode($arSearch, " ");
        }

        return $arFields;
    }



    function saveFields(&$arFields){
        /*        $hd = fopen(__DIR__ . "/log_order.txt", "a");
                fwrite($hd, print_r($arFields, 1));
                fwrite($hd, print_r($_POST, 1));
                fwrite($hd, print_r($_GET, 1));
                fclose($hd);*/

        /*$userId = $GLOBALS["USER"]->GetID();
<<<<<<< HEAD

        $fields = array(
            'DATE_INSERT' => new \Bitrix\Main\Type\DateTime(),
            'DATE_UPDATE' => new \Bitrix\Main\Type\DateTime(),
            'USER_ID' => $userId,
            'CODE' => md5(time().randString(10))
        );

=======

        $fields = array(
            'DATE_INSERT' => new \Bitrix\Main\Type\DateTime(),
            'DATE_UPDATE' => new \Bitrix\Main\Type\DateTime(),
            'USER_ID' => $userId,
            'CODE' => md5(time().randString(10))
        );

>>>>>>> master
        $nuwFUserCreated = \Bitrix\Sale\Internals\FuserTable::add($fields);

        if($nuwFUserCreated) {
            $nuwFUserId = \Bitrix\Sale\Internals\FuserTable::getList(array(
                'filter' => array(
                    'USER_ID' => $userId
                ),
                'select' => array(
                    'ID'
                ),
                'order' => array('ID' => "DESC")
            ))->fetch()["ID"];

            foreach ($arFields["BASKET_ITEMS"] as $arItem) {
                if($nuwFUserId != $arItem["FUSER_ID"]) {
                    CSaleBasket::Update($arItem["ID"], ["FUSER_ID" => $nuwFUserId]);
                    $arItem["FUSER_ID"] = $nuwFUserId;
                }
            }
        }*/
        $arFields = $_POST;

        unset($arFields["soa-action"]);
        unset($arFields["location_type"]);
        unset($arFields["sessid"]);

        \CUserOptions::SetOption("order", $GLOBALS["USER"]->GetID(), $arFields);

    }


}