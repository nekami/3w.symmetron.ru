<?
namespace Mcart\BasketSymmetron;

use Bitrix\Main\Localization\Loc;

class SymmetronBasket extends \Bitrix\Sale\Basket{

    public function getListOfFormatText()
    {
        $list = array();

        $registry = \Bitrix\Sale\Registry::getInstance(\Bitrix\Sale\Registry::REGISTRY_TYPE_ORDER);
        /** @var BasketItem $basketItemClassName */
        $basketItemClassName = $registry->getBasketItemClassName();


        $arDistr = ["BUY" => [], "EXAMPLE" => [], "REQUEST" => []];
        $arPropsProjIDs = [];
        /** @var BasketItem $basketItem */
        foreach ($this->collection as $basketItem)
        {
            $basketItemData = $basketItem->getField("NAME");

            /** @var \Bitrix\Sale\BasketPropertiesCollection $basketPropertyCollection */
            if ($basketPropertyCollection = $basketItem->getPropertyCollection())
            {
                $basketItemDataProperty = [];
                /** @var \Bitrix\Sale\BasketPropertyItem $basketPropertyItem */
                foreach ($basketPropertyCollection as $basketPropertyItem)
                {
                    if ($basketPropertyItem->getField('CODE') == "PRODUCT.XML_ID"
                        || $basketPropertyItem->getField('CODE') == "CATALOG.XML_ID"
                        || $basketPropertyItem->getField('CODE') == "SUM_OF_CHARGE"
                    )
                    {
                        continue;
                    }

                    if (strval(trim($basketPropertyItem->getField('VALUE'))) == "")
                        continue;

                    $basketItemDataProperty[strval(trim($basketPropertyItem->getField('CODE')))] = $basketPropertyItem->getField('VALUE');

                    //$basketItemDataProperty .= (!empty($basketItemDataProperty) ? "; " : "").trim($basketPropertyItem->getField('NAME')).": ".trim($basketPropertyItem->getField('VALUE'));
                }

                if(!empty($basketItemDataProperty["REQUEST"])) {

                    $measure = (strval($basketItem->getField("MEASURE_NAME")) != '') ? $basketItem->getField("MEASURE_NAME") : Loc::getMessage("SOA_SHT");
                    $arDistr["REQUEST"][$basketItem->getBasketCode()] = htmlspecialchars_decode($basketItemData." - ".$basketItemClassName::formatQuantity($basketItem->getQuantity())." ".$measure);

                } elseif (!empty($basketItemDataProperty["EXAMPLE"])) {

                    $measure = (strval($basketItem->getField("MEASURE_NAME")) != '') ? $basketItem->getField("MEASURE_NAME") : Loc::getMessage("SOA_SHT");
                    $arDistr["EXAMPLE"][$basketItem->getBasketCode()] = htmlspecialchars_decode($basketItemData."{PROJECTS_DATA} - ".$basketItemClassName::formatQuantity($basketItem->getQuantity())." ".$measure);
                    $arPropsProjIDs[$basketItem->getBasketCode()] = htmlspecialchars_decode($basketItemDataProperty["PROJECT"]);

                } elseif (empty($basketItemDataProperty["HIDDEN"]) || $basketItemDataProperty["HIDDEN"] != "Y") {

                    $measure = (strval($basketItem->getField("MEASURE_NAME")) != '') ? $basketItem->getField("MEASURE_NAME") : Loc::getMessage("SOA_SHT");
                    $arDistr["BUY"][$basketItem->getBasketCode()] = htmlspecialchars_decode($basketItemData . " - " . $basketItemClassName::formatQuantity($basketItem->getQuantity()) . " " . $measure . " x " . SaleFormatCurrency($basketItem->getPrice(), $basketItem->getCurrency()));

                }

                //if (!empty($basketItemDataProperty))
                //    $basketItemData .= " [".$basketItemDataProperty."]";
            }

            //$measure = (strval($basketItem->getField("MEASURE_NAME")) != '') ? $basketItem->getField("MEASURE_NAME") : Loc::getMessage("SOA_SHT");
            //$list[$basketItem->getBasketCode()] = $basketItemData." - ".$basketItemClassName::formatQuantity($basketItem->getQuantity())." ".$measure." x ".SaleFormatCurrency($basketItem->getPrice(), $basketItem->getCurrency()) . "ddddddd";

        }

        $arProjectsNames = [];
        $arPropsProjIDsFilter = array_filter($arPropsProjIDs, function ($el){
            return !empty($el);
        });

        if(\CModule::IncludeModule("iblock") && !empty($arPropsProjIDsFilter)) {
            $rsProjNames = \CIBlockElement::GetList([], ["ID" => $arPropsProjIDsFilter], false, false, ["ID", "NAME"]);
            while($arProjNames = $rsProjNames->Fetch()) {
                $arProjectsNames[$arProjNames["ID"]] = trim($arProjNames["NAME"]);
            }
        }


        //$hd = fopen(__DIR__ . "/log_order_parce.txt", "a");
        //fwrite($hd, print_r($arProjectsNames, 1));
        //fclose($hd);
        foreach ($arDistr["EXAMPLE"] as $key => $val) {
            $strProj = "";
            if(!empty($arProjectsNames[$arPropsProjIDs[$key]])) {
                $strProj = " (Проект " . $arProjectsNames[$arPropsProjIDs[$key]] . ")";
                //$strProj = htmlspecialchars($strProj);
                $arDistr["EXAMPLE"][$key] = str_replace("{PROJECTS_DATA}",
                    $strProj,
                    $arDistr["EXAMPLE"][$key]);
            } else {
                $arDistr["EXAMPLE"][$key] = str_replace("{PROJECTS_DATA}",
                    "", $arDistr["EXAMPLE"][$key]);
            }
        }


        if(!empty($arDistr["BUY"]))
            $arDistr["BUY"][] = "<br>";

        if(!empty($arDistr["EXAMPLE"]))
            $arDistr["EXAMPLE"][] = "<br>";

        $list = array_merge($arDistr["BUY"], $arDistr["EXAMPLE"], $arDistr["REQUEST"]);

        return !empty($list) ? $list : false;
    }
}