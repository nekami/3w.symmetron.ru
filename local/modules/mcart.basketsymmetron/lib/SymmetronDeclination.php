<?
namespace Mcart\BasketSymmetron;

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Grid\Declension;
use http\Env\Request;

/**
 * Class SymmetronDeclination
 * @package Mcart\BasketSymmetron
 */
class SymmetronDeclination {

    /**
     * @var array
     */
    private $arDeclensions;
    /**
     * @var
     */
    private $classDeclensionsHL;

    /**
     * SymmetronDeclination constructor.
     * @param array $arWords
     */
    function __construct(array $arWords = [])
    {
        $this->arDeclensions = $arWords ? $this->getDeclinations($arWords) : $arWords;
    }

    /**
     * @param array $arWords
     * @return array
     */
    public function getDeclinations(array $arWords = []) : array {

        \CModule::IncludeModule('highloadblock');
        $hlBlock = HLBT::getList(["filter" => ["NAME" => "DeclensionOfWords"]])->fetch();
        $entity = HLBT::compileEntity($hlBlock);
        $this->classDeclensionsHL = $entity->getDataClass();

        $rsDeclensions = $this->classDeclensionsHL::getList(["filter" => $arWords ? ["UF_WORD_DEC_1" => $arWords] : []]);
        $arResult = [];

        while ($arDeclension = $rsDeclensions->Fetch()){
            $arResult[$arDeclension["UF_WORD_DEC_1"]] = $arDeclension;
        }

        return $arResult;
    }

    /**
     * @param string $word
     * @return array
     */
    public function wordRequest(string $word) : array {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://ws3.morpher.ru/russian/declension?s=$word");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
        ));
        $output = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($code != 200){
            return [];
        }

        $arOut = \Bitrix\Main\Web\Json::decode($output);
        $arResult = [
            "UF_WORD_DEC_IN" => $arOut["П"],
            "UF_WORD_DEC_0" => $arOut["множественное"]["Р"],
            "UF_WORD_DEC_1" => $word,
            "UF_WORD_DEC_2" => $arOut["Р"]
        ];

        return $arResult;
    }

    /**
     * @param string $word
     * @return string
     */
    public function checkWord(string $word) : string {
        $arWord = $this->arDeclensions[$word];
        if(empty($arWord)){
            $arReqWord = $this->wordRequest($word);
            if(empty($arReqWord)) {
                return $word;
            } else {
                if($this->classDeclensionsHL::add($arReqWord) <= 0)
                    return $word;
                $this->arDeclensions[$word] = $arReqWord;
            }
        }

        return "";
    }

    /**
     * @param string $word
     * @return string
     */
    public function declinationIn(string $word) : string {

        if(empty($word))
            return $word;

        if($retWord = $this->checkWord($word))
            return $retWord;

        return $this->arDeclensions[$word]["UF_WORD_DEC_IN"];
    }

    /**
     * @param string $word
     * @param int $num
     * @return string
     */
    public function declinationNum(string $word, int $num) : string {

        if(empty($word))
            return $word;

        if($retWord = $this->checkWord($word))
            return $retWord;

        $arWord = $this->arDeclensions[$word];

        $bxDeclension = new Declension($arWord["UF_WORD_DEC_1"], $arWord["UF_WORD_DEC_2"], $arWord["UF_WORD_DEC_0"]);
        return $bxDeclension->get($num);
    }

    /**
     * @return array
     */
    public function getDeclinationsArray() : array {
        return $this->arDeclensions;
    }

}