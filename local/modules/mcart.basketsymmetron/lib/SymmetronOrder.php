<?
namespace Mcart\BasketSymmetron;

use Bitrix\Sale\BasketBase;
use Bitrix\Sale\Basket;

class SymmetronOrder extends \Bitrix\Sale\Order{


    public function appendBasket(BasketBase $basket){

        $result = parent::appendBasket($basket);

        //$this->currentBasket = $this->basketCollection;

        $trigger = htmlspecialchars(trim($_GET["part"]))?:"";

        if ($trigger !== "" && !$this->basketCollection->isEmpty())
        {
            $countIndex = 0;

            if ($trigger == "BUY") {
                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if(!empty($arProps["REQUEST"]["VALUE"]) || $arProps["EXAMPLE"]["VALUE"] == "Y" || $arProps["HIDDEN"]["VALUE"] == "Y") {
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            } elseif ($trigger == "REQUEST") {

                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if(empty($arProps["REQUEST"]["VALUE"])){
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            } elseif ($trigger == "EXAMPLE") {

                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if($arProps["EXAMPLE"]["VALUE"] != "Y"){
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            } else {
                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if($arProps["HIDDEN"]["VALUE"] == "Y") {
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            }
        }

        return $result;
    }


    /**
     * Return order basket.
     *
     * @return Basket
     */
    public function getBasket()
    {

        $result = parent::getBasket();

        //$this->currentBasket = $this->basketCollection;

        $trigger = htmlspecialchars(trim($_GET["part"]))?:"";

        if ($trigger !== "" && !$this->basketCollection->isEmpty())
        {
            $countIndex = 0;

            if ($trigger == "BUY") {
                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if(!empty($arProps["REQUEST"]["VALUE"]) || $arProps["EXAMPLE"]["VALUE"] == "Y" || $arProps["HIDDEN"]["VALUE"] == "Y") {
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            } elseif ($trigger == "REQUEST") {

                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if(empty($arProps["REQUEST"]["VALUE"])){
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            } elseif ($trigger == "EXAMPLE") {

                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if($arProps["EXAMPLE"]["VALUE"] != "Y"){
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            } else {
                foreach ($this->basketCollection as $arItem) {

                    $arProps = $arItem->getPropertyCollection()->getPropertyValues();

                    if($arProps["HIDDEN"]["VALUE"] == "Y") {
                        $this->basketCollection->deleteItem($countIndex);
                    }
                    $countIndex++;
                }
            }
        }

        return $result;

    }

}
