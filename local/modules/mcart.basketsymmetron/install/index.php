<?

IncludeModuleLangFile(__FILE__);


if (class_exists("mcart_basketsymmetron"))
    return;

Class mcart_basketsymmetron extends CModule {

    var $MODULE_ID = "mcart.basketsymmetron";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";

    function __construct() {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = BREAKNOTIFICATION_VERSION;
            $this->MODULE_VERSION_DATE = BREAKNOTIFICATION_VERSION_DATE;
        }

        $this->MODULE_NAME = GetMessage("MCART_BASKETSYMM_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MCART_BASKETSYMM_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("MCART_BASKETSYMM_PARTNER_NAME");
        $this->PARTNER_URI = "http://mcart.ru/";
    }

    function DoInstall() {

        if (!IsModuleInstalled($this->MODULE_ID)) {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        }
        return true;
    }

    function DoUninstall() {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();

        return true;
    }

    function InstallDB() {

        RegisterModule($this->MODULE_ID);
        return true;
    }

    function UnInstallDB() {

        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function InstallEvents() {

        RegisterModuleDependences("main", "OnPageStart", $this->MODULE_ID, 'CBasketSymmetron', 'updateProductPrice');
        RegisterModuleDependences("sale", "OnOrderNewSendEmail", $this->MODULE_ID, 'CBasketSymmetron', 'changeTemplate');
        RegisterModuleDependences("sale", "OnBeforeOrderAdd", $this->MODULE_ID, 'CBasketSymmetron', 'deleteHiddenPositionInOrder');
        RegisterModuleDependences("search", "BeforeIndex", $this->MODULE_ID, 'CBasketSymmetron', 'searchTitleNew');
        return true;
    }

    function UnInstallEvents() {
        UnRegisterModuleDependences("main", "OnPageStart", $this->MODULE_ID, 'CBasketSymmetron', 'updateProductPrice');
        UnRegisterModuleDependences("sale", "OnOrderNewSendEmail", $this->MODULE_ID, 'CBasketSymmetron', 'changeTemplate');
        UnRegisterModuleDependences("sale", "OnBeforeOrderAdd", $this->MODULE_ID, 'CBasketSymmetron', 'deleteHiddenPositionInOrder');
        UnRegisterModuleDependences("search", "BeforeIndex", $this->MODULE_ID, 'CBasketSymmetron', 'searchTitleNew');
        return true;
    }

    function InstallFiles() {
        return true;
    }

    function UnInstallFiles() {
        return true;
    }

}

//end class
?>