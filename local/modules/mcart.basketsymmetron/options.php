<?
global $MESS;
IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin.php");

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");


$module_id = "mcart.basketsymmetron";
CModule::IncludeModule($module_id);
$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);

$OptionsFieldsList = [
    'BASKETSM_KEY_YANDEX' => 'STRING',
    'BASKETSM_KEY_DADATA' => 'STRING',
];

foreach ($OptionsFieldsList as $OptionsFieldsName => $OptionsFieldsValue) {
    if ($OptionsFieldsValue == 'INT') {
        $OptionsFields[$OptionsFieldsName] = COption::GetOptionInt($module_id, $OptionsFieldsName);
    } elseif ($OptionsFieldsValue == 'STRING') {
        $OptionsFields[$OptionsFieldsName] = COption::GetOptionString($module_id, $OptionsFieldsName);
    }
}
$STATUSES = explode(',', COption::GetOptionString($module_id, "STATUSES"));

if ($MOD_RIGHT >= "R"):
    if ($MOD_RIGHT >= "Y" || $USER->IsAdmin()):
        if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && check_bitrix_sessid()) {
            foreach ($OptionsFields as $FieldName => $FieldValue) {
                if ($_POST[$FieldName]) {
                    $OptionsFields[$FieldName] = htmlspecialchars($_POST[$FieldName]);
                    if ($OptionsFieldsList[$FieldName] == 'INT') {
                        COption::SetOptionInt($module_id, $FieldName, $OptionsFields[$FieldName]);
                    } elseif ($OptionsFieldsList[$FieldName] == 'STRING') {
                        COption::SetOptionString($module_id, $FieldName, $OptionsFields[$FieldName]);
                    }
                }
            }
        }
    endif;

    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("MCART_BASKETSM_ST_SETTINGS"), "ICON" => "main_settings", "TITLE" => GetMessage("MCART_BASKETSM_ST_SETTINGS")),
    );
    $tabControl = new CAdminTabControl("tabControl", $aTabs);
    $tabControl->Begin();
    ?>
    <form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialchars($mid) ?>&lang=<?= LANGUAGE_ID ?>" name="webdav_settings">
        <? $tabControl->BeginNextTab(); ?>
        <? foreach ($OptionsFields as $FieldName => $FieldValue) : ?>
            <tr>
                <td ><? echo GetMessage('MCART_' . $FieldName) ?></td>
                <td><input type="text" name="<?= $FieldName ?>" id="<?= $FieldName ?>" value=<?= $FieldValue ?>></td>
            </tr>
        <? endforeach; ?>

        <? $tabControl->Buttons(); ?>
        <? ?>
        <input type="submit" name="Update" <? if ($MOD_RIGHT < "W") echo "disabled" ?> value="<? echo GetMessage("MAIN_SAVE") ?>">
        <input type="reset" name="reset" value="<? echo GetMessage("MAIN_RESET") ?>">
        <input type="hidden" name="Update" value="Y">

        <?= bitrix_sessid_post(); ?>
        <? $tabControl->End(); ?>
    </form>
<? endif; ?>
