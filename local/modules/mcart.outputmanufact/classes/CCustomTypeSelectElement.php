<?php
use Bitrix\Main\Localization\Loc,
    Bitrix\Iblock;

Loc::loadMessages(__FILE__);

/**
 * Class CCustomTypeSelectElement
 */
class CCustomTypeSelectElement{

    /**
     * property initialisation
     *
     * @return array
     */
    function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE'             => Iblock\PropertyTable::TYPE_ELEMENT,
            'USER_TYPE'             	=> 'mcart_select_element',
            'DESCRIPTION'           	=> 'Mcart Select Element',
            'GetPropertyFieldHtml'  	=> array(__CLASS__, 'GetPropertyFieldHtml'),
            'PrepareSettings'           => array(__CLASS__, 'PrepareSettings'),
            'GetSettingsHTML'           => array(__CLASS__,'GetSettingsHTML'),
            "GetPropertyFieldHtmlMulty" => array(__CLASS__, "GetPropertyFieldHtmlMulty"),
        );
    }

    /**
     * @param $arProperty
     * @param $value
     * @param $strHTMLControlName
     * @return false|string
     */
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {

        ob_start();
        echo "this property must be multiple";
        $html_value = ob_get_contents();

        ob_end_clean();

        return $html_value;

    }

    /**
     * @param $arProperty
     * @param $arValues
     * @param $strHTMLControlName
     * @return false|string
     */
    function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName) {

        $currentElementID = htmlspecialchars($_GET["ID"]);

        $iBlockSecID = 0;
        if(!empty($currentElementID)) {
            $iBlockSecID = CIBlockElement::GetList(Array("SORT" => "ASC"), array("ID" => $currentElementID),
                false, false, array("ID", "IBLOCK_SECTION_ID"))->Fetch()["IBLOCK_SECTION_ID"];


        } else {
            $iBlockSecID = htmlspecialchars($_GET["IBLOCK_SECTION_ID"]);
        }

        $section = 0;
        if(!empty($iBlockSecID) && $arProperty["LINK_IBLOCK_ID"] > 0) {
            $sectionCode = CIBlockSection::GetList(Array("SORT" => "ASC"), array("ID" => $iBlockSecID), false,
                array("ID", "CODE"))->Fetch();

            if(!empty($sectionCode["CODE"])){
                $section = CIBlockSection::GetList(Array("SORT" => "ASC"), array("CODE" => $sectionCode["CODE"],
                    "IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"]), false, array("ID", "CODE"))->Fetch()["ID"];
            }
        }

        $arProperty["SECTION_ID"] = $section;
        $name = $strHTMLControlName["VALUE"];
        ob_start();
        self::_ShowElementPropertyField($name, $arProperty, $arValues);
        $html_value = ob_get_contents();
        ob_end_clean();

        return $html_value;

    }

    /**
     * @param $arProperty
     * @return array
     */
    public static function PrepareSettings($arProperty)
    {
        $size = 0;
        if(is_array($arProperty["USER_TYPE_SETTINGS"]))
            $size = intval($arProperty["USER_TYPE_SETTINGS"]["size"]);
        if($size <= 0)
            $size = 1;

        $width = 0;
        if(is_array($arProperty["USER_TYPE_SETTINGS"]))
            $width = intval($arProperty["USER_TYPE_SETTINGS"]["width"]);
        if($width <= 0)
            $width = 0;

        if(is_array($arProperty["USER_TYPE_SETTINGS"]) && $arProperty["USER_TYPE_SETTINGS"]["group"] === "Y")
            $group = "Y";
        else
            $group = "N";

        if(is_array($arProperty["USER_TYPE_SETTINGS"]) && $arProperty["USER_TYPE_SETTINGS"]["multiple"] === "Y")
            $multiple = "Y";
        else
            $multiple = "N";

        return array(
            "size" =>  $size,
            "width" => $width,
            "group" => $group,
            "multiple" => $multiple,
        );
    }

    /**
     * @param $arProperty
     * @param $strHTMLControlName
     * @param $arPropertyFields
     * @return string
     */
    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $settings = self::PrepareSettings($arProperty);

        $arPropertyFields = array(
            "HIDE" => array("ROW_COUNT", "COL_COUNT", "MULTIPLE_CNT"),
        );

        return '

		<tr valign="top">
			<td>'.Loc::getMessage("IBLOCK_PROP_ELEMENT_LIST_SETTING_SIZE").':</td>
			<td><input type="text" size="5" name="'.$strHTMLControlName["NAME"].'[size]" value="'.$settings["size"].'"></td>
		</tr>
		<tr valign="top">
			<td>'.Loc::getMessage("IBLOCK_PROP_ELEMENT_LIST_SETTING_WIDTH").':</td>
			<td><input type="text" size="5" name="'.$strHTMLControlName["NAME"].'[width]" value="'.$settings["width"].'">px</td>
		</tr>
		<tr valign="top">
			<td>'.Loc::getMessage("IBLOCK_PROP_ELEMENT_LIST_SETTING_SECTION_GROUP").':</td>
			<td><input type="checkbox" name="'.$strHTMLControlName["NAME"].'[group]" value="Y" '.($settings["group"]=="Y"? 'checked': '').'></td>
		</tr>
		<tr valign="top">
			<td>'.Loc::getMessage("IBLOCK_PROP_ELEMENT_LIST_SETTING_MULTIPLE").':</td>
			<td><input type="checkbox" name="'.$strHTMLControlName["NAME"].'[multiple]" value="Y" '.($settings["multiple"]=="Y"? 'checked': '').'></td>
		</tr>
		';
    }

    /**
     * @param $name
     * @param $property_fields
     * @param $values
     * @param bool $bVarsFromForm
     */
    public static function _ShowElementPropertyField($name, $property_fields, $values, $bVarsFromForm = false)
    {
        global $bCopy;
        if(!CModule::IncludeModule("iblock"))
            return;

        $name = htmlspecialcharsbx($name);
        $index = 0;
        $show = true;
        $selfFolderUrl = (defined("SELF_FOLDER_URL") ? SELF_FOLDER_URL : "/bitrix/admin/");

        $MULTIPLE_CNT = intval($property_fields["MULTIPLE_CNT"]);
        if ($MULTIPLE_CNT <= 0 || $MULTIPLE_CNT > 30)
            $MULTIPLE_CNT = 5;

        $cnt = ($property_fields["MULTIPLE"] == "Y"? $MULTIPLE_CNT : 1);

        if(!is_array($values))
            $values = array();

        $fixIBlock = $property_fields["LINK_IBLOCK_ID"] > 0;
        $windowTableId = 'iblockprop-'.Iblock\PropertyTable::TYPE_ELEMENT.'-'.$property_fields['ID'].'-'.$property_fields['LINK_IBLOCK_ID'];

        echo '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb'.md5($name).'">';
        $key = '';

        $sectionIDinURL = "";
        if(!empty($property_fields["SECTION_ID"])) {
            $sectionIDinURL = '&amp;SECTION_ID='.$property_fields["SECTION_ID"];
        }

        foreach ($values as $key=>$val)
        {
            $show = false;
            if ($bCopy)
            {
                $key = "n".$index;
                $index++;
            }

            $descr = "";
            if (is_array($val) && array_key_exists("DESCRIPTION", $val))
                $descr = $val["DESCRIPTION"];

            if (is_array($val) && array_key_exists("VALUE", $val))
                $val = $val["VALUE"];

            $db_res = CIBlockElement::GetByID($val);
            $ar_res = $db_res->GetNext();
            echo '<tr><td>'.
                '<input name="'.$name.'['.$key.'][DESCRIPTION]" value="'.htmlspecialcharsbx($descr).'" size="5" type="text">'.
                '<input name="'.$name.'['.$key.'][VALUE]" id="'.$name.'['.$key.']" value="'.htmlspecialcharsbx($val).'" size="5" type="text">'.
                '<input type="button" value="..." onClick="jsUtils.OpenWindow(\''.$selfFolderUrl.'mcart.outputmanufact/iblock_elem_search_section.php?lang='.LANGUAGE_ID.'&amp;IBLOCK_ID='.$property_fields["LINK_IBLOCK_ID"].'&amp;n='.$name.'&amp;k='.$key.($fixIBlock ? '&amp;iblockfix=y' : '').'&amp;tableId='.$windowTableId.$sectionIDinURL.'\', 900, 700);">'.
                '&nbsp;<span id="sp_'.md5($name).'_'.$key.'" >'.$ar_res['NAME'].'</span>'.
                '</td></tr>';

            if ($property_fields["MULTIPLE"] != "Y")
            {
                $bVarsFromForm = true;
                break;
            }
        }

        if (!$bVarsFromForm || $show)
        {
            for ($i = 0; $i < $cnt; $i++)
            {
                $val = "";
                $key = "n".$index;
                $index++;

                echo '<tr><td>'.
                    '<input name="'.$name.'['.$key.'][DESCRIPTION]" value="" size="5" type="text">'.
                    '<input name="'.$name.'['.$key.'][VALUE]" id="'.$name.'['.$key.']" value="'.htmlspecialcharsbx($val).'" size="5" type="text">'.
                    '<input type="button" value="..." onClick="jsUtils.OpenWindow(\''.$selfFolderUrl.'mcart.outputmanufact/iblock_elem_search_section.php?lang='.LANGUAGE_ID.'&amp;IBLOCK_ID='.$property_fields["LINK_IBLOCK_ID"].'&amp;n='.$name.'&amp;k='.$key.($fixIBlock ? '&amp;iblockfix=y' : '').'&amp;tableId='.$windowTableId.$sectionIDinURL.'\', 900, 700);">'.
                    '&nbsp;<span id="sp_'.md5($name).'_'.$key.'"></span>'.
                    '</td></tr>';
            }
        }

        if($property_fields["MULTIPLE"]=="Y")
        {
            echo '<tr><td>'.
                '<input type="button" value="'.GetMessage("IBLOCK_AT_PROP_ADD").'..." onClick="jsUtils.OpenWindow(\''.$selfFolderUrl.'mcart.outputmanufact/iblock_elem_search_section.php?lang='.LANGUAGE_ID.'&amp;IBLOCK_ID='.$property_fields["LINK_IBLOCK_ID"].'&amp;n='.$name.'&amp;m=y&amp;k='.$key.($fixIBlock ? '&amp;iblockfix=y' : '').'&amp;tableId='.$windowTableId.$sectionIDinURL.'\', 900, 700);">'.
                '<span id="sp_'.md5($name).'_'.$key.'" ></span>'.
                '</td></tr>';
        }

        echo '</table>';
        echo '<script type="text/javascript">'."\r\n";
        echo "var MV_".md5($name)." = ".$index.";\r\n";
        echo "function InS".md5($name)."(id, name){ \r\n";
        echo "	oTbl=document.getElementById('tb".md5($name)."');\r\n";
        echo "	oRow=oTbl.insertRow(oTbl.rows.length-1); \r\n";
        echo "	oCell=oRow.insertCell(-1); \r\n";
        echo "	oCell.innerHTML=".
            "'<input name=\"".$name."[n'+MV_".md5($name)."+']\" value=\"'+id+'\" id=\"".$name."[n'+MV_".md5($name)."+']\" size=\"5\" type=\"text\">'+\r\n".
            "'<input type=\"button\" value=\"...\" '+\r\n".
            "'onClick=\"jsUtils.OpenWindow(\'".$selfFolderUrl."mcart.outputmanufact/iblock_elem_search_section.php?lang=".LANGUAGE_ID."&amp;IBLOCK_ID=".$property_fields["LINK_IBLOCK_ID"]."&amp;n=".$name."&amp;k=n'+MV_".md5($name)."+'".($fixIBlock ? '&amp;iblockfix=y' : '')."&amp;tableId=".$windowTableId.$sectionIDinURL."\', '+\r\n".
            "' 900, 700);\">'+".
            "'&nbsp;<span id=\"sp_".md5($name)."_n'+MV_".md5($name)."+'\" >'+name+'</span>".
            "';";
        echo 'MV_'.md5($name).'++;';
        echo '}';
        echo "\r\n</script>";
    }

}