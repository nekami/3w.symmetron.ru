<?

use Bitrix\Main\Loader;

global $USER;

if (!Loader::includeModule('mcart.scripts') || !$USER->IsAdmin())
	return;

// сформируем верхний пункт меню
$aMenu = [
	"parent_menu" => "global_menu_store", // поместим в раздел "Сервис"
	"sort" => 50000,                    // вес пункта меню
	"url" => "mcart_script_duplicate_removal.php?lang=" . LANGUAGE_ID,  // ссылка на пункте меню
	"text" => 'Скрипты',       // текст пункта меню
	"title" => 'Дополнительные скрипты', // текст всплывающей подсказки
	"icon" => "mcart_scripts_menu_icon", // малая иконка
	"page_icon" => "mcart_scripts_menu_icon", // большая иконка
	"items_id" => "menu_mcart_scripts",  // идентификатор ветви
	"items" => [],          // остальные уровни меню сформируем ниже.
];

//если нам нужно добавить ещё пункты - точно так же добавляем элементы в массив $aMenu["items"]
$aMenu["items"][] = [
	"text" => 'Удаление дубликатов',
	"url" => "mcart_script_duplicate_removal.php?lang=" . LANGUAGE_ID,
	"title" => 'Перейти на страницу скрипта',
];

$aMenu["items"][] = [
    "text" => 'Установка статуса "Наличие"',
    "url" => "mcart_script_reset_availability.php?lang=" . LANGUAGE_ID,
    "title" => 'Перейти на страницу скрипта',
];

//вернем полученный список
return $aMenu;