<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
global $APPLICATION, $USER;

if (!$USER->IsAdmin()) {
    return;
}

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Loader::includeModule('mcart.scripts');
Loader::includeModule('iblock');

/**
 * GETTING ITEM IDS
 */
global $DB;

$strSql = "select count(*) as c, prop.VALUE from b_iblock_element as el
inner join b_iblock_element_property as prop on el.ID = prop.IBLOCK_ELEMENT_ID
where prop.IBLOCK_PROPERTY_ID = \"3040\" and el.IBLOCK_ID=4
GROUP BY prop.VALUE having count(*) > 1;";

$rsRows = $DB->Query($strSql, false, __LINE__);

$itemIDs = [];
while ($arRow = $rsRows->Fetch()) {
    $itemIDs[$arRow['VALUE']] = $arRow['VALUE'];
}

/**
 * GETTING IBLOCK ELEMENTS
 */
$rowCounter = 0;
$elementCounter = 0;
$arResult = [];
if (!empty($itemIDs)) {
    $arSort = [
        'ID' => 'ASC'
    ];

    $arSelect = [
        'ID',
        'IBLOCK_ID',
        'NAME',
        'ACTIVE',
        'PROPERTY_ITEMID'
    ];

    $arFilter = [
        'IBLOCK_ID' => 4,
        'PROPERTY_ITEMID' => $itemIDs
    ];

    $rsElements = \CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

    while ($arElement = $rsElements->GetNext(false)) {
        $elementCounter++;
        if (!isset($arResult[$arElement['PROPERTY_ITEMID_VALUE']])) {
            $rowCounter++;
            $arResult[$arElement['PROPERTY_ITEMID_VALUE']]['ACTIVE_FOUND'] = false;
        }

        if ($arElement['ACTIVE'] === 'Y') {
            $arResult[$arElement['PROPERTY_ITEMID_VALUE']]['ACTIVE_FOUND'] = true;
            $arElement['SAVE'] = true;
        } else {
            $arElement['SAVE'] = false;
        }

        $arResult[$arElement['PROPERTY_ITEMID_VALUE']]['ELEMENTS'][$arElement['ID']] = $arElement;
    }
}

foreach ($arResult as $itemid => $row) {
    $activeId = 0;
    if (!$row['ACTIVE_FOUND']) {
        $firstElement = reset($row['ELEMENTS']);
        $arResult[$itemid]['ELEMENTS'][$firstElement['ID']]['SAVE'] = true;
    }
}

$columnsList = [
    [
        "id" => "ITEMID",
        "content" => 'ITEMID',
        //"sort" => "hour",
        //"align" => "right",
        "default" => true,
    ],
    [
        "id" => "PRODUCTS",
        "content" => 'Товары',
        //"sort" => "total_count",
        "default" => true,
    ]
];

$sTableID = 'duplicate_removal';
$lAdmin = new \CAdminList($sTableID, $oSort); // основной объект списка
$lAdmin->AddHeaders($columnsList);

$hrefTemplate = '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=#IBLOCK#&type=catalog&ID=#ELEMENT#&lang=ru&find_section_section=-1&WF=Y';
$makeLink = static function ($element) use ($hrefTemplate) {

    if ($element['SAVE']) {
        $classes = 'save';
    } else {
        $classes = 'delete';
    }

    $href = $hrefTemplate;
    $href = str_replace(
        [
            '#IBLOCK#',
            '#ELEMENT#'
        ],
        [
            $element['IBLOCK_ID'],
            $element['ID']
        ],
        $href
    );

    $link = '<a href="' . $href . '" class="' . $classes . '">';
    $link .= '[' . $element['ID'] . '][' . $element['ACTIVE'] . '] ' . $element['NAME'];

    if ($element['SAVE'] || $element['ACTIVE'] === 'Y') {
        $link .= ' (останется)';
    } else {
        $link .= ' (удалится)';
    }

    $link .= '</a>';

    return $link;
};

$deleteCounter = 0;
foreach ($arResult as $itemid => $rowData) {
    $row =& $lAdmin->AddRow($arRes['ID'], $arRes);
    $row->AddViewField('ITEMID', $itemid);

    $elementsText = '';
    foreach ($rowData['ELEMENTS'] as $element) {
        $elementsText .= $makeLink($element);
        if ($deleteCounter < 800 && $_REQUEST['delete'] === 'Y' && !$element['SAVE'] && $element['ACTIVE'] !== 'Y') {
            $elementsText .= 'удален';
            \CIBlockElement::Delete($element['ID']);
            $deleteCounter++;
        }
    }

    $row->AddViewField('PRODUCTS', $elementsText);
}

$APPLICATION->SetTitle('Скрипт очистки дублей');


require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_after.php');

if (!empty($message)) {
    if (is_array($message)) {
        foreach ($message as $msg) {
            echo $msg->Show();
        }
    } else {
        echo $message->Show();
    }
}
?>

    Всего рядов: <?= $rowCounter ?> <br>
    Всего элементов: <?= $elementCounter ?>

<?php

$lAdmin->DisplayList();

?>

    <form action="" method="post">
        <input type="hidden" name="delete" value="Y">
        <button type="submit" class="adm-btn">Удалить</button>
    </form>

    <style type="text/css">
        .adm-list-table-row a.save {
            color: #038500;
            display: block;
            margin-bottom: 10px;
        }

        .adm-list-table-row a.delete {
            display: block;
            color: #0B1A92;
        }
    </style>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php'); ?>