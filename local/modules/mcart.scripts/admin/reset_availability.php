<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
global $APPLICATION, $USER;

if (!$USER->IsAdmin()) {
    return;
}

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use MCart\Scripts\AvailabilityUpdater;

Loc::loadMessages(__FILE__);
Loader::includeModule('mcart.scripts');

$updater = new AvailabilityUpdater();


$columnsList = [
    [
        "id" => "ID",
        "content" => 'ITEMID',
        //"sort" => "hour",
        //"align" => "right",
        "default" => true,
    ],
    [
        "id" => "NAME",
        "content" => 'Товары',
        //"sort" => "total_count",
        "default" => true,
    ]
];

$sTableID = 'duplicate_removal';
$lAdmin = new \CAdminList($sTableID, $oSort); // основной объект списка
$lAdmin->AddHeaders($columnsList);

$hrefTemplate = '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=#IBLOCK#&type=catalog&ID=#ELEMENT#&lang=ru&find_section_section=-1&WF=Y';
$makeLink = static function ($element) use ($hrefTemplate) {

    $classes = [];

    $href = $hrefTemplate;
    $href = str_replace(
        [
            '#IBLOCK#',
            '#ELEMENT#'
        ],
        [
            $element['IBLOCK_ID'],
            $element['ID']
        ],
        $href
    );

    $link = '<a href="' . $href . '" class="' . $classes . '">';
    $link .= $element['NAME'];

    $link .= '</a>';

    return $link;
};

$rowCounter = 0;
$updateCounter = 0;
$rsRows = $updater->getElements();

while ($arRow = $rsRows->fetch()) {
    $rowCounter++;
    $row =& $lAdmin->AddRow($arRow['ID'], $arRow);
    $row->AddViewField('ID', $arRow['ID']);

    $elementsText = '';
    $elementsText .= $makeLink($arRow);
    if ($updateCounter < 800 && $_REQUEST['update'] === 'Y') {
        $updateCounter++;
    }

    $row->AddViewField('NAME', $elementsText);
}

$APPLICATION->SetTitle('Скрипт очистки дублей');


require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_after.php');

if (!empty($message)) {
    if (is_array($message)) {
        foreach ($message as $msg) {
            echo $msg->Show();
        }
    } else {
        echo $message->Show();
    }
}
?>

    Всего товаров: <?= $rowCounter ?> <br>

<?php

$lAdmin->DisplayList();

?>

    <form action="" method="post">
        <input type="hidden" name="update" value="Y">
        <button type="submit" class="adm-btn">Удалить</button>
    </form>

    <style type="text/css">
        .adm-list-table-row a.save {
            color: #038500;
            display: block;
            margin-bottom: 10px;
        }

        .adm-list-table-row a.delete {
            display: block;
            color: #0B1A92;
        }
    </style>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php'); ?>