#!/usr/bin/php -q
<?php

use MCart\Scripts\AvailabilityUpdater;
use MCart\Scripts\DuplicateFinder;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_CAT_CRON", true);
define('NO_AGENT_CHECK', true);
define('SITE_ID', 's1'); // your site ID - need for language ID

$_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . "/../../../../");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

set_time_limit(0);

$startDate = date('Y.m.d H:i:s');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

ini_set('memory_limit', '1024M');

if (\Bitrix\Main\Loader::includeModule('mcart.scripts')) {

    $updater = new AvailabilityUpdater();
    $updater->update();

} else {

    die('mcart.scripts');

}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");