<?
$MESS ['mcart.scripts_MODULE_NAME'] = "Локальные скрипты";
$MESS ['mcart.scripts_PARTNER_NAME'] = "MCart";
$MESS ['mcart.scripts_PARTNER_URI'] = "https://mcart.ru/";
$MESS ['mcart.scripts_MODULE_DESCRIPTION'] = "Модуль с локальными для сайта скриптами";
$MESS ['mcart.scripts_INSTALL_TITLE'] = "Установка модуля: Локальные скрипты";
$MESS ['mcart.scripts_UNINSTALL_TITLE'] = "Удаление модуля: Локальные скрипты";