<?php

namespace MCart\Scripts;

use Bitrix\Catalog\ProductTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Loader;

class AvailabilityUpdater
{

    protected $catalogIBlockId = 0;

    protected $statusPropId = 0;

    protected $noActiveVariantId = 0;

    protected $elementsList = [];

    public function __construct()
    {
        Loader::includeModule('iblock');

        $this->getIblockId();
        $this->getStatusPropId();
        $this->getValueEnumId();
    }

    protected function getIblockId()
    {
        if ($this->catalogIBlockId < 1) {
            $arIBlock = \CIBlock::GetList(
                [],
                [
                    'CODE' => 'pbd'
                ]
            )
                ->Fetch();

            if (is_array($arIBlock)) {
                $this->catalogIBlockId = (int)$arIBlock['ID'];
            }
        }

        return $this->catalogIBlockId;
    }

    protected function getStatusPropId()
    {
        if ($this->statusPropId < 1) {
            $arProp = \CIBlockProperty::GetList(
                [],
                [
                    'ACTIVE' => 'Y',
                    'IBLOCK_ID' => $this->getIblockId(),
                    'CODE' => 'ACTIVE_STATUS'
                ]
            )
                ->Fetch();

            if (is_array($arProp)) {
                $this->statusPropId = $arProp['ID'];
            }
        }

        return $this->statusPropId;
    }

    protected function getValueEnumId()
    {
        if ($this->noActiveVariantId < 1) {
            $arPropVariant = \CIBlockPropertyEnum::GetList(
                [],
                [
                    'IBLOCK_ID' => $this->getIblockId(),
                    'PROPERTY_ID' => $this->getStatusPropId(),
                    'XML_ID' => 'NOACTIVE'
                ]
            )
                ->Fetch();

            if (is_array($arPropVariant)) {
                $this->noActiveVariantId = $arPropVariant['ID'];
            }
        }

        return $this->noActiveVariantId;
    }

    public function getElements($limit = 1000)
    {
        $query = ProductTable::query();

        $query->registerRuntimeField(
            'ELEMENT',
            new ReferenceField(
                'ELEMENT',
                ElementTable::class,
                ['=this.ID' => 'ref.ID']
            )
        );

        $query->registerRuntimeField(
            'PROP',
            new ReferenceField(
                'PROP',
                ElementPropertyTable::class,
                ['=this.ID' => 'ref.IBLOCK_ELEMENT_ID']
            )
        );

        $query->setSelect([
            'ID' => 'ID',
            'NAME' => 'ELEMENT.NAME',
            'IBLOCK_ID' => 'ELEMENT.IBLOCK_ID'
        ]);

        $query->where('QUANTITY', 0);
        $query->where('PROP.IBLOCK_PROPERTY_ID', $this->getStatusPropId());
        $query->whereNot('PROP.VALUE', $this->getValueEnumId());
        $query->where('ELEMENT.IBLOCK_ID', $this->getIblockId());
        $query->where('ELEMENT.ACTIVE', 'Y');

        if ($limit > 0) {
            $query->setLimit($limit);
        }

        return $query->exec();
    }

    public function update()
    {
        $rsProducts = $this->getElements(0);

        $counter = 0;
        while ($arProduct = $rsProducts->fetch()) {
            \CIBlockElement::SetPropertyValuesEx(
                $arProduct['ID'],
                $this->getIblockId(),
                [$this->getStatusPropId() => $this->getValueEnumId()]
            );

            $counter++;
            fwrite(STDERR, 'ID' . $arProduct['ID'] . '_C' . $counter . "\r\n");
        }
    }


}