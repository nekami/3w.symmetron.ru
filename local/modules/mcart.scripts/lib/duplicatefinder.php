<?php

namespace MCart\Scripts;

use Bitrix\Main\Loader;

class DuplicateFinder
{

    protected $elementsList = [];

    public function __construct()
    {
        Loader::includeModule('mcart.scripts');
        Loader::includeModule('iblock');
    }

    public function getDuplicateGroups()
    {
        $itemIDs = $this->getItemIDs();

        if (!empty($itemIDs)) {
            $arSort = [
                'ID' => 'ASC'
            ];

            $arSelect = [
                'ID',
                'IBLOCK_ID',
                'NAME',
                'ACTIVE',
                'PROPERTY_ITEMID'
            ];

            $arFilter = [
                'IBLOCK_ID' => 4,
                'PROPERTY_ITEMID' => $itemIDs
            ];

            $rsElements = \CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

            while ($arElement = $rsElements->GetNext(false)) {
                if (!isset($arResult[$arElement['PROPERTY_ITEMID_VALUE']])) {
                    $arResult[$arElement['PROPERTY_ITEMID_VALUE']]['ACTIVE_FOUND'] = false;
                }

                if ($arElement['ACTIVE'] === 'Y') {
                    $arResult[$arElement['PROPERTY_ITEMID_VALUE']]['ACTIVE_FOUND'] = true;
                    $arElement['SAVE'] = true;
                } else {
                    $arElement['SAVE'] = false;
                }

                $this->elementsList[$arElement['PROPERTY_ITEMID_VALUE']]['ELEMENTS'][$arElement['ID']] = $arElement;
            }
        }

        $this->markSaved();

        return $this->elementsList;
    }

    protected function markSaved()
    {
        foreach ($this->elementsList as $itemid => $row) {
            if (!$row['ACTIVE_FOUND']) {
                $firstElement = reset($row['ELEMENTS']);
                $this->elementsList[$itemid]['ELEMENTS'][$firstElement['ID']]['SAVE'] = true;
            }
        }
    }

    protected function getItemIDs()
    {
        global $DB;

        $strSql = "select count(*) as c, prop.VALUE from b_iblock_element as el
inner join b_iblock_element_property as prop on el.ID = prop.IBLOCK_ELEMENT_ID
where prop.IBLOCK_PROPERTY_ID = \"3040\" and el.IBLOCK_ID=4
GROUP BY prop.VALUE having count(*) > 1;";

        $rsRows = $DB->Query($strSql, false, __LINE__);

        $itemIDs = [];
        while ($arRow = $rsRows->Fetch()) {
            $itemIDs[$arRow['VALUE']] = $arRow['VALUE'];
        }

        return $itemIDs;
    }

}