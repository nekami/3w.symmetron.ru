<?
IncludeModuleLangFile(__FILE__);

if (class_exists('mcart_scripts')) {
    return;
}

Class mcart_scripts extends CModule
{
    const MODULE_ID = 'mcart.scripts';
    public $MODULE_ID = 'mcart.scripts';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = GetMessage('mcart.scripts_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('mcart.scripts_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = GetMessage('mcart.scripts_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('mcart.scripts_PARTNER_URI');
    }

    function InstallDB($arParams = [])
    {
        return true;
    }

    function UnInstallDB($arParams = [])
    {
        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        CopyDirFiles(__DIR__ . '/themes', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/themes', true, true);
    }

    function UnInstallFiles()
    {
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        DeleteDirFiles(__DIR__ . '/themes/.default', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/themes/.default');
        DeleteDirFilesEx('/bitrix/themes/.default/icons/mcart.scripts');
    }

    function DoInstall()
    {
        RegisterModule(self::MODULE_ID);
        $this->InstallFiles();
    }

    function DoUninstall()
    {
        UnRegisterModule(self::MODULE_ID);
        $this->UnInstallFiles();
    }
}