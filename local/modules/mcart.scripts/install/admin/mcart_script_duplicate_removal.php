<?php
if (is_file($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/mcart.scripts/admin/duplicate_removal.php')) {
    require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/mcart.scripts/admin/duplicate_removal.php';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/mcart.scripts/admin/duplicate_removal.php';
}