<?php
if (is_file($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/mcart.scripts/admin/reset_availability.php')) {
    require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/mcart.scripts/admin/reset_availability.php';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/mcart.scripts/admin/reset_availability.php';
}