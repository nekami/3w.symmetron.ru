<?
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule("iblock");
IncludeModuleLangFile(__FILE__);



/***************************************************************************
HTML form
 ****************************************************************************/
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");

if(!empty(htmlspecialchars($_GET["IBLOCK_ID"]))) {

    $APPLICATION->IncludeComponent(
        "mcart:prop.line",
        "",
        Array(
            "CACHE_TIME" => "300",
            "CACHE_TYPE" => "A",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "IBLOCK_ID" => htmlspecialchars($_GET["IBLOCK_ID"])
        )
    );

}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");