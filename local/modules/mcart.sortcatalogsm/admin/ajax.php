<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("iblock"))
    die("");

$arElementsChecked = [];
if(htmlspecialchars($_GET["ELEMENT_ID"])) {
    $rsElementsChecked = CIBlockElement::GetList([], ["ID" => htmlspecialchars($_GET["ELEMENT_ID"])], false, false,
        ["PROPERTY_" . $_GET["PROP_REG_AJAX"]]);
    while ($arElementChecked = $rsElementsChecked->Fetch()) {
        if(!empty($arElementChecked["PROPERTY_" . $_GET["PROP_REG_AJAX"] . "_VALUE"]["VALUE"]))
            $arElementsChecked[] = $arElementChecked["PROPERTY_" . $_GET["PROP_REG_AJAX"] . "_VALUE"]["VALUE"];
    }
}


$IBLOCK_ID = htmlspecialchars($_GET["IBLOCK_ID"]);
$SECTIONS = array_filter($_GET["SECTIONS"], function ($el){return !empty($el);});
$PROP_REG_AJAX = htmlspecialchars($_GET["PROP_REG_AJAX"]);

if(empty($SECTIONS) || empty($IBLOCK_ID))
    die("");

$rsProperty = CIBlockProperty::GetList(
    [],
    ['IBLOCK_ID' => $IBLOCK_ID]
);

$arPropsNames = [];
while($arProperty = $rsProperty->Fetch())
{
    if(!in_array($arProperty["ID"], $arElementsChecked))
        $arPropsNames[$arProperty["ID"]] = $arProperty;
}


$arSelect = ["ID", "IBLOCK_ID"];
$arFilter = ["IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>$SECTIONS];
$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$arElements = [];
while($arElement = $rsElements->Fetch()){
    $arElements[] = $arElement["ID"];
}


$arRegulations = [];
if(!empty($arPropsNames) && !empty($arElements)) {
    $propElements = [];
    if(Cmodule::IncludeModule("mcart.basketsymmetron")){
        $propElements = \Mcart\BasketSymmetron\ElementPropertyTable::getList([
            "filter" => [
                "IBLOCK_PROPERTY_ID" => array_keys($arPropsNames),
                "IBLOCK_ELEMENT_ID" => $arElements,
                "!VALUE" => ""
            ]
        ]);

        /*$rsPropsCodes = Bitrix\Iblock\PropertyTable::getList([
            "filter" => ["ID" => array_keys($arPropsNames)],
            "select" => ["ID", "CODE"]
        ]);

        $arPropsCodes = [];
        while ($arPropsCode = $rsPropsCodes->Fetch()){
            $arPropsCodes[$arPropsCode["ID"]] = $arPropsCode["CODE"];
        }*/

        $arFirst = ["ITEMID", "UNITID", "DOC_AND_FILES", "DATE_OF_DISC", "STATUS_STOCK", "URLIMAGE", "URLPDF",
            "IMPORT_TIMESTAMP", "TEXT_HTML"];
        $arSecond = ["OPN", "STATUS", "SYMROHSCONFORMITY", "ATT_SHORT_DISCR", "SYMPACKAGE", "ATT_Brief_characteristic",
            "UPAK", "Complect", "Product_description", "Description_execution", "Description_options",
            "Completeness_delivery"];

        while($arProp = $propElements->Fetch()){
            if ((!empty(trim($arProp["VALUE"])) && trim($arProp["VALUE"]) != "NaN" && trim($arProp["VALUE"]) != "‒")
                || is_array($arProp["VALUE"])) {

                $propId = $arProp["IBLOCK_PROPERTY_ID"];

                $propCode = $arPropsNames[$propId]["CODE"];

                if($arPropsNames[$propId]["USER_TYPE"] == "mcart_property_with_measure_units"){
                    if((double)$arProp["DESCRIPTION"] == 0)
                        continue;

                    if(count(explode("|", $arProp["VALUE"])) < 2)
                        continue;
                }

                if($propCode == "ARTICLE"){
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 10,
                        "SHOW" => true,
                        "SET_SORT" => true,
                        "PARAM_SEARCH" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => true,
                            "SORT" => 20
                        ]
                    ];
                } elseif ($propCode == "PRODUCERID") {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 20,
                        "SHOW" => true,
                        "SET_SORT" => true,
                        "PARAM_SEARCH" => [
                            "VALUE" => true,
                            "SORT" => 10
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => true,
                            "SORT" => 10
                        ]
                    ];
                } elseif ($propCode == "DESCRIPTION") {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 30,
                        "SHOW" => true,
                        "SET_SORT" => true,
                        "PARAM_SEARCH" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => true,
                            "SORT" => 30
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ]
                    ];
                } elseif ($propCode == "ACTIVE_STATUS") {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 1010,
                        "SHOW" => true,
                        "SET_SORT" => true,
                        "PARAM_SEARCH" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => true,
                            "SORT" => 1010
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ]
                    ];
                } elseif ($propCode == "RETAIL_PRICE") {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 1020,
                        "SHOW" => true,
                        "SET_SORT" => true,
                        "PARAM_SEARCH" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => true,
                            "SORT" => 1020
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => false,
                            "SORT" => 500
                        ]
                    ];
                } elseif (in_array($propCode, $arFirst)) {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 3000,
                        "SHOW" => false,
                        "SET_SORT" => false,
                        "PARAM_SEARCH" => [
                            "VALUE" => false,
                            "SORT" => 3000
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => false,
                            "SORT" => 3000
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => false,
                            "SORT" => 3000
                        ]
                    ];
                } elseif (in_array($propCode, $arSecond)) {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 2000,
                        "SHOW" => false,
                        "SET_SORT" => false,
                        "PARAM_SEARCH" => [
                            "VALUE" => false,
                            "SORT" => 2000
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => false,
                            "SORT" => 2000
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => true,
                            "SORT" => 2000
                        ]
                    ];
                } else {
                    $arValues = [
                        "VALUE" => $propId,
                        "SORT" => 500,
                        "SHOW" => true,
                        "SET_SORT" => true,
                        "PARAM_SEARCH" => [
                            "VALUE" => true,
                            "SORT" => 500
                        ],
                        "SETTINGS_WINDOW" => [
                            "VALUE" => true,
                            "SORT" => 500
                        ],
                        "SPECIFICATION" => [
                            "VALUE" => true,
                            "SORT" => 500
                        ]
                    ];
                }

                //$arValues = ["ID" => $arProp["IBLOCK_PROPERTY_ID"]];

                $arRegulations[$propCode] = $arValues;
            }
        }
    }


    usort($arRegulations, function ($a, $b){return ($a["SORT"] - $b["SORT"]) <=> 0;});

}


if(!empty($arRegulations)){


    //create HTML for return ajax
    $count = intval(htmlspecialchars($_GET["SELECT_LEN"]));
    ob_start();
    //echo "<pre>";
    //print_r($arRegulations);
    //echo "</pre>";

    foreach ($arRegulations as $arReg){

        $n = preg_replace("/[^a-zA-Z0-9_\\[\\]]/", "", "PROP[$PROP_REG_AJAX][n$count][VALUE]");
        $md5 = md5($n);

        ?>

        <tr>
            <td>
                <div style="display: inline-block">
                    <input name='PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SORT]' id='PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SORT]'
                           value="<?= $arReg["SORT"] ?>" type="text" size="5">
                    <input name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]" id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]"
                           value="<?= $arReg["VALUE"] ?>" onblur="propNameAjax(this, 'PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]')"
                           size="5" type="text">

                    <input type="button" value="..."
                           onclick="jsUtils.OpenWindow('/bitrix/admin/mcart.sortcatalogsm/iblock_property_search.php?lang=ru&IBLOCK_ID=<?= $IBLOCK_ID ?>&n=PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]&k=n0&propid=<?=$PROP_REG_AJAX?>', 900, 700);">

                    <span id="sp_PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]_n0"><?=$arPropsNames[$arReg["VALUE"]]["NAME"]?></span>

                    <span onclick='deleteItem(this)' style='color: red;'>X</span>
                    </br>
                    <table>
                        <tr>
                            <th><?= GetMessage("SORTCATALOG_SHORT_NAME"); ?></th>
                            <th><?= GetMessage("SORTCATALOG_HINT"); ?></th>
                            <th>Выводить на листе</th>
                        </tr>
                        <tr>
                            <td id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]_HINT_S"><input name=''
                                                                                                         id='<?=$PROP_REG_AJAX?>_<?= $arReg["VALUE"] ?>_S'
                                                                                                         value='<?=$arPropsNames[$arReg["VALUE"]]["NAME"]?>'
                                                                                                         type="text" size="20"
                                                                                                         onchange="hintProperty(this, 'S', '<?= $arReg["VALUE"] ?>')">
                            </td>
                            <td id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]_HINT_L"><input name=''
                                                                                                         id='<?=$PROP_REG_AJAX?>_<?= $arReg["VALUE"] ?>_L'
                                                                                                         value='<?=$arPropsNames[$arReg["VALUE"]]["HINT"]?>'
                                                                                                         type="text" size="20"
                                                                                                         onchange="hintProperty(this, 'L', '<?= $arReg["VALUE"] ?>')">
                            </td>
                            <td align="center">
                                <input type="checkbox"
                                       id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SHOW]"
                                       name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SHOW]"
                                       <?if($arReg["SHOW"]):?>checked<?endif;?>
                                >
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="display: inline-block">
                    <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SET_SORT]">
                        Настройка сортировки
                    </label>
                    <input
                            id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SET_SORT]"
                            name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SET_SORT]"
                            type="checkbox"
                            <?if($arReg["SET_SORT"]):?>checked<?endif;?>
                    >
                    <table>
                        <tr>
                            <td>
                                <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][PARAM_SEARCH][VALUE]">
                                    Параметрический Поиск
                                </label>
                            </td>
                            <td>
                                <input
                                        id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][PARAM_SEARCH][VALUE]"
                                        name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][PARAM_SEARCH][VALUE]"
                                        type="checkbox"
                                        <?if($arReg["PARAM_SEARCH"]["VALUE"]):?>checked<?endif;?>
                                >
                            </td>
                            <td>
                                <input
                                        id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][PARAM_SEARCH][SORT]"
                                        name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][PARAM_SEARCH][SORT]"
                                        value="<?=$arReg["PARAM_SEARCH"]["SORT"];?>"
                                        type="text" size="5"
                                >
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SETTINGS_WINDOW][VALUE]">
                                    Шестерёнка
                                </label>
                            </td>
                            <td>
                                <input
                                        id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SETTINGS_WINDOW][VALUE]"
                                        name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SETTINGS_WINDOW][VALUE]"
                                        type="checkbox"
                                        <?if($arReg["SETTINGS_WINDOW"]["VALUE"]):?>checked<?endif;?>
                                >
                            </td>
                            <td>
                                <input
                                        id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SETTINGS_WINDOW][SORT]"
                                        name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SETTINGS_WINDOW][SORT]"
                                        value="<?=$arReg["SETTINGS_WINDOW"]["SORT"];?>"
                                        type="text" size="5"
                                >
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SPECIFICATION][VALUE]">
                                    Спецификация
                                </label>
                            </td>
                            <td>
                                <input
                                        id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SPECIFICATION][VALUE]"
                                        name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SPECIFICATION][VALUE]"
                                        type="checkbox"
                                        <?if($arReg["SPECIFICATION"]["VALUE"]):?>checked<?endif;?>
                                >
                            </td>
                            <td>
                                <input
                                        id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SPECIFICATION][SORT]"
                                        name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SPECIFICATION][SORT]"
                                        value="<?=$arReg["SPECIFICATION"]["SORT"];?>"
                                        type="text" size="5"
                                >
                            </td>
                        </tr>
                    </table>
                </div>

                </br>
                <hr width="60%" align="left">
                </br>
            </td>
        </tr>



        <?

        /*$ResultHTML .= "<tr><td>";

        $ResultHTML .= "<select name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE]'>";
        $ResultHTML .= "<option value=''>-</option>";
        foreach ($arProps as $key=>$val) {
            if($reg == $key){
                $ResultHTML .= "<option selected value='" . $key . "'>" . $val . "</option>";
            } else {
                $ResultHTML .= "<option value='" . $key . "'>" . $val . "</option>";
            }
        }
        $ResultHTML .= "</select> ";
        $ResultHTML .= "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][DESCRIPTION]' id='PROP[".$PROP_REG_AJAX."][n".$count."][DESCRIPTION]'
        value='200'>";
        $ResultHTML .= " <span onclick='deleteItem(this)' style='color: red;'>X</span></br>";
        $ResultHTML .= "</td></tr>";*/
        $count++;
    }

    $html_value = ob_get_contents();
    ob_end_clean();

    //$ResultHTML .= "<tr><td><input type=\"button\" value=\"Добавить\" onclick=\"addNewRow('tbdf4285bc1d07139fc6122003f756eaea')\"></td></tr>";

    //$ResultHTML .= "</tbody>";

    $GLOBALS['APPLICATION']->RestartBuffer();
    echo $html_value;
    die;
} else {
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "";
    die;
}

//return ajax result

/*echo "<pre>";
print_r($_GET);
echo "</pre>";
echo "<pre>";
print_r($pointSection);
echo "</pre>";
die;*/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>