<?
$MESS["SORTCATALOG_IBID"] = "ID инфоблока каталога";
$MESS["SORTCATALOG_NO_SELECTED"] = "Не выбрано";
$MESS["SORTCATALOG_NO_EX_CAT"] = "Нет такого каталога";
$MESS["SORTCATALOG_DOP_PARAM"] = "Дополнительные параметры";
$MESS["SORTCATALOG_PROPID"] = "ID свойства, отвечающего за настройку правил";