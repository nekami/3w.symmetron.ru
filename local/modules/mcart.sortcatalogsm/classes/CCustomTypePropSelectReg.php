<?php

class CCustomTypePropSelectReg{

//описываем поведение пользовательского свойства
    function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE'           => 'S',
            'USER_TYPE'             	=> 'table_prop_reg',
            'DESCRIPTION'           	=> 'Regulations', //именно это будет выведено в списке типов свойств во вкладке редактирования свойств ИБ
            //указываем необходимые функции, используемые в создаваемом типе
            'GetPropertyFieldHtml'  	=> array('CCustomTypePropSelectReg', 'GetPropertyFieldHtml'),
            'GetPropertyFieldHtmlMulty' => array('CCustomTypePropSelectReg', 'GetPropertyFieldHtmlMulty'),
            'ConvertToDB'           	=> array('CCustomTypePropSelectReg', 'ConvertToDB'),
            'ConvertFromDB'         	=> array('CCustomTypePropSelectReg', 'ConvertFromDB'),
            'PrepareSettings'           => array('CCustomTypePropSelectReg', 'PrepareSettings'),
            'GetSettingsHTML'           => array('CCustomTypePropSelectReg','GetSettingsHTML'),
        );
    }
    //формируем пару полей для создаваемого свойства
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        $ID = intval($_REQUEST['ID']);
        //echo "sdfsdfsdfsdf";

        ?>
        <script>
            if (typeof window[deleteItem] != 'function'){
                function deleteItem(X){
                    var tr = X.parentNode.parentNode;
                    tr.parentNode.removeChild(tr);
                }
            }

            if (typeof window[deleteItemN0] != 'function'){
                function deleteItemN0(X){
                    if(document.getElementById(X)){
                        var n0 = document.getElementById(X);
                        n0.value = "";
                        n0.parentNode.parentNode.style.display = "none";
                    }
                }
            }


            if (typeof window[hintProperty] != 'function'){
                function hintProperty(el, type, propId){
                    if(document.getElementById(propId + "_" + type)){
                        document.getElementById(propId + "_" + type).value = el.value
                    }
                }
            }



        </script>

        <?

        $IBLOCK_ID = $arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"];
        $propName = GetMessage("SORTCATALOG_NO_SELECTED");
        if(!empty($value["VALUE"])) {
            $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID, "ID" => $value["VALUE"]));
            if ($prop_fields = $properties->Fetch()) {
                $propName = $prop_fields["NAME"];
            } else {
                $propName = GetMessage("SORTCATALOG_NO_EX_CAT");
            }
        }

        $n = preg_replace("/[^a-zA-Z0-9_\\[\\]]/", "", $strHTMLControlName["VALUE"]);
        $md5 = md5($n);
        ob_start();
        ?>
        <?
        $descr = 500;
        if(!empty($value["DESCRIPTION"])){
            $descr = $value["DESCRIPTION"];
        }
        ?>

        <script>

            var IBLOCK_ID_CAT = "<?=$IBLOCK_ID?>";
            var messNoSelect = "<?=GetMessage("SORTCATALOG_NO_SELECTED")?>";
            var noProp = "<?=GetMessage("SORTCATALOG_NO_EX")?>";


            var propId = "<?=$arProperty["ID"]?>";

            if (typeof window[propNameAjax] != 'function') {
                function propNameAjax(dom, valId) {

                    arJsParams = [];
                    arJsParams["ID"] = dom.value;
                    arJsParams["IBLOCK_ID_CAT"] = IBLOCK_ID_CAT;


                    //get hinds
                    var hindSTr = document.getElementById(valId + "_HINT_S")
                    if(hindSTr)
                        var hindS = hindSTr.childNodes[0]

                    if(hindS){
                        var hindSProp = document.getElementById(arJsParams["ID"] + "_S")
                        if(hindSProp) {
                            hindS.id = propId + "_" + arJsParams["ID"] + "_S"
                            hindS.value = hindSProp.value
                        } else {
                            hindS.value = ""
                        }
                    }


                    var hindLTr = document.getElementById(valId + "_HINT_L")
                    if(hindLTr)
                        var hindL = hindLTr.childNodes[0]


                    if(hindL){
                        var hindLProp = document.getElementById(arJsParams["ID"] + "_L")
                        if(hindLProp) {
                            hindL.id = propId + "_" + arJsParams["ID"] + "_L"
                            hindL.value = hindLProp.value
                        } else {
                            hindL.value = ""
                        }
                    }



                    if (String(arJsParams["ID"]) !== "") {
                        BX.ajax.get(
                            //"/local/php_interface/ajax.php",
                            "/local/modules/mcart.sortcatalogsm/lib/prop_name_ajax.php",
                            arJsParams,
                            function (data) {

                                var varIdSpan = "sp_" + String(dom.id) + "_n0";
                                var parrent = dom.parentNode;
                                var childs = parrent.childNodes;

                                if (String(data) !== "") {

                                    for (var i = 0; i < parrent.childNodes.length; i++) {
                                        if (String(childs[i].id) === varIdSpan) {
                                            childs[i].innerHTML = String(data);
                                        }
                                    }
                                } else {


                                    for (var i = 0; i < parrent.childNodes.length; i++) {
                                        if (String(childs[i].id) === varIdSpan) {
                                            childs[i].innerHTML = noProp;
                                        }
                                    }

                                }
                            }
                        );
                    } else {
                        var varIdSpan = "sp_" + String(dom.id) + "_n0";
                        var parrent = dom.parentNode;
                        var childs = parrent.childNodes;


                        for (var i = 0; i < parrent.childNodes.length; i++) {
                            if (String(childs[i].id) === varIdSpan) {
                                childs[i].innerHTML = messNoSelect;
                            }
                        }
                    }
                }
            }

        </script>

        <input name='<?=$strHTMLControlName["DESCRIPTION"]?>' id='<?=$strHTMLControlName["DESCRIPTION"]?>' value='<?=$descr;?>' type="text" size="5">
        <input name="<?=$strHTMLControlName["VALUE"]?>" id="<?=$strHTMLControlName["VALUE"]?>" value="<?=$value["VALUE"]?>" onblur="propNameAjax(this, '<?=$strHTMLControlName["VALUE"]?>')" size="5" type="text">

        <input type="button" value="..." onclick="jsUtils.OpenWindow('/bitrix/admin/mcart.sortcatalogsm/iblock_property_search.php?lang=ru&IBLOCK_ID=<?=$IBLOCK_ID?>&n=<?=$strHTMLControlName["VALUE"]?>&k=n0&propid=<?=$arProperty["ID"]?>', 900, 700);">

        <span id="sp_<?=$strHTMLControlName["VALUE"]?>_n0"><?=$propName?></span>

        <?if($strHTMLControlName["VALUE"] != "PROP[".$arProperty["ID"]."][n0][VALUE]"):?>
            <span onclick='deleteItem(this)' style='color: red;'>X</span>
        <?else:?>
            <span onclick='deleteItemN0("PROP[<?=$arProperty["ID"]?>][n0][VALUE]")' style='color: red;'>X</span>
        <?endif;?>
        </br>
            <table>
                <tr>
                    <th><?=GetMessage("SORTCATALOG_SHORT_NAME");?></th>
                    <th><?=GetMessage("SORTCATALOG_HINT");?></th>
                </tr>
                <tr>
                    <td id="<?=$strHTMLControlName["VALUE"]?>_HINT_S"><input name='' id='<?=$arProperty["ID"]?>_<?=$value["VALUE"]?>_S' value='' type="text" size="20" onchange="hintProperty(this, 'S', '<?=$value["VALUE"]?>')"></td>
                    <td id="<?=$strHTMLControlName["VALUE"]?>_HINT_L"><input name='' id='<?=$arProperty["ID"]?>_<?=$value["VALUE"]?>_L' value='' type="text" size="20" onchange="hintProperty(this, 'L', '<?=$value["VALUE"]?>')"></td>
                </tr>
            </table>
        <script>
            document.getElementById("sp_<?=$strHTMLControlName["VALUE"]?>_n0").innerHTML = '<?=$propName?>';
        </script>
        </br>
        <hr width="60%" align="left">
        </br>
        <?
        $html_value = ob_get_contents();
        ob_end_clean();

        return $html_value;

    }

    function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName) {
        $IBLOCK_ID = $arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"];
        ?>
        <script>
            if (typeof window[deleteItem] != 'function'){
                function deleteItem(X){
                    var tr = X.parentNode.parentNode;
                    tr.parentNode.removeChild(tr);
                }
            }

            if (typeof window[hintProperty] != 'function'){
                function hintProperty(el, type, propId){
                    if(document.getElementById(propId + "_" + type)){
                        document.getElementById(propId + "_" + type).value = el.value
                    }
                }
            }
        </script>

        <script>

            var IBLOCK_ID_CAT = "<?=$IBLOCK_ID?>";
            var messNoSelect = "<?=GetMessage("SORTCATALOG_NO_SELECTED")?>";
            var noProp = "<?=GetMessage("SORTCATALOG_NO_EX")?>";


            var propId = "<?=$arProperty["ID"]?>";

            if (typeof window[propNameAjax] != 'function') {
                function propNameAjax(dom, valId) {

                    arJsParams = [];
                    arJsParams["ID"] = dom.value;
                    arJsParams["IBLOCK_ID_CAT"] = IBLOCK_ID_CAT;


                    //get hinds
                    var hindSTr = document.getElementById(valId + "_HINT_S")
                    if(hindSTr)
                        var hindS = hindSTr.childNodes[0]

                    if(hindS){
                        var hindSProp = document.getElementById(arJsParams["ID"] + "_S")
                        if(hindSProp) {
                            hindS.id = propId + "_" + arJsParams["ID"] + "_S"
                            hindS.value = hindSProp.value
                        } else {
                            hindS.value = ""
                        }
                    }


                    var hindLTr = document.getElementById(valId + "_HINT_L")
                    if(hindLTr)
                        var hindL = hindLTr.childNodes[0]


                    if(hindL){
                        var hindLProp = document.getElementById(arJsParams["ID"] + "_L")
                        if(hindLProp) {
                            hindL.id = propId + "_" + arJsParams["ID"] + "_L"
                            hindL.value = hindLProp.value
                        } else {
                            hindL.value = ""
                        }
                    }



                    if (String(arJsParams["ID"]) !== "") {
                        BX.ajax.get(
                            //"/local/php_interface/ajax.php",
                            "/local/modules/mcart.sortcatalogsm/admin/prop_name_ajax.php",
                            arJsParams,
                            function (data) {
                                console.log(data)
                                var varIdSpan = "sp_" + String(dom.id) + "_n0";
                                console.log(String(dom.id))
                                var parrent = dom.parentNode;
                                var childs = parrent.childNodes;

                                if (String(data) !== "") {

                                    for (var i = 0; i < parrent.childNodes.length; i++) {
                                        if (String(childs[i].id) === varIdSpan) {
                                            childs[i].innerHTML = String(data);
                                        }
                                    }
                                } else {


                                    for (var i = 0; i < parrent.childNodes.length; i++) {
                                        if (String(childs[i].id) === varIdSpan) {
                                            childs[i].innerHTML = noProp;
                                        }
                                    }

                                }
                            }
                        );
                    } else {
                        var varIdSpan = "sp_" + String(dom.id) + "_n0";
                        var parrent = dom.parentNode;
                        var childs = parrent.childNodes;


                        for (var i = 0; i < parrent.childNodes.length; i++) {
                            if (String(childs[i].id) === varIdSpan) {
                                childs[i].innerHTML = messNoSelect;
                            }
                        }
                    }
                }
            }

            countProps = 0

            if (typeof window[addNewProperty] != 'function') {
                function addNewProperty (el) {
                    console.log(el)
                    num = "n" + countProps
                    html = `
                        <div style="display: inline-block">
                        <input name='<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SORT]' id='<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SORT]'
                               value='500' type="text" size="5">
                        <input name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][VALUE]" id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][VALUE]"
                               value="" onblur="propNameAjax(this, '<?=$strHTMLControlName["VALUE"]?>[` + num + `]')"
                               size="5" type="text">

                        <input type="button" value="..."
                               onclick="jsUtils.OpenWindow('/bitrix/admin/mcart.sortcatalogsm/iblock_property_search.php?lang=ru&IBLOCK_ID=<?= $IBLOCK_ID ?>&n=<?= $strHTMLControlName["VALUE"] ?>[` + num + `][VALUE][VALUE]&k=n0&propid=<?= $arProperty["ID"] ?>', 900, 700);">

                        <span id="sp_<?= $strHTMLControlName["VALUE"] ?>[` + num + `][VALUE][VALUE]_n0"><?=GetMessage("SORTCATALOG_NO_EX_CAT")?></span>

                        <span onclick='deleteItem(this)' style='color: red;'>X</span>
                        </br>
                        <table>
                            <tr>
                                <th><?= GetMessage("SORTCATALOG_SHORT_NAME"); ?></th>
                                <th><?= GetMessage("SORTCATALOG_HINT"); ?></th>
                                <th>Выводить на листе</th>
                            </tr>
                            <tr>
                                <td id="<?= $strHTMLControlName["VALUE"] ?>[` + num + `][VALUE][VALUE]_HINT_S"><input name=''
                                                                                           id='<?= $arProperty["ID"] ?>__S'
                                                                                           value='' type="text" size="20"
                                                                                           onchange="hintProperty(this, 'S', '')">
                                </td>
                                <td id="<?= $strHTMLControlName["VALUE"] ?>[` + num + `][VALUE][VALUE]_HINT_L"><input name=''
                                                                                           id='<?= $arProperty["ID"] ?>__L'
                                                                                           value='' type="text" size="20"
                                                                                           onchange="hintProperty(this, 'L', '')">
                                </td>
                                <td align="center">
                                    <input type="checkbox"
                                           id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SHOW]"
                                           name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SHOW]"
                                    >
                                </td>
                            </tr>
                        </table>
                    </div>




                    <div style="display: inline-block">
                        <label for="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SET_SORT]">
                            Настройка сортировки
                        </label>
                        <input
                            id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SET_SORT]"
                            name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SET_SORT]"
                            type="checkbox"
                        >
                        <table>
                            <tr>
                                <td>
                                    <label for="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][PARAM_SEARCH][VALUE]">
                                        Параметрический Поиск
                                    </label>
                                </td>
                                <td>
                                    <input
                                        id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][PARAM_SEARCH][VALUE]"
                                        name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][PARAM_SEARCH][VALUE]"
                                        type="checkbox"
                                    >
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][PARAM_SEARCH][SORT]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][PARAM_SEARCH][SORT]"
                                            type="text" size="5" value="500"
                                    >
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label for="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SETTINGS_WINDOW][VALUE]">
                                        Шестерёнка
                                    </label>
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SETTINGS_WINDOW][VALUE]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SETTINGS_WINDOW][VALUE]"
                                            type="checkbox"
                                    >
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SETTINGS_WINDOW][SORT]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SETTINGS_WINDOW][SORT]"
                                            type="text" size="5" value="500"
                                    >
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label for="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SPECIFICATION][VALUE]">
                                        Спецификация
                                    </label>
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SPECIFICATION][VALUE]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SPECIFICATION][VALUE]"
                                            type="checkbox"
                                    >
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SPECIFICATION][SORT]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[` + num + `][VALUE][SPECIFICATION][SORT]"
                                            type="text" size="5" value="500"
                                    >
                                </td>
                            </tr>
                        </table>
                    </div>
                    </br> <hr width='60%' align='left'> </br>`

                    table = el.parentNode.parentNode.parentNode
                    //table.innerHTML += "<tr><td><input type='button' value='Добавить' onclick='addNewProperty(this)'></td></tr>"
                    var currentRow = table.insertRow(-1);
                    var currentCell = currentRow.insertCell(-1);
                    currentCell.innerHTML = "<tr><td><input type='button' value='Добавить' onclick='addNewProperty(this)'></td></tr>";

                    el.parentNode.innerHTML = html
                    countProps++
                }
            }

        </script>


        <?
        //$propName = GetMessage("SORTCATALOG_NO_SELECTED");
        $arNames = [];
        $arNamesIDs = [];
        foreach ($arValues as $key => $value) {
            $arNamesIDs[] = $value["VALUE"]["VALUE"];
        }

        if(!empty($arNamesIDs)) {
            $properties = Bitrix\Iblock\PropertyTable::getList([
                "filter" => ["ID" => $arNamesIDs, "ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID],
                "select" => ["ID", "NAME"]
            ]);
            //$properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID, "ID" => $arNamesIDs));
            while ($propFields = $properties->Fetch()) {
                $arNames[$propFields["ID"]] = $propFields["NAME"];
            }
        }

        ob_start();

        foreach ($arValues as $key => $arValue){
            $arValues[$key]["KEY"] = $key;
        }

        usort($arValues, function ($a, $b){return ($a["VALUE"]["SORT"] - $b["VALUE"]["SORT"]) <=> 0;});

        $arValues2 = $arValues;
        $arValues = [];
        foreach ($arValues2 as $key => $arValue){
            $keyCur = $arValue["KEY"];
            unset($arValue["KEY"]);
            $arValues[$keyCur] = $arValue;
        }


        foreach ($arValues as $key => $value) {
            ?>
            <tr>
                <td>
                    <div style="display: inline-block">
                        <input name='<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SORT]' id='<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SORT]'
                               value='<?= $value["VALUE"]["SORT"]; ?>' type="text" size="5">
                        <input name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][VALUE]" id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][VALUE]"
                               value="<?= $value["VALUE"]["VALUE"] ?>" onblur="propNameAjax(this, '<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][VALUE]')"
                               size="5" type="text">

                        <input type="button" value="..."
                               onclick="jsUtils.OpenWindow('/bitrix/admin/mcart.sortcatalogsm/iblock_property_search.php?lang=ru&IBLOCK_ID=<?= $IBLOCK_ID ?>&n=<?= $strHTMLControlName["VALUE"] ?>[<?=$key?>][VALUE][VALUE]&k=n0&propid=<?= $arProperty["ID"] ?>', 900, 700);">

                        <span id="sp_<?= $strHTMLControlName["VALUE"] ?>[<?=$key?>][VALUE][VALUE]_n0"><?=$arNames[$value["VALUE"]["VALUE"]] ?:GetMessage("SORTCATALOG_NO_EX_CAT")?></span>

                        <span onclick='deleteItem(this)' style='color: red;'>X</span>
                        </br>
                        <table>
                            <tr>
                                <th><?= GetMessage("SORTCATALOG_SHORT_NAME"); ?></th>
                                <th><?= GetMessage("SORTCATALOG_HINT"); ?></th>
                                <th>Выводить на листе</th>
                            </tr>
                            <tr>
                                <td id="<?= $strHTMLControlName["VALUE"] ?>[<?=$key?>][VALUE][VALUE]_HINT_S"><input name=''
                                                                                           id='<?= $arProperty["ID"] ?>_<?= $value["VALUE"]["VALUE"] ?>_S'
                                                                                           value='' type="text" size="20"
                                                                                           onchange="hintProperty(this, 'S', '<?= $value["VALUE"]["VALUE"] ?>')">
                                </td>
                                <td id="<?= $strHTMLControlName["VALUE"] ?>[<?=$key?>][VALUE][VALUE]_HINT_L"><input name=''
                                                                                           id='<?= $arProperty["ID"] ?>_<?= $value["VALUE"]["VALUE"] ?>_L'
                                                                                           value='' type="text" size="20"
                                                                                           onchange="hintProperty(this, 'L', '<?= $value["VALUE"]["VALUE"] ?>')">
                                </td>
                                <td align="center">
                                    <input type="checkbox"
                                           id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SHOW]"
                                           name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SHOW]"
                                           <?if($value["VALUE"]["SHOW"] == "Y"):?>checked<?endif;?>
                                    >
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="display: inline-block">
                        <label for="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SET_SORT]">
                            Настройка сортировки
                        </label>
                        <input
                            id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SET_SORT]"
                            name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SET_SORT]"
                            <?if($value["VALUE"]["SET_SORT"] == "Y"):?>checked<?endif;?>
                            type="checkbox"
                        >
                        <table>
                            <tr>
                                <td>
                                    <label for="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][PARAM_SEARCH][VALUE]">
                                        Параметрический Поиск
                                    </label>
                                </td>
                                <td>
                                    <input
                                        id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][PARAM_SEARCH][VALUE]"
                                        name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][PARAM_SEARCH][VALUE]"
                                        <?if($value["VALUE"]["PARAM_SEARCH"]["VALUE"] == "Y"):?>checked<?endif;?>
                                        type="checkbox"
                                    >
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][PARAM_SEARCH][SORT]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][PARAM_SEARCH][SORT]"
                                            value="<?=$value["VALUE"]["PARAM_SEARCH"]["SORT"]?>"
                                            type="text" size="5"
                                    >
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label for="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SETTINGS_WINDOW][VALUE]">
                                        Шестерёнка
                                    </label>
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SETTINGS_WINDOW][VALUE]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SETTINGS_WINDOW][VALUE]"
                                            <?if($value["VALUE"]["SETTINGS_WINDOW"]["VALUE"] == "Y"):?>checked<?endif;?>
                                            type="checkbox"
                                    >
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SETTINGS_WINDOW][SORT]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SETTINGS_WINDOW][SORT]"
                                            value="<?=$value["VALUE"]["SETTINGS_WINDOW"]["SORT"]?>"
                                            type="text" size="5"
                                    >
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label for="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SPECIFICATION][VALUE]">
                                        Спецификация
                                    </label>
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SPECIFICATION][VALUE]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SPECIFICATION][VALUE]"
                                            <?if($value["VALUE"]["SPECIFICATION"]["VALUE"] == "Y"):?>checked<?endif;?>
                                            type="checkbox"
                                    >
                                </td>
                                <td>
                                    <input
                                            id="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SPECIFICATION][SORT]"
                                            name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][VALUE][SPECIFICATION][SORT]"
                                            value="<?=$value["VALUE"]["SPECIFICATION"]["SORT"]?>"
                                            type="text" size="5"
                                    >
                                </td>
                            </tr>
                        </table>
                    </div>

                    </br>
                    <hr width="60%" align="left">
                    </br>
                </td>
            </tr>
            <?
        }
        ?>
        <tr><td><input type="button" value="Добавить" onclick="addNewProperty(this)"></td></tr>
        <?
        $html_value = ob_get_contents();
        ob_end_clean();

        return $html_value;
        
    }

    function PrepareSettings($arFields)
    {
        $val2 = intval($arFields["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"]);
        return array("CATALOG_IBLOCK_ID" => $val2);
    }
    /*
     * <td class="adm-detail-content-cell-r">
                <input type="hidden" id="PROPERTY_SECTION_PROPERTY_N" name="PROPERTY_SECTION_PROPERTY" value="N">
                <input type="checkbox" id="PROPERTY_SECTION_PROPERTY_Y" name="PROPERTY_SECTION_PROPERTY" value="Y" checked="checked" class="adm-designed-checkbox"><label class="adm-designed-checkbox-label" for="PROPERTY_SECTION_PROPERTY_Y" title=""></label>
            </td>
     */

    function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array( 'MULTIPLE', 'SEARCHABLE', 'FILTRABLE', 'WITH_DESCRIPTION', 'MULTIPLE_CNT', 'ROW_COUNT', 'COL_COUNT' ,'DEFAULT_VALUE'), //will hide the field
            "USER_TYPE_SETTINGS_TITLE" => GetMessage("SORTCATALOG_DOP_PARAM")
        );
        $catalog_iblock_id = $arProperty['USER_TYPE_SETTINGS']["CATALOG_IBLOCK_ID"];

        $ck2 = "<input type='text' name='".$strHTMLControlName["NAME"]."[CATALOG_IBLOCK_ID]' id='".$strHTMLControlName["NAME"]."[CATALOG_IBLOCK_ID]' value='".$catalog_iblock_id."'/>";

        return

            '<tr>
        <td>'.GetMessage("SORTCATALOG_IBID").'</td>
        <td>'.$ck2.'</td>
        </tr>
		';

    }


    //сохраняем в базу
    function ConvertToDB($arProperty, $value){

        $arValues = $value["VALUE"];
        //echo "<pre>";
        //print_r($arValues);
        //echo "</pre>";
        //die();
        //delete empty value

        if(empty($arValues["VALUE"]))
            return false;

        $arValues["SORT"] = $arValues["SORT"]?:500;
        $arValues["SHOW"] = $arValues["SHOW"] == "on" ? "Y" : "N";
        $arValues["SET_SORT"] = $arValues["SET_SORT"] == "on" ? "Y" : "N";

        $arValues["PARAM_SEARCH"]["VALUE"] = $arValues["PARAM_SEARCH"]["VALUE"] == "on" ? "Y" : "N";
        $arValues["PARAM_SEARCH"]["SORT"] = $arValues["PARAM_SEARCH"]["SORT"]?:500;

        $arValues["SETTINGS_WINDOW"]["VALUE"] = $arValues["SETTINGS_WINDOW"]["VALUE"] == "on" ? "Y" : "N";
        $arValues["SETTINGS_WINDOW"]["SORT"] = $arValues["SETTINGS_WINDOW"]["SORT"]?:500;

        $arValues["SPECIFICATION"]["VALUE"] = $arValues["SPECIFICATION"]["VALUE"] == "on" ? "Y" : "N";
        $arValues["SPECIFICATION"]["SORT"] = $arValues["SPECIFICATION"]["SORT"]?:500;

        //ID priority sort
        $IBLOCK_ID = $arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"];
        $properties = Bitrix\Iblock\PropertyTable::getList([
            "filter" => ["ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID, "CODE" => [
                    "ARTICLE", "PRODUCERID", "DESCRIPTION", "ACTIVE_STATUS", "RETAIL_PRICE"
            ]],
            "select" => ["ID", "CODE"]
        ]);

        $arIDsCodes = [];
        while ($propFields = $properties->Fetch()) {
            $arIDsCodes[$propFields["ID"]] = $propFields["CODE"];
        }

        if($arIDsCodes[$arValues["VALUE"]] == "ARTICLE")
            $arValues["SORT"] = 10;
        if($arIDsCodes[$arValues["VALUE"]] == "PRODUCERID")
            $arValues["SORT"] = 20;
        if($arIDsCodes[$arValues["VALUE"]] == "DESCRIPTION")
            $arValues["SORT"] = 30;
        if($arIDsCodes[$arValues["VALUE"]] == "ACTIVE_STATUS")
            $arValues["SORT"] = 1010;
        if($arIDsCodes[$arValues["VALUE"]] == "RETAIL_PRICE")
            $arValues["SORT"] = 1020;

        $value["VALUE"] = serialize($arValues);
        return $value;
    }
    //читаем из базы
    function ConvertFromDB($arProperty, $value){
        $value["VALUE"] = unserialize($value["VALUE"]);
        return $value;
    }
}