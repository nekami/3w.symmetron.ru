<?

class CCustomTypeSelectSectionReg{

//описываем поведение пользовательского свойства
    function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE'             => 'S',
            'USER_TYPE'             	=> 'select_section_reg',
            'DESCRIPTION'           	=> 'Select section for reg', //именно это будет выведено в списке типов свойств во вкладке редактирования свойств ИБ
            //указываем необходимые функции, используемые в создаваемом типе
            'GetPropertyFieldHtml'  	=> array('CCustomTypeSelectSectionReg', 'GetPropertyFieldHtml'),
            'ConvertToDB'           	=> array('CCustomTypeSelectSectionReg', 'ConvertToDB'),
            'ConvertFromDB'         	=> array('CCustomTypeSelectSectionReg', 'ConvertFromDB'),
            'PrepareSettings'           => array('CCustomTypeSelectSectionReg', 'PrepareSettings'),
            'GetSettingsHTML'           => array('CCustomTypeSelectSectionReg','GetSettingsHTML'),
        );
    }
    //формируем пару полей для создаваемого свойства
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {

        $IBLOCK_ID_REG = intval($_GET['IBLOCK_ID']);

        $IBLOCK_ID = $arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"];

        $secName = GetMessage("SORTCATALOG_NO_SELECTED");
        if(!empty($value["VALUE"])) {
            $uf_arresult = CIBlockSection::GetList(Array("SORT" => "­­ASC"), Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $value["VALUE"]), false, array("NAME"));
            if ($uf_value = $uf_arresult->Fetch()) {
                $secName = $uf_value["NAME"];
            } else {
                $secName = GetMessage("SORTCATALOG_NO_EX");
            }
        }

        $n = preg_replace("/[^a-zA-Z0-9_\\[\\]]/", "", $strHTMLControlName["VALUE"]);
        $md5 = md5($n);
        ob_start();

        ?>
        <?
        $arJsParams = array(
            "IBLOCK_ID" => $arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"],
            "PROP_REG_AJAX" => $arProperty["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"],
            //"SELECT_SECTION" => $strHTMLControlName["VALUE"],
            "IBLOCK_ID_REG" => $IBLOCK_ID_REG,
            "PROP_CODE_SELECT_SECTION" => $arProperty["CODE"],
            "ELEMENT_ID" => htmlspecialchars($_GET["ID"])
        );
        //print_r($arJsParams);
        $arJsParamsUrl = "";

        foreach ($arJsParams as $key => $param){
            $arJsParamsUrl .= '&JS['.$key.']='.$param;
        }
        ?>

        <script>
            //BX.ready(function(){
			//console.log(typeof(arSectionIDs))
				if(typeof(arSectionIDs) == "undefined"){
					var arSectionIDs = {}
					BX.ready(function() {
                        //console.log(document.getElementById("save"))

                        document.getElementById("save").parentNode.innerHTML +=
                            '<input type="button" class="button" onclick="changeFunc(this);" value="Подгрузить">'
                    })
				}
                if (window["changeFunc"] != 'function') {

                    function changeFunc (elem) {
                        elem.style.display = "none"
                        let arJsParams = <?=CUtil::PhpToJSObject($arJsParams);?>;
                        let arSecIDs = Object.keys(arSectionIDs).map(function(key) {
                            return arSectionIDs[key];
                        });
                        arJsParams["SECTIONS"] = arSecIDs

                        if (arJsParams.length <= 0)
                            return false;
                        var idSelectAjax = "";
                        var PropAjax = BX('tr_PROPERTY_' + arJsParams.PROP_REG_AJAX);

                        for (var i = 0; i < PropAjax.childNodes.length; i++) {
                            if (String(PropAjax.childNodes[i].className) === "adm-detail-content-cell-r") {
                                for (var j = 0; j < PropAjax.childNodes[i].childNodes.length; j++) {
                                    if (String(PropAjax.childNodes[i].childNodes[j].id) !== "") {
                                        idSelectAjax = String(PropAjax.childNodes[i].childNodes[j].id);
                                    }
                                }
                            }
                        }

                        if (idSelectAjax !== "") {
                            var selectAjax = BX(idSelectAjax).childNodes[0];
                            //var firstSelect = selectAjax.childNodes[0].childNodes[0].childNodes[0];
                            //if(Number(firstSelect.options[firstSelect.selectedIndex].value) === 0){
                            //var sectionSelect = BX(arJsParams.SELECT_SECTION);
                            //arJsParams["SECTION_VAL"] = sectionSelect.value;
                            countSelNaw = 1;
                            sel = selectAjax.childNodes[0];

                            while (sel !== undefined) {
                                sel = selectAjax.childNodes[countSelNaw];
                                countSelNaw++;
                            }
                            //console.log(countSelNaw);
                            arJsParams["SELECT_LEN"] = countSelNaw;
                            if (String(arJsParams["SECTION_VAL"]) !== "") {
                                BX.ajax.get(
                                    //"/local/php_interface/ajax.php",
                                    "/bitrix/admin/mcart.sortcatalogsm/ajax.php",
                                    arJsParams,
                                    function (data) {
                                        if (String(data) !== "") {
                                            selectAjax.innerHTML = data + selectAjax.innerHTML;
                                        }
                                    }
                                );
                            }

                            //}
                        }
                        //console.log("fffffffffffffff");

                    }
                    //BX(arJsParams.SELECT_SECTION).onchange = changeFunc();

                }

            //});

        </script>

        <script>

            var IBLOCK_ID_CAT = "<?=$IBLOCK_ID?>";
            var messNoSelect = "<?=GetMessage("SORTCATALOG_NO_SELECTED")?>";
            var noProp = "<?=GetMessage("SORTCATALOG_NO_EX")?>";
            if (typeof window["categorNameAjax"] != 'function') {
                function categorNameAjax(dom) {
                    //console.log(dom)
                    arJsParams = [];
                    if(dom.value)
                        arSectionIDs[dom.id] = dom.value
                    arJsParams["ID"] = dom.value;
                    arJsParams["IBLOCK_ID_CAT"] = IBLOCK_ID_CAT;
                    if (String(arJsParams["ID"]) !== "") {
                        BX.ajax.get(
                            "/local/modules/mcart.sortcatalogsm/admin/categor_name_ajax.php",
                            arJsParams,
                            function (data) {

                                var varIdSpan = "sp_" + String(dom.id) + "_n0";
                                var parrent = dom.parentNode;
                                var childs = parrent.childNodes;

                                if (String(data) !== "") {
                                    for (var i = 0; i < parrent.childNodes.length; i++) {
                                        if (String(childs[i].id) === varIdSpan) {
                                            childs[i].innerHTML = String(data);
                                        }
                                    }
                                } else {

                                    for (var i = 0; i < parrent.childNodes.length; i++) {
                                        if (String(childs[i].id) === varIdSpan) {
                                            childs[i].innerHTML = noProp;
                                        }
                                    }

                                }
                            }
                        );
                    } else {
                        var varIdSpan = "sp_" + String(dom.id) + "_n0";
                        var parrent = dom.parentNode;
                        var childs = parrent.childNodes;
                        for (var i = 0; i < parrent.childNodes.length; i++) {
                            if (String(childs[i].id) === varIdSpan) {
                                childs[i].innerHTML = messNoSelect;
                            }
                        }
                    }
                }
            }

        </script>
        <input name="<?=$strHTMLControlName["VALUE"]?>" id="<?=$strHTMLControlName["VALUE"]?>" value="<?=$value["VALUE"]?>" onblur="categorNameAjax(this)" size="5" type="text">

        <input type="button" value="..." onclick="jsUtils.OpenWindow('/bitrix/admin/mcart.sortcatalogsm/iblock_section_search.php?lang=ru&IBLOCK_ID=<?=$IBLOCK_ID?>&n=<?=$strHTMLControlName["VALUE"]?>&k=n0<?=$arJsParamsUrl?>', 900, 700);">

        <span id="sp_<?=$strHTMLControlName["VALUE"]?>_n0" onload="this.innerHTML = '<?=$secName?>'"><?=$secName?></span>
        <script>
			//BX.ready(function(){
            document.getElementById("sp_<?=$strHTMLControlName["VALUE"]?>_n0").innerHTML = '<?=$secName?>';
            <?if(!empty($value["VALUE"])):?>
                arSectionIDs["<?=$strHTMLControlName["VALUE"]?>"] = "<?=$value["VALUE"]?>"
            <?endif;?>
				//})
        </script>
        <?

        $html_select = ob_get_contents();
        ob_end_clean();
        return $html_select;

    }

    function PrepareSettings($arFields)
    {
        $val1 = intval($arFields["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"]);
        $val2 = intval($arFields["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"]);
        return array("PROP_REG_AJAX" => $val1, "CATALOG_IBLOCK_ID" => $val2);
    }
    /*
     * <td class="adm-detail-content-cell-r">
                <input type="hidden" id="PROPERTY_SECTION_PROPERTY_N" name="PROPERTY_SECTION_PROPERTY" value="N">
                <input type="checkbox" id="PROPERTY_SECTION_PROPERTY_Y" name="PROPERTY_SECTION_PROPERTY" value="Y" checked="checked" class="adm-designed-checkbox"><label class="adm-designed-checkbox-label" for="PROPERTY_SECTION_PROPERTY_Y" title=""></label>
            </td>
     */

    function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array( 'MULTIPLE', 'SEARCHABLE', 'FILTRABLE', 'WITH_DESCRIPTION', 'MULTIPLE_CNT', 'ROW_COUNT', 'COL_COUNT' ,'DEFAULT_VALUE'), //will hide the field
            "USER_TYPE_SETTINGS_TITLE" => "Дополнительные настройки"
        );
        $prop_reg_ajax = $arProperty['USER_TYPE_SETTINGS']["PROP_REG_AJAX"];
        $catalog_iblock_id = $arProperty['USER_TYPE_SETTINGS']["CATALOG_IBLOCK_ID"];

        $ck = "<input type='text' name='".$strHTMLControlName["NAME"]."[PROP_REG_AJAX]' id='".$strHTMLControlName["NAME"]."[PROP_REG_AJAX]' value='".$prop_reg_ajax."'/>";
        $ck2 = "<input type='text' name='".$strHTMLControlName["NAME"]."[CATALOG_IBLOCK_ID]' id='".$strHTMLControlName["NAME"]."[CATALOG_IBLOCK_ID]' value='".$catalog_iblock_id."'/>";

        return

            '<tr>
        <td>'.GetMessage("SORTCATALOG_PROPID").'</td>
        <td>'.$ck.'</td>
        </tr>
        <tr>
        <td>'.GetMessage("SORTCATALOG_IBID").'</td>
        <td>'.$ck2.'</td>
        </tr>
		';

    }

    //сохраняем в базу
    function ConvertToDB($arProperty, $value){
        return $value;
    }
    //читаем из базы
    function ConvertFromDB($arProperty, $value){
        return $value;
    }
}