<?php

class CCustomTypeHint{

//описываем поведение пользовательского свойства
    function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE'           => 'S',
            'USER_TYPE'             	=> 'table_prop_hints',
            'DESCRIPTION'           	=> 'Hints', //именно это будет выведено в списке типов свойств во вкладке редактирования свойств ИБ
            //указываем необходимые функции, используемые в создаваемом типе
            'GetPropertyFieldHtmlMulty'  	=> array('CCustomTypeHint', 'GetPropertyFieldHtmlMulty'),
            'ConvertToDB'           	=> array('CCustomTypeHint', 'ConvertToDB'),
            'ConvertFromDB'         	=> array('CCustomTypeHint', 'ConvertFromDB'),
            'PrepareSettings'           => array('CCustomTypeHint', 'PrepareSettings'),
            'GetSettingsHTML'           => array('CCustomTypeHint','GetSettingsHTML'),
        );
    }
    //формируем пару полей для создаваемого свойства
    function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName) {

        $arVals = array();
        foreach ($arValues as $key => $val){
            $arVals[$key] = trim($val["VALUE"]["ID"]);
        }


        $arFilter = array(
            'IBLOCK_ID' => $arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"],
        );

        $rsProperty = CIBlockProperty::GetList(
            Array("sort"=>"asc", "name"=>"asc"),
            $arFilter
        );
        ob_start();
        ?>
        <script>

            if (typeof window[hintPropertySortCat] != 'function'){
                function hintPropertySortCat(el, type, propId){
                    if(document.getElementById("<?=$arProperty["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"]?>_" + propId + "_" + type)){
                        document.getElementById("<?=$arProperty["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"]?>_" + propId + "_" + type).value = el.value
                        //document.getElementById("111").setAttribute()
                    }
                }
            }

        </script>
        <table id="table_hint_<?=$arProperty["ID"];?>" width="100%">
            <tr>
                <th align="right" style="width:40%;"><?=GetMessage("MSC_CTH_PROPERTY_NAME");?></th><th style="width:30%;"><?=GetMessage("MSC_CTH_SHORT_NAME");?></th><th style="width:30%;"><?=GetMessage("MSC_CTH_HINT");?></th>
            </tr>
            <?
            //$arPropLinks = CIBlockSectionPropertyLink::GetArray($arProperty["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"], 0);

            $arPropLinks = array();
            $props = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => 4, "CHECK_PERMISSIONS" => "N"));
            while($p = $props->Fetch())
            {
                //if(empty($p["HINT"]))
                    //$p["HINT"] = $p["NAME"];
                $arPropLinks[$p["ID"]] = $p;
            }


            $count = 0;
            while($element = $rsProperty->Fetch())
            {
                $key = array_search($element["ID"], $arVals);
                if ($key === false){
                ?>
                    <tr>
                        <td align="right"><?=$element["NAME"]." [".$element["ID"]."]: ";?><input size="5" name="<?=$strHTMLControlName["VALUE"]?>[n<?=$count?>][ID]" value="<?=$element["ID"]?>" type="text" style="display: none"></td>
                        <td><input name="<?=$strHTMLControlName["VALUE"]?>[n<?=$count?>][SHORT]" id="<?=$element["ID"] . "_S"?>" onchange="hintPropertySortCat(this, 'S', '<?=$element["ID"];?>');" value="<?=$arPropLinks[$element["ID"]]["NAME"];?>" type="text" style="width:90%;"></td>
                        <td><input name="<?=$strHTMLControlName["VALUE"]?>[n<?=$count?>][LONG]" id="<?=$element["ID"] . "_L"?>" onchange="hintPropertySortCat(this, 'L', '<?=$element["ID"];?>');" value="<?=$arPropLinks[$element["ID"]]["HINT"];?>" type="text" style="width:90%;"></td>
                    </tr>
                <?} else {?>
                    <tr>
                        <td align="right"><?=$element["NAME"]." [".$element["ID"]."]: ";?><input size="5" name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][ID]" value="<?=$element["ID"]?>" type="text" style="display: none"></td>
                        <td><input name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][SHORT]" id="<?=$element["ID"] . "_S"?>" onchange="hintPropertySortCat(this, 'S', '<?=$element["ID"];?>');" value="<?=$arValues[$key]["VALUE"]["SHORT"]?>" type="text" style="width:90%;"></td>
                        <td><input name="<?=$strHTMLControlName["VALUE"]?>[<?=$key?>][LONG]" id="<?=$element["ID"] . "_L"?>" onchange="hintPropertySortCat(this, 'L', '<?=$element["ID"];?>');" value="<?=$arValues[$key]["VALUE"]["LONG"]?>" type="text" style="width:90%;"></td>
                    </tr>

                    <script>
                        var short = document.getElementById("<?=$arProperty["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"]?>_<?=$element["ID"]?>_S")
                        if(short)
                            short.value = '<?=$arValues[$key]["VALUE"]["SHORT"]?>'

                        var long = document.getElementById("<?=$arProperty["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"]?>_<?=$element["ID"]?>_L")
                        if(long)
                            long.value = '<?=$arValues[$key]["VALUE"]["LONG"]?>'
                    </script>

                <?}
                $count++;
            }
            ?>
        </table>

        <script>
            var table = document.getElementById('table_hint_<?=$arProperty["ID"];?>')
            var parentTd = table.parentNode.parentNode.parentNode.parentNode.parentNode
            var parentTr = parentTd.parentNode
            //parentTr.childNodes[0].style.display = "none";
            if(parentTr.childNodes[0].class === "adm-detail-content-cell-l") {
                parentTr.childNodes[0].style.display = "none"
            }else {
                parentTr.childNodes[1].style.display = "none"
            }

            parentTd.setAttribute("colspan", "2")
            parentTd.setAttribute("width", "100%")

        </script>

        <?
        $html_value = ob_get_contents();
        ob_end_clean();

        return $html_value;

    }



    function PrepareSettings($arFields)
    {
        $val1 = intval($arFields["USER_TYPE_SETTINGS"]["PROP_REG_AJAX"]);
        $val2 = intval($arFields["USER_TYPE_SETTINGS"]["CATALOG_IBLOCK_ID"]);
        return array("PROP_REG_AJAX" => $val1, "CATALOG_IBLOCK_ID" => $val2);
    }
    /*
     * <td class="adm-detail-content-cell-r">
                <input type="hidden" id="PROPERTY_SECTION_PROPERTY_N" name="PROPERTY_SECTION_PROPERTY" value="N">
                <input type="checkbox" id="PROPERTY_SECTION_PROPERTY_Y" name="PROPERTY_SECTION_PROPERTY" value="Y" checked="checked" class="adm-designed-checkbox"><label class="adm-designed-checkbox-label" for="PROPERTY_SECTION_PROPERTY_Y" title=""></label>
            </td>
     */

    function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array( 'MULTIPLE', 'SEARCHABLE', 'FILTRABLE', 'WITH_DESCRIPTION', 'MULTIPLE_CNT', 'ROW_COUNT', 'COL_COUNT' ,'DEFAULT_VALUE'), //will hide the field
            "USER_TYPE_SETTINGS_TITLE" => "Дополнительные настройки"
        );
        $prop_reg_ajax = $arProperty['USER_TYPE_SETTINGS']["PROP_REG_AJAX"];
        $catalog_iblock_id = $arProperty['USER_TYPE_SETTINGS']["CATALOG_IBLOCK_ID"];

        $ck = "<input type='text' name='".$strHTMLControlName["NAME"]."[PROP_REG_AJAX]' id='".$strHTMLControlName["NAME"]."[PROP_REG_AJAX]' value='".$prop_reg_ajax."'/>";
        $ck2 = "<input type='text' name='".$strHTMLControlName["NAME"]."[CATALOG_IBLOCK_ID]' id='".$strHTMLControlName["NAME"]."[CATALOG_IBLOCK_ID]' value='".$catalog_iblock_id."'/>";

        return

            '<tr>
        <td>'.GetMessage("SORTCATALOG_PROPID").'</td>
        <td>'.$ck.'</td>
        </tr>
        <tr>
        <td>'.GetMessage("SORTCATALOG_IBID").'</td>
        <td>'.$ck2.'</td>
        </tr>
		';

    }


    //сохраняем в базу
    function ConvertToDB($arProperty, $value){
        if(empty($value["VALUE"]["SHORT"]) && empty($value["VALUE"]["LONG"])){
            return "";
        }

        $value["VALUE"] = serialize($value["VALUE"]);
        return $value;
    }
    //читаем из базы
    function ConvertFromDB($arProperty, $value){

        $value["VALUE"] = unserialize($value["VALUE"]);

        return $value;
    }
}