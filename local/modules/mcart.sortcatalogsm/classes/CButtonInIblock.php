<?

class CButtonInIblock
{
    function ButtonInIblock()
    {

        $IBLOCK_ID = htmlspecialcharsbx(COption::GetOptionString("mcart.sortcatalogsm", "SORTCATALOGSM_REG_IBLOCK_ID"));
        $type = htmlspecialcharsbx(COption::GetOptionString("mcart.sortcatalogsm", "SORTCATALOGSM_REG_IBLOCK_TYPE"));

        if ($_GET["IBLOCK_ID"] != $IBLOCK_ID)
            return;

        if ($_GET["type"] != $type)
            return;

        global $APPLICATION;
        if ($APPLICATION->GetCurPage() != "/bitrix/admin/iblock_list_admin.php")
            return;
        ?>
        <?$uri = $APPLICATION->GetCurUri("sbros=Y");?>
        <?if(empty($_GET["bxajaxid"])):?>
            <script>
                $(".adm-toolbar-panel-align-right").append("<a class='ui-btn-main' title='' id='sbros' name='sbros' href='<?=$uri;?>'><?=GetMessage("SORTCATALOG_SBROS_US_PAR");?></a>")
            </script>
        <?endif;?>
        <?
        if($_GET["sbros"] == "Y") {
            if(!empty($_POST["ID"])) {
                $arRuls = array();
                foreach ($_POST["ID"] as $val){
                    $val = str_replace("E", "", $val);
                    $arRuls[] = $val;
                }
                //echo "<pre>";
                //print_r($arRuls);
                //echo "</pre>";
                if(!empty($arRuls)) {
                    $arFilter = Array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ID" => $arRuls
                    );

                    $arSelect = Array(
                        "PROPERTY_CATALOG_SECTION",
                        "ID",
                    );

                    $rsRegElements = CIBlockElement::GetList(Array("SORT" => "ASC", "ID" => "ASC"), $arFilter, false, false, $arSelect);

                    $arSec = array();
                    while ($arRegElement = $rsRegElements->Fetch()) {
                        $arSec[] = $arRegElement["PROPERTY_CATALOG_SECTION_VALUE"];
                    }

                    if(!empty($arSec)) {

                        $arFilter = array(
                            'IBLOCK_ID' => 4,
                            "ID" => $arSec
                        );

                        $arSelect = array("ID", "LEFT_MARGIN", "RIGHT_MARGIN");
                        $pointsSections = CIBlockSection::GetList(array('ID' => 'ASC'), $arFilter, false, $arSelect);
                        $arPointSections = array();
                        //$arPointSectionsIDs = array();
                        while ($pointSection = $pointsSections->Fetch()) {
                            $arPointSections[] = $pointSection;
                            //$arPointSectionsIDs[] = $pointSection["ID"];
                        }

                        if(!empty($arPointSections)) {
                            $arChildSections = array();
                            //foreach ($arSec as $sec) {

                            $arSelect = array("ID", "NAME", "LEFT_MARGIN", 'RIGHT_MARGIN');

                            foreach ($arPointSections as $arElememLogicFilter){

                                $arFilter = array(
                                    "IBLOCK_ID" => 4,
                                    ">LEFT_MARGIN" => $arElememLogicFilter["LEFT_MARGIN"],
                                    "<RIGHT_MARGIN" => $arElememLogicFilter["RIGHT_MARGIN"]
                                );

                                $rsChildSection = CIBlockSection::GetList(array('ID' => 'ASC'), $arFilter, false, $arSelect);

                                while ($arChildSection = $rsChildSection->Fetch()) {
                                    $arChildSections[$arChildSection["ID"]] = $arChildSection["ID"];
                                }
                            }

                            if(!empty($arChildSections)){

                                $arFilter = Array(
                                    "IBLOCK_ID" => $IBLOCK_ID,
                                    "!ID" => $arRuls,
                                    "PROPERTY_CATALOG_SECTION" => $arChildSections
                                );

                                $arSelect = Array(
                                    "PROPERTY_CATALOG_SECTION",
                                    "ID",
                                );

                                $rsRegElementsRep = CIBlockElement::GetList(Array("SORT" => "ASC", "ID" => "ASC"), $arFilter, false, false, $arSelect);

                                while ($arRegElementRep = $rsRegElementsRep->Fetch()) {
                                    if(!empty($arChildSections[$arRegElementRep["PROPERTY_CATALOG_SECTION_VALUE"]])){
                                        unset($arChildSections[$arRegElementRep["PROPERTY_CATALOG_SECTION_VALUE"]]);
                                    }
                                }

                                foreach ($arSec as $sec) {
                                    $r = CUserOptions::GetList(array(), array("CATEGORY" => "catalog.section", "NAME" => $sec));
                                    $arRes = array();
                                    if ($arR = $r->Fetch()) {
                                        $arRes[] = $arR;
                                        CUserOptions::DeleteOptionsByName($arR["CATEGORY"], $arR["NAME"]);
                                    }
                                }

                                foreach ($arChildSections as $sec) {
                                    $r = CUserOptions::GetList(array(), array("CATEGORY" => "catalog.section", "NAME" => $sec));
                                    $arRes = array();
                                    if ($arR = $r->Fetch()) {
                                        $arRes[] = $arR;
                                        CUserOptions::DeleteOptionsByName($arR["CATEGORY"], $arR["NAME"]);
                                    }
                                }


                            }
                        }
                    }
                }
            } else {
                $r = CUserOptions::GetList(array(), array("CATEGORY" => "catalog.section"));
                $arRes = array();
                while ($arR = $r->Fetch()) {
                    $arRes[] = $arR;
                    CUserOptions::DeleteOptionsByName($arR["CATEGORY"], $arR["NAME"]);
                }
            }

            ?>

            <script>
                history.replaceState(false, false, "/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=<?=$IBLOCK_ID?>&type=<?=$type?>&lang=<?=htmlspecialchars($_GET["lang"])?>&find_section_section=<?=intval(htmlspecialchars($_GET["find_section_section"]))?>");
            </script>

            <?

        }

        /*if ($_GET["sbros"] == "Y") {
            ?>
            <script>
                history.replaceState(false, false, "/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=<?=$IBLOCK_ID?>&type=<?=$type?>&lang=<?=htmlspecialchars($_GET["lang"])?>&find_section_section=<?=intval(htmlspecialchars($_GET["find_section_section"]))?>");
            </script>
            <?
            $r = CUserOptions::GetList(array(), array("CATEGORY" => "catalog.section"));
            $arRes = array();
            while ($arR = $r->Fetch()) {
                $arRes[] = $arR;
                CUserOptions::DeleteOptionsByName($arR["CATEGORY"], $arR["NAME"]);
            }
        }*/
    }
}