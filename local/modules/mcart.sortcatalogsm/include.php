<?php
\Bitrix\Main\Loader::registerAutoLoadClasses(
    "mcart.sortcatalogsm",
    array(
        "CCustomTypePropSelectReg"    => "classes/CCustomTypePropSelectReg.php",
        "CCustomTypeSelectSectionReg" => "classes/CCustomTypeSelectSectionReg.php",
        "CCustomTypeHint"             => "classes/CCustomTypeHint.php",
        "CButtonInIblock"             => "classes/CButtonInIblock.php",
        "\\Mcart\\SortCatalogSM\\Helper" => "lib/Helper.php",
    )
);
?>