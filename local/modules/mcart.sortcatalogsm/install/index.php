<?

IncludeModuleLangFile(__FILE__);


if (class_exists("mcart_sortcatalogsm"))
    return;

Class mcart_sortcatalogsm extends CModule {

    var $MODULE_ID = "mcart.sortcatalogsm";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";

    function __construct() {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = BREAKNOTIFICATION_VERSION;
            $this->MODULE_VERSION_DATE = BREAKNOTIFICATION_VERSION_DATE;
        }

        $this->MODULE_NAME = GetMessage("MCART_SORTCATALOGSM_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MCART_SORTCATALOGSM_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("MCART_SORTCATALOGSM_PARTNER_NAME");
        $this->PARTNER_URI = "http://mcart.ru/";
    }

    function DoInstall() {

        if (!IsModuleInstalled("mcart.sortcatalogsm")) {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        }
        return true;
    }

    function DoUninstall() {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();

        return true;
    }

    function InstallDB() {

        RegisterModule("mcart.sortcatalogsm");
        return true;
    }

    function UnInstallDB() {

        UnRegisterModule("mcart.sortcatalogsm");
        return true;
    }

    function InstallEvents() {

        RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "mcart.sortcatalogsm", 'CCustomTypePropSelectReg', 'GetUserTypeDescription');
        RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "mcart.sortcatalogsm", 'CCustomTypeSelectSectionReg', 'GetUserTypeDescription');
        RegisterModuleDependences("main", "OnEpilog", "mcart.sortcatalogsm", 'CButtonInIblock', 'ButtonInIblock');
        return true;
    }

    function UnInstallEvents() {
        UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "mcart.sortcatalogsm", 'CCustomTypePropSelectReg', 'GetUserTypeDescription');
        UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "mcart.sortcatalogsm", 'CCustomTypeSelectSectionReg', 'GetUserTypeDescription');
        UnRegisterModuleDependences("main", "OnEpilog", "mcart.sortcatalogsm", 'CButtonInIblock', 'ButtonInIblock');
        return true;
    }

    function InstallFiles() {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.sortcatalogsm/install/components/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.sortcatalogsm/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true);
        return true;
    }

    function UnInstallFiles() {
        return true;
    }

}

//end class
?>