<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?/*foreach($arResult["ITEMS"] as $arItem):?>
    <?
        echo $arItem["NAME"]."<br>";
    ?>
<?endforeach;*/?>


<?

$APPLICATION->IncludeComponent(
    "bitrix:main.interface.grid",
    "",
    array(
//уникальный идентификатор грида
        "GRID_ID"=>$arResult["GRID_ID"],
//описание колонок грида, поля типизированы
        "HEADERS"=>array(
            array("id"=>"ID", "name"=>"ID", "sort"=>"ID", "default"=>true),
            array("id"=>"NAME", "name"=>GetMessage("PROP_LINE_TEMP_NAME"), "sort"=>"NAME", "default"=>true),
            array("id"=>"CODE", "name"=>GetMessage("PROP_LINE_TEMP_CODE"), "sort"=>"CODE", "default"=>true),
        ),
//сортировка
        "SORT"=>$arResult["SORT"],
//это необязательный
        "SORT_VARS"=>$arResult["SORT_VARS"],
//данные
        "ROWS"=>$arResult["ROWS"],
//футер списка, можно задать несколько секций
        "FOOTER"=>array(array("title"=>"Всего", "value"=>$arResult["ROWS_COUNT"])),
//объект постранички
        "NAV_OBJECT"=>$arResult["NAV_OBJECT"],
//можно использовать в режиме ajax
        "AJAX_MODE"=>"Y",
        "AJAX_OPTION_JUMP"=>"N",
        "AJAX_OPTION_STYLE"=>"N",
//фильтр
        "FILTER"=>$arResult["FILTER"],
    ),
    $component
);

?>

<script>

    var n = <?=CUtil::PhpToJSObject(htmlspecialchars($_GET["n"]))?>;
    var nmd5 = <?=CUtil::PhpToJSObject(md5(htmlspecialchars($_GET["n"])))?>;
    var k = <?=CUtil::PhpToJSObject(htmlspecialchars($_GET["k"]))?>;

    function SelEl(id, name) {

        if(n != '') {
            if (window.opener.document.getElementById(n)) {
                window.opener.document.getElementById(n).value = id;
                if(k != '') {
                    if (window.opener.document.getElementById('sp_' + n + '_' + k)) {
                        window.opener.document.getElementById('sp_' + n + '_' + k).innerHTML = name;
                    }
                }
            }
        }
        window.close();

    }

</script>
