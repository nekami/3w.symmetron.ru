<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 300;

if(empty($arParams["IBLOCK_ID"]))
    $arParams["IBLOCK_ID"] = 4;
else
    $arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);



//if($this->startResultCache(false, array($_GET["PAGEN_1"], $_GET["order"], $_GET["by"])))
//{
	if(!Loader::includeModule("iblock"))
	{
		//$this->abortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

    //уникальный идентификатор грида
    $arResult["GRID_ID"] = "prop_grid_for_admins";

    //определяем фильтр, поля фильтра типизированы
    //по умолчанию тип text, поддерживается date, list, number, quick (поле ввода и список), custom
    $arResult["FILTER"] = array(
        array("id"=>"FIND", "name"=>GetMessage("PROP_LINE_SEORCH_F"), "type"=>"quick", "items"=>array("ID"=>"ID", "NAME"=>GetMessage("PROP_LINE_SEORCH_NAME"), "CODE"=>GetMessage("PROP_LINE_SEORCH_CODE"))),
        /*array("id"=>"PERSONAL_PROFESSION", "name"=>"Профессия"),
        array("id"=>"PERSONAL_BIRTHDAY", "name"=>"День рождения", "type"=>"date"),
        array("id"=>"PERSONAL_PROFESSION", "name"=>"Профессия"),
        array("id"=>"PERSONAL_WWW", "name"=>"Веб"),
        array("id"=>"PERSONAL_ICQ", "name"=>"АйСикЮ", "params"=>array("size"=>15)),
        array("id"=>"PERSONAL_GENDER", "name"=>"Пол", "type"=>"list", "items"=>array(""=>"(пол)", "M"=>"Мужской", "F"=>"Женский")),
        array("id"=>"GROUPS_ID", "name"=>"Группы пользователей", "type"=>"list", "items"=>$aGroups, "params"=>array("size"=>5, "multiple"=>"multiple"), "valign"=>"top"),
        array("id"=>"PERSONAL_PHONE", "name"=>"Телефон"),
        array("id"=>"PERSONAL_MOBILE", "name"=>"Мобильник"),
        array("id"=>"PERSONAL_CITY", "name"=>"Город"),*/
    );

    //инициализируем объект с настройками пользователя для нашего грида
    $grid_options = new CGridOptions($arResult["GRID_ID"]);


    //получим текущий фильтр (передаем описанный выше фильтр)
    $aFilter = $grid_options->GetFilter($arResult["FILTER"]);

    //echo "<pre>";
    //print_r($aFilter);
    //echo "</pre>";

    $arFilterForQ = Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]);
    if(!empty($aFilter["FIND_list"]) && !empty($aFilter["FIND"])) {
        if($aFilter["FIND_list"] == "ID") {
            $arFilterForQ[$aFilter["FIND_list"]] = $aFilter["FIND"];
        } else {
            $arFilterForQ[$aFilter["FIND_list"]] = $aFilter["FIND"]."%";
        }
    }

    //какую сортировку сохранил пользователь (передаем то, что по умолчанию)
    $aSort = $grid_options->GetSorting(array("sort" => array("NAME" => "asc"), "vars" => array("ID" => "ID", "NAME" => "NAME", "CODE" => "CODE")));
    $arResult["SORT"] = $aSort["sort"];
    $arResult["SORT_VARS"] = $aSort["vars"];

    //размер страницы в постраничке (передаем умолчания)
    $aNav = $grid_options->GetNavParams(array("nPageSize" => 20));

    $properties = CIBlockProperty::GetList($aSort["sort"], $arFilterForQ);

    //постраничка с учетом размера страницы
    $properties->NavStart($aNav["nPageSize"]);

	$arResult["ITEMS"] = array();
    $arResult["ROWS"] = array();


    while ($prop_fields = $properties->GetNext())
    {
        $arResult["ITEMS"][] = $prop_fields;

        //возможные действия над строкой
        $aActions = Array(
            array("ICONCLASS"=>"edit", "TEXT"=>GetMessage("PROP_LINE_SEORCH_SELECT"), "ONCLICK"=>"SelEl('".$prop_fields["ID"]."', '".$prop_fields["NAME"]."')", "DEFAULT"=>true),
        );

        $arResult["ROWS"][] = array(
            "data" => array(
                    "ID" => $prop_fields["ID"],
                    "NAME" => $prop_fields["NAME"],
                    "CODE" => $prop_fields["CODE"]
            ),
            "actions" => $aActions
        );
    }

    //информация для футера списка
    $arResult["ROWS_COUNT"] = $properties->SelectedRowsCount();

    //объект постранички - нужен гриду. Не убираем ссылку "все".
    //$properties->bShowAll = false;
    $arResult["NAV_OBJECT"] = $properties;

	//$this->setResultCacheKeys(array(
	//	"GRID_ID",
	//));

	$this->includeComponentTemplate();
//}
