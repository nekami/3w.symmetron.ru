<?php
namespace Mcart\SortCatalogSM;

class Helper
{
    public static function GetPropertyID($IBlockID)
    {
        $property = array();
        $result = \Bitrix\Iblock\PropertyTable::GetList(array(
            "select" => array("ID", "CODE"),
            "filter" => array("IBLOCK_ID" => $IBlockID),
        ));
        while ($prop = $result->fetch())
            $property[$prop["CODE"]] = $prop["ID"];
        return $property;
    }

    protected static function GetPropertiesCode($ids)
    {
        $property = array_fill_keys($ids, "");
        $result = \Bitrix\Iblock\PropertyTable::GetList(array(
            "select" => array("ID", "CODE"),
            "filter" => array("ID" => $ids),
        ));
        while ($prop = $result->fetch())
            $property[$prop["ID"]] = $prop["CODE"];

        $property = array_filter($property, function ($elem) {
            return !empty($elem);
        });

        return $property;
    }

    public static function GetArrayPair($value)
    {
        return unserialize($value);
    }

    protected static function GetUnsortedProperties($IBlockID, $catalogSectionId)
    {
        $arSelectPropsNoSort = [];
        $arPropsCodeID = static::GetPropertyID($IBlockID);

        if ($arPropsCodeID["REG_CATALOG_SECTION"])
        {
            $propertyId = $arPropsCodeID["REG_CATALOG_SECTION"];
            $propertyIdSection = $arPropsCodeID["CATALOG_SECTION"];
            $propRegElr = \CIBlockElement::GetPropertyValues(
                $IBlockID,
                array('ACTIVE' => 'Y', "PROPERTY_CATALOG_SECTION" => $catalogSectionId),
                true, array("ID" => [$propertyId, $propertyIdSection])
            );

            $arAllReg = array();
            while ($propRegEl = $propRegElr->Fetch()){
                $arAllReg[] = $propRegEl;
            }

            foreach ($catalogSectionId as $id){
                foreach ($arAllReg as $propRegEl){
                    if(in_array($id, $propRegEl[$propertyIdSection])){
                        $arSelectPropsNoSort = array_merge($arSelectPropsNoSort, array_map(
                            static::class."::GetArrayPair",
                            $propRegEl[$propertyId]
                        ));
                        break;
                    }
                }
            }

            /*if ($propRegEl = $propRegElr->Fetch()) {

            }*/
        }
        return $arSelectPropsNoSort;
    }

    public static function GetCatalogParentSectionsId($filter, $arSecParIDs)
    {
        $arSecParIDs = $arSecParIDs ?: array();
        $rsSectionsPar = \Bitrix\Iblock\SectionTable::getList(array(
            "filter" => $filter,
            "order" => array('LEFT_MARGIN' => 'DESC'),
            "select" => array("ID", "LEFT_MARGIN")
        ));

        while ($arSectionPar = $rsSectionsPar->fetch()) {
            $arSecParIDs[] = $arSectionPar["ID"];
        };
        return $arSecParIDs;
    }

    public static function GetResultCode($arData, $arCode){
        $arRes = [];
        foreach ($arData as $data){
            $arRes[$data["VALUE"]] = $arCode[$data["VALUE"]];
        }
        return $arRes;
    }

    public static function GetResultFilter($arData, $code, $defaultSort){
        $arRes = [];
        foreach ($arData as $data){
            if($data[$code]["VALUE"] != "Y")
                continue;

            if($data["SET_SORT"] == "Y")
                $data["SORT"] = $data[$code]["SORT"]?:$defaultSort;
            $arRes[] = $data;
        }
        return $arRes;
    }


    public static function GetDefaultProperties($catalogSectionId)
    {
        $arResult = [];
        if ($IBlockID = static::GetIBlockID()) {
            $arSelectPropsNoSort = static::GetUnsortedProperties($IBlockID, $catalogSectionId);
            usort($arSelectPropsNoSort, function($a, $b){
                return ($a["SORT"] - $b["SORT"]) <=> 0;
            });

            $arSelectPropsSort = array_column($arSelectPropsNoSort, "VALUE");
            $arSelectPropsSort = static::GetPropertiesCode($arSelectPropsSort);


            //distribute settings
            $arResult["PROPERTY_ALL"] = $arSelectPropsSort;

            $propertiesOnList = array_filter($arSelectPropsNoSort, function ($el){return $el["SHOW"] == "Y";});
            $arResult["PROPERTY_ON_LIST"] = static::GetResultCode($propertiesOnList, $arSelectPropsSort);


            $propertiesOnSearch = static::GetResultFilter($arSelectPropsNoSort, "PARAM_SEARCH", 10);
            usort($propertiesOnSearch, function($a, $b){
                return ($a["SORT"] - $b["SORT"]) <=> 0;
            });
            $arResult["PROPERTY_ON_SEARCH"] = static::GetResultCode($propertiesOnSearch, $arSelectPropsSort);


            $propertiesOnSettingsWin = static::GetResultFilter($arSelectPropsNoSort, "SETTINGS_WINDOW", 20);
            usort($propertiesOnSettingsWin, function($a, $b){
                return ($a["SORT"] - $b["SORT"]) <=> 0;
            });
            $arResult["PROPERTY_ON_WIN"] = static::GetResultCode($propertiesOnSettingsWin, $arSelectPropsSort);


            $propertySpecification = static::GetResultFilter($arSelectPropsNoSort, "SPECIFICATION", 30);
            usort($propertySpecification, function($a, $b){
                return ($a["SORT"] - $b["SORT"]) <=> 0;
            });
            $arResult["PROPERTY_ON_SPEC"] = static::GetResultCode($propertySpecification, $arSelectPropsSort);

        }
        return $arResult;
    }

    public static function GetIBlockID()
    {
        return \Bitrix\Main\Config\Option::get("mcart.sortcatalogsm", "SORTCATALOGSM_REG_IBLOCK_ID");
    }

    public static function getHints($catalogId){

        if(!\CModule::IncludeModule("iblock")){
            return [];
        }

        $IBLOCK_ID_REG = htmlspecialcharsbx(\COption::GetOptionString("mcart.sortcatalogsm", "SORTCATALOGSM_REG_IBLOCK_ID"));

        $properties = \CIBlockProperty::GetList(["sort"=>"asc", "name"=>"asc"], ["ACTIVE"=>"Y", "IBLOCK_ID"=>$IBLOCK_ID_REG, "CODE" => "PROPERTY_HINTS"]);

        if ($arProp = $properties->Fetch()){

            $propHintsId = $arProp["ID"];

            $propHints = \CIBlockElement::GetPropertyValues($IBLOCK_ID_REG, ['ACTIVE' => 'Y', "PROPERTY_CATALOG_SECTION" => $catalogId], true, ["ID" => $propHintsId]);

            if ($propHint = $propHints->Fetch()) {

                $arEndHints = [];

                foreach ($propHint[$propHintsId] as $arHintsForProperty){

                    $arHintsForProperty = unserialize($arHintsForProperty);

                    $arEndHints[$arHintsForProperty["ID"]] = array(
                        "SHORT" => $arHintsForProperty["SHORT"],
                        "LONG" => $arHintsForProperty["LONG"]
                    );

                }

                return $arEndHints;

            }

        }

        return [];

    }
}
?>