<?define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("iblock"))
    die("");

//get current section
$IBLOCK_ID = htmlspecialchars($_GET["IBLOCK_ID"]);
$arFilter = array(
    'IBLOCK_ID' => $IBLOCK_ID,
    "ID" => htmlspecialchars($_GET["SECTION_VAL"])
);

$arSelect = array("ID", "LEFT_MARGIN", "RIGHT_MARGIN");
$pointSection = CIBlockSection::GetList(array('ID' => 'ASC'), $arFilter, false, $arSelect)->Fetch();


//get parents sections
$rsSections = \Bitrix\Iblock\SectionTable::getList(array(
    "filter" => array(
        "IBLOCK_ID" => $IBLOCK_ID,
        '<LEFT_MARGIN' => $pointSection["LEFT_MARGIN"],
        '>RIGHT_MARGIN' => $pointSection["RIGHT_MARGIN"]
    ),
    "order" => array('LEFT_MARGIN' => 'DESC'),
    "select" => array("ID", "NAME", "LEFT_MARGIN", "RIGHT_MARGIN", "IBLOCK_SECTION_ID")
));

$arSections = array();
$arSecIDs = array();
while ($arSection = $rsSections->fetch()) {
    $arSections[$arSection["ID"]] = $arSection;
    $arSecIDs[] = $arSection["ID"];
};

if(empty($arSecIDs)){
    echo "";
    die;
}


//get elements regulations for parents from iblock of regulation
$IBLOCK_ID_REG = htmlspecialchars($_GET["IBLOCK_ID_REG"]);
$PROP_CODE_SELECT_SECTION = htmlspecialchars($_GET["PROP_CODE_SELECT_SECTION"]);
$PROP_REG_AJAX = htmlspecialchars($_GET["PROP_REG_AJAX"]);
$arFilter = Array(
    "IBLOCK_ID" => $IBLOCK_ID_REG,
    "PROPERTY_".$PROP_CODE_SELECT_SECTION => $arSecIDs
);

$arSelect = Array(
    "PROPERTY_".$PROP_CODE_SELECT_SECTION,
    "PROPERTY_".$PROP_REG_AJAX,
    "ID"
);

$rsRegElements = CIBlockElement::GetList(Array("SORT"=>"ASC", "ID"=>"ASC"), $arFilter, false, false, $arSelect);

$arRegElements = array();
while($arRegElement = $rsRegElements->Fetch())
{
    $arSections[$arRegElement["PROPERTY_".$PROP_CODE_SELECT_SECTION."_VALUE"]]["ID"] = $arRegElement["ID"];
    $arSections[$arRegElement["PROPERTY_".$PROP_CODE_SELECT_SECTION."_VALUE"]]["REG"][] = $arRegElement["PROPERTY_".$PROP_REG_AJAX."_VALUE"];
}

//get nearest parent with regulations
$arRegulations = array();
$idSectionForClone = 0;
foreach ($arSections as $arSec){
    if(!empty($arSec["REG"])){
        $arRegulations = $arSec["REG"];
        $idSectionForClone = $arSec["ID"];
        break;
    }
}

//create HTML for ajax
//$ResultHTML = "";


if(!empty($arRegulations)){

    //get name properties
    $rsPropsNames = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID));
    $arPropsNames = array();
    while ($arPropsName = $rsPropsNames->Fetch())
    {
        $arPropsNames[$arPropsName["ID"]] = $arPropsName["NAME"];
    }
    
    //add empty string
    //$arRegulations[] = "";
    //get description for property
    $rsDescrs = CIBlockElement::GetPropertyValues($IBLOCK_ID_REG, array('ACTIVE' => 'Y', 'ID' => $idSectionForClone), true, array('ID' => array($PROP_REG_AJAX)));
    $arDescrs = array();
    if ($arDescr = $rsDescrs->Fetch())
    {
        foreach ($arDescr[$PROP_REG_AJAX] as $key => $val) {
            $arDescrs[$val] = $arDescr["DESCRIPTION"][$PROP_REG_AJAX][$key];
        }
    }

    //create HTML for return ajax
    $count = intval(htmlspecialchars($_GET["SELECT_LEN"]));
    ob_start();
    foreach ($arRegulations as $reg){
        $n = preg_replace("/[^a-zA-Z0-9_\\[\\]]/", "", "PROP[$PROP_REG_AJAX][n$count][VALUE]");
        $md5 = md5($n);

        ?>
        <tr><td>
        <input name='PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][DESCRIPTION]' id='PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][DESCRIPTION]' value='<?=$arDescrs[$reg];?>'>
        <input name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE]" id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE]" value="<?=$reg?>" size="5" type="text">

        <input type="button" value="..." onclick="jsUtils.OpenWindow('/iblock_property_search.php?lang=ru&IBLOCK_ID=<?=$IBLOCK_ID?>&n=PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE]&k=n0', 900, 700);">

        <span id="sp_<?=$n?>_n0">
            <?if(!empty($arPropsNames[$reg])){
                echo $arPropsNames[$reg];
            } else {
                echo GetMessage("SORTCATALOGSM_NO_PARAM");
            }?>
        </span>
        <span onclick='deleteItem(this)' style='color: red;'>X</span></br>
        </td></tr>
        <?

        /*$ResultHTML .= "<tr><td>";

        $ResultHTML .= "<select name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE]'>";
        $ResultHTML .= "<option value=''>-</option>";
        foreach ($arProps as $key=>$val) {
            if($reg == $key){
                $ResultHTML .= "<option selected value='" . $key . "'>" . $val . "</option>";
            } else {
                $ResultHTML .= "<option value='" . $key . "'>" . $val . "</option>";
            }
        }
        $ResultHTML .= "</select> ";
        $ResultHTML .= "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][DESCRIPTION]' id='PROP[".$PROP_REG_AJAX."][n".$count."][DESCRIPTION]' 
        value='200'>";
        $ResultHTML .= " <span onclick='deleteItem(this)' style='color: red;'>X</span></br>";
        $ResultHTML .= "</td></tr>";*/
        $count++;
    }

    $html_value = ob_get_contents();
    ob_end_clean();

    //$ResultHTML .= "<tr><td><input type=\"button\" value=\"Добавить\" onclick=\"addNewRow('tbdf4285bc1d07139fc6122003f756eaea')\"></td></tr>";

    //$ResultHTML .= "</tbody>";

    $GLOBALS['APPLICATION']->RestartBuffer();
    echo $html_value;
    die;
} else {
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "";
    die;
}

//return ajax result

/*echo "<pre>";
print_r($_GET);
echo "</pre>";
echo "<pre>";
print_r($pointSection);
echo "</pre>";
die;*/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>