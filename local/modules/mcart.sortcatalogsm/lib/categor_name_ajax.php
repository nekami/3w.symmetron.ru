<?define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("iblock"))
    die("");

$ID = htmlspecialchars($_GET["ID"]);
$IBLOCK_ID_CAT = htmlspecialchars($_GET["IBLOCK_ID_CAT"]);

if(!empty($ID) && !empty($IBLOCK_ID_CAT)) {
    $res = CIBlockSection::GetList(Array("ID"=>"ASC"), array("ID" => $ID, "IBLOCK_ID" => $IBLOCK_ID_CAT), true)->Fetch();
    if(!empty($res["ID"])){
        $GLOBALS['APPLICATION']->RestartBuffer();
        echo $res["NAME"];
    } else {
        $GLOBALS['APPLICATION']->RestartBuffer();
        echo "";
    }

} else {
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "";
}
die();