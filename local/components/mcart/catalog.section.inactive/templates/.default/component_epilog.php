<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$path = "/bitrix/components/bitrix/catalog.section/templates/.default/";
require($_SERVER["DOCUMENT_ROOT"].$path."component_epilog.php");

CJSCore::Init(array("popup", "currency"));