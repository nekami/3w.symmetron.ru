function SymmetronCatalogSection(className, optionModalCode, items) {
    if(items === undefined) {
        items = {};
    }

    this.class = className
    this.optionModalCode = optionModalCode
    this.items = items;
    this.bind();
    this.init();

    let scroll = findGetParameter("scroll");
    let tableRight = $("div.scroll-table.table-right");
    if (scroll !== null && tableRight.length !== 0) {
        tableRight.scrollLeft(scroll);
    }

    let tlClosed = findGetParameter("tl-closed");
    let tableLeft = $("div.table-left");
    let tableSep = $("div.table-separator");
    if (tlClosed !== null && tableLeft.length !== 0 && tableSep.length !== 0) {
        if (tlClosed === "true") {
            tableLeft.addClass("closed");
            tableSep.addClass("closed");
            $('.analogue-table__left').floatThead('reflow');
            $('.analogue-table').floatThead('reflow');
        } else if (tlClosed === "false") {
            tableLeft.removeClass("closed");
            tableSep.removeClass("closed");
            $('.analogue-table__left').floatThead('reflow');
            $('.analogue-table').floatThead('reflow');
        }
    }
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

SymmetronCatalogSection.prototype.init = function () {
    var _this = this
    BX.ready(function () {
        var button = $(_this.class).find(".table-filter__list .default-button.add-table-filter")
        var onclick = button.attr("onclick")
        button.attr("onclick", "return false;")
        if(onclick) {
            button.attr("bxajaxid", onclick.match(/bxajaxid=(.{32})/)[1]);
        }
    });
}

SymmetronCatalogSection.prototype.bind = function () {
    var _this = this

    BX.ready(function () {
        $(_this.class).on('click', ".table-filter__list .default-button.add-table-filter", function () {
            _this.changeColumns(this)
        });

        /* Toggle item minicard */
        $(_this.class).on('click', '.mini-card__select', function () {
            var element = this
            var lenStr = element.id.indexOf('_left')
            if (lenStr + 1) {
                newId = element.id.slice(0, lenStr)
                element = document.getElementById(newId)
            }

            if (!$(element).next('.mini-card').hasClass('open')) {
                let arrId = this.id.split('_');

                if (arrId.length === 4 && _this.items[arrId[2]] !== undefined) {
                    window.dataLayer = window.dataLayer || [];
                    window.dataLayer.push({
                        'ecommerce': {
                            'impressions': [
                                {
                                    'name': _this.items[arrId[2]]['name'],
                                    'id': Number(_this.items[arrId[2]]['id']),
                                    'price': Number(_this.items[arrId[2]]['price']),
                                    'brand': _this.items[arrId[2]]['brand'],
                                    'category': _this.items['bread'],
                                    'list': 'catalog',
                                    'position': Number(parseInt(this.rowIndex / 2) + 1)
                                }]
                        },
                        'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True',
                    });
                }
            }

            _this.toggleMini(element)
            var mini = $(element).next('.mini-card')

            var leftLine = $("#" + element.id + "_left")
            _this.toggleMini(leftLine)
            /*leftLine.next('.mini-card').css({
                'height': $(mini).innerHeight()
            })*/
            _this.fixSize(mini[0], leftLine.next('.mini-card')[0], true)
            //console.log($(mini).innerHeight());
        });

        $(_this.class).on('click', '.mini-cart__wrapper', function () {
            let arrId = this.parentElement.parentElement.parentElement.id.split('_');

            if (arrId.length === 3 && _this.items[arrId[2]] !== undefined) {
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                    'ecommerce': {
                        'click': {
                            'actionField': {'list': 'catalog'},
                            'products': [{
                                'name': _this.items[arrId[2]]['name'],
                                'id': Number(_this.items[arrId[2]]['id']),
                                'price': Number(_this.items[arrId[2]]['price']),
                                'brand': _this.items[arrId[2]]['brand'],
                                'category': _this.items['bread'],
                                'position': Number(this.parentElement.parentElement.parentElement.rowIndex / 2)
                            }]
                        }
                    },
                    'event': 'gtm-ee-event',
                    'gtm-ee-event-category': 'Enhanced Ecommerce',
                    'gtm-ee-event-action': 'Product Clicks',
                    'gtm-ee-event-non-interaction': 'False',
                });
            }
        });

        ///////////////////////////////////
        /* Favorite */
        $(_this.class).on('click', '.favorite', function () {
            _this.toggleFavs(this)

        });

        $(_this.class).on('click', '.items-selector', function (event) {
            event.preventDefault();
            $(_this.class).find('.items-selector').parent().toggleClass('open');
            console.log($(_this.class))
        });

        $("body").on('click', function (event) {
            if ($(event.target).parents(".table-filter__selector").length === 0)
                $(_this.class).find('.items-selector').parent().removeClass('open');
        });

        $(document).on('click', function (event) {
            _this.searchStrange(event)
        });
        //card position for first loading
        _this.searchStrange(event)
        ///////////////////////////////////

        //card follow scroll
        $('.scroll-table').scroll(function () {
            _this.positionCardForScroll();
        });
        //_this.positionCardForScroll();

        ///////////////////////////////////


        $('.table-filter__hint').tooltipster({
            side: 'bottom',
            arrow: false,
            /*trigger: 'click'*/
        });

        ///////////////////////////////////


        if ($(_this.class).find('.scroll-table').length > 0) {
            _this.scrolledTable()
        }

        $(".table-filter__arrow").on("click", function (ev) {
            $(".scroll-blocks").css("display", "none");

            // let bxajaxid = $("main div:first").attr("id");
            //CHANGED
            let bxajaxid = $("#analogue").parent().attr("id");

            BX.ajax.insertToNode($(this).attr("data-href") + "&scroll=" + $("div.scroll-table.table-right").scrollLeft() + "&tl-closed=" + $("div.table-left").hasClass('closed') + "&bxajaxid=" + bxajaxid.split("_")[1], bxajaxid);
            return false;

        })

        $(".add-table-filter").on("click", function (ev) {
            $(".scroll-blocks").css("display", "none")
        })

        ///////////////////////////////////

        $(document).ready(function () {
            $('.message-close').on('click', function () {
                $(this).parent().parent().addClass('closed')
            })

            $(_this.class).find('.close-left').on('click', function () {
                if ($(this).parent().hasClass('closed')) {
                    $(this).parent().removeClass('closed')
                } else {
                    $(this).parent().addClass('closed')
                }

                if ($(_this.class).find('.table-left').hasClass('closed')) {
                    $(_this.class).find('.table-left').removeClass('closed')
                    $(_this.class).find('.analogue-table__left').floatThead('reflow')
                    $(_this.class).find('.analogue-table').floatThead('reflow')

                } else {
                    $(_this.class).find('.table-left').addClass('closed')
                    $(_this.class).find('.analogue-table__left').floatThead('reflow')
                    $(_this.class).find('.analogue-table').floatThead('reflow')
                }
            })
        })

        $(document).ready(function () {
            if ($('.analogue-table').length > 0) {
                var tableA = $('.analogue-table');
                tableA.floatThead({
                    zIndex: 20,
                    responsiveContainer: function (tableA) {
                        return tableA.closest('.scroll-table');
                    }
                });

                /*var tableAtr = $('.analogue-table .mini-card__select');
                tableAtr.each(function(){
                    var tableAtrHeight = $(this).height()
                    $('.analogue-table__left .mini-card__select td').each(function(){
                        $(this).height(tableAtrHeight)
                    })
                })*/

                var tableAtr = $('.analogue-table .mini-card__select');
                var tableAtrLeft = $('.analogue-table__left .mini-card__select')
                tableAtr.each(function (i) {
                    var tableAtrHeight = $(this).height()
                    //console.log(tableAtrLeft[i])
                    $(tableAtrLeft[i]).height(tableAtrHeight)
                })


                /*var tableAtr = $('.analogue-table .mini-card');
                var tableAtrLeft = $('.analogue-table__left .mini-card')
                tableAtr.each(function(i){
                    //var tableAtrHeight = this.clientHeight

                    //tableAtrLeft[i].style.height = tableAtrHeight + "px"
                    var cellsCollection = $(this)
                    cellsCollection.push(tableAtrLeft[i])
                    cellsCollection.equalHeights()
                })*/

            }

            if ($('.analogue-table__left').length > 0) {
                var tableAL = $('.analogue-table__left');
                tableAL.floatThead({
                    zIndex: 20,
                    responsiveContainer: function (tableAL) {
                        return tableAL.closest('.table-left');
                    }
                });
            }

            var tableThHeight = $('.analogue-table thead').innerHeight()
            $('.table-filter__selector').css({
                'top': tableThHeight - 25
            })

            $('.analogue-table__left th').css({
                'height': tableThHeight
            })

            /* смотрим с какого браузера зашел накркоман который пользуется этой таблицей, в качестве значения указаны отступ когда шестеренка вырубится  */
            function get_name_browser() {
                var ua = navigator.userAgent;

                if (ua.search(/Chrome/) != -1) return 260;
                if (ua.search(/Firefox/) != -1) return 260;
                if (ua.search(/Opera/) != -1) return 260;
                if (ua.search(/Safari/) != -1) return 230;
                if (ua.search(/MSIE/) != -1 || ua.search(/Trident/) != -1) return 260;
            }

            var browser = get_name_browser();
            /*  получаем название браузера, если оно соответствует тому чем пользуются, ставим значение   */
            var tableOffsetTop = $('.scroll-table__wrapper').offset().top
            /*  значение отступа таблицы сверху, потому что колбасный фльтр его может поменять  */
            $(window).scroll(function () {
                /*  при скролле рассчитываем положение шестеренки добавляя фиксированный класс */
                if (window.pageYOffset >= tableOffsetTop && window.pageYOffset < ($('.table-full').height() + browser)) {
                    $('.table-filter__selector').addClass('fixed')
                } else {
                    $('.table-filter__selector').removeClass('fixed')
                }

            })


            //resize all table
            $(window).resize(function (ev) {
                //console.log(ev)

                var tableThHeight = $('.analogue-table thead').innerHeight()

                $('.analogue-table__left th').css({
                    'height': tableThHeight - 1
                })

                var tableAtr = $('.analogue-table .mini-card__select');
                var tableAtrLeft = $('.analogue-table__left .mini-card__select')
                tableAtr.each(function (i) {
                    //var tableAtrHeight = $(this).height()
                    //console.log(tableAtrLeft[i])
                    //$(tableAtrLeft[i]).height(tableAtrHeight)
                    $(tableAtrLeft[i]).outerHeight($(this).innerHeight());
                })

                var tableAtr = $('.analogue-table .mini-card.open');
                var tableAtrLeft = $('.analogue-table__left .mini-card.open')
                var count = 0;
                tableAtr.each(function (i) {
                    _this.fixSize(this, tableAtrLeft[i], true)
                })

            });

        });

    });
}

SymmetronCatalogSection.prototype.fixSize = function (el1, el2, start) {
    if (start)
        el1.style = ""

    let currentHeight = parseInt($(el1).innerHeight()) + 1
    $(el1).css({'height': currentHeight});
    $(el2).css({'height': currentHeight});

    //console.log($(el1).innerHeight())
    //console.log($(el2).innerHeight())
}

SymmetronCatalogSection.prototype.positionCardForScroll = function () {
    var tableScroll = $('.scroll-table').scrollLeft()
    if (tableScroll >= 0) {
        $('.mini-cart__wrapper').css({
            'margin-left': tableScroll
        })
    }
}

SymmetronCatalogSection.prototype.changeColumns = function (list) {
    var href = $(list).attr("href")
    var bxajaxid = $(list).attr("bxajaxid")
    list = $(list).parent().prev().find("input:checked")
    if (href.indexOf("?") === -1)
        href += "?"
    for (var i = 0; i < list.length; i++) {
        href += "&" + this.optionModalCode + "[]=" + list[i].value
    }
    href += "&" + this.optionModalCode + "[]=&bxajaxid=" + bxajaxid
    BX.ajax.insertToNode(href, 'comp_' + bxajaxid);
}

SymmetronCatalogSection.prototype.toggleMini = function (element) {
    var mini = $(element).next('.mini-card')
    if (mini.hasClass('open')) {
        setTimeout(function () {
            mini.removeClass('open')
        }, 0)
        mini.children().children().slideUp(0)
    } else {
        mini.addClass('open')
        mini.children().children().slideDown(0)
    }
}

SymmetronCatalogSection.prototype.toggleFavs = function (element) {
    let el = $(element), id = parseInt(el.data('id'));
    if (id)
        $.ajax({
            url: '/ajax.php',
            type: 'POST',
            data: {action: 'fav', id: id},
        }).done(function (data) {
            data = JSON.parse(data);
            if (data.noAuth) {
                BX.Mcart.modals.result_off.open(["Ошибка!", "Избранное доступно только авторизованным пользователям!"]);
                return;
            }
            el.toggleClass('act');
        });
}

SymmetronCatalogSection.prototype.searchStrange = function (event) {
    var searchSelect = $('.search-select')
    var projectSelect = $('.project-select')
    var subscribeBlock = $('.subscribe-wrapper')

    if (!projectSelect.is(event.target) && projectSelect.has(event.target).length === 0 && projectSelect.hasClass('open')) {
        projectSelect.removeClass('open')
    }

    if (!searchSelect.is(event.target) && searchSelect.has(event.target).length === 0 && searchSelect.hasClass('open')) {
        searchSelect.removeClass('open')
    }

    if (!subscribeBlock.is(event.target) && subscribeBlock.has(event.target).length === 0 && subscribeBlock.hasClass('open')) {
        subscribeBlock.removeClass('open')
    }
}

SymmetronCatalogSection.prototype.scrolledTable = function () {

    function loopr() {
        $('.scroll-table').animate({scrollLeft: "+=100px"}, "5", 'linear', loopr);
        var tableScrollL = $('.scroll-table').scrollLeft()
        scrollHidden(tableScrollL)
    }

    function loopl() {
        $('.scroll-table').animate({scrollLeft: "-=100px"}, "5", 'linear', loopl);
        var tableScrollL = $('.scroll-table').scrollLeft()
        scrollHidden(tableScrollL)
    }

    function stop() {
        $('.scroll-table').stop();
    }

    function scrollHidden() {
        var scrollTable = $('.scroll-table')[0]
        var tableScrollL = $('.scroll-table').scrollLeft()
        //var tableInner = $('.scroll-table.table-right').innerWidth()
        if (tableScrollL === 0) {
            $(".scroll-toLeft").addClass('hidden')
        } else {
            $(".scroll-toLeft").removeClass('hidden')
        }

        if (scrollTable.scrollLeft === scrollTable.scrollWidth - scrollTable.offsetWidth
            || scrollTable.scrollWidth === scrollTable.clientWidth) {
            $(".scroll-toRight").addClass('hidden')
        } else {
            $(".scroll-toRight").removeClass('hidden')
        }
    }

    $(".scroll-toLeft").hover(loopl, stop);
    $(".scroll-toRight").hover(loopr, stop);

    $('.scroll-table').scroll(function (ev) {
        scrollHidden()
    });

    scrollHidden()


}
