<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $USER;
if (USER_ID) {
    $res = CUser::GetList($by = "id", $order = "asc", ['ID' => USER_ID], ['SELECT' => ["UF_FAVS"]])->Fetch();
    $favs = $res["UF_FAVS"];
}

$MAT = false;
if ($arParams["MCART_ANALOGS_TOGETHER"]) {
    $MAT = true;
}
?>

<? if ($arParams["SET_TITLE"] == "Y"): ?>
    <? $this->SetViewTarget('catalog_title'); ?>
<!--    <section class="catalog_title">-->
<!--        <div class="wrapper">-->
<!--            <div class="section-header">-->
<!--                <h2>--><?//= $arResult["NAME"]; ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
    <? $this->EndViewTarget(); ?>
<? endif; ?>

<? $actualLink = "https://" . $_SERVER["SERVER_NAME"] . $APPLICATION->GetCurPage(false); ?>

<? $this->SetViewTarget('link_canonical'); ?>
<link rel="canonical" href="<?= $actualLink ?>"/>
<? $this->EndViewTarget(); ?>

<?
//unset($arResult["ITEMS"]);
//echo "<pre>";
//print_r($arResult);
//echo "</pre>";
$pagination = $arResult["NAV_RESULT"];
?>

<? $this->SetViewTarget('link_pagination'); ?>
<? if ($pagination->NavPageNomer > 1): ?>
    <meta name="robots" content="noindex, follow"/>
<? endif; ?>


<? if ($pagination->NavPageCount > 1): ?>

    <? $pageNum = $pagination->NavPageNomer; ?>
    <? if ($pageNum == 1): ?>
        <link rel="next" href="<?= $actualLink ?>?PAGEN_1=2">
    <? elseif ($pageNum == 2): ?>
        <link rel="prev" href="<?= $actualLink ?>">
        <link rel="next" href="<?= $actualLink ?>?PAGEN_1=3">
    <? elseif ($pageNum == $pagination->NavPageCount): ?>
        <link rel="prev" href="<?= $actualLink ?>?PAGEN_1=<?= $pageNum - 1; ?>">
    <? else: ?>
        <link rel="prev" href="<?= $actualLink ?>?PAGEN_1=<?= $pageNum - 1; ?>">
        <link rel="next" href="<?= $actualLink ?>?PAGEN_1=<?= $pageNum + 1; ?>">
    <? endif; ?>
<? endif; ?>
<? $this->EndViewTarget(); ?>

<section class="analogue" id="<?= $arParams["LIST_ID"] ?>">
    <div class="wrapper">

        <? if (!empty($arResult["CURRENT_ELEMENT"])): ?>
            <div class="section-header">
                <span class="current-product"><?= $arResult["CURRENT_ELEMENT"]["NAME"]; ?></span>
                <h2><?= $arParams["SECTION_NAME"]; ?></h2>
            </div>
        <? endif; ?>

        <div class="section-content">
            <div class="scroll-table__wrapper">
<!--                --><?// $flagModalOptionsShow = $arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_WIN"] ? true : false; ?>
<!--                --><?// if ($flagModalOptionsShow): ?>
<!--                    --><?// $flagModalOptionsShow = false; ?>
<!--                    <div class="table-filter__selector">-->
<!--						<span class="items-selector"><svg-->
<!--                                    xmlns:dc="http://purl.org/dc/elements/1.1/"-->
<!--                                    xmlns:cc="http://creativecommons.org/ns#"-->
<!--                                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"-->
<!--                                    xmlns:svg="http://www.w3.org/2000/svg"-->
<!--                                    xmlns="http://www.w3.org/2000/svg"-->
<!--                                    viewBox="0 0 510.66666 512" xml:space="preserve" id="svg2"-->
<!--                                    version="1.1"><metadata id="metadata8"><rdf:RDF><cc:Work-->
<!--                                                rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type-->
<!--                                                    rdf:resource="http://purl.org/dc/dcmitype/StillImage"/></cc:Work></rdf:RDF></metadata><defs-->
<!--                                        id="defs6"/><g-->
<!--                                        transform="matrix(1.3333333,0,0,-1.3333333,0,512)"-->
<!--                                        id="g10"><g transform="scale(0.1)" id="g12"><path-->
<!--                                                id="path14"-->
<!--                                                style="fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none"-->
<!--                                                d="m 3633.59,2326.79 -268.79,45.62 c -28,90.39 -64.02,177.58 -108,260.78 l 158.4,221.61 c 67.18,94.41 56.79,223.2 -25.59,304.8 l -238.4,238.4 c -44.8,44.8 -104.02,69.61 -167.23,69.61 -49.57,0 -96.79,-15.2 -136.79,-44.03 L 2624.8,3265.19 c -86.4,45.62 -176.79,83.2 -270.39,111.21 l -44.8,265.58 c -19.22,114.42 -117.62,197.62 -233.59,197.62 h -336.8 c -116.02,0 -214.42,-83.2 -233.63,-197.62 l -46.37,-271.99 c -89.61,-28.01 -176.84,-64.8 -260,-109.61 l -220.001,158.44 c -40,28.79 -88.008,43.98 -137.617,43.98 -63.204,0 -123.204,-24.8 -167.192,-69.61 L 435.199,3154.8 c -81.609,-81.61 -92.82,-210.4 -25.59,-304.81 l 160,-224.8 C 525.59,2541.2 490.391,2454.01 463.199,2363.58 L 197.621,2318.82 C 83.1992,2299.6 0,2201.2 0,2085.19 v -336.8 C 0,1632.41 83.1992,1534.01 197.621,1514.8 l 271.988,-46.41 c 28.012,-89.57 64.801,-176.8 109.61,-260 L 421.602,989.211 C 354.41,894.801 364.801,766.012 447.191,684.398 L 685.59,446.012 c 44.801,-44.813 104.019,-69.614 167.219,-69.614 49.57,0 96.8,15.192 136.8,43.981 l 224.801,160 C 1295.2,538 1380,503.578 1467.19,476.398 l 44.8,-268.789 C 1531.21,93.1914 1629.61,9.98828 1745.59,9.98828 h 337.61 c 116.02,0 214.42,83.20312 233.6,197.62072 l 45.58,268.789 c 90.43,28 177.62,63.981 260.82,108 l 221.6,-158.386 c 40,-28.793 88.01,-44.032 137.58,-44.032 63.21,0 122.42,24.809 167.23,69.61 l 238.4,238.398 c 81.6,81.602 92.77,210.391 25.58,304.813 L 3255.2,1217.22 c 44.02,83.98 80.82,171.17 108,260.78 l 268.79,44.8 c 114.42,19.18 197.62,117.58 197.62,233.6 v 336.79 c 1.6,116.02 -81.6,214.42 -196.02,233.6 z M 3615.2,1756.4 c 0,-10.39 -7.19,-19.18 -17.58,-20.78 L 3261.6,1679.6 c -42.38,-7.19 -76.01,-38.4 -86.4,-79.22 -30.4,-117.58 -76.8,-230.39 -139.18,-335.19 -21.61,-36.8 -20,-82.39 4.76,-117.58 L 3238.4,869.211 c 5.58,-8.012 4.8,-20 -2.38,-27.231 l -238.4,-238.402 c -5.63,-5.578 -11.21,-6.359 -15.24,-6.359 -4.76,0 -8.79,1.601 -11.99,3.98 L 2692.81,798.82 c -34.41,24.758 -80.82,26.371 -117.61,4.758 C 2470.39,741.199 2357.62,694.801 2240,664.398 2198.4,654.012 2167.19,619.602 2160.78,578 l -56.8,-336.02 c -1.6,-10.39 -10.39,-17.582 -20.78,-17.582 h -336.79 c -10.39,0 -19.22,7.192 -20.82,17.582 L 1669.61,578 c -7.23,42.379 -38.4,76.012 -79.22,86.398 -114.37,29.614 -224.8,75.204 -328.01,134.422 -16.79,9.571 -35.97,14.371 -54.37,14.371 -21.6,0 -44.03,-6.402 -62.42,-20 l -280,-199.179 c -3.988,-2.422 -7.969,-4.024 -12,-4.024 -3.199,0 -9.61,0.821 -15.192,6.41 L 600,834.801 c -7.191,7.179 -8.012,18.39 -2.379,27.179 L 794.41,1138 c 24.809,35.19 26.371,81.6 4.809,118.4 -62.418,103.98 -110.43,216.79 -140.821,334.41 -11.207,40.78 -44.808,71.99 -86.41,79.18 l -338.398,57.62 c -10.391,1.6 -17.57,10.39 -17.57,20.78 v 336.8 c 0,10.43 7.179,19.21 17.57,20.82 l 333.601,55.97 c 42.418,7.23 76.789,38.4 87.219,80 29.57,117.62 75.199,231.21 136.801,336.02 21.598,36.8 19.18,82.38 -5.621,116.8 l -199.18,280 c -5.629,8 -4.808,20 2.379,27.18 l 238.402,238.4 c 5.618,5.63 11.207,6.41 15.188,6.41 4.812,0 8.832,-1.6 12.031,-3.99 l 275.98,-196.79 c 35.2,-24.81 81.6,-26.41 118.4,-4.81 104.02,62.38 216.8,110.39 334.41,140.78 40.78,11.21 72,44.81 79.18,86.41 l 57.62,338.4 c 1.6,10.43 10.39,17.61 20.78,17.61 h 336.84 c 10.39,0 19.18,-7.18 20.78,-17.61 l 56.01,-333.6 c 7.19,-42.38 38.4,-76.79 80,-87.18 120.79,-30.39 235.98,-77.62 343.21,-140.82 36.79,-21.61 82.38,-20 117.58,4.8 l 276.01,198.4 c 3.99,2.42 8.01,4.02 11.99,4.02 3.21,0 9.61,-0.82 15.2,-6.4 l 238.4,-238.4 c 7.18,-7.23 8,-18.4 2.42,-27.23 L 3041.6,2702.8 c -24.8,-34.41 -26.4,-80.82 -4.8,-117.61 62.42,-104.81 108.79,-217.58 139.22,-335.2 10.39,-41.6 44.76,-72.77 86.36,-79.18 l 336.02,-56.8 c 10.39,-1.6 17.62,-10.43 17.62,-20.82 V 1756.4 Z"/><path-->
<!--                                                id="path16"-->
<!--                                                style="fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none"-->
<!--                                                d="m 1915.2,2751.59 c -455.98,0 -826.41,-370.39 -826.41,-826.4 0,-455.98 370.43,-826.37 826.41,-826.37 456.01,0 826.4,370.39 826.4,826.37 0,456.01 -370.39,826.4 -826.4,826.4 z m 0,-1436.79 c -336.8,0 -610.4,273.59 -610.4,610.39 0,336.79 273.6,610.43 610.4,610.43 336.79,0 610.39,-273.64 610.39,-610.43 0,-336.8 -273.6,-610.39 -610.39,-610.39 z"/></g></g></svg></span>-->
<!--                        <div class="table-filter__list">-->
<!--                            <div class="filter-list__wrapper" id="option_window">-->
<!--                                --><?// $checked = array_column($arResult["COLUMNS"], "PROPERTY_CODE"); ?>
<!--                                --><?// foreach ($arParams["PARAMS_SORT_CATALOG"]["PROPERTY_ON_WIN"] as $id => $code): ?>
<!--                                    --><?// if (!empty($arResult["COLUMN_LIST"][$code])): ?>
<!--                                        <div class="table-filter__item">-->
<!--                                            <label><input name="select_option" value="--><?//= $code ?><!--"-->
<!--                                                          type="checkbox" --><?//= in_array($code, $checked) ? "checked" : "" ?><!-->
<!--                                                --><?// if (!empty($arResult["PROPERTIES_HINTS"][$id]["SHORT"])): ?>
<!--                                                    --><?//= $arResult["PROPERTIES_HINTS"][$id]["SHORT"]; ?>
<!--                                                --><?// else: ?>
<!--                                                    --><?//= $arResult["COLUMN_LIST"][$code] ?>
<!--                                                --><?// endif; ?>
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    --><?// endif; ?>
<!--                                --><?// endforeach; ?>
<!--                            </div>-->
<!--                            <div>-->
<!--                                --><?//
//                                $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
//                                $uriString = $request->getRequestUri();
//                                $uri = new \Bitrix\Main\Web\Uri($uriString);
//                                $url = $uri->deleteParams(["OPTIONS_MODAL"])->getUri();
//                                ?>
<!--                                <a class="default-button add-table-filter" href="--><?//= $url ?><!--">-->
<!--                                    --><?//= Loc::GetMessage("CT_BCS_CATALOG_MESS_BTN_APPLY") ?>
<!--                                </a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                --><?// endif; ?>

                <div class="table-full">

                    <!--left table-->

                    <div class="table-left">
                        <table class="analogue-table__left">
                            <thead>
                            <tr>
                                <th>
                                    <? $column = reset($arResult["COLUMNS"]); ?>
                                    <div class="table-filter">

                                        <? if (strlen($column["NAME"]) > 0): ?>
                                            <?
                                            $urlParams = [
                                                $arParams["SORT_CODE"] . "=" . $column["CODE"],
                                                $arParams["ORDER_CODE"] . "=" . ($column["SORT"] === "desc" ? "asc" : "desc"),
                                            ];
                                            $url = $APPLICATION->GetCurPageParam(implode("&", $urlParams), [$arParams["SORT_CODE"], $arParams["ORDER_CODE"]]);
                                            ?>
                                            <div class="table-filter___item <?= $column["SORT_CLASS"] ?>"
                                                 code="<?= $column["CODE"] ?>">


                                                <? if (!empty($column["HINT"])): ?>
                                                    <span class="table-filter__hint" title="<?= $column["HINT"]; ?>">
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         version="1.1" width="55" height="55"
                                                                         viewBox="-27.5 -27.5 55 55">
																		<circle r="5.5" fill="black"/>
																		<circle r="5.5" cy="-14" fill="black"/>
																		<circle r="5.5" cy="14" fill="black"/>
																	</svg>

															</span>
                                                <? endif; ?>


                                                <span class="table-filter__name"><?= $column["NAME"]; ?></span>
                                                <a class="table-filter__arrow" data-href="<?= $url ?>"></a>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="left_elements">
                            <!-- items-container -->
                            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                                <? $area_id = $this->GetEditAreaId($arItem["ID"]); ?>

                                <? $arItem["DISPLAY_PROPERTIES"] = array_slice($arItem["DISPLAY_PROPERTIES"], 0, 1); ?>
                                <? $itemParams = [
                                    "RESULT" => [
                                        "COLUMNS" => array_slice($arResult["COLUMNS"], 0, 1),
                                        "ITEM" => $arItem,
                                        "AREA_ID" => $area_id . "_str_left",
                                        "IS_FAV" => in_array($arItem["ID"], $favs),
                                        "SECTION_NAME" => $arResult["NAME"],
                                        "PACK_INFO" => $arItem["PACK_INFO"],
                                        "ITEM_MEASURE" => $arItem["ITEM_MEASURE"],
                                        "BASKET_PRODS" => $arResult["BASKET_PRODS"],
                                        "MEASURE_FRACTIONAL" => $arResult["MEASURE_FRACTIONAL"],
                                        "MEASURE_DATA" => $arResult["MEASURE_DATA"],
                                    ],
                                    "PARAMS" => [
                                        "PROPERTY_LEFT" => $arParams["PROPERTY_CODE_FAST_SHOW_LEFT"],
                                        "PROPERTY_RIGHT" => $arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"],
                                    ],
                                ];
                                ?>
                                <? $APPLICATION->IncludeComponent(
                                    'bitrix:catalog.item',
                                    'line',
                                    $itemParams,
                                    $component,
                                    array('HIDE_ICONS' => 'Y')
                                ); ?>
                                <tr class="mini-card">
                                    <td>

                                    </td>
                                </tr>
                            <? endforeach; ?>
                            <tr class="left-container" style="display: none">
                                <td></td>
                            </tr>
                            <!-- items-split -->
                            </tbody>
                        </table>

                    </div>
                    <div class="scroll-table table-right ">
                        <table class="analogue-table">
                            <thead>
                            <tr>

                                <? foreach (array_slice($arResult["COLUMNS"], 1) as $column): ?>
                                    <th>
                                        <div class="table-filter">

                                            <? if (strlen($column["NAME"]) > 0): ?>
                                                <?
                                                $urlParams = [
                                                    $arParams["SORT_CODE"] . "=" . $column["CODE"],
                                                    $arParams["ORDER_CODE"] . "=" . ($column["SORT"] === "desc" ? "asc" : "desc"),
                                                ];
                                                $url = $APPLICATION->GetCurPageParam(implode("&", $urlParams), [$arParams["SORT_CODE"], $arParams["ORDER_CODE"]]);
                                                ?>
                                                <div class="table-filter___item <?= $column["SORT_CLASS"] ?>"
                                                     code="<?= $column["CODE"] ?>">


                                                    <? if (!empty($column["HINT"])): ?>
                                                        <span class="table-filter__hint"
                                                              title="<?= $column["HINT"]; ?>">
																		<svg xmlns="http://www.w3.org/2000/svg"
                                                                             version="1.1" width="55" height="55"
                                                                             viewBox="-27.5 -27.5 55 55">
																			<circle r="5.5" fill="black"/>
																			<circle r="5.5" cy="-14" fill="black"/>
																			<circle r="5.5" cy="14" fill="black"/>
																		</svg>

																</span>
                                                    <? endif; ?>


                                                    <span class="table-filter__name"><?= $column["NAME"]; ?></span>
                                                    <a class="table-filter__arrow" data-href="<?= $url ?>"></a>
                                                </div>
                                            <? endif; ?>
                                        </div>

                                    </th>
                                <? endforeach; ?>
                            </tr>
                            </thead>
                            <? /* Item row output */ ?>
                            <tbody id="right_elements">

                            <!-- items-split -->
                            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                                <? $area_id = $this->GetEditAreaId($arItem["ID"]); ?>
                                <? $arItem["DISPLAY_PROPERTIES"] = array_slice($arItem["DISPLAY_PROPERTIES"], 1); ?>
                                <? $itemParams = [
                                    "RESULT" => [
                                        "COLUMNS" => array_slice($arResult["COLUMNS"], 1),
                                        "ITEM" => $arItem,
                                        "AREA_ID" => $area_id . "_str",
                                        "IS_FAV" => in_array($arItem["ID"], $favs),
                                        "SECTION_NAME" => $arResult["NAME"],
                                        "PACK_INFO" => $arItem["PACK_INFO"],
                                        "ITEM_MEASURE" => $arItem["ITEM_MEASURE"],
                                        "BASKET_PRODS" => $arResult["BASKET_PRODS"],
                                        "MEASURE_FRACTIONAL" => $arResult["MEASURE_FRACTIONAL"],
                                        "MEASURE_DATA" => $arResult["MEASURE_DATA"],
                                        "DECLINATION_OBJ" => $arResult["DECLINATION_OBJ"],
                                    ],
                                    "PARAMS" => [
                                        "PROPERTY_LEFT" => $arParams["PROPERTY_CODE_FAST_SHOW_LEFT"],
                                        "PROPERTY_RIGHT" => $arParams["PROPERTY_CODE_FAST_SHOW_RIGHT"],
                                        "BREAD" => $arParams["BREAD"],
                                    ],
                                ];
                                ?>
                                <? $APPLICATION->IncludeComponent(
                                    'bitrix:catalog.item',
                                    'line',
                                    $itemParams,
                                    $component,
                                    array('HIDE_ICONS' => 'Y')
                                ); ?>
                                <? $itemParams["RESULT"]["AREA_ID"] = $area_id; ?>
                                <? $APPLICATION->IncludeComponent(
                                    'bitrix:catalog.item',
                                    'mini',
                                    $itemParams,
                                    $component,
                                    array('HIDE_ICONS' => 'Y')
                                ); ?>
                            <? endforeach; ?>
                            <? if (count($arResult["ITEMS"]) == 0): ?>
                                <tr class="filter-nothing">
                                    <td colspan="<?= count($arResult["COLUMNS"]) ?>">
                                        <h2><?= Loc::GetMessage("CT_BCS_CATALOG_MESS_ELEMENTS_LIST_EMPTY") ?></h2>
                                        <p><?= Loc::GetMessage("CT_BCS_CATALOG_MESS_LESS_STRONG_FILTER") ?></p>
                                        <div class="remove-filters">
                                            <div class="remove-all ">
                                                <span class="delete-red"></span><span><?= Loc::GetMessage("CT_BCS_CATALOG_MESS_REMOVE_ALL_FILTERS") ?></span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <? endif; ?>
                            <tr class="items-container" style="display: none">
                                <td></td>
                            </tr>
                            <!-- items-container -->
                            </tbody>
                        </table>
                    </div>
                    <div class="scroll-blocks scroll-toLeft">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
                            <defs>
                                <clipPath>
                                    <path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/>
                                </clipPath>
                                <clipPath>
                                    <path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/>
                                </clipPath>
                            </defs>
                            <path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373"
                                  transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" fill="#4d4d4d"/>
                        </svg>
                    </div>
                    <div class="scroll-blocks scroll-toRight">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
                            <defs>
                                <clipPath>
                                    <path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/>
                                </clipPath>
                                <clipPath>
                                    <path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/>
                                </clipPath>
                            </defs>
                            <path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373"
                                  transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" fill="#4d4d4d"/>
                        </svg>
                    </div>
                    <div class="table-separator">
                        <div class="close-left"></div>
                    </div>
                </div>

            </div>
            <!-- pagination-container -->
            <? if (!$MAT): ?>
                <div class="pagination-container">
                    <?= $arResult["NAV_STRING"] ?>
                </div>
            <? endif; ?>
            <!-- pagination-container -->
        </div>

    </div>
    <!--do not show for analogue and together card-->
    <?if(!in_array($arParams["LIST_ID"], ['together', 'analogue-card'])):?>
        <div class="message-wrap">
            <div class="message-box">
                <div class="message">Для комфортного просмотра контента на мобильном устройстве, приведите устройство в
                    горизонтальное положение
                </div>
                <span class="message-close"></span>
            </div>
        </div>
    <?endif;?>
</section>
<script>
    new SymmetronCatalogSection("#<?=$arParams["LIST_ID"];?>", "<?=$arParams["OPTIONS_MODAL_CODE"]?>", <?=CUtil::PhpToJSObject($arResult["YA_RESULT"])?>);
    BX.Currency.setCurrencyFormat('RUB', <? echo \CUtil::PhpToJSObject($arResult["CURRENCY_INFO"], false, true); ?>);
</script>
