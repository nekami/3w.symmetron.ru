<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<? if (!empty($arResult["LINK"])) { ?>

	<? foreach ($arResult["LINK"] as $section) { ?> 

		<div class="links"><a class="catalog-link" href='<?=$section["SECTION_PAGE_URL"]?>?arrFilter_3043_<?=$arResult["BRAND_FOR_FITER"]?>=Y&arrFilter_3050_<?=$arResult["SERIA_FOR_FITER"]?>=Y&set_filter=' target="_blank">
						
			<?=$arParams["SERIA_NAME"]?> <?=GetMessage("IN_CATALOGUE")?> <?=$section["NAME"]?></a>
		</div>
	<? } ?>
	
<? } ?>