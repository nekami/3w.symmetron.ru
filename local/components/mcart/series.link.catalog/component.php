<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
	Bitrix\Iblock,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(!Loader::includeModule("iblock"))
{
	ShowError(Loc::getMessage("SIMPLECOMP_EXAM2_IBLOCK_MODULE_NONE"));
	return;
}

if(!empty($arParams['BRAND_CODE']) && !empty($arParams["SERIA_NAME"])) 
{ 
	if($this->startResultCache(false, [$arParams['BRAND_CODE'], $arParams["SERIA_NAME"]]))
	{	
		// по коду бренда ищем его ID
		
		$IdBrand = '';
	  
		$resBrands = CIBlockElement::GetList([], ['IBLOCK_CODE' => 'brands', 'CODE' => $arParams['BRAND_CODE'], 'ACTIVE' => 'Y'], false, ["nTopCount"=>1], ["ID"]);
		
		if ($arBrand = $resBrands->fetch())	
			if (!empty($arBrand["ID"])) $IdBrand = $arBrand["ID"];		

		if(!empty($IdBrand)) 
		{ 
			// параметры для фильтра

			$arResult["BRAND_FOR_FITER"] = abs(crc32($IdBrand));
			
			$arResult["SERIA_FOR_FITER"] = abs(crc32($arParams["SERIA_NAME"]));
			
			// поиск элементов каталога с текущей серией и производителем
			
			$resElement = CIBlockElement::GetList(
				[], 
				['IBLOCK_CODE' => 'pbd', 'PROPERTY_PRODUCERID' => $IdBrand, 'PROPERTY_series' => $arParams["SERIA_NAME"], 'ACTIVE' => 'Y'], 
				false, 
				false, 
				["ID", "IBLOCK_SECTION_ID"]
			);

			$arSectionId = [];
				
			while ($arElement = $resElement->fetch())
			{
				if (empty($arSectionId[$arElement["IBLOCK_SECTION_ID"]]))
				{		
					$arSectionId[$arElement["IBLOCK_SECTION_ID"]] = $arElement["IBLOCK_SECTION_ID"];						
				}
			}			
			
			// запрос на название категорий	
			
			$resSection = CIBlockSection::GetList(
				[],
				["ID" => $arSectionId, "ACTIVE" => 'Y'],
				false,				
				["ID", "NAME", "SECTION_PAGE_URL"],
				false
			);
			
			while($arSection = $resSection->GetNext())
			{
				$arResult["LINK"][] = $arSection;
			}

			unset($arSectionId);
		}
		
		$this->includeComponentTemplate();
	}
	else
	{
		echo Loc::getMessage("PARAMS_UNDEFINED");
	}
}
?>