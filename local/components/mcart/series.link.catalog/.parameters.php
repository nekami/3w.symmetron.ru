<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"BRAND_CODE" => array(
			"NAME" => GetMessage("BRAND_CODE"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["BRAND"]}'
		),	
		"SERIA_NAME" => array(
			"NAME" => GetMessage("SERIA_NAME"),
			"TYPE" => "STRING"			
		)
	)
);
?>