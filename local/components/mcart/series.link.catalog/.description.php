<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SERIES_LINK_CATALOG_NAME"),
	"DESCRIPTION" => GetMessage("SERIES_LINK_CATALOG_DESC"),
	"PATH" => array(
		"ID" => "content"
	),
);
?>