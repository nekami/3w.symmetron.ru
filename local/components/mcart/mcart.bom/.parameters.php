<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS"  =>  array(
        "IB_ID"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IB_ID_BOM"),
            "TYPE" => "STRING",
            "DEFAULT" => "4",
        ),

        "CODE_ART"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CODE_ART_BOM"),
            "TYPE" => "STRING",
            "DEFAULT" => "ARTICLE",
        ),

        "PRODUCERID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("PRODUCERID_BOM"),
            "TYPE" => "STRING",
            "DEFAULT" => "PRODUCERID",
        ),

        "PRODUCERID_IBLOCK" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("PRODUCERID_IBLOCK_BOM"),
            "TYPE" => "STRING",
            "DEFAULT" => "11",
        ),
    ),
);