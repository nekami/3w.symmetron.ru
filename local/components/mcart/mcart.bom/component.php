<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")) die();


function detectDelimiter($csvFile)
{
    $delimiters = array(
        ';' => 0,
        ',' => 0,
        "\t" => 0,
        "|" => 0
    );

    $handle = fopen($csvFile, "r");
    $firstLine = fgets($handle);
    fclose($handle);
    foreach ($delimiters as $delimiter => &$count) {
        $count = count(str_getcsv($firstLine, $delimiter));
    }

    return array_search(max($delimiters), $delimiters);
}

function removeDirectory($dir)
{
    if ($objs = glob($dir . "/*")) {
        foreach ($objs as $obj) {
            is_dir($obj) ? removeDirectory($obj) : unlink($obj);
        }
    }
    rmdir($dir);
}

function addFile($tmpName, $format, $arResult, $objComp)
{
    $arFile = array();
    $fileName = md5_file($tmpName);

    $arFile["SUBDIR"] = 'tmp_ex/' . substr($fileName, 0, 3);

    $arFile["SRC"] = "/upload/" . $arFile["SUBDIR"] . "/" . $fileName . $format;


    if (!file_exists($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"])) {
        mkdir($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"]);
    } else {
        removeDirectory($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"]);
        mkdir($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"]);
    }

    if (!move_uploaded_file($tmpName, $_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"])) {
        $arResult["ERROR"] = GetMessage('LOAD_FILE_ERROR');
        $arResult["FORM"] = true;
        $objComp->includeComponentTemplate();
        return false;
    }

    return $arFile;
}

function string2Slug($str)
{
    $str = trim($str);
    $str = str_replace(" ", "_", $str);
    $temp = explode("\\u", $str);
    $str = '';
    foreach ($temp as $bit) {
        $str .= substr($bit, 4);
    }
    $str = str_replace("'", "", $str);
    $str = str_replace("\"", "", $str);
    $str = str_replace("\\", "", $str);
    $str = str_replace("\/", "", $str);
    $str = str_replace("/", "", $str);
    $str = str_replace("?", "", $str);
    $str = str_replace("#", "", $str);
    $str = str_replace("&", "", $str);
    $str = str_replace("%", "", $str);
    $str = str_replace("!", "", $str);
    return $str;
}

if ($_REQUEST['type'] === "loadFile") {
    if (key_exists(0, $_FILES)) {
        $arResult["DATA"] = [];

        if (strpos($_FILES[0]["name"], ".xlsx")) {
            CModule::IncludeModule("mcart.downloadbom");

            $arFile = addFile($_FILES[0]['tmp_name'], ".xlsx", $arResult, $this);

            if (!empty($arFile)) {
                $xlsx = new SimpleXLSX($_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"]);

                $data1 = array();
                $firstDateLine = false;

                foreach ($xlsx->rows() as $fields) {
                    $emptyCount = 0;
                    $currow = array();

                    foreach ($fields as $col) {
                        if (empty(string2Slug($col))) {
                            $emptyCount += 1;
                        }

                        $currow[] = $col;
                    }

                    if (count($currow) <= $emptyCount) {
                        if ($firstDateLine) {
                            break;
                        }
                    } else {
                        $firstDateLine = true;
                        $data1[] = $currow;
                    }

                    if (count($data1) === 200) {
                        break;
                    }
                }

                removeDirectory($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"]);
                $arResult["DATA"] = $data1;
            }
        } elseif (strpos($_FILES[0]["name"], ".xls")) {
            CModule::IncludeModule("mcart.downloadbom");

            $arFile = addFile($_FILES[0]['tmp_name'], ".xls", $arResult, $this);

            if (!empty($arFile)) {
                $excel = new Spreadsheet_Excel_Reader($_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"]);

                $data1 = array();
                $firstDateLine = false;

                for ($r = 1; $r <= $excel->sheets[0]['numRows']; $r++) {
                    $emptyCount = 0;
                    $currow = array();
                    for ($c = 1; $c <= $excel->sheets[0]['numCols']; $c++) {
                        $value = $excel->sheets[0]['cells'][$r][$c];
                        if (empty(string2Slug($value))) {
                            $emptyCount += 1;
                        }

                        $currow[] = $value;
                    }

                    if (count($currow) <= $emptyCount) {
                        if ($firstDateLine) {
                            break;
                        }
                    } else {
                        $firstDateLine = true;
                        $data1[] = $currow;
                    }

                    if (count($data1) === 200) {
                        break;
                    }
                }

                removeDirectory($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"]);
                $arResult["DATA"] = $data1;
            }
        } elseif (strpos($_FILES[0]["name"], ".csv")) {
            $arFile = addFile($_FILES[0]['tmp_name'], ".csv", $arResult, $this);
            ini_set('auto_detect_line_endings', true);

            if (!empty($arFile)) {
                $data1 = array();

                $delimiter = detectDelimiter($_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"]);
                $handle = fopen($_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"], 'r');

                $firstDateLine = false;
                $arStrs = file($_SERVER["DOCUMENT_ROOT"] . $arFile["SRC"]);


                while (($data = fgetcsv($handle, 0, $delimiter)) !== false) {
                    $currow = array();
                    $emptyCount = 0;

                    foreach ($data as $v) {
                        if (empty(string2Slug($v))) {
                            $emptyCount += 1;
                        }

                        $currow[] = $v;
                    }

                    if (count($currow) <= $emptyCount) {
                        if ($firstDateLine) {
                            break;
                        }
                    } else {
                        $firstDateLine = true;
                        $data1[] = $currow;
                    }

                    if (count($data1) === 200) {
                        break;
                    }
                }

                removeDirectory($_SERVER["DOCUMENT_ROOT"] . "/upload/" . $arFile["SUBDIR"]);
                $arResult["DATA"] = $data1;
            }
        }

        if (empty($arResult["DATA"])) {
            $this->includeComponentTemplate();
        } else {
            $this->includeComponentTemplate("selectColumn");
        }
    } else {
        $this->includeComponentTemplate();
    }
} elseif ($_REQUEST['type'] === 'getBack') {
    $arResult["DATA"] = json_decode($_REQUEST["data"], true);
    if (empty($arResult["DATA"])) {
        $this->includeComponentTemplate();
    } else {
        $this->includeComponentTemplate("selectColumn");
    }
} elseif ($_REQUEST['type'] === "selectProduct") {
    $resultExcel = [];
    $_REQUEST["data"] = json_decode($_REQUEST["data"], true);

    if (($codeCol = array_search(1, $_REQUEST["col"])) !== false && ($totalCol = array_search(2, $_REQUEST["col"])) !== false) {
        foreach (array_keys($_REQUEST["select"]) as $row) {
            if (key_exists($_REQUEST["data"][$row][$codeCol], $resultExcel)) {
                $resultExcel[$_REQUEST["data"][$row][$codeCol]] = $resultExcel[$_REQUEST["data"][$row][$codeCol]] + IntVal($_REQUEST["data"][$row][$totalCol]);
            } else {
                $resultExcel[$_REQUEST["data"][$row][$codeCol]] = IntVal($_REQUEST["data"][$row][$totalCol]);
            }
        }

        $arSelect = Array("ID", "NAME", "PROPERTY_" . $arParams["CODE_ART"], "PROPERTY_" . $arParams["PRODUCERID"], "DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID" => $arParams["IB_ID"], "ACTIVE" => "Y", "PROPERTY_" . $arParams["CODE_ART"] => array_keys($resultExcel));

        if (empty($resultExcel)) {
            echo "Ошибка...";
            die;
        }

        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $result = [];
        $arProd = [];
        $sectionURL = [];
        while ($ob = $res->Fetch()) {
            if(key_exists(IntVal($ob["IBLOCK_SECTION_ID"]), $sectionURL)){
                $ob["DETAIL_PAGE_URL"] = $sectionURL[IntVal($ob["IBLOCK_SECTION_ID"])];

                $ob["DETAIL_PAGE_URL"] = str_replace("#ELEMENT_CODE#", $ob["CODE"], $ob["DETAIL_PAGE_URL"]);
                $ob["DETAIL_PAGE_URL"] = str_replace("#SITE_DIR#", "", $ob["DETAIL_PAGE_URL"]);
            }else{
                $sub_res = CIBlockSection::GetNavChain(IntVal($ob["IBLOCK_ID"]), IntVal($ob["IBLOCK_SECTION_ID"]));
                $url = "";
                while($sub_ob = $sub_res->Fetch()){
                    $url .= (strlen($url) ? "/" : "") . $sub_ob[CODE];
                }

                $ob["DETAIL_PAGE_URL"] = str_replace("#SECTION_CODE_PATH#", $url, $ob["DETAIL_PAGE_URL"]);

                $sectionURL[IntVal($ob["IBLOCK_SECTION_ID"])] = $ob["DETAIL_PAGE_URL"];

                $ob["DETAIL_PAGE_URL"] = str_replace("#ELEMENT_CODE#", $ob["CODE"], $ob["DETAIL_PAGE_URL"]);
                $ob["DETAIL_PAGE_URL"] = str_replace("#SITE_DIR#", "", $ob["DETAIL_PAGE_URL"]);
            }

            $arProd[] = $ob["PROPERTY_" . $arParams["PRODUCERID"] . "_VALUE"];
            $result[$ob["ID"]] = $ob;
            $result[$ob["ID"]]["QUANTITY_EXCEL"] = IntVal($resultExcel[$ob["PROPERTY_" . $arParams["CODE_ART"] . "_VALUE"]]);
        }

        if (empty($result)) {
            echo "По вашему запросу не удалось распознать не одного компонента. Проверьте корректность выбранных столбцов и повторите запрос.";
            die();
        }

        $measureArr = [];
        $res = CCatalogMeasureRatio::GetList(array(), array('PRODUCT_ID' => array_keys($result), false, false, array()));
        while ($ob = $res->GetNext()) {
            $measureArr[$ob["PRODUCT_ID"]] = $ob["RATIO"];
        }

        $arSelect = Array("ID", "QUANTITY", "AVAILABLE", "MEASURE");
        $arFilter = Array("ID" => array_keys($result));
        $res = CCatalogProduct::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->Fetch()) {
            $result[$ob["ID"]]["QUANTITY"] = $ob["QUANTITY"];
            $result[$ob["ID"]]["AVAILABLE"] = $ob["AVAILABLE"];
            $result[$ob["ID"]]["MEASURE"] = ($measureArr[$ob["ID"]] ?: 1);
            $result[$ob["ID"]]["PRICE"] = [];
        }

        $res = CPrice::GetListEx(
            array(),
            array(
                "PRODUCT_ID" => array_keys($result),
            ),
            false,
            false,
            array("QUANTITY_FROM", "QUANTITY_TO", "PRICE", "PRODUCT_ID")
        );
        while ($ob = $res->Fetch()) {
            $result[$ob["PRODUCT_ID"]]["PRICE"][] = $ob;
        }

        if (!empty($arProd)) {
            $arSelect = Array("ID", "NAME");
            $arFilter = Array("IBLOCK_ID" => IntVal($arParams["PRODUCERID_IBLOCK"]), "ID" => $arProd);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

            $arProd = array();
            while ($arPr = $res->Fetch()) {
                $arProd[$arPr["ID"]] = $arPr["NAME"];
            }
        } else {
            $arProd = [];
        }

        $arResult["DATA"] = [];
        foreach ($result as $value) {
            $arResult["DATA"][$value["ID"]] = [
                "ID" => $value["ID"],
                "NAME" => $value["NAME"],
                "QUANTITY" => IntVal($value["QUANTITY"]),
                "QUANTITY_EXCEL" => IntVal($value['QUANTITY_EXCEL']),
                "ARTICLE" => $value["PROPERTY_" . $arParams["CODE_ART"] . "_VALUE"],
                "PRODUCER" => $arProd[IntVal($value["PROPERTY_" . $arParams["PRODUCERID"] . "_VALUE"])],
                "AVAILABLE" => $value["AVAILABLE"],
                "MEASURE" => IntVal($value["MEASURE"]),
                "URL" => $value["DETAIL_PAGE_URL"],
            ];

            if (empty($value['PRICE'])) {
                $arResult["DATA"][$value["ID"]]["STATUS"] = "MISSING";
            } elseif (IntVal($value['QUANTITY']) === 0) {
                $arResult["DATA"][$value["ID"]]["STATUS"] = "QUANTITY";
            } elseif (IntVal($value['QUANTITY']) < IntVal($value["MEASURE"])) {
                $arResult["DATA"][$value["ID"]]["STATUS"] = "MULTIPLICITY";
            } elseif (IntVal($value['QUANTITY']) < IntVal($value['QUANTITY_EXCEL'])) {
                $arResult["DATA"][$value["ID"]]["STATUS"] = "QUANTITY";
            } else {
                $arResult["DATA"][$value["ID"]]["STATUS"] = "OTHER";
            }

            if (!empty($value['PRICE'])) {
                $arResult["DATA"][$value["ID"]]["PRICE"] = false;
                foreach ($value['PRICE'] as $price) {
                    if ((IntVal($price["QUANTITY_FROM"]) <= IntVal($value["QUANTITY"]) || IntVal($price["QUANTITY_FROM"]) == 0) && (IntVal($price["QUANTITY_TO"]) >= IntVal($value["QUANTITY"]) || IntVal($price["QUANTITY_TO"]) == 0)) {
                        $arResult["DATA"][$value["ID"]]["PRICE"] = $price["PRICE"];
                    }
                }
            } else {
                $arResult["DATA"][$value["ID"]]["PRICE"] = false;
            }
        }

        $this->includeComponentTemplate('selectProduct');
    } else {
        echo "Ошибка...";
        die();
    }
} elseif ($_REQUEST["type"] === "addStaff") {
    $arFields = array(
        "PRODUCT_ID" => IntVal($_REQUEST["ID"]),
        "PRICE" => IntVal($_REQUEST["PRICE"]),
        "CURRENCY" => "RUB",
        "QUANTITY" => IntVal($_REQUEST["QUANTITY_EXCEL"]),
        "LID" => LANG,
        "DELAY" => "N",
        "CAN_BUY" => "Y",
        "NAME" => htmlspecialchars($_REQUEST["NAME"]),
        "DETAIL_PAGE_URL" => $_REQUEST["URL"],
    );

    if ($_REQUEST["STATUS"] === "MISSING") {
        $arFields["PRICE"] = 0;
        $arFields["PROPS"] = array(
            array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "MISSING", "SORT" => 100)
        );
    } elseif ($_REQUEST["STATUS"] === "QUANTITY") {
        $arFields["PRICE"] = 0;
        $arFields["PROPS"] = array(
            array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "QUANTITY", "SORT" => 100)
        );
    } elseif ($_REQUEST["STATUS"] === "MULTIPLICITY") {
        $arFields["PRICE"] = 0;
        $arFields["PROPS"] = array(
            array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "MULTIPLICITY", "SORT" => 100)
        );
    }

    $res = CSaleBasket::Add($arFields);

    echo json_encode(['result' => $res]);
} elseif($_REQUEST["type"] === "addStaffOrder" || $_REQUEST["type"] === "addStaffRequest" || $_REQUEST["type"] === "addStaffSamples"){
    $result = [];
    foreach ($_REQUEST["data"] as $key => $val){
        if($_REQUEST["type"] === "addStaffOrder" && $val["STATUS"] !== "OTHER"){
            continue;
        }elseif($_REQUEST["type"] === "addStaffRequest" && $val["STATUS"] === "OTHER"){
            continue;
        }

        $arFields = array(
            "PRODUCT_ID" => IntVal($val["ID"]),
            "PRICE" => IntVal($val["PRICE"]),
            "CURRENCY" => "RUB",
            "QUANTITY" => IntVal($val["QUANTITY_EXCEL"]),
            "LID" => LANG,
            "DELAY" => "N",
            "CAN_BUY" => "Y",
            "NAME" => htmlspecialchars($val["NAME"]),
            "DETAIL_PAGE_URL" => $val["URL"],
        );

        if($_REQUEST["type"] === "addStaffSamples"){
            $arFields["PRICE"] = 0;
            $arFields["PROPS"] = array(
                array("NAME" => "Образец", "CODE" => "EXAMPLE", "VALUE" => "Y", "SORT" => 100)
            );
        }elseif ($val["STATUS"] === "MISSING") {
            $arFields["PRICE"] = 0;
            $arFields["PROPS"] = array(
                array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "MISSING", "SORT" => 100)
            );
        } elseif ($val["STATUS"] === "QUANTITY") {
            $arFields["PRICE"] = 0;
            $arFields["PROPS"] = array(
                array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "QUANTITY", "SORT" => 100)
            );
        } elseif ($val["STATUS"] === "MULTIPLICITY") {
            $arFields["PRICE"] = 0;
            $arFields["PROPS"] = array(
                array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "MULTIPLICITY", "SORT" => 100)
            );
        }

        $result[$val["ID"]] = CSaleBasket::Add($arFields);
    }

    echo json_encode(['result' => $result]);
} else {
    $this->includeComponentTemplate();
}