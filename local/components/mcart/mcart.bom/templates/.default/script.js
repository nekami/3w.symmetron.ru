function onClickFakeInput() {
    $("#form-for-bom #bom-file").click();
}

function onChangeFileInput() {
    let self = $(this);
    if (self.val()) {
        var filename = self.val().replace(/C:\\fakepath\\/i, '');
        $(this).closest('.bomfile-holder').find('.chose-file').html(filename);

        let files = this.files;

        let data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        $.ajax({
            url: BX.message("URL") + '/ajax.php?type=loadFile',
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                document.querySelector(".work-bom").innerHTML = data;
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
}

function getHome(event) {
    event.preventDefault();
    let data = new FormData();

    $.ajax({
        url: BX.message("URL") + '/ajax.php',
        data: data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            document.querySelector(".work-bom").innerHTML = data;
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function openCloseSelect() {
    let self = $(this).parent();

    if (self.hasClass('open')) {
        self.removeClass('open')
    } else {
        self.addClass('open')
    }
}

function closeSelect(e) {
    var div = $("#popup");

    $.each($('div.bom-form .project-select__btn'), function (i, v) {
        v = $(v);
        if (!v.is(e.target)) {
            v.parent().removeClass('open');
        }
    });
}

function selectOption() {
    let self = $(this);

    var selectedText = self.children('label').text();
    var selectedValue = self.children('label').children('input').val();

    self.parent().parent().siblings('.project-select__btn').text(selectedText);
    self.parent().parent().siblings('input:hidden').val(selectedValue);

    if (self.parent().parent().parent().hasClass('open')) {
        self.parent().parent().parent().removeClass('open');
    }

    if (selectedValue == "1" || selectedValue == "2") {
        self.parent().parent().parent().addClass("selectMcart");
    } else {
        self.parent().parent().parent().removeClass("selectMcart");
    }


    let input = self.parent().parent().siblings('input:hidden');
    $.each($("div.project-select input.selectCol"), function (i, v) {
        let selfV = $(v);

        if (input.attr('name') === selfV.attr('name')) {
            return;
        }

        if (input.val() === selfV.val()) {
            selfV.parent().children(".project-select__btn").text('Не выбрано');
            selfV.parent().removeClass("selectMcart");
            selfV.val(0);
        }
    });

    verification();
}

function verification() {
    let col1 = $("div.project-select input.selectCol[value=1]");
    let col2 = $("div.project-select input.selectCol[value=2]");

    if (col1.length !== 0 && col2.length !== 0 && $("input.mcartCheckbox:checked").length > 0) {
        document.querySelector("div.bom-form .buttons :first-child").classList.remove("disabled");
    } else {
        document.querySelector("div.bom-form .buttons :first-child").classList.add("disabled");
    }
}

function verification2() {
    if ($("input.mcartCheckbox2:checked").length > 0) {
        document.querySelector("div.bom-form .buttons #button-add-order").classList.remove("disabled");
        document.querySelector("div.bom-form .buttons #button-add-samples").classList.remove("disabled");
        document.querySelector("div.bom-form .buttons #button-add-request").classList.remove("disabled");
    } else {
        document.querySelector("div.bom-form .buttons #button-add-order").classList.add("disabled");
        document.querySelector("div.bom-form .buttons #button-add-samples").classList.add("disabled");
        document.querySelector("div.bom-form .buttons #button-add-request").classList.add("disabled");
    }
}

function allRows(event) {
    event.preventDefault();

    let table = document.querySelector("section.work-bom table");

    if (table.classList.contains("all")) {
        table.classList.remove("all");
        this.value = "Показать все";
    } else {
        table.classList.add("all");
        this.value = "Скрыть";
    }
}

function checkboxAll() {
    $("div.bom-form .mcartCheckbox").prop("checked", $(this).prop("checked"));

    verification();
}

function checkboxAll2() {
    $("div.bom-form .mcartCheckbox2").prop("checked", $(this).prop("checked"));

    verification2();
}

$('#bom-file').change(function () {
    if ($(this).val()) {
        var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
        $(this).closest('.bomfile-holder').find('.chose-file').html(filename);
    }
});

function selectProduct(event) {
    event.preventDefault();
    let self = $("#form-for-bom");

    $.ajax({
        url: BX.message("URL") + '/ajax.php?type=selectProduct',
        data: self.serialize(),
        type: 'POST',
        success: function (data) {
            document.querySelector(".work-bom").innerHTML = data;
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function getBack(event) {
    event.preventDefault();
    let self = $("#form-for-bom");

    $.ajax({
        url: BX.message("URL") + '/ajax.php?type=getBack',
        data: self.serialize(),
        type: 'POST',
        success: function (data) {
            document.querySelector(".work-bom").innerHTML = data;
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function addStaff(self) {
    self = $(self);
    let input = $("input#staff_" + self.attr('data-val'));

    if (input) {
        $.ajax({
            url: BX.message("URL") + '/ajax.php?type=addStaff',
            data: JSON.parse(input.val()),
            type: 'POST',
            success: function (data) {
                data = JSON.parse(data);

                input.replaceWith("");

                if (parseInt(data['result']) === 0) {
                    self.replaceWith('<div class="nrecognized">Не добавлено</div>');
                } else {
                    self.replaceWith('<div class="nrecognized">Добавлено</div>');
                }

                verification2();
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
}

function addStaffOrder() {
    let data = {
        data: {}
    };

    $.each($("input.mcartCheckbox2:checked"), function (i, v) {
        data.data[i] = JSON.parse(v.value);
    });

    $.ajax({
        url: BX.message("URL") + '/ajax.php?type=addStaffOrder',
        data: data,
        type: 'POST',
        success: function (data) {
            data = JSON.parse(data);

            if (data['result']) {
                $.each(data['result'], function (i, v) {
                    let input = $("input#staff_" + i);
                    let button = $("div#button_" + i);

                    input.replaceWith("");

                    if (parseInt(v) === 0) {
                        button.replaceWith('<div class="nrecognized">Не добавлено</div>');
                    } else {
                        button.replaceWith('<div class="nrecognized">Добавлено</div>');
                    }
                });

                verification2();
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function addStaffRequest() {
    let data = {
        data: {}
    };

    $.each($("input.mcartCheckbox2:checked"), function (i, v) {
        data.data[i] = JSON.parse(v.value);
    });

    $.ajax({
        url: BX.message("URL") + '/ajax.php?type=addStaffRequest',
        data: data,
        type: 'POST',
        success: function (data) {
            data = JSON.parse(data);

            if (data['result']) {
                $.each(data['result'], function (i, v) {
                    let input = $("input#staff_" + i);
                    let button = $("div#button_" + i);

                    input.replaceWith("");

                    if (parseInt(v) === 0) {
                        button.replaceWith('<div class="nrecognized">Не добавлено</div>');
                    } else {
                        button.replaceWith('<div class="nrecognized">Добавлено</div>');
                    }
                });

                verification2();
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function addStaffSamples(){
    let data = {
        data: {}
    };

    $.each($("input.mcartCheckbox2:checked"), function (i, v) {
        data.data[i] = JSON.parse(v.value);
    });

    $.ajax({
        url: BX.message("URL") + '/ajax.php?type=addStaffSamples',
        data: data,
        type: 'POST',
        success: function (data) {
            data = JSON.parse(data);

            if (data['result']) {
                $.each(data['result'], function (i, v) {
                    let input = $("input#staff_" + i);
                    let button = $("div#button_" + i);

                    input.replaceWith("");

                    if (parseInt(v) === 0) {
                        button.replaceWith('<div class="nrecognized">Не добавлено</div>');
                    } else {
                        button.replaceWith('<div class="nrecognized">Добавлено</div>');
                    }
                });

                verification2();
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

$(document).ready(function () {
    $(this).on('click', '#form-for-bom .fake-input', onClickFakeInput);
    $(this).on('change', '#form-for-bom #bom-file', onChangeFileInput);
    $(this).on('click', 'div.bom-form .buttons #button-home', getHome);
    $(this).on('click', 'div.bom-form .buttons #button-all', allRows);
    $(this).on('click', 'div.bom-form .buttons #button-next', selectProduct);
    $(this).on('click', 'div.bom-form .buttons #button-back', getBack);
    $(this).on('click', 'div.bom-form .buttons #button-add-order', addStaffOrder);
    $(this).on('click', 'div.bom-form .buttons #button-add-request', addStaffRequest);
    $(this).on('click', 'div.bom-form .buttons #button-add-samples', addStaffSamples);
    $(this).on("click", 'div.bom-form .project-select__btn', openCloseSelect);
    $(this).on("click", "div.bom-form .mcartSupperCheckbox", checkboxAll);
    $(this).on("click", "div.bom-form .mcartCheckbox", verification);
    $(this).on("click", "div.bom-form .mcartSupperCheckbox2", checkboxAll2);
    $(this).on("click", "div.bom-form .mcartCheckbox2", verification2);
    $(this).on("mouseup", this, closeSelect);
    $(this).on("click", 'div.project-select span.option', selectOption);
});