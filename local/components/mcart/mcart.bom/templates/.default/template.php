<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="work-bom">
    <div class="wrapper">
        <h1>Загрузка BOM</h1>
        <div class="bom-form">
            <form enctype="multipart/form-data" method="post" id="form-for-bom">
                <label class="requirements">
                    <span class="bom__label">Требования к файлам.</span>
                    <span class="bom__grey">1. В файле обязательно должны присутствовать столбцы Код товара производителя (Part Number) и требуемым количеством.</span>
                    <span class="bom__grey">2. Одновременно распознаем до 200 записей. Если между записями в файле есть пустая строка, файл будет распознан до этого разделителя.</span>
                    <br>
                    <span class="bom__grey">При успешном распознавании компонента вы сможет добавить в корзину.</span>
                </label>
                <br>
                <div class="bom-file">
                    <label for="bom-file">
                        <span class="bom__label">Прикрепите файл</span>
                        <span class="bom__grey">(*.csv, *.xlsx, *.xls до 1 Мб, без формул в ячейках)</span>
                    </label>
                    <div class="bomfile-holder">
                        <div class="fake-input">Выберите файл</div>
                        <input type="file" name="file" id="bom-file" required/>
                        <span class="chose-file">Файл отсутствует</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        BX.message({
            URL: '<? echo $this->GetFolder(); ?>'
        });
    </script>
</section>
