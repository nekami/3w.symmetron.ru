<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent(
    "mcart:mcart.bom",
    ".default",
    array(
        "CODE_ART" => "ARTICLE",
        "SYMPACKAGE" => "SYMPACKAGE",
        "PRODUCERID" => "PRODUCERID",
        "PRODUCERID_IBLOCK" => "11",
        "IB_ID" => "4",
    ),
    false
);