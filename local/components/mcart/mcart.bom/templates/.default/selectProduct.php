<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="work-bom">
    <div class="wrapper">
        <h1>Выбор столбцов для распознавания</h1>
        <div class="bom-form">
            <form enctype="multipart/form-data" method="post" id="form-for-bom">
                <input hidden name="data"
                       value='<?= json_encode($_REQUEST["data"], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) ?>'>
                <div>
                    <table class="all">
                        <tr>
                            <th><input class="mcartSupperCheckbox2"
                                       style="-webkit-appearance: checkbox !important;"
                                       value="1" type="checkbox"></th>
                            <th>Название конечной ветки каталога</th>
                            <th>Артикул</th>
                            <th>Производитель</th>
                            <th>Статус на складе</th>
                            <th>Количество</th>
                            <th>Статус</th>
                            <th>Цена за ед.</th>
                            <th>Действия</th>
                        </tr>
                        <? foreach ($arResult["DATA"] as $key => $value): ?>
                            <tr>
                                <td id="<?= "staff_" . $key ?>">
                                    <? if ($value["QUANTITY_EXCEL"] === 0): ?>
                                        <input data-val="<?= $key ?>"
                                               class="mcartCheckbox2"
                                               data-order="N"
                                               data-sample="N"
                                               data-request="N"
                                               style="-webkit-appearance: checkbox !important; display: none"
                                               name="select[<?= $key ?>]"
                                               value='<?= json_encode($value, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) ?>'
                                               type="checkbox">
                                    <? else: ?>
                                        <input data-val="<?= $key ?>"
                                               class="mcartCheckbox2"
                                               style="-webkit-appearance: checkbox !important;"
                                               data-order="<?= ($value["STATUS"] === "OTHER" ? "Y" : "N") ?>"
                                               data-sample="Y"
                                               data-request="<?= ($value["STATUS"] !== "OTHER" ? "Y" : "N") ?>"
                                               name="select[<?= $key ?>]"
                                               value='<?= json_encode($value, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) ?>'
                                               type="checkbox">
                                    <? endif; ?>
                                </td>
                                <td>
                                    <a href="<?= $value["URL"] ?>" target="_blank"><?= $value["NAME"] ?></a>
                                </td>
                                <td>
                                    <?= $value["ARTICLE"] ?>
                                </td>
                                <td>
                                    <?= $value["PRODUCER"] ?>
                                </td>
                                <td>
                                    <? if (!empty($value["AVAILABLE"])) { ?>
                                        <span class="favorite-status <?= $value["QUANTITY"] ? 'stock' : 'out' ?>">
                                                <?= $value["QUANTITY"] ? 'В наличии ' . $value["QUANTITY"] . ' шт.' : 'Нет в наличии' ?>
                                            </span>
                                    <? } else { ?>
                                        <span class="favorite-status out">Товар недоступен</span>
                                    <? } ?>
                                </td>
                                <td>
                                    <?= $value["QUANTITY_EXCEL"] ?>
                                </td>
                                <td id="status_<?= $key ?>">
                                    <? if ($value["QUANTITY_EXCEL"] === 0): ?>
                                        <div class="nrecognized">Нулевое количество</div>
                                    <? else: ?>
                                        <div class="nrecognized">Распознано</div>
                                    <? endif; ?>
                                </td>
                                <td>
                                    <?= $value["PRICE"] ?>
                                </td>
                                <td id="button_<?= $key ?>">
                                    <? if ($value["QUANTITY_EXCEL"] === 0): ?>
                                    <? elseif ($value["STATUS"] === "OTHER"): ?>
                                        <div data-status="OTHER" data-val="<?= $key ?>" class="status-add"
                                             onclick="addStaff(this);">+
                                            Добавить
                                        </div>
                                    <? elseif ($value["STATUS"] === "MISSING"): ?>
                                        <div data-status="MISSING" data-val="<?= $key ?>" class="status-add"
                                             onclick="addStaff(this);">+
                                            Запросить цену
                                        </div>
                                    <? elseif ($value["STATUS"] === "QUANTITY"): ?>
                                        <div data-status="QUANTITY" data-val="<?= $key ?>" class="status-add"
                                             onclick="addStaff(this);">+
                                            Запросить условия поставки
                                        </div>
                                    <? elseif ($value["STATUS"] === "MULTIPLICITY"): ?>
                                        <div data-status="MULTIPLICITY" data-val="<?= $key ?>" class="status-add"
                                             onclick="addStaff(this);">+
                                            Запросить данное количество
                                        </div>
                                    <? endif; ?>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    </table>
                </div>
            </form>

            <div class="buttons">
                <input type="submit" value="Оформить покупку" class="default-button upload-bom disabled"
                       id="button-add-order">
                <input type="submit" value="Запросить образцы" class="default-button upload-bom disabled"
                       id="button-add-samples">
                <input type="submit" value="Оформить запрос" class="default-button upload-bom disabled"
                       id="button-add-request">
                <input type="submit" value="Назад" class="default-button upload-bom" id="button-back">
            </div>
        </div>
    </div>
    <script>
        BX.message({
            URL: '<? echo $this->GetFolder(); ?>'
        });
    </script>
</section>