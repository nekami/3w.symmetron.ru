<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="work-bom">
    <div class="wrapper">
        <h1>Выбор столбцов для распознавания</h1>
        <div class="bom-form">
            <form enctype="multipart/form-data" method="post" id="form-for-bom">
                <input hidden name="data"
                       value='<?= json_encode($arResult["DATA"], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) ?>'>
                <div style="overflow-y: hidden;">
                    <table>
                        <? foreach ($arResult["DATA"] as $key => $value): ?>
                            <? if ($key === 0): ?>
                                <tr>
                                    <th>
                                        <input checked class="mcartSupperCheckbox"
                                               style="-webkit-appearance: checkbox !important;"
                                               value="1" type="checkbox">
                                    </th>
                                    <? foreach ($value as $subKey => $subValue): ?>
                                        <th>
                                            <div class="project-select">
                                                <div class="project-select__btn">Не
                                                    выбрано
                                                </div>
                                                <input class="selectCol" name="col[<?= $subKey ?>]" hidden
                                                       value="0">
                                                <div class="project-select__options">
                                                    <div class="options">
                                                        <span class="option">
                                                            <label><input type="radio" value="0">Не выбрано</label>
                                                        </span>
                                                        <span class="option">
                                                            <label><input type="radio" value="1">Код товара</label>
                                                        </span>
                                                        <span class="option">
                                                            <label><input type="radio" value="2">Количество</label>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    <? endforeach; ?>
                                </tr>
                                <tr>
                                    <th>
                                        <input checked class="mcartCheckbox"
                                               style="-webkit-appearance: checkbox !important;"
                                               name="select[<?= $key ?>]"
                                               value="1" type="checkbox">
                                    </th>
                                    <? foreach ($value as $subKey => $subValue): ?>
                                        <td>
                                            <?= $subValue ?>
                                        </td>
                                    <? endforeach; ?>
                                </tr>
                            <? else: ?>
                                <tr>
                                    <td>
                                        <input checked class="mcartCheckbox"
                                               style="-webkit-appearance: checkbox !important;"
                                               name="select[<?= $key ?>]"
                                               value="1" type="checkbox">
                                    </td>
                                    <? foreach ($value as $subKey => $subValue): ?>
                                        <td>
                                            <?= $subValue ?>
                                        </td>
                                    <? endforeach; ?>
                                </tr>
                            <? endif; ?>
                        <? endforeach; ?>
                    </table>
                </div>

                <div class="buttons">
                    <input type="submit" value="Далее" class="default-button upload-bom disabled" id="button-next">
                    <input type="submit" value="Показать все" class="default-button upload-bom" id="button-all">
                    <input type="submit" value="Назад" class="default-button upload-bom" id="button-home">
                </div>
            </form>
        </div>
    </div>
    <script>
        BX.message({
            URL: '<? echo $this->GetFolder(); ?>'
        });
    </script>
</section>
