<?
$MESS ['subscr_form_title_desc'] = "Выберите рассылку";
$MESS ['subscr_form_email_title'] = "Введите ваш e-mail";
$MESS ['subscr_reset_form_button'] = "Отписаться полностью";
$MESS ['subscr_form_button'] = "Подтвердить";//"Подписаться";
$MESS ['subscr_form_response_ERROR'] = "Что-то пошло не так";
$MESS ['subscr_form_response_NOTE'] = "Поздравляем!";
$MESS ['subscr_form_button_sent'] = "ГОТОВО";

$MESS ['mcart_symmetron_subscr_title'] = "<h2>Подпишитесь на нашу рассылку,</h2>
            <p>и получайте новости, спецпредложения анонсы событий и выставок по почте</p>";



$MESS ['SEM_EVERY_DAY'] = "Еженедельный дайджест новостей";
$MESS ['SEM_SHOW'] = "События и выставки";
$MESS ['SEM_PRODUCTS'] = "Продукция";
