<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$buttonId = $this->randString();
$prefix = "SYMMETRON_DEFAULT_SITE_TEMPLATE_";

global $USER;
if (empty($arResult["EMAIL"]) && $USER->IsAuthorized())
    $arResult["EMAIL"] = $USER->GetEmail();

if( $USER->IsAuthorized() ) {
    Loader::includeModule("sender");
    $subscriptionDb = \Bitrix\Sender\MailingSubscriptionTable::getSubscriptionList(array(
       'select' => array('ID' => 'CONTACT_ID', 'EMAIL' => 'CONTACT.CODE', 'EXISTED_MAILING_ID' => 'MAILING.ID'),
       'filter' => array('=CONTACT.CODE' => $USER->GetEmail(), '!MAILING.ID' => null),
    ));
    while ( $ob = $subscriptionDb->Fetch() )
        $arrSubs[] = (int) $ob["EXISTED_MAILING_ID"];
}
?>
<form class="lk-subscribe" id="bx_subscribe_subform_<?=$buttonId?>" role="form" method="POST">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="sender_subscription" id="sender_subscription" value="add" />
    <input type="hidden" name="old_email" value="<?=$arResult["EMAIL"]?>" />
    <div class="change-subscribe">
        <h3><?=Loc::getMessage($prefix."EDIT_EMAIL");?></h3>
        <div class="change-subscribe">
            <input type="email" class="subscribe-trigger" name="SENDER_SUBSCRIBE_EMAIL" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" placeholder="<?=htmlspecialcharsbx(GetMessage('subscr_form_email_title'))?>" <?=$arResult["EMAIL"] ? 'readonly':''?> />
        </div>
    </div>
    <div class="subscribe-lk" id="allCheckboxes">
        <?
        if( !empty($arrSubs) ) {
            foreach($arResult["RUBRICS"] as $ID => $itemValue)
                if( array_search($itemValue['ID'], $arrSubs) !== FALSE )
                    $arResult["RUBRICS"][$ID]['CHECK'] = 1; 
        }
        ?>
        <?if(!empty($arParams["EVERY_DAY"])):?>
        <div class="subscribe-inputs__item">
            <h4 class="head"><?=GetMessage("SEM_EVERY_DAY");?></h4>
            <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
            <?if(in_array($itemValue["ID"], $arParams["EVERY_DAY"])):?>
            <div class="subscribe-input"><label>
                <?if ($itemValue['CHECK']):?><input type="hidden" name="SENDER_UNSUBSCRIBE_RUB_ID[]" value="<?=$itemValue["ID"]?>"><?endif;?>
                <input type="checkbox" name="SENDER_SUBSCRIBE_RUB_ID[]" id="SENDER_SUBSCRIBE_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" <?=$itemValue['CHECK'] ? 'checked':''?>><?=$itemValue["NAME"];?>
            </label></div>
            <?endif;?>
            <?endforeach;?>
        </div>
        <?endif;?>
        <?if(!empty($arParams["SHOW"])):?>
        <div class="subscribe-inputs__item">
            <h4 class="head"><?=GetMessage("SEM_SHOW");?></h4>
            <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
            <?if(in_array($itemValue["ID"], $arParams["SHOW"])):?>
            <div class="subscribe-input"><label>
                <?if ($itemValue['CHECK']):?><input type="hidden" name="SENDER_UNSUBSCRIBE_RUB_ID[]" value="<?=$itemValue["ID"]?>"><?endif;?>
                <input type="checkbox" name="SENDER_SUBSCRIBE_RUB_ID[]" id="SENDER_SUBSCRIBE_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" <?=$itemValue['CHECK'] ? 'checked':''?>><?=$itemValue["NAME"];?>
            </label></div>
            <?endif;?>
            <?endforeach;?>
        </div>
        <?endif;?>
        <?if(!empty($arParams["PRODUCTS"])):?>
        <div class="subscribe-inputs__item">
            <h4 class="head"><?=GetMessage("SEM_PRODUCTS");?></h4>
            <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
            <?if(in_array($itemValue["ID"], $arParams["PRODUCTS"])):?>
            <div class="subscribe-input"><label>
                <?if ($itemValue['CHECK']):?><input type="hidden" name="SENDER_UNSUBSCRIBE_RUB_ID[]" value="<?=$itemValue["ID"]?>"><?endif;?>
                <input type="checkbox" name="SENDER_SUBSCRIBE_RUB_ID[]" id="SENDER_SUBSCRIBE_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" <?=$itemValue['CHECK'] ? 'checked':''?>><?=$itemValue["NAME"];?>
            </label></div>
            <?endif;?>
            <?endforeach;?>
        </div>
        <?endif;?>
    </div>
    <div class="subscribe-footer">
        <input id="bx_subscribe_reset_btn_<?=$buttonId?>" name="remove_all_subscription" type="submit" class="default-button change-all" value="<?=GetMessage("subscr_reset_form_button")?>">
        <input id="bx_subscribe_btn_<?=$buttonId?>" type="submit" class="sender-btn btn-subscribe default-button subscribe-submit" value="<?=GetMessage("subscr_form_button")?>">
    </div>
</form>


<?if(isset($arResult['MESSAGE'])): CJSCore::Init(array("popup"));?>
<div id="sender-subscribe-response-cont" style="display: none;">
    <div class="bx_subscribe_response_container">
        <table>
            <tr>
                <td style="padding-right: 40px; padding-bottom: 0px;"><img src="<?=($this->GetFolder().'/images/'.($arResult['MESSAGE']['TYPE']=='ERROR' ? 'icon-alert.png' : 'icon-ok.png'))?>" alt=""></td>
                <td>
                    <div style="font-size: 22px;"><?=GetMessage('subscr_form_response_'.$arResult['MESSAGE']['TYPE'])?></div>
                    <div style="font-size: 16px;"><?=htmlspecialcharsbx($arResult['MESSAGE']['TEXT'])?></div>
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
    BX.ready(function(){
        var oPopup = BX.PopupWindowManager.create('sender_subscribe_component', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: 'rgba(57,60,67,0.82)', opacity: '80'
            }
        });
        oPopup.setContent(BX('sender-subscribe-response-cont'));
        oPopup.show();
    });
</script>
<?endif;?>

<script>
    (function () {
        var btn = BX('bx_subscribe_btn_<?=$buttonId?>');
        var btn_reset = BX('bx_subscribe_reset_btn_<?=$buttonId?>');
        var form = BX('bx_subscribe_subform_<?=$buttonId?>');

        if(!btn)
        {
            return;
        }

        function mailSender()
        {
            setTimeout(function() {
                if(!btn)
                {
                    return;
                }

                var btn_span = btn.querySelector("span");
                var btn_subscribe_width = btn_span.style.width;
                    //BX.addClass(btn, "send");
                    btn_span.outterHTML = "<span><i class='fa fa-check'></i> <?=GetMessage("subscr_form_button_sent")?></span>";
                    if(btn_subscribe_width)
                    {
                        btn.querySelector("span").style["min-width"] = btn_subscribe_width+"px";
                    }
                }, 400);
        }

        BX.ready(function()
        {
            /*BX.bind(btn_reset, 'click', function() {
                setTimeout(mailSender, 250);
                return false;
            });*/

            BX.bind(btn, 'click', function() {
                setTimeout(mailSender, 250);
                return false;
            });
        });

        BX.bind(form, 'submit', function () {
            btn.disabled=true;
            setTimeout(function () {
                btn.disabled=false;
            }, 2000);

            return true;
        });
    })();
</script>