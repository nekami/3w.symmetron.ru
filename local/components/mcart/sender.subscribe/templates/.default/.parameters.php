<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

\Bitrix\Main\Loader::includeModule('sender');


//$arFilter = array("SITE_ID" => SITE_ID);
$mailingList = \Bitrix\Sender\Subscription::getMailingList(array());
$arList = array();
//$arList[0] = " -- ";
foreach($mailingList as $mailing)
{
    $arList[$mailing["ID"]] = $mailing["NAME"];
}

$arTemplateParameters = array(
    "EVERY_DAY"  =>  Array(
        "NAME" => GetMessage("SEM_EVERY_DAY"),
        "TYPE" => "LIST",
        "VALUES" => $arList,
        "DEFAULT" => "",
        "REFRESH" => "Y",
        "MULTIPLE" => "Y",
    ),

    "SHOW"  =>  Array(
        "NAME" => GetMessage("SEM_SHOW"),
        "TYPE" => "LIST",
        "VALUES" => $arList,
        "DEFAULT" => "",
        "REFRESH" => "Y",
        "MULTIPLE" => "Y",
    ),

    "PRODUCTS"  =>  Array(
        "NAME" => GetMessage("SEM_PRODUCTS"),
        "TYPE" => "LIST",
        "VALUES" => $arList,
        "DEFAULT" => "",
        "REFRESH" => "Y",
        "MULTIPLE" => "Y",
    ),
);
?>