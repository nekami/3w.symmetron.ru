<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>

<div class="filter filter-news">
<div class="filter-news-wrapper">
			
	<div class="filter-head">
        <span class="filter-head__button"></span>
        <? if ( !empty($arParams["FILTER_TITLE"]) ) { ?>
			<p>Фильтр <?=$arParams["FILTER_TITLE"];?></p>
		<? } ?>
	</div>
	
	<div class="filter-wrapper">
	
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?=$arResult["FORM_ACTION"]?>" method="get" class="filter_articles">	
			
			<? foreach($arResult["HIDDEN"] as $arItem) { ?>
				<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?=$arItem["CONTROL_ID"]?>" 
				value="<?echo $arItem["HTML_VALUE"]?>" />
			<? } ?>	

			<? foreach ($arResult["ITEMS"] as $key => $arItem) { ?>			
			
				<? if(empty($arItem["VALUES"]) || isset($arItem["PRICE"])) continue; ?>				
				<? if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)) continue; ?>
				
				<div class="filter-element">				
					<div class="filter-element-head">
					
                        <span class="filter-element-head__button"></span>
						
                        <p><?=$arItem["NAME"]?></p>
												
						<!-- отображение максимального количества элементов из всех значений свойства -->
							
						<div class="filter-element-head__number" data-id="<?=$arResult["GroupCount"][$arItem["ID"]]?>"
							<? if (empty($arResult["GroupCount"][$arItem["ID"]])) { ?>style="display: none;"<? } ?>>
								<?=$arResult["GroupCount"][$arItem["ID"]]?>	
						</div>
							
						<!-- удаление фильтра по свойству -->
							
						<span class="filter-element-head__close" id="<?=$arItem["CODE"]?>"
							<? if (empty($arResult["GroupCount"][$arItem["ID"]])) { ?>style="display: none;"<? } ?>></span>
							
                    </div>
					
					<input type="text" class="filter-element-search" placeholder="Поиск" value="">
					
					<div class="filter-checkbox-list" data-role="bx_filter_block">		
						
						<? foreach($arItem["VALUES"] as $val => $ar) { ?>
						
							<div class="filter-checkbox-element <? if ($ar["DISABLED"]) { ?>disabled<? } ?>" title="<?=$ar["VALUE"];?>">
								
								<input 
									type="checkbox" 
									id="<?=$ar["CONTROL_ID"]?>" 
									<?=$ar["CHECKED"] ? 'checked="checked"': '' ?> 
									onclick="smartFilter.click(this)" value="<?=$ar["HTML_VALUE"] ?>" 
									name="<?=$ar["CONTROL_NAME"] ?>" 
									<? if ($ar["DISABLED"]) { ?>disabled<? } ?> 
									data-count="<?=$ar["ELEMENT_COUNT"];?>"
								/>
								
								<label for="<?=$ar["CONTROL_ID"]?>"><?=$ar["VALUE"]?></label>
								
								<div class="bx_filter_popup_result left" <? if ( !empty($ar["CHECKED"]) && isset($ar["ELEMENT_COUNT"])) { ?> style="display: inline-block;" <? } ?>>		
									<span data-role="count_<?=$ar["CONTROL_ID"]?>"><?=$ar["ELEMENT_COUNT"]; ?></span>
									<?=$arParams["FILTER_TITLE"];?>											
									<span class="arrow"></span>										
								</div>
								
							</div>
							
						<? } ?>
						
					</div>
				</div>
			<? } ?>			
			
			<? // отображение данных по фильтру ?>
			
			<div id="modef" class="modef_show" <? if(empty($arResult["ELEMENT_COUNT"])) { ?>style="display: none;"<? } ?>>
				<a href="<?=$arResult["FILTER_URL"]?>" class="filter-show-more">
				
					<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
						
					<?echo GetMessage("CT_BCSF_ELEMENT_COUNT")?>
				</a>
			</div>			
			
			<? // сброс фильтра ?>
						
			<button class="filter-bottom" id="del_filter" name="del_filter"				
				<? if(empty($arResult["ELEMENT_COUNT"])) { ?>style="display: none;"<? } ?>>				
				<span class="filter-bottom-close"></span>					
				<p class="filter-bottom-text"><?=GetMessage("CT_BCSF_DEL_FILTER")?></p>					
			</button>
			
		</form>
		
	</div>
	</div>
</div>

<script>	
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>