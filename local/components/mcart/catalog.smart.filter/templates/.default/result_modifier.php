<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";


if (strpos($arResult["FORM_ACTION"], "/filter/clear/apply/"))
{
	$_SESSION["ELEMENT_COUNT"] = 0;
	$arResult["ELEMENT_COUNT"] = 0;	
}	
else 
{
	if ( !empty($arResult["ELEMENT_COUNT"]) )
		$_SESSION["ELEMENT_COUNT"] = $arResult["ELEMENT_COUNT"];
	else 
		$arResult["ELEMENT_COUNT"] = $_SESSION["ELEMENT_COUNT"];
}

// определить к какой группе относится единица найденного элемента
// подсчитать общее количество элементов в группе

$arResult["GroupCount"] = [];

foreach($arResult["ITEMS"] as $arItem) 
{
	foreach($arItem["VALUES"] as $val => $ar) 
	{
		if (!empty($ar["CHECKED"]) && empty($ar["DISABLED"]))
		{
			$ar["CONTROL_NAME_ALT"] = preg_replace('/[a-zA-Z_]/i', "", $ar["CONTROL_NAME_ALT"]);			
			$arResult["GroupCount"][$ar["CONTROL_NAME_ALT"]][] = $ar["ELEMENT_COUNT"];
		}
	}
}

foreach($arResult["GroupCount"] as $key => &$arItem)
{
	$arItem = array_sum($arItem);
}

$this->__component->SetResultCacheKeys(["GroupCount"]);
?>