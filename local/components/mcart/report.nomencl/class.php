<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


use Bitrix\Main\SystemException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\UI\Filter\Options;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Entity;

class CMcartPollsReport extends CBitrixComponent
{
    const PREF = 'MCART_POLLS_REPORT_';
    protected $obRequest;
    protected $arFilter = [];
    protected $iBlockID;
    private $excludeUsers = [];


    public function __construct($component = null){
        parent::__construct($component);
        Loc::loadMessages(__FILE__);
        $this->obRequest = Application::getInstance()->getContext()->getRequest();
        $this->iBlockID = 4;
    }
	public function executeComponent(){
			try{
				 $this->checkRequiredModules();

				$this->PrepareRows();
			}catch(\Exception $e){
				ShowError($e->getMessage());
			}
			$this->includeComponentTemplate();
		}


	protected function checkRequiredModules(){
			if (!Loader::includeModule('iblock')){
				throw new SystemException(Loc::getMessage("IBLOCK_MODULE_NOT_INSTALLED"));
			}
			if (!Loader::includeModule('catalog')){
				throw new SystemException(Loc::getMessage("LISTS_MODULE_NOT_INSTALLED"));
			}
		}


	private function PrepareRows() {

        $arrRows = array();
		global $DB;
		$iblock_id = 4;

		//all
		$resAll = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id),
			false, false, Array());
		$this->arResult["ALL"] = $resAll->SelectedRowsCount();
		//неактивные
		$resNoActive = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"N"),
			false, false, Array());
		$this->arResult["NO_ACTIVE"] = $resNoActive->SelectedRowsCount();

		//active
		$resActive = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y"),
			false, false, Array());
		$this->arResult["ACTIVE"] = $resActive->SelectedRowsCount();
		/*//no picture
		$resNoPicture = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "DETAIL_PICTURE"=>false),
			Array(), false, Array());
		//no picture no active
		$resNoPictureNoAc = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"N", "DETAIL_PICTURE"=>false),
			Array(), false, Array());
		*/

		//no picture active
		$resNoPictureAc = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "DETAIL_PICTURE"=>false),
			false, false, Array());
		$this->arResult["NO_PICTURE"] = $resNoPictureAc->SelectedRowsCount();

		//нет даташита
		$resNoDoc = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "PROPERTY_DOC_AND_FILES"=>false),
			false, false, Array());
		$this->arResult["NO_DOC_AND_FILES"] = $resNoDoc->SelectedRowsCount();

		//даташит больше 1 ВЫГРУЗКА
		$arResult['DOC_AND_FILES'] = 0;
		$resNoDoc = CIBlockElement::GetList(array("id"=>"asc"),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "!PROPERTY_DOC_AND_FILES"=>false),
			false, false, array("ID", "NAME", "PROPERTY_DOC_AND_FILES"));
		while ($ob = $resNoDoc->GetNext()) {

			if ($old_id == $ob["ID"]) {
				$arrTmpDoc["old_id"]++;
			} else {
				if ($arrTmpDoc["old_id"] > 1){
					$this->arResult['DOCS']++;
				}
				$old_id = $ob["ID"];
				$arrTmpDoc["old_id"] = 1;
			}
		}
		if ($arrTmpDoc["old_id"] > 1){
			$this->arResult['DOCS']++;
		}

		//no price ВЫГРУЗКА
		$resNoPrice = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "CATALOG_PRICE_1"=>false),
			false, false, Array());
		$this->arResult["NO_PRICE"] = $resNoPrice->SelectedRowsCount();

		// CATALOG_PRICE
		$this->arResult['PRICE_1'] = 0;
		$this->arResult['PRICE_2'] = 0;
		$this->arResult['PRICE_3+'] = 0;

		$resPrice = CIBlockElement::GetList(Array("id"=>"asc"),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "!CATALOG_PRICE_1"=>false),
			false, false, Array("ID", "NAME"));
		$old_id = 0;
		$arrTmp["old_id"] = 0;
		while ($ob = $resPrice->GetNext()) {

			if ($old_id == $ob["ID"]) {
				$arrTmp["old_id"]++;
			} else {
				if ($arrTmp["old_id"] == 1){
					$this->arResult['PRICE_1']++;
				} elseif ($arrTmp["old_id"] == 2) {
					$this->arResult['PRICE_2']++;
				} elseif ($arrTmp["old_id"] >= 3) {
					$this->arResult['PRICE_3+']++;
				}
				$old_id = $ob["ID"];
				$arrTmp["old_id"] = 1;
			}
		}
		if ($arrTmp["old_id"] == 1){
			$this->arResult['PRICE_1']++;
		} elseif ($arrTmp["old_id"] == 2) {
			$this->arResult['PRICE_2']++;
		} elseif ($arrTmp["old_id"] >= 3) {
			$this->arResult['PRICE_3+']++;
		}


		//no tip upak ВЫГРУЗКА
		$resNoUpak = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "PROPERTY_UPAK"=>false),
			false, false, Array());
		$this->arResult["NO_UPAK"] = $resNoUpak->SelectedRowsCount();
		/*while ($ob = $resNoUpak->GetNext()){

		}
		*/
		//no kol v upak ВЫГРУЗКА
		$resNoKolUpak = CIBlockElement::GetList(Array(),
			array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "PROPERTY_SYMPACKAGE" =>false),
			false, false, Array("ID", "NAME"));
		$this->arResult["NO_KOV_V_UPAK"] = $resNoKolUpak->SelectedRowsCount();
				/*while ($ob = $resNoKolUpak->GetNext()){
			print_r($ob);
		}
		*/

		//ALL ACTIVE ID
		while ($ob = $resActive->GetNext()) {
			$arAllActive[$ob["ID"]] = $ob["ID"];
		}
		//MOQ
		$MOQ = CIBlockElement::GetList(Array(),
            array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>$iblock_id, "ACTIVE"=>"Y", "PROPERTY_PURCHQTYLOWEST" => false));
		$this->arResult["NO_MOQ"] = $MOQ->SelectedRowsCount();

		//PRORERTIES
		$BASE_PROP = array(	'44',
							'3040',
							'3042',
							'3043',
							'3077',
							'3109',
							'3108',
							'3110',
							'3111',
							'3112',
							'3113',
							'3118',
							'3122',
							'3123',
							'3151',
							'3304',
							'3341',
							'3438',
							'3498',
							'3522',
							'3589',
'3041',
'3619',
'3620',
'3618');
		$this->arResult['PROPS'] = 0;
		$this->arResult['PROP_1_3'] = 0;
		$this->arResult['PROP_0'] = 0;
		$arProp = $DB->query("SELECT IBLOCK_PROPERTY_ID, IBLOCK_ELEMENT_ID 
								FROM b_iblock_element_property 
								GROUP BY IBLOCK_ELEMENT_ID, IBLOCK_PROPERTY_ID");
		while ($obj = $arProp->GetNext()) {
			if (!in_array($obj["IBLOCK_PROPERTY_ID"], $BASE_PROP))
				$arrAllProp[$obj["IBLOCK_ELEMENT_ID"]][] = $obj["IBLOCK_PROPERTY_ID"];
		}
		$arrTmp = array_intersect_key($arrAllProp, $arAllActive);
		foreach ($arrTmp as $id => $prop) {
			$arPropCount[$id] = count($prop);
			if ($arPropCount[$id] > 0 && $arPropCount[$id] <= 3) {
				$this->arResult['PROP_1_3']++;
			} elseif ($arPropCount[$id] > 3) {
				$this->arResult['PROPS']++;
			}
		}
		$this->arResult['PROP_0'] = $this->arResult["ACTIVE"] - $this->arResult['PROP_1_3'] - $this->arResult['PROPS'];
	}
}
/*
Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_PRODUCERID")

add $arResult['PRODUCERID']=$ob['PROPERTY_PRODUCERID_VALUE'];

$resProduceridName = CIBlockElement::GetList(Array(),
	array("CHECK_PERMISSIONS"=>"N", "IBLOCK_ID"=>11, "ACTIVE"=>"Y", "ID"=>$arResult['PRODUCERID']),
 	false,  Array("nTopCount"=>100), Array('ID', 'NAME'));
while ($ob = $resProduceridName->GetNext()){
print_r($ob);
$arResult['PRODUCERID_NAME']=$ob['NAME'];
}
