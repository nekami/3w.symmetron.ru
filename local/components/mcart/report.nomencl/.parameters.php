<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;

if(!Loader::includeModule('lists'))
	return;

$strSelectedType = $arCurrentValues["IBLOCK_TYPE_ID"];

$arTypes = [];
$rsTypes = CLists::GetIBlockTypes();
while($ar = $rsTypes->Fetch())
{
	$arTypes[$ar["IBLOCK_TYPE_ID"]] = "[".$ar["IBLOCK_TYPE_ID"]."] ".$ar["NAME"];
	if(!$strSelectedType)
		$strSelectedType = $ar["IBLOCK_TYPE_ID"];
}

$arIBlocks = [];
$rsIBlocks = CIBlock::GetList(["sort" => "asc"], ["TYPE" => $strSelectedType, "ACTIVE"=>"Y"]);
while($ar = $rsIBlocks->Fetch())
{
	$arIBlocks[$ar["ID"]] = "[".$ar["ID"]."] ".$ar["NAME"];
}

$arComponentParameters = [
	"GROUPS" => [
	],
	"PARAMETERS" => [
		"IBLOCK_TYPE_ID" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("REPORT_IBLOCK_TYPE_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arTypes,
			"DEFAULT" => "lists",
		],
		"IBLOCK_ID" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("REPORT_IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
		],
		"BASE_PROP_ID" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("REPORT_BASE_PROP_ID"),
			"TYPE" => "TEXT",
			"DEFAULT"=>""
		],

//		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	],
];

