<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<table>
	<tr>
		<th>Отчет состояния номенклатуры</th>
    	<th>Количество</th>
		<th></th>
	</tr>
	<tr>
		<td>Общее количество номенклатуры</td>
		<td><?echo $arResult["ALL"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Активное количество номенклатуры</td>
		<td><?echo $arResult["ACTIVE"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Деактивированное количество номенклатуры</td>
		<td><?echo $arResult["NO_ACTIVE"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура без загруженных характеристик</td>
		<td><?echo $arResult["PROP_0"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура с загруженными характеристиками (от 1 до 3) </td>
		<td><?echo $arResult["PROP_1_3"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура с загруженными характеристиками (более 3х)</td>
		<td><?echo $arResult["PROPS"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура у которой нет фотографии</td>
		<td><?echo $arResult["NO_PICTURE"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура у которой нет даташита</td>
		<td><?echo $arResult["NO_DOC_AND_FILES"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура у которой более 1 даташита</td>
		<td><?echo $arResult["DOCS"];?></td>
		<td></td>
	</tr>

	<tr>
		<td>Номенклатура, которая не имеет цен</td>
		<td><?echo $arResult["NO_PRICE"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура, которая имеет 1 цену</td>
		<td><?echo $arResult["PRICE_1"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура, которая имеет 2 цены</td>
		<td><?echo $arResult["PRICE_2"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура, которая имеет 3 и более цен</td>
		<td><?echo $arResult["PRICE_3+"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура, которая не имеет типа упаковки</td>
		<td><?echo $arResult["NO_UPAK"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура, которая не имеет количества в упаковке</td>
		<td><?echo $arResult["NO_KOV_V_UPAK"];?></td>
		<td></td>
	</tr>
	<tr>
		<td>Номенклатура, которая не имеет количества в поле MOQ</td>
		<td><?echo  $arResult["NO_MOQ"];?></td>
		<td></td>
	</tr>

</table>