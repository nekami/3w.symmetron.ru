<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MCART_TASKREPORTRK_LIST_NAME"),
	"DESCRIPTION" => GetMessage("MCART_TASKREPORTRK_LIST_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 30,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "mcart",
        "NAME" => GetMessage("MCART")
	),
);
