<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @global CMain $APPLICATION */

$arParams["FILTER_NAME"] = trim($arParams["FILTER_NAME"]);
if ($arParams["FILTER_NAME"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
    $arParams["FILTER_NAME"] = "arrFilter";

if (!empty($filterName) && !empty($GLOBALS[$filterName]) && is_array($GLOBALS[$filterName]))
    $arParams["FILTER"] = $GLOBALS[$filterName];


$arDefaultUrlTemplates404 = array(
	"sections" => "#SECTION_CODE_PATH#/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"SECTION_ID",
	"SECTION_CODE",
);

if($arParams["SEF_MODE"] == "Y")
{
	$arVariables = array();

	$engine = new CComponentEngine($this);
	if (\Bitrix\Main\Loader::includeModule('iblock'))
	{
		$engine->addGreedyPart("#SECTION_CODE_PATH#");
		$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
	}
	$arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

	$componentPage = $engine->guessComponentPath(
		$arParams["SEF_FOLDER"],
		$arUrlTemplates,
		$arVariables
	);

	$componentPage = "sections";
	CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
	$arResult = array(
		"FOLDER" => $arParams["SEF_FOLDER"],
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);
}
else
{
	$arVariables = array();

	$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
	CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = "sections";

	$currentPage = htmlspecialcharsbx($APPLICATION->GetCurPage())."?";
	$arResult = array(
		"FOLDER" => "",
		"URL_TEMPLATES" => array(
			"sections" => $currentPage.$arVariableAliases["SECTION_ID"]."=#SECTION_ID#",
		),
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);
}


if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
//$arParams["SECTION_ID"] = intval($arResult["VARIABLES"]["SECTION_ID"]);
//$arParams["SECTION_CODE"] = trim($arResult["VARIABLES"]["SECTION_CODE"]);

$arParams["SEF_FOLDER"] = trim($arParams["SEF_FOLDER"]);

$arResult["SECTIONS"] = array();
$arResult["CUR_SECTIONS"] = array();

$arParams["SECTION_ID"] = intval($arVariables["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arVariables["SECTION_CODE"]);

if($this->startResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()))) {
    if ($APPLICATION->GetCurPage(false) != $arParams["SEF_FOLDER"] && empty($arVariables["SECTION_ID"])) {
        $this->abortResultCache();
        \Bitrix\Iblock\Component\Tools::process404(
            trim($arParams["MESSAGE_404"]) ?: ""//"Oops... Page not found!"
            , true
            , $arParams["SET_STATUS_404"] === "Y"
            , $arParams["SHOW_404"] === "Y"
            , $arParams["FILE_404"]
        );
        return;
    }

    if (!\Bitrix\Main\Loader::includeModule("iblock")) {
        $this->abortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $existIblock = \Bitrix\Iblock\IblockSiteTable::getList(array(
        'select' => array('IBLOCK_ID'),
        'filter' => array('=IBLOCK_ID' => $arParams['IBLOCK_ID'], '=SITE_ID' => SITE_ID, '=IBLOCK.ACTIVE' => 'Y')
    ))->fetch();
    if (empty($existIblock)) {
        $this->abortResultCache();
        return;
    }

    $arResult["GET_FILTER_STRING"] = $this->GetGetParameters();

    $arResult["SECTIONS"] = $this->LoadElements();

    $arResult["SECTION_ID"] = $arParams["SECTION_ID"];
    if ($arParams["SECTION_ID"] <= 0 &&
        '' == $arParams["SECTION_CODE"]) {
        $arResult["SECTION_ID"] = false;
        foreach ($arResult["SECTIONS"] as $section)
            if ($section["DEPTH_LEVEL"] == 1) {
                $arParams["SECTION_ID"] = $section["ID"];
            }
    }
    $arResult["SECTION"] = $this->LoadCurSection();
    $arResult["CUR_SECTIONS"] = $this->LoadCurSections();
    $this->GetBreadcrumbs();


    $this->setResultCacheKeys(array(
        //"SECTION",
        "BREADCRUMBS",
    ));
    $this->endResultCache();
}

$this->IncludeComponentTemplate();
