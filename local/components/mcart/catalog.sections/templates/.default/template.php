<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$this->addExternalCSS($templateFolder."/breadcrumb.css");

echo "<div id='breadcrumb_section' style='display: none' >";
$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "for_sections",
    Array('START_FROM'=>1)
);
echo "</div>";

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<section class="catalog">
    <div class="wrapper">
        <h2><?=Loc::GetMessage("MCART_SYMMETRON_CATALOG_TITLE")?></h2>
        <div class="catalog-breadcrumb"></div>
        <div class="catalog-wrapper">
            <?
            $to_load = [0];
            while (count($to_load) > 0)
            {
                $id = array_shift($to_load);
                $section = $arResult["SECTIONS"][$id];
                $this->__component->GetLevel($id, $section["DEPTH_LEVEL"] + 1);
                foreach ($section["CHILDS"] as $child_id)
                    if (!empty($arResult["SECTIONS"][$child_id]["CHILDS"]))
                        $to_load[] = $child_id;
            }
            ?>
        </div>
    </div>
</section>
<script>
    BX.ready(function() {
        BX.message({
            MCART_SYMMETRON_CATALOG_TITLE: '<?=Loc::getMessage("MCART_SYMMETRON_CATALOG_TITLE")?>',
            MCART_SYMMETRON_CATALOG_URL: '<?=$arParams["SECTION_FOLDER"]?>',
            MCART_SYMMETRON_MAIN_TITLE:'<?=Loc::getMessage("MCART_SYMMETRON_MAIN_TITLE")?>'
        });
        BX.Mcart.CatalogSections.init();
    });
</script>