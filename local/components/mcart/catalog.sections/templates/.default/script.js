BX.namespace("BX.Mcart.CatalogSections");

BX.Mcart.CatalogSections.init = function ()
{
    var _this = this;
	function charVisible () {
		 $('.catalog__item').each(function(){
            if ($(this).children().children('a').innerWidth() >= 360) {
                $(this).addClass('chars-visible')
            }
        })
	}
    charVisible ();
    $('.catalog-wrapper').on("click", ".catalog__item", function (e) {
        _this.itemClick(e, $(this), true);
		setTimeout(charVisible (), 1000);
    });
    $('.catalog-wrapper').on("click", ".catalog-item__holder > a", function (e) {
        if (!$(this).parent().hasClass("endpoint"))
            e.preventDefault();
        else
            e.stopPropagation();
    });
    $('.catalog-breadcrumb').on("click", ".bx-breadcrumb-item a", function (e) {
        var element_id = $(this).parent().attr("element_id");
        if (element_id === undefined)
            return;
        e.preventDefault();
        var item = $(".catalog__item[element_id="+element_id+"]")
        _this.itemClick(e, $(item[0]), $(this).parent().attr('id') != undefined)

        if ($(this).parent().attr('id') == undefined) {
            _this.rebootUrl();
        }
    });

    this.reloadBreadrcrumb();
    $(".catalog-lvl.open").find(".sectionName").each(function () {
        _this.sectionItemHoversOn($(this));
    })
    this.scroll();


    window.onpopstate = function(e) {
        //location.reload()
        var selectLis = $(".catalog__item.chosen")
        if(selectLis.length >= 2) {
            var backLi = selectLis[selectLis.length - 2]
            backLi.click();
        }
    }

};

BX.Mcart.CatalogSections.sectionItemHoversOff = function (element) {
    $(element).removeClass("tooltipstered")
}

BX.Mcart.CatalogSections.sectionItemHoversOn = function (element) {
    if ($(element).hasClass('tooltipstered')) {
        return false
    } else {
        var catalogItemWidth = $(element).innerWidth()
        $(element).tooltipster({
            animation: false,
            animationDuration: 0,
            delay: 0,
            distance: 0,
            minIntersection: 0,
            functionPosition: function(instance, helper, position){
                position.coord.top += 1;
                position.coord.left += -catalogItemWidth + 5;
                return position;
            },
            functionReady: function(instance, helper){
                if ($(instance.elementOrigin()).parent().parent().hasClass('chosen')) {
                    $('.tooltipster-box').addClass('lvl-prev');
                }

                if ($(instance.elementOrigin()).parent().parent().parent().parent().hasClass('active-catalog-lvl') && $(instance.elementOrigin()).parent().parent().hasClass('chosen'))  {
                    $('.tooltipster-box').addClass('lvl-choosen');
                }
            },
            side: ['right'],
            arrow: false,
        })
    }
}

BX.Mcart.CatalogSections.scroll = function ()
{
    if ($(".active-catalog-lvl.election-catalog-lvl").length > 0) {
        $('.catalog-wrapper')[0].scrollLeft = 0;
    }
    else
        $('.catalog-wrapper').animate({ scrollLeft: $('.catalog-wrapper')[0].scrollWidth + $('.catalog__item').width() }, 800);
};

BX.Mcart.CatalogSections.closeLevels = function (from)
{
    var _this = this;
    var levels = $('.catalog-lvl.open');
    for (var i = 0; i < levels.length; i++) {
        var lvl = $(levels[i]);
        var level = lvl.attr("level");
        if (level >= from) {
            lvl.find(".catalog__item.chosen").removeClass('chosen');
            lvl.find(".sectionName").each(function () {
                _this.sectionItemHoversOff(this);
            });
        }
        if (level > from) {
            lvl.removeClass('open');
        }
    }
};
BX.Mcart.CatalogSections.itemClose = function (item)
{
    var catalog_lvl = item.parent().parent();
    var $level = catalog_lvl.attr("level");
    if ($level === undefined)
        $level = 1;
    $level = parseInt($level);

    if (catalog_lvl.hasClass("open")) {
        if (catalog_lvl.hasClass("active-catalog-lvl"))
        {
            var parent_id = catalog_lvl.attr("parent_id");

            if (parent_id > 0) {
                // close sub categories
                var element_id = item.attr("element_id");
                $(".catalog-lvl[parent_id="+element_id+"]")
                    .removeClass("open")
                    .removeClass("election-catalog-lvl");

                // deactivate current item
                catalog_lvl.removeClass('active-catalog-lvl');
                catalog_lvl.addClass('election-catalog-lvl');
                catalog_lvl.find(".catalog__item.chosen").removeClass('chosen');

                // activate previous lvl
                $(".catalog__item[element_id="+parent_id+"]")
                    .parent().parent().addClass("active-catalog-lvl");
            }
        } else {
            this.closeLevels($level + 1);
            // remove classes from all other
            $(".catalog__item.active-catalog-lvl").removeClass("active-catalog-lvl");
            $(".catalog__item.election-catalog-lvl").removeClass("election-catalog-lvl");

            // activate current level
            catalog_lvl.addClass('active-catalog-lvl');

            // mark sub level
            var element_id = item.attr("element_id");
            $(".catalog-lvl[parent_id="+element_id+"]").addClass("election-catalog-lvl");
        }
    }
};
BX.Mcart.CatalogSections.itemChoose = function (item, is)
{
    var _this = this;

    if (item.children("div").hasClass("endpoint"))
        return;

    var catalog_lvl = item.parent().parent();

    if (catalog_lvl.find(".catalog__item.chosen").length > 0) {
        var $level = catalog_lvl.attr("level");
        if ($level === undefined)
            $level = 1;
        $level = parseInt($level);
        this.closeLevels($level);
    }

    $(".catalog-lvl.election-catalog-lvl").removeClass("election-catalog-lvl");
    $(".catalog-lvl.active-catalog-lvl").removeClass("active-catalog-lvl");
    catalog_lvl.addClass('active-catalog-lvl');
    item.addClass("chosen");

    if (is) {
        var element_id = item.attr("element_id");
        var element = $(".catalog-lvl[parent_id=" + element_id + "]")
        element.addClass("open").addClass("election-catalog-lvl");
        element.find(".sectionName").each(function () {
            _this.sectionItemHoversOn($(this));
        })
    } else catalog_lvl.addClass('election-catalog-lvl');

    catalog_lvl.find(".sectionName").each(function () {
        _this.sectionItemHoversOn($(this));
    })
};

BX.Mcart.CatalogSections.rebootUrl = function () {
    var get = window.location.search;
    //var params = get.replace("?", "").split("&");

    var url = "/";
    history.pushState(null, null, url+get);
};
BX.Mcart.CatalogSections.reloadUrl = function () {
    var get = window.location.search;
    //var params = get.replace("?", "").split("&");

    var url = $(".catalog-lvl.open.active-catalog-lvl .chosen a").attr("href");
    history.pushState(null, null, url+get);
};

BX.Mcart.CatalogSections.reloadBreadrcrumb = function () {
    var breadcrumbs = document.getElementById("breadcrumb_section")
    var breadcrumbsLiFiltred = []
    var countLi = 0
    if(breadcrumbs){
        breadcrumbsLi = breadcrumbs.childNodes
        for(var i = 0; i < breadcrumbsLi.length; i++){
            if(breadcrumbsLi[i].nodeName == "LI"){
                //breadcrumbsLiFiltred.push(breadcrumbsLi[i]);
                countLi++;
            }
        }

    }
    var breadcrumb = [];

    var items = $(".catalog-lvl.open > ul > li.chosen").find("a");
    for (var i = 0; i < items.length; i++) {
        var item = $(items[i]);
        breadcrumb.push([item.text().trim(), item.attr("href"), item.parent().parent().attr("element_id")]);
    }

    var node = $('.catalog-breadcrumb');
    var html = "";
    html += '<ul class="breadcrumbs-items bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';
    if (countLi == 0){
         // Main icon
        html +=
            '<li element_id="' + breadcrumb[0][2] + '" class="bx-breadcrumb-item ' + (breadcrumb.length === 1 ? 'parent-crumb' : '') + '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' +
                '<a href="/" title="' + BX.message('MCART_SYMMETRON_MAIN_TITLE') + '" itemprop="url">' +
                    '<svg' +
                        ' xmlns:dc="http://purl.org/dc/elements/1.1/"' +
                        ' xmlns:cc="http://creativecommons.org/ns#"' +
                        ' xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"' +
                        ' xmlns:svg="http://www.w3.org/2000/svg"' +
                        ' xmlns="http://www.w3.org/2000/svg"' +
                        ' viewBox="0 0 86.458 69.124931"' +
                        ' xml:space="preserve"' +
                        ' id="svg2"' +
                        ' version="1.1">' +
                    '<metadata id="metadata8"><rdf:RDF><cc:Work rdf:about="">' +
                        '<dc:format>image/svg+xml</dc:format>' +
                        '<dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />' +
                    '</cc:Work></rdf:RDF></metadata>' +
                    '<defs id="defs6"><clipPath id="clipPath18" clipPathUnits="userSpaceOnUse">' +
                        '<path id="path16" d="M 0,51.844 H 64.843 V 0 H 0 Z" /></clipPath></defs>' +
                    '<g transform="matrix(1.3333333,0,0,-1.3333333,0,69.124933)" id="g10">' +
                    '<g id="g12">' +
                        '<g clip-path="url(#clipPath18)" id="g14">' +
                            '<g transform="translate(48.2774,22.1616)" id="g20">' +
                                '<path id="path22"' +
                                    ' style="fill:none;stroke:#008aff;stroke-width:5.31099987;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"' +
                                    ' d="M 0,0 V -19.506 H -31.711 V 0" />' +
                            '</g><g transform="translate(3.0636,22.7285)" id="g24">' +
                                '<path id="path26"' +
                                    ' style="fill:none;stroke:#008aff;stroke-width:6.12699986;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"' +
                                    ' d="M 0,0 29.358,26.052 58.716,0" />' +
                            '</g>' +
                        '</g>' +
                    '</g></g></svg>' +
                    '<meta itemprop="position" content="1">' +
                '</a>' +
            '</li>';
        html +=
            '<li class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">'+
                '<a href="' + BX.message("MCART_SYMMETRON_CATALOG_URL") + '" title="' + BX.message("MCART_SYMMETRON_CATALOG_TITLE") + '" itemprop="url">'+
                '<span itemprop="name">' + BX.message("MCART_SYMMETRON_CATALOG_TITLE") + '</span></a>'+
            '<meta itemprop="position" content="2" /></li>';
    } else {
        html += breadcrumbs.innerHTML
    }

    // Crumbs
    var offset = countLi ? countLi : 2
    for (var i = 0; i < breadcrumb.length; i++)
    {
        var addit = ''
        if (i + offset === breadcrumb.length)
            addit = 'parent-crumb';
        html +=
            '<li element_id="' + breadcrumb[i][2] + '" class="bx-breadcrumb-item '+addit+'" id="bx_breadcrumb_'+i+'" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">'+
                '<a href="' + breadcrumb[i][1] + '" onclick="" title="' + breadcrumb[i][0] + '" itemprop="url">'+
                '<span itemprop="name">' + breadcrumb[i][0] + '</span></a>'+
            '<meta itemprop="position" content="' + (i + 3) + '" /></li>'
    }
    html += "</ul>";

    node.html(html);
};

BX.Mcart.CatalogSections.itemClick = function (e, item, is)
{
    e.stopPropagation();
    if (item.hasClass("chosen") && item.parent().parent().attr("level") != 1) {
        this.itemClose(item);
    } else {
        this.itemChoose(item, is);
    }
    this.reloadBreadrcrumb();
    if (is) this.reloadUrl();
    this.scroll();
};