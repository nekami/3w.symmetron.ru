<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
class McartSymmetronCatalogSections extends CBitrixComponent
{
    public function GetLevel($parent_id, $level)
    {
        $sections_id = array_reverse($this->arResult["SECTIONS"][$parent_id]["CHILDS"]);
        $class = "catalog-lvl-".$level." catalog-lvl";
        if ($parent_id == 0 || isset($this->arResult["CUR_SECTIONS"][$parent_id]))
            $class .= " open";
        if (in_array($this->arResult["SECTION"]["ID"], $sections_id)) {
            $class .= " active-catalog-lvl";
            if ($this->arResult["SECTION_ID"] === false)
                $class .= " election-catalog-lvl";
        } elseif ($this->arResult["SECTION_ID"] > 0 && $this->arResult["SECTION"]["ID"] == $parent_id)
            $class .= " election-catalog-lvl";
        ?>
        <div class="<?=$class?>" level="<?=$level?>" parent_id="<?=$parent_id?>">
            <ul>
            <?foreach ($sections_id as $id):?>
                <?if (empty($section = $this->arResult["SECTIONS"][$id]))
                    continue;?>
                <?
                $class = "catalog__item";
                if (isset($this->arResult["CUR_SECTIONS"][$id])) {
                    $class .= " chosen";
                }
                ?>
                <li class="<?=$class?>" element_id="<?=$section["ID"]?>">
                    <div class="catalog-item__holder <?=$section["IS_END"] ? "endpoint" : ""?>">
						<a class="sectionName" href="<?=!empty($section["URL"]) ? $section["URL"] : ""?>" title="<?=$section["NAME"]?>" onclick="">
                            <?=$section["NAME"]?>
                        </a>
                    </div>
                </li>
            <?endforeach;?>
            </ul>
        </div>
        <?
    }

    private function GetSectionsElementCount()
    {
        $arFilter = array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
        );
        if (!empty($this->arParams["FILTER"]) && is_array($this->arParams["FILTER"])) {
            foreach ($this->arParams["FILTER"] as $key => $filter) {
                $arFilter["PROPERTY_".$key] = $filter;
            }
        }
        $result = \CIBlockElement::GetList(array(), $arFilter, array("IBLOCK_SECTION_ID"));
        while ($row = $result->fetch())
            $arResult[$row["IBLOCK_SECTION_ID"]] = $row["CNT"];
        return $arResult;
    }
    public function LoadElements()
    {
        $sectionsElCount = $this->GetSectionsElementCount();

        $folderCur = $this->arParams["SEF_FOLDER"];
        $folderEnd = $this->arParams["SECTION_FOLDER"];

        $arFilter = array(
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
        );

        $arSelect = array(
            "ID",
            "NAME",
            "LEFT_MARGIN",
            "RIGHT_MARGIN",
            "DEPTH_LEVEL",
            "SECTION_PAGE_URL",
            "IBLOCK_SECTION_ID",
        );

        $sections = array(0 => ["DEPTH_LEVEL" => 0]);
        $rsSections = \CIBlockSection::GetList(array("LEFT_MARGIN" => "DESC"), $arFilter, false, $arSelect);
        while($arSection = $rsSections->GetNext()) {
            $id = $arSection["ID"];
            if (empty($sectionsElCount[$id]) && empty($sections[$id]))
                continue;

            $sections[$id]["ID"]            = $id;
            $sections[$id]["NAME"]          = $arSection["NAME"];
            $sections[$id]["PARENT_ID"]     = $arSection["IBLOCK_SECTION_ID"];
            $sections[$id]["DEPTH_LEVEL"]   = $arSection["DEPTH_LEVEL"];
            $sections[$id]["URL"]           = $arSection["SECTION_PAGE_URL"];

            if (!isset($sections[$id]["CHILDS"]))
                $sections[$id]["CHILDS"] = [];
            if ($arSection["IBLOCK_SECTION_ID"] > 0)
                $sections[$arSection["IBLOCK_SECTION_ID"]]["CHILDS"][] = $id;
            else $sections[0]["CHILDS"][] = $id;
        }
        foreach ($sections as &$section) {
            if (empty($section["CHILDS"])) {
                $section["IS_END"] = true;
                $section["URL"] = $section["URL"].$this->arResult["GET_FILTER_STRING"];
            } else {
                $section["IS_END"] = false;
                $section["URL"] = str_replace($folderEnd, $folderCur, $section["URL"]);
            }
        }
        return $sections;
    }

    public function LoadCurSection()
    {
        $arFilter = array(
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
        );

        $arSelect = array(
            "ID",
            "LEFT_MARGIN",
            "RIGHT_MARGIN",
        );

        $section = false;
        if($this->arParams["SECTION_ID"] > 0)
        {
            $arFilter["ID"] = $this->arParams["SECTION_ID"];
            $rsSections = \CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
            $section = $rsSections->fetch();
        }
        elseif('' != $this->arParams["SECTION_CODE"])
        {
            $arFilter["=CODE"] = $this->arParams["SECTION_CODE"];
            $rsSections = \CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
            $section = $rsSections->fetch();
        }

        return $section;
    }
    public function LoadCurSections()
    {
        $arFilter = array(
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
        );

        $section = $this->arResult["SECTION"];
        $sections = [];
        if (is_array($section))
        {
            $arFilter["<=LEFT_BORDER"] = $section["LEFT_MARGIN"];
            $arFilter[">=RIGHT_BORDER"] = $section["RIGHT_MARGIN"];
            $result = \CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), $arFilter, false, array("ID"));
            while ($sec = $result->fetch())
                $sections[$sec["ID"]] = $sec;
        }
        return $sections;
    }

    public function GetBreadcrumbs()
    {
        $this->arResult["BREADCRUMBS"] = [];
        $breadcrumbs = [];
        foreach ($this->arResult["CUR_SECTIONS"] as $section)
        {
            $cur = $this->arResult["SECTIONS"][$section["ID"]];
            $breadcrumbs[] = [$cur["NAME"], $cur["URL"]];
        }
        $this->arResult["BREADCRUMBS"] = $breadcrumbs;
    }

    public function GetGetParameters()
    {
        if (empty($this->arParams["FILTER"]))
            return "";
        $get = [];
        $filterName = $this->arParams["FILTER_NAME"];

        foreach ($this->arParams["FILTER"] as $key => $filter)
        {
            $code = is_integer($key) ? "ID" : "CODE";
            $arFilter = array(
                $code => $key,
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"]
            );
            $result = \Bitrix\Iblock\PropertyTable::getList(array("filter" => $arFilter, "select" => array("ID")));
            if ($prop = $result->fetch())
            {
                $name = $filterName."_".$prop["ID"]."_";
                if (!is_array($filter))
                    $filter = [$filter];
                foreach ($filter as $value)
                    $get[] = $name.abs(crc32($value))."=Y";
            } else {
                unset($this->arParams["FILTER"][$key]);
            }
        }
        if (empty($get))
            return "";
        return "?".implode("&", $get)."&set_filter=";
    }
}