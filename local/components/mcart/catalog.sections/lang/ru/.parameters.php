<?
$MESS["CP_BC_VARIABLE_ALIASES_SECTION_ID"] = "Идентификатор раздела";
$MESS["CP_BC_VARIABLE_ALIASES_SECTION_CODE"] = "Символьный код раздела";
$MESS["CP_BC_VARIABLE_ALIASES_SECTION_CODE_PATH"] = "Путь из символьных кодов раздела";
$MESS["IBLOCK_SECTION_FOLDER"] = "Путь к каталогу";

$MESS["IBLOCK_CACHE_FILTER"] = "Кешировать при установленном фильтре";
$MESS["CP_BC_CACHE_GROUPS"] = "Учитывать права доступа";

$MESS["SECTIONS_TOP_PAGE"] = "Список разделов";
$MESS["SECTION_PAGE"] = "Раздел";
$MESS["T_IBLOCK_FILTER"] = "Фильтр";
$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_IBLOCK"] = "Инфоблок";
