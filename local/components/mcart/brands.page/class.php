<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
* @global CUser $USER
* @global CMain $APPLICATION
* @global CIntranetToolbar $INTRANET_TOOLBAR
*/

class CBrandsPage extends CBitrixComponent
{
    public static function buildPrefix($i)
    {
        return "BLOCK_TEMPLATE_".$i."G";
    }

    public static function getParametersOptions($i, $values, $file, $hidden = false)
    {
        if (!file_exists($file))
            return [];
        $prefix = static::buildPrefix($i);

        $arCurrentValues = [];
        $len = strlen($prefix);
        foreach ($values as $code => $value) {
            if (substr($code, 0, $len) == $prefix && !empty(substr($code, $len)))
                $arCurrentValues[substr($code, $len)] = $value;
        }
        $arCurrentValues = array_merge($values, $arCurrentValues);
        $arTemplateParameters = [];
        require $file;
        $result = [];
        foreach ($arTemplateParameters as $code => $parameter) {
            if ($hidden && $parameter["TYPE"] == "HIDDEN") {
                $result[$code] = $parameter["VALUE"];
            } elseif ($parameter["TYPE"] !== "HIDDEN") {
                $parameter["PARENT"] = $prefix;
                $result[$prefix.$code] = $parameter;
            }
        }
        return $result;
    }

    public static function getParameters($i, $values, $file = "")
    {
        $prefix = static::buildPrefix($i);

        $arCurrentValues = [];
        $len = strlen($prefix);
        foreach ($values as $code => $value) {
            if (substr($code, 0, $len) == $prefix && !empty(substr($code, $len)))
                $arCurrentValues[substr($code, $len)] = $value;
        }

        return array_merge($arCurrentValues, static::getParametersOptions($i, $values, $file, true));
    }
}
