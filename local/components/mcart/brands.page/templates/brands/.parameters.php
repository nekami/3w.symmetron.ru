<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = [];

$templates = [];
$path = __DIR__."/bitrix/news.detail/";
$files = scandir($path);
foreach ($files as $element) {
    if ($element == "." || $element == "..")
        continue;
    if (is_dir($path.$element)) {
        $templates[$element] = $element;
    }
}

if (isset($arCurrentValues['BLOCKS_COUNT']) && $arCurrentValues['BLOCKS_COUNT'] > 0) {
    CBitrixComponent::includeComponentClass($componentName);

    for ($i = 1; $i < $arCurrentValues['BLOCKS_COUNT'] + 1; ++$i) {
        $code = CBrandsPage::buildPrefix($i);
        $arTemplateParameters[$code] = array(
            "PARENT" => "DETAIL_SETTINGS",
            "NAME" => GetMessage("BRANDS_DETAIL_BLOCKS_TEMPLATE").$i,
            "TYPE" => "LIST",
            "VALUES" => array_merge(["-" => "---"], $templates),
            "REFRESH" => "Y",
        );
        if (isset($arCurrentValues[$code]) && in_array($arCurrentValues[$code], $templates))
        {
            $file = $path.$arCurrentValues[$code]."/.parameters.php";
            $arTemplateParameters = array_merge(
                $arTemplateParameters,
                CBrandsPage::getParametersOptions($i, $arCurrentValues, $file)
            );
        }
    }
}
