<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$arSectionIDs = array();
$arElems = array();
foreach ($arResult["ITEMS"] as $arItem){
    if(!empty($arItem["PROPERTIES"]["SVG_LOGO"]["VALUE"])) {
        $arItem["PROPERTIES"]["SVG_LOGO"] = \CFile::GetFileArray($arItem["PROPERTIES"]["SVG_LOGO"]["VALUE"]);
    }

    if(!empty($arItem["IBLOCK_SECTION_ID"])) {
        //$arSectionIDs[$arItem["IBLOCK_SECTION_ID"]] = $arItem["IBLOCK_SECTION_ID"];
        $arElems[$arItem["ID"]] = $arItem;
    }
}


$arElemSortSectionEnd = array();
$arFilter = array('IBLOCK_ID' => $arParams["IBLOCK_ID"]);
$rsSections = CIBlockSection::GetList(array("SORT"=>"ASC"), $arFilter, false, array("ID", "NAME", "CODE"));
while ($arSection = $rsSections->Fetch()) {
    $arSectionIDs[$arSection["ID"]] = $arSection["ID"];
    $arElemSortSectionEnd[$arSection["ID"]]["NAME"] = $arSection["NAME"];
    $arElemSortSectionEnd[$arSection["ID"]]["CODE"] = $arSection["CODE"];
}


$r = Bitrix\Iblock\SectionElementTable::getList(array("filter"=>array("IBLOCK_SECTION_ID" => $arSectionIDs)));

$arLinkInSections = array();
while($arr = $r->Fetch()){
    if (isset($arElems[$arr["IBLOCK_ELEMENT_ID"]]))
        $arLinkInSections[$arr["IBLOCK_SECTION_ID"]][] = $arElems[$arr["IBLOCK_ELEMENT_ID"]];
}

foreach ($arElemSortSectionEnd as $key => $sec){

    usort($arLinkInSections[$key], function($a, $b){
        return ($a['SORT'] - $b['SORT']);
    });

    $arElemSortSectionEnd[$key]["ELEM"] = $arLinkInSections[$key];
}

$arResult["ITEMS"] = $arElemSortSectionEnd;
