<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<?$actualLink = "https://" . $_SERVER["SERVER_NAME"] . $APPLICATION->GetCurPage(false);?>

<?$this->SetViewTarget('link_canonical');?>
    <link rel="canonical" href="<?=$actualLink?>"/>
<?$this->EndViewTarget();?>

<section class="brands">
    <div class="wrapper">
        <h1><?= GetMessage("MANUFACT_NAME_SECTION"); ?></h1>
        <div class="brands-list">
            <div class="brands-list__tabs">
                <? $count = 0; ?>
                <? foreach ($arResult["ITEMS"] as $arSec): ?>
                    <?
                    $active = "";
                    if(!empty($_GET["key"])){
                        if($arSec["CODE"] == $_GET["key"])
                            $active = " active";
                    } else {
                        if($count == 0)
                            $active = " active";
                    }
                    ?>
                    <a
                        href="<?if($arSec["CODE"] != 80){
                            echo $APPLICATION->GetCurPage(false) . "?key=" . $arSec["CODE"];
                        } else {
                            echo $APPLICATION->GetCurPage(false);
                        }?>"
                        id="<?=$arSec["CODE"];?>" data-tab="tab-<?= $count; ?>"
                       class="brands-list__tab<?= $active ?>"><?= $arSec["NAME"]; ?></a>
                    <? $count++; ?>
                <? endforeach; ?>
            </div>


            <div class="brands-slider">
                <? $count = 0; ?>
                <? foreach ($arResult["ITEMS"] as $arSec): ?>
                    <?
                    $active = "";
                    if(!empty($_GET["key"])){
                        if($arSec["CODE"] == $_GET["key"])
                            $active = " active";
                    } else {
                        if($count == 0)
                            $active = " active";
                    }
                    ?>
                    <div id="tab-<?= $count ?>" class="brands-list__items<?= $active ?>">
                        <? foreach ($arSec["ELEM"] as $arItem): ?>
                            <?
                            //$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            //$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <?
                            $w = 150;
                            $h = 100;
                            if (isset($arItem["PROPERTIES"]["W_LIST"])) {
                                $w = is_numeric($arItem["PROPERTIES"]["W_LIST"]["VALUE"])
                                && $arItem["PROPERTIES"]["W_LIST"]["VALUE"] > 1
                                    ? $arItem["PROPERTIES"]["W_LIST"]["VALUE"]
                                    : $arItem["PROPERTIES"]["W_LIST"]["DEFAULT_VALUE"];
                            }
                            if (isset($arItem["PROPERTIES"]["H_LIST"])) {
                                $h = is_numeric($arItem["PROPERTIES"]["H_LIST"]["VALUE"])
                                && $arItem["PROPERTIES"]["H_LIST"]["VALUE"] > 1
                                    ? $arItem["PROPERTIES"]["H_LIST"]["VALUE"]
                                    : $arItem["PROPERTIES"]["H_LIST"]["DEFAULT_VALUE"];
			    }                  

                            $img = $templateFolder."/images/brand.jpg";
                            $title = "";
                            if (!empty($arItem["PROPERTIES"]["SVG_LOGO"]["SRC"])) {
                                $img = $arItem["PROPERTIES"]["SVG_LOGO"]["SRC"];
			    }
			    elseif (!empty($arItem["PREVIEW_PICTURE"]["SRC"])){
				                $img = $arItem["PREVIEW_PICTURE"]["SRC"];
                            } else {
                                $title = $arItem["NAME"];
                            } ?>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="brands-list__item" title="<?=$arItem["NAME"]?>">
                                <img src="<?= $img ?>" style="max-width: <?= $w ?>px; height:  <?= $h ?>px"/>
                            </a>


                        <? endforeach; ?>
                    </div>
                    <? $count++; ?>
                <? endforeach; ?>

            </div>
        </div>
    </div>
</section>