$(document).ready(function(){

    $('.brands-list__tabs a').click(function(e){
        e.preventDefault()
        e.stopPropagation()
        var tab_id = $(this).attr('data-tab');
        var url = $(this).attr('href');
        window.history.pushState(false, false, url);
        $('.brands-list__tabs a').removeClass('active');
        $('.brands-list__items').removeClass('active');

        $(this).addClass('active');
        $("#"+tab_id).addClass('active');
    })

	$('.brands-list__item').tooltipster({
		side: 'bottom',
		arrow: false,
		distance: 0,
		minIntersection: 0,
 		delay: [300, 0],
		viewportAware: false
		/*trigger: 'click'*/
	});

})