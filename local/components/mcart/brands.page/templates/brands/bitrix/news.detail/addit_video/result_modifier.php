<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$obVideo = CIBlockElement::GetList(["SORT"=>"ASC"], ["IBLOCK_CODE" => "videoblog", "ID" => $arResult["PROPERTIES"]["VIDEO"]["VALUE"], "ACTIVE" => "Y"], false, false, ["ID", "DATE_CREATE", "NAME", "DETAIL_TEXT", "PROPERTY_VIDEO_URL"]);

$arResult["VIDEO"] = [];

while ($arVideo = $obVideo->fetch()) 
{
	$arResult["VIDEO"][] = $arVideo;
}

$this->__component->setResultCacheKeys(array("VIDEO"));
?>