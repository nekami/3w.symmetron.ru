<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$filterName = $arParams["FILTER_NAME"] ?: "brandBannerFilter";
\Bitrix\Main\Loader::includeModule("iblock");
if (!empty($arParams["LIST_IBLOCK_ID"])) {
    $arFilter = array("CODE" => "TYPE", "IBLOCK_ID" => $arParams["LIST_IBLOCK_ID"]);
    $dbProp = \CIBlockPropertyEnum::GetList(array(), $arFilter);
    while ($prop = $dbProp->fetch()) {
        $props[$prop["XML_ID"]] = $prop["ID"];
    }
    if (!isset($GLOBALS[$filterName])) {
        $GLOBALS[$filterName] = [];
    }
    $GLOBALS[$filterName] = array_merge(array(
        "=PROPERTY_TYPE" => $props["BANNER"],
        "SECTION_CODE"   => $arResult["CODE"],
    ));

    ?>
    <? $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "brand_banner",
        Array(
            "ACTIVE_DATE_FORMAT"              => "d.m.Y",
            "ADD_SECTIONS_CHAIN"              => "N",
            "AJAX_MODE"                       => "N",
            "AJAX_OPTION_ADDITIONAL"          => "",
            "AJAX_OPTION_HISTORY"             => "N",
            "AJAX_OPTION_JUMP"                => "N",
            "AJAX_OPTION_STYLE"               => "Y",
            "CACHE_FILTER"                    => "N",
            "CACHE_TYPE"                      => $arParams["CACHE_TYPE"],
            "CACHE_TIME"                      => $arParams["CACHE_TIME"],
            "CACHE_GROUPS"                    => $arParams["CACHE_GROUPS"],
            "CHECK_DATES"                     => $arParams["CHECK_DATES"],
            "COMPOSITE_FRAME_MODE"            => "A",
            "COMPOSITE_FRAME_TYPE"            => "AUTO",
            "DETAIL_URL"                      => "",
            "DISPLAY_BOTTOM_PAGER"            => "N",
            "DISPLAY_DATE"                    => "N",
            "DISPLAY_NAME"                    => "N",
            "DISPLAY_PICTURE"                 => "N",
            "DISPLAY_PREVIEW_TEXT"            => "N",
            "DISPLAY_TOP_PAGER"               => "N",
            "FIELD_CODE"                      => array("NAME", "TAGS", "DETAIL_TEXT", "DETAIL_PICTURE", ""),
            "FILTER_NAME"                     => $filterName,
            "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
            "IBLOCK_ID"                       => $arParams["LIST_IBLOCK_ID"],
            "IBLOCK_TYPE"                     => "news",
            "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
            "INCLUDE_SUBSECTIONS"             => "N",
            "MESSAGE_404"                     => "",
            "NEWS_COUNT"                      => "5",
            "PAGER_BASE_LINK_ENABLE"          => "N",
            "PAGER_DESC_NUMBERING"            => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL"                  => "N",
            "PAGER_SHOW_ALWAYS"               => "N",
            "PAGER_TEMPLATE"                  => ".default",
            "PAGER_TITLE"                     => "",
            "PARENT_SECTION"                  => "",
            "PARENT_SECTION_CODE"             => "",
            "PREVIEW_TRUNCATE_LEN"            => "",
            "PROPERTY_CODE"                   => array("COLOR", "URL", ""),
            "SET_BROWSER_TITLE"               => "N",
            "SET_LAST_MODIFIED"               => "N",
            "SET_META_DESCRIPTION"            => "N",
            "SET_META_KEYWORDS"               => "N",
            "SET_STATUS_404"                  => "Y",
            "SET_TITLE"                       => "N",
            "SHOW_404"                        => "Y",
            "SORT_BY1"                        => $arParams["SORT_BY1"],
            "SORT_BY2"                        => $arParams["SORT_BY2"],
            "SORT_ORDER1"                     => $arParams["SORT_ORDER1"],
            "SORT_ORDER2"                     => $arParams["SORT_ORDER2"],
            "STRICT_SECTION_CHECK"            => "N",
        ),
        $component
    );
}
