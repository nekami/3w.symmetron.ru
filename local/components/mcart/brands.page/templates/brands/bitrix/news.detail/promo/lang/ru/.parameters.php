<?
$MESS["T_IBLOCK_DESC_PROMO_IBLOCK_ID"] = "ID инфоблока с промо";
$MESS["T_IBLOCK_DESC_FILTER_NAME"] = "Дополнительный фильтр (название переменной)";


$MESS["T_IBLOCK_DESC_ASC"] = "По возрастанию";
$MESS["T_IBLOCK_DESC_DESC"] = "По убыванию";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Название";
$MESS["T_IBLOCK_DESC_FACT"] = "Дата начала активности";
$MESS["T_IBLOCK_DESC_FSORT"] = "Сортировка";
$MESS["T_IBLOCK_DESC_FTSAMP"] = "Дата последнего изменения";
$MESS["T_IBLOCK_DESC_IBORD1"] = "Поле для первой сортировки новостей";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Направление для первой сортировки новостей";
$MESS["T_IBLOCK_DESC_IBORD2"] = "Поле для второй сортировки новостей";
$MESS["T_IBLOCK_DESC_IBBY2"] = "Направление для второй сортировки новостей";

