<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$arTemplateParameters = array(
    "LIST_IBLOCK_ID" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_SERIES_IBLOCK_ID"),
        "TYPE" => "STRING",
    ),
    "FILTER_NAME" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_FILTER_NAME"),
        "TYPE" => "STRING",
    ),
);
