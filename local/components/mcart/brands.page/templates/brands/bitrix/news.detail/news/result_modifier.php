<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$obNews = CIBlockElement::GetList(["SORT"=>"ASC"], ["IBLOCK_CODE" => "news", "PROPERTY_brands" => $arResult["ID"], "ACTIVE" => "Y"], false, false, ["ID"]);

$arResult["NEWS_ID"] = [];

while ($arNews = $obNews->fetch()) 
{
	$arResult["NEWS_ID"][] = $arNews["ID"];
}

$this->__component->setResultCacheKeys(array("NEWS_ID"));

?>