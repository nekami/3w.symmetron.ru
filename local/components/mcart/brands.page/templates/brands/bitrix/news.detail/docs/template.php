<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult["DOCS_TYPES"])):?>
    <section class="docs">
        <div class="wrapper">
            <h2><?=Loc::GetMessage("SYMMETRON_BRAND_DOCS_TITLE")?></h2>
            <div class="docs-wrapper">
                <div class="docs-sections">
                    <?foreach ($arResult["DOCS_TYPES"] as $id => $name):?>
                        <a href="<?="/docs/?SECTION_ID=".$id;?>"><?=$name?></a>
                    <?endforeach;?>
                </div>
                <?/*$APPLICATION->IncludeComponent(
                    "bitrix:search.title",
                    "brands_docs",
                    array(
                        "COMPONENT_TEMPLATE" => "brands_docs",
                        "TOP_COUNT" => "5",
                        "ORDER" => "date",
                        "USE_LANGUAGE_GUESS" => "N",
                        "CHECK_DATES" => "N",
                        "SHOW_OTHERS" => "N",
                        "PAGE" => $arResult["DOCS_FILTER"],
                        "DOCS_WHERE" => $arResult["DOCS_WHERE"],
                        "SHOW_INPUT" => "Y",
                        "INPUT_ID" => "title-search-input",
                        "CONTAINER_ID" => "title-search",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                    ),
                    false
                );*/?>
            </div>
        </div>
    </section>
<?endif;
