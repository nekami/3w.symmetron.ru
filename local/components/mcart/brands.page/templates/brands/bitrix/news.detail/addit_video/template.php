<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die;
$this->setFrameMode(true); 
?>

<? if ( !empty($arResult["VIDEO"]) ) { ?>
	<div class="series-video">
        <h1 class="series-video__title">Видео <?=$arResult["NAME"]?></h1>
        <div class="series-video-wrapper">		
			<? foreach ($arResult["VIDEO"] as $item) { ?>		
			
				<div class="series-video-item">
					
					<iframe width="560" height="315" src="<?=$item["PROPERTY_VIDEO_URL_VALUE"]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					
					<span class="series-video-item__date">
						<?=FormatDate("j F Y", MakeTimeStamp($item["DATE_CREATE"]));?>
					</span>
					
					<h4 class="series-video-item__name"><?=$item["NAME"];?></h4>
					
					<p class="series-video-item__description"><?=$item["DETAIL_TEXT"];?></p>
					
				</div>				
			
			<? } ?>			
        </div>
    </div>
<? } ?>

<?
/*
$returnParams = [];
foreach ($arParams as $key => $value) {
    if (substr($key, 0, 1) === "~")
        $returnParams[substr($key, 1)] = $value;
}
$arParams = $returnParams;

$arParams["FIELD_CODE"] = array("PREVIEW_PICTURE", "");
$arParams["PROPERTY_CODE"] = array("VIDEO_URL", "");

$arParams["ELEMENT_ID"] = $arResult["PROPERTIES"]["VIDEO"]["VALUE"];
$arParams["IBLOCK_ID"] = $arResult["PROPERTIES"]["VIDEO"]["LINK_IBLOCK_ID"];
$arParams["IBLOCK_TYPE"] = "news";

if (!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])) {
    $APPLICATION->IncludeComponent(
        "bitrix:news.detail",
        "brand_addit_video",
        $arParams,
        $component->getParent()
    );
}
*/
?>