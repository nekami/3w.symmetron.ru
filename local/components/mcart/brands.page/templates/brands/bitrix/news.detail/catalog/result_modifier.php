<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["ADDITIONAL_BRANDS"] = [];

if(!empty($arResult["ID"])){
    $arSelect = ["PROPERTY_ADDITIONAL_BRANDS"];
    $arFilter = ["ID"=>$arResult["ID"]];
    $resAdditionalBrands = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

    while($arAdditionalBrand = $resAdditionalBrands->Fetch()){
        if(!empty($arAdditionalBrand["PROPERTY_ADDITIONAL_BRANDS_VALUE"]))
            $arResult["ADDITIONAL_BRANDS"][] = $arAdditionalBrand["PROPERTY_ADDITIONAL_BRANDS_VALUE"];
    }

    $arResult["ADDITIONAL_BRANDS"][] = $arResult["ID"];
}