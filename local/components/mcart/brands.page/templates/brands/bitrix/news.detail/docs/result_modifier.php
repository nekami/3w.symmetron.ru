<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if (!empty($arParams["LIST_IBLOCK_ID"])) {
    /* Types of documents */
    $arResult["DOCS_TYPES"] = [];
    $arFilter = array(
        "IBLOCK_ID"   => $arParams["LIST_IBLOCK_ID"],
        "CODE"        => $arResult["CODE"],
        "DEPTH_LEVEL" => 1,
    );
    $section = \CIBlockSection::GetList(array(), $arFilter, false, array("ID"));
    if ($section_id = $section->fetch()) {
        $arFilter = array(
            "IBLOCK_ID"   => $arParams["LIST_IBLOCK_ID"],
            "SECTION_ID"  => $section_id["ID"],
            "DEPTH_LEVEL" => 2,
        );
        $section = \CIBlockSection::GetList(array(), $arFilter, true, array("ID", "NAME"));
        while ($sec = $section->fetch()) {
            if ($sec["ELEMENT_CNT"] > 0) {
                $arResult["DOCS_TYPES"][$sec["ID"]] = $sec["NAME"];
            }
        }

        $arFilter = array(
            "IBLOCK_ID"  => $arParams["LIST_IBLOCK_ID"],
            "SECTION_ID" => $section_id["ID"],
        );
        $count = \CIBlockElement::GetList(array(), $arFilter, array());
        if ($count > 0) {
            $arResult["DOCS_TYPES"][$section_id["ID"]] = Loc::GetMessage("SYMMETRON_BRAND_DOCS_OTHER");
        }
    }

    /* Filter string */
    $arResult["DOCS_FILTER"] = "";
    $arResult["DOCS_WHERE"] = "";
    $ib = \CIBlock::GetByID($arParams["LIST_IBLOCK_ID"]);
    if ($iblock = $ib->fetch()) {
        $delimiter = strpos("?", $arParams["SEARCH_PAGE"]) === false ? "?" : "&";
        $arResult["DOCS_WHERE"] = "iblock_".$iblock["IBLOCK_TYPE_ID"];
        $arResult["DOCS_FILTER"] = $arParams["SEARCH_PAGE"].$delimiter."where=".$arResult["DOCS_WHERE"];
    }
}
