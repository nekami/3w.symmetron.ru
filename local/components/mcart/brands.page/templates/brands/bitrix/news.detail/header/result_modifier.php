<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult["PROPERTIES"]["SUPPORT_MANAGER"]))
{
    $user = CUser::GetByID($arResult["PROPERTIES"]["SUPPORT_MANAGER"]["VALUE"]);
    if ($user = $user->fetch()) {
        $fio = trim($user["NAME"]." ".$user["LAST_NAME"]." ".$user["SECOND_NAME"]);
        if (empty($fio))
            $fio = $user["LOGIN"];
        $user["FULL_NAME"] = $fio;
        $arResult["PROPERTIES"]["SUPPORT_MANAGER"] = $user;
    }
}
if(!empty($arResult["PROPERTIES"]["SVG_LOGO"]["VALUE"])) {
    $arResult["PROPERTIES"]["SVG_LOGO"] = \CFile::GetFileArray($arResult["PROPERTIES"]["SVG_LOGO"]["VALUE"]);
}


//get srtificat
if(!empty($arResult["PROPERTIES"]["SERT"]["VALUE"])){
    $arFilter = ["ID" => $arResult["PROPERTIES"]["SERT"]["VALUE"]];
    $arSelect = ["ID", "PROPERTY_FILE", "PROPERTY_LINK_TO_RES"];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    if($sert = $res->Fetch()){
        if(!empty($sert["PROPERTY_FILE_VALUE"])){
            $arFile = CFile::GetFileArray($sert["PROPERTY_FILE_VALUE"]);
            $arResult["PATH_SERT"] = $arFile["SRC"];
        } elseif(!empty($sert["PROPERTY_LINK_TO_RES_VALUE"])) {
            $arResult["PATH_SERT"] = $sert["PROPERTY_LINK_TO_RES_VALUE"];
        }
    }
}

//get web form
if(CModule::IncludeModule("form")) {
    $arFilter = ["SID" => "FEEDBACK_BRAND"];
    $rsForms = CForm::GetList($by = "ID", $order = "desc", $arFilter, $is_filtered);
    if ($arForm = $rsForms->Fetch()) {
        $arResult["FEEDBACK_BRAND_FORM_ID"] = $arForm["ID"];
    }
}