<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$filterName = $arParams["FILTER_NAME"] ?: "brandsCatalogFilter";
if (!empty($arParams["LIST_IBLOCK_ID"])) {
    if (!isset($GLOBALS[$filterName])) {
        $GLOBALS[$filterName] = [];
    }

    $GLOBALS[$filterName] = array_merge(array(
        "PROPERTY_PRODUCERID" => $arResult["ADDITIONAL_BRANDS"],
    ));

    ?>
    <? $APPLICATION->IncludeComponent(
        "mcart:catalog.sections",
        ".default",
        Array(
            "CACHE_FILTER"         => "N",
            "CACHE_GROUPS"         => $arParams["CACHE_GROUPS"],
            "CACHE_TIME"           => $arParams["CACHE_TIME"],
            "CACHE_TYPE"           => $arParams["CACHE_TYPE"],
            "COMPOSITE_FRAME_MODE" => $arParams["COMPOSITE_FRAME_MODE"],
            "COMPOSITE_FRAME_TYPE" => $arParams["COMPOSITE_FRAME_TYPE"],
            "FILTER"               => ["PRODUCERID" => $arResult["ADDITIONAL_BRANDS"]],
            "FILTER_NAME"          => "",
            "IBLOCK_ID"            => $arParams["LIST_IBLOCK_ID"],
            "IBLOCK_TYPE"          => "catalog",
            "SECTION_FOLDER"       => "/catalog/",
            "SEF_FOLDER"           => $arParams["SEF_MODE"] != "Y" ? "/" : $arParams["SEF_FOLDER"],
            "SEF_MODE"             => $arParams["SEF_MODE"] != "Y" ? "N" : "Y",
            "SEF_URL_TEMPLATES"    => Array("sections" => "#SECTION_CODE_PATH#/"),
            "VARIABLE_ALIASES"     => Array(
                "SECTION_ID"   => "SECTION_ID",
                "SECTION_CODE" => "SECTION_CODE",
            ),
        ),
        $component
    );
}