<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"PROPERTY_CODE" => Array(
        "TYPE" => "HIDDEN",
        "VALUE" => ["DISTRIBUTOR", "SUPPORT", "W_DETAIL", "H_DETAIL", "SUPPORT_MANAGER", "SVG_LOGO"],
	),
); 
