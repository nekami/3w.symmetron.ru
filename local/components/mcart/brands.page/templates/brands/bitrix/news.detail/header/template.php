<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?
$APPLICATION->AddChainItem($arResult["NAME"]);
$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "",
    ["START_FROM" => 1]
);
?>
<?$actualLink = "https://" . $_SERVER["SERVER_NAME"] . $arResult["DETAIL_PAGE_URL"];?>

<?$this->SetViewTarget('link_canonical');?>
    <link rel="canonical" href="<?=$actualLink?>"/>
<?$this->EndViewTarget();?>

<section class="brand-head">
    <div class="wrapper">
        <div class="brand-start">

            <div class="brand-mainInfo">
                <h1><?=$arResult["NAME"]?></h1>
				<div class="brand-description">
					<span class="brand-description__text"><?=$arResult["PREVIEW_TEXT"]?></span>
					<span class="brand-description__more more"><?=Loc::GetMessage("SYMMETRON_BRAND_MORE");?></span>
					<span class="brand-description__more less"><?=Loc::GetMessage("SYMMETRON_BRAND_LESS");?></span>
				</div>
            </div>


            <?if (!empty($arResult["PROPERTIES"]["STAT"]["VALUE"])
                || !empty($arResult["PROPERTIES"]["SUPPORT"]["VALUE"])
                || !empty($arResult["PATH_SERT"])):?>
                <div class="distributor">
<div class="brand-logo__holder">
		<?if(isset($arResult["PROPERTIES"]["SVG_LOGO"]["SRC"])):?>
		<div class="brand-logo">
                        <img src="<?=$arResult["PROPERTIES"]["SVG_LOGO"]["SRC"]?>">
                </div>
                <?elseif(is_array($arResult["DETAIL_PICTURE"])):?>
                <?
                    $w = 50; $h = 30;
                    if (isset($arResult["PROPERTIES"]["W_DETAIL"]))
                        $w = is_numeric($arResult["PROPERTIES"]["W_DETAIL"]["VALUE"])
                        && $arResult["PROPERTIES"]["W_DETAIL"]["VALUE"] > 1
                            ? $arResult["PROPERTIES"]["W_DETAIL"]["VALUE"]
                            : $arResult["PROPERTIES"]["W_DETAIL"]["DEFAULT_VALUE"];
                    if (isset($arResult["PROPERTIES"]["H_DETAIL"]))
                        $h = is_numeric($arResult["PROPERTIES"]["H_DETAIL"]["VALUE"])
                        && $arResult["PROPERTIES"]["H_DETAIL"]["VALUE"] > 1
                            ? $arResult["PROPERTIES"]["H_DETAIL"]["VALUE"]
                            : $arResult["PROPERTIES"]["H_DETAIL"]["DEFAULT_VALUE"];
                    ?>
                <style>
                    .brand-logo__wrapper img {
                        max-width: <?=$w?>px;
                        height:  <?=$h?>px;
                    }
                </style>
                    <div class="brand-logo">
                        <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">
                    </div>
                <?endif?>
            </div>

                    <!--show distributor-->
                    <?if (!empty($arResult["PROPERTIES"]["STAT"]["VALUE"])):?>
                        <span class="distributor-info"><?=$arResult["PROPERTIES"]["STAT"]["VALUE"]?></span>
                    <?endif;?>

                    <!--show support-->
                    <?if (!empty($arResult["PROPERTIES"]["SUPPORT"]["VALUE"])):?>
                        <span class="distributor-support">
                            <a href="#modal-callback-brand" class="service-item support" rel="modal:open"><?=Loc::GetMessage("SYMMETRON_BRAND_SEND_MAIL");?></a>
                        </span>

                        <?if(!empty($arResult["FEEDBACK_BRAND_FORM_ID"])):?>
                            <div id="modal-callback-brand" class="modal">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:form.result.new",
                                    ".default",
                                    array(
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "A",
                                        "CHAIN_ITEM_LINK" => "",
                                        "CHAIN_ITEM_TEXT" => "",
                                        "COMPOSITE_FRAME_MODE" => "A",
                                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                                        "EDIT_URL" => "",
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                                        "LIST_URL" => "",
                                        "SEF_MODE" => "N",
                                        "SUCCESS_URL" => "",
                                        "USE_EXTENDED_ERRORS" => "Y",
                                        "WEB_FORM_ID" => $arResult["FEEDBACK_BRAND_FORM_ID"],
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "VARIABLE_ALIASES" => array(
                                            "WEB_FORM_ID" => "WEB_FORM_ID",
                                            "RESULT_ID" => "RESULT_ID",
                                        )
                                    ),
                                    false
                                );?>
                                <?if (isset($_POST["web_form_submit"]) && isset($_POST["WEB_FORM_ID"]) &&
                                    $_POST["WEB_FORM_ID"] == $arResult["FEEDBACK_BRAND_FORM_ID"]):?>
                                    <script>
                                        BX.ready(function () {
                                            $("#modal-callback-brand").modal();
                                        });
                                    </script>
                                <?endif;?>
                            </div>
                            <script>
                                var EMAIL_TO_HIDDEN_FIELD = document.getElementById("EMAIL_TO_HIDDEN_FIELD")
                                if(EMAIL_TO_HIDDEN_FIELD)
                                    EMAIL_TO_HIDDEN_FIELD.value = "<?=$arResult["PROPERTIES"]["SUPPORT"]["VALUE"]?>"
                            </script>
                        <?endif;?>

                    <?endif;?>

                    <!--show sertificate-->
                    <?if(!empty($arResult["PATH_SERT"])):?>
                        <noindex><a rel="nofollow" class="distributor-sert" target="_blank" href="<?=$arResult["PATH_SERT"];?>">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 198.69 196.15"><defs><style>.cls-1{fill:#2e3132;}</style></defs><title>pdf</title><path class="cls-1" d="M258.13,112.51v32h-16v55.13h16v109H440.82V112.51ZM424.82,292.67H274.13v-93h80.73V144.51H274.13v-16H424.82ZM290.13,252.73H408.82v16H290.13Zm0-31H408.82v16H290.13ZM265.1,177v10.52H259V157.62h11.64a13.47,13.47,0,0,1,5.92,1.23,9.1,9.1,0,0,1,3.93,3.49,9.8,9.8,0,0,1,1.37,5.16,8.57,8.57,0,0,1-3,6.92c-2,1.69-4.78,2.54-8.32,2.54Zm0-5h5.49a5.41,5.41,0,0,0,3.73-1.15,4.18,4.18,0,0,0,1.28-3.28,5,5,0,0,0-1.29-3.55,4.84,4.84,0,0,0-3.57-1.39H265.1Zm23,15.51V157.62h9.18a14,14,0,0,1,7.05,1.77,12.37,12.37,0,0,1,4.85,5.05,15.55,15.55,0,0,1,1.74,7.43v1.37a15.64,15.64,0,0,1-1.71,7.41,12.24,12.24,0,0,1-4.83,5,14.11,14.11,0,0,1-7,1.81Zm6.15-24.88v19.93h3a6.68,6.68,0,0,0,5.52-2.35,10.72,10.72,0,0,0,2-6.75v-1.58q0-4.56-1.89-6.9a6.66,6.66,0,0,0-5.52-2.35Zm42,12.67H324.53v12.21h-6.16V157.62h19.44v5H324.53v7.71h11.81Z" transform="translate(-242.13 -112.51)"/></svg>
                            <span><?=Loc::GetMessage("SYMMETRON_BRAND_SERT");?></span>
                        </a></noindex>
                    <?endif;?>
                </div>
            <?endif;?>
        </div>

    </div>
</section>



<!--<div id="modal-file" class="modal" >
<form name="SIMPLE_FORM_1" action="/" method="POST" enctype="multipart/form-data"><input type="hidden" name="sessid" id="sessid_1" value="d7830d31f7e871394639c5a4bd549741"><input type="hidden" name="WEB_FORM_ID" value="1"><input type="hidden" name="lang" value="ru"> 
<div class="exel main-modal"> 
  <h2>Обратная связь</h2>
 <span class="required-fields"><div class="desc"></div></span> 
  <div class="fio"><span class="fio">ФИО<font color="red"><span class="form-required starrequired">*</span></font></span> <input type="text" class="inputtext" name="form_text_1" value="" size="0"></div>
 
  <div class="mail"><span class="mail">E-mail</span> <input type="text" class="inputtext" name="form_email_2" value="" size="0"></div>
 
  <div class="mail"> </div>

 
  <div class="file"> <input type="text" name="path_to_file" id="path_to_file" style="display: none;"> 
    <div class="fake-input">Выберите файл</div>
    <input name="form_file_3" class="inputfile" size="0" type="file"><span class="bx-input-file-desc"></span> <span class="chose-file">Файл отсутствует</span> </div>
 
  <div class="comment"><span class="comment">Комментарии</span> <textarea name="form_textarea_4" cols="0" rows="0" class="inputtextarea"></textarea></div>
 
  <div class="submit"> 
    <div class="personal-data"><input type="checkbox" id="form_checkbox_SIMPLE_QUESTION_758_5" name="form_checkbox_SIMPLE_QUESTION_758[]" value="5"><label for="form_checkbox_SIMPLE_QUESTION_758_5"><span class="">&nbsp; Я согласен на обработку своих персональных данных в соответствии с положениями федерального закона от 27.07.2006 №152-ФЗ «О персональных данных»</span></label></div>
   <input type="submit" name="web_form_submit" value="Отправить"> </div>
 </div>
 </form><a href="#close-modal" rel="modal:close" class="close-modal ">Close</a></div>-->
