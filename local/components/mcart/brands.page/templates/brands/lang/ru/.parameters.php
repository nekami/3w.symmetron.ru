<?
$MESS["BRANDS_DETAIL_BLOCKS_TEMPLATE"] = "Шаблон блока №";

$MESS["T_IBLOCK_DESC_PROMO_IBLOCK_ID"] = "ID инфоблока с промо";
$MESS["T_IBLOCK_DESC_ARTICLE_IBLOCK_ID"] = "ID инфоблока со статьями";
$MESS["T_IBLOCK_DESC_DOCS_IBLOCK_ID"] = "ID инфоблока с документами";
$MESS["T_IBLOCK_DESC_CATALOG_IBLOCK_ID"] = "ID инфоблока с каталогом";

$MESS["T_IBLOCK_DESC_SEARCH_PAGE"] = "Страница поиска";
