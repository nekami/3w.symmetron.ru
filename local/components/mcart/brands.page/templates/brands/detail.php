<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arTemplateParams = [
    "SEF_FOLDER" => $arResult["FOLDER"].CComponentEngine::MakePathFromTemplate($arResult["URL_TEMPLATES"]["detail"], $arResult["VARIABLES"]),
    "SEF_MODE" => $arParams["SEF_MODE"],

    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],

    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

    "SET_TITLE"                       => "N",
    "SET_LAST_MODIFIED"               => "N",
    "MESSAGE_404"                     => "",
    "SET_STATUS_404"                  => "Y",
    "SHOW_404"                        => "Y",

    "DISPLAY_TOP_PAGER"               => "N",
    "DISPLAY_BOTTOM_PAGER"            => "N",
    "PAGER_SHOW_ALWAYS"               => "N",
    "DISPLAY_NAME"                    => "N",
    "USE_PERMISSIONS"                 => "N",
    "CHECK_DATES"                     => "Y",

    "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
    "ADD_ELEMENT_CHAIN"               => "N",
    "ADD_SECTIONS_CHAIN"              => "N",
    "SET_META_DESCRIPTION"            => "N",
    "SET_META_KEYWORDS"               => "N",
    "SET_BROWSER_TITLE"               => "N",

    "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
    "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],

    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
    "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],

];
?>
<?
if (isset($arParams['BLOCKS_COUNT']) && $arParams['BLOCKS_COUNT'] > 0) {

    $order = [];
    $blocks = array_fill(1, $arParams['BLOCKS_COUNT'], true);
    if (isset($arParams["BLOCKS_SORT"]) && is_array($arParams["BLOCKS_SORT"])) {
        foreach ($arParams["BLOCKS_SORT"] as $block) {
            if (isset($blocks[$block])) {
                $order[] = $block;
                $blocks[$block] = false;
            } elseif (isset($blocks[(-$block)])) {
                $blocks[(-$block)] = false;
            }
        }
        foreach ($blocks as $block => $is) {
            if ($is) {
                $order[] = $block;
            }
        }
    }

    foreach ($order as $i) {
        $code = $component->buildPrefix($i);

        if (empty($arParams[$code]) || $arParams[$code] === "-")
            continue;
        $path = __DIR__."/bitrix/news.detail/".$arParams[$code]."/.parameters.php";

        $currentParameters = array_merge(
            $arParams,
            $arTemplateParams,
            $component->getParameters($i, $arParams, $path)
        );

        $ElementID = $APPLICATION->IncludeComponent(
            "bitrix:news.detail",
            $arParams[$code],
            $currentParameters,
            $component
        );
    }
}
