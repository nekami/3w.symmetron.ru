<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//TODO вынести параметры компонента в настройки из публичной части
$params = Array(
    "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
    "ADD_ELEMENT_CHAIN" => "Y",
    "ADD_SECTIONS_CHAIN" => "N",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "BROWSER_TITLE" => "-",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CHECK_DATES" => "N",
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "ELEMENT_CODE" => $arResult["VARIABLES"]["SERIES_CODE"],
    "ELEMENT_ID" => $arResult["VARIABLES"]["SERIES_ID"],
    "FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "IBLOCK_ID" => $arParams["IBLOCK_SERIES_ID"],// Код информационного блока
    "IBLOCK_TYPE" => "catalog",
    "IBLOCK_URL" => "",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "MESSAGE_404" => "",
    "META_DESCRIPTION" => "-",
    "META_KEYWORDS" => "-",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_TEMPLATE" => ".default",
    "PAGER_TITLE" => "Страница",
    "PROPERTY_CODE" => array(	// Свойства
        0 => "themes",
        1 => "brands",
    ),
    "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
    "SET_CANONICAL_URL" => "N",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "Y",
    "SET_META_KEYWORDS" => "Y",
    "SET_STATUS_404" => "Y",	// Устанавливать статус 404
    "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
    "SHOW_404" => "N",	// Показ специальной страницы
    "STRICT_SECTION_CHECK" => "N",
    "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
    "USE_SHARE" => "N",
);
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.detail",
    "series",
    $params,
    $component
);?>
<!--<pre>--><?//print_r('template');?><!--</pre>-->
<!--<pre>--><?//print_r($arParams);?><!--</pre>-->
<?
if(empty($arResult["VARIABLES"]["SERIES_ID"]) && $arResult["VARIABLES"]["SERIES_CODE"]) {
    $arSelect = Array("ID", "NAME", "CODE", "IBLOCK_CODE");
    $arFilter = Array(
        'IBLOCK_ID' => $arParams["IBLOCK_SERIES_ID"],
        'CODE' => trim($arResult["VARIABLES"]["SERIES_CODE"]),
        'ACTIVE' => 'Y'
    );
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
    if ($ob = $res->fetch()) {
        $arResult["VARIABLES"]["SERIES_ID"] = $ob['ID'];
        $arResult["ENTITY_FILTER"] = $ob;
    }
}
$arFilter = ['=PROPERTY_series' => $arResult["VARIABLES"]["SERIES_ID"]];
$GLOBALS['arFilterSeriesNews'] = $arFilter;
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "news_block",
    array(
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "0",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "N",
        "COMPONENT_TEMPLATE" => "news_block",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "PREVIEW_TEXT",
            2 => "DATE_ACTIVE_FROM",
            3 => "",
        ),
        "FILTER_NAME" => "arFilterSeriesNews",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "8",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MEDIA_PROPERTY" => "",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "5",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => $arResult["ENTITY_FILTER"]["NAME"],
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "300",
        "PROPERTY_CODE" => array(
            0 => "TYPE",
            1 => "COLOR",
            2 => "TEXT_POSITION",
            3 => "themes",
            4 => "brands",
            5 => "series",
            6 => "activity",
        ),
        "LINK_PROPERTIES" => ['brands', 'themes', 'activity', 'TYPE'], //TODO вынести в настройки
        "SEARCH_PAGE" => "/search/",
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SLIDER_PROPERTY" => "",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_RATING" => "N",
        "USE_SHARE" => "N",
        "ENTITY_FILTER" => $arResult["ENTITY_FILTER"],
    ),
    $component
);?>
