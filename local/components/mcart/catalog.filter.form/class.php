<?php
if (CModule::IncludeModule("iblock") && function_exists("runkit_method_redefine")) {
    runkit_method_redefine(
        'CAllIBlockElement',
        'MkPropertyOrder',
        '$by, $order, $bSort, $db_prop, &$arJoinProps, &$arSqlOrder',
        '
        if (!function_exists("_Order1")){
            function _Order1($by, $order, $default_order, $nullable = true){
                static $arOrder = array(
                    "nulls,asc"  => array(true,  "asc" ),
                    "asc,nulls"  => array(false, "asc" ),
                    "nulls,desc" => array(true,  "desc"),
                    "desc,nulls" => array(false, "desc"),
                    "asc"        => array(true,  "asc" ),
                    "desc"       => array(false, "desc"),
                );
                $order = strtolower(trim($order));
                if(array_key_exists($order, $arOrder))
                    $o = $arOrder[$order];
                elseif(array_key_exists($default_order, $arOrder))
                    $o = $arOrder[$default_order];
                else
                    $o = $arOrder["desc,nulls"];

                if(!$nullable)
                {
                    if($o[1] == "asc")
                        $o[0] = true;
                    else
                        $o[0] = false;
                }

                return $o;
            }
        }
        if (!function_exists("_Order2")){
            function _Order2($by, $order, $default_order, $nullable = true){
                $o = _Order1($by, $order, $default_order, $nullable);

                if($o[0])
                {
                    if($o[1] == "asc")
                    {
                        return " IF(" . $by . " <> NULL OR " . $by . " <> 0 OR " . $by . " <> \'\', 0, 1), " . $by." asc";
                    }
                    else
                    {
                        return " IF(" . $by . " <> NULL OR " . $by . " <> 0 OR " . $by . " <> \'\', 0, 1), " . "length(".$by.")>0 asc, ".$by." desc";
                    }
                }
                else
                {
                    if($o[1] == "asc")
                    {
                        return " IF(" . $by . " <> NULL OR " . $by . " <> 0 OR " . $by . " <> \'\', 0, 1), " . "length(".$by.")>0 desc, ".$by." asc";
                    }
                    else
                    {
                        return " IF(" . $by . " <> NULL OR " . $by . " <> 0 OR " . $by . " <> \'\', 0, 1), " . $by." desc";
                    }
                }
            }
        }

        if($bSort && $db_prop["PROPERTY_TYPE"] != "L")
			return;

		global $DB;
		static $arJoinEFields = false;

		//Tables counters
		if($db_prop["VERSION"] == 2 && $db_prop["MULTIPLE"]=="N")
		{
			if(!array_key_exists($db_prop["IBLOCK_ID"], $arJoinProps["FPS"]))
				$iPropCnt = count($arJoinProps["FPS"]);
			else
				$iPropCnt = $arJoinProps["FPS"][$db_prop["IBLOCK_ID"]];
		}
		else
		{
			if(!array_key_exists($db_prop["ID"], $arJoinProps["FPV"]))
				$iPropCnt = count($arJoinProps["FPV"]);
			else
				$iPropCnt = $arJoinProps["FPV"][$db_prop["ID"]]["CNT"];
		}

		if($db_prop["PROPERTY_TYPE"] == "L")
		{
			if(!array_key_exists($db_prop["ID"], $arJoinProps["FPEN"]))
				$iFpenCnt = count($arJoinProps["FPEN"]);
			else
				$iFpenCnt = $arJoinProps["FPEN"][$db_prop["ID"]]["CNT"];
		}
		else
		{
			$iFpenCnt = -1;
		}

		$iElCnt = -1;
		$db_jprop = false;
		$ijPropCnt = -1;
		$ijFpenCnt = -1;

		if(is_array($by))
		{
			if(!$arJoinEFields) $arJoinEFields = array(
				"ID" => "BE#i#.ID",
				"TIMESTAMP_X" => "BE#i#.TIMESTAMP_X",
				"MODIFIED_BY" => "BE#i#.MODIFIED_BY",
				"CREATED" => "BE#i#.DATE_CREATE",
				"CREATED_DATE" => $DB->DateFormatToDB("YYYY.MM.DD", "BE#i#.DATE_CREATE"),
				"CREATED_BY" => "BE#i#.CREATED_BY",
				"IBLOCK_ID" => "BE#i#.IBLOCK_ID",
				"ACTIVE" => "BE#i#.ACTIVE",
				"ACTIVE_FROM" => "BE#i#.ACTIVE_FROM",
				"ACTIVE_TO" => "BE#i#.ACTIVE_TO",
				"SORT" => "BE#i#.SORT",
				"NAME" => "BE#i#.NAME",
				"SHOW_COUNTER" => "BE#i#.SHOW_COUNTER",
				"SHOW_COUNTER_START" => "BE#i#.SHOW_COUNTER_START",
				"CODE" => "BE#i#.CODE",
				"TAGS" => "BE#i#.TAGS",
				"XML_ID" => "BE#i#.XML_ID",
				"STATUS" => "BE#i#.WF_STATUS_ID",
			);
			//Joined Elements Field
			if(array_key_exists($by[2], $arJoinEFields))
			{
				//Then join elements
				if(!array_key_exists($db_prop["ID"], $arJoinProps["BE"]))
					$iElCnt = count($arJoinProps["BE"]);
				else
					$iElCnt = $arJoinProps["BE"][$db_prop["ID"]]["CNT"];

				$arSqlOrder[$by[0]] = _Order2(str_replace("#i#", $iElCnt, $arJoinEFields[$by[2]]), $order, "desc");
			}
			elseif(substr($by[2], 0, 9) == "PROPERTY_")
			{
				$jProp_ID = substr($by[2], 9);
				$db_jprop = CIBlockProperty::GetPropertyArray($jProp_ID, CIBlock::_MergeIBArrays($db_prop["LINK_IBLOCK_ID"]));
				if(is_array($db_jprop))
				{
					//join elements
					if(!array_key_exists($db_prop["ID"], $arJoinProps["BE"]))
						$iElCnt = count($arJoinProps["BE"]);
					else
						$iElCnt = $arJoinProps["BE"][$db_prop["ID"]]["CNT"];

					if($db_jprop["VERSION"] == 2 && $db_jprop["MULTIPLE"]=="N")
					{
						if(!array_key_exists($db_jprop["IBLOCK_ID"], $arJoinProps["BE_FPS"]))
							$ijPropCnt = count($arJoinProps["BE_FPS"]);
						else
							$ijPropCnt = $arJoinProps["BE_FPS"][$db_jprop["IBLOCK_ID"]]["CNT"];
					}
					else
					{
						if(!array_key_exists($db_jprop["ID"], $arJoinProps["BE_FPV"]))
							$ijPropCnt = count($arJoinProps["BE_FPV"]);
						else
							$ijPropCnt = $arJoinProps["BE_FPV"][$db_jprop["ID"]]["CNT"];
					}

					if($db_jprop["PROPERTY_TYPE"] == "L")
					{
						if(!array_key_exists($db_jprop["ID"], $arJoinProps["BE_FPEN"]))
							$ijFpenCnt = count($arJoinProps["BE_FPEN"]);
						else
							$ijFpenCnt = $arJoinProps["BE_FPEN"][$db_jprop["ID"]]["CNT"];
					}

					if($db_jprop["PROPERTY_TYPE"]=="L" && $bSort)
						$arSqlOrder["PROPERTY_".$by[1]."_".$by[2]] = _Order2("JFPEN".$ijFpenCnt.".SORT", $order, "desc");
					elseif($db_jprop["PROPERTY_TYPE"]=="L")
						$arSqlOrder["PROPERTY_".$by[1]."_".$by[2]] = _Order2("JFPEN".$ijFpenCnt.".VALUE", $order, "desc");
					elseif($db_jprop["VERSION"]==2 && $db_jprop["MULTIPLE"]=="N")
						$arSqlOrder["PROPERTY_".$by[1]."_".$by[2]] = _Order2("JFPS".$ijPropCnt.".PROPERTY_".$db_jprop["ORIG_ID"], $order, "desc");
					elseif($db_jprop["PROPERTY_TYPE"]=="N")
						$arSqlOrder["PROPERTY_".$by[1]."_".$by[2]] = _Order2("JFPV".$ijPropCnt.".VALUE_NUM", $order, "desc");
					else
						$arSqlOrder["PROPERTY_".$by[1]."_".$by[2]] = _Order2("JFPV".$ijPropCnt.".VALUE", $order, "desc");

				}
			}
		}
		else
		{
			if($db_prop["USER_TYPE"] == "mcart_property_with_measure_units"){
				$arSqlOrder[$by] = _Order2("FPV".$iPropCnt.".DESCRIPTION + 0", $order, "desc");
			}else{
				if($db_prop["PROPERTY_TYPE"]=="L" && $bSort)
					$arSqlOrder[$by] = _Order2("FPEN".$iFpenCnt.".SORT", $order, "desc");
				elseif($db_prop["PROPERTY_TYPE"]=="L")
					$arSqlOrder[$by] = _Order2("FPEN".$iFpenCnt.".VALUE", $order, "desc");
				elseif($db_prop["VERSION"]==2 && $db_prop["MULTIPLE"]=="N")
					$arSqlOrder[$by] = _Order2("FPS".$iPropCnt.".PROPERTY_".$db_prop["ORIG_ID"], $order, "desc");
				elseif($db_prop["PROPERTY_TYPE"]=="N")
					$arSqlOrder[$by] = _Order2("FPV".$iPropCnt.".VALUE_NUM", $order, "desc");
				else
					$arSqlOrder[$by] = _Order2("FPV".$iPropCnt.".VALUE", $order, "desc");
			}
		}

		//Pass join "commands" up there
		if($db_prop["VERSION"] == 2 && $db_prop["MULTIPLE"]=="N")
		{
			if(!array_key_exists($db_prop["IBLOCK_ID"], $arJoinProps["FPS"]))
				$arJoinProps["FPS"][$db_prop["IBLOCK_ID"]] = $iPropCnt;
		}
		else
		{
			if(!array_key_exists($db_prop["ID"], $arJoinProps["FP"]))
				$arJoinProps["FP"][$db_prop["ID"]] = array(
					"CNT" => count($arJoinProps["FP"]),
					"bFullJoin" => false,
				);
			if(!array_key_exists($db_prop["ID"], $arJoinProps["FPV"]))
				$arJoinProps["FPV"][$db_prop["ID"]] = array(
					"CNT" => $iPropCnt,
					"IBLOCK_ID" => $db_prop["IBLOCK_ID"],
					"MULTIPLE" => $db_prop["MULTIPLE"],
					"VERSION" => $db_prop["VERSION"],
					"JOIN" => $arJoinProps["FP"][$db_prop["ID"]]["CNT"],
					"bFullJoin" => false,
				);
		}

		if($iFpenCnt >= 0 && !array_key_exists($db_prop["ID"], $arJoinProps["FPEN"]))
			$arJoinProps["FPEN"][$db_prop["ID"]] = array(
				"CNT" => $iFpenCnt,
				"MULTIPLE" => $db_prop["MULTIPLE"],
				"VERSION" => $db_prop["VERSION"],
				"ORIG_ID" => $db_prop["ORIG_ID"],
				"JOIN" => $iPropCnt,
				"bFullJoin" => false,
			);

		if($iElCnt >= 0)
		{
			if(!array_key_exists($db_prop["ID"], $arJoinProps["BE"]))
				$arJoinProps["BE"][$db_prop["ID"]] = array(
					"CNT" => $iElCnt,
					"MULTIPLE" => $db_prop["MULTIPLE"],
					"VERSION" => $db_prop["VERSION"],
					"ORIG_ID" => $db_prop["ORIG_ID"],
					"JOIN" => $iPropCnt,
					"bJoinIBlock" => false,
					"bJoinSection" => false,
				);

			if(is_array($db_jprop))
			{
				if($db_jprop["VERSION"] == 2 && $db_jprop["MULTIPLE"]=="N")
				{
					if(!array_key_exists($db_jprop["IBLOCK_ID"], $arJoinProps["BE_FPS"]))
						$arJoinProps["BE_FPS"][$db_jprop["IBLOCK_ID"]] = array(
							"CNT" => $ijPropCnt,
							"JOIN" => $iElCnt,
						);
				}
				else
				{
					if(!array_key_exists($db_jprop["ID"], $arJoinProps["BE_FP"]))
						$arJoinProps["BE_FP"][$db_jprop["ID"]] = array(
							"CNT" => count($arJoinProps["BE_FP"]),
							"JOIN" => $iElCnt,
							"bFullJoin" => false,
						);
					if(!array_key_exists($db_jprop["ID"], $arJoinProps["BE_FPV"]))
						$arJoinProps["BE_FPV"][$db_jprop["ID"]] = array(
							"CNT" => $ijPropCnt,
							"IBLOCK_ID" => $db_jprop["IBLOCK_ID"],
							"MULTIPLE" => $db_jprop["MULTIPLE"],
							"VERSION" => $db_jprop["VERSION"],
							"JOIN" => $arJoinProps["BE_FP"][$db_jprop["ID"]]["CNT"],
							"BE_JOIN" => $iElCnt,
							"bFullJoin" => false,
						);
				}

				if($ijFpenCnt >= 0 && !array_key_exists($db_jprop["ID"], $arJoinProps["BE_FPEN"]))
					$arJoinProps["BE_FPEN"][$db_jprop["ID"]] = array(
						"CNT" => $ijFpenCnt,
						"MULTIPLE" => $db_jprop["MULTIPLE"],
						"VERSION" => $db_jprop["VERSION"],
						"ORIG_ID" => $db_jprop["ORIG_ID"],
						"JOIN" => $ijPropCnt,
						"bFullJoin" => false,
					);
			}
		}',
        RUNKIT_ACC_PUBLIC
    );
}

class McartCatalogFilterForm extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        /* Hide form by default or not */
        if ($arParams["HIDDEN"] === "N" || $arParams["HIDDEN"] === false) {
            $arParams["HIDDEN"] = false;
        } else {
            $arParams["HIDDEN"] = true;
        }

        /* Return full form or without header\footer */
        if ($arParams["FULL"] == "N" || $arParams["FULL"] === false) {
            $arParams["FULL"] = false;
        } else {
            $arParams["FULL"] = true;
        }

        /* Content type output */
        $types = ["empty", "checkbox", "radio", "range"];
        if (!in_array($arParams["TYPE"], $types)) {
            $arParams["TYPE"] = "empty";
        }
        if ($arParams["TYPE"] == "range")
            $arParams["RANGE_TYPE"] = trim($arParams["RANGE_TYPE"]) ?: "";

        if (!is_string($arParams["CODE"])) {
            $arParams["CODE"] = "";
        }

        return $arParams;
    }

    public function executeComponent()
    {
        $this->toMeasureUpdate();
        $this->includeComponentTemplate();
    }

    protected function toMeasureUpdate()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;


        if (!empty($arParams["USER_TYPE"]) &&
            \Bitrix\Main\Loader::includeModule("mcart.propertywithmeasureunits") &&
            $arParams["USER_TYPE"] == "mcart_property_with_measure_units") {
            $arParams["TYPE"] = "range_measure";

            $multiply = $arParams["USER_TYPE_SETTINGS"]["FIELD_MULTIPLIER"];

            if (empty($arParams["MEASURES_UNITS"])) {
                if (!empty($arParams["USER_TYPE_SETTINGS"])) {
                    //$elements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasureUnitsList(
                    //    $arParams["USER_TYPE_SETTINGS"], ["PROPERTY_ALTERNATIVE"]);
                    $elements = [];
                    if (!empty($arParams["USER_TYPE_SETTINGS"]["ELEMENT_ID"])) {
                        $arElements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasureUnitsListFromHL(
                            $arParams["USER_TYPE_SETTINGS"]["ELEMENT_ID"]
                        );

                        $elements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasureUnitsListFromArray(
                            $arParams["USER_TYPE_SETTINGS"], $arElements);
                    }
                }
            } else {
                $elements = \Mcart\PropertyWithMeasureUnits\Property::GetMeasureUnitsListFromArray(
                    $arParams["USER_TYPE_SETTINGS"], $arParams["MEASURES_UNITS"]);
            }

            foreach ($elements as $row) {
                if (!is_array($row["PROPERTY_ALTERNATIVE_VALUE"]) && !empty($row["PROPERTY_ALTERNATIVE_VALUE"])) {
                    $row["PROPERTY_ALTERNATIVE_VALUE"] = [$row["PROPERTY_ALTERNATIVE_VALUE"]];
                }
                $row["PROPERTY_ALTERNATIVE_VALUE"][] = $row["NAME"];
                $arResult["MEASURE_UNITS"][$row["ID"]] = [
                    "NAME" => $row["NAME"],
                    "MULTIPLIER" => $row["PROPERTY_" . $multiply . "_VALUE"],
                    "ALTERNATIVE" => array_unique($row["PROPERTY_ALTERNATIVE_VALUE"]),
                ];
                if ($row["PROPERTY_" . $multiply . "_VALUE"] == 1)
                    $arResult["DEFAULT_UNIT_ID"] = $row["ID"];
            }

            if ($arResult["DEFAULT_UNIT_ID"])
                $arResult["DEFAULT_UNIT"] = $arResult["MEASURE_UNITS"][$arResult["DEFAULT_UNIT_ID"]];

            foreach (["MIN", "MAX"] as $side) {
                $value = $arResult["MEASURE_UNITS"][$arParams["VALUES"][$side]['FACTOR']]["MULTIPLIER"];
                foreach ($arResult["MEASURE_UNITS"] as $element_id => $data) {
                    if ($side == "MIN" && $data["MULTIPLIER"] < $value ||
                        $side == "MAX" && $data["MULTIPLIER"] > $value) {
                        unset($arResult["MEASURE_UNITS"][$element_id]);
                    }
                }
            }

            foreach (["MIN", "MAX"] as $side) {
                if (empty($arParams["VALUES"][$side]["HTML_VALUE"])) {
                    $arParams["VALUES"][$side]["HTML_VALUE"] = $arParams["VALUES"][$side]["VALUE"];
                }
                if ($arParams["VALUES"][$side]["HTML_VALUE"] > 0 && $arParams["VALUES"][$side]["HTML_VALUE"] < 1) {
                    $arParams["VALUES"][$side]["HTML_VALUE"] =
                        \Mcart\PropertyWithMeasureUnits\Property::SwapBaseValueDegree($arParams["VALUES"][$side]["HTML_VALUE"]);
                }
                $arParams["VALUES"][$side]["HTML_REAL"] = ['VALUE' => $arParams["VALUES"][$side]["HTML_VALUE"] / $arResult["MEASURE_UNITS"][$arParams["VALUES"][$side]["FACTOR"]]["MULTIPLIER"], 'UNIT_ID' => $arParams["VALUES"][$side]["FACTOR"]];
                //$arParams["VALUES"][$side]["HTML_REAL"] = $this->ConvertMeasureValue($arParams["VALUES"][$side]["HTML_VALUE"]);
            }
        }
    }

    protected function ConvertMeasureValue($value)
    {
        $arResult = &$this->arResult;

        $value = \Mcart\PropertyWithMeasureUnits\Property::SwapBaseValueDegree($value);
        if ($value == 0 || empty($value)) {
            return [
                "VALUE" => $value,
                "UNIT_ID" => $arResult["DEFAULT_UNIT_ID"],
            ];
        }
        $count = 0;
        foreach ($arResult["MEASURE_UNITS"] as $element_id => $row) {
            $degree = floor(log10($value / $row["MULTIPLIER"]));
            if ($degree >= 0 && $degree < 3 ||
                $degree < 0 && $count == 0 ||
                $count == -1 + count($arResult["MEASURE_UNITS"])) {
                return [
                    //"VALUE" => round($value / $row["MULTIPLIER"], 3),
                    "VALUE" => $value / $row["MULTIPLIER"],
                    "UNIT_ID" => $element_id,
                ];
            }
            $count++;
        }
        return $value;
    }
}
