<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arParams["VALUES"] as $row):?>
    <? if (empty($row["CHECKED"])) {
        continue;
    } ?>
    <div class="filter-item__option" elid="<?= $row["CONTROL_ID"] ?>">
        <div class="items-choice__radio">
            <label>
                <input type="radio" name="<?= $row["CONTROL_NAME_ALT"] ?>" checked>
                <?= $row["VALUE"] ?>
            </label>
        </div>
        <input checked type="hidden"
               id="<?= $row["CONTROL_ID"] ?>"
               name="<?= $row["CONTROL_NAME_ALT"] ?>"
               value="<?= $row["HTML_VALUE_ALT"] ?>">
    </div>
    <? break; ?>
<? endforeach; ?>
<div class="filter-item__range"></div>