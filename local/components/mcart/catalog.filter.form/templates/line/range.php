<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="filter-item__option range-option">
    <?
    $addit = "";
    if (empty($arParams["VALUES"]["MIN"]["HTML_VALUE"])) {
        $addit = "_MORE";
    }
    if (empty($arParams["VALUES"]["MAX"]["HTML_VALUE"])) {
        $addit = "_LESS";
    }
    ?>
    <?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_RANGE".$addit, [
        "#FROM#" => "<span>".$arParams["VALUES"]["MIN"]["HTML_VALUE"]."</span>",
        "#TO#"   => "<span>".$arParams["VALUES"]["MAX"]["HTML_VALUE"]."</span>",
    ]); ?>
    <input checked type="hidden"
           id="<?= $arParams["VALUES"]["MIN"]["CONTROL_ID"] ?>"
           name="<?= $arParams["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
           value="<?= $arParams["VALUES"]["MIN"]["HTML_VALUE"] ?>">
    <input checked type="hidden"
           id="<?= $arParams["VALUES"]["MAX"]["CONTROL_ID"] ?>"
           name="<?= $arParams["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
           value="<?= $arParams["VALUES"]["MAX"]["HTML_VALUE"] ?>">
</div>
<div class="filter-item__range"></div>