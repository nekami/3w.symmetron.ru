<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__."/range.php");
?>
<div class="filter-item__option range-option">
    <?
    $addit = "";
    if (empty($arParams["VALUES"]["MIN"]["HTML_VALUE"])) {
        $addit = "_MORE";
    }
    if (empty($arParams["VALUES"]["MAX"]["HTML_VALUE"])) {
        $addit = "_LESS";
    }
    ?>
    <?$min_unit_id = $arParams["VALUES"]["MIN"]["HTML_REAL"]["UNIT_ID"] ?: $arResult["DEFAULT_UNIT_ID"]?>
    <?$max_unit_id = $arParams["VALUES"]["MAX"]["HTML_REAL"]["UNIT_ID"] ?: $arResult["DEFAULT_UNIT_ID"]?>
    <?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_RANGE".$addit, [
        "#FROM#" => "<span>".$arParams["VALUES"]["MIN"]["HTML_REAL"]["VALUE"]." ".$arResult["MEASURE_UNITS"][$min_unit_id]["NAME"]."</span>",
        "#TO#"   => "<span>".$arParams["VALUES"]["MAX"]["HTML_REAL"]["VALUE"]." ".$arResult["MEASURE_UNITS"][$max_unit_id]["NAME"]."</span>",
    ]); ?>
    <input checked type="hidden"
           id="<?= $arParams["VALUES"]["MIN"]["CONTROL_ID"] ?>"
           name="<?= $arParams["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
           value="<?= $arParams["VALUES"]["MIN"]["HTML_VALUE"] ?>">
    <input checked type="hidden"
           id="<?= $arParams["VALUES"]["MAX"]["CONTROL_ID"] ?>"
           name="<?= $arParams["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
           value="<?= $arParams["VALUES"]["MAX"]["HTML_VALUE"] ?>">
</div>
<div class="filter-item__range"></div>