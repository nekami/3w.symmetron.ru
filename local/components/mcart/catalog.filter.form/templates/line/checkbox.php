<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$count = 0;
foreach ($arParams["VALUES"] as $row):?>
    <? if (empty($row["CHECKED"])) {
        continue;
    }
    $count++;
    ?>
    <div class="filter-item__option checkbox-option" elid="<?= $row["CONTROL_ID"] ?>">
        <div class="option-name"><?= $row["VALUE"] ?></div>
        <input checked type="hidden"
               id="<?= $row["CONTROL_ID"] ?>"
               name="<?= $row["CONTROL_NAME"] ?>"
               value="<?= $row["HTML_VALUE"] ?>">
        <div class="option-delete"><span></span><span></span></div>
    </div>
<? endforeach; ?>
<div class="filter-item__addit">
    <?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_MORE") ?><span class="count"><?= $count - 3 ?></span>
</div>
<div class="filter-item__add"><span></span><span></span></div>