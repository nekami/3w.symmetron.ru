<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? if ($arParams["FULL"]): ?>
    <div class="filter-item <?= $arParams["CODE"] ?>__filter" itemid="<?= $arParams["CODE"] ?>">
    <div class="filter-item__delete"><span></span><span></span></div>
    <div class="filter-item__name"><?= $arParams["TITLE"] ?>:</div>
<? endif; ?>
<? $this->getComponent()->includeComponentTemplate($arParams["TYPE"]); ?>
<? if ($arParams["FULL"]): ?>
    </div>
<? endif;