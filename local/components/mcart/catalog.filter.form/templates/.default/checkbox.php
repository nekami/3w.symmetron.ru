<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? foreach ($arParams["VALUES"] as $row): ?>
    <div class="catalog-filter__item">
        <label>
            <input type="checkbox"
                   value="<?= $row["HTML_VALUE"] ?>"
                   name="<?= $row["CONTROL_NAME"] ?>"
                   id="<?= $row["CONTROL_ID"] ?>"
                <?= $row["CHECKED"] ? 'checked="checked"' : '' ?>
            /><?= $row["VALUE"] ?>
        </label>
    </div>
<? endforeach; 