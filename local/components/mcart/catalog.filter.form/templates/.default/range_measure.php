<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (!empty($arResult["MEASURE_UNITS"]) && !empty($arResult["DEFAULT_UNIT"])):?>
    <?
    $defaultUnitName = $arResult["DEFAULT_UNIT"]["NAME"];
    $defaultUnitLength = strlen($defaultUnitName);
    ?>
    <div class="range-inputs">
        <? foreach (["MIN", "MAX"] as $side):?>
            <div class="range-wrapper">
                <span><?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_RANGE_" . $side . "_TEXT") ?></span>
                <div class="range-input__wrapper measure">
                    <input type="hidden" id="<?= $arParams["VALUES"][$side]["CONTROL_ID"] ?>"
                           name="<?= $arParams["VALUES"][$side]["CONTROL_NAME"] ?>"
                           value="<?= $arParams["VALUES"][$side]["HTML_VALUE"] ?>">
                    <input type="hidden" id="<?= $arParams["VALUES"][$side]["CONTROL_ID"] ?>_OLD"
                           value="<?= $arParams["VALUES"][$side]["OLD"]["VALUE"] ?>"
                           data-measure="<?= $arParams["VALUES"][$side]["OLD"]["FACTOR"] ?>">
                    <input type="text" value="<?= $arParams["VALUES"][$side]["HTML_REAL"]["VALUE"] ?>">
                    <div class="range-value__select">
                        <? $unit_id = $arParams["VALUES"][$side]["HTML_REAL"]["UNIT_ID"] ?: $arResult["DEFAULT_UNIT_ID"] ?>
                        <span unit-id="<?= $unit_id ?>">
				<? if(preg_match_all('/'.$defaultUnitName.'$/m', $arResult["MEASURE_UNITS"][$unit_id]["NAME"], $matches, PREG_SET_ORDER, 0) && $defaultUnitName != $arResult["MEASURE_UNITS"][$unit_id]["NAME"]){ ?>
								<u><?= substr($arResult["MEASURE_UNITS"][$unit_id]["NAME"], 0, -$defaultUnitLength) ?></u><?= $defaultUnitName ?>
							<? }else{ ?>
								<?= $arResult["MEASURE_UNITS"][$unit_id]["NAME"] ?>
							<? } ?>
			</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.03 25.53">
                            <path class="select-icon" d="M461.66,309l-9,9-9-9Z" transform="translate(-443.63 -292.49)"/>
                            <path class="select-icon" d="M443.63,301.51l9-9,9,9Z"
                                  transform="translate(-443.63 -292.49)"/>
                        </svg>
                        <div class="range-select__options">
                            <? foreach ($arResult["MEASURE_UNITS"] as $element_id => $row):?>
                                <div class="range-option <?= ($element_id == $unit_id ? 'unit' : '') ?>">
                        <span unit-id="<?= $element_id ?>">
                            <? if(preg_match_all('/'.$defaultUnitName.'$/m', $row["NAME"], $matches, PREG_SET_ORDER, 0) && $defaultUnitName != $row["NAME"]){ ?>
								<u><?= substr($row["NAME"], 0, -$defaultUnitLength) ?></u><?= $defaultUnitName ?>
							<? }else{ ?>
								<?= $row["NAME"] ?>
							<? } ?>
                        </span>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
                <p class="error">
                    <?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_RANGE_" . $side . "_ERROR") ?>
                </p>
            </div>
        <? endforeach; ?>
    </div>
    <?
    $units_list = [];
    foreach ($arResult["MEASURE_UNITS"] as $element_id => $row) {
        $units_list[$element_id] = [
            "MULTIPLIER" => $row["MULTIPLIER"],
            "NAMES" => $row["ALTERNATIVE"],
        ];
    }
    ?>
    <script>
        SymmetronFilterFormObject.addUnits(
            "<?= $arParams["CODE"] ?>",
            <?=$arResult["DEFAULT_UNIT_ID"]?>,
            <?=\CUtil::PhpToJSObject($units_list)?>
        );
    </script>
<? endif;