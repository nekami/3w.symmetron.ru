<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? foreach ($arParams["VALUES"] as $row): ?>
    <div class="catalog-filter__item">
        <label>
            <input type="radio"
                   value="<?= $row["HTML_VALUE_ALT"] ?>"
                   name="<?= $row["CONTROL_NAME_ALT"] ?>"
                   id="<?= $row["CONTROL_ID"] ?>"
                <?= $row["CHECKED"] ? 'checked="checked"' : '' ?>
            /><?= $row["VALUE"] ?>
        </label>
    </div>
<? endforeach; 