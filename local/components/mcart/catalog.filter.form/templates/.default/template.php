<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if ($arParams["FULL"]):?>
    <script>
        var SymmetronFilterFormObject = new SymmetronFilterForm();
    </script>
<?endif;
if ($arParams["FULL"]):?>
    <div class="catalog-filter__list <?= $arParams["HIDDEN"] ? "hide" : "" ?>">
	 <div class="close-thisFilter">
		<span></span>
		<span></span>
	</div>
    <div class="catalog-filter__header">
		<?= $arParams["TITLE"] ?>
	</div>
    <div class="catalog-filter__search-wrap hide">
        <input type="text" placeholder="<?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_SEARCH")?>" class="catalog-filter__search">
        <div class="catalog-filter__submit">
            <img src="<?=$templateFolder?>/images/arrow.png">
        </div>
    </div>

        <? endif; ?>
    <div class="list__wrapper <?=$arParams["TYPE"]?>" itemid="<?= $arParams["CODE"] ?>">
        <? $this->getComponent()->includeComponentTemplate($arParams["TYPE"]); ?>
    </div>
<?
if ($arParams["FULL"]): ?>
    <div class="catalog-list__footer">
        <div class="tFilter-result">
            <?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_RESULT_COUNT", [
                "#COUNT#" => "<span>".($arParams["COUNT"] ?: 0)."</span>",
            ]) ?>
        </div>
    </div>
    <div id="submit-holder">
        <button class="default-button add-table-filter">
            <?= Loc::GetMessage("MCART_CATALOG_FILTER_FORM_ADD") ?>
        </button>
    </div>
    </div>
<?endif;
