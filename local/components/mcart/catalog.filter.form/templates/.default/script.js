Math.log10 = Math.log10 || function(x) {
  return Math.log(x) / Math.LN10;
};

function SymmetronFilterForm() {
    this.unit_storage = {};


    this.filterNode = $(".catalog-filter__list").first()
    this.bind();
}

SymmetronFilterForm.prototype.bind = function () {
    this.bindMoney();
    this.bindMeasure();
    this.bindSearch();
};

/**
 * Money input borders
 */
SymmetronFilterForm.prototype.bindMoney = function () {
    var _this = this;

    BX.ready(function () {
        _this.filterNode.on("change", ".range-input__wrapper.money > input[type='text']", function (event) {
            event.preventDefault()
            _this.moneyPrepareInputText(this)
        });
    });
};

SymmetronFilterForm.prototype.moneyPrepareInputText = function (input) {
    var value = $(input).val()

    /* Swap "," to "." */
    value = value.replace(/,/g, ".")
    /* Clear all (!) nondigit symbols charaacters */
    value = value.replace(/[^\d\.]*/g, "")

    /* Get relevant parts of string */
    value = /(\d*\.?\d{2})/.exec(value)
    if (value === null) {
        value = ""
    } else {
        value = value[0]
    }
    $(input).val(value)
}

/**
 * Search field block
 */
SymmetronFilterForm.prototype.bindSearch = function () {

    var _this = this;

    BX.ready(function () {
        _this.filterNode.on("click", ".catalog-filter__search-wrap > .catalog-filter__submit", function () {
            _this.searchFilter()
        });
        _this.filterNode.on("keyup", ".catalog-filter__search-wrap > input", function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                _this.searchFilter()
            }
        });
    });
};

SymmetronFilterForm.prototype.searchFilerVisible = function () {
    if (this.searchGetInputs().length >= 10) {
        this.filterNode.children(".catalog-filter__search-wrap").removeClass("hide")
        this.searchClear()
    } else {
        this.filterNode.children(".catalog-filter__search-wrap").addClass("hide")
    }
}
SymmetronFilterForm.prototype.searchClear = function () {
    this.filterNode.find(".catalog-filter__search-wrap > input").val("")
    this.searchGetInputs().parent().parent().removeClass("hideFiltered")
}
SymmetronFilterForm.prototype.searchGetInputs = function () {
    return this.filterNode.children(".list__wrapper").find("input:not([type=hidden])")
}
SymmetronFilterForm.prototype.searchFilter = function () {
    var searchText = this.filterNode.find(".catalog-filter__search-wrap > input").val()
    var inputs = this.searchGetInputs()
    var current, text;
    var regex = new RegExp(searchText.toLowerCase());

    for (var i = 0; i < inputs.length; i++) {
        current = $(inputs[i])
        if (current.attr("type") === "checkbox") {
            text = current.parent().text().trim().toLowerCase()
            if(text.search(regex) === -1) {
                current.parent().parent().addClass("hideFiltered")
            } else {
                current.parent().parent().removeClass("hideFiltered")
            }
        }
    }
}

/**
 * Measure units methods block
 */
SymmetronFilterForm.prototype.addUnits = function (code, default_element_id, unit_list) {
    for (var i in unit_list) {
        //unit_list[i].MULTIPLIER = Number.parseFloat(unit_list[i].MULTIPLIER);
        //I changed it, becos its not wirking in IE
        unit_list[i].MULTIPLIER = parseFloat(unit_list[i].MULTIPLIER);
    }

    this.unit_storage[code] = {
        base: default_element_id,
        list: unit_list,
    }
}

SymmetronFilterForm.prototype.bindMeasure = function () {
    var _this = this;

    BX.ready(function () {
        _this.filterNode.on("click", ".range-input__wrapper.measure > input[type='text']", function (event) {
            event.stopPropagation()
        });

        _this.filterNode.on("focus", ".range-input__wrapper.measure > input[type='text']", function (event) {
            _this.unitListCloseOther($(this).next())
            _this.unitListOpen($(this).next())
        });
        //_this.filterNode.on("blur", ".range-input__wrapper.measure > input[type='text']", function (event) {
        //    _this.unitListClose($(this).next())
        //});

        _this.filterNode.on("focusout", ".range-input__wrapper.measure > input[type='text']", function (event) {
            _this.filterInput(this.parentElement.querySelector(".range-value__select"));
	    _this.unitRecalculate($(this).parent());
        });

        _this.filterNode.on("click", ".range-input__wrapper.measure > .range-value__select", function (event) {
            _this.unitListClick(this)
            event.stopPropagation()
        });	

        _this.filterNode.on("change", ".range-input__wrapper.measure > input[type='text']", function (event) {
            event.preventDefault()
            _this.unitPrepareInputText(this, true)
        });

        _this.filterNode.on("keyup", ".range-input__wrapper.measure > input[type='text']", function (event) {
            event.preventDefault()
            if(event.keyCode == 13) {
                this.blur()
                _this.unitLostFocus();
		_this.unitRecalculate($(this).parent())
            } else if (event.keyCode != 32) {
                _this.unitPrepareInputText(this, false)
            }
        });

        _this.filterNode.on("click", ".range-input__wrapper.measure .range-select__options > .range-option", function (event) {
            event.stopPropagation();
	    _this.unitChangeMeasure(this);
	    _this.unitListClose($(this).parent().parent());
           //
            _this.unitRecalculate($(this).parents(".range-input__wrapper.measure").first());
        });

        $("body").on("click", function (event) {
            var target = $(event.target)
            //if (target.parents(_this.filterNode).length === 0)
            if (target.parents(".range-select__options").length === 0) {
                _this.unitLostFocus();
            }
        });

    });
};

SymmetronFilterForm.prototype.unitLostFocus = function () {
    var unitLists = this.filterNode.find(".range-input__wrapper.measure > .range-value__select")
    this.unitListClose(unitLists)
}

SymmetronFilterForm.prototype.unitListClick = function (element) {
    element = $(element)
    this.unitListCloseOther(element)

    if (!element.hasClass("open")) {
        this.unitListOpen(element)
    } else {
        this.unitListClose(element)
    }
}
SymmetronFilterForm.prototype.unitListOpen = function (element) {
	if(element.length == 0){
		element = element.context.parentElement.lastElementChild;
	}

    $(element).addClass("open");

    var unitId = $(element).children("span").first().attr("unit-id");
    var unitListNode = $(element).find(".range-select__options span[unit-id='" + unitId + "']").first().parent()

    $(element).children(".range-select__options").css({
        'top': -2 - unitListNode.position().top
    })
    this.unitListMark(unitListNode)
}
SymmetronFilterForm.prototype.unitListClose = function (element) {
    $(element).removeClass("open");
}
SymmetronFilterForm.prototype.filterInput = function(element){
    if(element instanceof Element && element.classList.contains('range-value__select')){
        let unit_storage = this.unit_storage;
        let timerId = setTimeout(function request() {
            if (element.classList.contains('open')) {
                timerId = setTimeout(request, 1);
            }else{
		function numberToString(num)
		{
		    let numStr = String(num);
	
		    if (Math.abs(num) < 1.0)
		    {
		        let e = parseInt(num.toString().split('e-')[1]);
		        if (e)
		        {
		            let negative = num < 0;
		            if (negative) num *= -1
		            num *= Math.pow(10, e - 1);
		            numStr = '0.' + (new Array(e)).join('0') + num.toString().substring(2);
		            if (negative) numStr = "-" + numStr;
		        }
		    }
		    else
		    {
		        let e = parseInt(num.toString().split('+')[1]);
		        if (e > 20)
		        {
		            e -= 20;
		            num /= Math.pow(10, e);
		            numStr = num.toString() + (new Array(e + 1)).join('0');
		        }
		    }
	
		    return numStr;
		}

                timerId = undefined;
                let values = element.parentElement.querySelectorAll("input");

                let unitId =  element.querySelector('.range-option.mark').children[0].getAttribute("unit-id");
                let unitIdOld = values[1].getAttribute("data-measure");

                let code = element.parentElement.parentElement.parentElement.parentElement.getAttribute("itemid");

                let valueOld = parseFloat(values[1].value) / parseFloat(unit_storage[code].list[unitId].MULTIPLIER);
                let value = parseFloat(values[2].value);

		let range_inputs = element.parentElement.parentElement.parentElement;

		if(values[0].id.includes("_MIN")){
			var alternativeOld = range_inputs.querySelector("#"+values[1].id.replace("_MIN", "_MAX"));
			var alternative = range_inputs.querySelector("#"+values[0].id.replace("_MIN", "_MAX"));
		}else{
			var alternativeOld = range_inputs.querySelector("#"+values[1].id.replace("_MAX", "_MIN"));
			var alternative = range_inputs.querySelector("#"+values[0].id.replace("_MAX", "_MIN"));
		}

		let alternativeValue = parseFloat(alternative.value) / parseFloat(unit_storage[code].list[unitId].MULTIPLIER);
		let alternativeValueOld = parseFloat(alternativeOld.value) / parseFloat(unit_storage[code].list[unitId].MULTIPLIER);

                valueOld = parseFloat(valueOld.toFixed(2));
                value = parseFloat(value.toFixed(2));

                if(values[0].id.includes("_MIN") && (value < valueOld || value > alternativeValueOld)){
                    let newVal =  parseFloat(values[1].value) / parseFloat(unit_storage[code].list[unitIdOld].MULTIPLIER);

                    values[2].setAttribute("value", parseFloat(newVal.toFixed(2)));
                    values[2].value = parseFloat(newVal.toFixed(2));

                    values[0].setAttribute("value", values[1].value);
                    values[0].value = values[1].value;

                    let unitListNode = $(element).find(".range-select__options span[unit-id='" + unitIdOld + "']").first().parent();

                    unitListNode = $(unitListNode);
                    unitListNode.parent().parent().children("span").replaceWith(
                        unitListNode.children("span").first().clone()
                    );

                    $(element).children(".range-select__options").css({
                        'top': -2 - unitListNode.position().top
                    });
                    unitListNode = $(unitListNode);
                    unitListNode.parent().children().removeClass("mark");
                    unitListNode.addClass("mark");

                    let error = element.parentElement.parentElement.querySelector("p[class='error']");
                    error.style.fontSize = "10px";
		    $(values[0]).change();
                }else if(values[0].id.includes("_MAX") && (value > valueOld || value < alternativeValueOld)){
                    let newVal =  parseFloat(values[1].value) / parseFloat(unit_storage[code].list[unitIdOld].MULTIPLIER);

                    values[2].setAttribute("value", parseFloat(newVal.toFixed(2)));
                    values[2].value = parseFloat(newVal.toFixed(2));

                    values[0].setAttribute("value", values[1].value);
                    values[0].value = values[1].value;

                    let unitListNode = $(element).find(".range-select__options span[unit-id='" + unitIdOld + "']").first().parent();

                    unitListNode = $(unitListNode);
                    unitListNode.parent().parent().children("span").replaceWith(
                        unitListNode.children("span").first().clone()
                    );

                    $(element).children(".range-select__options").css({
                        'top': -2 - unitListNode.position().top
                    });
                    unitListNode = $(unitListNode);
                    unitListNode.parent().children().removeClass("mark");
                    unitListNode.addClass("mark");

                    let error = element.parentElement.parentElement.querySelector("p[class='error']");
                    error.style.fontSize = "10px";
		    $(values[0]).change();
                }else if(value !== valueOld){
                    let error = element.parentElement.parentElement.querySelector("p[class='error']");
                    error.style.fontSize = "0px";

			let curentElement = element.parentElement;
			let alternativeElement = alternative.parentElement;

			let unitCurrent = curentElement.querySelector(".range-value__select").children[0];
			let unitAlternative = alternativeElement.querySelector(".range-value__select").children[0];

			let newValue = value * parseFloat(unit_storage[code].list[unitId].MULTIPLIER);
			let newAlternativeValue = alternativeValue * parseFloat(unit_storage[code].list[unitId].MULTIPLIER);

			let alternativeHidden = alternativeElement.querySelector("input[type=hidden]");
			let currentHidden = curentElement.querySelector("input[type=hidden]");	

			let alternativeText = alternativeElement.querySelector("input[type=text]");
			let currentText = curentElement.querySelector("input[type=text]");	
	
			if(values[0].id.includes("_MIN") && alternativeValue < value){
				let saveUnit = unitCurrent.outerHTML;
				
				$(unitCurrent).replaceWith(unitAlternative.outerHTML);
				$(unitAlternative).replaceWith(saveUnit);

				let save = alternativeText.value;

				alternativeText.value = currentText.value;
				alternativeText.setAttribute("value", currentText.value);

				currentText.value = save;
				currentText.setAttribute("value", save);
				
				alternativeHidden.value = numberToString(newValue);
				currentHidden.value = numberToString(newAlternativeValue);

				alternativeHidden.setAttribute("value", numberToString(newValue));
				currentHidden.setAttribute("value", numberToString(newAlternativeValue));

				$(alternativeHidden).change();
			}else if(values[0].id.includes("_MAX") && alternativeValue > value){
				let saveUnit = unitCurrent.outerHTML;
				
				$(unitCurrent).replaceWith(unitAlternative.outerHTML);
				$(unitAlternative).replaceWith(saveUnit);

				let save = alternativeText.value;

				alternativeText.value = currentText.value;
				alternativeText.setAttribute("value", currentText.value);

				currentText.value = save;
				currentText.setAttribute("value", save);

				alternativeHidden.value = numberToString(newValue);
				currentHidden.value = numberToString(newAlternativeValue);

				alternativeHidden.setAttribute("value", numberToString(newValue));
				currentHidden.setAttribute("value", numberToString(newAlternativeValue));

				$(alternativeHidden).change();
			}
                }
            }
        }, 1);
    }
}
SymmetronFilterForm.prototype.unitListCloseOther = function (element) {
    var unitLists = this.filterNode.find(".range-input__wrapper.measure > .range-value__select.open")
    var target = $(element)[0];

    for (var i = 0; i < unitLists.length; i++) {
        if (target === unitLists[i]) {
            continue;
        }
        this.unitListClose(unitLists[i])
    }
}
SymmetronFilterForm.prototype.unitListMark = function (element) {
    element = $(element);

    element.parent().children().removeClass("mark")
    element.addClass("mark")
}

SymmetronFilterForm.prototype.unitChangeMeasure = function (element) {
    element = $(element)
    element.parent().parent().children("span").replaceWith(
        element.children("span").first().clone()
    )
    this.unitListOpen(element.parent().parent())
    //this.unitListMark(element)
}
SymmetronFilterForm.prototype.unitRecalculate = function (inputWrapperNode) {
    inputWrapperNode = $(inputWrapperNode);

    this.filterInput(inputWrapperNode[0].querySelector(".range-value__select"));

    var inputCalc = inputWrapperNode.children("input[type='hidden']").first()
    var inputUser = inputWrapperNode.children("input[type='text']").first()
    var unitId =  inputWrapperNode.find(".range-value__select > span").first().attr("unit-id")
    var code = inputWrapperNode.parents(".catalog-filter__list > .list__wrapper").first().attr("itemid")

    // var value = inputUser.val();
    var value = inputUser[0].value;


    value = /(\-{0,1}\d+\.?\d*|\-{0,1}\d*\.\d*)/.exec(value);

    if (value === null) {
        value = ""
    } else {
        value = value[0]
    }

    // inputUser.val(value);
    inputUser[0].value = value;
    inputUser[0].setAttribute('value', value);

    if (value === undefined || value.trim() === "") {
        value = "";
    } else {
        value = parseFloat(value) * this.unit_storage[code].list[unitId].MULTIPLIER;

	function numberToString(num)
	{
	    let numStr = String(num);

	    if (Math.abs(num) < 1.0)
	    {
	        let e = parseInt(num.toString().split('e-')[1]);
	        if (e)
	        {
	            let negative = num < 0;
	            if (negative) num *= -1
	            num *= Math.pow(10, e - 1);
	            numStr = '0.' + (new Array(e)).join('0') + num.toString().substring(2);
	            if (negative) numStr = "-" + numStr;
	        }
	    }
	    else
	    {
	        let e = parseInt(num.toString().split('+')[1]);
	        if (e > 20)
	        {
	            e -= 20;
	            num /= Math.pow(10, e);
	            numStr = num.toString() + (new Array(e + 1)).join('0');
	        }
	    }

	    return numStr;
	}

	value = numberToString(value);
/*
        if (value <= 0) {
            value = "";
        } else {
            var degree = Math.floor(Math.log10(value));
            var step = 0;
            if (degree < 0)
            {
                degree = 2 * 3 - degree + (degree % 3)
                var result = 0;
                var digit;
                while (degree >= step++) {
                    digit = Math.floor(value * 10 + 0.0001);
                    value = value * 10 - digit;
                    result = result * 10 + 9 - digit;
                }
                value = -(result + 1);
            }
        }
*/
    }

    //inputCalc.val(value).change();
    inputCalc[0].value = value;
    inputCalc[0].setAttribute('value', value);
    inputCalc.change();
}
SymmetronFilterForm.prototype.unitPrepareInputText = function (input, change) {
    // var value = $(input).val();
    var value = $(input)[0].value;

    /* Clear all (!) whitespace charaacters */
    value = value.replace(/\s*/g, "")
    /* Swap "," to "." */
    value = value.replace(/,/g, ".")

    var unit = value.replace(/(\-{0,1}\d+\.?\d*|\-{0,1}\d*\.\d*)/, "");
    value = /(\-{0,1}\d+\.?\d*|\-{0,1}\d*\.\d*)/.exec(value);

    /* Get relevant parts of string */
    //value = /(\d*\.?\d*)\D*/.exec(value)

    if (value === null) {
        value = ""
    } else {
        value = value[0]
    }

    //var digit = /^(\d+\.?\d*|\d*\.\d*)/.exec(value)

    //if (digit === null) {
    //    digit = ""
    //} else {
    //    digit = digit[0]
    //}
    //var unit = value.replace(/[\d\.]*/g, "")

    /* Prettify value format*/
    //if (!change) {
    //    if (unit !== "") {
    //        digit = digit.replace(/\.$/, "")
    //        value = digit + unit
    //        value = value.replace(/([\d\.]+)([^\d\.]+)/g, "$1 $2")
    //    } else if (digit !== "" && digit.search(/\.$/) === -1) {
    //        value = digit
    //        if ($(input).val().search(new RegExp('^' + digit + ' +$')) === 0)
    //            value += " ";
    //    }
    //} else {
    //    value = digit
    //}

    if(unit !== ""){
	    value = value + " " + unit;
    }

    // $(input).val(value)
    $(input)[0].value = value;
    $(input)[0].setAttribute('value', value);

    /* Find unit id by name */
    var code = $(input).parents(".catalog-filter__list > .list__wrapper").first().attr("itemid")
    var unitId = this.unit_storage[code].base;
    var found = false;
    if (unit.length > 0) {
        var list = this.unit_storage[code].list

		if(list[unitId]){
			var baseList = list[unitId]["NAMES"];
			var regex;

			baseList[baseList.length] = "";
			for (var addit = 0; addit < baseList.length; addit++) {
				regex = new RegExp('^' + unit + baseList[addit])
				if (found) break;
				for (var i in list) {
					if (found) break;
					for (var j = 0; j < list[i]["NAMES"].length; j++) {
						if (list[i]["NAMES"][j].search(regex) === 0) {
							unitId = i;
							found = true;
							break;
						}
					}
				}
			}
		}else{
			var regex;
			regex = new RegExp('^' + unit)
			for (var i in list) {
				if (found) break;
				for (var j = 0; j < list[i]["NAMES"].length; j++) {
					if (list[i]["NAMES"][j].search(regex) === 0) {
						unitId = i;
						found = true;
						break;
					}
				}
			}
		}
    }

    /* Set found unit */
    if (!change)
    //if (found)
    {
        var unitNode = $(input).parent().find(".range-option > span[unit-id='" + unitId + "']").parent()
        this.unitChangeMeasure(unitNode);
    }
}