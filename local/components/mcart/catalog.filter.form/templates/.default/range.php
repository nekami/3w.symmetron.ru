<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="range-inputs">
    <?foreach (["MIN", "MAX"] as $side):?>
    <?if (!isset($arParams["VALUES"][$side]["HTML_VALUE"])) {
            $arParams["VALUES"][$side]["HTML_VALUE"] = $arParams["VALUES"][$side]["VALUE"];
        }?>
    <div class="range-wrapper">
        <span><?=Loc::GetMessage("MCART_CATALOG_FILTER_FORM_RANGE_".$side."_TEXT")?></span>
        <div class="range-input__wrapper <?=$arParams["RANGE_TYPE"]?>">
            <input type="text" id="<?= $arParams["VALUES"][$side]["CONTROL_ID"] ?>"
                   name="<?= $arParams["VALUES"][$side]["CONTROL_NAME"] ?>"
                   value="<?= $arParams["VALUES"][$side]["HTML_VALUE"] ?>">
        </div>
    </div>
    <?endforeach;?>
</div>
