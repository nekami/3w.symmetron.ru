//(function() {
//    'use strict';
//var BX = window.BX;
//if (BX.Mcart === undefined) BX.Mcart = {};

//if (BX.Mcart.CatalogQuantity === true) return;
//BX.Mcart.CatalogQuantity = true;

CatalogQuantityItemDetail = function (item, collections, itemIds, detailPage, isAuthorized, objDel) {
    this.item = item
    this.collections = collections
    this.itemIds = itemIds
    this.detailPage = detailPage
    this.isAuthorized = isAuthorized
    this.objDel = objDel
    this.bind()
}

CatalogQuantityItemDetail.prototype.bind = function () {
    var _this = this

    $(document).ready(function () {
        if ($(window).width() <= 768 && $('.card-sidebar').length) {
            $('.card-sidebar').stickybits({useStickyClasses: true})

            $('.close-sidebar').on('click tap', function () {
                if ($(this).parent().parent().hasClass('showFull')) {
                    $(this).parent().parent().removeClass('showFull')
                }
                $(".inCartMess").css("display", "none");
            })
        }

        /*if (_this.checkStickyButton(element.ELEMENT_ID)) {
            _this.openStickyButton(element.ELEMENT_ID)
            return;
        }
        return
        console.log("1111111")
        */
    })

    //$(".add-to-cart").on("click", function (ev) {
    //    console.log($(this))
    //})

    if (document.getElementById(_this.itemIds.QUANTITY_BLOCK)) {
        document.getElementById(_this.itemIds.QUANTITY_DOWN_ID).onclick = function (e) {
            _this.changeCount(e.target, e.target.innerHTML, _this.item);
        }

        document.getElementById(_this.itemIds.QUANTITY_UP_ID).onclick = function (e) {
            _this.changeCount(e.target, e.target.innerHTML, _this.item);
        }

        document.getElementById(_this.itemIds.QUANTITY_ID).onchange = function (e) {
            _this.changeCount(e.target, e.target.value, _this.item);
        }
    }

    $("#" + _this.itemIds.NO_QUANTITY).on('click', function () {
        _this.addToBaskItem(_this.item, _this.detailPage, 'request_missing')
    })


    $("#" + _this.itemIds.QUANTITY_REQ_BUTTON).on('click', function () {
        _this.addToBaskItem(_this.item, _this.detailPage, 'request_quantity')
    })


    $("#" + _this.itemIds.QUANTITY_IN_BASKET_BUTTON).on('click', function () {
        _this.changeQuantityMess(_this.item)
    })


    $("#" + _this.itemIds.EDIT_QUANTITY_IN_BASKET_BUTTON).on('click', function () {
        _this.addToBaskItem(_this.item, _this.detailPage, 'update')
    })


    $("#" + _this.itemIds.ADD_BASKET_LINK).on('click', function () {
        _this.addToBaskItem(_this.item, _this.detailPage, 'add')
    })

    //for mobile
    $("#" + _this.itemIds.ANALOG_BUTTON).on('click', function (ev) {
        if (_this.checkStickyButton(_this.item.ELEMENT_ID + "_avalibilty")) {
            _this.openStickyButton(_this.item.ELEMENT_ID + "_avalibilty")
            ev.preventDefault()
            return;
        }
    })

    $("#" + _this.itemIds.QUANTITY_REQ_BASKET).on('click', function (ev) {
        if (_this.checkStickyButton(_this.item.ELEMENT_ID + "_avalibilty")) {
            _this.openStickyButton(_this.item.ELEMENT_ID + "_avalibilty")
            ev.preventDefault()
            return;
        }
    })


    document.getElementById(_this.itemIds.MODAL_ID).onclick = function () {
        _this.getRequestModal(_this.item.ELEMENT_ID)
    }

    if (document.getElementById(_this.itemIds.QUANTITY_BLOCK_MODAL)) {
        document.getElementById(_this.itemIds.QUANTITY_BLOCK_MODAL_UP).onclick = function (e) {
            _this.changeCountModal(e.target, e.target.innerHTML, _this.item);
        }

        document.getElementById(_this.itemIds.QUANTITY_BLOCK_MODAL_DOWN).onclick = function (e) {
            _this.changeCountModal(e.target, e.target.innerHTML, _this.item);
        }

        document.getElementById(_this.itemIds.QUANTITY_IN_MODAL).onchange = function (e) {
            _this.changeCountModal(e.target, e.target.value, _this.item);
        }
    }


    document.getElementById(_this.itemIds.QUANTITY_REQ_BUTTON_MODAL).onclick = function () {
        _this.addToBaskItemModal(_this.item, _this.detailPage, 'request_quantity');
    }


    $("#" + _this.itemIds.REQUEST_LINK_MULT).on('click', function () {

        var prodId = _this.item.ELEMENT_ID

        var quantityDom = document.getElementById(prodId + "_quantity")
        quantity = quantityDom ? quantityDom.value : 1

        var arData = {
            id: parseInt(prodId),
            price: document.getElementById(prodId + "_price_digit").innerHTML,
            quant: quantity,
            name: document.getElementById("name_prod_" + prodId).innerHTML,
            url: _this.detailPage,
            action: 'request_multyplicity',
            example: "N"
        }

        _this.addToBasket(arData, 'REQ_MULT')
    })


    document.getElementById(_this.itemIds.FAVORITES).onclick = function () {
        if (_this.isAuthorized)
            _this.addToFavs()
        else
            _this.noAuth()
    }

    $("#" + _this.itemIds.ADD_EXAMPLE_LINK).on('click', function (e) {
        //if(_this.isAuthorized)
        _this.addToBaskExample(e.target, _this.detailPage)
        //else
        //    _this.noAuth()
    })

    //_this.checkBasket(_this.item.ELEMENT_ID)
    /*$("#" + _this.QUANTITY_BLOCK).on("click", ".quantity-button", function () {
        BX.Mcart.changeCount(this, $(this).html(), <?=CUtil::PhpToJSObject($temp)?>);
    })
    $("#" + _this.QUANTITY_BLOCK).on("change", "input", function () {
        BX.Mcart.changeCount(this, $(this).val(), <?=CUtil::PhpToJSObject($temp)?>);
    })*/
}


CatalogQuantityItemDetail.prototype.addToFavs = function () {
    var prodId = this.item.ELEMENT_ID
    var element = document.getElementById(this.itemIds.FAVORITES)
    BX.ajax.get("/ajax.php?id=" + prodId + "&action=fav",
        function (data) {
            data = JSON.parse(data)
            for (var i = 0; i < element.childNodes.length; i++) {

                if (element.childNodes[i].className == "fav_status") {
                    if (data.active) {
                        element.childNodes[i].innerHTML = BX.message("MCART_CATALOG_QUANTITY_ITEM_FAVORITE_Y");
                    } else {
                        element.childNodes[i].innerHTML = BX.message("MCART_CATALOG_QUANTITY_ITEM_FAVORITE_N");
                    }
                    break;
                }
            }
            element.classList.toggle("act");
        }
    );
}

CatalogQuantityItemDetail.prototype.noAuth = function () {
    BX.Mcart.modals.result_off.open([
        BX.message("MCART_CATALOG_QUANTITY_ERROR"),
        BX.message("MCART_CATALOG_QUANTITY_NO_AUTH")
    ]);
}

CatalogQuantityItemDetail.prototype.toBasketAlready = function (code) {
    BX.Mcart.modals.result_off.open([
        BX.message("MCART_CATALOG_QUANTITY_NOTIFY"),
        BX.message("MCART_CATALOG_QUANTITY_" + code),
        {
            url: "/cart/",
            text: BX.message("MCART_CATALOG_QUANTITY_BASKET")
        }
    ]);
}

CatalogQuantityItemDetail.prototype.checkStickyButton = function (element) {
    return $(window).width() <= 768 && !$('.showFull').length &&
        $("#" + element).parents(".card-sidebar").length > 0;

}

CatalogQuantityItemDetail.prototype.openStickyButton = function (element) {
    $("#" + element).parents(".card-sidebar").first().addClass('showFull')
}

CatalogQuantityItemDetail.prototype.addToBasket = function (arData, code) {
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        'ecommerce': {
            'add': {
                'products': [{
                    'name': this.item.NAME,
                    'id': Number(arData.id),
                    'price': Number(arData.price ? arData.price : '0'),
                    'brand': this.item.BRAND,
                    'category': this.item.BREAD,
                    'quantity': Number(arData.quant)
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',
    });

    var _this = this
    BX.ajax.get(
        BX.message("MCART_CATALOG_QUANTITY_PATH") + "/ajax_add.php",
        arData,
        function (data) {
            if (data > 0) {
                _this.toBasketAlready(code);
            }
        }
    );
}


CatalogQuantityItemDetail.prototype.addToBaskItem = function (element, path, action) {
    var _this = this
    var prodId = element.ELEMENT_ID

    if (_this.checkStickyButton(prodId + "_avalibilty")) {
        _this.openStickyButton(prodId + "_avalibilty")
        return;
    }

    var quantityDom = document.getElementById(prodId + "_quantity")
    quantity = quantityDom ? quantityDom.value : 1
    quantity = !quantity ? 1 : quantity

    var arData = {
        id: parseInt(prodId),
        price: document.getElementById(prodId + "_price_digit").innerHTML,
        quant: quantity,
        name: document.getElementById("name_prod_" + prodId).innerHTML,
        url: path,
        action: action,
        example: "N"
    }

    var add_basket_link = document.getElementById(prodId + '_add_basket_link')
    if (add_basket_link) {
        add_basket_link.style.display = "none"
    }

    var quantity_in_basket_button = document.getElementById(prodId + '_quantity_in_basket_button')
    if (quantity_in_basket_button) {
        quantity_in_basket_button_text = document.getElementById(prodId + '_quantity_in_basket_button_text')
        quantity_in_basket_button_text.innerHTML = quantity + " "
            + _this.objDel.declinationNum(_this.collections.UNITS, parseInt(quantity)) + " "
            + BX.message("MCART_CATALOG_QUANTITY_IN_BASKET")
    }

    _this.addToBasket(arData, "ITEM_ADD")
    document.getElementById(prodId + '_avalibilty').setAttribute("data-in-basket", quantity)
    //element.attributes["is_in_basket"].value = "1";
    //if(action == "request_quantity" || action == "request_missing") {
    if (action == "request_quantity") {
        element.REQUEST = "QUANTITY"
        priceId = document.getElementById(prodId + "_price_id")
        if (priceId)
            priceId.parentNode.style.display = "none"

        priceTotal = document.getElementById(prodId + "_price_total")
        if (priceTotal)
            priceTotal.parentNode.style.display = "none"

        priceDigit = document.getElementById(prodId + "_price_digit")
        if (priceDigit)
            priceDigit.parentNode.style.display = "none"

        priceDigit = document.getElementById(prodId + "_quant_block")
        if (priceDigit)
            priceDigit.style.display = "none"

    }
    if (action == "request_missing") {
        element.REQUEST = "MISSING"
    }
    //}
    _this.checkMaxQuantity(element, quantity)
}

CatalogQuantityItemDetail.prototype.addToBaskPrice = function (element, path) {
    var _this = this

    if (_this.checkStickyButton(element)) {
        _this.openStickyButton(element)
        return;
    }

    var prodId = $(element).attr("data-id");
    var arData = {
        id: prodId,
        quant: 1,
        name: document.getElementById("name_prod_" + prodId).innerHTML,
        url: path,
        example: "N"
    };
    _this.addToBasket(arData, "PRICE" + "_ADD")
}

CatalogQuantityItemDetail.prototype.addToBaskExample = function (element, path) {
    var _this = this

    var prodId = $(element).attr("data-id");
    var arData = {
        id: prodId,
        quant: 1,
        name: document.getElementById("name_prod_" + prodId).innerHTML,
        url: path,
        example: "Y"
    };
    _this.addToBasket(arData, "EXAMPLE" + "_ADD")

}

CatalogQuantityItemDetail.prototype.checkBasket = function (elementID) {
    BX.ajax.get(
        BX.message("MCART_CATALOG_QUANTITY_PATH") + "/ajax.php",
        {ELEMENT_ID: elementID},
        function (data) {
            if (data !== undefined) {
                data = $.parseJSON(data)
                //$("#" + elementID + "_add_basket_link").attr("is_in_basket", data.BASKET_QUANTITY);
                //$("#" + elementID + "_add_example_link").attr("is_in_basket", data.BASKET_EXAMPLE);
            }
        }
    );
}

CatalogQuantityItemDetail.prototype.findCurrentPrice = function (prices, quantity) {
    for (var p in prices) {
        if (prices[p].QUANTITY_FROM == "")
            return p;

        if (parseFloat(prices[p].QUANTITY_FROM) <= parseFloat(quantity) &&
            (parseFloat(prices[p].QUANTITY_TO) >= parseFloat(quantity) ||
                !(parseFloat(prices[p].QUANTITY_TO) > 0)))
            return p;
    }

    for (var p in prices) {
        if (parseFloat(prices[p].QUANTITY_TO) >= parseFloat(quantity) ||
            !(parseFloat(prices[p].QUANTITY_TO) > 0))
            return p;
    }

}

CatalogQuantityItemDetail.prototype.changeCountModal = function (element, operation, data) {
    element = $(element);
    var quantity = $("#" + data.ELEMENT_ID + "_quantity_in_modal");
    //var item_in = $("#" + data.ELEMENT_ID + "_add_basket_link").attr("is_in_basket") == "1";

    var newVal;

    if (operation == "+") {
        newVal = parseFloat(quantity.val()) + parseFloat(data.RATIO_QUANTITY);
    } else if (operation == "-") {
        newVal = parseFloat(quantity.val()) - parseFloat(data.RATIO_QUANTITY);
    } else {
        newVal = parseFloat(quantity.val());
    }

    if (data.CATALOG_CAN_BUY_ZERO == "N" && newVal > parseFloat(data.CATALOG_QUANTITY) && data.CATALOG_AVAILABLE == "Y") {
        newVal = parseFloat(data.CATALOG_QUANTITY);
    }

    if (newVal < parseFloat(data.MIN_QUANTITY)) {
        newVal = parseFloat(data.MIN_QUANTITY);
    }

    newVal = Math.ceil(newVal / parseFloat(data.RATIO_QUANTITY) - 0.000000001) * parseFloat(data.RATIO_QUANTITY);
    if (newVal < parseFloat(data.RATIO_QUANTITY)) {
        newVal = parseFloat(data.RATIO_QUANTITY);
    }
    newVal = Math.floor(newVal * 1000) / 1000;

    quantity.val(newVal);
}

CatalogQuantityItemDetail.prototype.changeCount = function (element, operation, data) {
    var _this = this

    element = $(element);
    var quantity = $("#" + data.ELEMENT_ID + "_quantity");
    //var item_in = $("#" + data.ELEMENT_ID + "_add_basket_link").attr("is_in_basket") == "1";
    var quantVal = quantity.val()
    quantVal = quantVal ? quantVal : 1
    var newVal;
    if (operation == "+") {
        newVal = parseFloat(quantVal) + parseFloat(data.RATIO_QUANTITY);
    } else if (operation == "-") {
        newVal = parseFloat(quantVal) - parseFloat(data.RATIO_QUANTITY);
    } else {
        newVal = parseFloat(quantVal);
    }

    if (data.CATALOG_CAN_BUY_ZERO == "N" && newVal > parseFloat(data.CATALOG_QUANTITY)) {
        newVal = parseFloat(data.CATALOG_QUANTITY);
    }
    if (newVal < parseFloat(data.MIN_QUANTITY)) {
        newVal = parseFloat(data.MIN_QUANTITY);
    }

    newVal = Math.ceil(newVal / parseFloat(data.RATIO_QUANTITY) - 0.000000001) * parseFloat(data.RATIO_QUANTITY);
    if (newVal < parseFloat(data.RATIO_QUANTITY)) {
        newVal = parseFloat(data.RATIO_QUANTITY);
    }
    newVal = Math.floor(newVal * 1000) / 1000;
    if (data.PRICES.length > 0) {
        var currentPriceId = _this.findCurrentPrice(data.PRICES, newVal);
        var currentPrice = data.PRICES[currentPriceId];
        var total = BX.Currency.currencyFormat(currentPrice.BASE_PRICE * newVal, "RUB", true)

        quantity.val(newVal);
        $("#" + data.ELEMENT_ID + "_price_id").html(currentPriceId)
        $("#" + data.ELEMENT_ID + "_price_total").html(total)
        $("#" + data.ELEMENT_ID + "_price_digit").html(currentPrice.BASE_PRICE)
    }

    _this.currentPriceShow(data.ELEMENT_ID, newVal)
    _this.calculator(newVal)
    _this.checkMaxQuantity(data, newVal)
}

CatalogQuantityItemDetail.prototype.calculator = function (newVal) {
    var _this = this

    var calculator = document.getElementById(_this.itemIds.QUANTITY_CALCULATOR)
    if (!calculator)
        return

    if (newVal < _this.item.SYMPACKAGE || _this.item.RATIO_QUANTITY <= 1 || newVal > _this.item.CATALOG_QUANTITY) {
        calculator.style.display = "none"
    } else {
        var qUpac = parseInt(newVal / _this.item.SYMPACKAGE)
        var htmlCalculator = qUpac + " " + _this.objDel.declinationNum(_this.collections.PACKING, qUpac)

        var mod = newVal % _this.item.SYMPACKAGE
        if (mod > 0)
            htmlCalculator += "<br>" + mod + " " + _this.objDel.declinationNum(_this.collections.MEASURE, parseInt(mod))

        calculator.innerHTML = htmlCalculator

        calculator.style.display = ""
    }

}

CatalogQuantityItemDetail.prototype.currentPriceShow = function (elementId, quantity) {

    var arPrises = document.getElementById(elementId + "_all_prices")

    if (arPrises) {

        arPrises = arPrises.childNodes

        for (var i in arPrises) {

            if (arPrises[i].nodeName !== "DIV")
                continue

            var quantityFrom = arPrises[i].getAttribute("data-from")
            var quantityTo = arPrises[i].getAttribute("data-to")

            if (quantityTo > 0) {
                if ((quantityFrom <= quantity) && (quantity <= quantityTo))
                    arPrises[i].style = "font-weight: bold"
                else
                    arPrises[i].style = "font-weight: normal"
            } else {
                if (quantityFrom <= quantity)
                    arPrises[i].style = "font-weight: bold"
                else
                    arPrises[i].style = "font-weight: normal"
            }

        }

    }
}

CatalogQuantityItemDetail.prototype.checkMaxQuantity = function (element, quantity) {

    var _this = this

    var addBasket = document.getElementById(element.ELEMENT_ID + "_add_basket_link")
    var requestQuantity = document.getElementById(element.ELEMENT_ID + "_quantity_request_button")
    var quantityInBasket = document.getElementById(element.ELEMENT_ID + "_quantity_in_basket_button")
    var editQuantityInBasket = document.getElementById(element.ELEMENT_ID + "_edit_quantity_in_basket_button")
    var noQuantityMess = document.getElementById(element.ELEMENT_ID + "_no_quantity_mess")

    var basket_quantity = document.getElementById(element.ELEMENT_ID + '_avalibilty').getAttribute("data-in-basket")

    if (element.REQUEST == "QUANTITY") {
        quantityRequestBasket = document.getElementById(element.ELEMENT_ID + "_quantity_request_basket")
        if (quantityRequestBasket)
            quantityRequestBasket.style.display = ""
        if (requestQuantity)
            requestQuantity.style.display = "none"

        if (noQuantityMess)
            noQuantityMess.style.display = "none"
        return false
    }

    if (element.REQUEST == "MISSING") {
        noQuantityButton = document.getElementById(element.ELEMENT_ID + "_no_quantity")
        if (noQuantityButton)
            noQuantityButton.style.display = "none"
        missingReqBasket = document.getElementById(element.ELEMENT_ID + "_missing_req_basket")
        if (missingReqBasket)
            missingReqBasket.style.display = ""

        return false
    }

    document.getElementById(element.ELEMENT_ID + "_quantity_in_basket_button_mess").style.display = "none"
    if (basket_quantity > 0
        //&& parseInt(basket_quantity) <= parseInt(element.CATALOG_QUANTITY)
        && parseInt(quantity) <= parseInt(element.CATALOG_QUANTITY)) {
        requestQuantity.style.display = "none"
        if (noQuantityMess)
            noQuantityMess.style.display = "none"
        if (basket_quantity == quantity) {
            quantityInBasket.style.display = ""
            editQuantityInBasket.style.display = "none"
            editQuantityInBasket.setAttribute("data-sign", "")
        } else if (basket_quantity < quantity) {
            quantityInBasket.style.display = "none"
            editQuantityInBasket.style.display = ""
            editQuantityInBasket.setAttribute("data-sign", "+")
            addVal = quantity - basket_quantity
            editQuantityInBasket.innerHTML = BX.message("MCART_CATALOG_QUANTITY_ADD_MORE_PROD") + " " + addVal
                + " " + _this.objDel.declinationNum(_this.collections.PRODUCTS, parseInt(addVal))
        } else if (basket_quantity > quantity) {
            quantityInBasket.style.display = "none"
            editQuantityInBasket.style.display = ""
            editQuantityInBasket.setAttribute("data-sign", "-")
            addSub = basket_quantity - quantity
            editQuantityInBasket.innerHTML = BX.message("MCART_CATALOG_QUANTITY_SUB_MORE_PROD") + " " + addSub
                + " " + _this.objDel.declinationNum(_this.collections.PRODUCTS, parseInt(addSub))
        }

        return false;
    }

    editQuantityInBasket.style.display = "none"
    quantityInBasket.style.display = "none"
    if (element.CATALOG_QUANTITY >= quantity) {
        if (noQuantityMess)
            noQuantityMess.style.display = "none"
        if (addBasket)
            addBasket.style.display = ""
        if (requestQuantity)
            requestQuantity.style.display = "none"
    } else {
        if (noQuantityMess)
            noQuantityMess.style.display = ""
        if (addBasket)
            addBasket.style.display = "none"
        if (requestQuantity)
            requestQuantity.style.display = ""

    }
}

CatalogQuantityItemDetail.prototype.changeQuantityMess = function (element) {
    _this = this
    if (_this.checkStickyButton(element.ELEMENT_ID + "_avalibilty")) {
        _this.openStickyButton(element.ELEMENT_ID + "_avalibilty")
        return;
    }
    document.getElementById(element.ELEMENT_ID + "_quantity_in_basket_button_mess").style.display = ""
}

CatalogQuantityItemDetail.prototype.getRequestModal = function (elementId) {
    quantity = document.getElementById(elementId + "_quantity")
    quantityModal = document.getElementById(elementId + "_quantity_in_modal")

    if (quantity && quantityModal) {
        quantityModal.value = quantity.value
    }
}

CatalogQuantityItemDetail.prototype.addToBaskItemModal = function (element, path, action) {
    var _this = this

    quantity = document.getElementById(element.ELEMENT_ID + "_quantity")
    quantityModal = document.getElementById(element.ELEMENT_ID + "_quantity_in_modal")

    quantityInBasketButton = document.getElementById(element.ELEMENT_ID + "_quantity_in_basket_button")

    if (quantityInBasketButton) {
        quantityInBasketButton.style.display = "none"
    }

    noQuantityButton = document.getElementById(element.ELEMENT_ID + "_no_quantity")

    if (noQuantityButton) {
        noQuantityButton.style.display = "none"
    }

    if (quantity && quantityModal) {
        quantity.value = quantityModal.value
        _this.addToBaskItem(element, path, action)
    }
}

//})();

