<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
\Bitrix\Main\UI\Extension::load("mcart.symmetrondeclination");
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
?>
<?
$arCollections = array(
    "UNITS"     => Loc::getMessage('MCART_CATALOG_QUANTITY_COLLECTION_UNITS_1'),
    "PRODUCTS"  => Loc::getMessage('MCART_CATALOG_QUANTITY_COLLECTION_PRODUCTS_1')
);

if(!function_exists("declineWordPosition")){
    function declineWordPosition($quantVal, $arCollection){

        $arPositionVariantsException = array(11, 12, 13, 14);

        $mod = $quantVal % 10;
        if(in_array($quantVal, $arPositionVariantsException))
            $posWord = $arCollection[5];
        else
            $posWord = $arCollection[$mod];

        return $posWord;
    }
}
//echo "<pre>";
//print_r($arResult);
//print_r($arParams);
//echo "</pre>";
$quantityNow = $arResult["BASKET_QUANTITY"] ? $arResult["BASKET_QUANTITY"] : $arResult['MIN_QUANTITY'];
$mainId = $arParams['ELEMENT_ID'];//$this->GetEditAreaId($arParams['ELEMENT_ID']);
$itemIds = array(
	'ID'                   => $mainId,
	'QUANTITY_ID'          => $mainId.'_quantity',
	'QUANTITY_DOWN_ID'     => $mainId.'_quant_down',
	'QUANTITY_UP_ID'       => $mainId.'_quant_up',
	'QUANTITY_MEASURE'     => $mainId.'_quant_measure',
	'QUANTITY_LIMIT'       => $mainId.'_quant_limit',
    'QUANTITY_BLOCK'       => $mainId.'_quant_block',

    'ALL_PRICES'           => $mainId.'_all_prices',

	'BASKET_ACTIONS_ID'    => $mainId.'_basket_actions',
	'ADD_BASKET_LINK'      => $mainId.'_add_basket_link',
	'ADD_EXAMPLE_LINK'     => $mainId.'_add_example_link',

	'QUANTITY_REQ_BASKET'  => $mainId.'_quantity_request_basket',
	'NO_QUANTITY'          => $mainId.'_no_quantity',

	'QUANTITY_REQ_BUTTON'  => $mainId.'_quantity_request_button',
	'QUANTITY_IN_BASKET_BUTTON' => $mainId.'_quantity_in_basket_button',
    'EDIT_QUANTITY_IN_BASKET_BUTTON' => $mainId.'_edit_quantity_in_basket_button',

	'PRICE_ID'             => $mainId.'_price_id',
	'PRICE_TOTAL'          => $mainId.'_price_total',
    'PRICE_DIGIT'          => $mainId.'_price_digit',
    "QUANTITY_CALCULATOR"  => $mainId.'_quantity_calculator',

    'AVALIBILTY'          => $mainId.'_avalibilty',
    'NO_QUANTITY_MESS'    => $mainId.'_no_quantity_mess',

    'MISSING_REQ_BASKET'  => $mainId.'_missing_req_basket',

    'MODAL_ID'            => $mainId.'_modal_id',
    'QUANTITY_BLOCK_MODAL'=> $mainId.'_quant_block_modal',
    'QUANTITY_IN_MODAL'   => $mainId.'_quantity_in_modal',
    'QUANTITY_BLOCK_MODAL_UP'   => $mainId.'_quantity_in_modal_up',
    'QUANTITY_BLOCK_MODAL_DOWN' => $mainId.'_quantity_in_modal_down',
    'QUANTITY_REQ_BUTTON_MODAL' => $mainId.'_quantity_request_button_modal',
    'FAVORITES'           => $mainId.'_favorites',

    'ANALOG_BUTTON' => $mainId."_analog_button",

    'REQUEST_LINK_MULT'        => $mainId.'_request_button_mult'
);
?>
<span id="<?=$itemIds["AVALIBILTY"]?>" class="avalibilty <?=$arParams["CATALOG_QUANTITY"] > 0 ? "" : "out"?>"
      data-in-basket="<?=$arResult["BASKET_QUANTITY"] ? $arResult["BASKET_QUANTITY"] : "0"?>"
      data-id="<?=$arParams["ELEMENT_ID"]?>">
    <?=$arResult["AVAILABLE_TEXT"]?>
</span>

<?$upacTitle = trim(strtolower($arParams["PACK_INFO"]["UPAK"]["VALUE"]));?>
<?$measure = trim(strtolower($arParams["ITEM_MEASURE"]["MEASURE_TITLE"])) ?: Loc::getMessage('MCART_CATALOG_QUANTITY_REQUEST_MEASURE_DEF');?>
<?$arParams["ITEM_MEASURE"]["MEASURE_TITLE"] = $measure;?>
<?$measureSymbol = $arParams["ITEM_MEASURE"]["SYMBOL"] ?: Loc::getMessage('MCART_CATALOG_QUANTITY_REQUEST_MEASURE_MINI_DEF');?>

<div class="totalIn">
    <?if(!empty($arParams["PACK_INFO"]["SYMPACKAGE"]) && !empty($arParams["PACK_INFO"]["UPAK"])):?>
        <?$arParams["PACK_INFO"]["UPAK"]["VALUE"] = $upacTitle;?>
        <?=$arParams["PACK_INFO"]["SYMPACKAGE"] . " "
        . $arParams["DECLINATION_OBJ"]->declinationNum($measure, (int)$arParams["PACK_INFO"]["SYMPACKAGE"])
        . " " . Loc::getMessage('MCART_CATALOG_QUANTITY_REQUEST_IN') . " "
        . $arParams["DECLINATION_OBJ"]->declinationIn($upacTitle)
        ?>
    <?else:?>
        <?=Loc::getMessage('MCART_CATALOG_QUANTITY_REQUEST_UNITY');?>
    <?endif;?>
</div>


<?if($arParams["CATALOG_AVAILABLE"] == "Y"):?>
    <?$arCurPrice = array();?>
	<div class="wholesale" id="<?=$itemIds["ALL_PRICES"]?>">
		<?foreach ($arParams["PRICES"] as $key => $arPrice):?>
			<?if(empty($arPrice["QUANTITY_FROM"])){
				$arPrice["QUANTITY_FROM"] = 0;
			}?>
            <?$stylePrice = "";?>

                <?if($arPrice["QUANTITY_FROM"] <= $quantityNow) {
                    $stylePrice = 'font-weight: bold;';

                    $arCurPrice = array("PRICE_ID" => $key, "PRICE" => $arPrice);;
                }?>
				<div class="quantity" style="<?=$stylePrice?>"
                     data-from="<?=$arPrice["QUANTITY_FROM"]?>"
                     data-to="<?=$arPrice["QUANTITY_TO"]?>">
                    <?$quantityFrom = $arPrice["QUANTITY_FROM"] <= 9999 ? $arPrice["QUANTITY_FROM"] : number_format($arPrice["QUANTITY_FROM"], 0, "", " ")?>
					<span class="quantity-items"><?=
						Loc::getMessage('MCART_CATALOG_QUANTITY_ITEM_FROM')." "
						.$quantityFrom
						." " . $measureSymbol . '.'
					?></span>
					<span class="quantity-separator"></span>
					<span class="quantity-price"><?=$arPrice["PRINT_BASE_PRICE"] . "/" . $measureSymbol . ".";?></span>
				</div>
		<?endforeach;?>
	</div>


	<?/* Buttons to change quantity */?>
    <?if (count($arParams["PRICES"]) > 0 && !in_array($arResult["BASKET_REQUEST"], ["QUANTITY", "MULTIPLICITY"]) && !empty($arParams["CATALOG_QUANTITY"])):?>

        <div class="quantity-num" id="<?=$itemIds['QUANTITY_BLOCK']?>">
            <span class="quantity-button" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</span>

            <input id="<?=$itemIds['QUANTITY_ID']?>" type="number" min="1"
                   value="<?=$quantityNow?>"
                   name="quant_elems_<?=$arParams['ELEMENT_ID']?>">

            <span class="quantity-button" id="<?=$itemIds['QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</span>
        </div>
		<div class="noQuantity" id="<?=$itemIds["NO_QUANTITY_MESS"];?>" <?if($quantityNow <= $arParams["CATALOG_QUANTITY"]):?>style="display: none"<?endif;?>><?=Loc::getMessage("MCART_CATALOG_QUANTITY_THIS_QUANTITY_IS_NONE")?></div>
    <?else:?>
        <input style="display: none" id="<?=$itemIds['QUANTITY_ID']?>" type="number" min="1"
               value="<?=$quantityNow?>"
               name="quant_elems_<?=$arParams['ELEMENT_ID']?>">
    <?endif;?>


    <!-- calculator -->

    <?$calculatorShow = false;
    $quantityNow = $arResult["BASKET_QUANTITY"] ?: $arParams["ITEM_MEASURE_RATIO"];
    $quantityInPac = intval($arParams["PACK_INFO"]["SYMPACKAGE"]);
    if($quantityNow >= $arParams["PACK_INFO"]["SYMPACKAGE"] && $arParams["CATALOG_QUANTITY"] >= $quantityNow){
        $calculatorShow = $quantityInPac > 0 ? true : false;
    }?>
    <div id="<?=$itemIds['QUANTITY_CALCULATOR']?>"
         class="quantity-num__info quantity-num__instock"
         data-pac-quant="<?=$quantityInPac?>"
         data-pac-type="<?=$arParams["PACK_INFO"]["UPAK"]["VALUE"]?>"
         data-measure="<?=$arParams["ITEM_MEASURE"]["MEASURE_TITLE"]?>"
         <?if(!$calculatorShow):?>style="display: none" <?endif;?>
    >

        <?if($calculatorShow):?>

            <?$qUpac = (int)($quantityNow / $quantityInPac);?>
            <?=$qUpac . " " . $arParams["DECLINATION_OBJ"]->declinationNum($upacTitle, $qUpac);?>
            <?if($mod = $quantityNow % $quantityInPac):
                echo "<br>";
                echo $mod . " " . $arParams["DECLINATION_OBJ"]->declinationNum($measure, $mod);
            endif;?>
        <?endif;?>
    </div>

    <!-- end calculator -->


    <?/*price*/?>
    <?if (count($arParams["PRICES"]) == 0 || $arResult["BASKET_REQUEST"] === "QUANTITY" || empty($arParams["CATALOG_QUANTITY"])):?>
        <div class="price" style="display: none"><span id="<?=$itemIds['PRICE_TOTAL']?>">0</span></div>
        <div class="price" style="display: none"><span id="<?=$itemIds['PRICE_DIGIT']?>">0</span></div>
        <div class="price" style="display: none"><span id="<?=$itemIds['PRICE_ID']?>">0</span></div>
    <?else:?>
        <?/*foreach ($arParams["PRICES"] as $key => $price):?>
            <?if ($price["QUANTITY_TO"] > 0 && $price["MIN_QUANTITY"] > $price["QUANTITY_TO"])
                continue;*/?>
            <div class="price"><span id="<?=$itemIds['PRICE_TOTAL']?>">
                    <?=CCurrencyLang::CurrencyFormat($arCurPrice["PRICE"]["BASE_PRICE"] * $quantityNow, $arCurPrice["PRICE"]["CURRENCY"], true);?>
                </span></div>
            <div class="price_digit" style="display: none"><span id="<?=$itemIds['PRICE_DIGIT']?>">
                    <?=$arCurPrice["PRICE"]["BASE_PRICE"];?>
                </span></div>
            <div class="price_id" style="display: none"><span id="<?=$itemIds['PRICE_ID']?>"><?=$arCurPrice["PRICE_ID"];?></span></div>
            <?/*break;?>
        <?endforeach;*/?>
    <?endif;?>

    <?/* Buy button */?>
	<form  id="<?=$itemIds['BASKET_ACTIONS_ID'];?>" onkeydown="if(event.keyCode==13){return false;}">
        <?$action = (count($arParams["PRICES"]) == 0 ? "addToBaskPrice" : "addToBaskItem")?>
        <div class="cb"></div>
        <?
            if($arResult["BASKET_REQUEST"] !== "MISSING" && $arResult["BASKET_REQUEST"] !== "QUANTITY"
                && !empty($arParams["CATALOG_QUANTITY"]) && !empty($arParams["CATALOG_AVAILABLE"])
                && intval($arResult["BASKET_QUANTITY"]) < 1 && !empty($arParams["PRICES"])
                && intval($arParams["ITEM_MEASURE_RATIO"]) <= intval($arParams["CATALOG_QUANTITY"])) {
                $basketButtonAdd = true;
            }
        ?>
        <?/*MISSING request basket button*/?>
        <a class="add-to-cart" id="<?=$itemIds['MISSING_REQ_BASKET']?>"
            data-id="<?=$arParams["ELEMENT_ID"]?>"
            <?=$arResult["BASKET_REQUEST"] !== "MISSING" ? 'style="display: none"' : ''?>
            href="javascript:void(0)"
            rel="nofollow"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_BUTTON_REQ_MISSING")?></a>

        <?/*QUANTITY request basket button*/?>
        <a class="add-to-cart req" id="<?=$itemIds['QUANTITY_REQ_BASKET']?>"
           data-id="<?=$arParams["ELEMENT_ID"]?>"
            <?=!in_array($arResult["BASKET_REQUEST"], ["QUANTITY", "MULTIPLICITY"]) ? 'style="display: none"' : ''?>
           href="javascript:void(0)"
           rel="nofollow"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_BUTTON_REQ_QUANTITY_BASKET")?></a>


        <?/*no quantity button*/?>
        <a class="add-to-cart" id="<?=$itemIds['NO_QUANTITY']?>"
           data-id="<?=$arParams["ELEMENT_ID"]?>"
           <?=!empty($arParams["CATALOG_QUANTITY"]) && !empty($arParams["PRICES"]) || !empty($arResult["BASKET_REQUEST"]) ? 'style="display: none"' : ''?>
           href="javascript:void(0)"
           rel="nofollow"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_BUTTON_EMPTY")?></a>

        <?/*quantity request button*/?>
        <a class="btn reqQuant" id="<?=$itemIds['QUANTITY_REQ_BUTTON']?>"
           <?=intval($arParams["CATALOG_QUANTITY"]) >= intval($arResult["BASKET_QUANTITY"])
               && intval($arParams["ITEM_MEASURE_RATIO"]) <= intval($arParams["CATALOG_QUANTITY"])
               || !empty($arResult["BASKET_REQUEST"])
               || intval($arParams["CATALOG_QUANTITY"]) == 0 ? 'style="display: none"' : ''?>
           data-id="<?=$arParams["ELEMENT_ID"]?>"
           href="javascript:void(0)"
           rel="nofollow"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_BUTTON_REQ_QUANTITY")?></a>

        <?/*quantity in basket button*/?>
        <a class="btn inCart" id="<?=$itemIds['QUANTITY_IN_BASKET_BUTTON']?>"
            <?=intval($arResult["BASKET_QUANTITY"]) < 1
            || intval($arResult["BASKET_QUANTITY"]) > intval($arParams["CATALOG_QUANTITY"])
            || !empty($arResult["BASKET_REQUEST"]) ? 'style="display: none"' : ''?>
           data-id="<?=$arParams["ELEMENT_ID"]?>"
           href="javascript:void(0)"
           rel="nofollow">

            <span id="<?=$itemIds['QUANTITY_IN_BASKET_BUTTON']?>_text"><?=$arResult["BASKET_QUANTITY"] . " " .
                $arParams["DECLINATION_OBJ"]->declinationNum($arCollections["UNITS"], (int)$arResult["BASKET_QUANTITY"]) .
                " " . Loc::getMessage("MCART_CATALOG_QUANTITY_IN_BASKET")?>
            </span><svg class="inCart-i" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400.67 391.32"><path  d="M428.4,310.9H198.8l-5-19.8,237.3-33.2a17,17,0,0,0,13.2-13.7l19.4-106.9a17.36,17.36,0,0,0-3.7-14,16.86,16.86,0,0,0-13.1-6.1H153.6L144.3,77a17.1,17.1,0,0,0-16.6-13.2H80.3a17,17,0,0,0,0,34h33.9l54.4,233.8a17.1,17.1,0,0,0,16.6,13.2H428.3a17,17,0,1,0,.1-33.9ZM160.8,147H417.6a8.11,8.11,0,0,1,8,9.2l-9.7,68.3a9.36,9.36,0,0,1-7.4,7.8L187.3,261.1Z" transform="translate(-63.3 -26.98)"/><path  d="M187,365.9a26.2,26.2,0,1,0,26.2,26.2A26.23,26.23,0,0,0,187,365.9Z" transform="translate(-63.3 -26.98)"/><path  d="M386,365.9a26.2,26.2,0,1,0,26.2,26.2A26.16,26.16,0,0,0,386,365.9Z" transform="translate(-63.3 -26.98)"/><path  d="M394.09,201l-4.39-33.73h-193l7.61,58.44a14.17,14.17,0,0,0,15.88,12.22l161.69-21A14.18,14.18,0,0,0,394.09,201Zm-21-161.69A14.17,14.17,0,0,0,357.16,27.1l-161.69,21A14.18,14.18,0,0,0,183.25,64l4.33,33.25h193Z" transform="translate(-63.3 -26.98)"/></svg></a>
		
        <span class="inCartMess" id="<?=$itemIds['QUANTITY_IN_BASKET_BUTTON']?>_mess" style="display: none">
			
            <?=Loc::getMessage("MCART_CATALOG_QUANTITY_YOU_CAN_CHANGE_QUANTITY")?>
			
			</span>

        <?/*edit quantity in basket button*/?>
        <a class="add-to-cart" id="<?=$itemIds['EDIT_QUANTITY_IN_BASKET_BUTTON']?>"
           style="display: none"
           data-id="<?=$arParams["ELEMENT_ID"]?>"
           href="javascript:void(0)"
           data-sign=""
           rel="nofollow"></a>

        <?/*add basket button*/?>
        <a class="add-to-cart" id="<?=$itemIds['ADD_BASKET_LINK']?>"
           <?=!$basketButtonAdd ? 'style="display: none"' : ''?>
           data-id="<?=$arParams["ELEMENT_ID"]?>"
           href="javascript:void(0)"
           rel="nofollow"><svg class="cart-i" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400.67 354.5"><path class="cls-1" d="M428.4,310.9H198.8l-5-19.8,237.3-33.2a17,17,0,0,0,13.2-13.7l19.4-106.9a17.36,17.36,0,0,0-3.7-14,16.86,16.86,0,0,0-13.1-6.1H153.6L144.3,77a17.1,17.1,0,0,0-16.6-13.2H80.3a17,17,0,0,0,0,34h33.9l54.4,233.8a17.1,17.1,0,0,0,16.6,13.2H428.3a17,17,0,1,0,.1-33.9ZM160.8,147H417.6a8.11,8.11,0,0,1,8,9.2l-9.7,68.3a9.36,9.36,0,0,1-7.4,7.8L187.3,261.1Z" transform="translate(-63.3 -63.8)"/><path class="cls-1" d="M187,365.9a26.2,26.2,0,1,0,26.2,26.2A26.23,26.23,0,0,0,187,365.9Z" transform="translate(-63.3 -63.8)"/><path class="cls-1" d="M386,365.9a26.2,26.2,0,1,0,26.2,26.2A26.16,26.16,0,0,0,386,365.9Z" transform="translate(-63.3 -63.8)"/></svg><?=Loc::getMessage("MCART_CATALOG_QUANTITY_BUTTON_BUY")?></a>
	</form>
<?else:?>

    <?/* Buy button */?>
    <form  id="<?=$itemIds['BASKET_ACTIONS_ID'];?>" onkeydown="if(event.keyCode==13){return false;}">
        <?$quantityNow = $arResult["BASKET_QUANTITY"] ?: $arParams["ITEM_MEASURE_RATIO"];?>
        <div class="price" style="display: none"><span id="<?=$itemIds['PRICE_DIGIT']?>">0</span></div>
        <input id="<?=$itemIds['QUANTITY_ID']?>" type="number" min="1"
               value="<?=$quantityNow?>"
               name="quant_elems_<?=$arParams['ELEMENT_ID']?>"
                style="display: none">

        <?/*analog button*/?>
        <a class="add-to-cart" id="<?=$itemIds['ANALOG_BUTTON']?>"
           href="#analogue"
           rel="nofollow"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_BUTTON_ANALOG")?></a>

    </form>

<?endif;?>


<?$multiplicity = true;
if(intval($arParams["CATALOG_QUANTITY"]) > 0 && intval($arParams["CATALOG_QUANTITY"]) < intval($arParams["ITEM_MEASURE_RATIO"])){
    if(intval($arParams["CATALOG_QUANTITY"]) % intval($arParams["ITEM_MEASURE_RATIO"]) > 0) {
        $multiplicity = false;
    }
}
?>

<div class="in-box not-multiples" <?if($multiplicity):?>style="display: none;"<?endif;?>>
                                    <span><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.74 16.36"><circle class="not-equal--svg" cx="9.37" cy="3.6" r="3.6"/>
                                            <circle class="not-equal--svg" cx="15.14" cy="12.77" r="3.6"/><circle class="not-equal--svg" cx="3.6" cy="12.77" r="3.6"/>
                                        </svg><?=Loc::getMessage("MCART_CATALOG_QUANTITY_MESS_NOT_MULT")?></span>
    <a href="javascript:void(0)" id="<?=$itemIds["REQUEST_LINK_MULT"];?>"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_NOT_MULT")?></a>
</div>



<script>
	BX.message({
		MCART_CATALOG_QUANTITY_PATH: "<?=$templateFolder;?>",

		MCART_CATALOG_QUANTITY_ERROR: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_ERROR")?>",
		MCART_CATALOG_QUANTITY_NOTIFY: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_NOTIFY")?>",
		MCART_CATALOG_QUANTITY_BASKET: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_BASKET")?>",

		MCART_CATALOG_QUANTITY_EXAMPLE_IN: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_EXAMPLE_IN")?>",
		MCART_CATALOG_QUANTITY_EXAMPLE_ADD: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_EXAMPLE_ADD")?>",

        MCART_CATALOG_QUANTITY_REQ_MULT: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQ_MULT")?>",

        MCART_CATALOG_QUANTITY_PRICE_IN: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_PRICE_IN")?>",
        MCART_CATALOG_QUANTITY_PRICE_ADD: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_PRICE_ADD")?>",
        MCART_CATALOG_QUANTITY_ITEM_ADD: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_ITEM_ADD")?>",

		MCART_CATALOG_QUANTITY_NO_AUTH: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_NO_AUTH")?>",

        MCART_CATALOG_QUANTITY_IN_BASKET: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_IN_BASKET")?>",
        MCART_CATALOG_QUANTITY_ADD_MORE_PROD: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_ADD_MORE_PROD")?>",
        MCART_CATALOG_QUANTITY_SUB_MORE_PROD: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_SUB_MORE_PROD")?>",

	});
</script>
<?/* Favorite block */?>
<?if (isset($arParams["FAVORITE"])):?>
<script>
	BX.message({
		MCART_CATALOG_QUANTITY_ITEM_FAVORITE_Y: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_ITEM_FAVORITE_Y")?>",
		MCART_CATALOG_QUANTITY_ITEM_FAVORITE_N: "<?=Loc::getMessage("MCART_CATALOG_QUANTITY_ITEM_FAVORITE_N")?>",
	});
</script>

    <div class="sidebar-question">
        <a href="#modal-add-more" rel="modal:open" id="<?=$itemIds["MODAL_ID"]?>">
		<span><svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 448">
			<title><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_MORE_MODAL_LINK")?></title>
			<path d="M288,480a10.67,10.67,0,0,1-10.67-10.67v-32H138.67a32,32,0,0,1-32-32v-32H128v32A10.67,10.67,0,0,0,138.67,416H288a10.67,10.67,0,0,1,10.67,10.67v16.92l24.46-24.46a10.67,10.67,0,0,1,7.54-3.13H480a10.67,10.67,0,0,0,10.67-10.67v-256A10.67,10.67,0,0,0,480,138.67H426.67V117.33H480a32,32,0,0,1,32,32v256a32,32,0,0,1-32,32H335.08l-39.54,39.54A10.67,10.67,0,0,1,288,480Z" transform="translate(0 -32)"/><path d="M192,394.67A10.67,10.67,0,0,1,181.33,384V352H32A32,32,0,0,1,0,320V64A32,32,0,0,1,32,32H373.33a32,32,0,0,1,32,32V320a32,32,0,0,1-32,32H239.08l-39.54,39.54a10.67,10.67,0,0,1-7.54,3.13ZM32,53.33A10.67,10.67,0,0,0,21.33,64V320A10.67,10.67,0,0,0,32,330.67H192a10.67,10.67,0,0,1,10.67,10.67v16.92l24.46-24.46a10.67,10.67,0,0,1,7.54-3.13H373.33A10.67,10.67,0,0,0,384,320V64a10.67,10.67,0,0,0-10.67-10.67Z" transform="translate(0 -32)"/><path d="M53.33,117.33H170.67v21.33H53.33Z" transform="translate(0 -32)"/><path d="M53.33,181.33H352v21.33H53.33Z" transform="translate(0 -32)"/><path d="M53.33,245.33H352v21.33H53.33Z" transform="translate(0 -32)"/><path d="M330.67,117.33H352v21.33H330.67Z" transform="translate(0 -32)"/><path d="M288,117.33h21.33v21.33H288Z" transform="translate(0 -32)"/>
		</svg></span>
            <span><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_MORE_MODAL_LINK")?></span>
        </a>
    </div>

    <div id="modal-add-more" class="modal">


        <div class="more-header"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_MORE_MODAL")?></div>
		<div class="more-text"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_TEXT")?></div>

		<div class="more-quant quantity-num" id="<?=$itemIds['QUANTITY_BLOCK_MODAL']?>">
            <span class="quantity-button" id="<?=$itemIds['QUANTITY_BLOCK_MODAL_DOWN']?>" href="javascript:void(0)" rel="nofollow">-</span>
			<input type="number" min="1" id="<?=$itemIds["QUANTITY_IN_MODAL"];?>">
            <span class="quantity-button" id="<?=$itemIds['QUANTITY_BLOCK_MODAL_UP']?>" href="javascript:void(0)" rel="nofollow">+</span>
        </div>


        <div class="more-text__more"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_MESS1_MODAL")?></div>
		<div class="more-footer">
			<div class="more-text__req"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_MESS2_MODAL")?></div>
			<a class="sidebar-sample" id="<?=$itemIds['QUANTITY_REQ_BUTTON_MODAL']?>"
			   href="javascript:void(0)"
			   rel="nofollow"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_REQUEST_IN_MODAL")?></a>
		</div>
    </div>

<?//$toFav = $USER->IsAuthorized() ? 'onclick="BX.Mcart.addToFavs(this);"' : 'onclick="BX.Mcart.noAuth();"';?>
<div class="sidebar-favorite <?= $arParams["FAVORITE"] == "Y" ? 'act':''?>" id="<?=$itemIds["FAVORITES"]?>" data-id="<?=$arParams["ELEMENT_ID"]?>">
	<span>
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="1 -3 36 40" enable-background="new 0 0 50 50"><rect id="backgroundrect" x="0" y="0" fill="none" stroke="none"/>
		<style>
			#fill-heart {
				fill: transparent;
			}
		</style>
		<g class="currentLayer" style=""><path d="M19.054054260253906,31.59189147949219 l-0.6000000000000001,-0.5 C5.554054260253906,20.591891479492187 2.0540542602539062,16.891891479492188 2.0540542602539062,10.891891479492188 c0,-5 4,-9 9,-9 c4.1,0 6.4,2.3 8,4.1 c1.6,-1.8 3.9,-4.1 8,-4.1 c5,0 9,4 9,9 c0,6 -3.5,9.7 -16.4,20.2 l-0.6000000000000001,0.5 zM11.054054260253906,3.8918914794921875 c-3.9,0 -7,3.1 -7,7 c0,5.1 3.2,8.5 15,18.1 c11.8,-9.6 15,-13 15,-18.1 c0,-3.9 -3.1,-7 -7,-7 c-3.5,0 -5.4,2.1 -6.9,3.8 L19.054054260253906,8.991891479492189 l-1.1,-1.3 C16.454054260253905,5.991891479492187 14.554054260253906,3.8918914794921875 11.054054260253906,3.8918914794921875 z" id="svg_1" class=""/>
		<path  id="svg_22" d="M-29.97297297297297,2.4594594594594668 " /><path  id="svg_2" d="M19.324324324324337,8.837837837837846 " />
		<path  id="fill-heart" d="M 19.216216216216225 7.432432432432435 L 24.972972972972972 3.027027027027027 L 31.243243243243242 3.891891891891892 L 35.24324324324324 10.702702702702704 L 31.56756756756757 19.56756756756757 L 19.243243243243242 30.27027027027027 L 5.081081081081081 17.62162162162162 L 3.027027027027027 11.027027027027026 L 4.972972972972973 5.621621621621622 L 10.27027027027027 2.918918918918919 L 15.243243243243244 3.891891891891892 L 18.7027027027027 6.378378378378378"  /></g>
	</svg>
	</span>
	<span class="fav_status"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_ITEM_FAVORITE_".$arParams["FAVORITE"])?></span>
</div>
<?endif;?>


<?if($arParams["CATALOG_AVAILABLE"] == "Y"):?>
	<?//$toExample = $USER->IsAuthorized() ? "onclick=\"BX.Mcart.addToBaskExample(this, '".$arParams["DETAIL_PAGE"]."');\"" : 'onclick="BX.Mcart.noAuth();"';?>

<button class="sidebar-sample" id="<?=$itemIds['ADD_EXAMPLE_LINK']?>" is_in_basket="<?=$arResult["BASKET_EXAMPLE"] ? "1" : "0"?>" data-id="<?=$arParams["ELEMENT_ID"]?>"><?=Loc::getMessage("MCART_CATALOG_QUANTITY_ITEM_EXAMPLE")?></button>
	<div class="close-sidebar">
		<span></span><span></span>
	</div>
<?endif;?>

<script>
    <?/*for JS*/?>
    <?$temp = array(
        "ELEMENT_ID" => $arParams["ELEMENT_ID"],
        "NAME" => $arParams["NAME"],
        "CATALOG_CAN_BUY_ZERO" => $arParams["CATALOG_CAN_BUY_ZERO"],
        "PRICES" => $arParams["PRICES"],
        "CATALOG_QUANTITY" => $arParams["CATALOG_QUANTITY"],
        "MIN_QUANTITY" => $arResult['MIN_QUANTITY'],
        "BASKET_QUANTITY" => $arResult['BASKET_QUANTITY'] ? intval($arResult['BASKET_QUANTITY']) : 0,
        "RATIO_QUANTITY" => $arResult["RATIO_QUANTITY"],
        "REQUEST" => "",
        "SYMPACKAGE" => $arParams["PACK_INFO"]["SYMPACKAGE"],
        "CATALOG_AVAILABLE" => $arParams["CATALOG_AVAILABLE"],
        "BRAND" => $arParams["BRAND"],
        "BREAD" => $arParams["BREAD"],
    );

    $arCollections["PACKING"] = $arParams["PACK_INFO"]["UPAK"]["VALUE"];
    $arCollections["MEASURE"] = $arParams["ITEM_MEASURE"]["MEASURE_TITLE"];
    ?>

    var item = <?=CUtil::PhpToJSObject($temp)?>;
    var arCollections = <?=CUtil::PhpToJSObject($arCollections)?>;
    var itemIds = <?=CUtil::PhpToJSObject($itemIds)?>;
    var detailPage = '<?=$arParams["DETAIL_PAGE"]?>';
    var isAuthorized = "<?=$USER->IsAuthorized()?>";
    var dictWords = <?=CUtil::PhpToJSObject($arParams["DECLINATION_OBJ"]->getDeclinationsArray());?>;
    var objDel = new BX.Mcart.SymmetronDeclination(dictWords);

    new CatalogQuantityItemDetail(item, arCollections, itemIds, detailPage, isAuthorized, objDel);
</script>
