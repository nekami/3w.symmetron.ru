<?php
define('STOP_STATISTICS', true);
//require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.section/");
//BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.element/");
//BXClearCache(true, "/" . SITE_ID . "/mcart/catalog.quantity/");

$GLOBALS['APPLICATION']->RestartBuffer();

$arResult = [
    "BASKET_EXAMPLE" => "0",
    "BASKET_QUANTITY" => "0",
];
if (isset($_REQUEST["ELEMENT_ID"]) && $_REQUEST["ELEMENT_ID"] > 0) {
    global $CACHE_MANAGER;
    $CACHE_MANAGER->ClearByTag("mcart_catalog_quantity");

    $basket = \Bitrix\Sale\Basket::loadItemsForFUser(
        \Bitrix\Sale\Fuser::getId(),
        \Bitrix\Main\Context::getCurrent()->getSite()
    );
    foreach ($basket as $basketItem) {
        $id = $basketItem->getProductId();

        if ($_REQUEST["ELEMENT_ID"] != $id)
            continue;

        $properties = $basketItem->getPropertyCollection()->getPropertyValues();
        if (isset($properties["EXAMPLE"]))
            $arResult["BASKET_EXAMPLE"] = "1";
        else
            $arResult["BASKET_QUANTITY"] = $basketItem->getQuantity() > 0 ? "1" : "0";
    }
}
echo json_encode($arResult);
//require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
die();