<?php

define('STOP_STATISTICS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

//BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.section/");
//BXClearCache(true, "/" . SITE_ID . "/bitrix/catalog.element/");
//BXClearCache(true, "/" . SITE_ID . "/mcart/catalog.quantity/");

$GLOBALS['APPLICATION']->RestartBuffer();

if(!Cmodule::IncludeModule("mcart.basketsymmetron"))
    die;

if(!Cmodule::IncludeModule("sale"))
    die;

if (!empty($_GET)) {

    global $CACHE_MANAGER;
    $CACHE_MANAGER->ClearByTag("mcart_catalog_quantity");

    $arFields = array(
        "PRODUCT_ID" => htmlspecialchars($_GET["id"]),
        "PRICE" => htmlspecialchars($_GET["price"]) ? : 0,
        "CURRENCY" => "RUB",
        "QUANTITY" => htmlspecialchars($_GET["quant"]),
        "LID" => LANG,
        "DELAY" => "N",
        "CAN_BUY" => "Y",
        "NAME" => htmlspecialchars($_GET["name"]),
        "DETAIL_PAGE_URL" => htmlspecialchars($_GET["url"])
    );

    if($_GET["example"] == "Y") {
        $arProps = array(
            array("NAME" => "Образец", "CODE" => "EXAMPLE", "VALUE" => "Y")
        );
        $arFields["PROPS"] = $arProps;
    }

    $res = "";
    if($_GET["action"] == "update"){

        $idInBasket = CBasketSymmetron::getIdExistingElementFromBasket($arFields["PRODUCT_ID"]);

        if(!empty($idInBasket)){
            $res = CSaleBasket::update($idInBasket, $arFields);
        }

    } elseif ($_GET["action"] == "request_quantity"){

        if($idInBasket = CBasketSymmetron::getIdExistingElementFromBasket($arFields["PRODUCT_ID"])){

            $arBasketItem = CSaleBasket::GetList(
                array(),
                array(
                    "ID" => $idInBasket,
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "ORDER_ID" => "NULL"
                ),
                false,
                false
            )->Fetch();
            if(!empty($arBasketItem)) {
                $arFieldsUpdate = array(
                    "PRICE" => 0,
                    //"QUANTITY" => 0,
                    "PROPS" => array(
                        array("NAME" => "Скрытый", "CODE" => "HIDDEN", "VALUE" => "Y", "SORT" => 100),
                        //array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => $value, "SORT" => 100)
                    )
                );


                $arBasketItem["PRICE"] = 0;
                $arBasketItem["QUANTITY"] = $arFields["QUANTITY"];
                $arBasketItem["PROPS"] = array(
                    array(
                        "NAME" => "Родительский товар",
                        "CODE" => "PARENT_PRODUCT",
                        "VALUE" => $arBasketItem["ID"],
                        "SORT" => 100
                    ),
                    array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "QUANTITY", "SORT" => 100)
                );

                unset($arBasketItem["ID"]);

                if(CSaleBasket::Update($idInBasket, $arFieldsUpdate))
                    if($res = CSaleBasket::Add($arBasketItem))
                        echo $res;

            }
        } else {
            $arFields["PRICE"] = 0;
            $arFields["PROPS"] = array(
                array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "QUANTITY", "SORT" => 100)
            );

            $res = CSaleBasket::Add($arFields);
        }

    } elseif($_GET["action"] == "request_missing") {
        $arFields["PRICE"] = 0;
        $arFields["PROPS"] = array(
            array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "MISSING", "SORT" => 100)
        );

        $res = CSaleBasket::Add($arFields);
    } elseif ($_GET["action"] == "request_multyplicity"){
        $flagRequestMult = true;

        $arBasketItem = CSaleBasket::GetList(
            array(),
            array(
                "PRODUCT_ID" => $arFields["PRODUCT_ID"],
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                //"LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            false,
            false,
            array("ID", "PRODUCT_ID")
        )->Fetch();

        if(!empty($arBasketItem)){
            $rsProps = CSaleBasket::GetPropsList(
                array(),
                array("BASKET_ID" => $arBasketItem["ID"])
            );

            while ($arProp = $rsProps->Fetch()) {
                if ($arProp["CODE"] == "REQUEST" && $arProp["VALUE"] == "MULTIPLICITY") {
                    $flagRequestMult = true;
                }
            }
        }

        if($flagRequestMult){
            $arFields["PRICE"] = 0;
            $arFields["PROPS"] = array(
                array("NAME" => "Запрос", "CODE" => "REQUEST", "VALUE" => "MULTIPLICITY", "SORT" => 100)
            );

            $res = CSaleBasket::Add($arFields);
        } else {
            echo 1;
        }

    } else {
        $res = CSaleBasket::Add($arFields);
    }
    echo $res;
}

die;