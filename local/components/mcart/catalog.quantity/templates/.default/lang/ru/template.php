<?
    $MESS["MCART_CATALOG_QUANTITY_ITEM_ONE_EL"] = "шт.";
    $MESS["MCART_CATALOG_QUANTITY_ITEM_FROM"] = "от";
    $MESS["MCART_CATALOG_QUANTITY_ITEM_FAVORITE_Y"] = "В избранном";
    $MESS["MCART_CATALOG_QUANTITY_ITEM_FAVORITE_N"] = "Добавить в избранное";
    $MESS["MCART_CATALOG_QUANTITY_ITEM_CALLBACK"] = "Задать вопрос";
    $MESS["MCART_CATALOG_QUANTITY_ITEM_EXAMPLE"] = "Запросить образец";

    $MESS["MCART_CATALOG_QUANTITY_ERROR"] = "Ошибка!";
    $MESS["MCART_CATALOG_QUANTITY_NOTIFY"] = "Уведомление";
    $MESS["MCART_CATALOG_QUANTITY_BASKET"] = "Перейти в корзину";

    $MESS["MCART_CATALOG_QUANTITY_EXAMPLE_IN"] = "Запрос образца уже добавлен в корзину";
    $MESS["MCART_CATALOG_QUANTITY_EXAMPLE_ADD"] = "Образец добавлен в корзину";

    $MESS["MCART_CATALOG_QUANTITY_REQ_MULT"] = "Запрос некратных остатков добавлен в корзину";

    $MESS["MCART_CATALOG_QUANTITY_PRICE_IN"] = "Запрос цены уже добавлен в корзину";
    $MESS["MCART_CATALOG_QUANTITY_PRICE_ADD"] = "Цена по товару запрошена";
    $MESS["MCART_CATALOG_QUANTITY_ITEM_ADD"] = "Товар успешно добавлен в корзину";

    $MESS["MCART_CATALOG_QUANTITY_NO_AUTH"] = "Вам нужно авторизоваться";



    $MESS["MCART_CATALOG_QUANTITY_BUTTON_REQ_MISSING"] = "Запрос условий поставки в корзине";
    $MESS["MCART_CATALOG_QUANTITY_BUTTON_REQ_QUANTITY"] = "Запросить это количество";
    $MESS["MCART_CATALOG_QUANTITY_BUTTON_REQ_QUANTITY_BASKET"] = "Запрос количества в корзине";

    $MESS["MCART_CATALOG_QUANTITY_BUTTON_BUY"] = "Добавить в корзину";
    $MESS["MCART_CATALOG_QUANTITY_BUTTON_PRICE"] = "Запросить цену";
    $MESS["MCART_CATALOG_QUANTITY_BUTTON_EMPTY"] = "Запросить условия поставки";
    $MESS["MCART_CATALOG_QUANTITY_BUTTON_ANALOG"] = "Показать аналоги";


    $MESS["MCART_CATALOG_QUANTITY_COLLECTION_UNITS_1"] = "единица";
    $MESS["MCART_CATALOG_QUANTITY_COLLECTION_UNITS_2"] = "единицы";
    $MESS["MCART_CATALOG_QUANTITY_COLLECTION_UNITS_5"] = "единиц";

    $MESS["MCART_CATALOG_QUANTITY_COLLECTION_PRODUCTS_1"] = "товар";
    $MESS["MCART_CATALOG_QUANTITY_COLLECTION_PRODUCTS_2"] = "товара";
    $MESS["MCART_CATALOG_QUANTITY_COLLECTION_PRODUCTS_5"] = "товаров";


    $MESS["MCART_CATALOG_QUANTITY_THIS_QUANTITY_IS_NONE"] = "Такого количества<br/>нет в наличии";
    $MESS["MCART_CATALOG_QUANTITY_YOU_CAN_CHANGE_QUANTITY"] = "Количество можно изменить введя новое значение или в Корзине";
    $MESS["MCART_CATALOG_QUANTITY_IN_BASKET"] = "в корзине";
    $MESS["MCART_CATALOG_QUANTITY_ADD_MORE_PROD"] = "Добавить ещё";
    $MESS["MCART_CATALOG_QUANTITY_SUB_MORE_PROD"] = "Убрать";

    $MESS["MCART_CATALOG_QUANTITY_REQUEST_IN"] = "в";
    $MESS["MCART_CATALOG_QUANTITY_REQUEST_UNITY"] = "Поштучная продажа";
    $MESS["MCART_CATALOG_QUANTITY_REQUEST_MEASURE_MINI_DEF"] = "шт";
    $MESS["MCART_CATALOG_QUANTITY_REQUEST_MEASURE_DEF"] = "штука";

    $MESS["MCART_CATALOG_QUANTITY_MESS_NOT_MULT"] = "Имеются некратные остатки.";
    $MESS["MCART_CATALOG_QUANTITY_REQUEST_NOT_MULT"] = "Запросить";
?>