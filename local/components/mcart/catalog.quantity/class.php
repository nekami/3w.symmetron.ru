<?
use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
	ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
	return;
}

class CatalogQuantity extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams = parent::onPrepareComponentParams($arParams);

		$arParams["CATALOG_AVAILABLE"] = $arParams["CATALOG_AVAILABLE"] == "Y" ? "Y" : "N";
		$arParams["CATALOG_QUANTITY"]  =
            (double)($arParams["CATALOG_QUANTITY"]) > 0
				? (double)($arParams["CATALOG_QUANTITY"])
				: 0;

		if (!is_array($arParams["PRICES"]))
			$arParams["PRICES"] = [];

		$arParams["ITEM_MEASURE_RATIO"] =
            (double)($arParams["ITEM_MEASURE_RATIO"]) > 0
				? (double)($arParams["ITEM_MEASURE_RATIO"])
				: 1;

		$arParams["MIN_QUANTITY"] =
            (double)($arParams["MIN_QUANTITY"]) > 0
				? (double)($arParams["MIN_QUANTITY"])
				: 1;
		$arParams["ELEMENT_ID"] = intval($arParams["ELEMENT_ID"]);

		if (isset($arParams["FAVORITE"]))
			$arParams["FAVORITE"] = $arParams["FAVORITE"] == "Y" ? "Y" : "N";
		$arParams["CALLBACK_FORM"] = $arParams["CALLBACK_FORM"] == "Y";
        $arParams["CATALOG_CAN_BUY_ZERO"] = $arParams["CATALOG_CAN_BUY_ZERO"] == "Y" ? "Y" : "N";

		$arParams["DETAIL_PAGE"] = trim($arParams["DETAIL_PAGE"]);
		return $arParams;
	}

	protected function measureRatioInt()
    {
        $ifFrac = false;
        if (isset($this->arParams["ITEM_MEASURE_RATIO_ID"]) &&
            \Bitrix\Main\Loader::includeModule("mcart.symmetronsyncdb"))
        {
            //$arFilter = [];
            if ($this->arParams["ITEM_MEASURE_RATIO_ID"] < 0) {
                $this->arParams["ITEM_MEASURE_RATIO_ID"] = -1;
                //$arFilter = array("ID" => $this->arParams["ITEM_MEASURE_RATIO_ID"]);
            }
            $measure = null;

            //$measure_fractional2 = \Bitrix\Main\Config\Option::get('mcart.symmetronsyncdb', 'measure_fractional', '');
            $measure_fractional = $this->arParams["MEASURE_FRACTIONAL"];

            $arMeasure = $this->arParams["MEASURE_DATA"];

            foreach ($arMeasure as $row) {
                if ($this->arParams["ITEM_MEASURE_RATIO_ID"] === -1 && $row["IS_DEFAULT"] !== "Y"
                    || $this->arParams["ITEM_MEASURE_RATIO_ID"] != $row["ID"]) {
                    continue;
                }

                foreach ($measure_fractional as $code)
                {
                    if ($row["CODE"] == $code)
                    {
                        $ifFrac = true;
                        break;
                    }
                }
            }

        }
        if (!$ifFrac) {
            $this->arParams["ITEM_MEASURE_RATIO"] = intval($this->arParams["ITEM_MEASURE_RATIO"]) ?: 1;
        }

    }

	public function executeComponent()
	{
        if ($this->startResultCache()) {

            global $CACHE_MANAGER;
            $component = &$this;
            while ($component !== null) {
                $CACHE_MANAGER->StartTagCache($component->getCachePath());
                $CACHE_MANAGER->RegisterTag("mcart_catalog_quantity");
                $component = &$component->getParent();
            }

            $this->measureRatioInt();
            $this->arResult["AVAILABLE_TEXT"] = $this->availableText();
            $this->getFromBasketData();
            $this->preparePrices();

            $this->arResult["BUTTON_BUY"] = $this->buyButtonText();
            $this->includeComponentTemplate();

            $component = &$this;
            while ($component !== null) {
                $CACHE_MANAGER->EndTagCache();
                $component = &$component->getParent();
            }

        }
	}

	protected function getFromBasketData()
	{
		$basket = $this->arParams["BASKET_PRODS"] ?:[];

		foreach ($basket as $basketItem) {
			$id = $basketItem->getProductId();

			if ($this->arParams["ELEMENT_ID"] != $id)
				continue;

			$properties = $basketItem->getPropertyCollection()->getPropertyValues();


			if (!empty($properties["EXAMPLE"])) {
                $this->arResult["BASKET_EXAMPLE"] = true;
            } elseif (!empty($properties["REQUEST"]["VALUE"])) {
                $this->arResult["BASKET_QUANTITY"] = $basketItem->getQuantity();
                $this->arResult["BASKET_REQUEST"] = $properties["REQUEST"]["VALUE"];
            } elseif (!empty($properties["HIDDEN"]["VALUE"]) && $properties["HIDDEN"]["VALUE"] == "Y") {
			    continue;
            } else {
                $this->arResult["BASKET_QUANTITY"] = $basketItem->getQuantity();
            }

		}
	}

	protected function preparePrices()
	{
		/* Check prices to empty */
		if (count($this->arParams["PRICES"]) == 1 && empty(reset($this->arParams["PRICES"])["PRICE"]))
			$this->arParams["PRICES"] = [];

		/* Get min and ratio from prices */
		if (is_array($ar = reset($this->arParams["PRICES"])))
			$min_quantity = $ar["MIN_QUANTITY"];
		if ($min_quantity < $this->arParams["MIN_QUANTITY"])
			$min_quantity = $this->arParams["MIN_QUANTITY"];
		if (!empty($this->arResult["BASKET_QUANTITY"]))
		    $min_quantity -= $this->arResult["BASKET_QUANTITY"];
        $ratio_quantity = $this->arParams["ITEM_MEASURE_RATIO"];
		if (empty($min_quantity) || $min_quantity < 0) $min_quantity = $ratio_quantity;


		$min_quantity = ceil($min_quantity / $ratio_quantity - 0.000000001) * $ratio_quantity;

		$this->arResult["MIN_QUANTITY"] = $min_quantity;
		$this->arResult["RATIO_QUANTITY"] = $ratio_quantity;
	}

	protected function buyButtonText()
	{

        if(!empty($this->arResult["BASKET_REQUEST"])){
            if($this->arResult["BASKET_REQUEST"] == "MISSING")
                return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_REQ_MISSING');

            if($this->arResult["BASKET_REQUEST"] == "QUANTITY")
                return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_REQ_QUANTITY');

            if($this->arResult["BASKET_REQUEST"] == "PRICE")
                return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_REQ_PRICE');
        }

	    if(empty($this->arParams["CATALOG_QUANTITY"]))
            return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_EMPTY');

		if(count($this->arParams["PRICES"]) == 0)
			return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_CHECK');

		if($this->arParams["CATALOG_AVAILABLE"] !== "Y")
		    return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_ANALOG');

		return Loc::getMessage('MCART_CATALOG_QUANTITY_BUTTON_BUY');
	}

	protected function availableText()
	{
		if($this->arParams["CATALOG_AVAILABLE"] != "Y")
			return Loc::getMessage('MCART_CATALOG_QUANTITY_NOT_AVAILABLE');

		if ($this->arParams["CATALOG_QUANTITY"] == 0)
			return Loc::getMessage('MCART_CATALOG_QUANTITY_HAVENT');

//		$mod = 5;
//		if (!in_array($this->arParams["CATALOG_QUANTITY"], [11, 12, 13, 14]))
//			$mod = $this->arParams["CATALOG_QUANTITY"] % 10;
//		$mesure = $this->arParams["ITEM_MEASURE"]["MEASURE_TITLE"] ?: Loc::getMessage('MCART_CATALOG_QUANTITY_END_TO_'.$mod);
//        $mesure = Loc::getMessage('MCART_CATALOG_QUANTITY_SHT');
        
        $catalogQuantity = $this->arParams["CATALOG_QUANTITY"] <= 9999 ? $this->arParams["CATALOG_QUANTITY"] : number_format($this->arParams["CATALOG_QUANTITY"], 0, "", " ");
        $measure = $this->arParams["DECLINATION_OBJ"]->declinationNum(strtolower($this->arParams["ITEM_MEASURE"]["MEASURE_TITLE"]), (int)$this->arParams["CATALOG_QUANTITY"]);

		return Loc::getMessage('MCART_CATALOG_QUANTITY_HAVE').
			" ".$catalogQuantity.
            " ".$measure;
			//" ".Loc::getMessage('MCART_CATALOG_QUANTITY_END_TO_'.$mod);
	}

}