<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль \"Информационные блоки\" не установлен";

$MESS["MCART_CATALOG_QUANTITY_HAVE"] = "В наличии";
$MESS["MCART_CATALOG_QUANTITY_HAVENT"] = "Нет в наличии";
$MESS["MCART_CATALOG_QUANTITY_NOT_AVAILABLE"] = "Товар недоступен";

$MESS["MCART_CATALOG_QUANTITY_BUTTON_REQ_MISSING"] = "Запрос условий поставки в корзине";
$MESS["MCART_CATALOG_QUANTITY_BUTTON_REQ_QUANTITY"] = "Запрос количества в корзине";
$MESS["MCART_CATALOG_QUANTITY_BUTTON_REQ_PRICE"] = "Запрос количества в корзине";

$MESS["MCART_CATALOG_QUANTITY_BUTTON_BUY"] = "Добавить в корзину";
$MESS["MCART_CATALOG_QUANTITY_BUTTON_PRICE"] = "Запросить цену";
$MESS["MCART_CATALOG_QUANTITY_BUTTON_EMPTY"] = "Запросить условия поставки";
$MESS["MCART_CATALOG_QUANTITY_BUTTON_ANALOG"] = "Показать аналоги";

$MESS["MCART_CATALOG_QUANTITY_END_TO_1"] = "единица";
$MESS["MCART_CATALOG_QUANTITY_END_TO_2"] = "единицы";
$MESS["MCART_CATALOG_QUANTITY_END_TO_3"] = "единицы";
$MESS["MCART_CATALOG_QUANTITY_END_TO_4"] = "единицы";
$MESS["MCART_CATALOG_QUANTITY_END_TO_5"] = "единиц";
$MESS["MCART_CATALOG_QUANTITY_END_TO_6"] = "единиц";
$MESS["MCART_CATALOG_QUANTITY_END_TO_7"] = "единиц";
$MESS["MCART_CATALOG_QUANTITY_END_TO_8"] = "единиц";
$MESS["MCART_CATALOG_QUANTITY_END_TO_9"] = "единиц";
$MESS["MCART_CATALOG_QUANTITY_END_TO_0"] = "единиц";

$MESS["MCART_CATALOG_QUANTITY_SHT"] = "шт.";
?>