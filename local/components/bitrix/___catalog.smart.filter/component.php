<?

use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixCatalogSmartFilter $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

if (!Loader::includeModule('iblock')) {
    ShowError(GetMessage("CC_BCF_MODULE_NOT_INSTALLED"));
    return;
}

$FILTER_NAME = (string)$arParams["FILTER_NAME"];

$arResult["FACET_FILTER"] = false;
$arResult["COMBO"] = array();
$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
$arResult["ITEMS"] = $this->getResultItems();
$arResult["CURRENCIES"] = array();

$propertyEmptyValuesCombination = array();
foreach ($arResult["ITEMS"] as $PID => $arItem)
    $propertyEmptyValuesCombination[$arItem["ID"]] = array();

if (!empty($arResult["ITEMS"])) {
    /*Make iblock filter*/
    global ${$FILTER_NAME};
    if (!is_array(${$FILTER_NAME})) {
        ${$FILTER_NAME} = array();
        ${$FILTER_NAME}["OFFERS"] = array();
    }

    global $DB;

    $arrPropStr = [];

    $arElementFilter = array(
        "IBLOCK_ID" => $this->IBLOCK_ID,
        "SUBSECTION" => $this->SECTION_ID,
        "SECTION_SCOPE" => "IBLOCK",
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "Y",
    );

    $_CHECK = &$_REQUEST;

    foreach ($_CHECK as $key => $value) {
        $is3 = explode("_", $key);
        if (count($is3) == 3 && $is3[0] == "arrFilter") {
            if ($arResult["ITEMS"][$is3[1]]["PROPERTY_TYPE"] == "N") {
                $existValue = (strlen($value) > 0);
                $dv = ($arResult["ITEMS"][$is3[1]]["PROPERTY_TYPE"] === "mcart_property_with_measure_units" ? "DESCRIPTION" : "VALUE");
                if ($existValue) {
                    $filterKey = '';
                    $filterValue = '';
                    if ($is3[2] == "MIN") {
                        $strSql = "select * from b_iblock_element_property where " . $dv . " + 0 >= " . floatval($value) . " AND " . $dv . " <> '' AND IBLOCK_PROPERTY_ID = " . $is3[1] . ";";
                        $res = $DB->Query($strSql, false, $err_mess . __LINE__);

                        $subArray = [];
                        while ($row = $res->Fetch()) {
                            array_push($subArray, $row['IBLOCK_ELEMENT_ID']);
                        }

                        $filterKey = "ID";
                        if (key_exists($filterKey, ${$FILTER_NAME})) {
                            $filterValue = array_intersect(${$FILTER_NAME}[$filterKey], $subArray);
                        } else {
                            $filterValue = $subArray;
                        }
                    } elseif ($is3[2] == "MAX") {
                        $strSql = "select * from b_iblock_element_property where " . $dv . " + 0 <= " . floatval($value) . " AND " . $dv . " <> '' AND IBLOCK_PROPERTY_ID = " . $is3[1] . ";";
                        $res = $DB->Query($strSql, false, $err_mess . __LINE__);

                        $subArray = [];
                        while ($row = $res->Fetch()) {
                            array_push($subArray, $row['IBLOCK_ELEMENT_ID']);
                        }

                        $filterKey = "ID";
                        if ($arItem["IBLOCK_ID"] == $this->SKU_IBLOCK_ID) {
                            if (key_exists($filterKey, ${$FILTER_NAME}["OFFERS"])) {
                                $filterValue = array_intersect(${$FILTER_NAME}["OFFERS"][$filterKey], $subArray);
                            } else {
                                $filterValue = $subArray;
                            }
                        } else {
                            if (key_exists($filterKey, ${$FILTER_NAME})) {
                                $filterValue = array_intersect(${$FILTER_NAME}[$filterKey], $subArray);
                            } else {
                                $filterValue = $subArray;
                            }
                        }
                    }
                    if (!isset(${$FILTER_NAME}["OFFERS"])) {
                        ${$FILTER_NAME}["OFFERS"] = array();
                    }

                    if (count($filterValue) == 0) {
                        ${$FILTER_NAME}["OFFERS"][$filterKey] = 0;
                        ${$FILTER_NAME}[$filterKey] = 0;
                    } else {
                        ${$FILTER_NAME}["OFFERS"][$filterKey] = $filterValue;
                        ${$FILTER_NAME}[$filterKey] = $filterValue;
                    }
                }
            } else {
                $filterKey = "=PROPERTY_" . $is3[1];
                if (!key_exists($is3[1], $arrPropStr)) {
                    $arrPropStr[$is3[1]] = [];

                    $strSql = 'select DISTINCT(VALUE) AS VALUE from b_iblock_element_property where IBLOCK_PROPERTY_ID = ' . $is3[1] . ';';
                    $res = $DB->Query($strSql, false, $err_mess . __LINE__);

                    while ($row = $res->Fetch()) {
                        $arrPropStr[$is3[1]][abs(crc32($row['VALUE']))] = $row['VALUE'];
                    }
                }

                if (!key_exists($filterKey, ${$FILTER_NAME}["OFFERS"])) {
                    ${$FILTER_NAME}["OFFERS"][$filterKey] = [$arrPropStr[$is3[1]][$is3[2]]];
                } else {
                    ${$FILTER_NAME}["OFFERS"][$filterKey][] = $arrPropStr[$is3[1]][$is3[2]];
                }
                if (!key_exists($filterKey, ${$FILTER_NAME})) {
                    ${$FILTER_NAME}[$filterKey] = [$arrPropStr[$is3[1]][$is3[2]]];
                } else {
                    ${$FILTER_NAME}[$filterKey][] = $arrPropStr[$is3[1]][$is3[2]];
                }
            }
        }
    }

    if ($arItem["IBLOCK_ID"] == $this->SKU_IBLOCK_ID) {
        $arElementFilter = array_merge(${$FILTER_NAME}["OFFERS"], $arElementFilter);
    } else {
        $arElementFilter = array_merge(${$FILTER_NAME}, $arElementFilter);
    }

    if ('Y' == $this->arParams['HIDE_NOT_AVAILABLE'])
        $arElementFilter['CATALOG_AVAILABLE'] = 'Y';

    $arElements = array();

    if (!empty($this->arResult["PROPERTY_ID_LIST"])) {
        $rsElements = CIBlockElement::GetPropertyValues($this->IBLOCK_ID, $arElementFilter, true, array('ID' => $this->arResult["PROPERTY_ID_LIST"]));
        while ($arElement = $rsElements->Fetch()) {
            unset($arElement['PROPERTY_VALUE_ID']);
            unset($arElement['DESCRIPTION']);
            foreach ($arElement as $i => $v) {
                $arElements[$arElement["IBLOCK_ELEMENT_ID"]][$i] = $v;
            }
        }
    } else {
        $rsElements = CIBlockElement::GetList(array('ID' => 'ASC'), $arElementFilter, false, false, array('ID', 'IBLOCK_ID'));
        while ($arElement = $rsElements->Fetch())
            $arElements[$arElement["ID"]] = array();
    }

    if (!empty($arElements) && $this->SKU_IBLOCK_ID && $arResult["SKU_PROPERTY_COUNT"] > 0) {
        $arSkuFilter = array(
            "IBLOCK_ID" => $this->SKU_IBLOCK_ID,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
            "=PROPERTY_" . $this->SKU_PROPERTY_ID => array_keys($arElements),
        );
        if ($this->arParams['HIDE_NOT_AVAILABLE'] == 'Y')
            $arSkuFilter['CATALOG_AVAILABLE'] = 'Y';

        $rsElements = CIBlockElement::GetPropertyValues($this->SKU_IBLOCK_ID, $arSkuFilter, false, array('ID' => $this->arResult["SKU_PROPERTY_ID_LIST"]));

        while ($arSku = $rsElements->Fetch()) {
            foreach ($arResult["ITEMS"] as $PID => $arItem) {
                if (isset($arSku[$PID]) && $arSku[$this->SKU_PROPERTY_ID] > 0) {
                    if (is_array($arSku[$PID])) {
                        foreach ($arSku[$PID] as $value)
                            $arElements[$arSku[$this->SKU_PROPERTY_ID]][$PID][] = $value;
                    } else {
                        $arElements[$arSku[$this->SKU_PROPERTY_ID]][$PID][] = $arSku[$PID];
                    }
                }
            }
        }
    }

    CTimeZone::Disable();
    $uniqTest = array();

    foreach ($arElements as $arElement) {
        $propertyValues = $propertyEmptyValuesCombination;
        $uniqStr = '';
        foreach ($arResult["ITEMS"] as $PID => $arItem) {
            if (is_array($arElement[$PID])) {
                foreach ($arElement[$PID] as $value) {
                    $temporary = explode("|", $value);
                    if (count($temporary) == 2) {
                        $value = $temporary[0];
                        if ($value !== false && !empty($value) && $value !== '0' && $value !== 0) {
                            $key = $this->fillItemValues($arResult["ITEMS"][$PID], $value);

                            if ($arResult["ITEMS"][$PID]["VALUES"]["MIN"]["VALUE"] == $value) {
                                $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["FACTOR"] = unserialize($temporary[1])[1];

                                $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["OLD"]["FACTOR"] = unserialize($temporary[1])[1];
                                $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["OLD"]["VALUE"] = $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["VALUE"];
                            }
                            if ($arResult["ITEMS"][$PID]["VALUES"]["MAX"]["VALUE"] == $value) {
                                $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["FACTOR"] = unserialize($temporary[1])[1];

                                $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["OLD"]["FACTOR"] = unserialize($temporary[1])[1];
                                $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["OLD"]["VALUE"] = $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["VALUE"];
                            }

                            $propertyValues[$PID][$key] = $arResult["ITEMS"][$PID]["VALUES"][$key]["VALUE"];
                            $uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
                        }
                    } else {
                        $key = $this->fillItemValues($arResult["ITEMS"][$PID], $value);
                        $propertyValues[$PID][$key] = $arResult["ITEMS"][$PID]["VALUES"][$key]["VALUE"];
                        $uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
                    }
                    //$key = $this->fillItemValues($arResult["ITEMS"][$PID], $value);
                    //$propertyValues[$PID][$key] = $arResult["ITEMS"][$PID]["VALUES"][$key]["VALUE"];
                    //$uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
                }
            } elseif ($arElement[$PID] !== false && !empty($arElement[$PID]) && $arElement[$PID] !== '0' && $arElement[$PID] !== 0) {
                $temporary = explode("|", $arElement[$PID]);
                if (count($temporary) == 2) {
                    $arElement[$PID] = $temporary[0];
                    if ($arElement[$PID] !== false && !empty($arElement[$PID]) && $arElement[$PID] !== '0' && $arElement[$PID] !== 0) {
                        $key = $this->fillItemValues($arResult["ITEMS"][$PID], $arElement[$PID]);

                        if ($arResult["ITEMS"][$PID]["VALUES"]["MIN"]["VALUE"] == $arElement[$PID]) {
                            $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["FACTOR"] = unserialize($temporary[1])[1];

                            $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["OLD"]["FACTOR"] = unserialize($temporary[1])[1];
                            $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["OLD"]["VALUE"] = $arResult["ITEMS"][$PID]["VALUES"]["MIN"]["VALUE"];
                        }
                        if ($arResult["ITEMS"][$PID]["VALUES"]["MAX"]["VALUE"] == $arElement[$PID]) {
                            $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["FACTOR"] = unserialize($temporary[1])[1];

                            $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["OLD"]["FACTOR"] = unserialize($temporary[1])[1];
                            $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["OLD"]["VALUE"] = $arResult["ITEMS"][$PID]["VALUES"]["MAX"]["VALUE"];
                        }

                        $propertyValues[$PID][$key] = $arResult["ITEMS"][$PID]["VALUES"][$key]["VALUE"];
                        $uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
                    }
                } else {
                    $key = $this->fillItemValues($arResult["ITEMS"][$PID], $arElement[$PID]);
                    $propertyValues[$PID][$key] = $arResult["ITEMS"][$PID]["VALUES"][$key]["VALUE"];
                    $uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
                }
            }
        }

        $uniqCheck = md5($uniqStr);
        if (isset($uniqTest[$uniqCheck]))
            continue;
        $uniqTest[$uniqCheck] = true;

        $this->ArrayMultiply($arResult["COMBO"], $propertyValues);
    }
    CTimeZone::Enable();

    $arSelect = array("ID", "IBLOCK_ID");
    foreach ($arResult["PRICES"] as &$value) {
        if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
            continue;
        $arSelect[] = $value["SELECT"];
        $arFilter["CATALOG_SHOP_QUANTITY_" . $value["ID"]] = 1;
    }
    unset($value);

    $rsElements = CIBlockElement::GetList(array(), $arElementFilter, false, false, $arSelect);
    while ($arElement = $rsElements->Fetch()) {
        foreach ($arResult["PRICES"] as $NAME => $arPrice)
            if (isset($arResult["ITEMS"][$NAME]))
                $this->fillItemPrices($arResult["ITEMS"][$NAME], $arElement);
    }

    if (isset($arSkuFilter)) {
        $rsElements = CIBlockElement::GetList(array(), $arSkuFilter, false, false, $arSelect);
        while ($arSku = $rsElements->Fetch()) {
            foreach ($arResult["PRICES"] as $NAME => $arPrice)
                if (isset($arResult["ITEMS"][$NAME]))
                    $this->fillItemPrices($arResult["ITEMS"][$NAME], $arSku);
        }
    }

    foreach ($arResult["ITEMS"] as $PID => $arItem)
        uasort($arResult["ITEMS"][$PID]["VALUES"], array($this, "_sort"));
}

if ($arParams["XML_EXPORT"] === "Y") {
    $arResult["SECTION_TITLE"] = "";
    $arResult["SECTION_DESCRIPTION"] = "";

    if ($this->SECTION_ID > 0) {
        $arSelect = array("ID", "IBLOCK_ID", "LEFT_MARGIN", "RIGHT_MARGIN");
        if ($arParams["SECTION_TITLE"] !== "")
            $arSelect[] = $arParams["SECTION_TITLE"];
        if ($arParams["SECTION_DESCRIPTION"] !== "")
            $arSelect[] = $arParams["SECTION_DESCRIPTION"];

        $sectionList = CIBlockSection::GetList(array(), array(
            "=ID" => $this->SECTION_ID,
            "IBLOCK_ID" => $this->IBLOCK_ID,
        ), false, $arSelect);
        $arResult["SECTION"] = $sectionList->GetNext();

        if ($arResult["SECTION"]) {
            $arResult["SECTION_TITLE"] = $arResult["SECTION"][$arParams["SECTION_TITLE"]];
            if ($arParams["SECTION_DESCRIPTION"] !== "") {
                $obParser = new CTextParser;
                $arResult["SECTION_DESCRIPTION"] = $obParser->html_cut($arResult["SECTION"][$arParams["SECTION_DESCRIPTION"]], 200);
            }
        }
    }
}

$allCHECKED = array();

$facetIndex = array();

foreach ($arResult["ITEMS"] as $PID => $arItem) {
    foreach ($arItem["VALUES"] as $key => $ar) {
        if (
            isset($_CHECK[$ar["CONTROL_NAME"]])
            || (
                isset($_CHECK[$ar["CONTROL_NAME_ALT"]])
                && $_CHECK[$ar["CONTROL_NAME_ALT"]] == $ar["HTML_VALUE_ALT"]
            )
        ) {
            if ($arItem["PROPERTY_TYPE"] == "N") {
                $arResult["ITEMS"][$PID]["VALUES"][$key]["HTML_VALUE"] = htmlspecialcharsbx($_CHECK[$ar["CONTROL_NAME"]]);
                $arResult["ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
            } elseif (isset($arItem["PRICE"])) {
                $arResult["ITEMS"][$PID]["VALUES"][$key]["HTML_VALUE"] = htmlspecialcharsbx($_CHECK[$ar["CONTROL_NAME"]]);
                $arResult["ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
            } elseif ($arItem["DISPLAY_TYPE"] == "U") {
                $arResult["ITEMS"][$PID]["VALUES"][$key]["HTML_VALUE"] = htmlspecialcharsbx($_CHECK[$ar["CONTROL_NAME"]]);
                $arResult["ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
            } elseif ($_CHECK[$ar["CONTROL_NAME"]] == $ar["HTML_VALUE"]) {
                $arResult["ITEMS"][$PID]["VALUES"][$key]["CHECKED"] = true;
                $arResult["ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
                $allCHECKED[$PID][$ar["VALUE"]] = true;
            } elseif ($_CHECK[$ar["CONTROL_NAME_ALT"]] == $ar["HTML_VALUE_ALT"]) {
                $arResult["ITEMS"][$PID]["VALUES"][$key]["CHECKED"] = true;
                $arResult["ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
                $allCHECKED[$PID][$ar["VALUE"]] = true;
            }
        }
    }
}

if ($arParams["SAVE_IN_SESSION"]) {
    $_SESSION[$FILTER_NAME][$this->SECTION_ID] = $_CHECK;
}

$arResult["JS_FILTER_PARAMS"] = array();
if ($arParams["SEF_MODE"] == "Y") {
    $section = false;
    if ($this->SECTION_ID > 0) {
        $sectionList = CIBlockSection::GetList(array(), array(
            "=ID" => $this->SECTION_ID,
            "IBLOCK_ID" => $this->IBLOCK_ID,
        ), false, array("ID", "IBLOCK_ID", "SECTION_PAGE_URL"));
        $sectionList->SetUrlTemplates($arParams["SEF_RULE"]);
        $section = $sectionList->GetNext();
    }

    if ($section) {
        $url = $section["DETAIL_PAGE_URL"];
    } else {
        $url = CIBlock::ReplaceSectionUrl($arParams["SEF_RULE"], array());
    }

    $arResult["JS_FILTER_PARAMS"]["SEF_SET_FILTER_URL"] = $this->makeSmartUrl($url, true);
    $arResult["JS_FILTER_PARAMS"]["SEF_DEL_FILTER_URL"] = $this->makeSmartUrl($url, false);
}

$uri = new \Bitrix\Main\Web\Uri($this->request->getRequestUri());
$uri->deleteParams(\Bitrix\Main\HttpRequest::getSystemParameters());
$pageURL = $uri->GetUri();
$paramsToDelete = array("set_filter", "del_filter", "ajax", "bxajaxid", "AJAX_CALL", "mode");
foreach ($arResult["ITEMS"] as $PID => $arItem) {
    foreach ($arItem["VALUES"] as $key => $ar) {
        $paramsToDelete[] = $ar["CONTROL_NAME"];
        $paramsToDelete[] = $ar["CONTROL_NAME_ALT"];
    }
}

$clearURL = CHTTP::urlDeleteParams($pageURL, $paramsToDelete, array("delete_system_params" => true));

if ($arResult["JS_FILTER_PARAMS"]["SEF_SET_FILTER_URL"]) {
    $arResult["FILTER_URL"] = $arResult["JS_FILTER_PARAMS"]["SEF_SET_FILTER_URL"];
    $arResult["FILTER_AJAX_URL"] = htmlspecialcharsbx(CHTTP::urlAddParams($arResult["FILTER_URL"], array(
        "bxajaxid" => $_GET["bxajaxid"],
    ), array(
        "skip_empty" => true,
        "encode" => true,
    )));
    $arResult["SEF_SET_FILTER_URL"] = $arResult["JS_FILTER_PARAMS"]["SEF_SET_FILTER_URL"];
    $arResult["SEF_DEL_FILTER_URL"] = $arResult["JS_FILTER_PARAMS"]["SEF_DEL_FILTER_URL"];
} else {
    $paramsToAdd = array(
        "set_filter" => "y",
    );
    foreach ($arResult["ITEMS"] as $PID => $arItem) {
        foreach ($arItem["VALUES"] as $key => $ar) {
            if (isset($_CHECK[$ar["CONTROL_NAME"]])) {
                if ($arItem["PROPERTY_TYPE"] == "N" || isset($arItem["PRICE"]))
                    $paramsToAdd[$ar["CONTROL_NAME"]] = $_CHECK[$ar["CONTROL_NAME"]];
                elseif ($_CHECK[$ar["CONTROL_NAME"]] == $ar["HTML_VALUE"])
                    $paramsToAdd[$ar["CONTROL_NAME"]] = $_CHECK[$ar["CONTROL_NAME"]];
            } elseif (isset($_CHECK[$ar["CONTROL_NAME_ALT"]])) {
                if ($_CHECK[$ar["CONTROL_NAME_ALT"]] == $ar["HTML_VALUE_ALT"])
                    $paramsToAdd[$ar["CONTROL_NAME_ALT"]] = $_CHECK[$ar["CONTROL_NAME_ALT"]];
            }
        }
    }

    $arResult["FILTER_URL"] = htmlspecialcharsbx(CHTTP::urlAddParams($clearURL, $paramsToAdd, array(
        "skip_empty" => true,
        "encode" => true,
    )));

    $arResult["FILTER_AJAX_URL"] = htmlspecialcharsbx(CHTTP::urlAddParams($clearURL, $paramsToAdd + array(
            "bxajaxid" => $_GET["bxajaxid"],
        ), array(
        "skip_empty" => true,
        "encode" => true,
    )));
}

if (isset($_REQUEST["ajax"]) && $_REQUEST["ajax"] === "y") {
    $arFilter = $this->makeFilter($FILTER_NAME);
    $arResult["ELEMENT_COUNT"] = CIBlockElement::GetList(array(), $arFilter, array(), false);

    if (isset($_GET["bxajaxid"])) {
        $arResult["COMPONENT_CONTAINER_ID"] = htmlspecialcharsbx("comp_" . $_GET["bxajaxid"]);
        if ($arParams["INSTANT_RELOAD"])
            $arResult["INSTANT_RELOAD"] = true;
    }
}

if (
    !empty($arParams["PAGER_PARAMS_NAME"])
    && preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PAGER_PARAMS_NAME"])
) {
    if (!is_array($GLOBALS[$arParams["PAGER_PARAMS_NAME"]]))
        $GLOBALS[$arParams["PAGER_PARAMS_NAME"]] = array();

    if ($arResult["JS_FILTER_PARAMS"]["SEF_SET_FILTER_URL"]) {
        $GLOBALS[$arParams["PAGER_PARAMS_NAME"]]["BASE_LINK"] = $arResult["JS_FILTER_PARAMS"]["SEF_SET_FILTER_URL"];
    } elseif (count($paramsToAdd) > 1) {
        $GLOBALS[$arParams["PAGER_PARAMS_NAME"]] = array_merge($GLOBALS[$arParams["PAGER_PARAMS_NAME"]], $paramsToAdd);
    }
}

$arInputNames = array();
foreach ($arResult["ITEMS"] as $PID => $arItem) {
    foreach ($arItem["VALUES"] as $key => $ar) {
        $arInputNames[$ar["CONTROL_NAME"]] = true;
        $arInputNames[$ar["CONTROL_NAME_ALT"]] = true;
    }
}
$arInputNames["set_filter"] = true;
$arInputNames["del_filter"] = true;

$arSkip = array(
    "AUTH_FORM" => true,
    "TYPE" => true,
    "USER_LOGIN" => true,
    "USER_CHECKWORD" => true,
    "USER_PASSWORD" => true,
    "USER_CONFIRM_PASSWORD" => true,
    "USER_EMAIL" => true,
    "captcha_word" => true,
    "captcha_sid" => true,
    "login" => true,
    "Login" => true,
    "backurl" => true,
    "ajax" => true,
    "mode" => true,
    "bxajaxid" => true,
    "AJAX_CALL" => true,
);

$arResult["FORM_ACTION"] = $clearURL;
$arResult["HIDDEN"] = array();
foreach (array_merge($_GET, $_POST) as $key => $value) {
    if (
        !isset($arInputNames[$key])
        && !isset($arSkip[$key])
        && !is_array($value)
    ) {
        $arResult["HIDDEN"][] = array(
            "CONTROL_ID" => htmlspecialcharsbx($key),
            "CONTROL_NAME" => htmlspecialcharsbx($key),
            "HTML_VALUE" => htmlspecialcharsbx($value),
        );
    }
}

if (
    $arParams["XML_EXPORT"] === "Y"
    && $arResult["SECTION"]
    && ($arResult["SECTION"]["RIGHT_MARGIN"] - $arResult["SECTION"]["LEFT_MARGIN"]) === 1
) {
    $exportUrl = CHTTP::urlAddParams($clearURL, array("mode" => "xml"));
    $APPLICATION->AddHeadString('<meta property="ya:interaction" content="XML_FORM" />');
    $APPLICATION->AddHeadString('<meta property="ya:interaction:url" content="' . CHTTP::urn2uri($exportUrl) . '" />');
}

if ($arParams["XML_EXPORT"] === "Y" && $_REQUEST["mode"] === "xml") {
    $this->setFrameMode(false);
    ob_start();
    $this->IncludeComponentTemplate("xml");
    $xml = ob_get_contents();
    $APPLICATION->RestartBuffer();
    while (ob_end_clean()) ;
    header("Content-Type: text/xml; charset=utf-8");
    $error = "";
    echo \Bitrix\Main\Text\Encoding::convertEncoding($xml, LANG_CHARSET, "utf-8", $error);
    CMain::FinalActions();
    die();
} elseif (isset($_REQUEST["ajax"]) && $_REQUEST["ajax"] === "y") {
    $this->setFrameMode(false);
    define("BX_COMPRESSION_DISABLED", true);
    ob_start();
    $this->IncludeComponentTemplate("ajax");
    $json = ob_get_contents();
    $APPLICATION->RestartBuffer();
    while (ob_end_clean()) ;
    header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);
    CMain::FinalActions();
    echo $json;
    die();
} else {
    $this->IncludeComponentTemplate();
}