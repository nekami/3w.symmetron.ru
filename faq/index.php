<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("FAQ");
?><div class="wrapper">
 <br>
	 &nbsp; &nbsp;&nbsp;<?$APPLICATION->IncludeComponent("bitrix:support.faq", ".default", Array(
	"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
		"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
		"IBLOCK_ID" => "12",	// Список инфоблоков
		"IBLOCK_TYPE" => "faq",	// Типы инфоблоков
		"PATH_TO_USER" => "",	// Шаблон пути к странице пользователя
		"RATING_TYPE" => "",	// Вид кнопок рейтинга
		"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
		"SHOW_RATING" => "",	// Включить рейтинг
		"COMPONENT_TEMPLATE" => ".default",
		"SECTION" => "11967",	// Список секций
		"EXPAND_LIST" => "Y",	// Показывать вложенные секции
		"SEF_FOLDER" => "/faq/",	// Каталог ЧПУ (относительно корня сайта)
                "SHOW_404" => "Y",
                "SET_STATUS_404" => "Y",
		"SEF_URL_TEMPLATES" => array(
			"faq" => "",
			"section" => "#SECTION_ID#/",
			"detail" => "#SECTION_ID#/#ELEMENT_ID#",
		)
	),
	false
);?><br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>