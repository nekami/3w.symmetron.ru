<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("manufacturer");
?>
<div class="series-wrapper">
    <div class="news">
        <h2 class="news__title">Новости Infineon</h2>
        <div class="filter filter-news">
            <div class="filter-head">
                <span class="filter-head__button"></span>
                <p>Фильтр новостей</p>
            </div>
            <div class="filter-wrapper">
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Производитель</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check1">
                            <label for="check1">Analog Devices evices evices evices evices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check2">
                            <label for="check2">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check3">
                            <label for="check3">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Раздел каталога</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check5">
                            <label for="check5">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check6">
                            <label for="check6">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check7">
                            <label for="check7">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Тема</p>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check9">
                            <label for="check9">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check10">
                            <label for="check10">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check11">
                            <label for="check11">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Серия</p>
                        <div class="filter-element-head__number">10</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check12">
                            <label for="check12">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check13">
                            <label for="check13">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check14">
                            <label for="check14">Infenion</label>
                        </div>
                    </div>
                </div>
                <button type="button" class="filter-show-more">Показать 8 статей</button>
                <div class="filter-bottom">
                    <span class="filter-bottom-close"></span>
                    <p class="filter-bottom-text">Сбросить все фильтры</p>
                </div>
            </div>
        </div>
        <div class="news-element">
            <span class="news-element__date">13 июня 2018</span>
            <h3 class="news-elements__title">
                <a href="#">Infineon укрепляет сотрудничество с DENSO,
                    став акционером</a>
            </h3>
            <p class="news-elements__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
                передовых автомобильных технологий, систем и компонентов для поддержки своего
                автомобильного бизнеса</p>
            <div class="cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>

        <div class="news-element">
            <span class="news-element__date">05 сентября 2018</span>
            <h3 class="news-elements__title">
                <a href="#">Infineon приобретает Siltecra - эксперта
                    по карбиду кремния</a>
            </h3>
            <p class="news-elements__text">Infineon Techolojies AG приобретает Siltecra GmbH,
                стартап, основанный в Дрездене. В процессе запуска была разработана инновационная технология
                (Cold Split) для эффективной обработки кристаллического материала с минимальной потерей
                материала.</p>
            <div class="cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>

        <div class="news-element">
            <span class="news-element__date">05 ноября 2018</span>
            <h3 class="news-elements__title">
                <a href="#">Infineon на выставке electronica 2018</a>
            </h3>
            <p class="news-elements__text">Infineon Techolojies AG дает ответы на
                основные вопросы, связанные с оцифровкой: как можно производить чистку энергии
                и как быстро и с небольшими потерями попадает в электромобиль?</p>
            <div class="cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>
        <ul class="pagination">
            <li class="pagination-arrow">
                <img class="pagination-arrow-left" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
            </li>
            <li class="pagination-page">1</li>
            <li class="pagination-page pagination-page-active">2</li>
            <li class="pagination-page">3</li>
            <li class="pagination-page">4</li>
            <li class="pagination-page">5</li>
            <li class="pagination-arrow">
                <img class="pagination-arrow-right" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
            </li>
        </ul>
        <a class="show-more-news" href="#">Все новости Infineon</a>
    </div>

    <div class="articles">
        <h2 class="articles__title">Статьи</h2>
        <div class="filter filter-article">
            <div class="filter-head">
                <span class="filter-head__button"></span>
                <p>Фильтр статей</p>
            </div>
            <div class="filter-wrapper">
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Производитель</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check20">
                            <label for="check20">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check21">
                            <label for="check21">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check22">
                            <label for="check22">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Раздел каталога</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check23">
                            <label for="check23">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check24">
                            <label for="check24">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check25">
                            <label for="check25">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Тема</p>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check26">
                            <label for="check26">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check27">
                            <label for="check27">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check28">
                            <label for="check28">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Серия</p>
                        <div class="filter-element-head__number">10</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check29">
                            <label for="check29">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check30">
                            <label for="check30">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check31">
                            <label for="check31">Infenion</label>
                        </div>
                    </div>
                </div>
                <button type="button" class="filter-show-more">Показать 8 статей</button>
                <div class="filter-bottom">
                    <span class="filter-bottom-close"></span>
                    <p class="filter-bottom-text">Сбросить все фильтры</p>
                </div>
            </div>
        </div>
        <div class="articles-element">
            <ul class="articles-element-head">
                <li class="articles-element-head__date">13 июня 2018</li>
                <li class="articles-element-head__link">
                    <a href="#">Рекомендации к применению</a>
                </li>
                <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
                <li class="article-element-head__duration">Приблизительное время чтения: 7 минут</li>
            </ul>
            <h3 class="articles-element__title">
                <a href="#">О пользе использования эффективных компонентов</a>
            </h3>
            <p class="articles-element__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком передовых
                автомобильных технологий, систем и компонентов для поддержки своего автомобильного бизнеса.</p>
            <div class="article-cloud-tags cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>

        <div class="articles-element">
            <ul class="articles-element-head">
                <li class="articles-element-head__date">13 июня 2018</li>
                <li class="articles-element-head__link">
                    <a href="#">Новые технологии</a>
                </li>
                <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
                <li class="article-element-head__duration">Приблизительное время чтения: 12 минут</li>
            </ul>
            <h3 class="articles-element__title">
                <a href="#">Новые 7</a>
            </h3>
            <p class="articles-element__text">Infineon Techolojies AG приобретает Siltectra GmpH стартап, основанный в Дрездене.
                В процессе запуска была разработана инновационная технология (Cold Split) для эффективной обработки
                кристаллического материала и с минимальной потерей материала.</p>
            <div class="article-cloud-tags cloud-tags">
                <a href="#" class="cloud-tags__link">Инновации</a>
                <a href="#" class="cloud-tags__link">Cold Split</a>
                <a href="#" class="cloud-tags__link">Infineon</a>
                <a href="#" class="cloud-tags__link">Siltectra</a>
                <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
            </div>
        </div>

        <div class="articles-element">
            <ul class="articles-element-head">
                <li class="articles-element-head__date">13 июня 2018</li>
                <li class="articles-element-head__link">
                    <a href="#">Аналитика</a>
                </li>
                <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
                <li class="article-element-head__duration">Приблизительное время чтения: 5 минут</li>
            </ul>
            <h3 class="articles-element__title">
                <a href="#">5 факторов, влияющих на производительность систем</a>
            </h3>
            <p class="articles-element__text">Infineon Techolojies AG дает ответы на
                основные вопросы, связанные с оцифровкой: как можно производить чистку энергии
                и как быстро и с небольшими потерями попадает в электромобиль?</p>
            <div class="article-cloud-tags cloud-tags">
                <a href="#" class="cloud-tags__link">Чистка энергии</a>
                <a href="#" class="cloud-tags__link">Оцифровка</a>
                <a href="#" class="cloud-tags__link">Infineon</a>
                <a href="#" class="cloud-tags__link">Автомобильное применение</a>
                <a href="#" class="cloud-tags__link">Снижение потерь</a>
            </div>
        </div>
        <ul class="pagination">
            <li class="pagination-arrow">
                <img class="pagination-arrow-left" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
            </li>
            <li class="pagination-page">1</li>
            <li class="pagination-page pagination-page-active">2</li>
            <li class="pagination-page">3</li>
            <li class="pagination-page">4</li>
            <li class="pagination-page">5</li>
            <li class="pagination-arrow">
                <img class="pagination-arrow-right" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
            </li>
        </ul>
    </div>

    <div class="events">
        <h2 class="events__title">Ближайшие мероприятия Infineon</h2>
        <div class="events-wrapper">
            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Декабрь 2018</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>12</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Семинар Infineon. Автомобильные технологии, системы и компоненты</a>
                    </h4>
                    <p class="event-description__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
                        передовых автомобильных технологий, систем и компонентов для поддержки своего автомобильного
                        бизнеса.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Инновации</a>
                        <a href="#" class="cloud-tags__link">Cold Split</a>
                        <a href="#" class="cloud-tags__link">Infineon</a>
                        <a href="#" class="cloud-tags__link">Siltectra</a>
                        <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Symmetron</span>
                    <span class="event-address__place">Ленинградское шоссе, 69 корпус 1, бизнес-парк River City
            (м. Речной вокзал)</span>
                    <span class="event-address__phone">Тел: <a href="tel:84959612020">(495) 961-2020</a></span>
                    <p class="event-address__price">Бесплатно</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация</a>
                </div>
            </div>

            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Январь 2019</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>19</span>
                            <span>24</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Выставка "Свет и технологии"</a>
                    </h4>
                    <p class="event-description__text">Главная выставка индустрии промышленного, выставочного, бизнес
                        и освещения общественных пространств. На выставке Infineon покажет последние разработки, позволяющие
                        оптимизировать технологические цепочки и снизить издержки как бизнеса, так и потребителя.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Активные компоненты</a>
                        <a href="#" class="cloud-tags__link">Светодиоды</a>
                        <a href="#" class="cloud-tags__link">Samsung</a>
                        <a href="#" class="cloud-tags__link">Разработка функционала</a>
                        <a href="#" class="cloud-tags__link">DENSO</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Экспоцентр</span>
                    <span class="event-address__place">Краснопресненская наб., 14</span>
                    <span class="event-address__phone">Тел: <a href="tel:88007073799">8(800) 9707-37-99</a></span>
                    <p class="event-address__price">600 ₽</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация обязательна</a>
                </div>
            </div>
        </div>
    </div>
    <div class="series-video">
        <h1 class="series-video__title">Видео Infineon</h1>
        <div class="series-video-wrapper">
            <div class="series-video-item">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/VMEUZevTZyA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <span class="series-video-item__date">13 июня 2018</span>
                <h4 class="series-video-item__name">Название видео</h4>
                <p class="series-video-item__description">Описание видеоролика длинный текст очень длинный текст
                очень длинный текст</p>
            </div>

            <div class="series-video-item">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/VMEUZevTZyA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <span class="series-video-item__date">24 ноября 2018</span>
                <h4 class="series-video-item__name">Название видео</h4>
                <p class="series-video-item__description">Описание видеоролика</p>
            </div>
        </div>
    </div>

    <div class="documents">
        <div class="documents-head">
            <h2 class="documents__title">Документация</h2>
            <span class="documents-head-show">раскрыть все</span>
        </div>

        <div class="docs-wrapper">
            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Брошюры</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Руководства по подбору компонента</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Руководства по применению</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Технические спецификации</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/doc-icon.png">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="series">
        <h1 class="series__title">Серии</h1>
        <div class="series-list">
            <div class="series__item">
                <a class="series__item-link" href="#" target="_blank">EDxxxx</a>
                <span class="series__item-name">Микросхемы драйверов</span>
                <p class="series__item-description">The Vybrid VF3xx family is a single-core
                (Arm Cortex-A5) solution in 176 LQFP</p>
            </div>

            <div class="series__item">
                <a class="series__item-link" href="#" target="_blank">BFPxxxx</a>
                <span class="series__item-name">ВЧ и СВЧ компоненты</span>
                <p class="series__item-description">The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP</p>
            </div>

            <div class="series__item">
                <a class="series__item-link" href="#" target="_blank">BSC-xxx-N-xxx</a>
                <span class="series__item-name">Микросхемы драйверов</span>
                <p class="series__item-description">The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP</p>
            </div>

            <div class="series__item">
                <a class="series__item-link" href="#" target="_blank">BTTxxx</a>
                <span class="series__item-name">Интеллектуальные ключи</span>
                <p class="series__item-description">The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP</p>
            </div>

            <div class="series__item">
                <a class="series__item-link" href="#" target="_blank">XHP серия</a>
                <span class="series__item-name">IGBT-модули</span>
                <p class="series__item-description">The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP</p>
            </div>

            <div class="series__item">
                <a class="series__item-link" href="#" target="_blank">XIDCxxDxxx</a>
                <span class="series__item-name">Диод-кристаллы</span>
                <p class="series__item-description">The Vybrid VF3xx family is a single-core
                    (Arm Cortex-A5) solution in 176 LQFP</p>
            </div>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>