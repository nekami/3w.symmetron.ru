<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test");
?>

<!--<h2 class="title-page">Характеристики и особенности применения драйверов
    MOSFET и IGBT</h2>
<div class="wrapper wrapper-new">
    <ul class="breadcrumbs-items bx-breadcrumb">
        <li class="bx-breadcrumb-item">
            <a href="#">
                <svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.458 69.124931" xml:space="preserve" id="svg2" version="1.1"><metadata id="metadata8"><rdf:rdf><cc:work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"></dc:type></cc:work></rdf:rdf></metadata><defs id="defs6"><clipPath id="clipPath18" clipPathUnits="userSpaceOnUse"><path id="path16" d="M 0,51.844 H 64.843 V 0 H 0 Z"></path></clipPath></defs><g transform="matrix(1.3333333,0,0,-1.3333333,0,69.124933)" id="g10"><g id="g12"><g clip-path="url(#clipPath18)" id="g14"><g transform="translate(48.2774,22.1616)" id="g20"><path id="path22" style="fill:none;stroke:#008aff;stroke-width:5.31099987;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" d="M 0,0 V -19.506 H -31.711 V 0"></path></g><g transform="translate(3.0636,22.7285)" id="g24"><path id="path26" style="fill:none;stroke:#008aff;stroke-width:6.12699986;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" d="M 0,0 29.358,26.052 58.716,0"></path></g></g></g></g></svg>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Производители</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Infineon Technologies</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Применения</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Силовая электроника</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Статьи</span>
            </a>
        </li>
    </ul>
</div>
<p class="page-text">Изолированный источник требуется для каждого транзистора верхнего плеча.
    Необходим высоковольтный быстродействующий каскад сдвига уровня. Простое и дешевое
    решение. Длительность управления испульса ограничена.</p>-->
<div class="event event-detail">

<div class="events-detail">
    <div class="event-item">
        <div class="event-date">
            <div class="event-date__month">
                <p>Декабрь 2018</p>
            </div>
            <div class="event-date__day">
                <div class="event-date__day-wrapper">
                    <span>12</span>
                </div>
            </div>
        </div>
        <div class="event-description">
            <h4 class="event-description__name">
                <a href="#">Семинар Infineon. Автомобильные технологии, системы и компоненты</a>
            </h4>
            <p class="event-description__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
                передовых автомобильных технологий, систем и компонентов для поддержки своего автомобильного
                бизнеса.</p>
            <div class="event-cloud-tags cloud-tags">
                <a href="#" class="cloud-tags__link">Инновации</a>
                <a href="#" class="cloud-tags__link">Cold Split</a>
                <a href="#" class="cloud-tags__link">Infineon</a>
                <a href="#" class="cloud-tags__link">Siltectra</a>
                <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
            </div>
            <div class="event-content">
                <h4 class="event-content__title">Темы семинара:</h4>
                <ul class="event-theme-list">
                    <li class="event-theme-item">обзор новых мощных IGBT-модулей Infineon;</li>
                    <li class="event-theme-item">технические аспекты применения IGBT-модулей: многоуровневые
                        топологии, транспортное и индустриальное термоциклирование, теплоотвод и
                        расчет тепловых режимов в IPOSIM;</li>
                    <li class="event-theme-item">дополнительные темы по силовым компонентам Infineon
                        на выбор участников семинара, а также обсуждение вопросов, технических задач и
                        проектов участников семинара;</li>
                </ul>
                <div class="speakers">
                    <div class="speakers__item">
                        <div class="speakers__item-img">
                            <img src="/local/templates/symmetron_default/images/photo-speaker-1.png">
                        </div>
                        <span class="speakers__item-name">Докладчик:
                            <b>Пётр Луневский,</b>
                        </span>
                        <p class="speakers__item-descr">ведущий специалист Infineon Technologies</p>
                    </div>

                    <div class="speakers__item">
                        <div class="speakers__item-img">
                            <img src="/local/templates/symmetron_default/images/photo-speaker-2.png">
                        </div>
                        <span class="speakers__item-name">Перевод:
                            <b>Евгений Обжерин,</b>
                        </span>
                        <p class="speakers__item-descr">к.т.н., инженер по применению продуктов
                            Infineon Technologies</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="event-address">
            <span class="event-address__city">Москва, Symmetron</span>
            <span class="event-address__place">Ленинградское шоссе, 69 корпус 1, бизнес-парк River City
            (м. Речной вокзал)</span>
            <span class="event-address__phone">Тел: <a href="tel:84959612020">(495) 961-2020</a></span>
            <p class="event-address__price">Бесплатно</p>
            <a href="#" class="event-address__reg" target="_blank">Регистрация</a>
        </div>
    </div>
</div>
</div>

<div class="articles">
    <h2 class="articles__title">Статьи</h2>
    <div class="filter filter-article">
        <div class="filter-head">
            <span class="filter-head__button"></span>
            <p>Фильтр статей</p>
        </div>
        <div class="filter-wrapper">
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Производитель</p>
                    <div class="filter-element-head__number">2</div>
                    <span class="filter-element-head__close"></span>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check20">
                        <label for="check20">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check21">
                        <label for="check21">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check22">
                        <label for="check22">Infenion</label>
                    </div>
                </div>
            </div>
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Раздел каталога</p>
                    <div class="filter-element-head__number">2</div>
                    <span class="filter-element-head__close"></span>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check23">
                        <label for="check23">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check24">
                        <label for="check24">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check25">
                        <label for="check25">Infenion</label>
                    </div>
                </div>
            </div>
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Тема</p>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check26">
                        <label for="check26">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check27">
                        <label for="check27">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check28">
                        <label for="check28">Infenion</label>
                    </div>
                </div>
            </div>
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Серия</p>
                    <div class="filter-element-head__number">10</div>
                    <span class="filter-element-head__close"></span>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check29">
                        <label for="check29">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check30">
                        <label for="check30">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check31">
                        <label for="check31">Infenion</label>
                    </div>
                </div>
            </div>
            <button type="button" class="filter-show-more">Показать 8 статей</button>
            <div class="filter-bottom">
                <span class="filter-bottom-close"></span>
                <p class="filter-bottom-text">Сбросить все фильтры</p>
            </div>
        </div>
    </div>
    <div class="articles-element">
        <ul class="articles-element-head">
            <li class="articles-element-head__date">13 июня 2018</li>
            <li class="articles-element-head__link">
                <a href="#">Рекомендации к применению</a>
            </li>
            <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
            <li class="article-element-head__duration">Приблизительное время чтения: 7 минут</li>
        </ul>
        <h3 class="articles-element__title">
            <a href="#">О пользе использования эффективных компонентов</a>
        </h3>
        <p class="articles-element__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком передовых
            автомобильных технологий, систем и компонентов для поддержки своего автомобильного бизнеса.</p>
        <div class="article-cloud-tags cloud-tags">
            <a href="#" class="cloud-tags__link">Активные компоненты</a>
            <a href="#" class="cloud-tags__link">Светодиоды</a>
            <a href="#" class="cloud-tags__link">Samsung</a>
            <a href="#" class="cloud-tags__link">Разработка функционала</a>
            <a href="#" class="cloud-tags__link">DENSO</a>
        </div>
    </div>

    <div class="articles-element">
        <ul class="articles-element-head">
            <li class="articles-element-head__date">13 июня 2018</li>
            <li class="articles-element-head__link">
                <a href="#">Новые технологии</a>
            </li>
            <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
            <li class="article-element-head__duration">Приблизительное время чтения: 12 минут</li>
        </ul>
        <h3 class="articles-element__title">
            <a href="#">Новые 7</a>
        </h3>
        <p class="articles-element__text">Infineon Techolojies AG приобретает Siltectra GmpH стартап, основанный в Дрездене.
            В процессе запуска была разработана инновационная технология (Cold Split) для эффективной обработки
            кристаллического материала и с минимальной потерей материала.</p>
        <div class="article-cloud-tags cloud-tags">
            <a href="#" class="cloud-tags__link">Инновации</a>
            <a href="#" class="cloud-tags__link">Cold Split</a>
            <a href="#" class="cloud-tags__link">Infineon</a>
            <a href="#" class="cloud-tags__link">Siltectra</a>
            <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
        </div>
    </div>

    <div class="articles-element">
        <ul class="articles-element-head">
            <li class="articles-element-head__date">13 июня 2018</li>
            <li class="articles-element-head__link">
                <a href="#">Аналитика</a>
            </li>
            <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
            <li class="article-element-head__duration">Приблизительное время чтения: 5 минут</li>
        </ul>
        <h3 class="articles-element__title">
            <a href="#">5 факторов, влияющих на производительность систем</a>
        </h3>
        <p class="articles-element__text">Infineon Techolojies AG дает ответы на
            основные вопросы, связанные с оцифровкой: как можно производить чистку энергии
            и как быстро и с небольшими потерями попадает в электромобиль?</p>
        <div class="article-cloud-tags cloud-tags">
            <a href="#" class="cloud-tags__link">Чистка энергии</a>
            <a href="#" class="cloud-tags__link">Оцифровка</a>
            <a href="#" class="cloud-tags__link">Infineon</a>
            <a href="#" class="cloud-tags__link">Автомобильное применение</a>
            <a href="#" class="cloud-tags__link">Снижение потерь</a>
        </div>
    </div>
    <ul class="pagination">
        <li class="pagination-arrow">
            <img class="pagination-arrow-left" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
        </li>
        <li class="pagination-page">1</li>
        <li class="pagination-page pagination-page-active">2</li>
        <li class="pagination-page">3</li>
        <li class="pagination-page">4</li>
        <li class="pagination-page">5</li>
        <li class="pagination-arrow">
            <img class="pagination-arrow-right" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
        </li>
    </ul>
</div>

<div class="manufacturer">
    <h2 class="manufacturer__title">Производители</h2>
    <ul class="manufacturer__list">
        <li class="manufacturer__item">
            <a href="#" class="manufacturer__link">TDK Lambda</a>
        </li>
        <li class="manufacturer__item">
            <a href="#" class="manufacturer__link">Epcos</a>
        </li>
        <li class="manufacturer__item">
            <a href="#" class="manufacturer__link">TTAF</a>
        </li>
        <li class="manufacturer__item">
            <a href="#" class="manufacturer__link">Vishay</a>
        </li>
    </ul>
</div>
<section class="components">
    <table class="components-table">
        <tr class="components-head">
            <th class="components-head__code">Код товара производителя</th>
            <th class="components-head__manufacture">Производитель</th>
            <th class="components-head__descr">Описание</th>
            <th class="components-head__avail">Наличие</th>
            <th class="components-head__price">Цена от</th>
        </tr>
        <tr class="components-element">
            <td>
                <a class="components-element__link" href="#">11111111</a>
            </td>
            <td>Vishay</td>
            <td>Резистор прецизионный PTF-65 0.25 Вт</td>
            <td class="component-element__quantity">
                <span class="components-element__no-availability"></span>
                <span>Нет в наличии</span>
            </td>
            <td>100.04 руб./шт.</td>
        </tr>

        <tr class="components-element">
            <td>
                <a class="components-element__link" href="#">11111111</a>
            </td>
            <td>Vishay</td>
            <td>Резистор прецизионный PTF-65 0.25 Вт Резистор прецизионный PTF-65 0.25 Вт Резистор прецизионный PTF-65 0.25 Вт</td>
            <td class="component-element__quantity">
                <span class="components-element__no-availability"></span>
                <span>Нет в наличии</span>
            </td>
            <td>100.04 руб./шт.</td>
        </tr>

        <tr class="components-element">
            <td>
                <a class="components-element__link" href="#">11111111</a>
            </td>
            <td>Vishay</td>
            <td>Резистор прецизионный PTF-65 0.25 Вт</td>
            <td class="component-element__quantity">
                <span class="components-element__availability"></span>
                <span>В наличии</span>
            </td>
            <td>100.04 руб./шт.</td>
        </tr>
    </table>
</section>
<script>
    let filter = document.querySelector('.filter')
    if(filter) {
        let filterElements = filter.querySelectorAll('.filter-element');
        filter.addEventListener('click', (e) => {
            filterElements.forEach((item) => {
                let openList = item.querySelector('.filter-element-head__button')
                let openInput = item.querySelector('.filter-element-search')
                let checkBoxList = item.querySelector('.filter-checkbox-list')
                let closeSelect = item.querySelector('.filter-element-head__close')
                if(e.target == openList) {
                    if(openList.classList.contains('filter-element-head__button--close')) {
                        openInput.style.display = 'none'
                        checkBoxList.style.display = 'none'
                        openList.classList.remove('filter-element-head__button--close')
                    } else {
                        openInput.style.display = 'block'
                        checkBoxList.style.display = 'block'
                        openList.classList.add('filter-element-head__button--close')
                    }
                }

                if(e.target == closeSelect) {
                    item.querySelector('.filter-element-head__number').style.display = 'none'
                    closeSelect.style.display = 'none'
                }
            })
            let closeFilterList = filter.querySelector('.filter-head__button')
            let filterWrapper = filter.querySelector('.filter-wrapper')
            if(e.target == closeFilterList) {
                if(closeFilterList.classList.contains('filter-head__button--open')) {
                    filterWrapper.style.display = 'none'
                    closeFilterList.classList.remove('filter-head__button--open')
                    filter.style.paddingBottom = '0px'
                } else {
                    filterWrapper.style.display = 'block'
                    closeFilterList.classList.add('filter-head__button--open')
                    filter.style.paddingBottom = '15px'
                }
            }
        })
    }
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
