<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("new-detail");
?>
    <section class="components">
        <table class="components-table">
            <tr class="components-head">
                <th class="components-head__code">Код товара производителя</th>
                <th class="components-head__manufacture">Производитель</th>
                <th class="components-head__descr">Описание</th>
                <th class="components-head__avail">Наличие</th>
                <th class="components-head__price">Цена от</th>
            </tr>
            <tr class="components-element">
                <td>
                    <a class="components-element__link" href="#">11111111</a>
                </td>
                <td>Vishay</td>
                <td>Резистор прецизионный PTF-65 0.25 Вт</td>
                <td class="component-element__quantity">
                    <span class="components-element__no-availability"></span>
                    <span>Нет в наличии</span>
                </td>
                <td>100.04 руб./шт.</td>
            </tr>

            <tr class="components-element">
                <td>
                    <a class="components-element__link" href="#">11111111</a>
                </td>
                <td>Vishay</td>
                <td>Резистор прецизионный PTF-65 0.25 Вт Резистор прецизионный PTF-65 0.25 Вт Резистор прецизионный PTF-65 0.25 Вт</td>
                <td class="component-element__quantity">
                    <span class="components-element__no-availability"></span>
                    <span>Нет в наличии</span>
                </td>
                <td>100.04 руб./шт.</td>
            </tr>

            <tr class="components-element">
                <td>
                    <a class="components-element__link" href="#">11111111</a>
                </td>
                <td>Vishay</td>
                <td>Резистор прецизионный PTF-65 0.25 Вт</td>
                <td class="component-element__quantity">
                    <span class="components-element__availability"></span>
                    <span>В наличии</span>
                </td>
                <td>100.04 руб./шт.</td>
            </tr>
        </table>
    </section>

    <div class="manufacturer">
        <h2 class="manufacturer__title">Производители</h2>
        <ul class="manufacturer__list">
            <li class="manufacturer__item">
                <a href="#" class="manufacturer__link">TDK Lambda</a>
            </li>
            <li class="manufacturer__item">
                <a href="#" class="manufacturer__link">Epcos</a>
            </li>
            <li class="manufacturer__item">
                <a href="#" class="manufacturer__link">TTAF</a>
            </li>
            <li class="manufacturer__item">
                <a href="#" class="manufacturer__link">Vishay</a>
            </li>
        </ul>
    </div>

    <div class="events">
        <h2 class="events__title">Ближайшие мероприятия Infineon</h2>
        <div class="events-wrapper">
            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Декабрь 2018</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>12</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Семинар Infineon. Автомобильные технологии, системы и компоненты</a>
                    </h4>
                    <p class="event-description__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
                        передовых автомобильных технологий, систем и компонентов для поддержки своего автомобильного
                        бизнеса.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Инновации</a>
                        <a href="#" class="cloud-tags__link">Cold Split</a>
                        <a href="#" class="cloud-tags__link">Infineon</a>
                        <a href="#" class="cloud-tags__link">Siltectra</a>
                        <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Symmetron</span>
                    <span class="event-address__place">Ленинградское шоссе, 69 корпус 1, бизнес-парк River City
            (м. Речной вокзал)</span>
                    <span class="event-address__phone">Тел: <a href="tel:84959612020">(495) 961-2020</a></span>
                    <p class="event-address__price">Бесплатно</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация</a>
                </div>
            </div>

            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Январь 2019</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>19</span>
                            <span>24</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Выставка "Свет и технологии"</a>
                    </h4>
                    <p class="event-description__text">Главная выставка индустрии промышленного, выставочного, бизнес
                        и освещения общественных пространств. На выставке Infineon покажет последние разработки, позволяющие
                        оптимизировать технологические цепочки и снизить издержки как бизнеса, так и потребителя.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Активные компоненты</a>
                        <a href="#" class="cloud-tags__link">Светодиоды</a>
                        <a href="#" class="cloud-tags__link">Samsung</a>
                        <a href="#" class="cloud-tags__link">Разработка функционала</a>
                        <a href="#" class="cloud-tags__link">DENSO</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Экспоцентр</span>
                    <span class="event-address__place">Краснопресненская наб., 14</span>
                    <span class="event-address__phone">Тел: <a href="tel:88007073799">8(800) 9707-37-99</a></span>
                    <p class="event-address__price">600 ₽</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация обязательна</a>
                </div>
            </div>
        </div>
    </div>

    <div class="documents">
        <div class="documents-head">
            <h2 class="documents__title">Документация</h2>
            <span class="documents-head-show">раскрыть все</span>
        </div>

        <div class="docs-wrapper">
            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Брошюры</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Руководства по подбору компонента</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Руководства по применению</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="docs-item">
                <div class="docs-item__head">
                    <span class="document-control"></span>
                    <p class="docs-item__head-name">Технические спецификации</p>
                </div>
                <div class="document-list">
                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/pdf-download.svg">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>

                    <div class="document-list-element">
                        <a href="#" download>
                            <img src="/local/templates/symmetron_default/images/doc-icon.png">
                        </a>
                        <div class="document-list-element-wrapper">
                            <div class="document-element">
                                <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                                <span class="document-element__lang">English</span>
                            </div>
                            <div class="document-char">
                                <span class="document-element__date">15.11.2017</span>
                                <span class="document-element__format">pdf</span>
                                <span class="document-element__weight">4,1 MB</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script>
            let docs = document.querySelector('.documents')
            if(docs) {
                let docsItems = document.querySelectorAll('.docs-item')
                let buttonShow = document.querySelector('.documents-head-show')
                buttonShow.addEventListener('click', (e) => {
                    if(buttonShow.classList.contains('documents-head-show--open') == false) {
                        docsItems.forEach((item) => {
                            let docsList = item.querySelector('.document-list')
                            if (docsList.classList.contains('document-list--open') == false) {
                                docsList.classList.add('document-list--open')
                            }
                        })
                        e.target.innerHTML = 'свернуть все'
                        buttonShow.classList.add('documents-head-show--open')
                        docsItems.forEach((item) => {
                            item.querySelector('.document-control').classList.add('document-control--open')
                        })
                    } else {
                        docsItems.forEach((item) => {
                            let docsList = item.querySelector('.document-list')
                            if (docsList.classList.contains('document-list--open')) {
                                docsList.classList.remove('document-list--open')
                            }
                        })
                        e.target.innerHTML = 'раскрыть все'
                        buttonShow.classList.remove('documents-head-show--open')
                        docsItems.forEach((item) => {
                            item.querySelector('.document-control').classList.remove('document-control--open')
                        })
                    }
                })

                docs.addEventListener('click', (e) => {
                    docsItems.forEach((element) => {
                        let openListButton = element.querySelector('.document-control')
                        let documentList = element.querySelector('.document-list')
                        if(e.target == openListButton) {
                            if(openListButton.classList.contains('document-control--open')) {
                                openListButton.classList.remove('document-control--open')
                                documentList.classList.remove('document-list--open')
                            } else {
                                openListButton.classList.add('document-control--open')
                                documentList.classList.add('document-list--open')
                            }
                        }
                    })
                })
            }
        </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>