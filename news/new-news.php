<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("new-news");
?>
    <div class="news news-page">
        <h2 class="news__title">Новости</h2>
        <div class="filter filter-news">
            <div class="filter-news-wrapper">
            <div class="filter-head">
                <span class="filter-head__button"></span>
                <p>Фильтр новостей</p>
            </div>
            <div class="filter-wrapper">
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Производитель</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check1">
                            <label for="check1">Analog Devices evices evices evices evices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check2">
                            <label for="check2">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check3">
                            <label for="check3">Infenion</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check1">
                            <label for="check1">Analog Devices evices evices evices evices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check2">
                            <label for="check2">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check3">
                            <label for="check3">Infenion</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check1">
                            <label for="check1">Analog Devices evices evices evices evices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check2">
                            <label for="check2">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check3">
                            <label for="check3">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Раздел каталога</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check5">
                            <label for="check5">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check6">
                            <label for="check6">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check7">
                            <label for="check7">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Раздел каталога</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check5">
                            <label for="check5">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check6">
                            <label for="check6">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check7">
                            <label for="check7">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Раздел каталога</p>
                        <div class="filter-element-head__number">2</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check5">
                            <label for="check5">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check6">
                            <label for="check6">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check7">
                            <label for="check7">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Тема</p>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check9">
                            <label for="check9">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check10">
                            <label for="check10">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check11">
                            <label for="check11">Infenion</label>
                        </div>
                    </div>
                </div>
                <div class="filter-element">
                    <div class="filter-element-head">
                        <span class="filter-element-head__button"></span>
                        <p>Серия</p>
                        <div class="filter-element-head__number">10</div>
                        <span class="filter-element-head__close"></span>
                    </div>
                    <input type="text" class="filter-element-search" placeholder="Поиск">
                    <div class="filter-checkbox-list">
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check12">
                            <label for="check12">Analog Devices</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check13">
                            <label for="check13">Atmel</label>
                        </div>
                        <div class="filter-checkbox-element">
                            <input type="checkbox" id="check14">
                            <label for="check14">Infenion</label>
                        </div>
                    </div>
                </div>
                <button type="button" class="filter-show-more">Показать 8 статей</button>
                <div class="filter-bottom">
                    <span class="filter-bottom-close"></span>
                    <p class="filter-bottom-text">Сбросить все фильтры</p>
                </div>
            </div>
            </div>
        </div>
        <div class="news-element-wrapper">
        <div class="news-element">
            <span class="news-element__date">13 июня 2018</span>
            <h3 class="news-elements__title">
                <a href="#">Infineon укрепляет сотрудничество с DENSO,
                    став акционером</a>
            </h3>
            <p class="news-elements__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
                передовых автомобильных технологий, систем и компонентов для поддержки своего
                автомобильного бизнеса</p>
            <div class="cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>

        <div class="news-element">
            <span class="news-element__date">05 сентября 2018</span>
            <h3 class="news-elements__title">
                <a href="#">Infineon приобретает Siltecra - эксперта
                    по карбиду кремния</a>
            </h3>
            <p class="news-elements__text">Infineon Techolojies AG приобретает Siltecra GmbH,
                стартап, основанный в Дрездене. В процессе запуска была разработана инновационная технология
                (Cold Split) для эффективной обработки кристаллического материала с минимальной потерей
                материала.</p>
            <div class="cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>

            <div class="news-element">
                <span class="news-element__date">05 сентября 2018</span>
                <h3 class="news-elements__title">
                    <a href="#">Infineon приобретает Siltecra - эксперта
                        по карбиду кремния</a>
                </h3>
                <p class="news-elements__text">Infineon Techolojies AG приобретает Siltecra GmbH,
                    стартап, основанный в Дрездене. В процессе запуска была разработана инновационная технология
                    (Cold Split) для эффективной обработки кристаллического материала с минимальной потерей
                    материала.</p>
                <div class="cloud-tags">
                    <a href="#" class="cloud-tags__link">Активные компоненты</a>
                    <a href="#" class="cloud-tags__link">Светодиоды</a>
                    <a href="#" class="cloud-tags__link">Samsung</a>
                    <a href="#" class="cloud-tags__link">Разработка функционала</a>
                    <a href="#" class="cloud-tags__link">DENSO</a>
                </div>
            </div>

            <div class="news-element">
                <span class="news-element__date">05 сентября 2018</span>
                <h3 class="news-elements__title">
                    <a href="#">Infineon приобретает Siltecra - эксперта
                        по карбиду кремния</a>
                </h3>
                <p class="news-elements__text">Infineon Techolojies AG приобретает Siltecra GmbH,
                    стартап, основанный в Дрездене. В процессе запуска была разработана инновационная технология
                    (Cold Split) для эффективной обработки кристаллического материала с минимальной потерей
                    материала.</p>
                <div class="cloud-tags">
                    <a href="#" class="cloud-tags__link">Активные компоненты</a>
                    <a href="#" class="cloud-tags__link">Светодиоды</a>
                    <a href="#" class="cloud-tags__link">Samsung</a>
                    <a href="#" class="cloud-tags__link">Разработка функционала</a>
                    <a href="#" class="cloud-tags__link">DENSO</a>
                </div>
            </div>

        <div class="news-element">
            <span class="news-element__date">05 ноября 2018</span>
            <h3 class="news-elements__title">
                <a href="#">Infineon на выставке electronica 2018</a>
            </h3>
            <p class="news-elements__text">Infineon Techolojies AG дает ответы на
                основные вопросы, связанные с оцифровкой: как можно производить чистку энергии
                и как быстро и с небольшими потерями попадает в электромобиль?</p>
            <div class="cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>
        </div>
        <ul class="pagination">
            <li class="pagination-arrow">
                Назад
            </li>
            <li class="pagination-page">1</li>
            <li class="pagination-page pagination-page-active">2</li>
            <li class="pagination-page">3</li>
            <li class="pagination-page">4</li>
            <li class="pagination-page">5</li>
            <li class="pagination-arrow">
                Вперёд
            </li>
        </ul>
    </div>

<script>
    let filter =document.querySelector('.filter.filter-news')
    if(filter) {
        let filterElements = filter.querySelectorAll('.filter-element');
        filter.addEventListener('click', (e) => {
            filterElements.forEach((item) => {
                let openList = item.querySelector('.filter-element-head__button')
                let openInput = item.querySelector('.filter-element-search')
                let checkBoxList = item.querySelector('.filter-checkbox-list')
                let closeSelect = item.querySelector('.filter-element-head__close')
                if(e.target == openList) {
                    if(openList.classList.contains('filter-element-head__button--close')) {
                        openInput.style.display = 'none'
                        checkBoxList.style.display = 'none'
                        openList.classList.remove('filter-element-head__button--close')
                    } else {
                        openInput.style.display = 'block'
                        checkBoxList.style.display = 'block'
                        openList.classList.add('filter-element-head__button--close')
                    }
                }

                if(e.target == closeSelect) {
                    item.querySelector('.filter-element-head__number').style.display = 'none'
                    closeSelect.style.display = 'none'
                }
            })
            let closeFilterList = document.querySelector('.filter-head__button')
            let filterWrapper = filter.querySelector('.filter-wrapper')
            if(e.target == closeFilterList) {
                if(closeFilterList.classList.contains('filter-head__button--open')) {
                    filterWrapper.style.display = 'none'
                    closeFilterList.classList.remove('filter-head__button--open')
                    filter.style.paddingBottom = '0px'
                } else {
                    filterWrapper.style.display = 'block'
                    closeFilterList.classList.add('filter-head__button--open')
                    filter.style.paddingBottom = '15px'
                }
            }
        })
    }
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>