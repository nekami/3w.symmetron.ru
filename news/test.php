<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test");
?>
<h2 class="title-page">Характеристики и особенности применения драйверов
MOSFET и IGBT</h2>
<div class="wrapper wrapper-new">
    <ul class="breadcrumbs-items bx-breadcrumb">
        <li class="bx-breadcrumb-item">
            <a href="#">
                <svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.458 69.124931" xml:space="preserve" id="svg2" version="1.1"><metadata id="metadata8"><rdf:rdf><cc:work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"></dc:type></cc:work></rdf:rdf></metadata><defs id="defs6"><clipPath id="clipPath18" clipPathUnits="userSpaceOnUse"><path id="path16" d="M 0,51.844 H 64.843 V 0 H 0 Z"></path></clipPath></defs><g transform="matrix(1.3333333,0,0,-1.3333333,0,69.124933)" id="g10"><g id="g12"><g clip-path="url(#clipPath18)" id="g14"><g transform="translate(48.2774,22.1616)" id="g20"><path id="path22" style="fill:none;stroke:#008aff;stroke-width:5.31099987;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" d="M 0,0 V -19.506 H -31.711 V 0"></path></g><g transform="translate(3.0636,22.7285)" id="g24"><path id="path26" style="fill:none;stroke:#008aff;stroke-width:6.12699986;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" d="M 0,0 29.358,26.052 58.716,0"></path></g></g></g></g></svg>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Производители</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Infineon Technologies</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Применения</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Силовая электроника</span>
            </a>
        </li>
        <li class="bx-breadcrumb-item">
            <a href="#">
                <span>Статьи</span>
            </a>
        </li>
    </ul>
</div>
<p class="page-text">Изолированный источник требуется для каждого транзистора верхнего плеча.
Необходим высоковольтный быстродействующий каскад сдвига уровня. Простое и дешевое
решение. Длительность управления испульса ограничена.</p>

<div class="news">
    <h2 class="news__title">Новости Infineon</h2>
    <div class="filter filter-news">
        <div class="filter-head">
            <span class="filter-head__button"></span>
            <p>Фильтр новостей</p>
        </div>
        <div class="filter-wrapper">
        <div class="filter-element">
            <div class="filter-element-head">
                <span class="filter-element-head__button"></span>
                <p>Производитель</p>
                <div class="filter-element-head__number">2</div>
                <span class="filter-element-head__close"></span>
            </div>
            <input type="text" class="filter-element-search" placeholder="Поиск">
            <div class="filter-checkbox-list">
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check1">
                    <label for="check1">Analog Devices evices evices evices evices</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check2">
                    <label for="check2">Atmel</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check3">
                    <label for="check3">Infenion</label>
                </div>
            </div>
        </div>
        <div class="filter-element">
            <div class="filter-element-head">
                <span class="filter-element-head__button"></span>
                <p>Раздел каталога</p>
                <div class="filter-element-head__number">2</div>
                <span class="filter-element-head__close"></span>
            </div>
            <input type="text" class="filter-element-search" placeholder="Поиск">
            <div class="filter-checkbox-list">
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check5">
                    <label for="check5">Analog Devices</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check6">
                    <label for="check6">Atmel</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check7">
                    <label for="check7">Infenion</label>
                </div>
            </div>
        </div>
        <div class="filter-element">
            <div class="filter-element-head">
                <span class="filter-element-head__button"></span>
                <p>Тема</p>
            </div>
            <input type="text" class="filter-element-search" placeholder="Поиск">
            <div class="filter-checkbox-list">
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check9">
                    <label for="check9">Analog Devices</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check10">
                    <label for="check10">Atmel</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check11">
                    <label for="check11">Infenion</label>
                </div>
            </div>
        </div>
        <div class="filter-element">
            <div class="filter-element-head">
                <span class="filter-element-head__button"></span>
                <p>Серия</p>
                <div class="filter-element-head__number">10</div>
                <span class="filter-element-head__close"></span>
            </div>
            <input type="text" class="filter-element-search" placeholder="Поиск">
            <div class="filter-checkbox-list">
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check12">
                    <label for="check12">Analog Devices</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check13">
                    <label for="check13">Atmel</label>
                </div>
                <div class="filter-checkbox-element">
                    <input type="checkbox" id="check14">
                    <label for="check14">Infenion</label>
                </div>
            </div>
        </div>
            <button type="button" class="filter-show-more">Показать 8 статей</button>
            <div class="filter-bottom">
                <span class="filter-bottom-close"></span>
                <p class="filter-bottom-text">Сбросить все фильтры</p>
            </div>
        </div>
    </div>
    <div class="news-element">
        <span class="news-element__date">13 июня 2018</span>
        <h3 class="news-elements__title">
            <a href="#">Infineon укрепляет сотрудничество с DENSO,
                став акционером</a>
        </h3>
        <p class="news-elements__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
        передовых автомобильных технологий, систем и компонентов для поддержки своего
        автомобильного бизнеса</p>
        <div class="cloud-tags">
            <a href="#" class="cloud-tags__link">Активные компоненты</a>
            <a href="#" class="cloud-tags__link">Светодиоды</a>
            <a href="#" class="cloud-tags__link">Samsung</a>
            <a href="#" class="cloud-tags__link">Разработка функционала</a>
            <a href="#" class="cloud-tags__link">DENSO</a>
        </div>
    </div>

    <div class="news-element">
        <span class="news-element__date">05 сентября 2018</span>
        <h3 class="news-elements__title">
            <a href="#">Infineon приобретает Siltecra - эксперта
                по карбиду кремния</a>
        </h3>
        <p class="news-elements__text">Infineon Techolojies AG приобретает Siltecra GmbH,
        стартап, основанный в Дрездене. В процессе запуска была разработана инновационная технология
        (Cold Split) для эффективной обработки кристаллического материала с минимальной потерей
        материала.</p>
        <div class="cloud-tags">
            <a href="#" class="cloud-tags__link">Активные компоненты</a>
            <a href="#" class="cloud-tags__link">Светодиоды</a>
            <a href="#" class="cloud-tags__link">Samsung</a>
            <a href="#" class="cloud-tags__link">Разработка функционала</a>
            <a href="#" class="cloud-tags__link">DENSO</a>
        </div>
    </div>

    <div class="news-element">
        <span class="news-element__date">05 ноября 2018</span>
        <h3 class="news-elements__title">
            <a href="#">Infineon на выставке electronica 2018</a>
        </h3>
        <p class="news-elements__text">Infineon Techolojies AG дает ответы на
        основные вопросы, связанные с оцифровкой: как можно производить чистку энергии
        и как быстро и с небольшими потерями попадает в электромобиль?</p>
        <div class="cloud-tags">
            <a href="#" class="cloud-tags__link">Активные компоненты</a>
            <a href="#" class="cloud-tags__link">Светодиоды</a>
            <a href="#" class="cloud-tags__link">Samsung</a>
            <a href="#" class="cloud-tags__link">Разработка функционала</a>
            <a href="#" class="cloud-tags__link">DENSO</a>
        </div>
    </div>
    <ul class="pagination">
        <li class="pagination-arrow">
            <img class="pagination-arrow-left" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
        </li>
        <li class="pagination-page">1</li>
        <li class="pagination-page pagination-page-active">2</li>
        <li class="pagination-page">3</li>
        <li class="pagination-page">4</li>
        <li class="pagination-page">5</li>
        <li class="pagination-arrow">
            <img class="pagination-arrow-right" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
        </li>
    </ul>
    <a class="show-more-news" href="#">Все новости Infineon</a>
</div>
<div class="articles">
    <h2 class="articles__title">Статьи</h2>
    <div class="filter filter-article">
        <div class="filter-head">
            <span class="filter-head__button"></span>
            <p>Фильтр статей</p>
        </div>
        <div class="filter-wrapper">
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Производитель</p>
                    <div class="filter-element-head__number">2</div>
                    <span class="filter-element-head__close"></span>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check20">
                        <label for="check20">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check21">
                        <label for="check21">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check22">
                        <label for="check22">Infenion</label>
                    </div>
                </div>
            </div>
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Раздел каталога</p>
                    <div class="filter-element-head__number">2</div>
                    <span class="filter-element-head__close"></span>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check23">
                        <label for="check23">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check24">
                        <label for="check24">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check25">
                        <label for="check25">Infenion</label>
                    </div>
                </div>
            </div>
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Тема</p>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check26">
                        <label for="check26">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check27">
                        <label for="check27">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check28">
                        <label for="check28">Infenion</label>
                    </div>
                </div>
            </div>
            <div class="filter-element">
                <div class="filter-element-head">
                    <span class="filter-element-head__button"></span>
                    <p>Серия</p>
                    <div class="filter-element-head__number">10</div>
                    <span class="filter-element-head__close"></span>
                </div>
                <input type="text" class="filter-element-search" placeholder="Поиск">
                <div class="filter-checkbox-list">
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check29">
                        <label for="check29">Analog Devices</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check30">
                        <label for="check30">Atmel</label>
                    </div>
                    <div class="filter-checkbox-element">
                        <input type="checkbox" id="check31">
                        <label for="check31">Infenion</label>
                    </div>
                </div>
            </div>
            <button type="button" class="filter-show-more">Показать 8 статей</button>
            <div class="filter-bottom">
                <span class="filter-bottom-close"></span>
                <p class="filter-bottom-text">Сбросить все фильтры</p>
            </div>
        </div>
    </div>
    <div class="articles-element">
        <ul class="articles-element-head">
            <li class="articles-element-head__date">13 июня 2018</li>
            <li class="articles-element-head__link">
                <a href="#">Рекомендации к применению</a>
            </li>
            <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
            <li class="article-element-head__duration">Приблизительное время чтения: 7 минут</li>
        </ul>
        <h3 class="articles-element__title">
            <a href="#">О пользе использования эффективных компонентов</a>
        </h3>
        <p class="articles-element__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком передовых
        автомобильных технологий, систем и компонентов для поддержки своего автомобильного бизнеса.</p>
        <div class="article-cloud-tags cloud-tags">
            <a href="#" class="cloud-tags__link">Активные компоненты</a>
            <a href="#" class="cloud-tags__link">Светодиоды</a>
            <a href="#" class="cloud-tags__link">Samsung</a>
            <a href="#" class="cloud-tags__link">Разработка функционала</a>
            <a href="#" class="cloud-tags__link">DENSO</a>
        </div>
    </div>

    <div class="articles-element">
        <ul class="articles-element-head">
            <li class="articles-element-head__date">13 июня 2018</li>
            <li class="articles-element-head__link">
                <a href="#">Новые технологии</a>
            </li>
            <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
            <li class="article-element-head__duration">Приблизительное время чтения: 12 минут</li>
        </ul>
        <h3 class="articles-element__title">
            <a href="#">Новые 7</a>
        </h3>
        <p class="articles-element__text">Infineon Techolojies AG приобретает Siltectra GmpH стартап, основанный в Дрездене.
        В процессе запуска была разработана инновационная технология (Cold Split) для эффективной обработки
        кристаллического материала и с минимальной потерей материала.</p>
        <div class="article-cloud-tags cloud-tags">
            <a href="#" class="cloud-tags__link">Инновации</a>
            <a href="#" class="cloud-tags__link">Cold Split</a>
            <a href="#" class="cloud-tags__link">Infineon</a>
            <a href="#" class="cloud-tags__link">Siltectra</a>
            <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
        </div>
    </div>

    <div class="articles-element">
        <ul class="articles-element-head">
            <li class="articles-element-head__date">13 июня 2018</li>
            <li class="articles-element-head__link">
                <a href="#">Аналитика</a>
            </li>
            <li class="articles-element-head__author">Автор: <a href="#">Игорь Владимиров</a></li>
            <li class="article-element-head__duration">Приблизительное время чтения: 5 минут</li>
        </ul>
        <h3 class="articles-element__title">
            <a href="#">5 факторов, влияющих на производительность систем</a>
        </h3>
        <p class="articles-element__text">Infineon Techolojies AG дает ответы на
            основные вопросы, связанные с оцифровкой: как можно производить чистку энергии
            и как быстро и с небольшими потерями попадает в электромобиль?</p>
        <div class="article-cloud-tags cloud-tags">
            <a href="#" class="cloud-tags__link">Чистка энергии</a>
            <a href="#" class="cloud-tags__link">Оцифровка</a>
            <a href="#" class="cloud-tags__link">Infineon</a>
            <a href="#" class="cloud-tags__link">Автомобильное применение</a>
            <a href="#" class="cloud-tags__link">Снижение потерь</a>
        </div>
    </div>
    <ul class="pagination">
        <li class="pagination-arrow">
            <img class="pagination-arrow-left" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
        </li>
        <li class="pagination-page">1</li>
        <li class="pagination-page pagination-page-active">2</li>
        <li class="pagination-page">3</li>
        <li class="pagination-page">4</li>
        <li class="pagination-page">5</li>
        <li class="pagination-arrow">
            <img class="pagination-arrow-right" src="/local/templates/symmetron_default/images/pagination-arrow.svg">
        </li>
    </ul>
</div>

<div class="events">
    <h2 class="events__title">Ближайшие мероприятия Infineon</h2>
    <div class="events-wrapper">
    <div class="event-item">
        <div class="event-date">
            <div class="event-date__month">
                <p>Декабрь 2018</p>
            </div>
            <div class="event-date__day">
                <div class="event-date__day-wrapper">
                    <span>12</span>
                </div>
            </div>
        </div>
        <div class="event-description">
            <h4 class="event-description__name">
                <a href="#">Семинар Infineon. Автомобильные технологии, системы и компоненты</a>
            </h4>
            <p class="event-description__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
            передовых автомобильных технологий, систем и компонентов для поддержки своего автомобильного
            бизнеса.</p>
            <div class="event-cloud-tags cloud-tags">
                <a href="#" class="cloud-tags__link">Инновации</a>
                <a href="#" class="cloud-tags__link">Cold Split</a>
                <a href="#" class="cloud-tags__link">Infineon</a>
                <a href="#" class="cloud-tags__link">Siltectra</a>
                <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
            </div>
        </div>
        <div class="event-address">
            <span class="event-address__city">Москва, Symmetron</span>
            <span class="event-address__place">Ленинградское шоссе, 69 корпус 1, бизнес-парк River City
            (м. Речной вокзал)</span>
            <span class="event-address__phone">Тел: <a href="tel:84959612020">(495) 961-2020</a></span>
            <p class="event-address__price">Бесплатно</p>
            <a href="#" class="event-address__reg" target="_blank">Регистрация</a>
        </div>
    </div>

    <div class="event-item">
        <div class="event-date">
            <div class="event-date__month">
                <p>Январь 2019</p>
            </div>
            <div class="event-date__day">
                <div class="event-date__day-wrapper">
                    <span>19</span>
                    <span>24</span>
                </div>
            </div>
        </div>
        <div class="event-description">
            <h4 class="event-description__name">
                <a href="#">Выставка "Свет и технологии"</a>
            </h4>
            <p class="event-description__text">Главная выставка индустрии промышленного, выставочного, бизнес
            и освещения общественных пространств. На выставке Infineon покажет последние разработки, позволяющие
            оптимизировать технологические цепочки и снизить издержки как бизнеса, так и потребителя.</p>
            <div class="event-cloud-tags cloud-tags">
                <a href="#" class="cloud-tags__link">Активные компоненты</a>
                <a href="#" class="cloud-tags__link">Светодиоды</a>
                <a href="#" class="cloud-tags__link">Samsung</a>
                <a href="#" class="cloud-tags__link">Разработка функционала</a>
                <a href="#" class="cloud-tags__link">DENSO</a>
            </div>
        </div>
        <div class="event-address">
            <span class="event-address__city">Москва, Экспоцентр</span>
            <span class="event-address__place">Краснопресненская наб., 14</span>
            <span class="event-address__phone">Тел: <a href="tel:88007073799">8(800) 9707-37-99</a></span>
            <p class="event-address__price">600 ₽</p>
            <a href="#" class="event-address__reg" target="_blank">Регистрация обязательна</a>
        </div>
    </div>
    </div>
</div>

<div class="documents">
    <div class="documents-head">
        <h2 class="documents__title">Документация</h2>
        <span class="documents-head-show">раскрыть все</span>
    </div>

    <div class="docs-wrapper">
        <div class="docs-item">
            <div class="docs-item__head">
                <span class="document-control"></span>
                <p class="docs-item__head-name">Брошюры</p>
            </div>
            <div class="document-list">
                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--html"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                    <div class="document-element">
                        <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                        <span class="document-element__lang">English</span>
                    </div>
                    <div class="document-char">
                        <span class="document-element__date">15.11.2017</span>
                        <span class="document-element__format">pdf</span>
                        <span class="document-element__weight">4,1 MB</span>
                    </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--jpg"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--ppt"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--txt"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="docs-item">
            <div class="docs-item__head">
                <span class="document-control"></span>
                <p class="docs-item__head-name">Руководства по подбору компонента</p>
            </div>
            <div class="document-list">
                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--pdf"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--blank"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--db"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--doc"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="docs-item">
            <div class="docs-item__head">
                <span class="document-control"></span>
                <p class="docs-item__head-name">Руководства по применению</p>
            </div>
            <div class="document-list">
                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--xls"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--xml"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--zip"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--pdf"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="docs-item">
            <div class="docs-item__head">
                <span class="document-control"></span>
                <p class="docs-item__head-name">Технические спецификации</p>
            </div>
            <div class="document-list">
                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--pdf"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--pdf"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--pdf"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>

                <div class="document-list-element">
                    <a href="#" download>
                        <span class="document-icon document-icon--pdf"></span>
                    </a>
                    <div class="document-list-element-wrapper">
                        <div class="document-element">
                            <a href="#" class="document-element__name" download>Automotive MOSFETs Product Brochure Automotive MOSFETs</a>
                            <span class="document-element__lang">English</span>
                        </div>
                        <div class="document-char">
                            <span class="document-element__date">15.11.2017</span>
                            <span class="document-element__format">pdf</span>
                            <span class="document-element__weight">4,1 MB</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="links">
    <a class="catalog-link" href="#" target="_blank">Компоненты Infineon в Каталоге</a>
</div>

<script>
    let docs = document.querySelector('.documents')
    if(docs) {
        let docsItems = document.querySelectorAll('.docs-item')
        let buttonShow = document.querySelector('.documents-head-show')
        buttonShow.addEventListener('click', (e) => {
            if(buttonShow.classList.contains('documents-head-show--open') == false) {
                docsItems.forEach((item) => {
                    let docsList = item.querySelector('.document-list')
                    if (docsList.classList.contains('document-list--open') == false) {
                        docsList.classList.add('document-list--open')
                    }
                })
                e.target.innerHTML = 'свернуть все'
                buttonShow.classList.add('documents-head-show--open')
                docsItems.forEach((item) => {
                    item.querySelector('.document-control').classList.add('document-control--open')
                })
            } else {
                docsItems.forEach((item) => {
                    let docsList = item.querySelector('.document-list')
                    if (docsList.classList.contains('document-list--open')) {
                        docsList.classList.remove('document-list--open')
                    }
                })
                e.target.innerHTML = 'раскрыть все'
                buttonShow.classList.remove('documents-head-show--open')
                docsItems.forEach((item) => {
                    item.querySelector('.document-control').classList.remove('document-control--open')
                })
            }
        })

        docs.addEventListener('click', (e) => {
            docsItems.forEach((element) => {
                let openListButton = element.querySelector('.document-control')
                let documentList = element.querySelector('.document-list')
                if(e.target == openListButton) {
                    if(openListButton.classList.contains('document-control--open')) {
                        openListButton.classList.remove('document-control--open')
                        documentList.classList.remove('document-list--open')
                    } else {
                        openListButton.classList.add('document-control--open')
                        documentList.classList.add('document-list--open')
                    }
                }
            })
        })
    }

    let filter =document.querySelector('.filter.filter-news')
    if(filter) {
        let filterElements = filter.querySelectorAll('.filter-element');
        filter.addEventListener('click', (e) => {
            filterElements.forEach((item) => {
                let openList = item.querySelector('.filter-element-head__button')
                let openInput = item.querySelector('.filter-element-search')
                let checkBoxList = item.querySelector('.filter-checkbox-list')
                let closeSelect = item.querySelector('.filter-element-head__close')
                if(e.target == openList) {
                    if(openList.classList.contains('filter-element-head__button--close')) {
                        openInput.style.display = 'none'
                        checkBoxList.style.display = 'none'
                        openList.classList.remove('filter-element-head__button--close')
                    } else {
                        openInput.style.display = 'block'
                        checkBoxList.style.display = 'block'
                        openList.classList.add('filter-element-head__button--close')
                    }
                }

                if(e.target == closeSelect) {
                    item.querySelector('.filter-element-head__number').style.display = 'none'
                    closeSelect.style.display = 'none'
                }
            })
            let closeFilterList = document.querySelector('.filter-head__button')
            let filterWrapper = filter.querySelector('.filter-wrapper')
            if(e.target == closeFilterList) {
                if(closeFilterList.classList.contains('filter-head__button--open')) {
                    filterWrapper.style.display = 'none'
                    closeFilterList.classList.remove('filter-head__button--open')
                    filter.style.paddingBottom = '0px'
                } else {
                    filterWrapper.style.display = 'block'
                    closeFilterList.classList.add('filter-head__button--open')
                    filter.style.paddingBottom = '15px'
                }
            }
        })
    }

    let filterArticle = document.querySelector('.filter.filter-article')
    if(filterArticle) {
        let filterElements = filterArticle.querySelectorAll('.filter-element');
        filterArticle.addEventListener('click', (e) => {
            filterElements.forEach((item) => {
                let openList = item.querySelector('.filter-element-head__button')
                let openInput = item.querySelector('.filter-element-search')
                let checkBoxList = item.querySelector('.filter-checkbox-list')
                let closeSelect = item.querySelector('.filter-element-head__close')
                if(e.target == openList) {
                    if(openList.classList.contains('filter-element-head__button--close')) {
                        openInput.style.display = 'none'
                        checkBoxList.style.display = 'none'
                        openList.classList.remove('filter-element-head__button--close')
                    } else {
                        openInput.style.display = 'block'
                        checkBoxList.style.display = 'block'
                        openList.classList.add('filter-element-head__button--close')
                    }
                }

                if(e.target == closeSelect) {
                    item.querySelector('.filter-element-head__number').style.display = 'none'
                    closeSelect.style.display = 'none'
                }
            })
            let closeFilterList = filterArticle.querySelector('.filter-head__button')
            let filterWrapper = filterArticle.querySelector('.filter-wrapper')
            if(e.target == closeFilterList) {
                if(closeFilterList.classList.contains('filter-head__button--open')) {
                    filterWrapper.style.display = 'none'
                    closeFilterList.classList.remove('filter-head__button--open')
                    filterArticle.style.paddingBottom = '0px'
                } else {
                    filterWrapper.style.display = 'block'
                    closeFilterList.classList.add('filter-head__button--open')
                    filterArticle.style.paddingBottom = '15px'
                }
            }
        })
    }
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>