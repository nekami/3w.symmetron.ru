<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("events-list");
?>

    <div class="events">
        <h2 class="events__title">Мероприятия</h2>
        <div class="events-container">
        <div class="events-wrapper">
            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Декабрь 2018</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>12</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Семинар Infineon. Автомобильные технологии, системы и компоненты</a>
                    </h4>
                    <p class="event-description__text">Долгосрочное партнерство с корпорацией DENSO, глобальным поставщиком
                        передовых автомобильных технологий, систем и компонентов для поддержки своего автомобильного
                        бизнеса.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Инновации</a>
                        <a href="#" class="cloud-tags__link">Cold Split</a>
                        <a href="#" class="cloud-tags__link">Infineon</a>
                        <a href="#" class="cloud-tags__link">Siltectra</a>
                        <a href="#" class="cloud-tags__link">Обработка кристаллов</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Symmetron</span>
                    <span class="event-address__place">Ленинградское шоссе, 69 корпус 1, бизнес-парк River City
            (м. Речной вокзал)</span>
                    <span class="event-address__phone">Тел: <a href="tel:84959612020">(495) 961-2020</a></span>
                    <p class="event-address__price">Бесплатно</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация</a>
                </div>
            </div>

            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Январь 2019</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>19</span>
                            <span>24</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Выставка "Свет и технологии"</a>
                    </h4>
                    <p class="event-description__text">Главная выставка индустрии промышленного, выставочного, бизнес
                        и освещения общественных пространств. На выставке Infineon покажет последние разработки, позволяющие
                        оптимизировать технологические цепочки и снизить издержки как бизнеса, так и потребителя.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Активные компоненты</a>
                        <a href="#" class="cloud-tags__link">Светодиоды</a>
                        <a href="#" class="cloud-tags__link">Samsung</a>
                        <a href="#" class="cloud-tags__link">Разработка функционала</a>
                        <a href="#" class="cloud-tags__link">DENSO</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Экспоцентр</span>
                    <span class="event-address__place">Краснопресненская наб., 14</span>
                    <span class="event-address__phone">Тел: <a href="tel:88007073799">8(800) 9707-37-99</a></span>
                    <p class="event-address__price">600 ₽</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация обязательна</a>
                </div>
            </div>

            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Январь 2019</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>19</span>
                            <span>24</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Выставка "Свет и технологии"</a>
                    </h4>
                    <p class="event-description__text">Главная выставка индустрии промышленного, выставочного, бизнес
                        и освещения общественных пространств. На выставке Infineon покажет последние разработки, позволяющие
                        оптимизировать технологические цепочки и снизить издержки как бизнеса, так и потребителя.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Активные компоненты</a>
                        <a href="#" class="cloud-tags__link">Светодиоды</a>
                        <a href="#" class="cloud-tags__link">Samsung</a>
                        <a href="#" class="cloud-tags__link">Разработка функционала</a>
                        <a href="#" class="cloud-tags__link">DENSO</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Экспоцентр</span>
                    <span class="event-address__place">Краснопресненская наб., 14</span>
                    <span class="event-address__phone">Тел: <a href="tel:88007073799">8(800) 9707-37-99</a></span>
                    <p class="event-address__price">600 ₽</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация обязательна</a>
                </div>
            </div>

            <div class="event-item">
                <div class="event-date">
                    <div class="event-date__month">
                        <p>Январь 2019</p>
                    </div>
                    <div class="event-date__day">
                        <div class="event-date__day-wrapper">
                            <span>19</span>
                            <span>24</span>
                        </div>
                    </div>
                </div>
                <div class="event-description">
                    <h4 class="event-description__name">
                        <a href="#">Выставка "Свет и технологии"</a>
                    </h4>
                    <p class="event-description__text">Главная выставка индустрии промышленного, выставочного, бизнес
                        и освещения общественных пространств. На выставке Infineon покажет последние разработки, позволяющие
                        оптимизировать технологические цепочки и снизить издержки как бизнеса, так и потребителя.</p>
                    <div class="event-cloud-tags cloud-tags">
                        <a href="#" class="cloud-tags__link">Активные компоненты</a>
                        <a href="#" class="cloud-tags__link">Светодиоды</a>
                        <a href="#" class="cloud-tags__link">Samsung</a>
                        <a href="#" class="cloud-tags__link">Разработка функционала</a>
                        <a href="#" class="cloud-tags__link">DENSO</a>
                    </div>
                </div>
                <div class="event-address">
                    <span class="event-address__city">Москва, Экспоцентр</span>
                    <span class="event-address__place">Краснопресненская наб., 14</span>
                    <span class="event-address__phone">Тел: <a href="tel:88007073799">8(800) 9707-37-99</a></span>
                    <p class="event-address__price">600 ₽</p>
                    <a href="#" class="event-address__reg" target="_blank">Регистрация обязательна</a>
                </div>
            </div>

        </div>
            <div class="filter filter-events">
                <div class="filter-container">
                <div class="filter-head">
                    <span class="filter-head__button filter-head__button--open"></span>
                    <p>Фильтр мероприятий</p>
                </div>
                <div class="filter-wrapper">
                    <div class="filter-element">
                        <div class="filter-element-head">
                            <span class="filter-element-head__button"></span>
                            <p>Тип мероприятия</p>
                            <div class="filter-element-head__number">2</div>
                            <span class="filter-element-head__close"></span>
                        </div>
                        <input type="text" class="filter-element-search" placeholder="Поиск">
                        <div class="filter-checkbox-list">
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check1">
                                <label for="check1">Analog Devices evices evices evices evices</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check2">
                                <label for="check2">Atmel</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check3">
                                <label for="check3">Infenion</label>
                            </div>
                        </div>
                    </div>
                    <div class="filter-element">
                        <div class="filter-element-head">
                            <span class="filter-element-head__button"></span>
                            <p>Тема</p>
                            <div class="filter-element-head__number">2</div>
                            <span class="filter-element-head__close"></span>
                        </div>
                        <input type="text" class="filter-element-search" placeholder="Поиск">
                        <div class="filter-checkbox-list">
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check5">
                                <label for="check5">Analog Devices</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check6">
                                <label for="check6">Atmel</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check7">
                                <label for="check7">Infenion</label>
                            </div>
                        </div>
                    </div>
                    <div class="filter-element">
                        <div class="filter-element-head">
                            <span class="filter-element-head__button"></span>
                            <p>Производители</p>
                        </div>
                        <input type="text" class="filter-element-search" placeholder="Поиск">
                        <div class="filter-checkbox-list">
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check9">
                                <label for="check9">Analog Devices</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check10">
                                <label for="check10">Atmel</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check11">
                                <label for="check11">Infenion</label>
                            </div>
                        </div>
                    </div>
                    <div class="filter-element">
                        <div class="filter-element-head">
                            <span class="filter-element-head__button"></span>
                            <p>Город</p>
                            <div class="filter-element-head__number">10</div>
                            <span class="filter-element-head__close"></span>
                        </div>
                        <input type="text" class="filter-element-search" placeholder="Поиск">
                        <div class="filter-checkbox-list">
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check12">
                                <label for="check12">Analog Devices</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check13">
                                <label for="check13">Atmel</label>
                            </div>
                            <div class="filter-checkbox-element">
                                <input type="checkbox" id="check14">
                                <label for="check14">Infenion</label>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="filter-show-more">Показать 8 статей</button>
                    <div class="filter-bottom">
                        <span class="filter-bottom-close"></span>
                        <p class="filter-bottom-text">Сбросить все фильтры</p>
                    </div>
                </div>
                </div>
        </div>
            <ul class="pagination">
                <li class="pagination-arrow">
                    <a href="#">Назад</a>
                </li>
                <li class="pagination-page">
                    <a href="#">1</a>
                </li>
                <li class="pagination-page pagination-page-active">
                    <a href="#">2</a>
                </li>
                <li class="pagination-page">
                    <a href="#">3</a>
                </li>
                <li class="pagination-page">
                    <a href="#">4</a>
                </li>
                <li class="pagination-page">
                    <a href="#">5</a>
                </li>
                <li class="pagination-arrow">
                    <a href="#">Вперёд</a>
                </li>
            </ul>

    </div>

        <script>
            let filter = document.querySelector('.filter')
            if(filter) {
                let filterElements = filter.querySelectorAll('.filter-element');
                filter.addEventListener('click', (e) => {
                    filterElements.forEach((item) => {
                        let openList = item.querySelector('.filter-element-head__button')
                        let openInput = item.querySelector('.filter-element-search')
                        let checkBoxList = item.querySelector('.filter-checkbox-list')
                        let closeSelect = item.querySelector('.filter-element-head__close')
                        if(e.target == openList) {
                            if(openList.classList.contains('filter-element-head__button--close')) {
                                openInput.style.display = 'none'
                                checkBoxList.style.display = 'none'
                                openList.classList.remove('filter-element-head__button--close')
                            } else {
                                openInput.style.display = 'block'
                                checkBoxList.style.display = 'block'
                                openList.classList.add('filter-element-head__button--close')
                            }
                        }

                        if(e.target == closeSelect) {
                            item.querySelector('.filter-element-head__number').style.display = 'none'
                            closeSelect.style.display = 'none'
                        }
                    })
                    let closeFilterList = filter.querySelector('.filter-head__button')
                    let filterWrapper = filter.querySelector('.filter-wrapper')
                    if(e.target == closeFilterList) {
                        if(closeFilterList.classList.contains('filter-head__button--open')) {
                            filterWrapper.style.display = 'none'
                            closeFilterList.classList.remove('filter-head__button--open')
                            filter.style.paddingBottom = '0px'
                        } else {
                            filterWrapper.style.display = 'block'
                            closeFilterList.classList.add('filter-head__button--open')
                            filter.style.paddingBottom = '15px'
                        }
                    }
                })
            }
        </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>