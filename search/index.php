<?
$where = array(
	0 => "iblock_catalog",
	1 => "iblock_news",
	2 => "iblock_lists",
	3 => "iblock_references",
	4 => "iblock_services",
);
if( $_REQUEST['category'] >= 0 ) $_REQUEST['where'] = $where[$_REQUEST['category']];
if( $_REQUEST['category'] === 'iblock_catalog' ) $_REQUEST['where'] = $where[0];

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
global $APPLICATION;

$title = "Результаты поиска";
$_GET['q'] ? $title .= ' по "'.$_GET['q'].'"' : '';
$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "N");
$APPLICATION->AddChainItem('Поиск', '/search/');
$APPLICATION->AddChainItem($title);

?><?
$_REQUEST["q"] = preg_replace('/(\S+)/', '"\\1"', $_REQUEST["q"]);
if ($_GET["category"] !== "all")
$_REQUEST["where"] = $_GET["category"];
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"searchresult",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DEFAULT_SORT" => "rank",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILTER_NAME" => "testF",
		"NO_WORD_LOGIC" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "symmetron_def",
		"PAGER_TITLE" => "Результаты поиска",
		"PAGE_RESULT_COUNT" => "25",
		"PATH_TO_USER_PROFILE" => "",
		"RATING_TYPE" => "",
		"RESTART" => "N",
		"SHOW_ITEM_DATE_CHANGE" => "N",
		"SHOW_ITEM_TAGS" => "N",
		"SHOW_ORDER_BY" => "N",
		"SHOW_RATING" => "",
		"SHOW_TAGS_CLOUD" => "N",
		"SHOW_WHEN" => "N",
		"SHOW_WHERE" => "Y",
		"USE_LANGUAGE_GUESS" => "N",
		"USE_SUGGEST" => "N",
		"USE_TITLE_RANK" => "Y",
		"arrFILTER" => array("iblock_catalog","iblock_news","iblock_lists","iblock_documents"),
		"arrFILTER_iblock_catalog" => array("4"),
		"arrFILTER_iblock_documents" => array("16"),
		"arrFILTER_iblock_lists" => array("11"),
		"arrFILTER_iblock_news" => array("8","14","15"),
		"arrFILTER_iblock_references" => array(0=>"all",),
		"arrFILTER_iblock_services" => array(0=>"all",),
		"arrFILTER_main" => "",
		"arrWHERE" => array()
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>