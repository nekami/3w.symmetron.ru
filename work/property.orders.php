<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Выгрузка заказа");

//use Bitrix\Main\Loader;
use Bitrix\Sale;
//Loader::includeModule("iblock");
//Loader::includeModule("sale");

 CModule::IncludeModule('sale');
/** Подключение PHPExcel */
require_once $_SERVER['DOCUMENT_ROOT'].'/Classes/PHPExcel.php';

function dump($data){
$text=print_r($data, 1);$fp = fopen($_SERVER['DOCUMENT_ROOT']."/zlog.txt", "w");fwrite($fp, $text);fclose($fp);
}
/***************** Производители ******************/
$arFilter = array('IBLOCK_ID' => 11);
$res = CIBlockElement::GetList(array('NAME' => 'asc'), $arFilter, false, false, array('ID','NAME','CODE'));
while($el = $res->GetNext()){
	$el['NAME'] = str_replace("&quot;",'"',$el['NAME']);
	$el['NAME'] = str_replace("&lt;",'<',$el['NAME']);
	$el['NAME'] = str_replace("&gt;",'>',$el['NAME']);
	$arManufacturers[$el['ID']]['NAME'] = $el['NAME'];
	$arManufacturers[$el['ID']]['CODE'] = $el['CODE'];
}
/***************** Доставка       ******************/
$arFilter = array("LID" => "s1",);
$res = CSaleDelivery::GetList(array('NAME' => 'asc'), $arFilter, false, false, array('ID','NAME'));
while($el = $res->Fetch()){	$arDelivery[$el['ID']] = $el['NAME']; }
?><div style="width:990px;" class="center-div"><br><a href="/bitrix/admin/fileman_admin.php?PAGEN_1=1&SIZEN_1=20&lang=ru&site=s1&path=%2Fcsv%2Foutput&show_perms_for=0" target="_blank">Папка выгрузки файлов</a><br><?
$domen="https://www.symmetron.ru/";
$domen2="https://www.symmetron.ru";
$folder="output";
$filemacros="Макрос.1.xlsm";
$_R=$_REQUEST;
if(CModule::IncludeModule("iblock") and CModule::IncludeModule("sale")) {

$orderID = 742;
//$orderID = 727;

echo "<h3 style='text-align:center;'>Выгрузка заказа</h3>";

$today1=date('Y-m-d');
$today=date('d.m.Y');
$_R['day']=date("d.m.Y", strtotime($_R['day']));
$_R['today']=date("d.m.Y", strtotime($_R['today']));
	//echo $day." - ".$today." - ".$today1."<br>".$_R['day']." ".$_R['today']." ".$_R['formunload']."<br>";
if ($_R['number']!=""){
	$filename=$_SERVER['DOCUMENT_ROOT']."/work/output/order.".$_R['number'].".xlsx";
	$wwwfilename=$domen2."/work/output/order.".$_R['number'].".xlsx";
}else{
	$filename=$_SERVER['DOCUMENT_ROOT']."/work/output/order.".$_R['day']."-".$_R['today'].".xlsx";
	$wwwfilename=$domen2."/work/output/order.".$_R['day']."-".$_R['today'].".xlsx";
}



// Загрузка шаблона
$filemail="Шаблон.выгрузки.заказа.xlsx";
$template = $_SERVER['DOCUMENT_ROOT'] . '/work/'.$filemail;
$objReaderOrder = PHPExcel_IOFactory::createReader('Excel2007');
$PHPExcelOrder  = $objReaderOrder->load($template); 


// Стили ячеек для ссылок
$styleArrayUri= array('font'=> array('name'=>'Roboto','size'=>'10','bold'=> false,'italic'=> false,'strike'=> false,'color' => array('rgb' => '0070c0')),);

$styleArrayCenter= array(
'alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
'font'=> array('bold'=> true,),
);

$styleArrayOrder= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),);
$styleArrayOrderCenter= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),);
	// 'fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'color'=>array('rgb'=>'ffffff')),
$styleArrayItem= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),);
$styleArrayItemCenter= array('alignment' => array ('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),);

$styleArrayOrderBorder= array('borders'=>array('bottom'=>array('style'=>PHPExcel_Style_Border::BORDER_THIN,'color'=>array('rgb'=>'000000')),),);

$styleArrayOrderBorderRight= array('borders'=>array('right'=>array('style'=>PHPExcel_Style_Border::BORDER_HAIR,'color'=>array('rgb'=>'000000')),),);
?>
<style>
.center-div{
     margin: 0 auto;
     max-width: 1200px; 
}
.button24send {
  display: inline-block;
  color: white;
  text-decoration: none;
  padding: .5em 2em;
  outline: none;
  border-width: 2px 0;
  border-style: solid none;
  border-color: #88ff63 #000 #085e00;
  border-radius: 6px;
  background: linear-gradient(#39f500, #0d9600) #0d9600;
  transition: 0.2s;
} 

</style>
<form id="myform" action="<? $_PHP_SELF ?>?clear_cache=Y" method="post">
 <input type="hidden" name="formunload" value="y"> 
<input type="hidden" name="bt" id="bt"> 
Дни: с <input type="date" name="day" value="<? echo $today1; ?>"> 
по <input type="date" name="today" value="<? echo $today1; ?>"><br>
 <br>
	Номер заказа: <input type="number" name="number" value=""><br><br>
<input type=input> <input type=submit class='button24send'></form><br><br>



<?
if ($_R['formunload']=="y"){
?>
Файл заказа: <a href='<? echo $wwwfilename; ?>'  target="_blank" style="color:#008aff"> <? echo $wwwfilename;?></a><br><br>
</div>
<?

// Старая версия
	//$arFilter['>=DATE_INSERT'] = $today.' 00:00:00';
	//$arFilter['<=DATE_INSERT'] = $today.' 23:59:59';
	//$db_sales = CSaleOrder::GetList(array('ID' => 'DESC'), $arFilter);
	//echo "<br><textarea rows=10 cols=145 name=text>";
	//while ($ar_sales = $db_sales->Fetch()){
	//print_r($ar_sales);
	////break;
	//}
	//echo "</textarea><br>";


	/*
// Выведем все свойства заказа с кодом $ID, сгруппированые по группам свойств
$db_props = CSaleOrderPropsValue::GetOrderProps(737);
$iGroup = -1;
	//$str="";
while ($arProps = $db_props->Fetch())
{
	//$str.=$db_props;
   if ($iGroup!=IntVal($arProps["PROPS_GROUP_ID"]))
   {
      echo "<b>".$arProps["GROUP_NAME"]."</b><br>";
      $iGroup = IntVal($arProps["PROPS_GROUP_ID"]);
   }

   echo $arProps["NAME"].": ";

   if ($arProps["TYPE"]=="CHECKBOX")
   {
      if ($arProps["VALUE"]=="Y")
         echo "Да";
      else
         echo "Нет";
   }
   elseif ($arProps["TYPE"]=="TEXT" || $arProps["TYPE"]=="TEXTAREA")
   {
      echo htmlspecialchars($arProps["VALUE"]);
   }
   elseif ($arProps["TYPE"]=="SELECT" || $arProps["TYPE"]=="RADIO")
   {
      $arVal = CSaleOrderPropsVariant::GetByValue($arProps["ORDER_PROPS_ID"], $arProps["VALUE"]);
      echo htmlspecialchars($arVal["NAME"]);
   }
   elseif ($arProps["TYPE"]=="MULTISELECT")
   {
      $curVal = split(",", $arProps["VALUE"]);
      for ($i = 0; $i<count($curVal); $i++)
      {
         $arVal = CSaleOrderPropsVariant::GetByValue($arProps["ORDER_PROPS_ID"], $curVal[$i]);
         if ($i>0) echo ", ";
         echo htmlspecialchars($arVal["NAME"]);
      }
   }
   elseif ($arProps["TYPE"]=="LOCATION")
   {
      $arVal = CSaleLocation::GetByID($arProps["VALUE"], LANGUAGE_ID);
      echo htmlspecialchars($arVal["COUNTRY_NAME"]." - ".$arVal["CITY_NAME"]);
   }

   echo "<br>";
}
	//echo "<br><textarea rows=10 cols=145 name=text>";print_r($str);echo "</textarea><br>";

	*/

// Узнаем имя заказчика (т.е. значение, которое было введено в поле свойства 
// заказа $ORDER_ID с установленным флагом IS_PAYER)
	/*
echo "<br>Узнаем имя заказчика: <br>";
$PAYER_NAME = "";$ORDER_ID="737";
$db_order = CSaleOrder::GetList(
        array("DATE_UPDATE" => "DESC"),
        array("ID" => $ORDER_ID)
    );
if ($arOrder = $db_order->Fetch())
{
   $db_props = CSaleOrderProps::GetList(
        array("SORT" => "ASC"),
        array(
                "PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
                "IS_PAYER" => "Y"
            )
    );
   if ($arProps = $db_props->Fetch())
   {
      $db_vals = CSaleOrderPropsValue::GetList(
            array(),
            array(
                    "ORDER_ID" => $ORDER_ID,
                    "ORDER_PROPS_ID" => $arProps["ID"]
                )
        );
      if ($arVals = $db_vals->Fetch())
        $PAYER_NAME = $arVals["VALUE"];
   }
}
echo $PAYER_NAME;
	*/
$today="11.10.2019";
// Заказы за определённую дату 

if ($_R['number']!=""){
	$parameters = [
	'order'  => ["ID" => "DESC"],
	'filter' => ["ID" => $_R['number']],
	];
}else{
	$parameters = [
	'order'  => ["ID" => "DESC"],
	'filter' => [">=DATE_INSERT" => $_R['day'].' 00:00:00',"<=DATE_INSERT" => $_R['today'].' 23:59:59'],
		//'filter' => [">=DATE_INSERT" => $today.' 00:00:00'],
	];
}
$dbRes = \Bitrix\Sale\Order::getList($parameters);
	//$rows=Bitrix\Sale\Order::getList()->getSelectedRowsCount();
	//echo "<br>Свойства инфоблока, количество записей: ".$rows."<br>";
$i=1;
$irow=3;
//echo "<br><textarea rows=10 cols=145 name=text>";
while ($order = $dbRes->fetch()){
	//echo "Заказ: ";
	//print_r($order);
	//echo $order['ID']."\n";
	$oborder = \Bitrix\Sale\Order::load($order['ID']);
	$propertyCollection = $oborder->getPropertyCollection();

	$userid=$oborder->getUserId();
	$rsUser = CUser::GetByID($userid);
	$arUser = $rsUser->Fetch();
	//echo "Пользователь: \n"; print_r($arUser); echo "\n\n";

	$strUser=$arUser["LOGIN"];
	if($arUser["NAME"]!="" OR $arUser["LAST_NAME"]!=""){ $strUser=$arUser["NAME"]." ".$arUser["LAST_NAME"]; }
	// Печатаем массив, содержащий актуальную на текущий момент корзину
	//echo "<pre>";
	//print_r($arBasketItems);
	//echo "</pre>";

	$orderCOMPANY="";
	$orderINN="";
	$orderADDRESS_CITY="";
	$orderCONTACT_PERSON="";
	$orderPHONE="";
	$orderEMAIL="";
	$orderCOMMENT="";
	$orderADDRESS_FROM_DADATA="";
	$orderADDRESS_INDEX="";
	$orderADDRESS_COUNTRY="";
	$orderREGION="";
	$orderADDRESS_STREET="";
	$orderADDRESS_BUILDING="";
	$orderADDRESS_BLOCK="";
	$orderADDRESS_ROOM="";
	$orderUF_DOB="";

	foreach ($propertyCollection as $Prop){
		//echo $Prop->getField('CODE').'['.$Prop->getPropertyId().'] '.$Prop->getName().': '.$Prop->getValue()."\n";
		if($Prop->getField('CODE') == 'COMPANY') {$orderCOMPANY= $Prop->getValue();}
		if($Prop->getField('CODE') == 'INN') {$orderINN= $Prop->getValue();}

		if($Prop->getField('CODE') == 'CONTACT_PERSON') {$orderCONTACT_PERSON= $Prop->getValue();}
		if($Prop->getField('CODE') == 'PHONE') {$orderPHONE= $Prop->getValue();}
		if($Prop->getField('CODE') == 'EMAIL') {$orderEMAIL= $Prop->getValue();}
		if($Prop->getField('CODE') == 'COMMENT') {$orderCOMMENT= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_FROM_DADATA') {$orderADDRESS_FROM_DADATA= $Prop->getValue();}

		if($Prop->getField('CODE') == 'ADDRESS_INDEX') {$orderADDRESS_INDEX= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_COUNTRY') {$orderADDRESS_COUNTRY= $Prop->getValue();}
		if($Prop->getField('CODE') == 'REGION') {$orderREGION= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_CITY') {$orderADDRESS_CITY= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_STREET') {$orderADDRESS_STREET= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_BUILDING') {$orderADDRESS_BUILDING= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_BLOCK') {$orderADDRESS_BLOCK= $Prop->getValue();}
		if($Prop->getField('CODE') == 'ADDRESS_ROOM') {$orderADDRESS_ROOM= $Prop->getValue();}

		if($Prop->getField('CODE') == 'UF_DOB') {$orderUF_DOB= $Prop->getValue();}

	}
	//echo "Доставка: ";
	$arDeliv = CSaleDelivery::GetByID($order["DELIVERY_ID"]);
	//print_r($arDeliv);
	//echo "\n".$arDeliv["NAME"]."\n\n";

	//$arDelivLocation = CSaleDelivery::GetLocationList(array($order["DELIVERY_ID"]));
	//print_r($arDelivLocation);
	//echo "\n".$arDelivLocation["NAME"]."\n\n";


	//print_r($oborder->getAvailableFields()); // Доступные поля

	//$collection = $oborder->getPropertyCollection();
	//foreach ($collection as $item){		dump($item);	}

	//$propertyCollection = $oborder->getPropertyCollection();
	//$ar = $propertyCollection->getArray();
	//print_r($ar);

	//$somePropValue = $propertyCollection->getItemByOrderPropertyId(30);
	//echo $somePropValue."\n";
	//echo $ar[properties][0][NAME]." = ".$ar[properties][0][VALUE][0]."\n";
	//$property = $propertyValue->getPropertyObject();
	//$property->getField('SIZE');

	//        Вот этим методом можно получить информацию о складе
	//        https://dev.1c-bitrix.ru/api_help/catalog/classes/ccatalogstore/getlist.php 

	$rsDeliveryOrder = Bitrix\Sale\Internals\ShipmentTable::getList(["filter" => ["ORDER_ID" => $order['ID']]]);
	$arDeliveryOrder = [];
	while($deliveryOrder = $rsDeliveryOrder->Fetch()){$arDeliveryOrder[] = $deliveryOrder["ID"];}

	if(!empty($arDeliveryOrder)){$arDelivertOrderEx = Bitrix\Sale\Internals\ShipmentExtraServiceTable::getList(["filter" => ["SHIPMENT_ID" => $arDeliveryOrder]])->Fetch();

		if(!empty($arDelivertOrderEx)){$arResult = CCatalogStore::GetList([],['ACTIVE' => 'Y','ID'=>$arDelivertOrderEx["VALUE"]],false,false)->Fetch();
			//echo "<pre>";	print_r($arResult);	echo "</pre>";
			$sklad=" ( ".$arResult["TITLE"]." )";
		}
	}

	if ($arDelivery[$order["DELIVERY_ID"]]!="Самовывоз"){$sklad="";}






	if ($curl=="y"){
		echo "\n\n cURL \n\n";
		$data = array("query" => $orderINN);                                                                    
		$data_string = json_encode($data);                                                                                   

		$ch = curl_init('https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',   
		    'Accept: application/json', 
		    'Authorization: Token a63b3aac3fe2a636a332fcbf1095c1dac5b0f1cf', 
		    'Content-Length: ' . strlen($data_string))                                                                       
		);

		$result = json_decode(curl_exec($ch), true);
		print_r ($result);
		echo "\n";


	}

	// Добавляем данные
	$uri = "https://dadata.ru/find/party/".$orderINN."/";
	$PHPExcelOrder->getActiveSheet()->getCell('G'.$irow)->getHyperlink()->setUrl($uri);
	$PHPExcelOrder->getActiveSheet()->getStyle('G'.$irow)->applyFromArray($styleArrayUri);

	$strADDRESS_ROOM="";
	$strADDRESS_BLOCK="";
	$strADDRESS_BUILDING="";
	$strADDRESS_STREET="";
	$strUF_DOB="";
	
	if ($orderADDRESS_ROOM!=""){$strADDRESS_ROOM="офис ".$orderADDRESS_ROOM;}
	if ($orderADDRESS_BLOCK!=""){$strADDRESS_BLOCK="корп. ".$orderADDRESS_BLOCK;}
	if ($orderADDRESS_BUILDING!=""){$strADDRESS_BUILDING="дом ".$orderADDRESS_BUILDING;}
	if ($orderADDRESS_STREET!=""){$strADDRESS_STREET="".$orderADDRESS_STREET;}
	//if ($orderREGION!=""){$strREGION="Регион ".$orderREGION;}

	$address=$strADDRESS_STREET." ".$strADDRESS_BUILDING." ".$strADDRESS_BLOCK." ".$strADDRESS_ROOM." ";

	if ($orderUF_DOB!=""){$strUF_DOB="доб. ".$orderUF_DOB;}
	
	$tel=$orderPHONE." ".$strUF_DOB;
	
	$PHPExcelOrder->setActiveSheetIndex(0)
	->setCellValue('A'.$irow, $i)
	->setCellValue('B'.$irow, date("d.m.Y", strtotime($oborder->getDateInsert())))
	->setCellValue('C'.$irow, $oborder->getId())
	->setCellValue('D'.$irow, $strUser)
	->setCellValue('E'.$irow, $orderCOMPANY)
	->setCellValue('F'.$irow, $orderINN)
	->setCellValue('G'.$irow, $orderCOMPANY)
	->setCellValue('H'.$irow, '')
	->setCellValue('I'.$irow, '')
	->setCellValue('J'.$irow, '')
	->setCellValue('K'.$irow, "")
	->setCellValue('L'.$irow, '')
	->setCellValue('M'.$irow, '')
	->setCellValue('N'.$irow, $arBasketItemsName[$order['ID']]["NAME"])
	->setCellValue('O'.$irow, '')
	->setCellValue('P'.$irow, $oborder->getPrice())
	->setCellValue('Q'.$irow, $orderCOMMENT)
	->setCellValue('R'.$irow, $tel)
	->setCellValue('S'.$irow, $orderCONTACT_PERSON)
	->setCellValue('T'.$irow, $orderEMAIL)

	->setCellValue('U'.$irow, $orderADDRESS_INDEX)
	->setCellValue('V'.$irow, $orderREGION)
	->setCellValue('W'.$irow, $orderADDRESS_CITY)
	->setCellValue('X'.$irow, $address)
	->setCellValue('Y'.$irow, $orderADDRESS_FROM_DADATA)
	->setCellValue('Z'.$irow, $arDelivery[$order["DELIVERY_ID"]].$sklad);


	$PHPExcelOrder->getActiveSheet()->getStyle("A".$irow.":C".$irow."")->applyFromArray($styleArrayOrderCenter);
	$PHPExcelOrder->getActiveSheet()->getStyle("D".$irow.":H".$irow."")->applyFromArray($styleArrayOrder);


	$PHPExcelOrder->getActiveSheet()->getStyle("P".$irow)->applyFromArray($styleArrayCenter);
	$PHPExcelOrder->getActiveSheet()->getStyle("Q".$irow.":Z".$irow."")->applyFromArray($styleArrayOrder);

	$PHPExcelOrder->getActiveSheet()->getRowDimension($irow)->setRowHeight(21);
	$PHPExcelOrder->getActiveSheet()->getStyle("Q".$irow.":Z".$irow."")->getAlignment()->setWrapText(true);

	//echo "Состав заказа: \n";

	//$res = CSaleBasket::GetList(array(), array("ORDER_ID" => $order['ID'])); // ID заказа
	//$json_product=array();
	//while ($arItem = $res->Fetch()) {
	// var_dump($arItem);

	//$json[] = array(
	//		'name' => $arItem['NAME'],
	//		'id' => $arItem['PRODUCT_ID'],
	//		'price' => $arItem['PRICE'],
	//		'quantity' => $arItem['QUANTITY']
	//);
	//}
	$start=$irow;	//echo "Start: ".$start."\n";
	$dbBasketItems = CSaleBasket::GetList(array("NAME" => "ASC"),array("ORDER_ID" => $order['ID']),false,false);
	$rowsItems=$dbBasketItems->SelectedRowsCount();	//echo "Количество товаров: ".$rowsItems."\n\n";
	$orderList="<table border=0 cellspacing=5 cellpadding=5><tr><td style='width:45px;text-align:center'>№<br>п/п</td><td style='text-align:center'>Код товара у производителя</td><td style='text-align:center'>Название</td><td style='text-align:center'>Кол-во</td><td style='text-align:center'>Цена за ед.</td><td style='text-align:center'>Стоимость</td><td style='text-align:center'>Тип</td></tr>";
	$n=1;
	while ($arItems = $dbBasketItems->Fetch()){ //echo "<textarea>";print_r($arItems);echo "</textarea>";
		$typeoforeder="";		//echo "Товар корзины\n";	//echo $arItems["PRODUCT_ID"]."=". $arItems["NAME"]."\n";		//echo "Св-ва товара корзины\n";
		$db_res = CSaleBasket::GetPropsList(array("SORT" => "ASC","NAME" => "ASC"),array("BASKET_ID" =>$arItems["ID"]));
		$rowsProps=$db_res->SelectedRowsCount();		//echo "Количество свойств: ".$rowsProps."\n\n";
		if ($rowsProps>0){
			while ($ar_res = $db_res->Fetch()){
				if($ar_res["NAME"] == "Запрос" AND $ar_res["VALUE"]=="QUANTITY"){$typeoforeder="Большего количества";}
				if($ar_res["NAME"] == "Запрос" AND $ar_res["VALUE"]=="MULTIPLICITY"){$typeoforeder="Не кратных остатков";$arItems["QUANTITY"]=0;}
				if($ar_res["NAME"] == "Запрос" AND $ar_res["VALUE"]=="MISSING"){$typeoforeder="Условия поставки";}
				if($ar_res["NAME"] == "Образец" AND $ar_res["VALUE"]=="Y"){$typeoforeder="Образец";}
			}
		}else{
			$typeoforeder="Покупка";
		}
		$arFilter = Array("ID"=>$arItems["PRODUCT_ID"]);
		$res = CIBlockElement::GetList(false, $arFilter, false, false, Array("*"));
		$ob = $res->GetNextElement();
		// Поля товара 
		$arFieldsNew = $ob->GetFields();	//echo "Поля товара корзины\n";print_r($arFieldsNew);echo "\n";
		// Все свойства товара
		$arProps = $ob->GetProperties();

		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('H'.$irow, $arProps["ITEMID"]["VALUE"]);
		// Код товара у производителя
		$uri=$domen2.$arFieldsNew['DETAIL_PAGE_URL'];
		$orderList.="<tr><td>$n </td><td><a href='".$uri."' target='_blank'> ".$arProps["ARTICLE"]["VALUE"]."</a></td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('I'.$irow, $arProps["ARTICLE"]["VALUE"]);
		$PHPExcelOrder->getActiveSheet(0)->getCell('I'.$irow)->getHyperlink()->setUrl($uri);
		$PHPExcelOrder->getActiveSheet(0)->getStyle('I'.$irow)->applyFromArray($styleArrayUri);

		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('J'.$irow, $arManufacturers[$arProps["PRODUCERID"]["VALUE"]]['NAME']);
		// Название
		$orderList.="<td>".$arItems["NAME"]."</td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('K'.$irow, $arItems["NAME"]);
		// Кол-во
		$orderList.="<td>".$arItems["QUANTITY"]."</td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('L'.$irow, $arItems["QUANTITY"]);
		// Цена за ед.
		$arItems["PRICE"] = substr($arItems["PRICE"], 0, -2);
		$orderList.="<td>".$arItems["PRICE"]."</td>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('M'.$irow, $arItems["PRICE"]);
		// Стоимость
		$orderList.="<td>".$arItems["QUANTITY"]*$arItems["PRICE"]."</td>";
		// Тип
		$orderList.="<td>".$typeoforeder."</td></tr>";
		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('N'.$irow, $typeoforeder);
		$PHPExcelOrder->setActiveSheetIndex(0)->getStyle('N'.$irow)->getAlignment()->setWrapText(true);

		$PHPExcelOrder->setActiveSheetIndex(0)->setCellValue('O'.$irow, $arItems["QUANTITY"]*$arItems["PRICE"]);

		$PHPExcelOrder->getActiveSheet(0)->getRowDimension($irow)->setRowHeight(21);

		$PHPExcelOrder->getActiveSheet(0)->getStyle("H".$irow)->applyFromArray($styleArrayItemCenter);
		$PHPExcelOrder->getActiveSheet(0)->getStyle("I".$irow.":K".$irow."")->applyFromArray($styleArrayItem);
		$PHPExcelOrder->getActiveSheet(0)->getStyle("L".$irow.":O".$irow."")->applyFromArray($styleArrayItemCenter);

		$irow++;
		$n++;
	}
	if ($rowsItems>1){ 
		$finish=$irow-1;
		//echo "Finish: ".$finish."\n";
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("A".$start.":A".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("B".$start.":B".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("C".$start.":C".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("D".$start.":D".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("E".$start.":E".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("F".$start.":F".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("G".$start.":G".$finish."");

		$PHPExcelOrder->getActiveSheet(0)->mergeCells("P".$start.":P".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("Q".$start.":Q".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("R".$start.":R".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("S".$start.":S".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("T".$start.":T".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("U".$start.":U".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("V".$start.":V".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("W".$start.":W".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("X".$start.":X".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("Y".$start.":Y".$finish."");
		$PHPExcelOrder->getActiveSheet(0)->mergeCells("Z".$start.":Z".$finish."");
	}

	$i++;
	if ($rowsItems==0){$irow++;}
	$line=$irow-1;
	$PHPExcelOrder->getActiveSheet()->getStyle("A".$line.":Z".$line."")->applyFromArray($styleArrayOrderBorder);
	//if($i==2){break;}

}
//echo "</textarea><br>";
$orderList.="</table>";
echo $orderList;


// Сохраняем файл Excel 2007
$objWriter = PHPExcel_IOFactory::createWriter($PHPExcelOrder, 'Excel2007');
$objWriter->save($filename);
$PHPExcelOrder->disconnectWorksheets();
unset($PHPExcelOrder);

}


}
?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>