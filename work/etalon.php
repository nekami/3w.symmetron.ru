<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Эталон");
?>
<div class="row">
	<div class="col-xs-12">
		
		<ul>
			<li><a href="?method=check_cat">check_cat - Проверка на наличие позиции в каталоге</a></li>
			<li><a href="?method=nomenclature">nomenclature — получение информации об элементе каталога по артикулу</a></li>
			<li><a href="?method=load_groups">load_groups — получение списка групп каталога</a></li>
			<li><a href="?method=load_manufacturers">load_manufacturers — получение списка производителей</a></li>
			<li><a href="?method=load_manufacturer">load_manufacturer — получение данных для производителя</a></li>
			<li><a href="?method=nomenclature_search">nomenclature_search — получение данных для производителя</a></li>
			<li><a href="?method=get_elements">get_elements — загрузка позиций по идентификаторам</a></li>
			<li><a href="?method=load_group">load_group — загрузка данных группы</a></li>
		</ul>
		
		
		<?if (isset($_GET['method'])):?>
		
			<?
			$freechipsApi = new CFreechipsApi('NuYB_COS57a07avOy3PG9YdgbHb0B4Sm');
			
			$method = $_GET['method'];
			$params = '';
			
			if ($method == 'check_cat') {
				$params = 'ВВзБГ-ХЛ'; // max232dr
			} else if ($method == 'nomenclature') {
				$params = 'CR0805-JW-202ELF'; // max232dr
				
			} else if ($method == 'load_manufacturer') {
				$params = '254';
			} else if ($method == 'nomenclature_search') {
				$params = array(
					'manufacturers' => array(251),
					//'name'          => 'element_name',
				);
			} else if ($method == 'get_elements') {
				$params = array(
					'manufacturers' => array(251),
					//'name'          => 'element_name',
				);
			} else if ($method == 'load_group') {
				$params = '0a9173cf-7862-11e6-bf5d-9cd21ef7021a';
			}
			
			$params = $_GET['pn'];
			$result = $freechipsApi->exec($method, $params);
			
			echo 'Response: <br>', $url, '<br>';
			echo '<pre>', print_r($result), '</pre>';
			
			?>
		
		<?else:?>
		<h2>Не выбран метод API</h2>
		<?endif?>
		
	</div>
</div>


<?
class CFreechipsApi {
	private $api_key = '';
	
	public function __construct($api_key)
	{
		$this->api_key = $api_key;
	}
	
	public function exec($method, $params = '')
	{
		$url = 'http://freechips.ru/'.$method.'/';
		
		$http_method = 'GET';
		
		switch ($method) {
			case 'nomenclature_search':
			case 'get_elements': { $http_method = 'POST'; } break;
		}
		
		
		$content = '';
		
		if ($http_method == 'POST') {
			$content = json_encode($params);
		} else {
			if ($params != '') {
				$url .= $params.'/';
			}
		}
		
		
		
		
		$opts = array(
			'http' => array(
				'method'  => $http_method,
				
				'header'  => "Authorization: Bearer ".$this->api_key."\r\n" .
							"Accept: application/json\r\n".
							"Content-Type: application/json\r\n".
							"Cache-Control: no-cache\r\n",
									
				'content' => $content
			)
		);
		
		
		echo 'Request: <br>', $url, '<br>';
		echo '<pre>', print_r($opts), '</pre>';


		$context = stream_context_create($opts);
		$res = file_get_contents($url, true, $context);
		
		
		$result_str = '';
		
		if ($method == 'load_group' || $method == 'get_elements') {
			$result_str = $method.'_'.time().'.zip';
			
			file_put_contents($result_str, $res);
		} else {
			$result_str = json_decode($res);
		}
		
		
		return $result_str;
	}
}
?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>