<?
function sendMessageMail($to,$replyto, $from, $title, $html, $filename){


    $EOL = "\r\n"; // ограничитель строк, некоторые почтовые сервера требуют \n - подобрать опытным путём 
    $boundary     = "--".md5(uniqid(time()));  // любая строка, которой не будет ниже в потоке данных.  
    $headers    = "MIME-Version: 1.0;$EOL";   
    $headers   .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";  
    $headers   .= "From: $from"."\r\n";
	//$headers   .= "Reply-To: $replyto"."\r\n";
	//$headers .= "Bcc: $replyto"."\r\n";
	$headers .= "cc: $replyto"."\r\n";

	$multipart  = "--$boundary$EOL";   
	$multipart .= "Content-Type: text/html; charset=utf-8$EOL";   
	$multipart .= "Content-Transfer-Encoding: base64$EOL";
	$multipart .= $EOL; // раздел между заголовками и телом html-части 
	$multipart .= chunk_split(base64_encode($html));   

	foreach ($filename as $k=>$v){
		$path = $_SERVER['DOCUMENT_ROOT'].$v;
	 	if ($path) {
			$fp = fopen($path,"rb");   
	 		if (!$fp){ print "Cannot open file:".$path; exit(); }   
	 		$file = fread($fp, filesize($path));   
	 		fclose($fp);   
		}  
		$name = basename($v); // в этой переменной надо сформировать имя файла (без всякого пути)  

		$multipart .= "$EOL--$boundary$EOL";
		$multipart .= "Content-Type: application/vnd.ms-excel$EOL";   
		$multipart .= "Content-Transfer-Encoding: base64$EOL";   
		$multipart .= "Content-Disposition: attachment; filename=\"$name\"$EOL";   
		$multipart .= $EOL; // раздел между заголовками и телом прикрепленного файла 
		$multipart .= chunk_split(base64_encode($file));   
	}
	$multipart .= "$EOL--$boundary--$EOL";   

    if(!mail($to, $title, $multipart, $headers)){
		return False; //если не письмо не отправлено
	}else { //// если письмо отправлено
    return True;}  
	  exit;  
}
?>