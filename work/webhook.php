Webhook
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arOptions = array(


	  'TEST_0' => array(
      'GROUP' => 'MAIN',
      'TITLE' => 'Адрес портала ',
      'TYPE' => 'STRING',
      'DEFAULT' => 'b24dev.symmetron.ru',
      'SORT' => '0',
      'NOTES' => '',
      'DISABLE' => '',

   ),
      'TEST_1' => array(
      'GROUP' => 'MAIN',
      'TITLE' => 'Ключ для обмена ',
      'TYPE' => 'STRING',
      'DEFAULT' => '3349',
      'SORT' => '1',
      'NOTES' => '',
      'DISABLE' => '',
   ),
      'TEST_2' => array(
      'GROUP' => 'MAIN',
      'TITLE' => 'ID Пользователя ',
      'TYPE' => 'STRING',
      'DEFAULT' => 'cnjj4rv3gcrfhrb4',
      'SORT' => '2',
      'NOTES' => '',
      'DISABLE' => '',
   ),


	// вторая закладка
   'TEST_49' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'TITLE ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$sTITLE.$sNumber',
      'SORT' => '1',
      'NOTES' => '',
      'DISABLE' => '',
   ),
   'TEST_52' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'LAST_NAME ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$sLAST_NAME',
      'SORT' => '2',
      'NOTES' => '',
      'DISABLE' => '',
   ),
      'TEST_53' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'PHONE ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$arPhone',
      'SORT' => '3',
      'NOTES' => '',
      'DISABLE' => '',
   ),
      'TEST_54' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'EMAIL ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$arEmail',
      'SORT' => '4',
      'NOTES' => '',
      'DISABLE' => '',
   ),
	  'TEST_55' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'COMMENTS ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$sComment',
      'SORT' => '5',
      'NOTES' => '',
      'DISABLE' => '',
   ),
      'TEST_56' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'COMPANY_TITLE ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$sCOMPANY_TITLE',
      'SORT' => '6',
      'NOTES' => '',
      'DISABLE' => '',
   ),
      'TEST_57' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'INN ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$sINN',
      'SORT' => '7',
      'NOTES' => '',
      'DISABLE' => '',
   ),
      'TEST_58' => array(
      'GROUP' => 'MAIN2',
      'TITLE' => 'Сумма ',
      'TYPE' => 'STRING',
      'DEFAULT' => '$sSumma',
      'SORT' => '8',
      'NOTES' => '',
      'DISABLE' => '',
   ),
);
$arCurOptionValues = array();
$module_id = 'mixart.symmetron';
foreach($arOptions as $opt => $arOptParams){
	if($arOptParams['TYPE'] != 'CUSTOM'){
		$arCurOptionValues[$opt] = COption::GetOptionString($module_id, $opt, $arOptParams['DEFAULT']);
		
		if(in_array($arOptParams['TYPE'], array('MSELECT')))
			$arCurOptionValues[$opt] = unserialize($arCurOptionValues[$opt]);
	}
}
	echo "<br><textarea rows=10 cols=145 name=text>";print_r($arCurOptionValues);echo "</textarea><br>";
	die;
	$http = new \Bitrix\Main\Web\HttpClient();
	$host="b24dev.symmetron.ru";
	$user="3349";
	$tokenID="cnjj4rv3gcrfhrb4";
	

	$sTITLE="Заказ с сайта №";
	$sNumber="1275";
	$sLAST_NAME="Меган Фокс";
		
	$sPhone="+7(123)456-78-99";
	$sEmail="test@mail.ru";
	
	$sCOMPANY_TITLE="ООО";
	$sINN="51651534";
	$sSumma="100256"." руб.";
	
	$sComment="Комментарий";
	
	//Форматируем телефон и почту для сохранения
    $arPhone = (!empty($sPhone)) ? ['VALUE' => $sPhone, 'VALUE_TYPE' => 'MOBILE'] : [];
    $arEmail = (!empty($sEmail)) ? ['VALUE' => $sEmail, 'VALUE_TYPE' => 'WORK'] : [];


	$url = 'order.821.xlsx';
	$data = file_get_contents($url);
	$base64 = base64_encode($data);
	$name = urldecode(basename($url));
	
	 $json = $http->post(
      'https://'.$host.'/rest/'.$user.'/'.$tokenID.'/crm.lead.add/',
      [
         'fields' => [
				'TITLE' => $sTITLE.$sNumber,
				'LAST_NAME' => $sLAST_NAME,
				'PHONE' => [
					'n0'=>$arPhone
					],
				'EMAIL' => [
					'n0'=>$arEmail
					],
				'COMMENTS' => $sComment,
				'COMPANY_TITLE' => $sCOMPANY_TITLE,
				'UF_CRM_1576501434424' => $sINN,
				'UF_CRM_1584516757940' => $sSumma,
				'CURRENCY_ID' => 'RUB',
				'SOURCE_ID' => 'WEB',
				'UF_CRM_1576502174' => ['fileData' => [$name, $base64]],
		 ],


  ]
   );
   $result = \Bitrix\Main\Web\Json::decode($json);
  echo "<pre>";print_r($result);echo "</pre>";
  //echo "data:<br><pre>";print_r($data);echo "</pre>";
  	if(array_key_exists('error', $result)){      
		CEventLog::Add(array(
			"SEVERITY" => "SECURITY",
			"AUDIT_TYPE_ID" => "LID",
			"MODULE_ID" => "mixart.symmetron",
			"ITEM_ID" => 1,
			"DESCRIPTION" => "Модуль mixart.symmetron не добавил лид из-за ошибки:".$result['error_description']."",
		));
		die;
	}else{	
		CEventLog::Add(array(
			"SEVERITY" => "SECURITY",
			"AUDIT_TYPE_ID" => "LID",
			"MODULE_ID" => "mixart.symmetron",
			"ITEM_ID" => 1,
			"DESCRIPTION" => "Модуль mixart.symmetron добавил лид.",
		));
	}

  /*
  
  if (count($result['result']) > 0){
      foreach ($result['result'] as $lead){ $leadID = $lead['ID'];  }
   } else{  $finish = true;  }
	*/
	/*
// обращаемся к Битрикс24 при помощи функции curl_exec
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_SSL_VERIFYPEER => 0,
  CURLOPT_POST => 1,
  CURLOPT_HEADER => 0,
  CURLOPT_RETURNTRANSFER => 1,
  CURLOPT_URL => $queryUrl,
  CURLOPT_POSTFIELDS => $queryData,
));
$result = curl_exec($curl);
curl_close($curl);
$result = json_decode($result, 1);
echo "<pre>";print_r($result);echo "</pre>";
//if (array_key_exists('error', $result)) echo "Ошибка при сохранении лида: ".$result['error_description']."<br/>";
	// если произошла какая-то ошибка - выведем её
	if(array_key_exists('error', $result))
	{      
		die("Ошибка при сохранении лида: ".$result['error_description']);
	}else{	
		echo "Лид добавлен, отличная работа :)";
	}
	*/

/*
	$url = 'https://3w.symmetron.ru/order.821.xlsx';
	$data = file_get_contents($url);
	$base64 = base64_encode($data);
	$name = urldecode(basename($url));
	
				'UF_CRM_1590410699' => ['fileData' => [$name, $base64]],	
*/
?>