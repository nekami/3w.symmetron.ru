<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("iblock"))    die("");

$arElementsChecked = [];
if(htmlspecialchars($_GET["ELEMENT_ID"])) {
    $rsElementsChecked = CIBlockElement::GetList([], ["ID" => htmlspecialchars($_GET["ELEMENT_ID"])], false, false,
        ["PROPERTY_" . $_GET["PROP_REG_AJAX"]]);
    while ($arElementChecked = $rsElementsChecked->Fetch()) {
        if(!empty($arElementChecked["PROPERTY_" . $_GET["PROP_REG_AJAX"] . "_VALUE"]["VALUE"]))
            $arElementsChecked[] = $arElementChecked["PROPERTY_" . $_GET["PROP_REG_AJAX"] . "_VALUE"]["VALUE"];
    }
}


$IBLOCK_ID = htmlspecialchars($_GET["IBLOCK_ID"]);
$SECTIONS = array_filter($_GET["SECTIONS"], function ($el){return !empty($el);});
$PROP_REG_AJAX = htmlspecialchars($_GET["PROP_REG_AJAX"]);

if(empty($SECTIONS) || empty($IBLOCK_ID))
    die("");

$rsProperty = CIBlockProperty::GetList(
    [],
    ['IBLOCK_ID' => $IBLOCK_ID]
);

$arPropsNames = [];
while($arProperty = $rsProperty->Fetch())
{
    if(!in_array($arProperty["ID"], $arElementsChecked))
        $arPropsNames[$arProperty["ID"]] = $arProperty;
}


$arSelect = ["ID", "IBLOCK_ID"];
$arFilter = ["IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>$SECTIONS];
$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$arElements = [];
while($arElement = $rsElements->Fetch()){
    $arElements[] = $arElement["ID"];
}


$arRegulations = [];
if(!empty($arPropsNames) && !empty($arElements)) {
    $propElements = [];
    if(Cmodule::IncludeModule("mcart.basketsymmetron")){
        $propElements = \Mcart\BasketSymmetron\ElementPropertyTable::getList([
            "filter" => [
                "IBLOCK_PROPERTY_ID" => array_keys($arPropsNames),
                "IBLOCK_ELEMENT_ID" => $arElements,
                "!VALUE" => ""
            ]
        ]);

        while($arProp = $propElements->Fetch()){
            if ((!empty(trim($arProp["VALUE"])) && trim($arProp["VALUE"]) != "NaN") || is_array($arProp["VALUE"])) {
                $arRegulations[$arPropsNames[$arProp["IBLOCK_PROPERTY_ID"]]["CODE"]] = $arProp["IBLOCK_PROPERTY_ID"];
            }
        }
    }

    //echo "<pre>";
    //print_r($arRegulations);
    //echo "</pre>";
    //die();
}


if(!empty($arRegulations)){


    //create HTML for return ajax
    $count = intval(htmlspecialchars($_GET["SELECT_LEN"]));
    ob_start();
    foreach ($arRegulations as $reg){

        $n = preg_replace("/[^a-zA-Z0-9_\\[\\]]/", "", "PROP[$PROP_REG_AJAX][n$count][VALUE]");
        $md5 = md5($n);

        ?>

        <tr>
            <td>
                <div style="display: inline-block">
<?
// Сортировка
// 3000
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522" )
	{echo "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' value='3000' type='text' size='5'>";}
// 2000
elseif( $reg == "3041" || $reg == "3108" || $reg == "3111" || $reg == "3589" || $reg == "3548" || $reg == "3486" || $reg == "3598" || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646" )
	{echo "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' value='2000' type='text' size='5'>";}
// 2100
elseif( $reg == "3498" ) {echo "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' value='2100' type='text' size='5'>";}
// 2200
elseif( $reg == "3112" ) {echo "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' value='2200' type='text' size='5'>";}
// 100
elseif( $reg == "3534" ) {echo "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' value='100' type='text' size='5'>";}
// 500 (все остальные)
else {echo "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SORT]' value='500' type='text' size='5'>";}

?>
                    <input name="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]" id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]"
                           value="<?= $reg ?>" onblur="propNameAjax(this, 'PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]')"
                           size="5" type="text">

                    <input type="button" value="..."
                           onclick="jsUtils.OpenWindow('/bitrix/admin/mcart.sortcatalogsm/iblock_property_search.php?lang=ru&IBLOCK_ID=<?= $IBLOCK_ID ?>&n=PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]&k=n0&propid=<?=$PROP_REG_AJAX?>', 900, 700);">

                    <span id="sp_PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]_n0"><?=$arPropsNames[$reg]["NAME"]?></span>

                    <span onclick='deleteItem(this)' style='color: red;'>X</span>
                    </br>
                    <table>
                        <tr>
                            <th><?= GetMessage("SORTCATALOG_SHORT_NAME"); ?></th>
                            <th><?= GetMessage("SORTCATALOG_HINT"); ?></th>
                            <th>Выводить на листе</th>
                        </tr>
                        <tr>
                            <td id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]_HINT_S"><input name=''
                                                                                                         id='<?=$PROP_REG_AJAX?>_<?= $reg ?>_S'
                                                                                                         value='<?=$arPropsNames[$reg]["NAME"]?>'
                                                                                                         type="text" size="20"
                                                                                                         onchange="hintProperty(this, 'S', '<?= $reg ?>')">
                            </td>
                            <td id="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][VALUE]_HINT_L"><input name=''
                                                                                                         id='<?=$PROP_REG_AJAX?>_<?= $reg ?>_L'
                                                                                                         value='<?=$arPropsNames[$reg]["HINT"]?>'
                                                                                                         type="text" size="20"
                                                                                                         onchange="hintProperty(this, 'L', '<?= $reg ?>')">
                            </td>
                            <td align="center">
<?
// Вывод на лист
// нет
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118" || $reg == "3122" || $reg == "3123" || $reg == "3304" 
  || $reg == "3522" || $reg == "3041" || $reg == "3108" || $reg == "3111" || $reg == "3498" || $reg == "3589" || $reg == "3548" || $reg == "3486" 
  || $reg == "3598" || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646" || $reg == "3534" || $reg == "3112")
     {echo "<input type='checkbox' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SHOW]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SHOW]' >";}
// да (все остальные)
else {echo "<input type='checkbox' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SHOW]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SHOW]' checked>";}

?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="display: inline-block">
                    <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SET_SORT]">
                        Настройка сортировки
                    </label>
<?
// Настройка сортировки
// нет
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SET_SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SET_SORT]' type='checkbox'>";}
// да (все остальные)
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SET_SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SET_SORT]' type='checkbox' checked>";}

?>
                    <table>
                        <tr>
                            <td>
                                <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][PARAM_SEARCH][VALUE]">
                                    Параметрический Поиск
                                </label>
                            </td>
                            <td>
<?
// Параметрический Поиск 1
// нет
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522"
 || $reg == "3041"  || $reg == "3108" || $reg == "3111" || $reg == "3112" || $reg == "3486" || $reg == "3498" || $reg == "3548" || $reg == "3589" || $reg == "3598" 
 || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646" )
     {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][VALUE]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][VALUE]' type='checkbox'>";}
// да (все остальные)
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][VALUE]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][VALUE]' type='checkbox' checked>";}

?>
                            </td>
                            <td>
<?
// Параметрический Поиск 2
// 3000
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522" || $reg == "3041")
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' value='3000' type='text' size='5'>";}
// 2000
elseif ( $reg == "3108" || $reg == "3111" || $reg == "3589" || $reg == "3548" || $reg == "3486" || $reg == "3598" || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' value='2000' type='text' size='5'>";}
// 2100
elseif ( $reg == "3498" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' value='2100' type='text' size='5'>";}
// 2200
elseif ( $reg == "3112" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' value='2200' type='text' size='5'>";}
// 100
elseif ( $reg == "3534" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' value='100' type='text' size='5'>";}
// 500 (все остальные)
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][PARAM_SEARCH][SORT]' value='500' type='text' size='5'>";}

?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SETTINGS_WINDOW][VALUE]">
                                    Шестерёнка
                                </label>
                            </td>
                            <td>
<?
// Шестерёнка 1
// нет
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118" || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522"
  || $reg == "3041" || $reg == "3108" || $reg == "3111" || $reg == "3112" || $reg == "3486" || $reg == "3498" || $reg == "3548" || $reg == "3589" || $reg == "3598"
  || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646")
	{echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][VALUE]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][VALUE]' type='checkbox'>";}
// да (все остальные)
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][VALUE]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][VALUE]' type='checkbox' checked>";}
								
?>
                            </td>
                            <td>
<?
// Шестерёнка 2
// 3000
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522" || $reg == "3041")
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' value='3000' type='text' size='5'>";}
// 2000
elseif( $reg == "3108" || $reg == "3111" || $reg == "3486" || $reg == "3548" || $reg == "3589" || $reg == "3598" || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' value='2000' type='text' size='5'>";}
// 2100
elseif( $reg == "3498" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' value='2100' type='text' size='5'>";}
// 2200
elseif( $reg == "3112" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' value='2200' type='text' size='5'>";}

// 100	
elseif( $reg == "3534" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' value='100' type='text' size='5'>";}
// да (все остальные)	
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SETTINGS_WINDOW][SORT]' value='500' type='text' size='5'>";}

?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="PROP[<?=$PROP_REG_AJAX?>][n<?=$count?>][VALUE][SPECIFICATION][VALUE]">
                                    Спецификация
                                </label>
                            </td>
                            <td>
<?
// Спецификация 1
// нет
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522" )
     {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][VALUE]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][VALUE]' type='checkbox' >";}
// да (все остальные)
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][VALUE]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][VALUE]' type='checkbox' checked>";}

?>
                            </td>
                            <td>
<?
// Спецификация 2
// 3000
if ( $reg == "3040" || $reg == "3042" || $reg == "3052" || $reg == "3056" || $reg == "3077" || $reg == "3109" || $reg == "3113" || $reg == "3118"  || $reg == "3122" || $reg == "3123" || $reg == "3304" || $reg == "3522" || $reg == "3041")
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' value='3000' type='text' size='5'>";}
// 2000
elseif ( $reg == "3108" || $reg == "3111" || $reg == "3589" || $reg == "3548" || $reg == "3486" || $reg == "3598" || $reg == "3626" || $reg == "3644" || $reg == "3645" || $reg == "3646" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' value='2000' type='text' size='5'>";}
// 2100
elseif ( $reg == "3498" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' value='2100' type='text' size='5'>";}
// 2200
elseif ( $reg == "3112" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' value='2200' type='text' size='5'>";}
// 100
elseif( $reg == "3534" )
	 {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' value='100' type='text' size='5'>";}
// 500
else {echo "<input id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE][SPECIFICATION][SORT]' value='500' type='text' size='5'>";}

?>
                            </td>
                        </tr>
                    </table>
                </div>

                </br>
                <hr width="60%" align="left">
                </br>
            </td>
        </tr>



        <?

        /*$ResultHTML .= "<tr><td>";

        $ResultHTML .= "<select name='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE]' id='PROP[".$PROP_REG_AJAX."][n".$count."][VALUE]'>";
        $ResultHTML .= "<option value=''>-</option>";
        foreach ($arProps as $key=>$val) {
            if($reg == $key){
                $ResultHTML .= "<option selected value='" . $key . "'>" . $val . "</option>";
            } else {
                $ResultHTML .= "<option value='" . $key . "'>" . $val . "</option>";
            }
        }
        $ResultHTML .= "</select> ";
        $ResultHTML .= "<input name='PROP[".$PROP_REG_AJAX."][n".$count."][DESCRIPTION]' id='PROP[".$PROP_REG_AJAX."][n".$count."][DESCRIPTION]'
        value='200'>";
        $ResultHTML .= " <span onclick='deleteItem(this)' style='color: red;'>X</span></br>";
        $ResultHTML .= "</td></tr>";*/
        $count++;
    }

    $html_value = ob_get_contents();
    ob_end_clean();

    //$ResultHTML .= "<tr><td><input type=\"button\" value=\"Добавить\" onclick=\"addNewRow('tbdf4285bc1d07139fc6122003f756eaea')\"></td></tr>";

    //$ResultHTML .= "</tbody>";

    $GLOBALS['APPLICATION']->RestartBuffer();
    echo $html_value;
    die;
} else {
    $GLOBALS['APPLICATION']->RestartBuffer();
    echo "";
    die;
}

//return ajax result

/*echo "<pre>";
print_r($_GET);
echo "</pre>";
echo "<pre>";
print_r($pointSection);
echo "</pre>";
die;*/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>