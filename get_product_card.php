<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if(isset($_REQUEST['id'])){
	$arOrder = array("SORT" => "ASC");
	$arFilter = array(
	    'IBLOCK_ID' => 4,
	    'ACTIVE' => 'Y',
	    'ID' => $_REQUEST['id'],
	);
	
	$arSelect = array(
	    'ID',
	    'CODE',
	    'IBLOCK_ID',
	    'DETAIL_PAGE_URL',
	);
	
	$dbRes = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	if($resR = $dbRes->GetNext()){
	    LocalRedirect($resR['DETAIL_PAGE_URL']);
	}else{
		LocalRedirect('/');
	}
}else{
	LocalRedirect('/');
}
